<?php

namespace service;

use PHPUnit\Framework\TestCase;
use saschahuber\affiliatewebsitecore\service\AffiliateDashboardMenuService;

class AffiliateDashboardMenuServiceTest extends TestCase
{
    public function testIsCurrentUrl(){
        $this->assertFalse(AffiliateDashboardMenuService::isCurrentUrl('/dashboard/settings', ['dashboard', 'settings', 'user']));
        $this->assertFalse(AffiliateDashboardMenuService::isCurrentUrl('/dashboard/ads', ['dashboard', 'settings', 'user']));
        $this->assertTrue(AffiliateDashboardMenuService::isCurrentUrl('/dashboard/settings/user', ['dashboard', 'settings']));
        $this->assertTrue(AffiliateDashboardMenuService::isCurrentUrl('/dashboard/settings/user', ['dashboard', 'settings', 'user']));
    }
}
