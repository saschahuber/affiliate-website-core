# AffiliateWebsiteCore

# Composer

Auf Linux Shell oder in Git-Bash

`COMPOSER=composer-dev.json composer update --ignore-platform-reqs`

`COMPOSER=composer-dev-linux.json composer update --ignore-platform-reqs`

# Tests

`docker run --rm -v $(pwd)/src:/src -v $(pwd)/tests:/tests -v $(pwd)/vendor:/vendor jitesoft/phpunit phpunit --configuration /tests/phpunit.xml`

# Semantic Release Tokens

1. Personal Token erstellen (https://gitlab.com/-/user_settings/personal_access_tokens)
2. Projekt Token als 'CUSTOM_PUSH_TOKEN' als CI/CD-Variable anlegen