<?php

namespace saschahuber\affiliatewebsitecore\helper;

use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\DealPageManager;
use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductComparisonManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\service\BlogTaxonomyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\BlogThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\BrandThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\CompanyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\DealPageThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\LinkPageThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\NewsTaxonomyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\NewsThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\PageThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\ProductComparisonThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\ProductTaxonomyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\ProductThumbnailGeneratorService;
use saschahuber\saastemplatecore\helper\FileHelper;

class ThumbnailHelper{
    CONST TYPE_PAGE = 'page';
    CONST TYPE_BLOG = 'blog';
    CONST TYPE_BLOG_CATEGORY = 'blog_category';
    CONST TYPE_BRAND = 'brand';
    CONST TYPE_DEAL_PAGE = 'deal-page';
    CONST TYPE_LINK_PAGE = 'link-page';
    CONST TYPE_NEWS = 'news';
    CONST TYPE_NEWS_CATEGORY = 'news_category';
    CONST TYPE_PRODUCT = 'produkt';
    CONST TYPE_PRODUCT_CATEGORY = 'produkt_category';
    CONST TYPE_PRODUCT_COMPARISON = 'product-comparison';
    const TYPE_COMPANY = 'company';

    public static function handleRequest(){
        $parsed_url = parse_url($_SERVER['REQUEST_URI']);

        $path = $parsed_url['path'];

        $path_parts = explode('/', trim($path, '/'));

        if(count($path_parts) < 5 || $path_parts[count($path_parts)-4] !== "thumbnail"){
            return;
        }
        
        $filename = $path_parts[count($path_parts)-1];
        $item_id = $path_parts[count($path_parts)-2];
        $item_type = $path_parts[count($path_parts)-3];
        
        switch ($item_type){
            case self::TYPE_PAGE:
                return (new PageThumbnailGeneratorService())->generatePageThumbnail((new PageManager())->getById($item_id));
            case self::TYPE_BLOG:
                return (new BlogThumbnailGeneratorService())->generateBlogThumbnail((new PostManager())->getById($item_id));
            case self::TYPE_BLOG_CATEGORY:
                return (new BlogTaxonomyThumbnailGeneratorService())->generateBlogTaxonomyThumbnail((new PostManager())->getTaxonomyById($item_id));
            case self::TYPE_BRAND:
                return (new BrandThumbnailGeneratorService())->generateBrandThumbnail((new BrandManager())->getById($item_id));
            case self::TYPE_DEAL_PAGE:
                return (new DealPageThumbnailGeneratorService())->generateDealPageThumbnail((new DealPageManager())->getById($item_id));
            case self::TYPE_LINK_PAGE:
                return (new LinkPageThumbnailGeneratorService())->generateLinkPageThumbnail((new LinkPageItemManager())->getById($item_id));
            case self::TYPE_NEWS:
                return (new NewsThumbnailGeneratorService())->generateNewsThumbnail((new NewsManager())->getById($item_id));
            case self::TYPE_NEWS_CATEGORY:
                return (new NewsTaxonomyThumbnailGeneratorService())->generateNewsTaxonomyThumbnail((new NewsManager())->getTaxonomyById($item_id));
            case self::TYPE_PRODUCT:
                return (new ProductThumbnailGeneratorService())->generateProductThumbnail((new ProductManager())->getById($item_id));
            case self::TYPE_PRODUCT_CATEGORY:
                return (new ProductTaxonomyThumbnailGeneratorService())->generateProductTaxonomyThumbnail((new ProductManager())->getTaxonomyById($item_id));
            case self::TYPE_PRODUCT_COMPARISON:
                return (new ProductComparisonThumbnailGeneratorService())->generateProductComparisonThumbnail((new ProductComparisonManager())->getById($item_id));
            case self::TYPE_COMPANY:
                return (new CompanyThumbnailGeneratorService())->generateCompanyThumbnail((new CompanyManager())->getById($item_id));
        }
    }

    public static function deleteThumbnail($item_type, $item_id){
        //Enthält nur "a-z A-Z - _"
        if(preg_match('/^[a-zA-Z-_]+$/', $item_type) !== 1){
            return;
        }

        if(filter_var($item_id, FILTER_VALIDATE_INT, ['options' => ['min_range' => 1]]) === false){
            return;
        }

        $front_path = APP_BASE . "/front/data/media/generated/thumbnail/{$item_type}/{$item_id}";
        $media_path = MEDIA_DIR . "/generated/thumbnail/{$item_type}/{$item_id}";

        if(is_dir($front_path)) {
            FileHelper::removeDirectory($front_path);
        }

        if(is_dir($media_path)) {
            FileHelper::removeDirectory($media_path);
        }
    }
}