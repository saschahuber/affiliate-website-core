<?php

namespace saschahuber\affiliatewebsitecore\helper;
use AllowDynamicProperties;
use saschahuber\saastemplatecore\Database;

#[AllowDynamicProperties]
class MailTrackingHelper{
    static function handleRequest(){
        global $DB;
        $DB = new Database();

        $PATH = explode('/', trim(parse_url($_SERVER['REQUEST_URI'])['path'], '/'));
        $actual_path = $PATH;
        $actual_path[count($actual_path) - 1] = rawurldecode($actual_path[count($actual_path) - 1]);

        //Automatische WebP Generierung
        if (count($actual_path) > 3 && $actual_path[1] === "email") {
            $tracking_data = json_decode(base64_decode($actual_path[2]), true);

            self::trackMailOpening($tracking_data);

            unset($actual_path[0]);
            unset($actual_path[2]);

            $img_path = IMG_DIR . '/' . implode('/', $actual_path);

            header('Content-Type: ' . mime_content_type($img_path));
            echo file_get_contents($img_path);
            exit;
        }
    }

    static function trackMailOpening($tracking_data){
        //Todo: Mail Öffnung tracken
    }
}