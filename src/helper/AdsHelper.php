<?php

namespace saschahuber\affiliatewebsitecore\helper;

use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\saastemplatecore\Database;

class AdsHelper{

    public static function handleRequest(){
        global $CONFIG, $DB;
        $DB = new Database();

        $parsed_url = parse_url($_SERVER['REQUEST_URI']);

        if($parsed_url['path'] !== "/ads.txt"){
            return;
        }

        $ad_manager = new AdManager();

        foreach($ad_manager->getAll() as $ad){
            if($ad->active && $ad->ads_txt){
                echo $ad->ads_txt . PHP_EOL;
            }
        }

        exit();
    }
}