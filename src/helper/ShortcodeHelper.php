<?php

namespace saschahuber\affiliatewebsitecore\helper;

use saschahuber\affiliatewebsitecore\shortcode\AiAttachmentShortcode;
use saschahuber\affiliatewebsitecore\shortcode\AlertShortcode;
use saschahuber\affiliatewebsitecore\shortcode\ApnShortcode;
use saschahuber\affiliatewebsitecore\shortcode\AppBoxShortcode;
use saschahuber\affiliatewebsitecore\shortcode\AttachmentShortcode;
use saschahuber\affiliatewebsitecore\shortcode\BlogPostsShortcode;
use saschahuber\affiliatewebsitecore\shortcode\BrandOverviewShortcode;
use saschahuber\affiliatewebsitecore\shortcode\BrandProdukteAndButtonsShortcode;
use saschahuber\affiliatewebsitecore\shortcode\ButtonShortcode;
use saschahuber\affiliatewebsitecore\shortcode\CaptionShortcode;
use saschahuber\affiliatewebsitecore\shortcode\ColShortcode;
use saschahuber\affiliatewebsitecore\shortcode\DisplayProductsShortcode;
use saschahuber\affiliatewebsitecore\shortcode\DummyShortcode;
use saschahuber\affiliatewebsitecore\shortcode\FaIconShortcode;
use saschahuber\affiliatewebsitecore\shortcode\FaqShortcode;
use saschahuber\affiliatewebsitecore\shortcode\GuideFilterShortcode;
use saschahuber\affiliatewebsitecore\shortcode\HerstellerShortcode;
use saschahuber\affiliatewebsitecore\shortcode\IconButtonInfoShortcode;
use saschahuber\affiliatewebsitecore\shortcode\ImprintCopyrightShortcode;
use saschahuber\affiliatewebsitecore\shortcode\KategorieBilderGridResponsive;
use saschahuber\affiliatewebsitecore\shortcode\KategorieBilderGridShortcode;
use saschahuber\affiliatewebsitecore\shortcode\KategorieBilderShortcode;
use saschahuber\affiliatewebsitecore\shortcode\NewsletterBlogpostShortcode;
use saschahuber\affiliatewebsitecore\shortcode\NewsletterDealsShortcode;
use saschahuber\affiliatewebsitecore\shortcode\NewsletterNewsShortcode;
use saschahuber\affiliatewebsitecore\shortcode\NewsletterReviewShortcode;
use saschahuber\affiliatewebsitecore\shortcode\NewsletterShortcode;
use saschahuber\affiliatewebsitecore\shortcode\ProdukteShortcodeNeu;
use saschahuber\affiliatewebsitecore\shortcode\ProKontraShortcode;
use saschahuber\affiliatewebsitecore\shortcode\ResponsiveShortcode;
use saschahuber\affiliatewebsitecore\shortcode\RowShortcode;
use saschahuber\affiliatewebsitecore\shortcode\SearchFormShortcode;
use saschahuber\affiliatewebsitecore\shortcode\SidebarNewsletterShortcode;
use saschahuber\affiliatewebsitecore\shortcode\SocialLinkShortcode;
use saschahuber\affiliatewebsitecore\shortcode\SpacerShortcode;
use saschahuber\affiliatewebsitecore\shortcode\StockAttachmentShortcode;
use saschahuber\affiliatewebsitecore\shortcode\UploadShortcode;
use saschahuber\affiliatewebsitecore\shortcode\VideoShortcode;
use saschahuber\saastemplatecore\helper\LogHelper;

class ShortcodeHelper {
    private static $shortcode_tags = [];
    private static $shortcode_docs;

    static function getShortcodeDocs(){
        if(count(self::$shortcode_tags) < 1){
            self::initShortcodes();
        }
        return self::$shortcode_docs;
    }

    static function initShortcodes(){
        self::addShortcodeClasses([
            DummyShortcode::class,
            AlertShortcode::class,
            ApnShortcode::class,
            AppBoxShortcode::class,
            AttachmentShortcode::class,
            BlogPostsShortcode::class,
            BrandOverviewShortcode::class,
            BrandProdukteAndButtonsShortcode::class,
            ButtonShortcode::class,
            CaptionShortcode::class,
            ColShortcode::class,
            DisplayProductsShortcode::class,
            FaIconShortcode::class,
            FaqShortcode::class,
            GuideFilterShortcode::class,
            IconButtonInfoShortcode::class,
            ImprintCopyrightShortcode::class,
            KategorieBilderGridShortcode::class,
            KategorieBilderGridResponsive::class,
            KategorieBilderShortcode::class,
            NewsletterBlogpostShortcode::class,
            NewsletterDealsShortcode::class,
            NewsletterNewsShortcode::class,
            NewsletterReviewShortcode::class,
            NewsletterShortcode::class,
            ProdukteShortcodeNeu::class,
            ProKontraShortcode::class,
            ResponsiveShortcode::class,
            RowShortcode::class,
            SearchFormShortcode::class,
            SidebarNewsletterShortcode::class,
            HerstellerShortcode::class,
            SocialLinkShortcode::class,
            SpacerShortcode::class,
            StockAttachmentShortcode::class,
            AiAttachmentShortcode::class,
            UploadShortcode::class,
            VideoShortcode::class
        ]);

        global $CONFIG;

        if($CONFIG->additional_shortcode_classes){
            self::addShortcodeClasses($CONFIG->additional_shortcode_classes);
        }
    }

    static function addShortcodeClasses($classes){
        foreach ($classes as $class){
            $shortcode = new $class();

            $tags = $shortcode->getTag();

            if(!is_array($tags)){
                $tags = [$tags];
            }

            foreach($tags as $tag){
                self::addShortcode($tag, $class);
            }
        }
    }

    static function addShortcode($tag, $callback_class)
    {
        if ('' === trim($tag)) {
            LogHelper::error("HTTP", 400, 'Invalid shortcode name: Empty name given.');
            return;
        }

        if (0 !== preg_match('@[<>&/\[\]\x00-\x20=]@', $tag)) {
            LogHelper::error("HTTP", 400, sprintf(
            /* translators: 1: Shortcode name, 2: Space-separated list of reserved characters. */
                'Invalid shortcode name: %1$s. Do not use spaces or reserved characters: %2$s',
                $tag,
                '& / < > [ ] ='
            ));
            return;
        }

        self::$shortcode_tags[$tag] = $callback_class;
        $shortcode = new $callback_class();
        $documentation = $shortcode->getDocs();
        if ($documentation !== null) {
            self::$shortcode_docs[$tag] = $documentation;
        }
    }

    static function removeShortcode($tag){
        unset(self::$shortcode_tags[$tag]);
    }

    static function doShortcode( $content, $ignored_tags = []) {
        if(count(self::$shortcode_tags) < 1){
            self::initShortcodes();
        }

        $removed_tags = [];
        if(count($ignored_tags)){
            foreach($ignored_tags as $tag){
                if(isset(self::$shortcode_tags[$tag])) {
                    $removed_tags[$tag] = self::$shortcode_tags[$tag];
                }
                self::removeShortcode($tag);
            }
        }

        if ($content === null || strpos( $content, '[' ) === false) {
            return $content;
        }

        if ( empty( self::$shortcode_tags ) || ! is_array( self::$shortcode_tags ) ) {
            return $content;
        }

        preg_match_all( '@\[([^<>&/\[\]\x00-\x20=]++)@', $content, $matches );
        $tagnames = array_intersect( array_keys( self::$shortcode_tags ), $matches[1] );

        if ( empty( $tagnames ) ) {
            return $content;
        }

        $pattern = self::getShortcodeRegex( $tagnames );
        $content = preg_replace_callback( "/$pattern/", [self::class, 'doShortcodeTag'], $content );

        $content = self::unescapeInvalidShortcodes( $content );

        foreach($removed_tags as $tag => $callback){
            self::addShortcode($tag, $callback);
        }

        return $content;
    }

    static function getShortcodeAttsRegex() {
        return '/([\w-]+)\s*=\s*"([^"]*)"(?:\s|$)|([\w-]+)\s*=\s*\'([^\']*)\'(?:\s|$)|([\w-]+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|\'([^\']*)\'(?:\s|$)|(\S+)(?:\s|$)/';
    }

    static function getShortcodeRegex($tagnames = null ) {
        if ( empty( $tagnames ) ) {
            $tagnames = array_keys( self::$shortcode_tags );
        }
        $tagregexp = implode( '|', array_map( 'preg_quote', $tagnames ) );

        // WARNING! Do not change this regex without changing do_shortcode_tag() and strip_shortcode_tag().
        // Also, see shortcode_unautop() and shortcode.js.

        // phpcs:disable Squiz.Strings.ConcatenationSpacing.PaddingFound -- don't remove regex indentation
        return '\\['                             // Opening bracket.
            . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]].
            . "($tagregexp)"                     // 2: Shortcode name.
            . '(?![\\w-])'                       // Not followed by word character or hyphen.
            . '('                                // 3: Unroll the loop: Inside the opening shortcode tag.
            .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash.
            .     '(?:'
            .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket.
            .         '[^\\]\\/]*'               // Not a closing bracket or forward slash.
            .     ')*?'
            . ')'
            . '(?:'
            .     '(\\/)'                        // 4: Self closing tag...
            .     '\\]'                          // ...and closing bracket.
            . '|'
            .     '\\]'                          // Closing bracket.
            .     '(?:'
            .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags.
            .             '[^\\[]*+'             // Not an opening bracket.
            .             '(?:'
            .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag.
            .                 '[^\\[]*+'         // Not an opening bracket.
            .             ')*+'
            .         ')'
            .         '\\[\\/\\2\\]'             // Closing shortcode tag.
            .     ')?'
            . ')'
            . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]].
        // phpcs:enable
    }

    static function doShortcodeTag($m ) {
        if ( '[' === $m[1] && ']' === $m[6] ) {
            return substr( $m[0], 1, -1 );
        }

        $tag  = $m[2];
        $attr = self::shortcodeParseAtts( $m[3] );

        $content = isset( $m[5] ) ? $m[5] : null;

        $class = self::$shortcode_tags[ $tag ];

        $output = $m[1] . call_user_func( [(new $class()), 'handle'], $attr, $content, $tag ) . $m[6];


        return $output;
    }

    static function unescapeInvalidShortcodes($content ) {
        // Clean up entire string, avoids re-parsing HTML.
        $trans = array(
            '&#91;' => '[',
            '&#93;' => ']',
        );

        $content = strtr( $content, $trans );

        return $content;
    }

    static function shortcodeParseAtts($text ) {
        $atts    = array();
        $pattern = self::getShortcodeAttsRegex();
        $text    = preg_replace( "/[\x{00a0}\x{200b}]+/u", ' ', $text );
        if ( preg_match_all( $pattern, $text, $match, PREG_SET_ORDER ) ) {
            foreach ( $match as $m ) {
                if ( ! empty( $m[1] ) ) {
                    $atts[ strtolower( $m[1] ) ] = stripcslashes( $m[2] );
                } elseif ( ! empty( $m[3] ) ) {
                    $atts[ strtolower( $m[3] ) ] = stripcslashes( $m[4] );
                } elseif ( ! empty( $m[5] ) ) {
                    $atts[ strtolower( $m[5] ) ] = stripcslashes( $m[6] );
                } elseif ( isset( $m[7] ) && strlen( $m[7] ) ) {
                    $atts[] = stripcslashes( $m[7] );
                } elseif ( isset( $m[8] ) && strlen( $m[8] ) ) {
                    $atts[] = stripcslashes( $m[8] );
                } elseif ( isset( $m[9] ) ) {
                    $atts[] = stripcslashes( $m[9] );
                }
            }

            // Reject any unclosed HTML elements.
            foreach ( $atts as &$value ) {
                if ( false !== strpos( $value, '<' ) ) {
                    if ( 1 !== preg_match( '/^[^<]*+(?:<[^>]*+>[^<]*+)*+$/', $value ) ) {
                        $value = '';
                    }
                }
            }
        } else {
            $atts = ltrim( $text );
        }

        return $atts;
    }
}