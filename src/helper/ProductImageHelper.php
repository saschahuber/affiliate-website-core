<?php

namespace saschahuber\affiliatewebsitecore\helper;

use saschahuber\affiliatewebsitecore\service\ImageTemplateService;
use saschahuber\saastemplatecore\Database;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\ImageOptimizationHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\helper\WebPHelper;
use saschahuber\saastemplatecore\service\ImageService;

class ProductImageHelper
{
    const IMG_WIDTH = 200;

    const PRODUCT_IMAGE_EXTENSION = 'jpg';

    static function handleRequest()
    {
        global $CONFIG, $DB;
        $DB = new Database();

        $request_uri = ArrayHelper::getArrayValue($_SERVER, 'REQUEST_URI', null);
        $parsed_url = parse_url($request_uri);
        $path = ArrayHelper::getArrayValue($parsed_url, 'path', null);

        $PATH = explode('/', trim($path, '/'));
        $actual_path = $PATH;
        $actual_path[count($actual_path) - 1] = rawurldecode($actual_path[count($actual_path) - 1]);

        //Automatische WebP Generierung
        if (count($actual_path) > 2 && $actual_path[0] === "produkt-img") {
            if ($actual_path[1] === "webp" && !$CONFIG->disable_webp) {
                header('Content-Type: image/webp');
                echo self::getProductWebpImage($actual_path);
                exit;
            } else {
                header('Content-Type: image/' . self::PRODUCT_IMAGE_EXTENSION);
                echo self::getProductImage($actual_path[1])['data'];
                exit;
            }
        }
    }

    static function getProductImage($product_id)
    {
        global $CONFIG;

        $amz_img_dir = TMP_DIR . "/produkt-img";
        $img_file = $amz_img_dir . "/" . $product_id . "." . self::PRODUCT_IMAGE_EXTENSION;

        FileHelper::createDirIfNotExists($amz_img_dir);

        if (!file_exists($img_file) || (time() - filemtime($img_file)) >= AMAZON_IMG_MAX_AGE) {
            $product_manager = AffiliateInterfacesHelper::getProductManager();
            $product = $product_manager->getById($product_id, true);

            if (isset($product->meta) && isset($product->meta['product_primary_image'])) {
                $product_primary_image = $product->meta['product_primary_image'];
            } else {
                $image_service = new ImageService();
                $product_primary_image = $CONFIG->website_domain . $image_service->getDynamicImage(ImageTemplateService::PRODUCT_THUMBNAIL, $product->title);

            }

            //Convert to PNG if neccessary
            $extension = UrlHelper::getFileExtension($product_primary_image);
            $img_content = file_get_contents($product_primary_image);
            if ($extension !== self::PRODUCT_IMAGE_EXTENSION) {
                $tmp_img_file = str_replace('.' . self::PRODUCT_IMAGE_EXTENSION, '.' . $extension, $img_file);
                file_put_contents($tmp_img_file, $img_content);
                $create_function_name = "imagecreatefrom" . str_replace('jpg', 'jpeg', $extension);
                if (function_exists($create_function_name)) {
                    $img = call_user_func($create_function_name, $tmp_img_file);

                    if ($extension === "png") {
                        $width = imagesx($img);
                        $height = imagesy($img);
                        $output = imagecreatetruecolor($width, $height);
                        $white = imagecolorallocate($output, 255, 255, 255);
                        imagefilledrectangle($output, 0, 0, $width, $height, $white);
                        imagecopy($output, $img, 0, 0, 0, 0, $width, $height);
                        $img = $output;
                    }

                    call_user_func("image" . str_replace('jpg', 'jpeg', self::PRODUCT_IMAGE_EXTENSION), $img, $img_file);
                }
                unlink($tmp_img_file);
            } else {
                file_put_contents($img_file, $img_content);
            }

            #die(json_encode([$extension, $tmp_img_file, $img_file, $create_function_name]));

            #unlink($img_file);

            $img_utils = new ImgUtils();
            $img = $img_utils->loadImgFile($img_file);
            $img = $img_utils->getImgResizedToWidth($img, self::IMG_WIDTH);
            $img_utils->storeImgFile($img, $img_file);
        }

        $img_file = ImageOptimizationHelper::getOptimizedImage($img_file);

        if (isset($product)) {
            FileHelper::copyToFrontDir($img_file, 'produkt-img/' . $product_id . '/' . $product->permalink . '.' . self::PRODUCT_IMAGE_EXTENSION);
        }

        #die(json_encode([$img_file, 'produkt-img/'.$product_id.'/'.$product->permalink.'.jpg']));

        $data = file_get_contents($img_file);

        return ['data' => $data];
    }

    static function getProductWebpImage($actual_path)
    {
        $product_id = $actual_path[2];

        $image = self::getProductImage($product_id);

        $amz_img_dir = TMP_DIR . "/produkt-img";
        $img_file = $amz_img_dir . "/" . $product_id . "." . self::PRODUCT_IMAGE_EXTENSION;

        FileHelper::createDirIfNotExists($amz_img_dir);
        file_put_contents($img_file, $image);

        $cache_path = CACHE_DIR . '/webp/' . md5($img_file . '-' . WEBP_QUALITY) . '.webp';

        FileHelper::copyToFrontDir($cache_path, implode('/', $actual_path));

        if (!file_exists($cache_path) || (time() - filemtime($cache_path)) >= AMAZON_IMG_MAX_AGE) {
            return WebPHelper::convertToWebP($img_file, $cache_path, self::IMG_WIDTH);
        } else {
            return file_get_contents($cache_path);
        }
    }
}