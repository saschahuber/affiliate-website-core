<?php

namespace saschahuber\affiliatewebsitecore\helper;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\AuthHelper;

#[AllowDynamicProperties]
class FavouriteHelper
{
    static function isFavourite($type, $item_id)
    {
        global $DB;

        if (!AuthHelper::isLoggedIn()) {
            return false;
        }

        if (!isset(AuthHelper::getCurrentUser()->favourites)) {
            $favourites = array();
            $querystring = 'SELECT favourite_element_type, favourite_element_id
                FROM user__favourite where user_id = ' . intval(AuthHelper::getCurrentUserId());
            foreach ($DB->getAll($querystring) as $favourite) {
                if (!isset($favourites[$favourite->favourite_element_type])) {
                    $favourites[$favourite->favourite_element_type] = array();
                }
                $favourites[$favourite->favourite_element_type][] = $favourite->favourite_element_id;
            }
            AuthHelper::getCurrentUser()->favourites = $favourites;
        }

        return array_key_exists($type, AuthHelper::getCurrentUser()->favourites)
            && in_array($item_id, AuthHelper::getCurrentUser()->favourites[$type]);
    }
}