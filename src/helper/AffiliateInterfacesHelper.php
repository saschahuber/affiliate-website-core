<?php

namespace saschahuber\affiliatewebsitecore\helper;

use saschahuber\affiliatewebsitecore\manager\ProductManager;

class AffiliateInterfacesHelper {
    public static function getProductManagerClass()
    {
        global $PRODUCT_MANAGER_CLASS;

        if(!isset($PRODUCT_MANAGER_CLASS)){
            $PRODUCT_MANAGER_CLASS = ProductManager::class;
        }
        return $PRODUCT_MANAGER_CLASS;
    }

    public static function getProductManager()
    {
        $class = self::getProductManagerClass();
        return new $class();
    }

    public static function setProductManagerClass($class){
        global $PRODUCT_MANAGER_CLASS;

        $PRODUCT_MANAGER_CLASS = $class;
    }
}