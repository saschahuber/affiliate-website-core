<?php

namespace saschahuber\affiliatewebsitecore;

use saschahuber\saastemplatecore\helper\LogHelper;

class LocationSpinner{
    private $location;
    private $content;
    private $variables;

    public function __construct($location){
        srand($location->id);
        $this->location = $location;
        $this->content = file_get_contents(COMMONS_BASE . '/resources/content.html');
        $this->variables = json_decode(file_get_contents(COMMONS_BASE . '/resources/variables.json'), true);
        $this->variables['TITLE'] = [$location->title];
    }

    public function getSpinnedContent(){
        return $this->spinText(str_replace("%%TITLE%%", $this->location->title, $this->content));
    }

    private function spinText($content){
        return preg_replace_callback("/%%(\w+)%%/", array($this, 'replaceVar'), $content);
    }

    private function replaceVar($matches){
        $variable = $matches[1];
        if(array_key_exists($variable, $this->variables)){
            $possible_values = $this->variables[$variable];
            $random_index = array_rand($possible_values);
            //Zufälliges Element aus Array wählen
            return $this->spinText($possible_values[$random_index]);
        }

        LogHelper::error("HTTP", 400, "No replacement for spinning-variable found: $variable");
        return null;
    }
}