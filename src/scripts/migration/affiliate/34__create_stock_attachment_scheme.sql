CREATE TABLE stock_attachment (
                                  stock_attachment_id INT NOT NULL AUTO_INCREMENT,
                                  title VARCHAR(127) NOT NULL,
                                  alt_text TEXT DEFAULT NULL,
                                  file_path VARCHAR(512) NOT NULL,
                                  file_name VARCHAR(512) NOT NULL,
                                  source_url VARCHAR(512) NOT NULL,
                                  description VARCHAR(512) NOT NULL,
                                  provider_name VARCHAR(512) NOT NULL,
                                  copyright_info VARCHAR(512) NOT NULL,
                                  stock_attachment_time TIMESTAMP default current_timestamp(),
                                  PRIMARY KEY (stock_attachment_id)
);

ALTER TABLE attachment add column description VARCHAR(512) DEFAULT NULL;
ALTER TABLE attachment add column provider_name VARCHAR(512) DEFAULT NULL;
ALTER TABLE attachment add column copyright_info VARCHAR(512) DEFAULT NULL;