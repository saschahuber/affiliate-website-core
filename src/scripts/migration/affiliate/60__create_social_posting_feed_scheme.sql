CREATE TABLE social_posting_feed_item
(
    social_posting_feed_item_id int         NOT NULL AUTO_INCREMENT,
    target                       varchar(64) NOT NULL,
    title                        varchar(127)         DEFAULT NULL,
    description                  text                 DEFAULT NULL,
    link                         text                 DEFAULT NULL,
    image_url                    text                 DEFAULT NULL,
    linked_element_type          varchar(64)          DEFAULT NULL,
    linked_element_id            int                  DEFAULT NULL,
    posting_time                 timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (social_posting_feed_item_id)
);