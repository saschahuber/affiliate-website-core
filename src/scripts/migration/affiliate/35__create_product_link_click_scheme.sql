CREATE TABLE product__link_click (
                                product__link_click_id INT NOT NULL AUTO_INCREMENT,
                                product_id INT NOT NULL,
                                source_url VARCHAR(512) NOT NULL,
                                user_id INT DEFAULT NULL,
                                user_ip VARCHAR(64) DEFAULT NULL,
                                user_agent VARCHAR(512) DEFAULT NULL,
                                session_id VARCHAR(128) DEFAULT NULL,
                                external_url VARCHAR(512) NOT NULL,
                                store_type VARCHAR(128) NOT NULL,
                                time DATETIME DEFAULT current_timestamp(),
                                PRIMARY KEY (product__link_click_id),
                                FOREIGN KEY (product_id) REFERENCES product(product_id)
);