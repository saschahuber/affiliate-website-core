ALTER TABLE company add column address VARCHAR(512) NOT NULL;
ALTER TABLE company add column website VARCHAR(512) DEFAULT NULL;
ALTER TABLE company add column email VARCHAR(512) DEFAULT NULL;
ALTER TABLE company add column phone_number VARCHAR(512) DEFAULT NULL;
ALTER TABLE company add column google_place_id VARCHAR(512) DEFAULT NULL;
ALTER TABLE company add column opening_hours_info TEXT DEFAULT NULL;
ALTER TABLE company add column is_featured BOOL DEFAULT false;
ALTER TABLE company add column admin_user_id int DEFAULT NULL;

ALTER TABLE company__taxonomy add column icon varchar(64) DEFAULT null after title;

CREATE TABLE company__action_log (
                                     company__action_log_id INT NOT NULL AUTO_INCREMENT,
                                     company_id INT NOT NULL,
                                     action VARCHAR(64) NOT NULL,
                                     user_id INT DEFAULT NULL,
                                     user_ip VARCHAR(64) DEFAULT NULL,
                                     user_agent VARCHAR(512) DEFAULT NULL,
                                     session_id VARCHAR(128) DEFAULT NULL,
                                     time DATETIME DEFAULT current_timestamp(),
                                     PRIMARY KEY (company__action_log_id),
                                     FOREIGN KEY (company_id) REFERENCES company(company_id)
);

-- company
ALTER TABLE company add column dofollow bool DEFAULT false after rel;
ALTER TABLE company add column doindex bool DEFAULT false after rel;
UPDATE company set dofollow = true where rel LIKE "%, follow";
UPDATE company set doindex = true where rel LIKE "index, %";
ALTER TABLE company drop column rel;

-- company__taxonomy
ALTER TABLE company__taxonomy add column dofollow bool DEFAULT false after rel;
ALTER TABLE company__taxonomy add column doindex bool DEFAULT false after rel;
UPDATE company__taxonomy set dofollow = true where rel LIKE "%, follow";
UPDATE company__taxonomy set doindex = true where rel LIKE "index, %";
ALTER TABLE company__taxonomy drop column rel;

-- deal_page
ALTER TABLE deal_page add column dofollow bool DEFAULT false after rel;
ALTER TABLE deal_page add column doindex bool DEFAULT false after rel;
UPDATE deal_page set dofollow = true where rel LIKE "%, follow";
UPDATE deal_page set doindex = true where rel LIKE "index, %";
ALTER TABLE deal_page drop column rel;

-- location
ALTER TABLE location add column dofollow bool DEFAULT false after rel;
ALTER TABLE location add column doindex bool DEFAULT false after rel;
UPDATE location set dofollow = true where rel LIKE "%, follow";
UPDATE location set doindex = true where rel LIKE "index, %";
ALTER TABLE location drop column rel;

-- news
ALTER TABLE news add column dofollow bool DEFAULT false after rel;
ALTER TABLE news add column doindex bool DEFAULT false after rel;
UPDATE news set dofollow = true where rel LIKE "%, follow";
UPDATE news set doindex = true where rel LIKE "index, %";
ALTER TABLE news drop column rel;

-- news__taxonomy
ALTER TABLE news__taxonomy add column dofollow bool DEFAULT false after rel;
ALTER TABLE news__taxonomy add column doindex bool DEFAULT false after rel;
UPDATE news__taxonomy set dofollow = true where rel LIKE "%, follow";
UPDATE news__taxonomy set doindex = true where rel LIKE "index, %";
ALTER TABLE news__taxonomy drop column rel;

-- page
ALTER TABLE page add column dofollow bool DEFAULT false after rel;
ALTER TABLE page add column doindex bool DEFAULT false after rel;
UPDATE page set dofollow = true where rel LIKE "%, follow";
UPDATE page set doindex = true where rel LIKE "index, %";
ALTER TABLE page drop column rel;

-- post
ALTER TABLE post add column dofollow bool DEFAULT false after rel;
ALTER TABLE post add column doindex bool DEFAULT false after rel;
UPDATE post set dofollow = true where rel LIKE "%, follow";
UPDATE post set doindex = true where rel LIKE "index, %";
ALTER TABLE post drop column rel;

-- post__taxonomy
ALTER TABLE post__taxonomy add column dofollow bool DEFAULT false after rel;
ALTER TABLE post__taxonomy add column doindex bool DEFAULT false after rel;
UPDATE post__taxonomy set dofollow = true where rel LIKE "%, follow";
UPDATE post__taxonomy set doindex = true where rel LIKE "index, %";
ALTER TABLE post__taxonomy drop column rel;

-- product
ALTER TABLE product add column dofollow bool DEFAULT false after rel;
ALTER TABLE product add column doindex bool DEFAULT false after rel;
UPDATE product set dofollow = true where rel LIKE "%, follow";
UPDATE product set doindex = true where rel LIKE "index, %";
ALTER TABLE product drop column rel;

-- product__taxonomy
ALTER TABLE product__taxonomy add column dofollow bool DEFAULT false after rel;
ALTER TABLE product__taxonomy add column doindex bool DEFAULT false after rel;
UPDATE product__taxonomy set dofollow = true where rel LIKE "%, follow";
UPDATE product__taxonomy set doindex = true where rel LIKE "index, %";
ALTER TABLE product__taxonomy drop column rel;

-- brand
ALTER TABLE brand add column dofollow bool DEFAULT false after rel;
ALTER TABLE brand add column doindex bool DEFAULT false after rel;
UPDATE brand set dofollow = true where rel LIKE "%, follow";
UPDATE brand set doindex = true where rel LIKE "index, %";
ALTER TABLE brand drop column rel;

ALTER TABLE location drop column content;

CREATE TABLE company__opening_hours
(
    company__opening_hours_id INT NOT NULL AUTO_INCREMENT,
    company_id                INT NOT NULL,
    weekday                   enum('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday') NOT NULL,
    is_open                   bool DEFAULT false,
    open1                     VARCHAR(8) DEFAULT NULL,
    close1                    VARCHAR(8) DEFAULT NULL,
    open2                     VARCHAR(8) DEFAULT NULL,
    close2                    VARCHAR(8) DEFAULT NULL,
    PRIMARY KEY (company__opening_hours_id),
    UNIQUE (company_id, weekday),
    FOREIGN KEY (company_id) REFERENCES company (company_id)
);

ALTER TABLE location add column featured bool default false;

ALTER TABLE company add column show_opening_hours BOOL DEFAULT false;