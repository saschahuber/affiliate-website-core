ALTER TABLE post__taxonomy add column h1 varchar(127) after title;
ALTER TABLE post__taxonomy add column subtitle varchar(127) after h1;
ALTER TABLE post__taxonomy add column header_template varchar(127) after subtitle;
ALTER TABLE post__taxonomy add column header_content TEXT after header_template;
ALTER TABLE post__taxonomy add column header_image_attachment_id integer DEFAULT NULL after header_template,
                           ADD CONSTRAINT post_taxonomy_header_image_attachment_id
                            FOREIGN KEY (header_image_attachment_id) REFERENCES attachment(attachment_id);

ALTER TABLE news__taxonomy add column h1 varchar(127) after title;
ALTER TABLE news__taxonomy add column subtitle varchar(127) after h1;
ALTER TABLE news__taxonomy add column header_template varchar(127) after subtitle;
ALTER TABLE news__taxonomy add column header_content TEXT after header_template;
ALTER TABLE news__taxonomy add column header_image_attachment_id integer DEFAULT NULL after header_template,
                           ADD CONSTRAINT news_taxonomy_header_image_attachment_id
                            FOREIGN KEY (header_image_attachment_id) REFERENCES attachment(attachment_id);

ALTER TABLE brand add column h1 varchar(127) after title;
ALTER TABLE brand add column subtitle varchar(127) after h1;
ALTER TABLE brand add column header_template varchar(127) after subtitle;
ALTER TABLE brand add column header_content TEXT after header_template;
ALTER TABLE brand add column header_image_attachment_id integer DEFAULT NULL after header_template,
                           ADD CONSTRAINT brand_taxonomy_header_image_attachment_id
                            FOREIGN KEY (header_image_attachment_id) REFERENCES attachment(attachment_id);

ALTER TABLE page add column h1 varchar(127) after title;
ALTER TABLE page add column subtitle varchar(127) after h1;
ALTER TABLE page add column header_template varchar(127) after subtitle;
ALTER TABLE page add column header_content TEXT after header_template;
ALTER TABLE page add column header_image_attachment_id integer DEFAULT NULL after header_template,
                           ADD CONSTRAINT page_taxonomy_header_image_attachment_id
                            FOREIGN KEY (header_image_attachment_id) REFERENCES attachment(attachment_id);

ALTER TABLE post add column hide_in_search BOOL DEFAULT false;
ALTER TABLE post__taxonomy add column hide_in_search BOOL DEFAULT false;

ALTER TABLE news add column hide_in_search BOOL DEFAULT false;
ALTER TABLE news__taxonomy add column hide_in_search BOOL DEFAULT false;

ALTER TABLE product__taxonomy add column hide_in_search BOOL DEFAULT false;

ALTER TABLE brand add column hide_in_search BOOL DEFAULT false;

ALTER TABLE company add column hide_in_search BOOL DEFAULT false;
ALTER TABLE company__taxonomy add column hide_in_search BOOL DEFAULT false;