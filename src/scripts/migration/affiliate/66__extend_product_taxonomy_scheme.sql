CREATE TABLE icon (
                                  icon_id INT NOT NULL AUTO_INCREMENT,
                                  title VARCHAR(127) NOT NULL,
                                  alt_text TEXT DEFAULT NULL,
                                  file_path VARCHAR(512) NOT NULL,
                                  file_name VARCHAR(512) NOT NULL,
                                  source_url VARCHAR(512) NOT NULL,
                                  description VARCHAR(512) NOT NULL,
                                  provider_name VARCHAR(512) NOT NULL,
                                  copyright_info VARCHAR(512) NOT NULL,
                                  icon_time TIMESTAMP default current_timestamp(),
                                  PRIMARY KEY (icon_id)
);

ALTER TABLE product__taxonomy ADD COLUMN icon_id INT DEFAULT NULL,
    ADD FOREIGN KEY (icon_id) REFERENCES icon(icon_id);