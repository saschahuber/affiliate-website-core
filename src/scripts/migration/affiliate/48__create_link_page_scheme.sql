CREATE TABLE link_page_item
(
    link_page_item_id   INT         NOT NULL AUTO_INCREMENT,
    title               VARCHAR(255) NOT NULL,
    content             TEXT                 DEFAULT NULL,
    attachment_id       INT                  DEFAULT NULL,
    button_text         VARCHAR(255)         DEFAULT NULL,
    button_url          VARCHAR(1024)        DEFAULT NULL,
    status              VARCHAR(64) NOT NULL DEFAULT 'draft',
    link_page_item_date DATETIME             DEFAULT CURRENT_TIMESTAMP(),
    linked_item_type    VARCHAR(64)          DEFAULT NULL,
    linked_item_id      INT                  DEFAULT NULL,
    PRIMARY KEY (link_page_item_id)
);