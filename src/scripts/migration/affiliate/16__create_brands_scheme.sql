CREATE TABLE brand (
    brand_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') DEFAULT 'draft',
    brand_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    content_after_products TEXT DEFAULT NULL,
    short_content TEXT NOT NULL,
    attachment_id INT DEFAULT NULL,
    PRIMARY KEY (brand_id),
    UNIQUE (permalink),
    FOREIGN KEY (attachment_id) REFERENCES attachment(attachment_id)
);

CREATE TABLE brand__product_mapping(
    brand__product_mapping_id INT NOT NULL AUTO_INCREMENT,
    product_id INT NOT NULL,
    brand_id INT NOT NULL,
    PRIMARY KEY (brand__product_mapping_id),
    UNIQUE (product_id, brand_id),
    FOREIGN KEY (product_id) REFERENCES product(product_id),
    FOREIGN KEY (brand_id) REFERENCES brand(brand_id)
);

CREATE TABLE brand__meta(
    brand__meta_id INT NOT NULL AUTO_INCREMENT,
    meta_key VARCHAR(128) NOT NULL,
    meta_value TEXT DEFAULT NULL,
    brand_id INT NOT NULL,
    PRIMARY KEY (brand__meta_id),
    UNIQUE (brand_id, meta_key),
    FOREIGN KEY (brand_id) REFERENCES brand(brand_id)
);