CREATE TABLE ad (
    ad_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    type VARCHAR(15) DEFAULT 'sidebar',
    status ENUM('publish','draft','trash') DEFAULT 'draft',
    start_date datetime DEFAULT current_timestamp,
    end_date datetime DEFAULT NULL,
    PRIMARY KEY (ad_id),
    UNIQUE (title, type)
);

CREATE TABLE ad__taxonomy_mapping (
    ad__taxonomy_mapping_id INT NOT NULL AUTO_INCREMENT,
    taxonomy_type VARCHAR(31) NOT NULL,
    ad_id INT NOT NULL,
    taxonomy_id INT NOT NULL,
    positive_keywords TEXT DEFAULT NULL,
    negative_keywords TEXT DEFAULT NULL,
    PRIMARY KEY (ad__taxonomy_mapping_id),
    UNIQUE KEY (taxonomy_type, ad_id, taxonomy_id),
    FOREIGN KEY (ad_id) REFERENCES  ad(ad_id)
)