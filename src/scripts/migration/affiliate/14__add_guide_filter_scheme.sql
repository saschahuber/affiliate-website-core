CREATE TABLE guide_filter(
    guide_filter_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(256) DEFAULT NULL,
    result_type VARCHAR(512) DEFAULT NULL,
    pre_filter_category_ids VARCHAR(512) DEFAULT NULL,
    PRIMARY KEY(guide_filter_id)
);

CREATE TABLE guide_filter__step(
    guide_filter__step_id INT NOT NULL AUTO_INCREMENT,
    guide_filter_id INT NOT NULL,
    active BOOL DEFAULT true,
    title VARCHAR(256) DEFAULT NULL,
    description VARCHAR(256) DEFAULT NULL,
    filter_progress_text VARCHAR(256) DEFAULT NULL,
    multiple_selections BOOL default false,
    extend_results BOOL default false,
    skippable BOOL DEFAULT false,
    step_order INT DEFAULT 0,
    PRIMARY KEY(guide_filter__step_id),
    FOREIGN KEY (guide_filter_id) REFERENCES guide_filter(guide_filter_id)
);

CREATE TABLE guide_filter__step_option(
    guide_filter__step_option_id INT NOT NULL AUTO_INCREMENT,
    guide_filter__step_id INT NOT NULL,
    active BOOL DEFAULT true,
    img VARCHAR(256) DEFAULT NULL,
    icon VARCHAR(256) DEFAULT NULL,
    label VARCHAR(256) DEFAULT NULL,
    and_connections BOOL default false,
    option_order INT DEFAULT 0,
    PRIMARY KEY(guide_filter__step_option_id),
    FOREIGN KEY (guide_filter__step_id) REFERENCES guide_filter__step(guide_filter__step_id)
);

CREATE TABLE guide_filter__step_option_filter(
    guide_filter__step_option_filter_id INT NOT NULL AUTO_INCREMENT,
    guide_filter__step_option_id INT NOT NULL,
    filter_type VARCHAR(256) DEFAULT NULL,
    filter_key VARCHAR(256) DEFAULT NULL,
    filter_comparator VARCHAR(256) DEFAULT NULL,
    filter_value VARCHAR(256) DEFAULT NULL,
    PRIMARY KEY(guide_filter__step_option_filter_id),
    FOREIGN KEY (guide_filter__step_option_id) REFERENCES guide_filter__step_option(guide_filter__step_option_id)
);