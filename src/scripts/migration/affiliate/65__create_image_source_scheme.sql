CREATE TABLE imprint_copyright_item
(
    imprint_copyright_item_id INT          NOT NULL AUTO_INCREMENT,
    element_type              VARCHAR(32) NOT NULL,
    element_id                INT          NOT NULL,
    element_title             VARCHAR(512) NOT NULL,
    element_permalink         VARCHAR(512) NOT NULL,
    attachment_type           VARCHAR(32) NOT NULL,
    attachment_id             INT          NOT NULL,
    description               VARCHAR(512) DEFAULT NULL,
    provider_name             VARCHAR(512) DEFAULT NULL,
    copyright_info            VARCHAR(512) DEFAULT NULL,
    PRIMARY KEY (imprint_copyright_item_id),
    UNIQUE(element_type, element_id, attachment_type, attachment_id)
);