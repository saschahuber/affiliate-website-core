ALTER TABLE author add column permalink VARCHAR(128) NOT NULL after title;
ALTER TABLE author add column job_title VARCHAR(128) NOT NULL after permalink;
ALTER TABLE author add column organization VARCHAR(128) NOT NULL after job_title;

CREATE TABLE author__link
(
    author__link_id INT         NOT NULL AUTO_INCREMENT,
    author_id       INT DEFAULT NULL,
    label             VARCHAR(128) NOT NULL,
    url             VARCHAR(512) NOT NULL,
    PRIMARY KEY (author__link_id),
    UNIQUE(author_id, label),
    FOREIGN KEY (author_id) REFERENCES author (author_id)
);