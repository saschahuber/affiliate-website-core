CREATE TABLE content_schedule_item
(
    content_schedule_item_id   INT          NOT NULL AUTO_INCREMENT,
    title                      VARCHAR(512) NOT NULL,
    content_schedule_item_date DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    linked_element_type        varchar(64)           DEFAULT NULL,
    linked_element_id          int                   DEFAULT NULL,
    PRIMARY KEY (content_schedule_item_id),
    UNIQUE (title)
);