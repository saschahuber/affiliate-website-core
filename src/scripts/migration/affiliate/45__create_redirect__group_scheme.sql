CREATE TABLE redirect__group
(
    redirect__group_id INT         NOT NULL AUTO_INCREMENT,
    title              VARCHAR(64) NOT NULL,
    alias              VARCHAR(64) NOT NULL,
    is_active          BOOL DEFAULT false,
    hits               INT  DEFAULT 0,
    UNIQUE (title),
    UNIQUE (alias),
    PRIMARY KEY (redirect__group_id)
);

CREATE TABLE redirect__group_item
(
    redirect__group_item_id INT         NOT NULL AUTO_INCREMENT,
    redirect__group_id INT         NOT NULL,
    title              VARCHAR(64) NOT NULL,
    target_url              VARCHAR(512) NOT NULL,
    is_active          BOOL DEFAULT false,
    hits               INT  DEFAULT 0,
    UNIQUE (redirect__group_id, target_url),
    PRIMARY KEY (redirect__group_item_id),
    FOREIGN KEY (redirect__group_id) REFERENCES redirect__group(redirect__group_id)
);