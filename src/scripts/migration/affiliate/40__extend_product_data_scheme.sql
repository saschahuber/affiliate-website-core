ALTER TABLE product__data_field add column field_description VARCHAR(512) DEFAULT NULL after field_name;
ALTER TABLE product__data_field add column field_values VARCHAR(512) DEFAULT NULL after field_type;

ALTER TABLE product__data_field_group add column product__taxonomy_id integer DEFAULT NULL after group_name,
  ADD FOREIGN KEY (product__taxonomy_id) REFERENCES product__taxonomy(product__taxonomy_id);

UPDATE product__data_field_group g
    inner join product__data_field_group_mapping gm
        on g.product__data_field_group_id = gm.product__data_field_group_id
SET g.product__taxonomy_id = gm.product__taxonomy_id;

UPDATE product__data_field set field_type = "boolean" where field_type = "true_false";
UPDATE product__data_field set field_type = "enum" where field_type = "select";
UPDATE product__data_field set field_type = "float" where field_type = "number";

DROP table product__data_field_group_mapping;