CREATE TABLE user__profile
(
    user__profile_id int(11)      NOT NULL AUTO_INCREMENT,
    user_id          INT                   DEFAULT NULL,
    fingerprint      varchar(128) NOT NULL,
    session_id       varchar(128) DEFAULT NULL,
    ip_address       varchar(64) DEFAULT NULL,
    data_key         varchar(64) NOT NULL,
    data_value       longtext                  DEFAULT NULL,
    update_time      timestamp    NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (user__profile_id),
    UNIQUE(user_id, fingerprint, session_id, ip_address, data_key),
    FOREIGN KEY (user_id) REFERENCES user (user_id)
);