CREATE TABLE post (
    post_id INT NOT NULL AUTO_INCREMENT,
    post_id_old INT DEFAULT NULL,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') NOT NULL,
    post_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    attachment_id INT DEFAULT NULL,
    social_posting BOOLEAN DEFAULT false,
    social_posting_message TEXT DEFAULT NULL,
    social_posting_hashtags TEXT DEFAULT NULL,
    PRIMARY KEY (post_id),
    UNIQUE (permalink),
    FOREIGN KEY (attachment_id) REFERENCES attachment(attachment_id)
);

CREATE TABLE post__taxonomy (
    post__taxonomy_id INT NOT NULL AUTO_INCREMENT,
    post__taxonomy_id_old INT DEFAULT NULL,
    type VARCHAR(512) NOT NULL,
    taxonomy_parent_id INT DEFAULT NULL,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') NOT NULL,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    attachment_id INT DEFAULT NULL,
    PRIMARY KEY (post__taxonomy_id),
    UNIQUE (permalink),
    FOREIGN KEY (taxonomy_parent_id) REFERENCES post__taxonomy(post__taxonomy_id)
);

CREATE TABLE post__taxonomy_mapping(
    post__taxonomy_mapping_id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    taxonomy_id INT NOT NULL,
    is_primary_taxonomy BOOL DEFAULT false,
    PRIMARY KEY (post__taxonomy_mapping_id),
    UNIQUE (post_id, taxonomy_id),
    FOREIGN KEY (post_id) REFERENCES post(post_id),
    FOREIGN KEY (taxonomy_id) REFERENCES post__taxonomy(post__taxonomy_id)
);

CREATE TABLE post__meta(
    post__meta_id INT NOT NULL AUTO_INCREMENT,
    meta_key VARCHAR(128) NOT NULL,
    meta_value TEXT DEFAULT NULL,
    post_id INT NOT NULL,
    PRIMARY KEY (post__meta_id),
    UNIQUE (post_id, meta_key),
    FOREIGN KEY (post_id) REFERENCES post(post_id)
);