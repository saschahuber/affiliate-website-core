ALTER table ad add column active BOOLEAN default false after status;

UPDATE ad set active = true where status = 'publish';

ALTER TABLE ad DROP COLUMN status;