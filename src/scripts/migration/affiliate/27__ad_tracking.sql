CREATE TABLE ad__tracking(
    ad__tracking_id INT NOT NULL AUTO_INCREMENT,
    ad_id INT NOT NULL,
    source TEXT NOT NULL,
    user_ip VARCHAR(32) NOT NULL,
    session_id VARCHAR(32) NOT NULL,
    params TEXT DEFAULT NULL,
    datetime DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (ad__tracking_id),
    FOREIGN KEY (ad_id) REFERENCES ad(ad_id)
);