ALTER TABLE product add column attachment_id integer DEFAULT NULL after content,
                           ADD FOREIGN KEY (attachment_id) REFERENCES attachment(attachment_id);