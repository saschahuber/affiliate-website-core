ALTER TABLE page MODIFY content LONGTEXT;
ALTER TABLE news MODIFY content LONGTEXT;
ALTER TABLE news__taxonomy MODIFY content LONGTEXT;
ALTER TABLE post MODIFY content LONGTEXT;
ALTER TABLE post__taxonomy MODIFY content LONGTEXT;
ALTER TABLE product MODIFY content LONGTEXT;
ALTER TABLE product__taxonomy MODIFY content LONGTEXT;
ALTER TABLE brand MODIFY content LONGTEXT;
ALTER TABLE company MODIFY content LONGTEXT;
ALTER TABLE company__taxonomy MODIFY content LONGTEXT;
ALTER TABLE ad MODIFY content LONGTEXT;
ALTER TABLE location MODIFY content LONGTEXT;