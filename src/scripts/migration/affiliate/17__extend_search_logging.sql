ALTER TABLE search_log add column user_ip VARCHAR(64) DEFAULT NULL;
ALTER TABLE search_log add column user_agent VARCHAR(512) DEFAULT NULL;
ALTER TABLE search_log add column session_id VARCHAR(128) DEFAULT NULL;