CREATE TABLE search_index(
    search_index_id INT NOT NULL AUTO_INCREMENT,
    searchable_item_id INT NOT NULL,
    searchable_item_type VARCHAR(128) NOT NULL,
    thumbnail_url VARCHAR(255) DEFAULT NULL,
    permalink VARCHAR(512) NOT NULL,
    is_external bool default false,
    search_title TEXT NOT NULL,
    search_content_primary TEXT NOT NULL,
    search_content_secondary TEXT DEFAULT NULL,
    search_meta TEXT DEFAULT NULL,
    relevance_modifier DOUBLE DEFAULT 1.0,
    PRIMARY KEY (search_index_id),
    UNIQUE (searchable_item_id, searchable_item_type),
    FULLTEXT INDEX (search_title, search_content_primary, search_content_secondary, search_meta),
    FULLTEXT INDEX (search_title),
    FULLTEXT INDEX (search_content_primary, search_content_secondary),
    FULLTEXT INDEX (search_meta)
);

CREATE TABLE search_log(
    search_log_id INT NOT NULL AUTO_INCREMENT,
    keyword VARCHAR(1024) NOT NULL,
    search_time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    results INT DEFAULT 0,
    PRIMARY KEY (search_log_id)
);
