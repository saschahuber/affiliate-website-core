CREATE TABLE product__price_history(
    product__price_history_id INT NOT NULL AUTO_INCREMENT,
    product_id int NOT NULL,
    price DOUBLE DEFAULT NULL,
    reduced_price DOUBLE DEFAULT NULL,
    price_time timestamp DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (product__price_history_id),
    UNIQUE (product_id, price_time),
    FOREIGN KEY (product_id) REFERENCES product(product_id)
);