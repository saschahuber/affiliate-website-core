CREATE TABLE vg_wort_pixel(
    vg_wort_pixel_id INT NOT NULL AUTO_INCREMENT,
    vg_wort_pixel VARCHAR(64) NOT NULL,
    relative_page_url VARCHAR(512) NOT NULL,
    PRIMARY KEY(vg_wort_pixel_id),
    UNIQUE(vg_wort_pixel)
);