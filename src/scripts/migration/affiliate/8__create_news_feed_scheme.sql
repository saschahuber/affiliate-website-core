CREATE TABLE news__feed_import(
    news__feed_import_id INT NOT NULL AUTO_INCREMENT,
    template VARCHAR(31) NOT NULL,
    feed_url VARCHAR(127) NOT NULL,
    settings TEXT NOT NULL,
    active BOOL DEFAULT false,
    PRIMARY KEY (news__feed_import_id),
    UNIQUE(template, feed_url)
);