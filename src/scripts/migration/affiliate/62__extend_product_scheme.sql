ALTER TABLE product__link ADD COLUMN is_active BOOL DEFAULT true;
ALTER TABLE product__link ADD COLUMN sort_order INT DEFAULT null;

CREATE TABLE product__custom_data_field
(
    product__custom_data_field_id INT          NOT NULL AUTO_INCREMENT,
    product_id              INT DEFAULT NULL,
    name                    VARCHAR(255) NOT NULL,
    value                   VARCHAR(512) NOT NULL,
    sort_order              INT DEFAULT 0,
    PRIMARY KEY (product__custom_data_field_id),
    FOREIGN KEY (product_id) REFERENCES product (product_id)
);