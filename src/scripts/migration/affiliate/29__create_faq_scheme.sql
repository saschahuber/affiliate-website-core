CREATE TABLE faq(
    faq_id INT          NOT NULL AUTO_INCREMENT,
    title  VARCHAR(512) NOT NULL,
    active BOOL DEFAULT false,
    PRIMARY KEY (faq_id),
    UNIQUE (title)
);

CREATE TABLE faq__question(
    faq__question_id INT          NOT NULL AUTO_INCREMENT,
    faq_id           INT          NOT NULL,
    item_order            INT          NOT NULL DEFAULT 0,
    question         VARCHAR(512) NOT NULL,
    answer           TEXT                  DEFAULT NULL,
    PRIMARY KEY (faq__question_id),
    FOREIGN KEY (faq_id) REFERENCES faq (faq_id)
);

ALTER TABLE post ADD COLUMN attached_faq_id INT DEFAULT NULL,
    ADD FOREIGN KEY (attached_faq_id) REFERENCES faq(faq_id);

ALTER TABLE post__taxonomy ADD COLUMN attached_faq_id INT DEFAULT NULL,
    ADD FOREIGN KEY (attached_faq_id) REFERENCES faq(faq_id);

ALTER TABLE news ADD COLUMN attached_faq_id INT DEFAULT NULL,
    ADD FOREIGN KEY (attached_faq_id) REFERENCES faq(faq_id);

ALTER TABLE news__taxonomy ADD COLUMN attached_faq_id INT DEFAULT NULL,
    ADD FOREIGN KEY (attached_faq_id) REFERENCES faq(faq_id);

ALTER TABLE product ADD COLUMN attached_faq_id INT DEFAULT NULL,
    ADD FOREIGN KEY (attached_faq_id) REFERENCES faq(faq_id);

ALTER TABLE product__taxonomy ADD COLUMN attached_faq_id INT DEFAULT NULL,
    ADD FOREIGN KEY (attached_faq_id) REFERENCES faq(faq_id);

ALTER TABLE brand ADD COLUMN attached_faq_id INT DEFAULT NULL,
    ADD FOREIGN KEY (attached_faq_id) REFERENCES faq(faq_id);