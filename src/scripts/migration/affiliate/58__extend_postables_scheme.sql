ALTER TABLE post ADD COLUMN is_sponsored BOOL DEFAULT false;
ALTER TABLE news ADD COLUMN is_sponsored BOOL DEFAULT false;
ALTER TABLE product ADD COLUMN is_sponsored BOOL DEFAULT false;
ALTER TABLE page ADD COLUMN is_sponsored BOOL DEFAULT false;