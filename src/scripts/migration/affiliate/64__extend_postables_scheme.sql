ALTER TABLE post ADD COLUMN image_text VARCHAR(128) DEFAULT NULL after title;
ALTER TABLE post__taxonomy ADD COLUMN image_text VARCHAR(128) DEFAULT NULL after title;
ALTER TABLE news ADD COLUMN image_text VARCHAR(128) DEFAULT NULL after title;
ALTER TABLE news__taxonomy ADD COLUMN image_text VARCHAR(128) DEFAULT NULL after title;
ALTER TABLE product ADD COLUMN image_text VARCHAR(128) DEFAULT NULL after title;
ALTER TABLE product__taxonomy ADD COLUMN image_text VARCHAR(128) DEFAULT NULL after title;
ALTER TABLE brand ADD COLUMN image_text VARCHAR(128) DEFAULT NULL after title;

ALTER TABLE post ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;
ALTER TABLE post__taxonomy ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;
ALTER TABLE news ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;
ALTER TABLE news__taxonomy ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;
ALTER TABLE product ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;
ALTER TABLE product__taxonomy ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;
ALTER TABLE brand ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;

ALTER TABLE post ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;
ALTER TABLE post__taxonomy ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;
ALTER TABLE news ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;
ALTER TABLE news__taxonomy ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;
ALTER TABLE product ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;
ALTER TABLE product__taxonomy ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;
ALTER TABLE brand ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;

UPDATE post__taxonomy set dynamic_image_type = "attachment", dynamic_image_id = header_image_attachment_id where header_image_attachment_id is not null;
UPDATE news__taxonomy set dynamic_image_type = "attachment", dynamic_image_id = header_image_attachment_id where header_image_attachment_id is not null;
UPDATE product__taxonomy set dynamic_image_type = "attachment", dynamic_image_id = header_image_attachment_id where header_image_attachment_id is not null;

ALTER TABLE post__taxonomy DROP CONSTRAINT post_taxonomy_header_image_attachment_id;
ALTER TABLE news__taxonomy DROP CONSTRAINT news_taxonomy_header_image_attachment_id;
ALTER TABLE product__taxonomy DROP CONSTRAINT product_taxonomy_header_image_attachment_id;

ALTER TABLE post__taxonomy DROP column header_image_attachment_id;
ALTER TABLE news__taxonomy DROP column header_image_attachment_id;
ALTER TABLE product__taxonomy DROP column header_image_attachment_id;

ALTER TABLE post__taxonomy DROP column header_template;
ALTER TABLE post__taxonomy DROP column header_content;

ALTER TABLE news__taxonomy DROP column header_template;
ALTER TABLE news__taxonomy DROP column header_content;

ALTER TABLE brand DROP column header_template;
ALTER TABLE brand DROP column header_content;

ALTER TABLE page DROP column header_template;
ALTER TABLE page DROP column header_content;