CREATE TABLE mail
(
    mail_id  INT           NOT NULL AUTO_INCREMENT,
    recipient     VARCHAR(1024) NOT NULL,
    cc_recipients       VARCHAR(1024) NOT NULL,
    subject    VARCHAR(2048) NOT NULL,
    content MEDIUMTEXT NOT NULL,
    priority INT DEFAULT 0,
    earliest_send INT DEFAULT NULL,
    latest_send INT DEFAULT NULL,
    creation_time DATETIME DEFAULT CURRENT_TIMESTAMP(),
    send_time DATETIME DEFAULT NULL,
    send_info TEXT DEFAULT NULL,
    fail_count INT DEFAULT 0,
    PRIMARY KEY (mail_id)
);