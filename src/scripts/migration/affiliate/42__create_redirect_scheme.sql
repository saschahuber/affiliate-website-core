CREATE TABLE redirect
(
    redirect_id  INT           NOT NULL AUTO_INCREMENT,
    from_url     VARCHAR(512) NOT NULL,
    to_url       VARCHAR(512) NOT NULL,
    is_active    BOOL DEFAULT false,
    is_temporary BOOL DEFAULT false,
    UNIQUE (from_url),
    PRIMARY KEY (redirect_id)
);