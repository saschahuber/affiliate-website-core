CREATE TABLE news__feed_import_item
(
    news__feed_import_item_id INT           NOT NULL AUTO_INCREMENT,
    news__feed_import_id      INT           NOT NULL,
    title                     VARCHAR(255)  NOT NULL,
    link                      VARCHAR(1024) NOT NULL,
    guid                      VARCHAR(255) NOT NULL,
    date                      DATETIME      NOT NULL,
    category                  VARCHAR(128)  DEFAULT NULL,
    content                   LONGTEXT      NOT NULL,
    PRIMARY KEY (news__feed_import_item_id),
    FOREIGN KEY (news__feed_import_id) REFERENCES news__feed_import (news__feed_import_id),
    UNIQUE (guid)
);