CREATE TABLE product__shop
(
    product__shop_id INT          NOT NULL AUTO_INCREMENT,
    title            VARCHAR(127) NOT NULL,
    url_format       VARCHAR(255) DEFAULT NULL,
    button_text      VARCHAR(127) NOT NULL,
    logo_src         varchar(512) DEFAULT NULL,
    is_hidden        BOOL         DEFAULT true,
    sort_order       INT          DEFAULT 1,
    PRIMARY KEY (product__shop_id)
);

CREATE TABLE product__feed
(
    product__feed_id   INT          NOT NULL AUTO_INCREMENT,
    product__shop_id   INT          DEFAULT NULL,
    title              VARCHAR(127) NOT NULL,
    template           VARCHAR(127) DEFAULT NULL,
    is_active          bool         DEFAULT false,
    url                TEXT         NOT NULL,
    import_cron_rule   VARCHAR(16)  DEFAULT '0 * * * *',
    product__feed_time TIMESTAMP    default current_timestamp(),
    FOREIGN KEY (product__shop_id) REFERENCES product__shop (product__shop_id),
    PRIMARY KEY (product__feed_id)
);

CREATE TABLE product__feed_import_log
(
    product__feed_import_log_id INT  NOT NULL AUTO_INCREMENT,
    product__feed_id            INT  NOT NULL,
    import_log                  TEXT NOT NULL,
    product__feed_log_time      TIMESTAMP default current_timestamp(),
    FOREIGN KEY (product__feed_id) REFERENCES product__feed (product__feed_id),
    PRIMARY KEY (product__feed_import_log_id)
);

CREATE TABLE product__link
(
    product__link_id INT      NOT NULL AUTO_INCREMENT,
    product_id       INT      NOT NULL,
    product__shop_id INT               DEFAULT NULL,
    product__feed_id INT               DEFAULT NULL,
    price            DOUBLE            DEFAULT NULL,
    reduced_price    DOUBLE            DEFAULT NULL,
    external_id      VARCHAR(128)      DEFAULT NULL,
    url              VARCHAR(512)      DEFAULT NULL,
    button_text      VARCHAR(512)      DEFAULT NULL,
    last_update      DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
    is_available     BOOL              DEFAULT true,
    FOREIGN KEY (product_id) REFERENCES product (product_id),
    FOREIGN KEY (product__shop_id) REFERENCES product__shop (product__shop_id),
    FOREIGN KEY (product__feed_id) REFERENCES product__feed (product__feed_id),
    UNIQUE (product_id, product__shop_id, product__feed_id),
    PRIMARY KEY (product__link_id)
);

INSERT INTO product__shop (title, button_text, is_hidden)
VALUES ('Amazon', '<i class="fab fa-amazon" aria-hidden="true"></i>&nbsp;Preis prüfen</a>', false);

ALTER TABLE product__link_click
    ADD COLUMN product__shop_id INT DEFAULT NULL AFTER product_id,
  ADD FOREIGN KEY (product__shop_id) REFERENCES product__shop(product__shop_id),
    ADD COLUMN product__link_id INT DEFAULT NULL AFTER product_id,
  ADD FOREIGN KEY (product__link_id) REFERENCES product__link(product__link_id);

ALTER TABLE product
    ADD COLUMN ean VARCHAR(127) DEFAULT NULL after amazon_asin;

ALTER TABLE product__price_history
    ADD COLUMN product__shop_id INT DEFAULT NULL AFTER product_id,
  ADD FOREIGN KEY (product__shop_id) REFERENCES product__shop(product__shop_id);

UPDATE product, product__meta
SET product.ean = product__meta.meta_value
WHERE product.product_id = product__meta.product_id
  AND product__meta.meta_key = 'product_ean';

INSERT INTO product__link (product_id, product__shop_id, price, reduced_price, external_id, url, last_update,
                           is_available)
SELECT product_id,
       1,
       price,
       reduced_price,
       amazon_asin,
       CONCAT("https://www.amazon.de/dp/", amazon_asin, "?tag=smartblogg-21&linkCode=ogi&th=1&psc=1"),
       last_amazon_update,
       is_available
FROM product;

ALTER TABLE product drop column amazon_asin;
ALTER TABLE product drop column price;
ALTER TABLE product drop column reduced_price;
ALTER TABLE product drop column is_available;
ALTER TABLE product drop column last_amazon_update;

INSERT INTO cronjob (title, cron_rule, script_file, script_params, active) VALUES
    ("Produkt Feed Update", "0 * * * *", "feed_updater.php", null, 0);