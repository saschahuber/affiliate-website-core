CREATE TABLE social_posting__filler_post (
    social_posting__filler_post_id INT NOT NULL AUTO_INCREMENT,
    post_message TEXT NOT NULL,
    post_hashtags TEXT DEFAULT NULL,
    post_link TEXT DEFAULT NULL,
    post_image_text TEXT DEFAULT NULL,
    post_image_template TEXT DEFAULT NULL,
    PRIMARY KEY (social_posting__filler_post_id)
);