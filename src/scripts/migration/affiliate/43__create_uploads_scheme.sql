CREATE TABLE upload
(
    upload_id      int(11)      NOT NULL AUTO_INCREMENT,
    title          varchar(127) NOT NULL,
    alt_text       text                  DEFAULT NULL,
    file_path      varchar(512) NOT NULL,
    file_name      varchar(512) NOT NULL,
    file_extension varchar(8)   NOT NULL,
    upload_time    timestamp    NOT NULL DEFAULT current_timestamp(),
    description    varchar(512)          DEFAULT NULL,
    provider_name  varchar(512)          DEFAULT NULL,
    copyright_info varchar(512)          DEFAULT NULL,
    PRIMARY KEY (upload_id)
)