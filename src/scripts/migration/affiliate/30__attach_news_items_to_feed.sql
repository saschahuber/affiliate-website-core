ALTER TABLE news modify column external_identifier VARCHAR(255) DEFAULT NULL;

ALTER TABLE news ADD COLUMN imported_feed_id INT DEFAULT NULL,
    ADD FOREIGN KEY (imported_feed_id) REFERENCES news__feed_import(news__feed_import_id);