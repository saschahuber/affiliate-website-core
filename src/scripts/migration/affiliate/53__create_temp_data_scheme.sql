CREATE TABLE temp_data
(
    temp_data_id int(11)      NOT NULL AUTO_INCREMENT,
    token        varchar(255) NOT NULL,
    temp_data    text                  DEFAULT NULL,
    expire_time  timestamp    NOT NULL DEFAULT current_timestamp(),
    PRIMARY KEY (temp_data_id)
)