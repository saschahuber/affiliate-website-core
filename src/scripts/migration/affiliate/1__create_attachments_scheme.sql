CREATE TABLE attachment (
    attachment_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(127) NOT NULL,
    alt_text TEXT DEFAULT NULL,
    file_path VARCHAR(512) NOT NULL,
    file_name VARCHAR(512) NOT NULL,
    old_attachment_url VARCHAR(127) DEFAULT NULL,
    old_attachment_id INT DEFAULT NULL,
    generator_data MEDIUMTEXT DEFAULT NULL,
    use_in_attachment_generator BOOLEAN DEFAULT false,
    PRIMARY KEY (attachment_id),
    UNIQUE(old_attachment_url),
    UNIQUE(old_attachment_id)
);
