CREATE TABLE product (
    product_id INT NOT NULL AUTO_INCREMENT,
    product_id_old INT DEFAULT NULL,
    title VARCHAR(512) NOT NULL,
    status ENUM('to_import','publish','draft','trash') DEFAULT 'to_import',
    product_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    amazon_asin VARCHAR(128) DEFAULT NULL,
    last_amazon_update DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00',
    price DOUBLE DEFAULT NULL,
    reduced_price DOUBLE DEFAULT NULL,
    is_fake BOOL DEFAULT true,
    is_prime  BOOL DEFAULT true,
    is_available  BOOL DEFAULT true,
    amazon_sales_rank INT DEFAULT null,
    hide_in_search BOOL DEFAULT false,
    social_posting BOOLEAN DEFAULT false,
    social_posting_message TEXT DEFAULT NULL,
    social_posting_hashtags TEXT DEFAULT NULL,
    PRIMARY KEY (product_id),
    UNIQUE (permalink),
    INDEX(product_id, amazon_asin)
);

CREATE TABLE product__taxonomy (
    product__taxonomy_id INT NOT NULL AUTO_INCREMENT,
    product__taxonomy_id_old INT DEFAULT NULL,
    taxonomy_parent_id INT DEFAULT NULL,
    type VARCHAR(512) NOT NULL,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') NOT NULL,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT DEFAULT NULL,
    content_after_products TEXT DEFAULT NULL,
    attachment_id INT DEFAULT NULL,
    PRIMARY KEY (product__taxonomy_id),
    UNIQUE (permalink),
    FOREIGN KEY (taxonomy_parent_id) REFERENCES product__taxonomy(product__taxonomy_id),
    FOREIGN KEY (attachment_id) REFERENCES attachment(attachment_id)
);

CREATE TABLE product__review (
    product__review_id INT NOT NULL AUTO_INCREMENT,
    product_id INT DEFAULT NULL,
    reviewer_name VARCHAR(63) DEFAULT NULL,
    review_summary VARCHAR(127) DEFAULT NULL,
    review_value FLOAT DEFAULT NULL,
    PRIMARY KEY (product__review_id),
    UNIQUE (product_id, reviewer_name),
    FOREIGN KEY (product_id) REFERENCES product(product_id)
);

CREATE TABLE product__taxonomy_mapping(
    product__taxonomy_mapping_id INT NOT NULL AUTO_INCREMENT,
    product_id INT NOT NULL,
    taxonomy_id INT NOT NULL,
    is_primary_taxonomy BOOL DEFAULT false,
    PRIMARY KEY (product__taxonomy_mapping_id),
    UNIQUE (product_id, taxonomy_id),
    FOREIGN KEY (product_id) REFERENCES product(product_id),
    FOREIGN KEY (taxonomy_id) REFERENCES product__taxonomy(product__taxonomy_id)
);

CREATE TABLE product__meta(
    product__meta_id INT NOT NULL AUTO_INCREMENT,
    meta_key VARCHAR(128) NOT NULL,
    meta_value TEXT DEFAULT NULL,
    product_id INT NOT NULL,
    PRIMARY KEY (product__meta_id),
    UNIQUE (product_id, meta_key),
    FOREIGN KEY (product_id) REFERENCES product(product_id)
);

CREATE TABLE product__data_field_group(
    product__data_field_group_id INT NOT NULL AUTO_INCREMENT,
    group_name VARCHAR(128) NOT NULL,
    PRIMARY KEY (product__data_field_group_id),
    UNIQUE(group_name)
);

CREATE TABLE product__data_field_group_mapping(
    product__data_field_group_mapping_id INT NOT NULL AUTO_INCREMENT,
    product__data_field_group_id INT NOT NULL,
    product__taxonomy_id INT NOT NULL,
    PRIMARY KEY (product__data_field_group_mapping_id),
    UNIQUE (product__data_field_group_id, product__taxonomy_id),
    FOREIGN KEY (product__data_field_group_id) REFERENCES product__data_field_group(product__data_field_group_id),
    FOREIGN KEY (product__taxonomy_id) REFERENCES product__taxonomy(product__taxonomy_id)
);

CREATE TABLE product__data_field(
    product__data_field_id INT NOT NULL AUTO_INCREMENT,
    field_order INT DEFAULT 0,
    field_name VARCHAR(128) NOT NULL,
    field_key VARCHAR(128) NOT NULL,
    field_type VARCHAR(128) NOT NULL,
    field_prefix VARCHAR(128) DEFAULT NULL,
    field_suffix VARCHAR(128) DEFAULT NULL,
    group_id INT NOT NULL,
    PRIMARY KEY (product__data_field_id),
    UNIQUE(field_name, group_id),
    UNIQUE(field_key, group_id),
    FOREIGN KEY (group_id) REFERENCES product__data_field_group(product__data_field_group_id)
);

CREATE TABLE product__data(
    product__data_id INT NOT NULL AUTO_INCREMENT,
    product_id INT NOT NULL,
    product__data_field_id INT NOT NULL,
    value VARCHAR(255) NOT NULL,
    PRIMARY KEY (product__data_id),
    UNIQUE (product_id, product__data_field_id),
    FOREIGN KEY (product_id) REFERENCES product(product_id),
    FOREIGN KEY (product__data_field_id) REFERENCES product__data_field(product__data_field_id)
);