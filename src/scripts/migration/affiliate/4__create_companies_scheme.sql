CREATE TABLE company (
    company_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') NOT NULL,
    company_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    attachment_id INT DEFAULT NULL,
    PRIMARY KEY (company_id),
    UNIQUE (permalink),
    FOREIGN KEY (attachment_id) REFERENCES attachment(attachment_id)
);

CREATE TABLE company__taxonomy (
    company__taxonomy_id INT NOT NULL AUTO_INCREMENT,
    type VARCHAR(512) NOT NULL,
    taxonomy_parent_id INT DEFAULT NULL,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') NOT NULL,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    attachment_id INT DEFAULT NULL,
    PRIMARY KEY (company__taxonomy_id),
    UNIQUE (permalink),
    FOREIGN KEY (taxonomy_parent_id) REFERENCES company__taxonomy(company__taxonomy_id)
);

CREATE TABLE company__taxonomy_mapping(
    company__taxonomy_mapping_id INT NOT NULL AUTO_INCREMENT,
    company_id INT NOT NULL,
    taxonomy_id INT NOT NULL,
    is_primary_taxonomy BOOL DEFAULT false,
    PRIMARY KEY (company__taxonomy_mapping_id),
    UNIQUE (company_id, taxonomy_id),
    FOREIGN KEY (company_id) REFERENCES company(company_id),
    FOREIGN KEY (taxonomy_id) REFERENCES company__taxonomy(company__taxonomy_id)
);

CREATE TABLE company__meta(
    company__meta_id INT NOT NULL AUTO_INCREMENT,
    meta_key VARCHAR(128) NOT NULL,
    meta_value TEXT DEFAULT NULL,
    company_id INT NOT NULL,
    PRIMARY KEY (company__meta_id),
    UNIQUE (company_id, meta_key),
    FOREIGN KEY (company_id) REFERENCES company(company_id)
);