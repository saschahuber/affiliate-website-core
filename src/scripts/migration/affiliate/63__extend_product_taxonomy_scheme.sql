ALTER TABLE product__taxonomy
    ADD COLUMN is_virtual BOOL DEFAULT FALSE;

CREATE TABLE product__taxonomy_virtual_mapping_rule
(
    product__taxonomy_virtual_mapping_rule_id INT          NOT NULL AUTO_INCREMENT,
    product__taxonomy_id                      INT DEFAULT NULL,
    product__data_field_id                    INT DEFAULT NULL,
    field_type                                VARCHAR(255) NOT NULL,
    first_field_value                         VARCHAR(255) NOT NULL,
    second_field_value                        VARCHAR(255) NOT NULL,
    PRIMARY KEY (product__taxonomy_virtual_mapping_rule_id),
    UNIQUE (product__taxonomy_id, product__data_field_id),
    FOREIGN KEY (product__taxonomy_id) REFERENCES product__taxonomy (product__taxonomy_id),
    FOREIGN KEY (product__data_field_id) REFERENCES product__data_field (product__data_field_id)
);

CREATE TABLE product__taxonomy_virtual_mapping
(
    product__taxonomy_virtual_mapping_id INT NOT NULL AUTO_INCREMENT,
    product_id                           INT NOT NULL,
    taxonomy_id                          INT NOT NULL,
    is_primary_taxonomy                  BOOL DEFAULT false,
    PRIMARY KEY (product__taxonomy_virtual_mapping_id),
    UNIQUE (product_id, taxonomy_id),
    FOREIGN KEY (product_id) REFERENCES product (product_id),
    FOREIGN KEY (taxonomy_id) REFERENCES product__taxonomy (product__taxonomy_id)
);