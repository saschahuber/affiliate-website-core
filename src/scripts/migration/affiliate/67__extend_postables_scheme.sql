ALTER TABLE company ADD COLUMN image_text VARCHAR(128) DEFAULT NULL after title;

ALTER TABLE company ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;

ALTER TABLE company ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;