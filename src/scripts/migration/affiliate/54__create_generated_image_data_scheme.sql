CREATE TABLE ai_attachment
(
    ai_attachment_id            int          NOT NULL AUTO_INCREMENT,
    title                       varchar(127)          DEFAULT NULL,
    prompt                      text                  DEFAULT NULL,
    revised_prompt              text                  DEFAULT NULL,
    alt_text                    text                  DEFAULT NULL,
    file_path                   varchar(512) NOT NULL,
    file_name                   varchar(512) NOT NULL,
    ai_attachment_time          timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    description                 varchar(512)          DEFAULT NULL,
    attachment_id               INT                   DEFAULT NULL,
    use_in_attachment_generator tinyint(1)            DEFAULT '0',
    PRIMARY KEY (ai_attachment_id),
    FOREIGN KEY (attachment_id) REFERENCES attachment (attachment_id)
);