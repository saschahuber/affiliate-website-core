ALTER TABLE post add column hide_in_newsletter BOOL DEFAULT false;
ALTER TABLE product add column hide_in_newsletter BOOL DEFAULT false;
ALTER TABLE news add column hide_in_newsletter BOOL DEFAULT false;