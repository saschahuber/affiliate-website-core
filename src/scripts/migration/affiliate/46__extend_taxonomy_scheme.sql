ALTER TABLE product__taxonomy add column h1 varchar(127) after title;
ALTER TABLE product__taxonomy add column subtitle varchar(127) after h1;
ALTER TABLE product__taxonomy add column header_template varchar(127) after subtitle;
ALTER TABLE product__taxonomy add column header_content TEXT after header_template;

ALTER TABLE product__taxonomy add column header_image_attachment_id integer DEFAULT NULL after header_template,
  ADD CONSTRAINT product_taxonomy_header_image_attachment_id
                            FOREIGN KEY (header_image_attachment_id) REFERENCES attachment(attachment_id);