ALTER TABLE ad__tracking add column fingerprint VARCHAR(128) DEFAULT NULL AFTER user_ip;

ALTER TABLE ad drop column async;
ALTER TABLE ad drop column height;