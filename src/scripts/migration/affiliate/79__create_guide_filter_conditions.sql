CREATE TABLE guide_filter__step_condition
(
    guide_filter__step_condition_id INT NOT NULL AUTO_INCREMENT,
    guide_filter__step_id           INT NOT NULL,
    condition_step_id               INT NOT NULL,
    condition_option_id             INT NOT NULL,
    PRIMARY KEY (guide_filter__step_condition_id),
    FOREIGN KEY (guide_filter__step_id) REFERENCES guide_filter__step (guide_filter__step_id),
    FOREIGN KEY (condition_step_id) REFERENCES guide_filter__step (guide_filter__step_id),
    FOREIGN KEY (condition_option_id) REFERENCES guide_filter__step_option (guide_filter__step_option_id)
);