CREATE TABLE toast
(
    toast_id            int          NOT NULL,
    title               varchar(512) NOT NULL,
    content             text         NOT NULL,
    small_info          varchar(32)  NOT NULL,
    hide_delay          int          DEFAULT 10000,
    is_active           boolean      DEFAULT false,
    active_start        datetime     DEFAULT NULL,
    active_end          datetime     DEFAULT NULL,
    click_url           varchar(512) DEFAULT NULL,
    linked_element_type varchar(64)  DEFAULT NULL,
    linked_element_id   int          DEFAULT NULL
);


CREATE TABLE toast__condition
(
    toast__condition_id int         NOT NULL,
    toast_id            int         NOT NULL,
    condition_type      varchar(32) NOT NULL,
    condition_data      longtext
);