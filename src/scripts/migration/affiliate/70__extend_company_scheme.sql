ALTER TABLE company add column package VARCHAR(32) DEFAULT 'PACKAGE_FREE';

ALTER TABLE company__taxonomy ADD COLUMN dynamic_image_type VARCHAR(128) DEFAULT NULL after attachment_id;
ALTER TABLE company__taxonomy ADD COLUMN dynamic_image_id INT DEFAULT NULL after dynamic_image_type;