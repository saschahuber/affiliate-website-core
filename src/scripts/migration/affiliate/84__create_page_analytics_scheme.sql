CREATE TABLE crawl_url
(
    crawl_url_id INT          NOT NULL AUTO_INCREMENT,
    url          VARCHAR(512) NOT NULL,
    url_type     VARCHAR(64)  NOT NULL,
    is_scheduled BOOL DEFAULT FALSE,
    PRIMARY KEY (crawl_url_id),
    UNIQUE (url)
);

CREATE TABLE crawl_url__result
(
    crawl_url__result_id INT NOT NULL AUTO_INCREMENT,
    crawl_url_id         INT NOT NULL,
    http_status          INT NOT NULL,
    redirects_to_url_id  INT       DEFAULT NULL,
    crawl_time           TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (crawl_url__result_id),
    FOREIGN KEY (crawl_url_id) REFERENCES crawl_url (crawl_url_id),
    FOREIGN KEY (redirects_to_url_id) REFERENCES crawl_url (crawl_url_id)
);

CREATE TABLE crawl_url__link
(
    crawl_url_link_id INT NOT NULL AUTO_INCREMENT,
    start_url_id      INT NOT NULL,
    end_url_id        INT NOT NULL,
    PRIMARY KEY (crawl_url_link_id),
    UNIQUE (start_url_id, end_url_id),
    FOREIGN KEY (start_url_id) REFERENCES crawl_url (crawl_url_id),
    FOREIGN KEY (end_url_id) REFERENCES crawl_url (crawl_url_id)
);

CREATE TABLE crawl_url__analysis_result
(
    crawl_url__analysis_result_id INT         NOT NULL AUTO_INCREMENT,
    crawl_url__result_id          INT         NOT NULL,
    analysis_type                 VARCHAR(64) NOT NULL,
    analysis_data                 LONGTEXT    NOT NULL,
    analysis_date                 DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (crawl_url__analysis_result_id),
    UNIQUE (crawl_url__result_id, analysis_type),
    FOREIGN KEY (crawl_url__result_id) REFERENCES crawl_url__result (crawl_url__result_id)
);