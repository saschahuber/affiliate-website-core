UPDATE product__taxonomy SET content = CONCAT(content,  "<br><br>",  content_after_products) where content_after_products != "";
UPDATE brand SET content = CONCAT(content,  "<br><br>",  content_after_products) where content_after_products != "";

ALTER TABLE product__taxonomy drop column content_after_products;
ALTER TABLE brand drop column content_after_products;

ALTER TABLE product__taxonomy add column all_products_headline TEXT DEFAULT NULL AFTER content;
ALTER TABLE brand add column all_products_headline TEXT DEFAULT NULL AFTER content;
