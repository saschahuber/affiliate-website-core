CREATE TABLE product_comparison
(
    product_comparison_id   int                                                         NOT NULL AUTO_INCREMENT,
    title                   varchar(512) COLLATE utf8mb4_general_ci                     NOT NULL,
    status                  enum ('publish','draft','trash') COLLATE utf8mb4_general_ci NOT NULL,
    product_comparison_date datetime                                                    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    meta_title              varchar(255) COLLATE utf8mb4_general_ci                              DEFAULT NULL,
    meta_description        varchar(255) COLLATE utf8mb4_general_ci                              DEFAULT NULL,
    doindex                 tinyint(1)                                                           DEFAULT '0',
    dofollow                tinyint(1)                                                           DEFAULT '0',
    og_title                varchar(512) COLLATE utf8mb4_general_ci                              DEFAULT NULL,
    og_description          varchar(1024) COLLATE utf8mb4_general_ci                             DEFAULT NULL,
    og_image                varchar(512) COLLATE utf8mb4_general_ci                              DEFAULT NULL,
    permalink               varchar(127) COLLATE utf8mb4_general_ci                     NOT NULL,
    content                 longtext COLLATE utf8mb4_general_ci,
    attachment_id           int                                                                  DEFAULT NULL,
    social_posting          tinyint(1)                                                           DEFAULT '0',
    social_posting_message  text COLLATE utf8mb4_general_ci,
    social_posting_hashtags text COLLATE utf8mb4_general_ci,
    attached_faq_id         int                                                                  DEFAULT NULL,
    author_id               int                                                                  DEFAULT NULL,
    hide_in_search          tinyint(1)                                                           DEFAULT '0',
    is_sponsored            tinyint(1)                                                           DEFAULT '0',
    product_title_1         varchar(512) COLLATE utf8mb4_general_ci                              DEFAULT NULL,
    product_id_1            int COLLATE utf8mb4_general_ci                                       DEFAULT NULL,
    product_title_2         varchar(512) COLLATE utf8mb4_general_ci                              DEFAULT NULL,
    product_id_2            int COLLATE utf8mb4_general_ci                                       DEFAULT NULL,
    product_title_3         varchar(512) COLLATE utf8mb4_general_ci                              DEFAULT NULL,
    product_id_3            int COLLATE utf8mb4_general_ci                                       DEFAULT NULL,
    PRIMARY KEY (product_comparison_id),
    FOREIGN KEY (product_id_1) REFERENCES product (product_id),
    FOREIGN KEY (product_id_2) REFERENCES product (product_id),
    FOREIGN KEY (product_id_3) REFERENCES product (product_id)
);

CREATE TABLE product_comparison__meta
(
    product_comparison__meta_id int                                     NOT NULL AUTO_INCREMENT,
    meta_key                    varchar(128) COLLATE utf8mb4_general_ci NOT NULL,
    meta_value                  text COLLATE utf8mb4_general_ci,
    product_comparison_id       int                                     NOT NULL,
    PRIMARY KEY (product_comparison__meta_id),
    UNIQUE (product_comparison__meta_id, meta_key),
    FOREIGN KEY (product_comparison_id) REFERENCES product_comparison (product_comparison_id)
);