ALTER TABLE push_notification ADD COLUMN linked_element_type VARCHAR(64) DEFAULT NULL AFTER additional_data;
ALTER TABLE push_notification ADD COLUMN linked_element_id int DEFAULT NULL AFTER linked_element_type;

UPDATE search_index set searchable_item_type = 'post__category' where searchable_item_type = 'post-kategorie';
UPDATE search_index set searchable_item_type = 'product__category' where searchable_item_type = 'produkt-kategorie';
UPDATE search_index set searchable_item_type = 'news__category' where searchable_item_type = 'news-kategorie';
UPDATE search_index set searchable_item_type = 'brand' where searchable_item_type = 'hersteller';
UPDATE search_index set searchable_item_type = 'company' where searchable_item_type = 'dienstleister';
UPDATE search_index set searchable_item_type = 'company_category' where searchable_item_type = 'dienstleister-kategorie';
UPDATE search_index set searchable_item_type = 'product' where searchable_item_type = 'produkt';