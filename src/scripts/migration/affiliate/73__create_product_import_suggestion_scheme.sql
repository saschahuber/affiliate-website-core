CREATE TABLE product__import_suggestion
(
    product__import_suggestion_id INT          NOT NULL AUTO_INCREMENT,
    product_id                    INT          NOT NULL,
    title                         VARCHAR(255) NOT NULL,
    thumbnail                     VARCHAR(255) NOT NULL,
    feed_id                       INT,
    external_product_id           VARCHAR(127) NOT NULL,
    suggestion_time           DATETIME DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (product__import_suggestion_id),
    UNIQUE (product_id, feed_id, external_product_id),
    FOREIGN KEY (product_id) REFERENCES product (product_id),
    FOREIGN KEY (feed_id) REFERENCES product__feed (product__feed_id)
);