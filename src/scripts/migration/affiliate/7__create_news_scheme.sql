CREATE TABLE news (
    news_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') NOT NULL,
    news_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    attachment_id INT DEFAULT NULL,
    social_posting BOOLEAN DEFAULT false,
    social_posting_message TEXT DEFAULT NULL,
    social_posting_hashtags TEXT DEFAULT NULL,
    external_identifier VARCHAR(127) DEFAULT NULL,
    PRIMARY KEY (news_id),
    UNIQUE (permalink),
    UNIQUE (external_identifier),
    FOREIGN KEY (attachment_id) REFERENCES attachment(attachment_id)
);

CREATE TABLE news__taxonomy (
    news__taxonomy_id INT NOT NULL AUTO_INCREMENT,
    type VARCHAR(512) NOT NULL,
    taxonomy_parent_id INT DEFAULT NULL,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') NOT NULL,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    attachment_id INT DEFAULT NULL,
    PRIMARY KEY (news__taxonomy_id),
    UNIQUE (permalink),
    FOREIGN KEY (taxonomy_parent_id) REFERENCES news__taxonomy(news__taxonomy_id)
);

CREATE TABLE news__taxonomy_mapping(
    news__taxonomy_mapping_id INT NOT NULL AUTO_INCREMENT,
    news_id INT NOT NULL,
    taxonomy_id INT NOT NULL,
    is_primary_taxonomy BOOL DEFAULT false,
    PRIMARY KEY (news__taxonomy_mapping_id),
    UNIQUE (news_id, taxonomy_id),
    FOREIGN KEY (news_id) REFERENCES news(news_id),
    FOREIGN KEY (taxonomy_id) REFERENCES news__taxonomy(news__taxonomy_id)
);

CREATE TABLE news__meta(
    news__meta_id INT NOT NULL AUTO_INCREMENT,
    meta_key VARCHAR(128) NOT NULL,
    meta_value TEXT DEFAULT NULL,
    news_id INT NOT NULL,
    PRIMARY KEY (news__meta_id),
    UNIQUE (news_id, meta_key),
    FOREIGN KEY (news_id) REFERENCES news(news_id)
);