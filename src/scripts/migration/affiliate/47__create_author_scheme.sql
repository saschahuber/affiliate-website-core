CREATE TABLE author
(
    author_id     INT         NOT NULL AUTO_INCREMENT,
    title         VARCHAR(64) NOT NULL,
    description   TEXT        NOT NULL,
    attachment_id INT DEFAULT NULL,
    PRIMARY KEY (author_id),
    FOREIGN KEY (attachment_id) REFERENCES attachment (attachment_id)
);

ALTER TABLE post
    add column author_id integer DEFAULT NULL,
    ADD FOREIGN KEY (author_id) REFERENCES author (author_id);

ALTER TABLE news
    add column author_id integer DEFAULT NULL,
    ADD FOREIGN KEY (author_id) REFERENCES author (author_id);

ALTER TABLE product
    add column author_id integer DEFAULT NULL,
    ADD FOREIGN KEY (author_id) REFERENCES author (author_id);