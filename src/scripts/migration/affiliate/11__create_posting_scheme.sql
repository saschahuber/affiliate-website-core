CREATE TABLE social_posting (
    social_posting_id INT NOT NULL AUTO_INCREMENT,
    app_name VARCHAR(127) NOT NULL,
    post_data TEXT NOT NULL,
    post_url VARCHAR(255) DEFAULT NULL,
    status VARCHAR(64) NOT NULL DEFAULT 'scheduled',
    scheduled_time DATETIME DEFAULT current_timestamp(),
    post_time DATETIME DEFAULT NULL,
    item_type varchar(32) DEFAULT NULL,
    item_id INT DEFAULT NULL,
    PRIMARY KEY (social_posting_id),
    UNIQUE (app_name, post_url),
    UNIQUE (app_name, scheduled_time)
);

CREATE TABLE social_follower_log (
    social_follower_log_id INT NOT NULL AUTO_INCREMENT,
    app_name VARCHAR(127) NOT NULL,
    follower_count INT NOT NULL,
    log_time DATETIME DEFAULT current_timestamp(),
    PRIMARY KEY (social_follower_log_id),
    UNIQUE(app_name, log_time)
);