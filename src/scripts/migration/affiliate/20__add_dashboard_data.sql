CREATE TABLE user__inbox (
    user__inbox_id INT NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    subject varchar(255) NOT NULL,
    template varchar(127) NOT NULL,
    html_file varchar(127) DEFAULT NULL,
    status enum('unread','read','deleted','') NOT NULL,
    date_sent timestamp DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (user__inbox_id),
    UNIQUE (user_id, html_file),
    FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE user__favourite(
    user__favourite_id INT NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    favourite_element_type varchar(64) NOT NULL,
    favourite_element_id int NOT NULL,
    favourite_date timestamp DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (user__favourite_id),
    UNIQUE (user_id, favourite_element_type, favourite_element_id),
    FOREIGN KEY (user_id) REFERENCES user(user_id)
);

ALTER TABLE user ADD UNIQUE (password_reset_token);