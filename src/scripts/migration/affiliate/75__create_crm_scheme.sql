CREATE TABLE crm__contact
(
    crm__contact_id     BIGINT       NOT NULL AUTO_INCREMENT,
    name                VARCHAR(255) NOT NULL,
    linked_element_type varchar(64) DEFAULT NULL,
    linked_element_id   int         DEFAULT NULL,
    website             VARCHAR(64)  NOT NULL,
    email               MEDIUMTEXT   NOT NULL,
    status              VARCHAR(64)  NOT NULL,
    PRIMARY KEY (crm__contact_id)
);

CREATE TABLE crm__note
(
    crm__note_id BIGINT     NOT NULL AUTO_INCREMENT,
    title                VARCHAR(255)        DEFAULT NULL,
    content              MEDIUMTEXT NOT NULL,
    linked_element_type varchar(64) DEFAULT NULL,
    linked_element_id   int         DEFAULT NULL,
    timestamp            TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (crm__note_id)
);

CREATE TABLE crm__contact_person
(
    crm__contact_person_id BIGINT       NOT NULL AUTO_INCREMENT,
    crm__contact_id        BIGINT          NOT NULL,
    first_name             VARCHAR(128) NOT NULL,
    last_name              VARCHAR(128) NOT NULL,
    position               VARCHAR(128) NOT NULL,
    email                  VARCHAR(255) NOT NULL,
    PRIMARY KEY (crm__contact_person_id),
    FOREIGN KEY (crm__contact_id) REFERENCES crm__contact (crm__contact_id)
);