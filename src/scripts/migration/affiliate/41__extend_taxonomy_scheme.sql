ALTER TABLE product__taxonomy add column show_in_filter BOOL DEFAULT true;
ALTER TABLE brand add column show_in_filter BOOL DEFAULT true;