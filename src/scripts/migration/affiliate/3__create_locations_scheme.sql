CREATE TABLE location (
    location_id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(512) NOT NULL,
    status ENUM('publish','draft','trash') NOT NULL,
    location_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    meta_title VARCHAR(255) DEFAULT NULL,
    meta_description VARCHAR(255) DEFAULT NULL,
    rel VARCHAR(128) DEFAULT NULL,
    og_title VARCHAR(512) DEFAULT NULL,
    og_description VARCHAR(1024) DEFAULT NULL,
    og_image VARCHAR(512) DEFAULT NULL,
    permalink VARCHAR(127) NOT NULL,
    content TEXT NOT NULL,
    attachment_id INT DEFAULT NULL,
    parent_location_id INT DEFAULT NULL,
    PRIMARY KEY (location_id),
    UNIQUE (permalink, parent_location_id),
    FOREIGN KEY (attachment_id) REFERENCES attachment(attachment_id),
    FOREIGN KEY (parent_location_id) REFERENCES location(location_id)
);

CREATE TABLE location__meta(
    location__meta_id INT NOT NULL AUTO_INCREMENT,
    meta_key VARCHAR(128) NOT NULL,
    meta_value TEXT DEFAULT NULL,
    location_id INT NOT NULL,
    PRIMARY KEY (location__meta_id),
    UNIQUE (location_id, meta_key),
    FOREIGN KEY (location_id) REFERENCES location(location_id)
);