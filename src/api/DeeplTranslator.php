<?php

namespace saschahuber\affiliatewebsitecore\api;
use AllowDynamicProperties;

#[AllowDynamicProperties]
class DeeplTranslator
{
    public function __construct()
    {
        global $CONFIG;
        $this->api_key = $CONFIG->deepl_api_key;
        $this->api_url = $CONFIG->deepl_api_url;
    }

    public function translate($text, $target_language = 'en')
    {
        $post = [
            'target_lang' => $target_language,
            'auth_key' => $this->api_key,
            'text' => $text
        ];

        $ch = curl_init($this->api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        curl_close($ch);

        # {"translations":[{"text":"TRANSLATION"}]}
        $translation_result = json_decode($response, true);
        return $translation_result;
    }

    public function get_translated_text($text, $target_language = 'en')
    {
        $translation_result = $this->translate($text, $target_language);

        if (isset($translation_result['translations']) && count($translation_result['translations']) > 0) {
            return $translation_result['translations'][0]['text'];
        }

        return null;
    }
}