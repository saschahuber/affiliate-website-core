<?php

namespace saschahuber\affiliatewebsitecore\api;

use AllowDynamicProperties;
use Amazon\ProductAdvertisingAPI\v1\ApiException;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\api\DefaultApi;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\GetItemsRequest;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\GetItemsResource;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\PartnerType;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\ProductAdvertisingAPIClientException;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\SearchItemsRequest;
use Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\SearchItemsResource;
use Amazon\ProductAdvertisingAPI\v1\Configuration;
use Exception;
use GuzzleHttp;
use saschahuber\affiliatewebsitecore\async_handler\FeedProductImportHandler;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;

require_once(VENDOR_BASE . '/autoload.php'); // change path as needed

#[AllowDynamicProperties]
class AmazonPAPIWrapper
{
    public function __construct()
    {
        // Keep these safe
        global $CONFIG;
        $this->keyId = $CONFIG->amazon_public_key;
        $this->secretKey = $CONFIG->amazon_secret_key;
        $this->associateId = $CONFIG->amazon_partner_id;

        $config = new Configuration();
        $config->setAccessKey($this->keyId);
        $config->setSecretKey($this->secretKey);
        $config->setHost('webservices.amazon.de');
        $config->setRegion('eu-west-1');

        $this->apiInstance = new DefaultApi(new GuzzleHttp\Client(), $config);
    }

    public function parseResponse($items)
    {
        $mappedResponse = array();
        foreach ($items as $item) {
            $mappedResponse[$item->getASIN()] = $item;
        }
        return $mappedResponse;
    }

    public function getByAsin($asin)
    {
        $results = $this->getByAsins(array($asin));

        if ($results != null && count($results) > 0) {
            return $results[0];
        }
        return null;
    }

    public function getByAsins($itemIds)
    {
        $resources = GetItemsResource::getAllowableEnumValues();

        # Forming the request
        $apiRequest = new GetItemsRequest();
        $apiRequest->setItemIds($itemIds);
        $apiRequest->setPartnerTag($this->associateId);
        $apiRequest->setPartnerType(PartnerType::ASSOCIATES);
        $apiRequest->setResources($resources);

        # Validating request
        $invalidPropertyList = $apiRequest->listInvalidProperties();
        $length = count($invalidPropertyList);
        if ($length > 0) {
            echo "Error forming the request", PHP_EOL;
            foreach ($invalidPropertyList as $invalidProperty) {
                echo $invalidProperty, PHP_EOL;
            }
            return null;
        }

        return $this->executeQuery($apiRequest, "getItems", "getItems");
    }

    public function executeQuery($apiRequest, $requestType = "getItems", $responseType = "getItems")
    {
        try {
            $apiResponse = $this->apiInstance->{$requestType}($apiRequest);

            # Parsing the response
            $itemsResult = $apiResponse->{$responseType . "Result"}();
            if ($itemsResult != null) {
                if ($itemsResult->getItems() != null) {
                    $results = [];
                    foreach ($itemsResult->getItems() as $item) {
                        $item_obj = $this->getItemObject($item);
                        $results[] = $item_obj;
                    }
                    return $results;
                }
            }
        } catch (ApiException $exception) {
            echo "Error calling PA-API 5.0!", PHP_EOL;
            echo "HTTP Status Code: ", $exception->getCode(), PHP_EOL;
            echo "Error Message: ", $exception->getMessage(), PHP_EOL;
            if ($exception->getResponseObject() instanceof ProductAdvertisingAPIClientException) {
                $errors = $exception->getResponseObject()->getErrors();
                foreach ($errors as $error) {
                    echo "Error Type: ", $error->getCode(), PHP_EOL;
                    echo "Error Message: ", $error->getMessage(), PHP_EOL;
                }
            } else {
                echo "Error response body: ", $exception->getResponseBody(), PHP_EOL;
            }
        } catch (Exception $exception) {
            echo "Error Message: ", $exception->getMessage(), PHP_EOL;
        }

        return null;
    }

    public function search($keyword, $searchIndex = "All", $itemCount = 10)
    {
        $resources = array(
            SearchItemsResource::ITEM_INFOTITLE,
            SearchItemsResource::ITEM_INFOFEATURES,
            SearchItemsResource::OFFERSLISTINGSPRICE,
            SearchItemsResource::ITEM_INFOPRODUCT_INFO,
            SearchItemsResource::ITEM_INFOTECHNICAL_INFO,
            SearchItemsResource::ITEM_INFOCONTENT_RATING,
            SearchItemsResource::IMAGESPRIMARYLARGE,
            SearchItemsResource::IMAGESVARIANTSLARGE
        );

        # Forming the request
        $apiRequest = new SearchItemsRequest();
        $apiRequest->setSearchIndex($searchIndex);
        $apiRequest->setKeywords($keyword);
        $apiRequest->setItemCount($itemCount);
        $apiRequest->setPartnerTag($this->associateId);
        $apiRequest->setPartnerType(PartnerType::ASSOCIATES);
        $apiRequest->setResources($resources);

        # Validating request
        $invalidPropertyList = $apiRequest->listInvalidProperties();
        $length = count($invalidPropertyList);
        if ($length > 0) {
            echo "Error forming the request", PHP_EOL;
            foreach ($invalidPropertyList as $invalidProperty) {
                echo $invalidProperty, PHP_EOL;
            }
            return null;
        }

        return $this->executeQuery($apiRequest, "searchItems", "getSearch");
    }

    public function getItemObject($item)
    {
        $item_data = json_decode($item, true);

        $is_prime = false;
        $is_available = false;
        if (isset($item['offers']) && isset($item['offers']['listings'])
            && isset($item['offers']['listings'][0]) && isset($item['offers']['listings'][0]['deliveryInfo'])
            && isset($item['offers']['listings'][0]['deliveryInfo']['isPrimeEligible'])) {
            $is_prime = $item['offers']['listings'][0]['deliveryInfo']['isPrimeEligible'] == 1;
            $is_available = count($item['offers']['listings']) > 0;
        }

        $sales_rank = null;
        if (isset($item_data['BrowseNodeInfo']) && isset($item_data['BrowseNodeInfo']['WebsiteSalesRank'])
            && isset($item_data['BrowseNodeInfo']['WebsiteSalesRank']['SalesRank'])) {
            $sales_rank = $item_data['BrowseNodeInfo']['WebsiteSalesRank']['SalesRank'];
        }

        $other_images = [];
        if (isset($item_data['Images']) && isset($item_data['Images']['Variants'])) {
            foreach ($item_data['Images']['Variants'] as $image) {
                $other_images[] = $image['Large']['URL'];
            }
        }

        $ean = null;
        if (isset($item_data['ItemInfo']) && isset($item_data['ItemInfo']['ExternalIds'])
            && isset($item_data['ItemInfo']['ExternalIds']['EANs'])
            && isset($item_data['ItemInfo']['ExternalIds']['EANs']['DisplayValues'])) {
            $ean = implode(',', $item_data['ItemInfo']['ExternalIds']['EANs']['DisplayValues']);
        }

        $features = null;
        if (isset($item_data['ItemInfo']) && isset($item_data['ItemInfo']['Features'])
            && isset($item_data['ItemInfo']['Features']['DisplayValues'])) {
            $features = $item_data['ItemInfo']['Features']['DisplayValues'];
        }

        $price = null;
        $reduced_price = null;

        if (isset($item_data['Offers'])) {
            $price_data = $item_data['Offers']['Listings'][0]['Price'];
            $price = doubleval($price_data['Amount']);
            if ($price_data && array_key_exists('Savings', $price_data)) {
                $reduced_price = $price - doubleval($price_data['Savings']['Amount']);
            }
        }

        $item_obj = array(
            'asin' => $item_data['ASIN'],
            'title' => $item_data['ItemInfo']['Title']['DisplayValue'],
            'link' => $item_data['DetailPageURL'],
            'price' => $price,
            'reduced_price' => $reduced_price,
            'primary_image' => $item_data['Images']['Primary']['Large']['URL'],
            'other_images' => $other_images,
            'sales_rank' => $sales_rank,
            'is_prime' => $is_prime,
            'is_available' => $is_available,
            'info' => $item_data['ItemInfo'],
            'ean' => $ean,
            'features' => $features
        );

        return $item_obj;
    }

    function displayResults($results, $imported_asins = [])
    {
        if (!is_array($results)) {
            return json_encode($results);
        }

        ob_start();
        ?>
        <div>
            <?php
            $rows = [];

            foreach ($results as $result) {
                $rows[] = $this->getTableRow($result, $imported_asins);
            }

            $column_labels = [
                'Bild',
                'Name',
                'ASIN',
                'EAN',
                'Preis',
                'Aktionen'
            ];

            (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
            ?>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getTableRow($result, $imported_asins = [])
    {
        $reduced_price_suffix = (isset($result['reduced_price']) ? " (Angebot: " . str_replace(".", ",", $result['reduced_price']) . "€)" : "");

        $short_title = $result['title'];
        if (strlen($short_title) > 128) {
            $short_title = substr($short_title, 0, 128) . "...";
        }

        $price_info = (isset($result['price']) ? str_replace(".", ",", $result['price']) . "€" . $reduced_price_suffix : "-");
        ob_start();


        $cells = [
            new Image($result['primary_image'], 75),
            $short_title,
            new LinkButton('https://www.amazon.de/dp/' . $result['asin'], $result['asin'], 'fas fa-eye', false, true),
            $result['ean'],
            $price_info,
            new HTMLElement(BufferHelper::buffered(function () use ($result, $imported_asins) {
                if (array_key_exists($result['asin'], $imported_asins) === false) {
                    $params = [
                        'mode' => FeedProductImportHandler::MODE_SEARCH,
                        'asin' => $result['asin']
                    ];

                    (new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Zu anderem Produkt importieren', "FeedProductImportHandler", $params))->display();

                    (new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Importieren', "AmazonProductImportHandler", ['asin' => $result['asin']]))->display();
                } else {
                    (new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => ProductManager::TYPE_PRODUCT, 'id' => $imported_asins[$result['asin']]]))->display();
                    (new LinkButton("/dashboard/produkte/bearbeiten/" . $imported_asins[$result['asin']], 'Bearbeiten', 'fas fa-pen', false, true))->display();
                }
            }))
        ];
        return new TableRow($cells);
    }
}

?>