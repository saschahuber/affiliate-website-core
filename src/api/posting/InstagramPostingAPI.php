<?php

namespace saschahuber\affiliatewebsitecore\api\posting;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\FacebookLogin;
use saschahuber\affiliatewebsitecore\service\SocialFollowerLogService;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\CacheHelper;

#[AllowDynamicProperties]
class InstagramPostingAPI
{
    function __construct()
    {
        global $CONFIG;

        $this->instagram_account_id = $CONFIG->instagram_account_id;
        $this->app_id = $CONFIG->facebook_app_id;
        $this->app_secret = $CONFIG->facebook_app_secret;
        $this->app_token = isset($CONFIG->facebook_app_token) ? $CONFIG->facebook_app_token : null;

        $this->social_follower_log_service = new SocialFollowerLogService();
    }

    function setup()
    {
        if ($this->app_token === null) {
            return;
        }

        if (!isset($this->facebook_app)) {
            $token = $this->app_token;

            $extended_token = CacheHelper::loadCache('fb_graph_tokens', 'extended@' . $this->app_id, 12 * 60);
            if ($extended_token) {
                $token = $extended_token;
            }

            $this->facebook_app = new FacebookLogin($this->app_id, $this->app_secret, $token);
            $this->extended_token = $this->facebook_app->getExtendedToken();
            CacheHelper::saveCache('fb_graph_tokens', 'extended@' . $this->app_id, $this->extended_token, true);

            $this->facebook_app->handle->setDefaultAccessToken($this->extended_token);

            $this->fan_count = $this->getPageFanCount();
            $this->social_follower_log_service->logFollowers('instagram', $this->fan_count);
            echo "current instagram followers: {$this->fan_count}" . PHP_EOL;
        }
    }

    function getExtendedToken($token)
    {
        return $this->facebook_app->handle->get('oauth/access_token?grant_type=fb_exchange_token'
            . '&client_id=' . $this->app_id . '&client_secret=' . $this->app_secret . '&fb_exchange_token=' . $token)
            ->getDecodedBody()['access_token'];
    }

    function getPageFanCount()
    {
        return $this->facebook_app->handle->get($this->instagram_account_id . '?fields=followers_count')
            ->getDecodedBody()['followers_count'];
    }

    function generatePost($content, $image_url)
    {
        $post_data = array(
            'message' => $content,
            'image_url' => $image_url
        );
        return $post_data;
    }

    /*
     * Posted einen Beitrag auf Instagram
     * Gibt die URL des geposteten Beitrags zurück bzw. false, wenn es einen Fehler gab
     */
    function post($post_data)
    {
        if (!isset($this->facebook_app)) {
            return false;
        }

        try {
            $caption = $post_data['message'];

            if (isset($post_data['hashtags']) && $post_data['hashtags'] !== null && strlen($post_data['hashtags']) > 0) {
                $caption .= " " . $post_data['hashtags'];
            }

            $image_url = $post_data['image_url'];

            $response = $this->facebook_app->handle->post('/' . $this->instagram_account_id . '/media'
                . '?image_url=' . rawurlencode($image_url)
                . '&caption=' . rawurlencode($caption), [],
                $this->extended_token)
                ->getDecodedBody();

            $creation_id = $response['id'];
            $response = $this->facebook_app->handle->post('/' . $this->instagram_account_id . '/media_publish?creation_id=' . $creation_id, [],
                $this->extended_token)->getDecodedBody();

            $response = $this->facebook_app->handle->get('/' . $response['id'] . '?fields=permalink', [],
                $this->extended_token)->getDecodedBody();
            echo json_encode($response);
        } catch (Exception $e) {
            $response = ['error' => $e->getMessage()];
            $error_message = 'Instagram Autoposting ist fehlgeschlagen; Message: ' . $post_data['message'] .
                '; IG-Antwort: ' . $response['error'];
            LogHelper::error('AUTOPOSTING', 102, $error_message, ERROR_SILENT_INTERNAL);

            LogHelper::varlog('instagram_autoposting', $error_message);

            #TODO: durch neue Version ersetzen
            #sendInternalMail("Fehler bei Instagram-Posting", $error_message);
        }

        if (isset($response['permalink'])) {
            return $response['permalink'];
        } else {
            //Fehler beim Posting
            return false;
        }
    }
}