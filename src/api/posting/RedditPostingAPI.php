<?php

namespace saschahuber\affiliatewebsitecore\api\posting;
use AllowDynamicProperties;

#[AllowDynamicProperties]
class RedditPostingAPI
{
    function __construct()
    {
        global $CONFIG;

        $this->username = $CONFIG->reddit_username;
        $this->password = $CONFIG->reddit_password;
        $this->subreddit = $CONFIG->reddit_subreddit;
        $this->client_id = $CONFIG->reddit_client_id;
        $this->client_secret = $CONFIG->reddit_client_secret;
    }

    function setup()
    {
        if (!isset($this->reddit_app)) {
            $this->setup_tokens();
            return true;
        } else {
            return false;
        }
    }

    function setup_tokens()
    {
        // connection params
        $params = array(
            'grant_type' => 'password',
            'username' => $this->username,
            'password' => $this->password
        );

        // curl settings and call to reddit
        $ch = curl_init('https://www.reddit.com/api/v1/access_token');
        curl_setopt($ch, CURLOPT_USERPWD, $this->client_id . ':' . $this->client_secret);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        // curl response from reddit
        $response_raw = curl_exec($ch);
        $response = json_decode($response_raw);
        curl_close($ch);

        $reddit_token = explode(',', $response_raw);
        $response_clean = array('"', ':', 'access_token', 'token_type', '{');

        $this->access_token = trim(str_replace($response_clean, '', $reddit_token[0]));
        $this->access_token_type = trim(str_replace($response_clean, '', $reddit_token[1]));
    }

    function post($post_data)
    {
        return $this->post_to_subreddit($this->subreddit, $post_data['title'], $post_data['link']);
    }

    function post_to_subreddit($subreddit, $postTitle, $linkUrl)
    {
        // api call endpoint
        $apiCallEndpoint = 'https://oauth.reddit.com/api/submit';

        // post data: posting a link to a subreddit
        $postData = array(
            'url' => $linkUrl . '?utm_source=reddit&utm_medium=post&utm_campaign=autoposting',
            'title' => $postTitle,
            'sr' => $subreddit,
            'kind' => 'link'
        );

        $userAgent = $subreddit . ' by /u/' . $this->username;
        $response_raw = $this->api_post($apiCallEndpoint, $userAgent, $postData);
        return $this->get_post_data($response_raw);
    }

    function get_post_data($response)
    {
        preg_match('"https://www.reddit.com/r/(.*?)/comments/(.*?)/(.*)/"', $response, $matches, PREG_OFFSET_CAPTURE);
        if (count($matches) >= 4 && count($matches[1]) > 0 && count($matches[2]) > 0 && count($matches[3]) > 0) {
            $subreddit = $matches[1][0];
            $id = $matches[2][0];
            $url = $matches[3][0];
            return "https://www.reddit.com/r/$subreddit/comments/$id/$url";
        } else {
            return false;
        }
    }

    function api_post($endpoint, $useragent, $postData)
    {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: " . $this->access_token_type . " " . $this->access_token));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    function get_post_info($subreddit, $postTitle, $linkUrl)
    {
        // api call endpoint
        $apiCallEndpoint = 'https://oauth.reddit.com/api/info?url=' . urlencode($linkUrl);

        // curl settings and call to post to the subreddit
        $ch = curl_init($apiCallEndpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $subreddit . ' by /u/' . $this->username . '');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: " . $this->access_token_type . " " . $this->access_token));
        //curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'POST' );
        //curl_setopt( $ch, CURLOPT_POSTFIELDS, $postData );

        // curl response from our post call
        $response_raw = curl_exec($ch);
        $response = json_decode($response_raw);
        curl_close($ch);

        // display response from reddit
        //var_dump( $response );

        echo '<br>-----------------------------------------<br><br>' .$response_raw;
    }
}
