<?php

namespace saschahuber\affiliatewebsitecore\api\posting;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\FacebookLogin;
use saschahuber\affiliatewebsitecore\service\SocialFollowerLogService;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\CacheHelper;

#[AllowDynamicProperties]
class FacebookPostingAPI
{
    function __construct()
    {
        global $CONFIG;

        $this->page_id = $CONFIG->facebook_page_id;
        $this->app_id = $CONFIG->facebook_app_id;
        $this->app_secret = $CONFIG->facebook_app_secret;
        $this->app_token = isset($CONFIG->facebook_app_token) ? $CONFIG->facebook_app_token : null;

        $this->social_follower_log_service = new SocialFollowerLogService();
    }

    function setup()
    {
        if ($this->app_token === null) {
            return;
        }

        if (!isset($this->facebook_app)) {
            $token = $this->app_token;

            $extended_token = CacheHelper::loadCache('fb_graph_tokens', 'extended@' . $this->app_id, 12 * 60);
            if ($extended_token) {
                $token = $extended_token;
            }

            $this->facebook_app = new FacebookLogin($this->app_id, $this->app_secret, $token);
            $this->extended_token = $this->getExtendedToken($token);
            CacheHelper::saveCache('fb_graph_tokens', 'extended@' . $this->app_id, $this->extended_token, true);
            $this->facebook_app->handle->setDefaultAccessToken($this->extended_token);

            $this->fan_count = $this->getPageFanCount($this->page_id);
            $this->social_follower_log_service->logFollowers('facebook', $this->fan_count);
            echo "current facebook fans: {$this->fan_count}" . PHP_EOL;
        }
    }

    function getExtendedToken($token)
    {
        return $this->facebook_app->handle->get('oauth/access_token?grant_type=fb_exchange_token'
            . '&client_id=' . $this->app_id . '&client_secret=' . $this->app_secret . '&fb_exchange_token=' . $token)
            ->getDecodedBody()['access_token'];
    }

    function get_page_token($page_id)
    {
        $page_token = CacheHelper::loadCache('fb_page_tokens', 'page@' . $this->app_id . '@' . $page_id, 12 * 60);
        if ($page_token === false) {
            $page_token = $this->facebook_app->handle->get($page_id . '?fields=id,username,access_token')
                ->getDecodedBody()['access_token'];
            CacheHelper::saveCache('fb_page_tokens', 'page@' . $this->app_id . '@' . $page_id, $page_token, true);
        }
        return $page_token;
    }

    function getPageFanCount($page_id)
    {
        return $this->facebook_app->handle->get($page_id . '?fields=fan_count')
            ->getDecodedBody()['fan_count'];
    }

    function generatePost($content, $link = null, $hashtags = null, $image_url = null)
    {
        $post_data = array(
            'message' => $content,
            'hashtags' => $hashtags,
            'link' => $link,
            'image_url' => $image_url
        );
        return $post_data;
    }

    private function upload_media($image_url, $post_caption)
    {
        $response = $this->facebook_app->handle->post('/' . $this->page_id . '/photos',
            [
                'url' => $image_url,
                'caption' => $post_caption,
                'published' => 'false'
            ], $this->get_page_token($this->page_id))->getDecodedBody();

        if (!isset($response['id'])) {
            LogHelper::error('FB_POST', 1, 'Bild wurde nicht akzeptiert; Page-ID: ' . $this->page_id .
                '; Bild-URL: ' . $image_url);

            return null;
        }

        return $response['id'];
    }

    /*
     * Posted einen Beitrag auf Facebook
     * Gibt die URL des geposteten Beitrags zurück bzw. false, wenn es einen Fehler gab
     */
    function post($post_data)
    {
        if (!isset($this->facebook_app)) {
            return false;
        }

        try {
            $message = $post_data['message'];
            if (array_key_exists('hashtags', $post_data) && $post_data['hashtags'] !== null) {
                $message .= "\n\n" . $post_data['hashtags'];
            }

            $data = [
                'message' => $message
            ];

            if (array_key_exists('image_url', $post_data) && $post_data['image_url'] !== null) {
                $uploaded_media = $this->upload_media($post_data['image_url'], $message);
                if (!$uploaded_media) {
                    LogHelper::error('FB_POST', 1, 'Bild wurde nach mehrmaligen Versuchen nicht akzeptiert; Page-ID: ' . $this->page_id .
                        '; Bild-URL: ' . $post_data['image_url']);
                    return false;
                }
                $data['attached_media'] = [array('media_fbid' => $uploaded_media)];
            }

            if (array_key_exists('link', $post_data)
                && $post_data['link'] !== null
                && strlen($post_data['link']) > 0) {
                $data['link'] = $post_data['link'] . '?utm_source=facebook&utm_medium=post&utm_campaign=autoposting';
            }

            $response = $this->facebook_app->handle->post($this->page_id . '/feed',
                $data, $this->get_page_token($this->page_id))->getDecodedBody();
        } catch (Exception $e) {
            $response = ['error' => $e->getMessage()];
            $error_message = 'Facebook Autoposting ist fehlgeschlagen; Message: ' . $post_data['message'] .
                '; FB-Antwort: ' . $response['error'];
            LogHelper::error('AUTOPOSTING', 102, $error_message, ERROR_SILENT_INTERNAL);

            LogHelper::varlog('facebook_autoposting', $error_message . " Daten: " . json_encode($data));

            #TODO: durch neue Version ersetzen
            #sendInternalMail("Fehler bei Facebook-Posting", $error_message. " Daten: " . json_encode($data));
        }

        if (isset($response['id'])) {
            $post_id = explode('_', $response['id'])[1];
            return 'https://facebook.com/' . $this->page_id . '/posts/' . $post_id;
        } else {
            //Fehler beim Posting
            return false;
        }
    }
}
