<?php

namespace saschahuber\affiliatewebsitecore\api\posting;

use Abraham\TwitterOAuth\TwitterOAuth;
use AllowDynamicProperties;
use Exception;
use saschahuber\affiliatewebsitecore\manager\FileManager;
use saschahuber\affiliatewebsitecore\service\SocialFollowerLogService;
use saschahuber\saastemplatecore\helper\LogHelper;

#[AllowDynamicProperties]
class TwitterPostingAPI
{
    function __construct()
    {
        global $CONFIG;

        $this->profile_name = $CONFIG->twitter_profile_name;
        $this->consumer_key = $CONFIG->twitter_consumer_key;
        $this->consumer_secret = $CONFIG->twitter_consumer_secret;
        $this->access_token = $CONFIG->twitter_access_token;
        $this->access_token_secret = $CONFIG->twitter_access_token_secret;

        $this->social_follower_log_service = new SocialFollowerLogService();
    }

    function setup()
    {
        if (!isset($this->twitter_app)) {
            $this->twitter_app = new TwitterOAuth($this->consumer_key, $this->consumer_secret,
                $this->access_token, $this->access_token_secret);
        }

        #$this->fan_count = $this->get_user_fan_count();
        #$this->social_follower_log_service->logFollowers('twitter', $this->fan_count);
        #echo "current twitter-followers: {$this->fan_count}".PHP_EOL;
    }

    function get_user_fan_count()
    {
        try {
            $data = $this->twitter_app->get("users/by/username/" . $this->profile_name,
                ["user.fields" => 'public_metrics']);
        } catch (Exception $e) {
            //Nichts tun
        }
    }

    function generatePost($content, $link = null, $hashtags = null, $image_url = null)
    {
        $post_data = array(
            'message' => $content,
            'link' => $link,
            'hashtags' => $hashtags,
            'image_url' => $image_url
        );
        return $post_data;
    }

    private function upload_media($image_url)
    {
        $file_manager = new FileManager();

        $tmp_image_path = $file_manager->downloadTmpImage($image_url);

        $data = [
            'media' => $tmp_image_path
        ];

        try {
            $result = $this->twitter_app->upload("media/upload", $data);
        } catch (Exception $e) {
            LogHelper::error('AUTOPOSTING', 102, $e->getMessage(), ERROR_SILENT_INTERNAL);
            return null;
        }

        if ($result === null || !isset($result->media_id_string)) {
            return null;
        }

        unlink($tmp_image_path);

        return $result->media_id_string;
    }

    /*
     * Posted einen Beitrag auf Facebook
     * Gibt die URL des geposteten Beitrags zurück bzw. false, wenn es einen Fehler gab
     */
    function post($post_data)
    {
        $status = $post_data['message'];

        if (array_key_exists('hashtags', $post_data) && $post_data['hashtags'] !== null) {
            $status .= "\n" . $post_data['hashtags'];
        }

        if (array_key_exists('link', $post_data) && $post_data['link'] !== null && strlen($post_data['link']) > 0) {
            $status .= "\n" . $post_data['link'] . '?utm_source=twitter&utm_medium=post&utm_campaign=autoposting';
        }

        $data = [
            'status' => $status
        ];

        if (array_key_exists('image_url', $post_data) && $post_data['image_url'] !== null) {
            $media_id_string = $this->upload_media($post_data['image_url']);

            $data['media_ids'] = $media_id_string;
        }

        try {
            $post = $this->twitter_app->post("statuses/update", $data);
        } catch (Exception $e) {
            $error_message = 'Twitter Autoposting ist fehlgeschlagen; Message: ' . $status;
            LogHelper::error('AUTOPOSTING', 102, $error_message, ERROR_SILENT_INTERNAL);

            #TODO: durch neue Version ersetzen
            #sendInternalMail("Fehler bei Twitter-Posting", $error_message);
        }

        if (isset($post->id)) {
            return 'https://twitter.com/' . $this->profile_name . '/status/' . $post->id;
        } else {
            //Fehler beim Posting
            return false;
        }
    }
}