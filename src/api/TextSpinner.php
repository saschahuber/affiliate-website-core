<?php

namespace saschahuber\affiliatewebsitecore\api;

use AllowDynamicProperties;

require_once(__DIR__ . '/DeeplTranslator.php');

#[AllowDynamicProperties]
class TextSpinner
{
    public function __construct()
    {
        $this->deepl_translator = new DeeplTranslator();
    }

    public function spinText($text, $pre_processing = true, $post_processing = true)
    {
        if ($pre_processing === true) {
            $text = $this->process($text);
        }

        $text = $this->double_translation($text);


        if ($post_processing === true) {
            $text = $this->process($text);
        }

        return $text;
    }

    private function double_translation($text)
    {
        $translated_content = $this->deepl_translator->get_translated_text($text, 'en');
        $retranslated_content = $this->deepl_translator->get_translated_text($translated_content, 'de');
        return $retranslated_content;
    }

    private function process($text)
    {
        $new_text = $this->replace_words($text);

        $new_text = $this->replace_synonyms($new_text);

        return $new_text;
    }

    private function replace_words($text)
    {
        $replacements = [
            'z. B.' => 'z.B.'
        ];

        foreach ($replacements as $to_replace => $replace_with) {
            $text = str_replace($to_replace, $replace_with, $text);
        }

        return $text;
    }

    private function replace_synonyms($text)
    {
        $words = explode(' ', $text);

        $synonyms = $this->get_synonyms();

        for ($index = 0; $index < count($words); $index++) {
            $word = $words[$index];

            foreach ($synonyms as $synonyms_index => $synonym_list) {
                foreach ($synonym_list as $synonym_item) {
                    if ($word == $synonym_item) {
                        $word = $synonym_list[array_rand($synonym_list)];
                    } elseif ($word == ucfirst($synonym_item)) {
                        $word = ucfirst($synonym_list[array_rand($synonym_list)]);
                    }
                }
            }

            $words[$index] = $word;
        }

        $new_text = implode(' ', $words);

        return $new_text;
    }

    private function get_synonyms()
    {
        return [
            ['z.B.', 'z.B.', 'zum Beispiel', 'beispielsweise'],
            ['verwenden', 'benutzen', 'nutzen'],
            ['Verwendung', 'Benutzung', 'Nutzung'],
        ];
    }
}