<?php

namespace saschahuber\affiliatewebsitecore;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\Database;
use saschahuber\saastemplatecore\DatabaseMigrationRunner;

#[AllowDynamicProperties]
class AffiliateDatabaseMigrationRunner extends DatabaseMigrationRunner{

    const SCRIPT_DEFAULT_CRONJOBS = 'default_cronjobs.sql';
    const SCRIPT_TEST_USERS = 'test_user.sql';
    const SCRIPT_CLEAR_DB = 'clear_db.sql';

    function __construct($params){
        global $CONFIG;

        $this->params = $params;

        foreach($CONFIG->default_migration_runner_params as $param){
            $this->params[] = $param;
        }

        $this->DB = new Database(false);
    }

    static function runIfNeccessary($params=[]){
        $database_migration_runner = new AffiliateDatabaseMigrationRunner($params);
        if($database_migration_runner->shouldRunMigrations()){
            $database_migration_runner->runMigrations();
        }
    }

    function shouldRunMigrations(){
        return true;
    }

    function runMigrations(){
        $clear_db = in_array("clear_db", $this->params);
        $add_test_data = in_array("test_data", $this->params);

        #Datenbank leeren
        if($clear_db === true){
            $this->clearDb();
        }

        $adding_default_cronjobs = $this->runAllMigrationScripts();

        if($adding_default_cronjobs){
            echo "Default cronjobs..." . PHP_EOL;
            $this->addDefaultCronjobs();
        }

        #Testdaten schreiben
        if($add_test_data === true){
            $this->addTestData();
        }
    }

    protected function runAllMigrationScripts(){
        $adding_default_cronjobs = false;

        $scripts = $this->getMigrationScripts();

        $executed_migrations = array();
        if($this->hasMigrationTable()){
            $executed_migrations = $this->getExecutedMigrationScripts();
        }
        else{
            $adding_default_cronjobs = true;
            echo "migration table not found...".PHP_EOL;
        }

        foreach($scripts as $script_base => $script_list){
            foreach($script_list as $script_num => $script){
                $this->runSingleMigrationScript($script_base, $script_num, $script, $executed_migrations);
            }
        }

        return $adding_default_cronjobs;
    }

    protected function runSingleMigrationScript($script_base, $script_num, $script, $executed_migrations){
        if(in_array($script, $executed_migrations)){
            echo "already ran migration #$script_num: ".$script.PHP_EOL;
            return;
        }
        echo "running migration #$script_num: ".$script.PHP_EOL;

        $script_content = file_get_contents($script_base . '/' . $script);
        $script_checksum = md5($script_content);

        if(!$this->DB->runMigrationScript($script_content)){
            echo "error in migration-script  #$script_num: $script".PHP_EOL;
            die();
        }
        $this->DB->query('INSERT INTO migration (script, checksum) 
            VALUES ("'.$this->DB->escape($script).'", "'.$this->DB->escape($script_checksum).'");');
    }

    protected function getExecutedMigrationScripts(){
        $executed_migrations = array();
        $dbquery = $this->DB->query("SELECT * FROM migration order by migration_id ASC");
        while ($migration_script = $dbquery->fetchObject()) {
            $executed_migrations[] = $migration_script->script;
        }
        return $executed_migrations;
    }

    protected function addDefaultCronjobs(){
        echo "Adding default cronjobs...".PHP_EOL;
        if(!$this->DB->runMigrationScript(file_get_contents($this->getLibScriptsPath() . "/" . self::SCRIPT_DEFAULT_CRONJOBS))){
            echo "Could not add default cronjobs...".PHP_EOL;
            die();
        }
    }

    protected function clearDb(){
        echo "Clearing db (custom scripts)...".PHP_EOL;
        $this->DB->runMigrationScript(file_get_contents($this->getCustomScriptsPath() . "/" . self::SCRIPT_CLEAR_DB));
        echo "Clearing db (affiliate scripts)...".PHP_EOL;
        $this->DB->runMigrationScript(file_get_contents($this->getAffiliateMigrationScriptsPath() . "/" . self::SCRIPT_CLEAR_DB));
        echo "Clearing db (base scripts)...".PHP_EOL;
        $this->DB->runMigrationScript(file_get_contents($this->getLibScriptsPath() . "/" . self::SCRIPT_CLEAR_DB));
    }

    protected function getMigrationScripts(){
        $scripts = [];

        $skip_base_scripts = in_array("skip_base_scripts", $this->params);

        if(!$skip_base_scripts) {
            $scripts[$this->getLibMigrationScriptsPath()] = $this->getScriptsInPath($this->getLibMigrationScriptsPath(), 'base');
        }

        $scripts[$this->getAffiliateMigrationScriptsPath()] = $this->getScriptsInPath($this->getAffiliateMigrationScriptsPath(), 'affiliate');

        $scripts[$this->getCustomMigrationScriptsPath()] = $this->getScriptsInPath($this->getCustomMigrationScriptsPath(), 'custom');

        return $scripts;
    }

    protected function getAffiliateMigrationScriptsPath(){
        return $this->getAffiliateScriptsPath() . '/migration';
    }

    protected function getAffiliateScriptsPath(){
        return __DIR__ . '/scripts';
    }
}