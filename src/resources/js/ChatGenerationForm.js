function generateText(button, formComponentId){
    let form = document.getElementById(formComponentId);

    let systemPrompt = form.querySelector('input[name="system_prompt_input"]').value;
    let prompt = form.querySelector('textarea[name="chat_generation_input"]').value;
    let model = form.querySelector('select[name="model"]').value;
    let tokens = form.querySelector('input[name="tokens"]').value;

    let responseContainer = form.querySelector('.response-container')

    let params = {
        system_prompt: systemPrompt,
        prompt: prompt,
        model: model,
        tokens: tokens
    };

    responseContainer.innerHTML = '<div class="centered"><i class="fas fa-spinner fa-spin"></i></div>';

    doApiCall('text_generator/createContent', params, function(response){
        let data = JSON.parse(response);
        responseContainer.innerHTML = '<div class="card">'+data.content+'</div>';
    });
}