function deleteAiImage(attachmentId){
    promptPopup('Möchtest du dieses Element wirklich unwiderruflich löschen?',
        function (shouldDelete) {
            if(shouldDelete) {
                doApiCall('ai_image_generator/delete', {attachment_id: attachmentId}, function(response){
                    window.location = '/dashboard/medien/dalle';
                });
            }
        },
        {yes: 'Ja', no: 'Nein'}
    )
}

function saveAiImageAsAttachment(attachmentId){
    doApiCall('ai_image/save_as_attachment', {attachment_id: attachmentId}, function(response){
        window.location = '/dashboard/medien/dalle';
    });
}