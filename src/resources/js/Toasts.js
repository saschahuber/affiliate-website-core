function fetchToastsForUser(){
    const urlParams = new URLSearchParams(window.location.search);

    let params = {
        fingerprint: getFingerprint(),
        url: window.location.href,
        referrer: document.referrer,
        mobile: window.mobileCheck(),
        utm_source: urlParams.get('utm_source'),
        utm_medium: urlParams.get('utm_medium'),
        utm_campaign: urlParams.get('utm_campaign'),
        utm_content: urlParams.get('utm_content'),
        utm_term: urlParams.get('utm_term')
    };

    doPublicApiCall('toast/user_toasts', params, function(response){
        let toastData = JSON.parse(response);

        for (let toast of toastData) {
            displayToast(toast.title, toast.content, toast.small_info, toast.delay);
        }
    });
}

window.addEventListener('load', function () {
    if((typeof websiteDomain === "undefined") || !(new String(window.location.href)).startsWith(websiteDomain)){
        return;
    }

    fetchToastsForUser();
});