function toggleFavorite(element, e, favType, favId) {
    let isActive = element.classList.contains('active');

    if (localStorage && !localStorage.getItem("favUsed")) {
        localStorage.setItem("favUsed", "true")
    }

    doApiCallWithResponseCode('favourite/toggle', {favType: favType, favId: favId}, function (responseCode, response) {
        if (responseCode === 200) {
            if (!isActive) {
                element.classList.add("active")
                element.dataset.tooltip = "Favorit entfernen"

                // GA event
                //gaEvent('Favorit', 'save ' + favType + '(' + favId + ')')
            } else {
                element.classList.remove("active")
                element.dataset.tooltip = "Favorit hinzufügen"
            }
        } else {
            console.error('Failed to toggle job favorite');
        }

        currentMouseTooltip.disappear()
        currentMouseTooltip = false
    });

    e.preventDefault()
    return false
}