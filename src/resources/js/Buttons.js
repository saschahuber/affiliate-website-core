function updateFabs(){
    let fabs = document.querySelectorAll(':not(i).fab:not(.hidden)');
    for (let i = 0; i < fabs.length; i++) {
        fabs[i].style.bottom = (25 + i*60) + "px";
    }
}