function tooltip(element, content, stay, offset) {
    if (offset === undefined) offset = {x: 0, y: 0}
    let position = element.getBoundingClientRect()
    if (position.y === undefined) position.y = position.top
    if (position.x === undefined) position.x = position.left

    let style = window.getComputedStyle(element)
    let size = {w: parseInt(style.getPropertyValue('width')) || 0, h: parseInt(style.getPropertyValue('height')) || 0}

    let tt = document.createElement('div')
    tt.className = 'tooltip'
    tt.innerHTML = '<span></span><p>' + content + '</p>'

    document.body.appendChild(tt)

    if (position.x > window.innerWidth - 300) {
        // Inhalt nach links ausbreiten
        tt.style.top = (position.y + size.h + document.documentElement.scrollTop + offset.y) + 'px'
        let right = document.documentElement.clientWidth - position.x - position.width + offset.x
        tt.style.right = right + 'px'
        tt.style.maxWidth = Math.min(480, document.documentElement.clientWidth - right - 25) + 'px'
        tt.classList.add('to-left')
    } else {
        // Inhalt nach rechts ausbreiten
        tt.style.top = (position.y + size.h + document.documentElement.scrollTop + offset.y) + 'px'
        let left = position.x + offset.x
        tt.style.left = left + 'px'
        tt.style.maxWidth = Math.min(480, document.documentElement.clientWidth - left - 25) + 'px'
        tt.classList.add('to-right')
    }

    tt.disappear = function () {
        if (!stay) element.removeEventListener('focus', tt.disappear)
        if (tt.disappearing) return
        tt.disappearing = true

        if (!tt.parentNode) return
        tt.classList.remove('visible')
        setTimeout(function () {
            document.body.removeChild(tt)
        }, 250)
    }

    if (!stay)
        element.addEventListener('focus', tt.disappear)

    setTimeout(function () {
        tt.classList.add('visible')
        if (!stay) {
            // Timed based on content length
            tt.classList.add('temp')
            let duration = Math.min(content.length * 60, 10000)
            setTimeout(function () {
                tt.disappear()
            }, duration + 250)
        }
    }, 10)

    return tt
}