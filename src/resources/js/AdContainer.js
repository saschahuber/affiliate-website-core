function loadAds(containerId, position, contentType, contentId, minDisplayWidth=null){
    let container = document.querySelector("#"+containerId+" .ad-container");

    if(minDisplayWidth && screen.width < minDisplayWidth){
        container.innerHTML = "";
        return;
    }

    let params = {
        position: position,
        content_type: contentType,
        content_id: contentId,
        fingerprint: getFingerprint(),
        source: window.location
    }

    doPublicApiCall('ads/get_fitting', params, function (response) {
        let data = JSON.parse(response);

        try {
            if (data.success && data.items) {
                let adItems = data.items;

                container.innerHTML = adItems.join('<br>');
            }
        } catch (e) {}
    });
}