function generateImageForParams(generateButton, imgPreviewId, imgUrlInputName=null){
    let form = generateButton.parentNode.parentNode.parentNode;

    let width = form.querySelector('.general-params input[name="width"]').value;
    let height = form.querySelector('.general-params input[name="height"]').value;
    let backgroundColor = form.querySelector('.general-params input[name="background_color"]').value;
    let savePath = form.querySelector('.general-params input[name="save_path"]').value;

    let layerParams = [];

    let layers = form.querySelectorAll('.image-generator-layer');

    for (let layer of layers) {
        let layerName = layer.dataset.layer;

        let inputs = layer.querySelectorAll("*[name]");

        let params = [];

        for (let input of inputs) {
            params.push(input.value);
        }

        let layerElement = {
            name: layerName,
            params: params
        };

        layerParams.push(layerElement);
    }

    let params = {
        width: width,
        height: height,
        save_path: savePath,
        background_color: backgroundColor,
        layer_params: btoa(JSON.stringify(layerParams)),
        layer_params_json: JSON.stringify(layerParams)
    };

    console.log([width, params]);

    doApiCall('image_generator/generateImage', params, function(response){
        if (response.length > 0) {
            let data = JSON.parse(response);
            let imageField = document.getElementById(imgPreviewId);
            imageField.setAttribute('src', data.image_src);

            if(imgUrlInputName !== null){
                let imgUrlInput = document.querySelector('input[name="'+imgUrlInputName+'"]');
                imgUrlInput.value = data.image_src;
            }
        }
    });
}