function preparePreviewContent(button, type, id, titleInputId, editorIdBefore, separatorContent, editorIdAfter){
    //var content = document.querySelector('#'+editorIdBefore+'_container .ck-content').innerHTML;
    var content = ckeditor_instances[editorIdBefore].getData();

    if(editorIdAfter) {
        content += atob(separatorContent);
        content += ckeditor_instances[editorIdAfter].getData();
    }

    showPreview(button, type, id, titleInputId, content)
}
function showPreview(button, type, id, titleInputId, content){
    let titleInput = document.getElementById(titleInputId);
    var postTitle = titleInput.value;

    let params = {
        id: id,
        type: type,
        title: postTitle,
        content: content
    };

    setPopupVisibility(true, 'global-popup');

    asyncHandler("ItemPreviewHandler",
        encodeQueryData(params),
        'global-popup-loading_animation',
        'global-async-target', "100%",
        function () {}
    );
}