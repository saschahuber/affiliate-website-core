function prepareContentPreview(itemUrl, dataConfig){
    let params = {};

    dataConfig = JSON.parse(atob(dataConfig));

    console.log(dataConfig);

    for(var key in dataConfig){
        let value = dataConfig[key];
        let id = value['id'];
        let isTinyMce = value['tinymce'];
        let isCKEditor = value['ckeditor'];

        if(isTinyMce){
            value = tinymce.get(id).getContent();
        }
        else if(isCKEditor){
            value = ckeditor_instances[id].getData();
        }
        else{
            value = document.getElementById(id).value;
        }

        params[key] = encodeURIComponent(value);
    }

    doApiCall('temp/store', {encoded_data: btoa(JSON.stringify(params))}, function(token){
        window.open(itemUrl+"?preview="+token, "_blank");
    });
}