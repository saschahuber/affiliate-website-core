var aiBlogPostData = null;

function generateAiBlogPost(button, titleInputId, containerId){
    let titleInput = document.getElementById(titleInputId);
    let container = document.getElementById(containerId);

    let innerHtmlBefore = button.innerHTML;
    button.innerHTML = '<i class="fas fa-spinner fa-spin"></i> Erstelle Inhaltsverzeichnis';

    aiBlogPostData = {
        content: [],
        tableOfContents: [],
        buttonContentBefore: innerHtmlBefore,
        button: button,
        progress: 0,
        progressMax: 0,
    };

    if(titleInput.value.length < 1){
        alert('Bitte gib einen Titel ein, um einen Artikel zu generieren');
        return;
    }

    var postTitle = titleInput.value;

    let params = {
        title: postTitle
    };

    doApiCall('blog_post_generator/createArticleContentList', params, function(response){
        let menuItems = JSON.parse(response);
        aiBlogPostData.tableOfContents = menuItems;

        aiBlogPostData.progressMax = menuItems.length;

        for (let index = 0; index < menuItems.length; index++) {
            var data = {
                title: menuItems[index],
                headline: '<h2>'+menuItems[index]+'</h2>',
                content: 'Wird generiert...'
            };
            aiBlogPostData.content[index] = data;

            generateHeadlineContent(index, postTitle);
        }

        button.innerHTML = '<i class="fas fa-spinner fa-spin"></i> Erstelle Inhalt für Überschriften: 0/' + aiBlogPostData.progressMax;

        updateTextAreaContent();
    });
}

function updateTextAreaContent(){
    let contentString = "";

    var content = aiBlogPostData.content;

    for (let i = 0; i < content.length; i++) {
        contentString += content[i].headline;
        contentString += '<p>' + content[i].content + '</h2>';
    }

    tinyMCE.activeEditor.setContent(contentString);

    if(aiBlogPostData.progress == aiBlogPostData.progressMax){
        aiBlogPostData.button.innerHTML = aiBlogPostData.buttonContentBefore;

        promptPopup("Dein Blogartikel wurde fertig generiert.",
            false, {ok: "Schließen"});
    }
}

function generateHeadlineContent(index, postTitle){
    let params = {
        post_title: postTitle,
        headline: aiBlogPostData.content[index].title
    };

    doApiCall('blog_post_generator/createArticleParagraph', params, function(response){
        aiBlogPostData.content[index].content = response;

        aiBlogPostData.progress++;

        aiBlogPostData.button.innerHTML = '<i class="fas fa-spinner fa-spin"></i> Erstelle Inhalt für Überschriften: ' + aiBlogPostData.progress + "/" + aiBlogPostData.progressMax;

        updateTextAreaContent();
    });
}