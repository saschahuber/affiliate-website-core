function loadSEOReport(button, keyword){
    let params = {
        url: window.location.href,
        keyword: keyword
    };

    doApiCall('seo/analyze', params, function (result) {
        promptPopup(result);
    });
}