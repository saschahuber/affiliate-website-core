function loadProductDataWithAi(button, productId, model, fieldData){
    fieldData = atob(fieldData);

    let previousButtonContent = button.innerHTML;

    button.innerHTML = '<i class="fas fa-spinner fa-spin"></i> Lade Produktdaten';

    let params = {
        product_id: productId,
        field_data: fieldData,
        model: model
    };

    doApiCall('product_data/loadProductDataWithAi', params, function(response){
        button.innerHTML = previousButtonContent;
        promptPopup(response);
        //location.reload();
    });
}

function applyAiLoadedProductData(button, productId, aiInputId, productDataInputName){
    let aiInput = document.getElementById(aiInputId);
    let productDataInput = document.getElementsByName(productDataInputName)[0];

    productDataInput.value = aiInput.value;

    button.innerHTML = "Hinzugefügt";

    button.classList.add('disabled');
}