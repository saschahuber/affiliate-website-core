function displaySponsoredPostInfoPopup(globalPopupId){
    let content = '<h2>Warum ist dieser Beitrag als Werbung markiert?</h2>\n' +
        '\n' +
        '            <br>\n' +
        '\n' +
        '            <p>\n' +
        '                Für unsere Testberichte stellen uns Hersteller teilweise Produkte zur Verfügung.\n' +
        '            </p>\n' +
        '\n' +
        '            <p>\n' +
        '                Manche unserer Ratgeber (z.B. "Garten bewässern mit Google Home & Alexa"\n' +
        '                oder "Garage in Mietwohnung smart machen")\n' +
        '                sind als Werbung markiert, da uns die dafür verwendeten Geräte von den Unternehmen bereitgestellt wurden.\n' +
        '            </p>\n' +
        '\n' +
        '            <p>\n' +
        '                Gemäß rechtlichen Vorgaben kennzeichnen wir daher Beiträge, für die wir eine finanzielle Gegenleistung erhalten haben,\n' +
        '                sowie solche, bei denen uns Produkte kostenlos zur Verfügung gestellt wurden, entsprechend als „Anzeige“, „Werbung“\n' +
        '                oder die Artikel enthalten ziemlich zu Beginn des Textes einen Abschnitt, in dem deutlich gemacht wird,\n' +
        '                dass uns der Hersteller ein Produkt bereitgestellt hat. Dies dient der Transparenz\n' +
        '                und soll euch als Leserinnen und Lesern eine klare Orientierung geben.\n' +
        '            </p>\n' +
        '\n' +
        '            <p>\n' +
        '                Unsere Bewertung, unser Fazit und unsere Meinung zu Produkten und Dienstleistungen ist <b>nicht käuflich</b>.\n' +
        '                Deshalb kommunizieren wir den Unternehmen im Vorfeld, dass wir auch schlechte Bewertungen vergeben und Makel in Produkten keineswegs verschweigen.\n' +
        '                <b>Die Unternehmen haben keinerlei Einfluss auf unsere Bewertungen</b>. Anfragen dieser Art lehnen wir jedes mal ab!\n' +
        '            </p>\n' +
        '\n' +
        '            <p>\n' +
        '                Bezahlungen dienen lediglich als Aufwandsentschädigung, da die Erstellung von hochwertigen Inhalten zeitaufwendig ist.\n' +
        '            </p>\n' +
        '\n' +
        '            <p>Mehr Infos findet ihr in unserem <a href="/werbe-hinweis" target="blank">Werbe-Hinweis</a>.</p>';

    let popupContentContainer = document.querySelector('#'+globalPopupId+' .popup-content');
    popupContentContainer.innerHTML = content;

    document.querySelector('#'+globalPopupId+' .popup').style.width = "900px";

    setPopupVisibility(true, globalPopupId);
}