function generateAiSocialPost(button, app, hashtags, containerId){
    let input = document.getElementById(containerId);

    let params = {
        topic: input.value,
        app: app,
        hashtags: hashtags
    };

    let innerHtmlBefore = button.innerHTML;
    button.innerHTML = '<i class="fas fa-spinner fa-spin"></i>';

    doApiCall('social_posts/getPostCaption', params, function(response){
        if (response.length > 0) {
            input.value = response;
            button.innerHTML = innerHtmlBefore;
        }
    });
}