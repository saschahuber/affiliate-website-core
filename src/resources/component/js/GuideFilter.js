var currentGuideStep = 0;
var filterSelections = {};
var currentStepSelections = [];

function getGuideFilterResultsList() {
    return document.getElementById('product-guide-results-list');
}

function getFilterContainer() {
    return document.querySelector('.filter-container');
}

function getFilterOptionsForm(){
    return document.querySelector('#guide_filter .filter-options-form');
}

function getFilterStepProgressBar(){
    return document.getElementById('filter-step-progress');
}

function getFilterFinishedProgressBar(){
    return document.getElementById('filter-finished-progress');
}

function getFilterLoadingContainer(){
    return document.querySelector('.filter-loading-container');
}

function getFilterButtonsContainer(){
    return document.querySelector('#guide_filter .filter-buttons-container');
}

function getFilterProgressInfoField(){
    return document.getElementById('filter-progress-info');
}

function getFilterProgressBar(){
    return document.getElementById('filter-loading-progress-bar');
}

function clearFilterButtonsContainer(){
    getFilterButtonsContainer().innerHTML = '';
}

function saveFilterSelection(){
    filterSelections[currentGuideStep] = currentStepSelections;
}

function resetGuideFilter(){
    getGuideFilterResultsList().innerHTML = "";
    getFilterLoadingContainer().classList.remove("hidden");
    getFilterContainer().classList.remove("finished");
    document.querySelector(".filter-result-count-container").classList.remove('hidden');
    initGuideFilter();
}

function updateProgressBar(){
    let progress = (currentGuideStep*100)/(guideFilterData['filter_steps'].length-1);
    setFilterProgress(progress);
}

function setFilterProgress(progress){
    let progressBar = getFilterStepProgressBar();
    progressBar.style.width = progress + "%";
}

function nextStep(){
    saveFilterSelection();
    currentStepSelections = [];
    if(currentGuideStep < guideFilterData['filter_steps'].length-1) {
        setupStep(currentGuideStep + 1);
    }
    else {
        showResults();
    }
}

function updateResultCount(){
    let tempSelections = JSON.parse(JSON.stringify(filterSelections));
    tempSelections[currentGuideStep] = currentStepSelections;

    let params = {
        guide_id: guideFilterId,
        filter_selections: JSON.stringify(tempSelections)
    }

    let filterResultCountContainer = document.querySelector('.filter-result-count-container');

    filterResultCountContainer.innerHTML = '<i class="fas fa-spinner fa-spin"></i>'

    //saveAnalyticsEvent('guide_filter', params);

    doPublicApiCall('kaufberater/result_count', params, function (response) {
        filterResultCountContainer.innerHTML = "Geschätzte Suchergebnisse: " + response;
    });
}

function addSkipButton(){
    let filterButtonsContainer = getFilterButtonsContainer();
    let skipButton = document.createElement('button');
    skipButton.innerHTML = 'Nächster Schritt <i class="fas fa-chevron-right"></i>';
    skipButton.classList.add('btn', 'btn-primary', 'guide-skip-button');
    skipButton.addEventListener('click', function () {
        nextStep();
    });
    filterButtonsContainer.appendChild(skipButton);
}

function addBackButton(){
    let filterButtonsContainer = getFilterButtonsContainer();
    let backButton = document.createElement('button');
    backButton.innerHTML = '<i class="fas fa-arrow-circle-left"></i> Vorheriger Schritt';
    backButton.classList.add('btn', 'btn-light', 'guide-back-button');
    backButton.addEventListener('click', function () {setupStep(currentGuideStep-1, true)});
    filterButtonsContainer.appendChild(backButton);
}

function addShowResultsButton(){
    let filterButtonsContainer = getFilterButtonsContainer();
    let showResultsButton = document.createElement('button');
    showResultsButton.innerHTML = 'Ergebnisse anzeigen <i class="fas fa-chevron-right"></i>';
    showResultsButton.classList.add('btn', 'btn-primary', 'guide-show-results-button');
    showResultsButton.addEventListener('click', function () {nextStep()});
    filterButtonsContainer.appendChild(showResultsButton);
}

function showResults(){
    document.querySelector(".filter-result-count-container").classList.add('hidden');

    setFilterProgress(100);

    let filterTexts = [];
    for (let filterStep of guideFilterData['filter_steps']) {
        filterTexts.push(filterStep['filter_progress_text']);
    }
    filterTexts.push("Die Ergebnisse werden sortiert...");
    filterTexts.push("Fertig! Hier kommen passende Vorschläge für dich...");

    getFilterContainer().classList.add('finished');

    for (let index in filterTexts) {
        (function (index) {
            setTimeout(function () {
                getFilterProgressInfoField().innerText = filterTexts[index];
                let progress = (index*100)/(filterTexts.length-1)
                getFilterFinishedProgressBar().style.width = progress + "%";
            }, 1500 * index);
        })(index);
    }

    setTimeout(function () {
        loadSearchResults()
    }, 1500 * filterTexts.length + 250);
}

function loadSearchResults(){
    let params = {
        guide_id: guideFilterId,
        filter_selections: JSON.stringify(filterSelections)
    }

    saveAnalyticsEvent('guide_filter', params);

    doPublicApiCall('kaufberater/search', params, function (response) {
        getFilterLoadingContainer().classList.add('hidden');
        getGuideFilterResultsList().innerHTML = response;
    });
}

function initGuideFilter(){
    setupStep(0);
    updateResultCount();
}

function setupStep(stepNum, back= false){
    clearFilterButtonsContainer();

    currentGuideStep = stepNum;

    currentStepSelections = [];
    if(typeof filterSelections[currentGuideStep] !== 'undefined') {
        currentStepSelections = filterSelections[currentGuideStep];
    }

    updateProgressBar();

    let currentStepData = guideFilterData['filter_steps'][currentGuideStep];

    if(stepNum > 0){
        addBackButton();
    }

    if(stepNum == guideFilterData['filter_steps'].length - 1){
        addShowResultsButton();
    }
    else if(stepNum < guideFilterData['filter_steps'].length){
        addSkipButton();
    }

    document.getElementById('filter-step-title').innerText = currentStepData['title'];
    document.getElementById('filter-step-description').innerText = currentStepData['description'];

    displayFilterOptions(stepNum, currentStepData, back);
}

function checkCondition(currentStepData, stepId, optionId){
    for (const [step, selectedOptions] of Object.entries(filterSelections)) {
        let stepToCheck = guideFilterData.filter_steps[step]

        if(stepToCheck.step_id !== stepId){
            continue;
        }

        let optionToCheck = null;

        for (const [key, option] of Object.entries(stepToCheck.options)){
            if(selectedOptions.includes(key)){
                optionToCheck = option;
                break;
            }
        }

        if(optionToCheck !== null && stepToCheck.step_id === stepId && optionToCheck.option_id === optionId){
            return true;
        }
    }

    return false;
}

function displayCurrentStep(stepNum, currentStepData){
    for (const [key, condition] of Object.entries(currentStepData.conditions)) {
        if(!checkCondition(currentStepData, condition.condition_step_id, condition.condition_option_id)){
            return false;
        }
    }

    return true;
}

function displayFilterOptions(stepNum, currentStepData, back = false){
    if(!displayCurrentStep(stepNum, currentStepData)){
        if(back){
            return setupStep(stepNum - 1);
        }
        else {
            return nextStep()
        }
    }

    let form = getFilterOptionsForm();
    form.innerHTML = '';

    let filterOptions = currentStepData['options'];
    for(index in filterOptions){
        form.appendChild(createFormInputItem(index, filterOptions[index],
        currentStepData['multiple_selections']));
    }
}

function createFormInputItem(index, filterOption, isMultiSelectAllowed){
    let filterItemContainer = document.createElement('div');
    filterItemContainer.setAttribute('id', index);

    let img = filterOption['img'];
    let icon = filterOption['icon'];

    let filterItemImageContainer = document.createElement('div');
    filterItemImageContainer.classList.add('filter-item-image-container');
    if(img !== null && img.length > 0){}
    else if(icon !== null && icon.length > 0){
        filterItemImageContainer.innerHTML = '<i class="'+icon+'"></i>';
    }
    filterItemContainer.appendChild(filterItemImageContainer);

    filterItemContainer.classList.add('filter-option-item');

    let filterItemLabel = document.createElement('p');
    filterItemLabel.innerText = filterOption['label'];
    filterItemContainer.appendChild(filterItemLabel);

    if(currentStepSelections.indexOf(index) > -1){
        filterItemContainer.classList.add('selected');
    }

    if(isMultiSelectAllowed) {
        //Wenn Checkboxen
        let filterItemCheckbox = document.createElement('i');
        filterItemCheckbox.classList.add('filter-checkbox-icon', 'far');
        if(currentStepSelections.indexOf(index) < 0){
            filterItemCheckbox.classList.add('fa-square');
        }
        else{
            filterItemCheckbox.classList.add('fa-check-square');
        }
        filterItemContainer.appendChild(filterItemCheckbox);

        filterItemContainer.addEventListener('click', function () {
            if(currentStepSelections.indexOf(index)  < 0){
                currentStepSelections.push(index);
                filterItemCheckbox.classList.remove('fa-square');
                filterItemCheckbox.classList.add('fa-check-square');
                filterItemContainer.classList.add('selected');
            }
            else{
                const itemToRemove = currentStepSelections.indexOf(index);
                if (itemToRemove > -1) {
                    currentStepSelections.splice(itemToRemove, 1);
                }
                filterItemCheckbox.classList.remove('fa-check-square');
                filterItemCheckbox.classList.add('fa-square');
                filterItemContainer.classList.remove('selected');
            }
            updateResultCount();
        });
    }
    else{
        //Wenn Radio-Button
        filterItemContainer.addEventListener('click', function () {
            currentStepSelections = [];
            currentStepSelections.push(index);
            nextStep();
            updateResultCount();
        });
    }

    return filterItemContainer;
}

initGuideFilter();