let searchIsLoading = false;

function hideSearchResults(){
    let searchButtonIcon = document.querySelector("#search-button div p i");
    searchButtonIcon.classList.add('fa-search');
    searchButtonIcon.classList.remove('fa-spinner');
    searchButtonIcon.classList.remove('fa-spin');

    document.getElementById("search-results-container").classList.add("hidden");
}

function search(value){
    let keyword = document.querySelector('#keyword-input').value;
    let location = document.querySelector('#location-input').value;
    let taxonomy = document.querySelector('#taxonomy-input').value;

    let params = {
        keyword: keyword,
        location: location,
        taxonomy: taxonomy
    }

    if(searchIsLoading){
        return;
    }

    searchIsLoading = true;
    let searchButtonIcon = document.querySelector("#search-button div p i");

    searchButtonIcon.classList.remove('fa-search');
    searchButtonIcon.classList.add('fa-spinner');
    searchButtonIcon.classList.add('fa-spin');

    doPublicApiCall('company_search/do_search', params, function (response) {
        saveAnalyticsEvent('company_search', params);

        searchIsLoading = false;
        searchButtonIcon.classList.add('fa-search');
        searchButtonIcon.classList.remove('fa-spinner');
        searchButtonIcon.classList.remove('fa-spin');

        document.getElementById("search-results-container").classList.remove("hidden");
        if (params.keyword.length === 0 && params.location.length === 0) {
            document.getElementById("search-results-container").innerHTML="";
            document.getElementById("search-results-container").classList.add("hidden");
            return;
        }
        document.getElementById("search-results-container").innerHTML=response;
    });
}

$(document).ready(function(e) {
    $('#keyword-input').keyup(delay(function (e) {
        search();
    }, 500));

    $('#location-input').keyup(delay(function (e) {
        search();
    }, 500));
});

function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}