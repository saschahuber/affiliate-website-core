function generateAiCompanyDescription(button, company_name_input_id, address_input_id, mail_input_id, phone_input_id,
                                      website_input_id, categories, company_id, containerId, model, tokens){
    let company_name = document.getElementById(company_name_input_id).value;
    let address = document.getElementById(address_input_id).value;
    let email = document.getElementById(mail_input_id).value;
    let phone_number = document.getElementById(phone_input_id).value;
    let website = document.getElementById(website_input_id).value;

    let container = document.getElementById(containerId);

    let innerHtmlBefore = button.innerHTML;
    button.innerHTML = '<i class="fas fa-spinner fa-spin"></i> Erstelle Firmenbeschreibung';

    let params = {
        company_name: company_name,
        address: address,
        email: email,
        phone_number: phone_number,
        website: website,
        categories: '',
        company_id: company_id,
        model: model,
        tokens: tokens
    };

    doApiCall('company_description_generator/generateAiCompanyDescription', params, function(response){
        //container.innerHTML = response;

        promptPopup(response);

        //updateTextAreaContent();
        button.innerHTML = '<i class="fas fa-robot" aria-hidden="true"></i> Unternehmensbeschreibung mit KI schreiben ('+model+')</span>';
    });
}