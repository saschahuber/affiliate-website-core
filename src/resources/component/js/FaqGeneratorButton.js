function generateFaqs(button, faqId){
    button.innerHTML = '<i class="fas fa-spinner fa-spin"></i> Erstelle FAQs';

    let params = {
        faq_id: faqId
    };

    doApiCall('faq_generator/generateFaqs', params, function(response){
        //promptPopup(response);
        location.reload();
    });
}