var aiBlogPostData = null;

function generateAiProductReview(button, productId, titleInputId, contentContainerId, proContraContainerId){
    let titleInput = document.getElementById(titleInputId);
    let contentContainer = document.getElementById(contentContainerId);
    let proContraContainer = document.getElementById(proContraContainerId);

    let productProList = document.getElementById("product_pro_list");
    let productContraList = document.getElementById("product_contra_list");

    if(titleInput.value.length < 1){
        alert('Bitte gib einen Titel ein, um einen Artikel zu generieren');
        return;
    }

    var postTitle = titleInput.value;

    let params = {
        productId: productId,
        title: postTitle,
        proList: JSON.stringify(productProList.value.split('\n')),
        contraList: JSON.stringify(productContraList.value.split('\n'))
    };

    let reviewIntroContainerId = "product_review_introduction" ;
    let reviewAttributeDescriptionContainerId = "product_review_attribute_description" ;
    let reviewContainerId = "product_review";
    let reviewSummaryContainerId = "product_review_summary";

    let popupContent = '<div>' +
        '<h2>Einleitung</h2>' +
        '<div id="'+reviewIntroContainerId+'"><i class="fas fa-spinner fa-spin"></i></div>' +
        '<h2>Eigenschaften</h2>' +
        '<div id="'+reviewAttributeDescriptionContainerId+'"><i class="fas fa-spinner fa-spin"></i></div>' +
        '<h2>Produkttest</h2>' +
        '<div id="'+reviewContainerId+'"><i class="fas fa-spinner fa-spin"></i></div>' +
        '<h2>Zusammenfassung</h2>' +
        '<div id="'+reviewSummaryContainerId+'"><i class="fas fa-spinner fa-spin"></i></div>' +
        '</div>';

    promptPopup(popupContent);

    doApiCall('product_review_generator/createIntroduction', params, function(response){
        let reviewIntroContainer = document.getElementById(reviewIntroContainerId);

        reviewIntroContainer.innerHTML = JSON.parse(response);
    });

    doApiCall('product_review_generator/createAttributeDescription', params, function(response){
        let reviewAttributeDescriptionContainer = document.getElementById(reviewAttributeDescriptionContainerId);

        reviewAttributeDescriptionContainer.innerHTML = JSON.parse(response);
    });

    doApiCall('product_review_generator/createReview', params, function(response){
        let reviewContainer = document.getElementById(reviewContainerId);
        reviewContainer.innerHTML = JSON.parse(response);

        params.review = JSON.parse(response);

        doApiCall('product_review_generator/createSummary', params, function(response){
            let reviewSummaryContainer = document.getElementById(reviewSummaryContainerId);

            reviewSummaryContainer.innerHTML = JSON.parse(response);
        });
    });
}