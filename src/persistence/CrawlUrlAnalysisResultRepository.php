<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;

class CrawlUrlAnalysisResultRepository extends Repository
{
    const TABLE_NAME = "crawl_url__analysis_result";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }
}