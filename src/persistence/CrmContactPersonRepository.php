<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;

class CrmContactPersonRepository extends Repository
{
    const TABLE_NAME = "crm__contact_person";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getAllByContactId($id)
    {
        return parent::getAllByX('crm__contact_id', $id, true);
    }
}