<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;
use stdClass;

class RedirectGroupRepository extends Repository
{
    const TABLE_NAME = "redirect__group";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getByAlias($alias)
    {
        return $this->getByX('alias', $alias);
    }

    public function createRedirectIfNotExists($from_url, $to_url, $is_active)
    {
        $item = $this->getByAlias($from_url);

        if (!$item) {
            $item = new stdClass();
        }

        $item->from_url = $from_url;
        $item->to_url = $to_url;
        $item->is_active = $is_active;

        $this->save($item);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0)
    {
        return parent::getAll($cache, $cache_lifetime_minutes);
    }

    public function trackHit($redirect_group_id)
    {
        $this->DB->query('UPDATE redirect__group set hits = hits+1 where redirect__group_id = ' . intval($redirect_group_id));
    }
}