<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;

class CompanyActionLogRepository extends Repository{
    const BASE_TABLE = "company__action_log";

    public function __construct(){
        parent::__construct(self::BASE_TABLE);
    }

    function getActions(){
        $item = [];
        foreach($this->DB->getAll("SELECT distinct(action) from ".self::BASE_TABLE) as $tag){
            $item[] = $tag->action;
        }
        return $item;
    }
}