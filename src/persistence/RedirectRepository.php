<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;
use stdClass;

class RedirectRepository extends Repository
{
    const TABLE_NAME = "redirect";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getByFromUrl($from_url)
    {
        return $this->getByX('from_url', $from_url);
    }

    public function createRedirectIfNotExists($from_url, $to_url, $is_active)
    {
        $item = $this->getByFromUrl($from_url);

        if (!$item) {
            $item = new stdClass();
        }

        $item->from_url = $from_url;
        $item->to_url = $to_url;
        $item->is_active = $is_active;

        $this->save($item);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0)
    {
        return parent::getAll($cache, $cache_lifetime_minutes);
    }
}