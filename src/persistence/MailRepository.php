<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;

class MailRepository extends Repository
{
    const TABLE_NAME = "mail";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }
}