<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\Repository;

class ToastRepository extends Repository
{
    const TABLE_NAME = "toast";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getAllToasts(){
        return $this->getAll();
    }

    public function getAllActiveToasts(){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);
        $query_builder->conditions([
            'is_active = 1',
            '(active_start >= CURRENT_TIMESTAMP() or active_start is null)',
            '(active_end <= CURRENT_TIMESTAMP() or active_end is null)',
        ]);

        return $this->getItems($query_builder->buildQuery());
    }

    public function getAllToastsForElement($linked_element_type, $linked_element_id){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);
        $query_builder->conditions([
            'linked_element_type = "'.$this->DB->escape($linked_element_type).'"',
            'linked_element_id = ' . intval($linked_element_type)
        ]);

        return $this->getItems($query_builder->buildQuery());
    }
}