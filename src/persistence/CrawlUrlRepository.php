<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\Repository;

class CrawlUrlRepository extends Repository
{
    const TABLE_NAME = "crawl_url";

    const URL_TYPE_WEBSITE = "URL_TYPE_WEBSITE";
    const URL_TYPE_JAVASCRIPT = "URL_TYPE_JAVASCRIPT";
    const URL_TYPE_CSS = "URL_TYPE_CSS";
    const URL_TYPE_IMAGE = "URL_TYPE_IMAGE";
    const URL_TYPE_VIDEO = "URL_TYPE_VIDEO";
    const URL_TYPE_UNKNOWN = "URL_TYPE_UNKNOWN";

    public function __construct(){
        parent::__construct(self::TABLE_NAME);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0){
        return parent::getAll($cache, $cache_lifetime_minutes);
    }
    
    public function getByUrl($url)
    {
        return parent::getByX('url', $url);
    }

    public function addNewUrl($url, $url_type){
        $url_item = new \stdClass();
        $url_item->url = $url;
        $url_item->url_type = $url_type;
        return $this->create($url_item);
    }

    public function setScheduled($crawl_url, $is_scheduled){
        $this->DB->query('UPDATE crawl_url SET is_scheduled = '.($is_scheduled?'1':'0').' where crawl_url.crawl_url_id = '.intval($crawl_url->crawl_url_id).';');
    }

    public function hasUrls(){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder->select('count(*) as anzahl');

        return $this->DB->getOne($query_builder->buildQuery())->anzahl > 0;
    }

    public function getUrlsToScheduleForCrawling($min_days_ago=7, $number=25){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder
            ->select('crawl_url.*, max(crawl_time) as last_crawl_time')
            ->conditions(['is_scheduled = 0'])
            ->join('crawl_url__result', 'crawl_url__result.crawl_url_id', 'crawl_url.crawl_url_id', 'left')
            ->groupBy('crawl_url.crawl_url_id')
            ->having('TIMESTAMPDIFF(DAY, last_crawl_time, NOW()) >= '.intval($min_days_ago).' or last_crawl_time is null')
            ->limit($number);

        #die($query_builder->buildQuery());

        return $this->DB->getAll($query_builder->buildQuery());
    }
}