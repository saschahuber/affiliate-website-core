<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;

class CrawlUrlResultRepository extends Repository
{
    const TABLE_NAME = "crawl_url__result";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }
}