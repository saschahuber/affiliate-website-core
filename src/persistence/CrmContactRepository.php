<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\Repository;

class CrmContactRepository extends Repository
{
    const TABLE_NAME = "crm__contact";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getByLinkedElement($type, $id){
        $query_builder = new DatabaseSelectQueryBuilder($this->base_table);
        $query_builder->conditions([
            'linked_element_type = "'.$this->DB->escape($type).'"',
            'linked_element_id = '.intval($id)
        ]);

        return $this->getItem($query_builder->buildQuery());
    }

    public function getAllContacts(){
        $querystring = 'SELECT * FROM '.$this->base_table;

        return $this->getItems($querystring);
    }
}