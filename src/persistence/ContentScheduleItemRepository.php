<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\Repository;

class ContentScheduleItemRepository extends Repository{
    const TABLE_NAME = "content_schedule_item";

    public function __construct(){
        parent::__construct(self::TABLE_NAME);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0){
        return parent::getAll($cache, $cache_lifetime_minutes);
    }

    public function removeDate($item_id){
        return $this->DB->query("UPDATE ".self::TABLE_NAME." SET content_schedule_item_date = null where content_schedule_item_id = ".intval($item_id));
    }

    public function getByLinkedElementTypeAndId($item_type, $item_id){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder->conditions([
            'linked_element_type = "' . $this->DB->escape($item_type) . '"',
            'linked_element_id = ' . intval($item_id)
        ]);

        return $this->DB->getOne($query_builder->buildQuery());
    }

    public function getAllForMonth($month, $year){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder->select('content_schedule_item.*, DAY(content_schedule_item_date) scheduled_day')
            ->conditions([
            'YEAR(content_schedule_item_date) = ' . intval($year),
            'MONTH(content_schedule_item_date) = ' . intval($month)
        ]);

        return $this->DB->getAll($query_builder->buildQuery());
    }

    public function getUnscheduled(){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder->conditions([
            'content_schedule_item_date is null'
        ]);

        return $this->DB->getAll($query_builder->buildQuery());
    }

    public function getAllPlannedInFuture(){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder->conditions([
            'content_schedule_item_date is not null',
            'content_schedule_item_date > CURRENT_TIMESTAMP()'
        ])->order('content_schedule_item_date ASC');

        return $this->DB->getAll($query_builder->buildQuery());
    }

    public function getNotYetLinked(){
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder->conditions([
                'linked_element_type is null',
                'linked_element_id is null'
            ]);

        return $this->DB->getAll($query_builder->buildQuery());
    }

    public function linkToTypeAndItem($content_schedule_item_id, $item_type, $item_id){
        return $this->DB->query('UPDATE ' . self::TABLE_NAME . ' 
            SET linked_element_type = "'.$this->DB->escape($item_type).'", 
            linked_element_id = ' . intval($item_id).' 
            WHERE '.self::TABLE_NAME.'_id = ' . intval($content_schedule_item_id));
    }

    public function unlinkElement($item_type, $item_id){
        return $this->DB->query('DELETE FROM ' . self::TABLE_NAME . ' 
            WHERE linked_element_type = "'.$this->DB->escape($item_type).'" 
            and linked_element_id = ' . intval($item_id));
    }
}