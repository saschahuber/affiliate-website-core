<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\Repository;

class CrmNoteRepository extends Repository
{
    const TABLE_NAME = "crm__note";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0)
    {
        $query_builder = new DatabaseSelectQueryBuilder($this->base_table);
        $query_builder->order('timestamp DESC');

        return $this->getItems($query_builder->buildQuery());
    }

    public function getByLinkedElement($type, $id){
        $query_builder = new DatabaseSelectQueryBuilder($this->base_table);
        $query_builder->conditions([
            'linked_element_type = "'.$this->DB->escape($type).'"',
            'linked_element_id = '.intval($id)
        ]);

        return $this->getItems($query_builder->buildQuery());
    }
}