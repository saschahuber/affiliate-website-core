<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use DateTime;
use saschahuber\saastemplatecore\persistence\Repository;
use stdClass;

class TempDataRepository extends Repository{
    const TABLE_NAME = "temp_data";

    public function __construct(){
        parent::__construct(self::TABLE_NAME);
    }

    public function getByX($search_col, $value, $intval = false){
        $item = parent::getByX($search_col, $value, $intval);
        $item->temp_data = unserialize($item->temp_data);
        return $item;
    }

    public function getByToken($token){
        return $this->getByX('token', $token);
    }

    public function addTempData($temp_data, $lifetime_seconds=ONE_HOUR){
        $token = trim(base64_encode(uniqid() . md5(time())), '=');

        $temp_data_item = new stdClass();
        $temp_data_item->token = $token;
        $temp_data_item->temp_data = serialize($temp_data);
        $temp_data_item->expire_time = date_format(new DateTime('+'.intval($lifetime_seconds).' seconds'),'Y-m-d H:i:s');

        $temp_data_item_id = $this->create($temp_data_item);

        return $token;
    }

    public function invalidateExpiredData(){
        $this->DB->query('DELETE FROM temp_data where expire_time < CURRENT_TIMESTAMP()');
    }
}