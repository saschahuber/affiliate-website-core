<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;
use stdClass;

class RedirectGroupItemRepository extends Repository
{
    const TABLE_NAME = "redirect__group_item";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getByAlias($from_url)
    {
        return $this->getByX('from_url', $from_url);
    }

    public function createRedirectIfNotExists($from_url, $to_url, $is_active)
    {
        $item = $this->getByAlias($from_url);

        if (!$item) {
            $item = new stdClass();
        }

        $item->from_url = $from_url;
        $item->to_url = $to_url;
        $item->is_active = $is_active;

        $this->save($item);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0)
    {
        return parent::getAll($cache, $cache_lifetime_minutes);
    }

    public function getByRedirectGroupId($redirect_group_id)
    {
        return parent::getAllByX('redirect__group_id', $redirect_group_id);
    }

    public function getActiveGroupItems($redirect_group_id)
    {
        $query = 'SELECT * FROM redirect__group_item where redirect__group_id = ' . intval($redirect_group_id) . ' and is_active = true;';
        return $this->DB->getAll($query);
    }

    public function trackHit($redirect_group_item_id)
    {
        $this->DB->query('UPDATE redirect__group_item set hits = hits+1 where redirect__group_item_id = ' . intval($redirect_group_item_id));
    }
}