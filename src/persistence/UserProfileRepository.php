<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\Repository;
use stdClass;

class UserProfileRepository extends Repository
{
    const BASE_TABLE = "user__profile";
    public function __construct(){
        parent::__construct(self::BASE_TABLE);
    }

    public function deleteByFingerprint($fingerprint){
        $this->DB->query('DELETE FROM '.self::BASE_TABLE.' WHERE fingerprint = "'.$this->DB->escape($fingerprint).'"');
    }

    public function deleteByFingerprintAndDataKey($fingerprint, $data_key){
        $this->DB->query('DELETE FROM '.self::BASE_TABLE.' 
            WHERE fingerprint = "'.$this->DB->escape($fingerprint).'" 
            and data_key = "'.$this->DB->escape($data_key).'"');
    }

    public function getLastUpdatedProfileFingerprints($limit=null){
        $query_builder = new DatabaseSelectQueryBuilder(self::BASE_TABLE);
        $query_builder->select('fingerprint, max(update_time) as last_update')
            ->groupBy('fingerprint')
            ->order('last_update DESC');

        if($limit) {
            $query_builder->limit($limit);
        }

        $fingerprints = [];
        foreach($this->getItems($query_builder->buildQuery()) as $profile){
            $fingerprints[$profile->fingerprint] = $profile->last_update;
        }

        return $fingerprints;
    }

    public function getAllLimit($limit=null){
        $query_builder = new DatabaseSelectQueryBuilder(self::BASE_TABLE);
        $query_builder->order('update_time DESC');

        if($limit) {
            $query_builder->limit($limit);
        }

        $fingerprints = $this->getLastUpdatedProfileFingerprints($limit);
        $query_builder->conditions([
            'fingerprint in ("'.implode('","', array_map($this->DB->escape, array_keys($fingerprints))).'")'
        ]);

        $profiles = [];
        foreach($this->getItems($query_builder->buildQuery()) as $user_profile){
            if(!array_key_exists($user_profile->fingerprint, $profiles)){
                $profiles[$user_profile->fingerprint] = [
                    'fingerprint' => $user_profile->fingerprint,
                    'update_time' => $fingerprints[$user_profile->fingerprint],
                    'profile_data' => []
                ];
            }

            $profiles[$user_profile->fingerprint]['profile_data'][$user_profile->data_key] = json_decode($user_profile->data_value, true);
        }

        return $profiles;
    }

    public function getProfileByFingerprint($fingerprint){
        $query_builder = new DatabaseSelectQueryBuilder(self::BASE_TABLE);
        $query_builder->select('max(update_time) as last_update')
            ->conditions(['fingerprint = "'.$this->DB->escape($fingerprint).'"']);
        $last_update = $this->getItem($query_builder->buildQuery())->last_update;

        $query_builder->conditions([
            'fingerprint in "'.$this->DB->escape($fingerprint).'")'
        ]);

        $profile = [
            'fingerprint' => $fingerprint,
            'update_time' => $last_update,
            'profile_data' => []
        ];
        foreach($this->getByFingerprint($fingerprint) as $user_profile){
            $profile['profile_data'][$user_profile->data_key] = json_decode($user_profile->data_value, true);
        }

        return $profile;
    }

    public function getByFingerprint($fingerprint){
        return $this->getAllByX('fingerprint', $fingerprint);
    }

    public function updateUserProfile($fingerprint, $user_id, $session_id, $data_map){}

    public function updateUserProfileField($fingerprint, $user_ip, $session_id, $user_id, $data_key, $data_value){
        $this->deleteByFingerprintAndDataKey($fingerprint, $data_key);

        $user_profile = new stdClass();
        $user_profile->fingerprint = $fingerprint;
        $user_profile->user_ip = $user_ip;
        $user_profile->user_id = $user_id;
        $user_profile->session_id = $session_id;
        $user_profile->data_key = $data_key;
        $user_profile->data_value = $data_value;

        $this->save($user_profile);
    }

    public function findFingerprintsWithProfilesToUpdate($log_time_max_ago_days=60, $profile_update_period = 14, $min_hits = 5, $limit=50){
        $query_builder = new DatabaseSelectQueryBuilder('application_analytics');
        $query_builder->selects(['application_analytics.fingerprint', 'count(*) as anzahl'])
            ->join('user__profile', 'application_analytics.fingerprint', 'user__profile.fingerprint', 'left')
            ->conditions([
                'user__profile.fingerprint is null',
                'log_time >= DATE(NOW() - INTERVAL '.intval($log_time_max_ago_days).' DAY)',
                [
                    'logic' => 'or',
                    'items' => [
                        'user__profile.update_time <= DATE(NOW() - INTERVAL '.intval($profile_update_period).' DAY)',
                        'user__profile.update_time is null'
                    ]
                ]
            ])
            ->groupBy('application_analytics.fingerprint')
            ->order('anzahl desc')
            ->having('anzahl >= '.intval($min_hits))
            ->limit($limit);

        $querystring = $query_builder->buildQuery();

        return $this->DB->getAll($querystring);
    }
}