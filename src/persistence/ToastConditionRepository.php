<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\Repository;

class ToastConditionRepository extends Repository
{
    const TABLE_NAME = "toast__condition";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getAllToasts(){
        return $this->getAll();
    }

    public function getByToastId($toast_id){
        return $this->getAllByX('toast_id', $toast_id, true);
    }
}