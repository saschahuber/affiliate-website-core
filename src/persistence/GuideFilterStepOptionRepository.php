<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\persistence\Repository;

#[AllowDynamicProperties]
class GuideFilterStepOptionRepository extends Repository{
    const BASE_TABLE = "guide_filter__step_option";

    public function __construct(){
        parent::__construct(self::BASE_TABLE);
    }

    function getActions(){
        $item = [];
        foreach($this->DB->getAll("SELECT distinct(action) from ".self::BASE_TABLE) as $tag){
            $item[] = $tag->action;
        }
        return $item;
    }
}