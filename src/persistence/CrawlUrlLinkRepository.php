<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\Repository;

class CrawlUrlLinkRepository extends Repository
{
    const TABLE_NAME = "crawl_url__link";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }
    
    public function deleteByStartUrlId($start_url_id){
        return $this->DB->query('DELETE FROM ' . self::TABLE_NAME . ' WHERE start_url_id = ' . intval($start_url_id));
    }

    public function getAllCrawlUrlsWithIncomingAndOutgoingLinks(){
        $querystring = "SELECT crawl_url.url, crawl_url.url_type, incoming_links.incoming_links, outgoing_links.outgoing_links FROM crawl_url 
            LEFT JOIN (SELECT end_url_id as url_id, count(crawl_url_link_id) as incoming_links from crawl_url__link GROUP by url_id) incoming_links 
                on crawl_url.crawl_url_id = incoming_links.url_id
            LEFT JOIN (SELECT start_url_id as url_id, count(crawl_url_link_id) as outgoing_links from crawl_url__link GROUP by url_id) outgoing_links 
                on crawl_url.crawl_url_id = outgoing_links.url_id";
        return $this->DB->getAll($querystring);
    }
}