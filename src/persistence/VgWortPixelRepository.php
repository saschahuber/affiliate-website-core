<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;

class VgWortPixelRepository extends Repository{
    const TABLE_NAME = "vg_wort_pixel";

    public function __construct(){
        parent::__construct(self::TABLE_NAME);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0){
        return parent::getAll($cache, $cache_lifetime_minutes);
    }

    public function getMappedPixels(){
        return [];
    }

    public function getByToken($token){
        return $this->getByX('token', $token);
    }
}