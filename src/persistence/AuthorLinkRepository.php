<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;

class AuthorLinkRepository extends Repository
{
    const TABLE_NAME = "author__link";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getAllByAuthorId($author_id)
    {
        return parent::getAllByX('author_id', $author_id, true);
    }
}