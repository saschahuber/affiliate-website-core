<?php

namespace saschahuber\affiliatewebsitecore\persistence;

use saschahuber\saastemplatecore\persistence\Repository;

class AuthorRepository extends Repository
{
    const TABLE_NAME = "author";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0)
    {
        return parent::getAll($cache, $cache_lifetime_minutes);
    }

    public function getByPermalink($permalink){
        return $this->getByX('permalink', $permalink, false);
    }
}