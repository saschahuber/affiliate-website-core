<?php

namespace saschahuber\affiliatewebsitecore\service;

use DateTime;
use saschahuber\affiliatewebsitecore\endpoint\SocialPostsController;
use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\persistence\ContentScheduleItemRepository;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\manager\PushNotificationManager;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\ToastService;

class ContentScheduleItemService extends Service {
    function __construct(){
        parent::__construct();

        $this->content_schedule_item_repository = new ContentScheduleItemRepository();
    }

    public function createContentScheduleItem($item){
        return $this->content_schedule_item_repository->create($item);
    }

    public function updateContentScheduleItem($item){
        return $this->content_schedule_item_repository->update($item);
    }

    public function removeDate($item_id){
        return $this->content_schedule_item_repository->removeDate($item_id);
    }

    function getById($id){
        return $this->content_schedule_item_repository->getById($id);
    }

    function getAll(){
        return $this->content_schedule_item_repository->getAll();
    }

    function getAllForMonth($month, $year){
        return $this->content_schedule_item_repository->getAllForMonth($month, $year);
    }

    function getUnscheduled(){
        return $this->content_schedule_item_repository->getUnscheduled();
    }

    function getAllPlannedInFuture(){
        return $this->content_schedule_item_repository->getAllPlannedInFuture();
    }

    public function getByLinkedElementTypeAndId($item_type, $item_id){
        return $this->content_schedule_item_repository->getByLinkedElementTypeAndId($item_type, $item_id);
    }

    public function getNotYetLinked(){
        return $this->content_schedule_item_repository->getNotYetLinked();
    }

    public function linkToTypeAndItem($content_schedule_item_id, $item_type, $item_id){
        return $this->content_schedule_item_repository->linkToTypeAndItem($content_schedule_item_id, $item_type, $item_id);
    }

    public function unlinkElement($item_type, $item_id){
        return $this->content_schedule_item_repository->unlinkElement($item_type, $item_id);
    }

    public function rescheduleItem($content_schedule_item_id, $rescheduled_day, $rescheduled_month, $rescheduled_year){
        $content_schedule_item = $this->getById($content_schedule_item_id);

        //Update date
        $date = new DateTime($content_schedule_item->content_schedule_item_date);
        $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
        $content_schedule_item->content_schedule_item_date = $date->format('Y-m-d H:i:s');

        $this->updateContentScheduleItem($content_schedule_item);

        if($content_schedule_item->linked_element_type && $content_schedule_item->linked_element_id){
            $manager = getManagerByContentType($content_schedule_item->linked_element_type);

            if($manager === null){
                return false;
            }

            $content_item = getContentElementByTypeAndId($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);

            if($content_item->status === Manager::STATUS_PUBLISH && DateHelper::isInPast($content_item->{$content_schedule_item->linked_element_type.'_date'})){
                LogHelper::logToMinuteLog("API", "Content-Planungs-Item '{$content_schedule_item->title}' hätte öffentliches Element [{$content_schedule_item->linked_element_type}] '{$content_item->title}' verändert", LogHelper::LOG_LEVEL_ERROR);

                return false;
            }

            //Update linked element date
            $date = new DateTime($content_item->{$content_schedule_item->linked_element_type.'_date'});
            $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
            $content_item->{$content_schedule_item->linked_element_type.'_date'} = $date->format('Y-m-d H:i:s');
            $manager->updateItem($content_item);


            //Update page-link-item
            $link_page_item_manager = new LinkPageItemManager();
            $link_page_item = $link_page_item_manager->getByLinkedItemTypeAndId($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
            if($link_page_item) {
                if (DateHelper::isInPast($link_page_item->link_page_item_date)) {
                    LogHelper::logToMinuteLog("API", "Content-Planungs-Item '{$content_schedule_item->title}' hätte Link-Page-Item '{$link_page_item->title}' verändert", LogHelper::LOG_LEVEL_ERROR);
                }
                $date = new DateTime($link_page_item->link_page_item_date);
                $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
                $link_page_item->link_page_item_date = $date->format('Y-m-d H:i:s');
                $link_page_item_manager->updateItem($link_page_item);
            }

            //Update push-notification
            $push_notification_manager = new PushNotificationManager();
            $notifications = $push_notification_manager->getNotificationsByElement($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
            foreach($notifications as $notification){
                if(DateHelper::isInPast($notification->scheduled_time)){
                    continue;
                }

                $date = new DateTime($notification->scheduled_time);
                $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
                $notification->scheduled_time = $date->format('Y-m-d H:i:s');
                $push_notification_manager->updateItem($notification);
            }

            //Update toast
            $toast_manager = new ScheduledToastService();
            $toasts = $toast_manager->getAllToastsForElement($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
            foreach($toasts as $toast){
                if(DateHelper::isInPast($toast->active_start)){
                    continue;
                }

                $date = new DateTime($toast->active_start);
                $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
                $toast->active_start = $date->format('Y-m-d H:i:s');
                $date->modify('+3 days');
                $toast->active_end = $date->format('Y-m-d H:i:s');
                $toast_manager->updateItem($link_page_item);
            }

            //Update social posts
            $social_post_manager = new SocialPostingFeedItemManager();
            $postings = $social_post_manager->getAllPostsForElement($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
            foreach($postings as $posting){

                if(DateHelper::isInPast($posting->posting_time)){
                    continue;
                }

                $date = new DateTime($posting->posting_time);
                $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
                $posting->posting_time = $date->format('Y-m-d H:i:s');
                $social_post_manager->updateItem($posting);
            }
        }

        return true;
    }

    public function rescheduleItemWithoutDate($content_schedule_item_id){
        $content_schedule_item = $this->getById($content_schedule_item_id);

        if($content_schedule_item->linked_element_type && $content_schedule_item->linked_element_id){
            return false;
        }

        $this->removeDate($content_schedule_item_id);

        /*
        if($content_schedule_item->linked_element_type && $content_schedule_item->linked_element_id){
            $manager = getManagerByContentType($content_schedule_item->linked_element_type);

            if($manager === null){
                return false;
            }

            $content_item = getContentElementByTypeAndId($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);

            if($content_item->status === Manager::STATUS_PUBLISH && DateHelper::isInPast($content_item->{$content_schedule_item->linked_element_type.'_date'})){
                LogHelper::logToMinuteLog("API", "Content-Planungs-Item '{$content_schedule_item->title}' hätte öffentliches Element [{$content_schedule_item->linked_element_type}] '{$content_item->title}' verändert", LogHelper::LOG_LEVEL_ERROR);

                return false;
            }

            //Update linked element date
            $date = new DateTime($content_item->{$content_schedule_item->linked_element_type.'_date'});
            $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
            $content_item->{$content_schedule_item->linked_element_type.'_date'} = $date->format('Y-m-d H:i:s');
            $manager->updateItem($content_item);


            //Update page-link-item
            $link_page_item_manager = new LinkPageItemManager();
            $link_page_item = $link_page_item_manager->getByLinkedItemTypeAndId($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
            if($link_page_item) {
                if (DateHelper::isInPast($link_page_item->link_page_item_date)) {
                    LogHelper::logToMinuteLog("API", "Content-Planungs-Item '{$content_schedule_item->title}' hätte Link-Page-Item '{$link_page_item->title}' verändert", LogHelper::LOG_LEVEL_ERROR);
                }
                $date = new DateTime($link_page_item->link_page_item_date);
                $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
                $link_page_item->link_page_item_date = $date->format('Y-m-d H:i:s');
                $link_page_item_manager->updateItem($link_page_item);
            }

            //Update push-notification
            $push_notification_manager = new PushNotificationManager();
            $notifications = $push_notification_manager->getNotificationsByElement($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
            foreach($notifications as $notification){
                if(DateHelper::isInPast($notification->scheduled_time)){
                    continue;
                }

                $date = new DateTime($notification->scheduled_time);
                $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
                $notification->scheduled_time = $date->format('Y-m-d H:i:s');
                $push_notification_manager->updateItem($notification);
            }

            //Update toast
            $toast_manager = new ScheduledToastService();
            $toasts = $toast_manager->getAllToastsForElement($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
            foreach($toasts as $toast){
                if(DateHelper::isInPast($toast->active_start)){
                    continue;
                }

                $date = new DateTime($toast->active_start);
                $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
                $toast->active_start = $date->format('Y-m-d H:i:s');
                $date->modify('+3 days');
                $toast->active_end = $date->format('Y-m-d H:i:s');
                $toast_manager->updateItem($link_page_item);
            }

            //Update social posts
            $social_post_manager = new SocialPostingFeedItemManager();
            $postings = $social_post_manager->getAllPostsForElement($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
            foreach($postings as $posting){

                if(DateHelper::isInPast($posting->posting_time)){
                    continue;
                }

                $date = new DateTime($posting->posting_time);
                $date->setDate($rescheduled_year, $rescheduled_month, $rescheduled_day);
                $posting->posting_time = $date->format('Y-m-d H:i:s');
                $social_post_manager->updateItem($posting);
            }
        }
        */

        return true;
    }
}