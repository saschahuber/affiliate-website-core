<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use stdClass;

#[AllowDynamicProperties]
class SocialFillerGeneratorService
{
    function __construct()
    {
        global $DB;
        $this->DB = $DB;
        $this->social_image_generator_service = new SocialImageGeneratorService();
        $this->social_posting_scheduler_service = new SocialPostingSchedulerService();
    }

    function scheduleRandomPost($app)
    {
        $item_id = $this->getLeastRecentlyUsedItemId($app);
        if ($item_id === false) {
            return false;
        }

        $post = $this->generateFillerPost($item_id);

        if ($post) {
            $this->social_posting_scheduler_service->schedulePost($app, $post->post_data, $post->publish_date_time,
                "filler_post", $item_id);
            echo "Scheduled post on $app for {$post->publish_date_time}: filler_post #$item_id" . PHP_EOL;
            return true;
        } else {
            return false;
        }
    }

    function getLeastRecentlyUsedItemId($app, $min_days_ago = 180)
    {
        $query = 'SELECT social_posting__filler_post.*, last_post FROM social_posting__filler_post 
            left join (SELECT item_id, MAX(scheduled_time) as last_post FROM social_posting 
                WHERE app_name = "' . $this->DB->escape($app) . '" 
                    and item_type = "filler_post" group by item_id) postings 
                on social_posting__filler_post.social_posting__filler_post_id = item_id
            where (
                    last_post is NULL
                    or last_post <= (NOW() - INTERVAL ' . intval($min_days_ago) . ' DAY)
                )
            order by last_post ASC LIMIT 1';

        $item = $this->DB->query($query)->fetchObject();
        if ($item) {
            return $item->id;
        } else {
            return false;
        }
    }

    function get_filler_post_by_id($filler_id)
    {
        return $this->DB->query("SELECT * FROM social_posting__filler_post 
            WHERE social_posting__filler_post_id = " . intval($filler_id))->fetchObject();
    }

    function generateFillerPost($item_id)
    {
        global $CONFIG;

        $publish_date_time = date("Y-m-d 14:00:00");

        $post_item = $this->get_filler_post_by_id($item_id);

        $message = $post_item->post_message;
        $hashtags = $this->formatHashtags($post_item->post_hashtags);
        $link = $post_item->post_link;
        $image_url = $this->generatePostImage($post_item->post_image_text, $post_item->post_image_template);

        $post_data = [
            'title' => null,
            'message' => $message,
            'hashtags' => $hashtags,
            'image_url' => $image_url,
            'link' => $link
        ];

        $post = new stdClass();
        $post->post_data = $post_data;
        $post->publish_date_time = $publish_date_time;
        return $post;
    }

    function format_image_text($image_text)
    {
        $max_line_length = 24;

        $formatted_text = "";

        $lines = explode("\n", $image_text);

        foreach ($lines as $line) {
            $formatted_line = "";
            $words = explode(" ", trim($line, "\n"));

            foreach ($words as $word) {
                if ((strlen($formatted_line) + strlen($word) + 1) >= $max_line_length) {
                    $formatted_text .= trim($formatted_line . " " . $word . "\n", " ");
                    $formatted_line = "";
                } else {
                    $formatted_line .= " " . $word;
                }
            }

            $formatted_line .= "\n";
            $formatted_text .= trim($formatted_line, " ");
        }

        return $formatted_text;
    }

    function generatePostImage($image_text, $image_template)
    {
        $image_text = $this->format_image_text($image_text);

        $params = array();
        return $this->social_image_generator_service->generateSocialImage("instagram", $image_template,
            $image_text, 'social', $params);
    }

    function formatHashtags($hashtags)
    {
        if ($hashtags === null) {
            return null;
        }

        $hashtags_new = preg_split("/[\s,-]+/", $hashtags);

        $formatted_hashtags = [];
        foreach ($hashtags_new as $hashtag) {
            $formatted_hashtags[] = '#' . trim($hashtag, '#');
        }
        return implode(' ', $formatted_hashtags);
    }
}