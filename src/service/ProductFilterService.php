<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class ProductFilterService extends Service{
    function __construct(){
        parent::__construct();
    }

    function getPreFilledFilterValuesByParams($params){
        $pre_filled_filter_params = [
            'data' => [],
            'kategorie' => [],
            'hersteller' => [],
            'price_min' => null,
            'price_max' => null,
            'reduced' => null,
        ];

        if(!array_key_exists('kategorie', $params)){
            $pre_filled_filter_params['kategorie'] = $this->getCategories($params);
        }

        if(!array_key_exists('hersteller', $params)){
            $pre_filled_filter_params['hersteller'] = $this->getBrands($params);
        }

        $price_min_max = $this->getPriceMinMax($params);
        if($price_min_max){
            $pre_filled_filter_params['price_min'] = $price_min_max['min'];
            $pre_filled_filter_params['price_max'] = $price_min_max['max'];
        }

        return $pre_filled_filter_params;
    }

    function getCategories($params){
        $query_builder = new DatabaseSelectQueryBuilder('product__taxonomy');

        $query_builder->select('product__taxonomy.*');

        $query_builder->join('product__taxonomy_mapping', 'product__taxonomy_mapping.taxonomy_id', 'product__taxonomy.product__taxonomy_id');
        $query_builder->join('product', 'product.product_id', 'product__taxonomy_mapping.product_id');
        $query_builder->join('product__link', 'product.product_id', 'product__link.product_id');

        $conditions = [];

        $conditions[] = 'product__taxonomy.status = "'.Manager::STATUS_PUBLISH.'"';
        $conditions[] = 'product__taxonomy.show_in_filter = true';

        $this->addCommonConditions($query_builder, $conditions, $params);

        $query_builder->conditions($conditions);

        $query_builder->order('product__taxonomy.title asc');

        $query_builder->groupBy('product__taxonomy.product__taxonomy_id');

        $allowed_values = [];
        $brands = $this->DB->getAll($query_builder->buildQuery(), true, 60);
        foreach($brands as $brand){
            $allowed_values[$brand->id] = $brand->title;
        }

        return $allowed_values;
    }

    function getBrands($params){
        $query_builder = new DatabaseSelectQueryBuilder('brand');

        $query_builder->select('brand.*');

        $query_builder->join('brand__product_mapping', 'brand__product_mapping.brand_id', 'brand.brand_id');
        $query_builder->join('product', 'product.product_id', 'brand__product_mapping.product_id');
        $query_builder->join('product__link', 'product.product_id', 'product__link.product_id');

        $conditions = [];

        $conditions[] = 'brand.status = "'.Manager::STATUS_PUBLISH.'"';
        $conditions[] = 'brand.show_in_filter = true';

        $this->addCommonConditions($query_builder, $conditions, $params);

        $query_builder->conditions($conditions);

        $query_builder->order('brand.title asc');

        $query_builder->groupBy('brand.brand_id');

        $allowed_values = [];
        $brands = $this->DB->getAll($query_builder->buildQuery(), true, 60);
        foreach($brands as $brand){
            $allowed_values[$brand->id] = $brand->title;
        }

        return $allowed_values;
    }

    function getPriceMinMax($params){
        $query_builder = new DatabaseSelectQueryBuilder('product__link');

        if(ArrayHelper::getArrayValue($params, 'reduced', 'false') === 'true'){
            $query_builder->select('IF(min(product__link.reduced_price) is not null, min(product__link.reduced_price), min(product__link.price)) as min_price');
            $query_builder->select('IF(max(product__link.reduced_price) is not null, max(product__link.reduced_price), max(product__link.price)) as max_price');
        }
        else {
            $query_builder->select('LEAST(min(product__link.reduced_price), min(product__link.price)) as min_price');
            $query_builder->select('GREATEST(max(product__link.reduced_price), max(product__link.price)) as max_price');
        }

        $query_builder->join('product', 'product.product_id', 'product__link.product_id');

        $conditions = [];

        $this->addCommonConditions($query_builder, $conditions, $params);

        $query_builder->conditions($conditions);

        $result = $this->DB->getOne($query_builder->buildQuery(), true, 60);

        return ['min' => $result->min_price?floor($result->min_price):0, 'max' => $result->max_price?ceil($result->max_price):3000];
    }

    private function addCommonConditions(&$query_builder, &$conditions, $params){

        $conditions[] = ProductService::getProductVisibilityQueryCondition();

        $kategorie = ArrayHelper::getArrayValue($params, 'kategorie', false);
        if($kategorie){
            $query_builder->join('product__taxonomy_mapping', 'product__taxonomy_mapping.product_id', 'product.product_id');

            $conditions[] = 'product__taxonomy_mapping.taxonomy_id = '.intval($kategorie);
        }

        $hersteller = ArrayHelper::getArrayValue($params, 'hersteller', false);
        if($hersteller){
            $query_builder->join('brand__product_mapping', 'brand__product_mapping.product_id', 'product.product_id');

            $conditions[] = 'brand__product_mapping.brand_id = '.intval($hersteller);
        }

        $conditions[] = '(product__link.reduced_price > 0 or product__link.reduced_price is null)';

        if(ArrayHelper::getArrayValue($params, 'reduced', 'false') === "true"){
            $conditions[] = ProductService::getReducedFilter();
        }
    }
}