<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\service\IntervalService;

#[AllowDynamicProperties]
class DailySocialMediaFollowersService extends IntervalService
{
    function __construct()
    {
        parent::__construct();
    }

    function getPlatforms()
    {
        $query = "SELECT distinct(app_name) as app_name FROM social_follower_log";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[] = $item->app_name;
        }
        return $items;
    }

    function getDailySocialMediaFollowers($interval = DatabaseTimeSeriesHelper::INTERVAL_MINUTE, $number = 60, $filter_value = null)
    {
        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $sql_date_format = "%Y-%m-%d %H:%i";
                $sql_interval = "MINUTE";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $sql_date_format = "%Y-%m-%d %H";
                $sql_interval = "HOUR";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $sql_date_format = "%Y-%m-%d";
                $sql_interval = "DAY";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $sql_date_format = "%Y-%m";
                $sql_interval = "MONTH";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $sql_date_format = "%Y";
                $sql_interval = "YEAR";
                break;
        }

        $filter_string = "social_follower_log.log_time > CURRENT_DATE - INTERVAL ".intval($number)." $sql_interval";
        if ($filter_value) {
            $filter_string .= ' and app_name = "' . $this->DB->escape($filter_value) . '"';
        }

        $query = "SELECT Date_format(social_follower_log.log_time, '$sql_date_format') AS timestamp,
                                           MAX(follower_count) AS anzahl
                                    FROM social_follower_log
                                    WHERE " . $filter_string . "
                                    GROUP BY timestamp;";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[$item->timestamp] = $item->anzahl;
        }

        return $items;
    }
}