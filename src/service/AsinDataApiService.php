<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class AsinDataApiService extends Service
{
    const API_URL = 'https://api.asindataapi.com/request';

    const TYPE_PRODUCT = 'product';

    const OUTPUT_JSON = 'json';

    const DOMAIN_AMAZON_DE = 'amazon.de';

    public function __construct()
    {
        parent::__construct();
    }

    public function getData($api_key, $asin, $amazon_domain = self::DOMAIN_AMAZON_DE, $type = self::TYPE_PRODUCT,
                            $include_html = true, $output = self::OUTPUT_JSON)
    {
        $queryString = http_build_query([
            'api_key' => $api_key,
            'amazon_domain' => $amazon_domain,
            'asin' => $asin,
            'type' => $type,
            'include_html' => $include_html ? 'true' : 'false',
            'output' => $output
        ]);

        $ch = curl_init(sprintf('%s?%s', self::API_URL, $queryString));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_TIMEOUT, 180);

        $api_result = curl_exec($ch);
        curl_close($ch);

        return json_decode($api_result, true);
    }
}