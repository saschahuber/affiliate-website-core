<?php

namespace saschahuber\affiliatewebsitecore\service;

use DOMDocument;
use saschahuber\affiliatewebsitecore\component\TableOfContentsComponent;
use saschahuber\saastemplatecore\service\Service;

class TableOfContentsService extends Service
{
    public function __construct(){
        parent::__construct();
    }

    public function generateTableOfContents(&$content){
        if(!$content || strlen($content) < 1){
            return [];
        }

        global $ROUTER;

        // Erstelle ein neues DOMDocument und lade den HTML-Code
        $dom = new DOMDocument();

        $convmap = [0x80, 0x10FFFF, 0, 0xFFFF];
        $encodedContent = mb_encode_numericentity($content, $convmap, 'UTF-8');
        @$dom->loadHTML($encodedContent);

        $toc_items = [];

        // Finde alle Überschriften
        $headings = $dom->getElementsByTagName('*');
        foreach ($headings as $node) {
            if (preg_match('/h[1-6]/i', $node->nodeName)) {
                // Generiere eine eindeutige ID für den Anker
                $id = $node->getAttribute('id');
                if(!$id) {
                    $id = strtolower(str_replace(' ', '-', trim($node->nodeValue)));
                }
                // Füge die ID dem Element hinzu
                $node->setAttribute('id', $id);
                // Füge den Eintrag zum Inhaltsverzeichnis hinzu
                $title = $node->nodeValue;
                $title = str_replace("\u{00a0}", ' ', $title);
                $title = str_replace('&nbsp;', ' ', $title);
                $title = trim($title);
                $title = trim($title, ':');
                $toc_items[] = ['level' => substr($node->nodeName, 1), 'title' => $title, 'id' => $id];
            }
        }

        // Speichere die Änderungen am HTML
        $content = $dom->saveHTML();

        return $toc_items;
    }

    public function displayTableOfContents($toc_items){
        if($toc_items && count($toc_items)){
            (new TableOfContentsComponent($toc_items))->display();
        }
    }
}