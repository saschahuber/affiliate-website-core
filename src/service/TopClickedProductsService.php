<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class TopClickedProductsService extends Service
{
    function __construct()
    {
        parent::__construct();
    }

    function getTopClickedProducts($days = 7, $limit = 10)
    {
        $query = "SELECT product.*, count(*) as anzahl_klicks from product
            join product__link_click on product.product_id = product__link_click.product_id
                where product__link_click.time > NOW() - INTERVAL " . intval($days) . " DAY group by product_id
                    order by anzahl_klicks DESC
                    LIMIT " . intval($limit) . ";";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[] = $item;
        }
        return $items;
    }
}