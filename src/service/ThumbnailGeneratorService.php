<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\builder\ImageBuilder;
use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\service\ImageGeneratorService;
use saschahuber\saastemplatecore\service\Service;

class ThumbnailGeneratorService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->image_generator_service = new ImageGeneratorServiceNeu();
        $this->image_manager = new ImageManager();
        $this->stock_image_manager = new StockImageManager();
        $this->ai_image_manager = new AiImageManager();
    }

    public function generateThumbnail($background_image, $text, $save_path)
    {
        global $CONFIG;

        //Don't regenerate image if it's not older than 3 days
        if(file_exists($save_path) && time()-filemtime($save_path) < 12 * ONE_HOUR){
            return $save_path;
        }

        $image_builder = new ImageBuilder(1920, 1080, "#0099cc");

        if ($background_image) {
            #$background_image = $background_image;
        } else {
            $background_image = IMG_DIR . "/default-background-1440p.png";
        }

        #die($background_image);

        return $image_builder->withBackgroundImage($background_image)
            ->withTint("#000000", 0.4)
            ->withWatermarkLogo(75, ImageGeneratorService::TopRight, 0.20, 75)
            ->withCenteredText(ImageBuilder::formatText($text, 30), 100, 80, 6, "#FFFFFF", 0)
            ->saveImage($save_path);
    }

    public function generateThumbnailWithSubheadline($background_image, $text, $subtext, $save_path)
    {
        global $CONFIG;

        //Don't regenerate image if it's not older than 3 days
        if(file_exists($save_path) && time()-filemtime($save_path) < 12 * ONE_HOUR){
            return $save_path;
        }

        $image_builder = new ImageBuilder(1920, 1080, "#0099cc");

        if ($background_image) {
            #$background_image = $background_image;
        } else {
            $background_image = IMG_DIR . "/default-background-1440p.png";
        }

        #die($background_image);

        return $image_builder->withBackgroundImage($background_image)
            ->withTint("#000000", 0.4)
            ->withWatermarkLogo(75, ImageGeneratorService::TopRight, 0.20, 75)
            ->withCenteredText(ImageBuilder::formatText($text, 30), 100, 80, 6, "#FFFFFF", 0)
            ->withCenteredText(ImageBuilder::formatText($subtext, 30), 60, 100, 6, "#FFFFFF", 150)
            ->saveImage($save_path);
    }

    public function getBackgroundImage($dynamic_image_type, $dynamic_image_id){
        $background_image = null;
        if($dynamic_image_id){
            switch($dynamic_image_type){
                case "attachment":
                    $background_image = DATA_DIR . "/.." . $this->image_manager->getAttachmentUrl($dynamic_image_id);
                    break;
                case "stock_attachment":
                    $background_image = DATA_DIR . "/.." . $this->stock_image_manager->getAttachmentUrl($dynamic_image_id);
                    break;
                case "ai_attachment":
                    $background_image = DATA_DIR . "/.." . $this->ai_image_manager->getAttachmentUrl($dynamic_image_id);
                    break;
            }
        }

        return $background_image;
    }
}