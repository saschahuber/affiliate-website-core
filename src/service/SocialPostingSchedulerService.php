<?php

namespace saschahuber\affiliatewebsitecore\service;
use AllowDynamicProperties;
use stdClass;

#[AllowDynamicProperties]
class SocialPostingSchedulerService
{
    function __construct()
    {
        global $DB;
        $this->DB = $DB;
    }

    function schedulePost($app_name, $post_data, $scheduled_time, $item_type = null, $item_id = null, $posting_interval = null, $social_post_id = null)
    {
        $social_posting = new stdClass();
        $social_posting->status = "scheduled";
        $social_posting->app_name = $app_name;
        $social_posting->post_data = json_encode($post_data);
        $social_posting->scheduled_time = $scheduled_time;
        $social_posting->posting_interval = $posting_interval;

        if ($item_type) {
            $social_posting->item_type = $item_type;
        }

        if ($item_id) {
            $social_posting->item_id = $item_id;
        }

        if ($social_post_id) {
            $social_posting->id = $social_post_id;
        }

        if ($social_post_id) {
            $this->DB->updateFromObject('social_posting', $social_posting);
        } else {
            $this->DB->insertFromObject('social_posting', $social_posting);
        }
    }
}