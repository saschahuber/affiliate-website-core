<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\service\ImageService;

#[AllowDynamicProperties]
class AffiliateImageService extends ImageService {
    const IMG_DIR = PUBLIC_DIR . '/img';
    const GENERATED_IMG_DIR = DATA_DIR . '/media/generated/';

    public function __construct(){
        parent::__construct();
    }

    public function getDynamicImage($template, $text, $background_image=null){
        $file_name = $this->getFilename($template, $text, $background_image);

        if(!file_exists(GENERATED_IMG_DIR . $file_name)){
            $this->generateImage($template, $text, $background_image);
        }

        return GENERATED_IMG_SRC . $file_name;
    }

    public function getDynamicImageKit($templates, $text, $background_image=null){
        $images = array();

        foreach($templates as $template){
            $images[$template] = $this->getDynamicImage($template, $text, $background_image);
        }

        return $images;
    }

    public function generateImage($template, $text, $background_image=null, $sub_dir=null, $name_prefix=null, $params=null){
        $image_template_service = new ImageTemplateService();

        $image = $image_template_service->generateTemplate($template, $text, $background_image, $params);
        
        $file_name = $this->getFilename($template, $text, $background_image, $name_prefix);
        
        $this->save($file_name, $image, $sub_dir);

        return GENERATED_IMG_SRC . ($sub_dir?$sub_dir.'/':'') . $file_name;
    }
    
    public function getFilename($template, $text, $background_image=null, $name_prefix=null){
        $text_part = UrlHelper::alias(substr($text, 0, 30));
        $template_part = substr(md5($template), 0, 4);
        $background_image_part = substr(md5(base64_encode(str_replace(BACKGROUND_ASSETS_DIR, '', $background_image))), 0, 8);
        
        return (($name_prefix?$name_prefix.'-':'') . $text_part . '-' . $template_part . (strlen($background_image_part)?'-'.$background_image_part:'')) . '.png';
    }

    function save(string $file_name, $image, $sub_dir=null){
        $path = GENERATED_IMG_DIR;
        FileHelper::createDirIfNotExists($path);

        if($sub_dir){
            $path = $path.$sub_dir.'/';
            FileHelper::createDirIfNotExists($path);
        }

        $img_path = $path . $file_name;
        imagePNG($image, $img_path);
        imageDestroy($image);
    }
}
