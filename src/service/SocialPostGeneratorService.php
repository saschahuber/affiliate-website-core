<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\DealPageManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\service\SocialPostingSchedulerService;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\KeywordHelper;
use saschahuber\saastemplatecore\service\Service;

define('ALL_ITEM_TYPES', [
    PostManager::TYPE_POST,
    ProductManager::TYPE_PRODUCT,
    #NewsManager::TYPE_NEWS
]);

class SocialPostGeneratorService extends Service
{
    function __construct()
    {
        parent::__construct();
        $this->social_posting_scheduler_service = new SocialPostingSchedulerService();
    }

    function scheduleRandomPost($app, $allowed_types = ALL_ITEM_TYPES)
    {
        $item_type = $this->getLeastRecentlyUsedItemType($app, $allowed_types);
        if ($item_type === false) {
            return false;
        }

        $item_id = $this->getLeastRecentlyUsedItemId($app, $item_type);
        if ($item_id === false) {
            return false;
        }

        $post = $this->generatePost($app, $item_type, $item_id);

        if ($post) {
            $this->social_posting_scheduler_service->schedulePost($app, $post->post_data, $post->publish_date_time, $item_type, $item_id);
            echo "Scheduled post on $app for {$post->publish_date_time}: $item_type #$item_id" . PHP_EOL;
            return true;
        } else {
            return false;
        }
    }

    function getLeastRecentlyUsedItemType($app, $allowed_types = ALL_ITEM_TYPES)
    {
        $query = 'SELECT item_type, MAX(post_time) as last_post FROM social_posting 
            WHERE app_name = "' . $this->DB->escape($app) . '" 
            and item_type in ("' . implode('", "', $allowed_types) . '") '
            . 'group by item_type ORDER BY last_post ASC';
        $dbquery = $this->DB->query($query);
        if ($dbquery->num_rows == count(ALL_ITEM_TYPES)) {
            return $dbquery->fetchObject()->item_type;
        } else if ($dbquery->num_rows < count(ALL_ITEM_TYPES)) {
            $item_types = ALL_ITEM_TYPES;
            while ($item = $dbquery->fetchObject()) {
                unset($item_types[array_search($item->item_type, $item_types)]);
            }
            return $item_types[array_rand($item_types)];
        } else {
            return ALL_ITEM_TYPES[array_rand(ALL_ITEM_TYPES)];
        }
    }

    function getLeastRecentlyUsedItemId($app, $item_type, $min_days_ago = 14)
    {
        if (!in_array($item_type, ALL_ITEM_TYPES)) {
            return false;
        }

        $escaped_item_type = $this->DB->escape($item_type);

        $additional_where = [];
        if ($item_type == "product") {
            $additional_where[] = 'is_fake = false';
        }
        #TODO: Nur News-Beiträge posten, die nicht älter sind als 14 Tage


        $query = 'SELECT ' . $escaped_item_type . '.*, last_post FROM ' . $escaped_item_type . ' 
            left join (SELECT item_id, MAX(post_time) as last_post FROM social_posting 
                WHERE app_name = "' . $this->DB->escape($app) . '" 
                    and status = "posted"
                    and item_type = "' . $escaped_item_type . '" group by item_id) postings 
                on ' . $escaped_item_type . '.' . $escaped_item_type . '_id = item_id
            where status = "publish"
                and social_posting = true
                ' . Helper::ifstr(count($additional_where), 'and ' . implode(' and ', $additional_where)) . '
                and (
                    last_post is NULL
                    or last_post <= (NOW() - INTERVAL ' . intval($min_days_ago) . ' DAY)
                )
            order by last_post ASC LIMIT 1';

        $item = $this->DB->query($query)->fetchObject();
        if ($item) {
            return $item->id;
        } else {
            return false;
        }
    }

    function generatePost($app, $item_type, $item_id)
    {
        switch ($item_type) {
            case ProductManager::TYPE_PRODUCT:
                return $this->generateProductPost($app, $item_id);
            case PostManager::TYPE_POST:
                return $this->generateBlogPost($app, $item_id);
            case NewsManager::TYPE_NEWS:
                return $this->generateNewsPost($app, $item_id);
            case DealPageManager::TYPE_DEAL:
                return $this->generateDealsPost($app, $item_id);
        }
        return false;
    }

    function generateDealsPost($app, $item_id)
    {
        global $CONFIG;

        $publish_date_time = date("Y-m-d 14:00:00");

        $deal_item = $this->DB->query("SELECT * FROM deal_page where deal_page_id = " . intval($item_id))->fetchObject();

        $message = $deal_item->title;
        $hashtags = "#deals #smarthome #angebote #sparfuchs";
        $link = $CONFIG->website_domain . '/deals/' . $deal_item->permalink;
        $hashtags = $this->formatHashtags($hashtags);

        $post_data = [
            'title' => "Jetzt sparen: " . $message,
            'message' => "Jetzt sparen: " . $message,
            'hashtags' => $hashtags,
            'link' => $link
        ];

        $post = new stdClass();
        $post->post_data = $post_data;
        $post->publish_date_time = $publish_date_time;
        return $post;
    }

    function generateBlogPost($app, $item_id)
    {
        global $CONFIG;

        $publish_date_time = date("Y-m-d 14:00:00");

        $post_manager = new PostManager();
        $post_item = $post_manager->getById($item_id);

        $message = $post_item->social_posting_message;
        $hashtags = $post_item->social_posting_hashtags;
        $link = $CONFIG->website_domain . $post_manager->generatePermalink($post_item);

        if ($message === null) {
            $message = $post_item->title;
        }

        if ($hashtags === null) {
            $hashtags = KeywordHelper::keywords($post_item->title);
        }
        $hashtags = $this->formatHashtags($hashtags);

        $post_data = [
            'title' => "Aus dem Archiv: " . $message,
            'message' => "Aus dem Archiv: " . $message,
            'hashtags' => $hashtags,
            'link' => $link
        ];

        $post = new stdClass();
        $post->post_data = $post_data;
        $post->publish_date_time = $publish_date_time;
        return $post;
    }

    function generateNewsPost($app, $item_id)
    {
        global $CONFIG;

        $publish_date_time = date("Y-m-d 14:00:00");

        $news_manager = new NewsManager();
        $news_item = $news_manager->getById($item_id);

        $message = $news_item->social_posting_message;
        $hashtags = $news_item->social_posting_hashtags;
        $link = $CONFIG->website_domain . $news_manager->generatePermalink($news_item);

        if ($message === null) {
            $message = $news_item->title;
        }

        if ($hashtags === null) {
            $hashtags = KeywordHelper::keywords($news_item->title);
        }
        $hashtags = $this->formatHashtags($hashtags);

        $post_data = [
            'title' => "Aus dem Archiv: " . $message,
            'message' => "Aus dem Archiv: " . $message,
            'hashtags' => $hashtags,
            'link' => $link
        ];

        $post = new stdClass();
        $post->post_data = $post_data;
        $post->publish_date_time = $publish_date_time;
        return $post;
    }

    function generateProductPost($app, $item_id)
    {
        global $CONFIG;
        $publish_date_time = date("Y-m-d 14:00:00");

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $product_item = $product_manager->getById($item_id);

        $message = $product_item->social_posting_message;
        $hashtags = $product_item->social_posting_hashtags;
        $link = $CONFIG->website_domain . $product_manager->generatePermalink($product_item);

        if ($message === null) {
            $message = $product_item->title;
        }

        if ($hashtags === null) {
            $hashtags = keywords($product_item->title);
        }
        $hashtags = $this->formatHashtags($hashtags);

        $post_data = [
            'title' => $message,
            'message' => $message,
            'hashtags' => $hashtags,
            'link' => $link
        ];

        $post = new stdClass();
        $post->post_data = $post_data;
        $post->publish_date_time = $publish_date_time;
        return $post;
    }

    function formatHashtags($hashtags)
    {
        $hashtags_new = preg_split("/[\s,-]+/", $hashtags);

        $formatted_hashtags = [];
        foreach ($hashtags_new as $hashtag) {
            $formatted_hashtags[] = '#' . trim($hashtag, '#');
        }
        return implode(' ', $formatted_hashtags);
    }
}