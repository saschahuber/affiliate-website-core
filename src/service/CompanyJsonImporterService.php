<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\PostableManager;
use saschahuber\affiliatewebsitecore\service\text_generator\CompanyDescriptionGeneratorService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\service\Service;
use stdClass;

#[AllowDynamicProperties]
class CompanyJsonImporterService extends Service
{
    const IMPORTER_STATUS_CREATED = "created";
    const IMPORTER_STATUS_UPDATED = "updated";
    const IMPORTER_STATUS_SKIPPED = "skipped";

    public function __construct()
    {
        parent::__construct();
        $this->company_manager = new CompanyManager();
        $this->company_description_generator_service = new CompanyDescriptionGeneratorService();
    }

    public function importCompanies($json_path)
    {
        $data = json_decode(file_get_contents($json_path), true);

        $index = 1;
        $count = count($data);

        foreach ($data as $company) {
            echo "Importing Company $index/$count [".$company['name']."]" . PHP_EOL;
            $status = $this->importCompany($company);

            if($status === self::IMPORTER_STATUS_SKIPPED){
                echo "Skipping {$company->title}" . PHP_EOL;
            }

            $index++;
        }
    }

    public function importCompany($new_company_data)
    {
        /*
        ["query","name","site","type","subtypes","category","phone","full_address","borough","street","city","postal_code","state","us_state","country","country_code","latitude","longitude","time_zone","plus_code","area_service","rating","reviews","reviews_link","reviews_tags","reviews_per_score","reviews_per_score_1","reviews_per_score_2","reviews_per_score_3","review
s_per_score_4","reviews_per_score_5","photos_count","photo","street_view","located_in","working_hours","working_hours_old_format","other_hours","popular_times","business_status","about","range","posts","logo","description","verified","owner_id","owner_title","owner_link","reservation_links","booking_appointment_link","menu_link","order_links","location_link","p
lace_id","google_id","cid","reviews_id","located_google_id","email_1","email_1.emails_validator.status","email_1.emails_validator.valid_format","email_1.emails_validator.in_blacklist","email_1.emails_validator.valid_dns","email_1.emails_validator.valid_smtp","email_1_full_name","email_1_title","email_2","email_2.emails_validator.status","email_2.emails_validato
r.valid_format","email_2.emails_validator.in_blacklist","email_2.emails_validator.valid_dns","email_2.emails_validator.valid_smtp","email_2_full_name","email_2_title","email_3","email_3.emails_validator.status","email_3.emails_validator.valid_format","email_3.emails_validator.in_blacklist","email_3.emails_validator.valid_dns","email_3.emails_validator.valid_smt
p","email_3_full_name","email_3_title","phone_1","phone_2","phone_3","facebook","instagram","linkedin","medium","reddit","skype","snapchat","telegram","whatsapp","twitter","vimeo","youtube","github","crunchbase","website_title","website_generator","website_description","website_keywords","website_has_fb_pixel","website_has_google_tag"
        */

        #die(json_encode($new_company_data, JSON_PRETTY_PRINT));

        $company = $this->company_manager->findCompanyByGooglePlaceId($new_company_data['google_id']);

        if (!$company) {
            $company = $this->company_manager->findCompanyByWebsite($new_company_data['site']);
        }

        if (!$company) {
            $company = $this->company_manager->findCompanyByPermalink(UrlHelper::alias($new_company_data['name']));
        }

        if($new_company_data['business_status'] === "CLOSED_PERMANENTLY"){
            return self::IMPORTER_STATUS_SKIPPED;
        }

        $is_new_company = false;
        if (!$company) {
            $is_new_company = true;
            $company = new stdClass();
            $company->status = PostableManager::STATUS_DRAFT;
        }

        //Objekt erstellen
        $company->title = $new_company_data['name'];

        if ($is_new_company) {

            $company->permalink = UrlHelper::alias($new_company_data['name']);
        }

        $company->google_place_id = $new_company_data['google_id'];

        $company->website = $new_company_data['site'];

        if(strlen($new_company_data['site']) < 1){
            return self::IMPORTER_STATUS_SKIPPED;
        }

        $company->phone_number = ArrayHelper::getArrayValue($new_company_data, 'phone', null);

        $company->email = ArrayHelper::getArrayValue($new_company_data, 'email_1', null);

        #die(json_encode($new_company_data));

        $company->address = $new_company_data['full_address'];
        $company->latitude = $new_company_data['latitude'];
        $company->longitude = $new_company_data['longitude'];

        if($this->skipImporting($company)){
            return self::IMPORTER_STATUS_SKIPPED;
        }

        #echo json_encode($company) . PHP_EOL;

        #return;

        if ($is_new_company) {
            $company_id = $this->company_manager->createItem($company);

            $company->company_id = $company_id;

            #$company->content = $this->company_description_generator_service->createCompanyDescription($company->title,
            #    $company->address, $company->email, $company->phone_number,
            #    $company->website, $company_id);
            #$this->company_manager->updateItem($company);
        } else {
            $this->company_manager->updateItem($company);
            $company_id = $company->company_id;
        }

        $opening_hours = $this->parseOpeningHours($new_company_data['working_hours']);
        if ($opening_hours) {
            $this->company_manager->updateOpeningHours($company_id, $opening_hours);
        }

        return $is_new_company ? self::IMPORTER_STATUS_CREATED : self::IMPORTER_STATUS_UPDATED;
    }

    private function skipImporting($company){
        $forbidden_parts = ['media markt', 'mediamarkt', 'saturn', 'otelo', 'hornbach', 'obi ', 'ikea', 'mercedes', 'baumarkt', 'werbung', 'agentur', 'expert ', 'euronics', 'telekom', 'grohe', 'shop', 'portal', 'support'];

        foreach($forbidden_parts as $forbidden_part){
            if(strpos(strtolower($company->title), strtolower($forbidden_part)) !== false){
                return true;
            }
        }

        return false;
    }

    private function parseOpeningHours($opening_hours_data)
    {
        if (!$opening_hours_data || $opening_hours_data === "") {
            return false;
        }

        #{"Montag":"08:00-19:00","Dienstag":"08:00-19:00","Mittwoch":"08:00-19:00","Donnerstag":"08:00-19:00","Freitag":"08:00-19:00","Samstag":"08:00-14:00","Sonntag":"Geschlossen"}

        $day_items = [
            'Montag' => 'monday',
            'Dienstag' => 'tuesday',
            'Mittwoch' => 'wednesday',
            'Donnerstag' => 'thursday',
            'Freitag' => 'friday',
            'Samstag' => 'saturday',
            'Sonntag' => 'sunday'
        ];

        $opening_hours = [
            'monday' => null,
            'tuesday' => null,
            'wednesday' => null,
            'thursday' => null,
            'friday' => null,
            'saturday' => null,
            'sunday' => null,
        ];

        foreach ($opening_hours_data as $weekday_german => $opening_hours_data_item) {
            $weekday = $day_items[$weekday_german];

            $open1 = null;
            $close1 = null;
            $open2 = null;
            $close2 = null;
            if ($opening_hours_data_item === "24 Stunden geöffnet") {
                $open1 = "0:00";
                $close1 = "23:59";
            } else {
                if ($opening_hours_data_item !== "Geschlossen") {
                    $times = explode(',', $opening_hours_data_item);

                    $time_parts = explode('-', $times[0]);
                    $open1 = $time_parts[0];
                    $close1 = $time_parts[1];

                    if (count($times) > 1) {
                        $time_parts = explode('-', $times[1]);
                        $open2 = $time_parts[0];
                        $close2 = $time_parts[1];
                    }
                }
            }

            $opening_hours[$weekday] = [
                'is_open' => ($opening_hours_data_item !== "Geschlossen"),
                'open1' => $open1,
                'close1' => $close1,
                'open2' => $open2,
                'close2' => $close2
            ];
        }

        return $opening_hours;
    }
}