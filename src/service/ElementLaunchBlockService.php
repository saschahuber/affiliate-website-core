<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\block\ElementLaunchOverviewBlock;
use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\saastemplatecore\manager\PushNotificationManager;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;
use saschahuber\saastemplatecore\service\Service;

class ElementLaunchBlockService extends Service
{
    public function __construct()
    {
        parent::__construct();

        $this->social_posting_feed_item_manager = new SocialPostingFeedItemManager();
        $this->link_page_item_manager = new LinkPageItemManager();
        $this->push_notification_manager = new PushNotificationManager();
        $this->scheduled_toast_service = new ScheduledToastService();
    }

    public function getElementLaunchBlock($element_type, $element_id)
    {
        $link_page_items = $this->link_page_item_manager->getAllByLinkedItemTypeAndId($element_type, $element_id);
        $push_notifications = $this->push_notification_manager->getNotificationsByElement($element_type, $element_id);
        $social_postings = $this->social_posting_feed_item_manager->getAllPostsForElement($element_type, $element_id);
        $toasts = $this->scheduled_toast_service->getAllToastsForElement($element_type, $element_id);

        return new ElementLaunchOverviewBlock($link_page_items, $push_notifications, $social_postings, $toasts, $element_type, $element_id);
    }
}