<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\service\Service;

class PagespeedService extends Service {
    function __construct(){
        parent::__construct();
    }

    function analyzePagespeed($url){
        global $CONFIG;

        $api_url = "https://content-pagespeedonline.googleapis.com/pagespeedonline/v5/runPagespeed?url=".urlencode($url)."&key={$CONFIG->google_api_key}";

        $response = FileHelper::getFromUrl($api_url);

        return json_decode($response, true);
    }
}