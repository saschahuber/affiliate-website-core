<?php

namespace saschahuber\affiliatewebsitecore\service;
use AllowDynamicProperties;
use stdClass;

#[AllowDynamicProperties]
class SocialFollowerLogService
{
    function __construct()
    {
        global $DB;
        $this->DB = $DB;
    }

    function logFollowers($app_name, $follower_count)
    {
        $current_followers = $this->getCurrentFollowers($app_name);
        if ($current_followers != $follower_count) {
            $follower_log = new stdClass();
            $follower_log->app_name = $app_name;
            $follower_log->follower_count = $follower_count;
            $this->DB->insertFromObject('social_follower_log', $follower_log);
        }
    }

    function getCurrentFollowers($app_name)
    {
        $query = "SELECT * FROM social_follower_log 
                    where app_name = '" . $this->DB->escape($app_name) . "' 
                    order by log_time DESC limit 1;";

        $follower_data = $this->DB->query($query)->fetchObject();
        if ($follower_data) {
            return $follower_data->follower_count;
        } else{
            return null;
        }
    }
}