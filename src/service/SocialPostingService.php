<?php

namespace saschahuber\affiliatewebsitecore\service;
use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\api\posting\FacebookPostingAPI;
use saschahuber\affiliatewebsitecore\api\posting\InstagramPostingAPI;
use saschahuber\affiliatewebsitecore\api\posting\RedditPostingAPI;
use saschahuber\affiliatewebsitecore\api\posting\TwitterPostingAPI;
use stdClass;

#[AllowDynamicProperties]
class SocialPostingService
{
    function __construct()
    {
        global $DB;
        $this->DB = $DB;
        $this->setupApis();
    }

    function setupApis()
    {
        //Facebook
        $this->facebook_api = new FacebookPostingAPI();
        $this->facebook_api->setup();

        //Twitter
        $this->twitter_api = new TwitterPostingAPI();
        $this->twitter_api->setup();

        //Instagram
        $this->instagram_api = new InstagramPostingAPI();
        $this->instagram_api->setup();

        //Reddit
        $this->reddit_api = new RedditPostingAPI();
        $this->reddit_api->setup();
    }

    function postScheduled()
    {
        $posts = array();

        #TODO: Höchstens ein Posting pro app gleichzeitig posten

        $dbquery = $this->DB->query('SELECT * FROM social_posting 
            where status = "scheduled" 
            AND scheduled_time < current_timestamp()
            AND post_time is null');
        while ($post = $dbquery->fetchObject()) {
            $posts[] = $post;
        }

        foreach ($posts as $post) {
            echo "Posting to {$post->app_name}: {$post->post_data}" . PHP_EOL;

            $post_data = json_decode($post->post_data, true);
            $posting_url = $this->post($post->app_name, $post_data);

            $escaped_posting_url = $posting_url ? '"' . $this->DB->escape($posting_url) . '"' : 'null';

            $status = $posting_url ? 'posted' : 'failed';

            if ($post->posting_interval !== null && $post->posting_interval !== "once") {
                $next_scheduled_time = DateTime::createFromFormat("Y-m-d H:i:s", $post->scheduled_time);

                switch ($post->posting_interval) {
                    case "weekly":
                        $next_scheduled_time->modify('+1 week');
                        break;
                    case "monthly":
                        $day = $next_scheduled_time->format('j');
                        $next_scheduled_time->modify('first day of +1 month');
                        $next_scheduled_time->modify('+' . (min($day, $next_scheduled_time->format('t')) - 1) . ' days');
                        break;
                    case "yearly":
                        $next_scheduled_time->modify('+1 year');
                        break;
                }

                $new_post = new stdClass();
                $new_post->app_name = $post->app_name;
                $new_post->post_data = $post->post_data;
                $new_post->scheduled_time = $next_scheduled_time->format("Y-m-d H:i:s");
                $new_post->item_type = $post->item_type;
                $new_post->item_id = $post->item_id;
                $new_post->posting_interval = $post->posting_interval;
                $new_post->status = "scheduled";

                echo "Rescheduling post for next interval (Next post: " . $next_scheduled_time->format("Y-m-d H:i:s") . ")" . PHP_EOL;

                $this->DB->insertFromObject("social_posting", $new_post);
            }

            $this->DB->query('UPDATE social_posting SET status = "' . $this->DB->escape($status) . '",
                post_url = ' . $escaped_posting_url . ',
                post_time = current_timestamp()
                WHERE social_posting_id = ' . intval($post->social_posting_id));
        }
    }

    function schedulePost($app_name, $post_data, $scheduled_time)
    {
        $scheduled_post = new stdClass();
        $scheduled_post->app_name = $app_name;
        $scheduled_post->post_data = json_encode($post_data);
        $scheduled_post->scheduled_time = $scheduled_time;
        $this->DB->insertFromObject('social_posting', $scheduled_post);

    }

    function post_multiple($posting_data)
    {
        foreach ($posting_data as $app_name => $post_data) {
            $this->post($app_name, $post_data);
        }
    }

    function post($app_name, $post_data)
    {
        $url = null;
        switch ($app_name) {
            case "facebook":
                $url = $this->post_facebook($post_data);
                break;
            case "instagram":
                $url = $this->post_instagram($post_data);
                break;
            case "twitter":
                $url = $this->post_twitter($post_data);
                break;
            case "reddit":
                $url = $this->post_reddit($post_data);
                break;
        }
        return $url;
    }

    function post_facebook($post_data)
    {
        return $this->facebook_api->post($post_data);
    }

    function post_twitter($post_data)
    {
        return $this->twitter_api->post($post_data);
    }

    function post_instagram($post_data)
    {
        return $this->instagram_api->post($post_data);
    }

    function post_reddit($post_data)
    {
        return $this->reddit_api->post($post_data);
    }
}