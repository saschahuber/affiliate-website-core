<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class DailyAffiliateLinkClicksService extends Service
{
    function __construct()
    {
        parent::__construct();
    }

    function getDailyAffiliateLinkClicks($day_range = 30, $product_id = null, $store = null)
    {
        $query = "SELECT TIMESTAMPDIFF(DAY, timestamp, NOW()) as log_index, label as store_type,
                DATE_FORMAT(timestamp, '%a, %d.%m.%Y') as formatted_date,
                value as hits
                FROM statistics
                where statistics_key = 'AFFILIATE_CLICKS' and TIMESTAMPDIFF(DAY, timestamp, NOW()) < " . intval($day_range) . "
                group by DATE_FORMAT(timestamp, '%a, %d.%m.%Y'), label
                order by log_index desc, timestamp asc";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[] = $item;
        }
        return $items;
    }
}