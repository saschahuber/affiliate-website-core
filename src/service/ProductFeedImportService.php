<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\async_handler\FeedProductImportHandler;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\manager\ProductShopManager;
use saschahuber\affiliatewebsitecore\model\product_feed\AwinProductFeed;
use saschahuber\affiliatewebsitecore\model\product_feed\MediaMarktSaturnProductFeed;
use saschahuber\affiliatewebsitecore\model\product_feed\RetailAdsProductFeed;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\service\Service;

class ProductFeedImportService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
        $this->product_link_manager = new ProductLinkManager();
        $this->product_feed_manager = new ProductFeedManager();
        $this->product_shop_manager = new ProductShopManager();
    }

    public static function getFeedTemplates()
    {
        return [
            'awin' => "Awin (csv, |-Trenner, als zip)",
            'retailads' => "retailAds",
            'mediamarkt_saturn' => "MediaMarkt Saturn"
        ];
    }

    public function getFeedTemplate($feed)
    {
        switch ($feed->template) {
            case 'mediamarkt_saturn':
                return new MediaMarktSaturnProductFeed();
            case 'retailads':
                return new RetailAdsProductFeed();
            case 'awin':
            default:
                return new AwinProductFeed();
        }
    }

    public function search($keyword, $feed)
    {
        $feed_template = $this->getFeedTemplate($feed);

        return $feed_template->findItems($keyword, $feed);
    }

    function displayResults($results, $feed_id, $imported_eans = [])
    {
        ob_start();
        ?>
        <div>
            <?php
            $rows = [];

            foreach (array_slice($results, 0, 100) as $result) {
                $rows[] = $this->getTableRow($result, $feed_id, $imported_eans);
            }

            $column_labels = [
                'Bild',
                'Name (& Link)',
                'EAN',
                'Preis',
                'Aktionen'
            ];

            (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
            ?>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getProductDataFromExternalId($feed, $external_id)
    {
        $feed_template = $this->getFeedTemplate($feed);
        return $feed_template->getProductDataFromExternalId($feed, $external_id);
    }

    function updateProductFromFeed($ean, $feed_id, $all_data, $product_id = null)
    {
        $product_feed = $this->product_feed_manager->getById($feed_id);
        $product_shop = $this->product_shop_manager->getById($product_feed->product__shop_id);

        if ($product_id) {
            $product = $this->product_manager->getById($product_id);
        } else {
            $product = $this->product_manager->getByEan($ean);
        }

        $product_link = null;
        if ($product) {
            $product_link = $this->product_link_manager->getByProductId($product->id, $feed_id);

            $product_link_id = null;
            if ($product_link) {
                $product_link_id = $product_link->id;
            }

            if ($ean && !$product->ean) {
                $product->ean = $ean;
                $this->product_manager->updateItem($product);
            }

            if(strlen($all_data['affiliate_link']) < 1){
                die("Affiliate-Link ist leer...");
            }

            $this->product_link_manager->saveProductLink($product->id, $product_shop->id, $product_feed->id, $all_data['price'],
                $all_data['reduced_price'], $all_data['external_id'], $all_data['affiliate_link'],
                $all_data['is_available'], $product_link_id);
        } else {
            //Produkt erstellen
            $product = $this->product_manager->updateProductFromFeed($feed_id, $ean, $all_data);

            #$this->product_link_manager->saveProductLink($product->id, $product_shop->id, $product_feed->id, $all_data['price'],
            #    $all_data['reduced_price'], $all_data['external_id'],
            #    $all_data['affiliate_link'], $all_data['is_available']);
        }

        return $product;
    }

    function getTableRow($result, $feed_id, $imported_eans = [])
    {
        global $CONFIG;

        $reduced_price_suffix = (isset($result['reduced_price']) ? " (Angebot: " . str_replace(".", ",", $result['reduced_price']) . "€)" : "");

        $short_title = $result['title'];
        if (strlen($short_title) > 128) {
            $short_title = substr($short_title, 0, 128) . "...";
        }

        $price_info = (isset($result['price']) ? str_replace(".", ",", $result['price']) . "€" . $reduced_price_suffix : "-");
        ob_start();

        $product = null;
        if (strlen($result['ean']) > 0) {
            $product = $this->product_manager->getByEan($result['ean'], false, null);
        }

        if ($product) {
            $ean_field = new LinkButton($CONFIG->website_domain . $this->product_manager->generatePermalink($product), $result['ean'], null, false, true);
        } else {
            $ean_field = new Text($result['ean']);
        }

        $cells = [
            new Image($result['primary_image'], 75),
            new LinkButton($result['affiliate_link'], $short_title, null, null, true),
            $ean_field,
            $price_info,
            new HTMLElement(BufferHelper::buffered(function () use ($feed_id, $result, $product, $imported_eans) {
                if (!array_key_exists($result['ean'], $imported_eans)) {
                    $params = [
                        'feed_id' => $feed_id,
                        #'data' => base64_encode(json_encode($result)),
                        'mode' => FeedProductImportHandler::MODE_SEARCH,
                        'ean' => $result['ean'],
                        'external_id' => $result['external_id'],
                    ];

                    (new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Zu anderem Produkt importieren', "FeedProductImportHandler", $params))->display();

                    if ($result['ean']) {
                        $label = "Importieren";
                        $params['mode'] = FeedProductImportHandler::MODE_IMPORT;
                        if ($product) {
                            $label = "Zu Produkt importieren (siehe EAN)";
                        }
                        (new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> ' . $label, "FeedProductImportHandler", $params))->display();
                    }
                }

                if ($product) {
                    (new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => ProductManager::TYPE_PRODUCT, 'id' => $product->id]))->display();
                    (new LinkButton("/dashboard/produkte/bearbeiten/" . $product->id, 'Bearbeiten', 'fas fa-pen', false, true))->display();
                }
            }))
        ];
        return new TableRow($cells);
    }
}
