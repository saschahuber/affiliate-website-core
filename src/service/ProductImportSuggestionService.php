<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\ProductImportSuggestionManager;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class ProductImportSuggestionService extends Service
{
    function __construct()
    {
        parent::__construct();
        $this->product_import_suggestion_manager = new ProductImportSuggestionManager();
    }

    function createImportSuggestion($item){
        return $this->product_import_suggestion_manager->createItem($item);
    }

    public function suggestionExists($product_id, $feed_id, $external_identifier){
        return $this->product_import_suggestion_manager->suggestionExists($product_id, $feed_id, $external_identifier);
    }

    public function getAll(){
        return $this->product_import_suggestion_manager->getAll();
    }

    public function getById($id){
        return $this->product_import_suggestion_manager->getById($id);
    }

    public function delete($id){
        return $this->product_import_suggestion_manager->delete($id);
    }

    public function deleteByProductId($id){
        return $this->product_import_suggestion_manager->deleteByProductId($id);
    }
}