<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\saastemplatecore\service\Service;
use stdClass;

class DealPageGeneratorService extends Service
{
    function __construct()
    {
        parent::__construct();
        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
        $this->brand_manager = new BrandManager();
    }

    function getAllReducedProducts()
    {
        $products = [];

        $reduced_products_query = 'SELECT *,
               group_concat(product__taxonomy_mapping.taxonomy_id) taxonomies,
               group_concat(brand__product_mapping.brand_id) brands
        FROM   product
               INNER JOIN brand__product_mapping
                       ON find_in_set(brand__product_mapping.product_id, product.product_id) > 0
               INNER JOIN product__taxonomy_mapping
                       ON find_in_set(product__taxonomy_mapping.product_id, product.product_id) > 0
        WHERE status = "publish"
        and is_available = true
        and amazon_asin is not null
        and product.hide_in_search is false
        ' . $this->product_manager->getReducedFilter() . '
        GROUP  BY product.product_id';

        #echo $reduced_products_query;

        $dbquery = $this->DB->query($reduced_products_query, false, false, false);

        while ($product = $dbquery->fetchObject()) {
            $product_data = new stdClass();

            $product_data->taxonomy_ids = array_unique(explode(',', $product->taxonomies));
            $product_data->brand_ids = array_unique(explode(',', $product->brands));
            $product_data->deal_info = $this->getDealInfo($product);

            $product_data->id = $product->id;
            $product_data->title = $product->title;
            $product_data->amazon_asin = $product->amazon_asin;

            if ($product_data->deal_info['score'] > 0) {
                $products[] = $product_data;
            }
        }

        return $products;
    }

    function createDealPage($deal_page)
    {
        $category_ids = array_unique(explode(',', $deal_page->product_categories));
        $brand_ids = array_unique(explode(',', $deal_page->product_brands));

        $all_reduced_products = $this->getAllReducedProducts();

        $category_deals = $this->getCategoryDeals($all_reduced_products, $category_ids);
        $brand_deals = $this->getBrandDeals($all_reduced_products, $brand_ids);

        $deal_page_data = array(
            'top_deals' => $this->getTopDeals($all_reduced_products),
            'category_deals' => $category_deals,
            'brand_deals' => $brand_deals,
        );

        $this->DB->query('UPDATE deal_page set content_data = "' . $this->DB->escape(json_encode($deal_page_data)) . '" 
            WHERE deal_page_id = ' . intval($deal_page->id));
    }

    protected function getTopDeals($products, $deal_num = 5)
    {
        $parking = [];
        $id_score_mappings = [];

        foreach ($products as $product) {
            $parking[$product->id] = $product;
            $id_score_mappings[$product->id] = $product->deal_info['score'];
        }

        $top_deals = [];
        arsort($id_score_mappings, SORT_NUMERIC);
        foreach ($id_score_mappings as $k => $s) {
            $top_deals[$k] = $parking[$k];
        }

        $deals_to_output = array_splice($top_deals, 0, $deal_num);

        $top_deal_data = [];
        foreach ($deals_to_output as $deal) {
            $deal_data = new stdClass();
            $deal_data->id = $deal->id;
            $deal_data->title = $deal->title;
            $deal_data->amazon_asin = $deal->amazon_asin;
            $deal_data->deal_info = $deal->deal_info;
            $top_deal_data[] = $deal_data;
        }
        return $top_deal_data;
    }

    protected function getCategoryDeals($products, $category_ids, $max_deals = 10)
    {
        $category_data = array();

        foreach ($category_ids as $category_id) {
            if (!isset($category_data[$category_id])) {
                $category_data[$category_id] = array();
            }
            foreach ($products as $product) {
                if (count($category_data[$category_id]) < $max_deals
                    && in_array($category_id, $product->taxonomy_ids)) {
                    $category_data[$category_id][] = $product;
                }
            }
        }

        return $category_data;
    }

    protected function getBrandDeals($products, $brand_ids, $max_deals = 10)
    {
        $brand_data = array();

        foreach ($brand_ids as $brand_id) {
            if (!isset($brand_data[$brand_id])) {
                $brand_data[$brand_id] = array();
            }
            foreach ($products as $product) {
                if (count($brand_data[$brand_id]) < $max_deals
                    && in_array($brand_id, $product->brand_ids)) {
                    $brand_data[$brand_id][] = $product;
                }
            }
        }

        return $brand_data;
    }

    protected function getDealInfo($product)
    {
        $deal_info = $this->product_manager->getDealInfo($product);

        return $deal_info;
    }

    protected function display_deal($deal)
    {
        $product = $this->product_manager->getById($deal->id);

        $discount = round(((($deal->deal_info->price) - ($deal->deal_info->reduced_price)) * 100 / $deal->deal_info->price), 0);
        $discount_badge = '<span class="discount">-' . $discount . '%</span>';

        ob_start();
        ?>
        <div class="product-item text-center">
            <div class="product-item-container">
                [row]
                [col span="lg-4"]
                <?= $discount_badge ?: "" ?>
                <div class="product-thumbnail-container">
                    <a title="<?= $deal->title ?>"
                       href="<?= ($product->amazon_link) ?>" rel="nofollow" target="_blank">
                        <?= $this->product_manager->getProductImageTag($product) ?>
                    </a>
                </div>
                [/col]
                [col span="lg-8"]
                <div class="product-data">
                    <p><span class="product-title"><?= $deal->title ?></span></p>
                    <?= $this->product_manager->displayProductDataTable($product) ?>
                </div>
                <div class="button-container">
                    <?= $this->product_manager->getProductBuyButton($product, "Zum Deal") ?>
                    <?= $this->product_manager->getProductDetailButton($product) ?>
                </div>
                [/col]
                [/row]
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($content);
    }
}