<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\saastemplatecore\service\TemplateService;

class NewsletterTemplateService{
    private TemplateService $template_service;

    function __construct(){
      $this->template_service = new TemplateService();  
    }

    private function getPostsNewsletterContent($data, $for_mail=true){
        $params = array(
            'for_mail' => $for_mail,
            'data' => $data
        );
        return $this->template_service->generateMailSubTemplate('posts_newsletter_content', $params);
    }

    private function getReviewNewsletterContent($data, $for_mail=true){
        $params = array(
            'for_mail' => $for_mail,
            'data' => $data
        );
        return $this->template_service->generateMailSubTemplate('reviews_newsletter_content', $params);
    }

    private function geNewsNewsletterContent($data, $for_mail=true){
        $params = array(
            'for_mail' => $for_mail,
            'data' => $data
        );
        return $this->template_service->generateMailSubTemplate('news_newsletter_content', $params);
    }

    private function geDealNewsletterContent($data, $for_mail=true){
        $params = array(
            'for_mail' => $for_mail,
            'data' => $data
        );
        return $this->template_service->generateMailSubTemplate('deals_newsletter_content', $params);
    }

    public function getNewsletterDataHtml($newsletter_data, $for_mail=true){
        $params = array(
            'newsletter_data' => $newsletter_data,
            'posts_newsletter_content' => $this->getPostsNewsletterContent($newsletter_data->posts, $for_mail),
            'reviews_newsletter_content' => $this->getReviewNewsletterContent($newsletter_data->reviews, $for_mail),
            'news_newsletter_content' => $this->geNewsNewsletterContent($newsletter_data->news, $for_mail),
            'deals_newsletter_content' => $this->geDealNewsletterContent($newsletter_data->deals, $for_mail)
        );

        return $this->template_service->generateMailSubTemplate('personal_newsletter', $params);
    }
}