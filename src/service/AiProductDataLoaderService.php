<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

#[AllowDynamicProperties]
class AiProductDataLoaderService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
        $this->product_data_field_manager = new ProductDataFieldManager();
    }

    function displayAiLoadedProductDataSettings($product_id, $field_data, $model)
    {
        /*
        Soll Tabelle ausgeben in der Form
        Gruppe | Feld | Beschreibung | Typ | Inhalt (Input-Felder und Präfixe/Suffixe) | Aktion (Button mit "Übernehmen", der Werte übernimmt)
        */

        $product = $this->product_manager->getById($product_id);

        $product_data = [];
        foreach ($this->product_manager->getProductData($product) as $data_item) {
            $product_data[$data_item->product__data_field_id] = $data_item->value;
        }

        $field_input_mapping = [];
        foreach ($field_data as $field_id => $field_info) {
            $data_field = $this->product_data_field_manager->getById($field_id);
            $field_input_mapping[$field_info['input_name']] = [
                'data_field' => $data_field,
                'field_info' => $field_info,
                'old_value' => ArrayHelper::getArrayValue($product_data, $data_field->product__data_field_id, null),
                'loaded_value' => null,
            ];
        }

        $loaded_data = $this->loadProductDataWithAi($product_id, $field_data, $model);
        foreach ($loaded_data as $key => $value) {
            $field_input_mapping[$key]['loaded_value'] = $value;
        }

        $rows = [];

        $column_labels = ['Feld-Name', 'Beschreibung', 'Datentyp', 'Bisheriger Wert', 'Wert von KI', 'Aktionen'];
        foreach ($field_input_mapping as $input_name => $data) {
            $data_field = $data['data_field'];
            $old_value = $data['old_value'];
            $field_info = $data['field_info'];
            $loaded_value = $data['loaded_value'];

            $new_input_name = 'ai_loaded_' . $input_name;

            switch ($data_field->field_type) {
                case "enum":
                    $allowed_values = [];
                    foreach (array_unique(array_merge([$loaded_value], array_map('trim', explode(',', $data_field->field_values)))) as $value) {
                        $allowed_values[$value] = $value;
                    }

                    $input = new TextInput($new_input_name, $data_field->field_name, null, $loaded_value);
                    break;
                case "boolean":
                    $allowed_values = [
                        null => 'Kein Wert',
                        '0' => 'Nein',
                        '1' => 'Ja'
                    ];
                    $input = new Select($new_input_name, $data_field->field_name, $allowed_values, $loaded_value);
                    break;
                case "string":
                case "float":
                default:
                    $input = new TextInput($new_input_name, $data_field->field_name, '', $loaded_value);
                    break;
            }

            $cells = [
                $data_field->field_name,
                $data_field->field_description,
                $data_field->field_type,
                $old_value,
                $input,
                new JsButton('Daten übernehmen', "applyAiLoadedProductData(this, {$product_id}, '{$input->getId()}', '$input_name')")
            ];
            if ($loaded_value && strval($loaded_value) !== strval($old_value)) {
                $rows[] = new TableRow($cells);
            }
        }

        if (count($rows)) {
            (new Text('Andere Werte gefunden', 'label'))->display();

            (new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

            $buy_buttons = [];

            foreach($product->product_links as $product_link){
                $buy_buttons[] = new LinkButton($product_link->url, $product_link->product_shop_title, null, null, true);
            }

            if(count($buy_buttons)) {
                BreakComponent::break();
                (new FloatContainer($buy_buttons))->display();
            }

            BreakComponent::break();
        } else {
            echo "Keine neuen Produktdaten gefunden.";
        }
    }

    private function loadProductDataWithAi($product_id, $field_data, $model)
    {
        $prompt = $this->generatePrompt($product_id, $field_data);
        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, $model, intval(ChatGenerationTemplate::getMaxTokensForModel($model)/2));
        $result = $this->text_generation_service->getNextChatMessage($template);
        $result = $result->getContent();

        $content = 'Prompt: ' . PHP_EOL . $prompt . PHP_EOL . PHP_EOL . 'Antwort:' . PHP_EOL . $result;

        LogHelper::logToMinuteLog("OpenAI", $content, LogHelper::LOG_LEVEL_INFO);

        return json_decode($result, true);
    }

    private function generatePrompt($product_id, $field_data)
    {
        $product_info = unserialize($this->product_manager->getMeta($product_id, 'product_info'));

        $product_info_string = '- ' . $product_info['Title']['DisplayValue'];

        foreach ($product_info['Features']['DisplayValues'] as $info_line) {
            $product_info_string .= '- ' . $info_line . PHP_EOL;
        }

        $json_config = "Das JSON-Objekt soll folgende Felder enthalten:" . PHP_EOL;

        foreach ($field_data as $field_id => $field_info) {
            $data_field = $this->product_data_field_manager->getById($field_id);

            $json_item_description = "Name des JSON-Attributes: " . $field_info['input_name'];
            $json_item_description .= ", (Beschreibung: {$data_field->field_name}";
            if (strlen($data_field->field_description)) {
                $json_item_description .= " - {$data_field->field_description}";
            }
            $json_item_description .= ")";
            $json_item_description .= ": " . ($data_field->field_type === "enum" ? "string" : $data_field->field_type) . "";

            $json_config .= $json_item_description . PHP_EOL;
        }

        return 'Erstelle ein JSON-Objekt mit Produktdaten aus folgenden Informationen:' . PHP_EOL
            . $product_info_string . PHP_EOL
            . $json_config . PHP_EOL
            . 'Wenn du ein Feld nicht mit korrekten Werten füllen kannst, so setze den Wert auf "null"!' . PHP_EOL
            . 'Gib außer dem validen JSON-Code keinen weiteren Inhalt zurück!';
    }
}