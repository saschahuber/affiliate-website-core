<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\builder\ImageBuilder;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\service\Service;

class ImageGeneratorServiceNeu extends Service
{

    function __construct()
    {
        parent::__construct();
    }

    function generateImageFromParams($width, $height, $background_color, $layers, $save_path)
    {
        $allowed_layers = [
            'withAttachmentImage', 'withFont', 'withBackgroundImage', 'withAlignedImage', 'withWatermarkLogo', 'withFooterWatermark',
            'withCenteredText', 'withAlignedText', 'withTint', 'withFrame', 'withRibbon', 'withPolygon', 'withBox'
        ];

        $image_builder = new ImageBuilder($width, $height, $background_color);

        foreach ($layers as $layer) {
            if (!in_array($layer['name'], $allowed_layers)) {
                ErrorHelper::http(403, "Layer " . $layer['name'] . " nicht erlaubt");
            }
            $image_builder = $this->applyLayer($image_builder, $layer['name'], $layer['params']);
        }

        if ($save_path === null || strlen($save_path) < 1) {
            FileHelper::createDirIfNotExists(MEDIA_DIR . '/generated');
            $save_path = GENERATED_IMG_DIR . md5(json_encode([$width, $height, $background_color, $layers])) . '.png';
        }

        return $image_builder->saveImage($save_path);
    }

    function applyLayer($image_builder, $layer, $params)
    {
        return call_user_func_array(array($image_builder, $layer), $params);
    }
}