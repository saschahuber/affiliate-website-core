<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\service\Service;

class AdService extends Service {
    public function __construct()
    {
        parent::__construct();
        $this->ad_manager = new AdManager();
    }

    public function getFittingAds($position, $content_type, $content_id){
        $ad_items = $this->getAdsByPositionAndContentItem($position, $content_type, $content_id);
        return $ad_items;
    }

    public function getAdsByPositionAndContentItem($position, $content_type, $content_id){
        switch($content_type){
            case PostManager::TYPE_POST:
                return $this->getPostAdsByPosition($position, $content_id);
            case PostManager::TYPE_POST_CATEGORY:
                return $this->getPostTaxonomyAdsByPosition($position, $content_id);
            case NewsManager::TYPE_NEWS:
                return $this->getNewsAdsByPosition($position, $content_id);
            case NewsManager::TYPE_NEWS_CATEGORY:
                return $this->getNewsTaxonomyAdsByPosition($position, $content_id);
            case ProductManager::TYPE_PRODUCT:
                return $this->getProductAdsByPosition($position, $content_id);
            case ProductManager::TYPE_PRODUCT_CATEGORY:
                return $this->getProductTaxonomyAdsByPosition($position, $content_id);
            default:
                return [];
        }
    }

    public function getPostAdsByPosition($position, $content_id){
        $post_manager = new PostManager();
        $post = $post_manager->getById($content_id, true);

        return [$post_manager->findAd($position, $post->content, $post->taxonomies)];
    }

    public function getPostTaxonomyAdsByPosition($position, $content_id){
        $post_manager = new PostManager();

        $category_content = null;
        $category = null;
        if($content_id) {
            $category = $post_manager->getTaxonomyById($content_id, true);
            $category_content = $category->content;
        }

        return [$post_manager->findAd($position, $category_content, [$category])];
    }

    public function getNewsAdsByPosition($position, $content_id){
        $news_manager = new NewsManager();
        $news = $news_manager->getById($content_id, true);

        return [$news_manager->findAd($position, $news->content, $news->taxonomies)];
    }

    public function getNewsTaxonomyAdsByPosition($position, $content_id){
        $news_manager = new NewsManager();

        $category_content = null;
        $category = null;
        if($content_id) {
            $category = $news_manager->getTaxonomyById($content_id, true);
            $category_content = $category->content;
        }

        return [$news_manager->findAd($position, $category_content, [$category])];
    }

    public function getProductAdsByPosition($position, $content_id){
        $product_manager = new ProductManager();
        $product = $product_manager->getById($content_id, true);

        return [$product_manager->findAd($position, $product->content, $product->taxonomies)];
    }

    public function getProductTaxonomyAdsByPosition($position, $content_id){
        $product_manager = new ProductManager();

        $category_content = null;
        $category = null;
        if($content_id) {
            $category = $product_manager->getTaxonomyById($content_id, true);
            $category_content = $category->content;
        }

        return [$product_manager->findAd($position, $category_content, [$category])];
    }
}