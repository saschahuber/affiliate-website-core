<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\service\Service;

class LinkedContentScheduleItemSelectorService extends Service {
    public function __construct(){
        parent::__construct();
    }

    public static function getLinkedContentScheduleItemSelector($element_type, $element_id){
        $content_schedule_item_service = new ContentScheduleItemService();

        $content_schedule_item = $content_schedule_item_service->getByLinkedElementTypeAndId($element_type, $element_id);

        $available_items = $content_schedule_item_service->getNotYetLinked();

        $allowed_values = [null => '-- Kein Content-Item gewählt --'];

        if($content_schedule_item){
            $allowed_values[$content_schedule_item->id] = $content_schedule_item->title;
        }

        foreach($available_items as $item){
            $allowed_values[$item->id] = $item->title;
        }

        return new Select('linked_content_schedule_item_id', 'Verknüpftes Content-Plan-Element', $allowed_values, $content_schedule_item?$content_schedule_item->id:null);
    }
}