<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\service\AsyncHandlerService;

#[AllowDynamicProperties]
class AffiliateAsyncHandlerService extends AsyncHandlerService {
    public function __construct(){
        parent::__construct();
    }

    public function handle($handler_name, $params){
        if(!$this->isAllowedHandler($handler_name)){
            ErrorHelper::http(ErrorHelper::HTTP_ERROR_NO_PERMISSION, "Unerlaubter Async-Handler: ".$handler_name);
        }

        $result = null;

        try {
            if(class_exists('saschahuber\\affiliatewebsitecore\\async_handler\\' . $handler_name)){
                $handler_type = 'saschahuber\\affiliatewebsitecore\\async_handler\\' . $handler_name;
            }
            else if(class_exists('saschahuber\\saastemplatecore\\async_handler\\' . $handler_name)){
                $handler_type = 'saschahuber\\saastemplatecore\\async_handler\\' . $handler_name;
            }
            else {
                $handler_type = $handler_name;
            }

            $handler = new $handler_type($params);
            $result = $handler->getResult();
        }
        catch (Exception $e){
            http_response_code(404);
        }

        return $result;
    }
}
