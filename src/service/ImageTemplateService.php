<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\ArrayHelper;

#[AllowDynamicProperties]
class ImageTemplateService extends \saschahuber\saastemplatecore\service\ImageTemplateService
{
    const INSTAGRAM_POST = "INSTAGRAM_POST";
    const PINTEREST_POST = "PINTEREST_POST";
    const BLOG_THUMBNAIL = "BLOG_THUMBNAIL";
    const COMPANY_THUMBNAIL = "COMPANY_THUMBNAIL";
    const NEWS_THUMBNAIL = "NEWS_THUMBNAIL";
    const DEAL_THUMBNAIL = "DEAL_THUMBNAIL";
    const SEARCH_THUMBNAIL = "SEARCH_THUMBNAIL";
    const PRODUCT_THUMBNAIL = "PRODUCT_THUMBNAIL";
    const OG_IMAGE = "OG_IMAGE";
    const ATTACHMENT = "ATTACHMENT";

    const BACKGROUND_ALEXA_OLD = BACKGROUND_ASSETS_DIR . 'alexa.jpg';

    public function __construct()
    {
        parent::__construct();
    }

    public function generateTemplate($template, $text, $background_image = null, $params = null)
    {
        switch ($template) {
            case ImageTemplateService::INSTAGRAM_POST:
                return $this->generateInstagramImage($text, $background_image, $params);
            case ImageTemplateService::PINTEREST_POST:
                return $this->generatePinterestImage($text, $background_image, $params);
            case ImageTemplateService::BLOG_THUMBNAIL:
                return $this->generateBlogThumbnail($text, $background_image, $params);
            case ImageTemplateService::COMPANY_THUMBNAIL:
                return $this->generateCompanyThumbnail($text, $background_image, $params);
            case ImageTemplateService::NEWS_THUMBNAIL:
                return $this->generateNewsThumbnail($text, $background_image, $params);
            case ImageTemplateService::DEAL_THUMBNAIL:
                return $this->generateDealThumbnail($text, $background_image, $params);
            case ImageTemplateService::SEARCH_THUMBNAIL:
                return $this->generateSearchThumbnail($text, $background_image, $params);
            case ImageTemplateService::PRODUCT_THUMBNAIL:
                return $this->generateProductThumbnail($text, $background_image, $params);
            case ImageTemplateService::OG_IMAGE:
                return $this->generateOgImage($text, $background_image, $params);
            case ImageTemplateService::ATTACHMENT:
                return $this->generateAttachment($text, $background_image, $params);
            default:
                return $this->generateDefaultImage($text, $background_image, $params);
        }
    }

    public function generateDefaultImage($text, $background_image = null, $params = null)
    {
        global $CONFIG;

        $width = 1920;
        $height = 1080;

        $image_generator = new ImageGeneratorService($width, $height);

        if ($background_image !== null) {
            $image_generator->backgroundImage($background_image);
        }

        $image_generator->tint("#000000", 0.2);

        $image_generator->addWatermarkLogo();

        $image_generator->centeredText($image_generator->formatText($text), 160, 120, 6,
            "#FFFFFF", 160);
        return $image_generator->getGeneratedImage();
    }

    public function generateOgImage($text, $background_image = null, $params = null)
    {
        $width = 1200;
        $height = 630;

        $image_generator = new ImageGeneratorService($width, $height);

        if ($background_image !== null) {
            $image_generator->backgroundImage($background_image);
        }

        $image_generator->tint("#000000", 0.2);

        $image_generator->addWatermarkLogo();

        $image_generator->centeredText($image_generator->formatText($text), 160, 120, 6,
            "#FFFFFF", 160);
        return $image_generator->getGeneratedImage();
    }

    public function generateSearchThumbnail($text, $background_image = null, $params = null)
    {
        $width = 480;
        $height = 270;

        $image_generator = new ImageGeneratorService($width, $height);

        if ($background_image !== null) {
            $image_generator->backgroundImage($background_image);
        }

        $image_generator->tint("#000000", 0.1);

        $image_generator->addWatermarkLogo();

        $image_generator->centeredText($image_generator->formatText($text), 32, 120, 6,
            "#FFFFFF", 50);
        return $image_generator->getGeneratedImage();
    }

    public function generateProductThumbnail($text, $background_image = null, $params = null)
    {
        $width = 300;
        $height = 300;

        $image_generator = new ImageGeneratorService($width, $height, "#FFFFFF");

        if ($background_image !== null) {
            $image_generator->backgroundImage($background_image);
        }

        #$image_generator->tint("#000000", 0.1);

        $image_generator->addWatermarkLogo();

        $image_generator->centeredText($image_generator->formatText($text), 50, 40, 3,
            "#666666", 50);
        return $image_generator->getGeneratedImage();
    }

    public function generateInstagramImage($text, $background_image = null, $params = null)
    {
        global $CONFIG;

        $width = 1080;
        $height = 1080;

        $image_generator = new ImageGeneratorService($width, $height);

        $text_color = "#666666";

        if ($background_image !== null) {
            $image_overlay_opacity = ArrayHelper::getArrayValue($params, 'image-overlay-opacity', 0.55);
            $image_generator->backgroundImage($background_image);
            $image_generator->tint("#000000", $image_overlay_opacity);
            $text_color = "#FFFFFF";
        }

        $image_generator->addWatermarkLogo(false, ImageGeneratorService::TopCenter, 0.2);

        $vertical_text_offset = ArrayHelper::getArrayValue($params, 'vertical-text-offset', 40);

        $image_generator->centeredText($image_generator->formatText($text, 30), 64, 512, 12,
            $text_color, $vertical_text_offset);

        $image_generator->addFooterWatermark($CONFIG->footer_watermark, ($background_image !== null) ? '#ffffff' : null);
        return $image_generator->getGeneratedImage();
    }

    public function generatePinterestImage($text, $background_image = null, $params = null)
    {
        global $CONFIG;

        $width = 1000;
        $height = 1500;

        $image_generator = new ImageGeneratorService($width, $height);

        $text_color = "#666666";

        if ($background_image !== null) {
            $image_overlay_opacity = ArrayHelper::getArrayValue($params, 'image-overlay-opacity', 0.55);
            $image_generator->backgroundImage($background_image);
            $image_generator->tint("#000000", $image_overlay_opacity);
            $text_color = "#FFFFFF";
        }

        $image_generator->addWatermarkLogo(false, ImageGeneratorService::TopCenter, 0.2);

        $vertical_text_offset = ArrayHelper::getArrayValue($params, 'vertical-text-offset', 40);

        $image_generator->centeredText($image_generator->formatText($text, 30), 64, 512, 12,
            $text_color, $vertical_text_offset);

        #$image_generator->addFooterWatermark($CONFIG->footer_watermark, ($background_image !== null) ? '#ffffff' : null);
        return $image_generator->getGeneratedImage();
    }

    public function generateBlogThumbnail($text, $background_image = null, $params = null)
    {
        $width = 1920;
        $height = 1080;

        $image_generator = new ImageGeneratorService($width, $height, "#FFFFFF");

        $text_color = "#666666";

        if ($background_image !== null) {
            $image_generator->backgroundImage($background_image);
            $image_generator->tint("#000000", 0.2);
            $text_color = "#FFFFFF";
        }

        $image_generator->addWatermarkLogo(100);

        $image_generator->centeredText($image_generator->formatText($text), 160, 120, 4,
            $text_color, 300, 1.3);
        return $image_generator->getGeneratedImage();
    }

    public function generateCompanyThumbnail($text, $background_image = null, $params = null)
    {
        $width = 1920;
        $height = 1080;

        $image_generator = new ImageGeneratorService($width, $height, "#FFFFFF");

        $text_color = "#666666";

        if ($background_image !== null) {
            $image_generator->backgroundImage($background_image);
            $image_generator->tint("#000000", 0.2);
            $text_color = "#FFFFFF";
        }

        $image_generator->addWatermarkLogo(100);

        $image_generator->centeredText($image_generator->formatText($text), 160, 120, 4,
            $text_color, 300, 1.3);
        return $image_generator->getGeneratedImage();
    }

    public function generateNewsThumbnail($text, $background_image = null, $params = null)
    {
        $width = 1920;
        $height = 1080;

        $image_generator = new ImageGeneratorService($width, $height, "#FFFFFF");

        $text_color = "#666666";

        if ($background_image !== null) {
            $image_generator->backgroundImage($background_image);
            $image_generator->tint("#000000", 0.2);
            $text_color = "#FFFFFF";
        }


        $image_generator->addWatermarkLogo(50);

        $image_generator->centeredText($image_generator->formatText($text), 160, 160, 3,
            $text_color, 300, 1.3);
        return $image_generator->getGeneratedImage();
    }

    public function generateDealThumbnail($text, $background_image = null, $params = null)
    {
        $width = 1920;
        $height = 1080;

        $image_generator = new ImageGeneratorService($width, $height, "#FFFFFF");

        $text_color = "#666666";

        if ($background_image !== null) {
            $image_generator->backgroundImage($background_image);
            $image_generator->tint("#000000", 0.2);
            $text_color = "#FFFFFF";
        }


        $image_generator->addWatermarkLogo(50);

        $image_generator->centeredText($image_generator->formatText($text), 160, 160, 3,
            $text_color, 300, 1.3);
        return $image_generator->getGeneratedImage();
    }

    public function generateAttachment($text, $background_image = null, $params = null)
    {
        #die(json_encode($params));

        $width = 1920;
        $height = 1080;

        $image_generator = new ImageGeneratorService($width, $height, "#FFFFFF");

        $text_color = "#666666";

        if ($background_image !== null) {
            $image_overlay_opacity = ArrayHelper::getArrayValue($params, 'image-overlay-opacity', 0.55);
            $image_generator->backgroundImage($background_image);
            $image_generator->tint("#000000", $image_overlay_opacity);
            $text_color = "#FFFFFF";
        }

        if (ArrayHelper::getArrayValue($params, 'show_logo') === true) {
            $image_generator->addWatermarkLogo(ArrayHelper::getArrayValue($params, 'vertical-logo-offset', 0));
        }

        if (ArrayHelper::getArrayValue($params, 'show_logo_as_watermark') === true) {
            $image_generator->addWatermarkLogo(ArrayHelper::getArrayValue($params, 'watermak_offset', 0), ImageGeneratorService::Center);
            #$image_generator->tint("#000000", 0.2);
        }

        $image_generator->centeredText($image_generator->formatText($text), 160, 160, 3,
            $text_color, 300 + ArrayHelper::getArrayValue($params, 'vertical-text-offset', 0), 1.3);
        return $image_generator->getGeneratedImage();
    }
}