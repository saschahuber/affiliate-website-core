<?php

namespace saschahuber\affiliatewebsitecore\service;

use stdClass;

class CompanyService{
    function __construct(){
        global $DB, $USER_AUTH_SERVICE;
        $this->DB = $DB;
        $this->user_auth_service = $USER_AUTH_SERVICE;
    }

    function create_company($customer_id, $company_name){
        $customer_company = new stdClass();
        $customer_company->customer_id = $customer_id;
        $customer_company->title = $company_name;
        $customer_company->status = "draft";
        $customer_company->id = $this->DB->insertFromObject('customer__company', $customer_company);
    }

    function create_company_address($customer_company_id, $street_name, $house_number, $zipcode, $city, $country){
        $customer_company_address = new stdClass();
        $customer_company_address->company_id = $customer_company_id;
        $customer_company_address->street_name = $street_name;
        $customer_company_address->house_number = $house_number;
        $customer_company_address->zipcode = $zipcode;
        $customer_company_address->city = $city;
        $customer_company_address->country = $country;

        $customer_company_address->id = $this->DB->insertFromObject('customer__company_address', $customer_company_address);
        return $customer_company_address;
    }

    function create_company_contact($customer_company_id, $salutation, $first_name, $last_name,
                                    $mail=null, $phone=null, $website=null){
        $customer_company_contact = new stdClass();
        $customer_company_contact->company_id = $customer_company_id;
        $customer_company_contact->salutation = $salutation;
        $customer_company_contact->first_name = $first_name;
        $customer_company_contact->last_name = $last_name;
        $customer_company_contact->mail = $mail;
        $customer_company_contact->phone = $phone;
        $customer_company_contact->website = $website;

        $customer_company_contact->id = $this->DB->insertFromObject('customer__company_contact', $customer_company_contact);
        return $customer_company_contact;
    }
}