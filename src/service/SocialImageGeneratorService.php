<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\manager\FileManager;

define("INSTAGRAM_IMAGE_DIR", IMG_DIR . '/assets/instagram');

#[AllowDynamicProperties]
class SocialImageGeneratorService
{
    function __construct()
    {
        $this->image_service = new AffiliateImageService();
        $this->file_manager = new FileManager();
        $this->img_utils = new ImgUtils();
    }

    function getImageTemplates($app_type)
    {
        global $CONFIG;

        switch ($app_type) {
            case "instagram":
            case "facebook":
            case "pinterest":
            case "image":
                $templates = array(
                    'instagram-1' => array('title' => "{$CONFIG->app_name} Logo", 'background_image' => null,
                        'template' => ImageTemplateService::INSTAGRAM_POST)
                );
                foreach (FileHelper::getFiles(INSTAGRAM_IMAGE_DIR) as $file) {
                    $templates['hintergrund-' . UrlHelper::alias($file)] = array('title' => "Hintergrund: $file",
                        'background_image' => INSTAGRAM_IMAGE_DIR . '/' . $file,
                        'template' => ImageTemplateService::INSTAGRAM_POST);
                }

                # TODO: Hier stock images hinzufügen?

                return $templates;
        }
        return [];
    }

    function getTemplateData($app_type, $template_id)
    {
        $templates = $this->getImageTemplates($app_type);

        return ArrayHelper::getArrayValue($templates, $template_id, $this->getFallbackTemplate($app_type));
    }

    function getFallbackTemplate($app_type)
    {

        switch ($app_type) {
            case 'instagram':
            case 'image':
                return array('title' => "Fallback Template", 'background_image' => null,
                    'template' => ImageTemplateService::INSTAGRAM_POST);
            default:
                return null;
        }
    }

    function getImageTemplateOptions($app_type)
    {
        $templates = $this->getImageTemplates($app_type);
        $output_templates = [];
        foreach ($templates as $key => $template) {
            $output_templates[strval($key)] = $template['title'];
        }
        return $output_templates;
    }

    function downloadBackgroundImage($background_image_url)
    {
        return $this->file_manager->downloadTmpImage($background_image_url);
    }

    function generateSocialImage($app_type, $template_id, $text, $sub_dir = null, $params = null)
    {
        global $CONFIG;

        $template_data = $this->getTemplateData($app_type, $template_id);

        $name_prefix = substr(md5(time()), 0, 7);

        $background_image_path = ArrayHelper::getArrayValue($template_data, 'background_image');
        if (ArrayHelper::getArrayValue($params, 'image-background-url') && $params['image-background-url'] !== "null") {
            $image_url = ArrayHelper::getArrayValue($params, 'image-background-url');

            /*
             * TODO: Sauber integrieren je nach Bildgrößen => aktuell nur für Instagram genutzt
             * Instagram Abmessungen => 1080 x 1080
             */
            $tmp_path = $this->downloadBackgroundImage($image_url);
            $background_image = $this->img_utils->getRatioAlignedImageFromSrc($tmp_path, 1080, 1080);
            $background_image_path = $this->img_utils->storeImgFile($background_image, $tmp_path);
        }

        return $CONFIG->website_domain . $this->image_service->generateImage($template_data['template'], $text,
                $background_image_path, $sub_dir, $name_prefix, $params);
    }

    function generateInstagramImageWithCustomBackground($background_url, $text, $sub_dir = null, $params = null)
    {
        global $CONFIG;
        $name_prefix = substr(md5(time()), 0, 7);
        return $CONFIG->website_domain . $this->image_service->generateImage(ImageTemplateService::INSTAGRAM_POST, $text,
                $background_url, $sub_dir, $name_prefix, $params);
    }
}