<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class FavouriteService extends Service
{
    public function __construct()
    {
        parent::__construct();
    }

    public function toggleFavourite($user_id, $fav_type, $fav_id){
        $exists = $this->DB->query('SELECT count(*) as anzahl from user__favourite where user_id = '.intval($user_id). ' 
            and favourite_element_type = "'.$this->DB->escape($fav_type).'" 
            and favourite_element_id = ' . intval($fav_id))->fetchObject()->anzahl > 0;

        if($exists){
            $this->DB->query('DELETE FROM user__favourite where user_id = '.intval($user_id).' and favourite_element_type = "'.$this->DB->escape($fav_type).'" and favourite_element_id = ' . intval($fav_id));
        }
        else {
            $user_favourite = new \stdClass();
            $user_favourite->user_id = $user_id;
            $user_favourite->favourite_element_type = $fav_type;
            $user_favourite->favourite_element_id = $fav_id;
            $this->DB->insertFromObject('user__favourite', $user_favourite);
        }
    }
}