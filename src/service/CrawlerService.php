<?php

namespace saschahuber\affiliatewebsitecore\service;

use Masterminds\HTML5;
use saschahuber\affiliatewebsitecore\persistence\CrawlUrlRepository;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\service\Service;

class CrawlerService extends Service
{
    function __construct(){
        parent::__construct();
    }

    public function crawlUrl($url)
    {
        global $CONFIG;
        
        list($responseCode, $redirectUrl) = $this->getHttpResponseCode($url);

        if ($responseCode !== 200) {
            $redirectUrlData = null;

            if($redirectUrl){
                $absolute_redirect_url = $this->createAbsoluteUrl($redirectUrl, $CONFIG->website_domain);
                $redirectUrlData = [
                    'type' => $this->determineFileType($absolute_redirect_url),
                    'url' => $absolute_redirect_url
                ];
            }

            return [
                'status' => $responseCode,
                #'content' => null,
                'redirect_url' => $redirectUrlData,
                'internal_links' => []
            ];
        }

        $html = $this->fetchHtml($url);
        $internalLinks = $this->extractInternalLinks($html, $CONFIG->website_domain);

        return [
            'status' => 200,
            'redirect_url' => null,
            #'content' => $html,
            'internal_links' => $internalLinks
        ];
    }

    private function getHttpResponseCode($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

        $response = curl_exec($ch);
        $redirectUrl = null;
        if (preg_match('/^Location:\s*(.*)$/mi', $response, $matches)) {
            $redirectUrl = trim($matches[1]);
        }
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return [$httpCode, $redirectUrl];
    }

    private function fetchHtml($url)
    {
        $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        curl_close( $ch );
        return $content;
    }

    private function extractInternalLinks($html, $domain)
    {
        $html5 = new HTML5();
        $dom = @$html5->loadHTML($html);

        $links = [];
        $elements = $dom->getElementsByTagName('a');

        foreach ($elements as $element) {
            $href = $element->getAttribute('href');
            $absolute_url = $this->createAbsoluteUrl($href, $domain);
            
            if ($this->isInternalLink($absolute_url, $domain)) {
                $links[] = $absolute_url;
            }
        }

        $imgElements = $dom->getElementsByTagName('img');
        foreach ($imgElements as $img) {
            $src = $img->getAttribute('src');
            $absolute_url = $this->createAbsoluteUrl($src, $domain);
            
            if ($this->isInternalLink($absolute_url, $domain)) {
                $links[] = $absolute_url;
            }
        }

        $unique_links = [];
        foreach(array_unique($links) as $link){
            $unique_links[] = [
                'type' => $this->determineFileType($link),
                'url' => $link
            ];
        }
        
        return $unique_links;
    }

    private function isInternalLink($url, $domain)
    {
        $parsedUrl = parse_url($url);
        if (isset($parsedUrl['host'])) {
            return $parsedUrl['host'] === $domain;
        }
        return true;
    }

    private function createAbsoluteUrl($src, $domain)
    {
        $src = trim($src, '/');
        
        if (filter_var($src, FILTER_VALIDATE_URL)) {
            return $src;
        }

        $absoluteUrl = trim(trim($domain, '/') . '/' . $src, '/');

        $parsed_url = parse_url($absoluteUrl);

        return $parsed_url['scheme'] . "://" . $parsed_url['host'] . ArrayHelper::getArrayValue($parsed_url, 'path', '');
    }

    private function determineFileType($url)
    {
        // Initialisiere cURL, um die Datei anzufordern
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Erlaube Weiterleitungen
        curl_setopt($ch, CURLOPT_NOBODY, true); // Wir wollen nur den Header und nicht den Inhalt
        curl_exec($ch);

        // Holen des Content-Type aus der Antwort
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        curl_close($ch);

        if ($contentType) {
            // Bestimmen des Dateityps anhand des Content-Type
            if (strpos($contentType, 'text/html') !== false) {
                return CrawlUrlRepository::URL_TYPE_WEBSITE;
            } elseif (strpos($contentType, 'application/javascript') !== false || strpos($contentType, 'text/javascript') !== false) {
                return CrawlUrlRepository::URL_TYPE_JAVASCRIPT;
            } elseif (strpos($contentType, 'text/css') !== false) {
                return CrawlUrlRepository::URL_TYPE_CSS;
            } elseif (strpos($contentType, 'image/') !== false) {
                return CrawlUrlRepository::URL_TYPE_IMAGE;
            } elseif (strpos($contentType, 'video/') !== false) {
                return CrawlUrlRepository::URL_TYPE_VIDEO;
            }
        }
        return CrawlUrlRepository::URL_TYPE_UNKNOWN;
    }
}