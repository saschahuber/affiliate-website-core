<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\manager\PostableManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\helper\CacheHelper;
use saschahuber\saastemplatecore\service\Service;
use stdClass;

class ImprintCopyrightService extends Service
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getCopyrightInfoHtml()
    {
        $copyright_data = CacheHelper::loadCache("copyright_info", "copyright_info");

        if (!$copyright_data) {
            $copyright_data = $this->buildCopyrightInfoHtml();
            CacheHelper::saveCache("copyright_info", "copyright_info", $copyright_data);
        }

        return $copyright_data;
    }

    public function refreshElementCopyright($element_type, $element_id){
        $post_manager = new PostManager();
        $news_manager = new NewsManager();
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $brand_manager = new BrandManager();
        $page_manager = new PageManager();

        $this->DB->query("DELETE FROM imprint_copyright_item 
           where element_type = '".$this->DB->escape($element_type)."' 
           and element_id = ".intval($element_id).";");

        $copyright_items = [];
        switch($element_type){
            case PostManager::TYPE_POST:
                $copyright_items = $this->getBlogItemAttachmentsCopyrightInfo($post_manager->getById($element_id));
                break;
            case PostManager::TYPE_POST_CATEGORY:
                $copyright_items = $this->getBlogCategoryItemAttachmentsCopyrightInfo($post_manager->getTaxonomyById($element_id));
                break;
            case ProductManager::TYPE_PRODUCT:
                $copyright_items = $this->getProductItemAttachmentsCopyrightInfo($product_manager->getById($element_id));
                break;
            case ProductManager::TYPE_PRODUCT_CATEGORY:
                $copyright_items = $this->getProductCategoryItemAttachmentsCopyrightInfo($product_manager->getTaxonomyById($element_id));
                break;
            case NewsManager::TYPE_NEWS:
                $copyright_items = $this->getNewsItemAttachmentsCopyrightInfo($news_manager->getById($element_id));
                break;
            case NewsManager::TYPE_NEWS_CATEGORY:
                $copyright_items = $this->getNewsCategoryItemAttachmentsCopyrightInfo($news_manager->getTaxonomyById($element_id));
                break;
            case BrandManager::TYPE_BRAND:
                $copyright_items = $this->getAttachmentCopyrightInfoForTypeItem($brand_manager, $brand_manager->getById($element_id), BrandManager::TYPE_BRAND);
                break;
            case PageManager::TYPE_PAGE:
                $copyright_items = $this->getAttachmentCopyrightInfoForTypeItem($page_manager, $page_manager->getById($element_id), PageManager::TYPE_PAGE);
                break;
        }

        $this->refreshCopyrightInfoItems([$copyright_items]);
    }

    public function refreshAllCopyrightInfoItems(){
        $copyright_infos = array_filter(array_merge(
            $this->getBlogAttachmentsCopyrightInfo(),
            $this->getBlogCategoryAttachmentsCopyrightInfo(),
            $this->getNewsAttachmentsCopyrightInfo(),
            $this->getNewsCategoryAttachmentsCopyrightInfo(),
            $this->getProductAttachmentsCopyrightInfo(),
            $this->getProductCategoryAttachmentsCopyrightInfo(),
            $this->getBrandsAttachmentsCopyrightInfo(),
            $this->getPageAttachmentsCopyrightInfo()
        ));

        $this->refreshCopyrightInfoItems($copyright_infos);
    }

    public function refreshCopyrightInfoItems($copyright_infos){
        foreach ($copyright_infos as $copyright_info) {
            if(!$copyright_info){
                continue;
            }

            if(array_key_exists('copyright_info_items', $copyright_info)) {
                foreach ($copyright_info['copyright_info_items'] as $copyright_info_item) {
                    if (strlen($copyright_info_item['copyright_info']) < 1) {
                        continue;
                    }

                    $this->updateCopyrightInfoItem($copyright_info, $copyright_info_item);
                }
            }
        }
    }

    public function updateCopyrightInfoItem($copyright_info, $copyright_info_item){
        $imprint_copyright_item = new stdClass();
        $imprint_copyright_item->element_type = $copyright_info['element_type'];
        $imprint_copyright_item->element_id = $copyright_info['element_id'];
        $imprint_copyright_item->element_title = $copyright_info['element_title'];
        $imprint_copyright_item->element_permalink = $copyright_info['element_permalink'];
        $imprint_copyright_item->attachment_type = $copyright_info_item['attachment_type'];
        $imprint_copyright_item->attachment_id = $copyright_info_item['attachment_id'];
        $imprint_copyright_item->description = $copyright_info_item['description'];
        $imprint_copyright_item->provider_name = $copyright_info_item['provider_name'];
        $imprint_copyright_item->copyright_info = $copyright_info_item['copyright_info'];

        $this->DB->replaceFromObject('imprint_copyright_item', $imprint_copyright_item);
    }

    public function buildCopyrightInfoHtml()
    {
        $imprint_copyright_items = $this->DB->getAll("SELECT * FROM imprint_copyright_item ORDER BY provider_name ASC, element_type ASC, element_id DESC");

        $element_type_string_mappings = getContentTypes();

        $copyright_info_groups = [];

        foreach($imprint_copyright_items as $imprint_copyright_item){
            if (!array_key_exists($imprint_copyright_item->provider_name, $copyright_info_groups)) {
                $copyright_info_groups[$imprint_copyright_item->provider_name] = [];
            }

            if (array_key_exists($imprint_copyright_item->element_type, $element_type_string_mappings)) {
                $element_type_string = $element_type_string_mappings[$imprint_copyright_item->element_type];
            }

            $copyright_html = '<a href="' . $imprint_copyright_item->element_permalink . '"> '
                . ($element_type_string ? ' ' . $element_type_string . ": '{$imprint_copyright_item->element_title}' " : $imprint_copyright_item->element_title)
                . '</a>: ' . $imprint_copyright_item->description
                . ' © ' . $imprint_copyright_item->copyright_info;

            $copyright_info_groups[$imprint_copyright_item->provider_name][] = $copyright_html;
        }

        $html_lines = [];
        foreach ($copyright_info_groups as $provider_name => $copyright_items) {
            if (count($copyright_items) < 1) {
                continue;
            }

            $html_lines[] = "<h2>$provider_name Bildquellen:</h2>";
            foreach ($copyright_items as $copyright_item) {
                $html_lines[] = "<p>$copyright_item</p>";
            }
        }

        return implode('', $html_lines);
    }

    public function getBlogAttachmentsCopyrightInfo()
    {
        $copyright_infos = [];

        $post_manager = new PostManager();

        foreach ($post_manager->getAll(PostableManager::STATUS_PUBLISH) as $item) {
            $copyright_info = $this->getBlogItemAttachmentsCopyrightInfo($item);;

            if($copyright_info){
                $copyright_infos[] = $copyright_info;
            }
        }

        return $copyright_infos;
    }

    public function getBlogItemAttachmentsCopyrightInfo($item)
    {
        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();

        $post_manager = new PostManager();

        //Content
        $content_html = ShortcodeHelper::doShortcode($item->content);
        $item_copyright_infos = $this->getAttachmentInfoForHtml($content_html);

        //Thumbnail
        $attachment = $image_manager->getAttachment($item->attachment_id);
        $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Beitragsbild von '{$item->title}'");
        $item_copyright_infos = array_values(array_filter($item_copyright_infos));

        //Dynamisches Bild
        if(isset($item->dynamic_image_type) && isset($item->dynamic_image_id)){
            $attachment = null;
            switch($item->dynamic_image_type){
                case "attachment":
                    $attachment = $image_manager->getAttachment($item->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $attachment = $stock_image_manager->getAttachment($item->dynamic_image_id);
                    break;
            }
            if($attachment){
                $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Dynamisches-Bild von '{$item->title}'", $item->dynamic_image_type);
                $item_copyright_infos = array_values(array_filter($item_copyright_infos));
            }
        }

        if (count($item_copyright_infos)) {
            return [
                'element_type' => PostManager::TYPE_POST,
                'element_id' => $item->id,
                'element_title' => $item->title,
                'element_permalink' => $post_manager->generatePermalink($item),
                'copyright_info_items' => $item_copyright_infos
            ];
        }

        return null;
    }

    public function getBlogCategoryAttachmentsCopyrightInfo()
    {
        $copyright_infos = [];

        $post_manager = new PostManager();

        foreach ($post_manager->getTaxonomies() as $item) {
            $copyright_info = $this->getBlogCategoryItemAttachmentsCopyrightInfo($item);;

            if($copyright_info){
                $copyright_infos[] = $copyright_info;
            }
        }

        return $copyright_infos;
    }

    public function getBlogCategoryItemAttachmentsCopyrightInfo($item)
    {
        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();

        $post_manager = new PostManager();

        //Content
        $content_html = ShortcodeHelper::doShortcode($item->content);
        $item_copyright_infos = $this->getAttachmentInfoForHtml($content_html);

        //Thumbnail
        $attachment = $image_manager->getAttachment($item->attachment_id);
        $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Beitragsbild von Blog-Kategorie '{$item->title}'");
        $item_copyright_infos = array_values(array_filter($item_copyright_infos));

        //Dynamisches Bild
        if(isset($item->dynamic_image_type) && isset($item->dynamic_image_id)){
            $attachment = null;
            switch($item->dynamic_image_type){
                case "attachment":
                    $attachment = $image_manager->getAttachment($item->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $attachment = $stock_image_manager->getAttachment($item->dynamic_image_id);
                    break;
            }
            if($attachment){
                $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Dynamisches-Bild von '{$item->title}'", $item->dynamic_image_type);
                $item_copyright_infos = array_values(array_filter($item_copyright_infos));
            }
        }

        if (count($item_copyright_infos)) {
            return [
                'element_type' => PostManager::TYPE_POST_CATEGORY,
                'element_id' => $item->id,
                'element_title' => $item->title,
                'element_permalink' => $post_manager->getTaxonomyPermalink($item),
                'copyright_info_items' => $item_copyright_infos
            ];
        }

        return null;
    }

    public function getNewsAttachmentsCopyrightInfo()
    {
        $copyright_infos = [];

        $news_manager = new NewsManager();

        foreach ($news_manager->getAll(PostableManager::STATUS_PUBLISH) as $item) {
            $copyright_info = $this->getNewsItemAttachmentsCopyrightInfo($item);;

            if($copyright_info){
                $copyright_infos[] = $copyright_info;
            }
        }

        return $copyright_infos;
    }

    public function getNewsItemAttachmentsCopyrightInfo($item)
    {
        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();

        $news_manager = new NewsManager();

        //Content
        $content_html = ShortcodeHelper::doShortcode($item->content);
        $item_copyright_infos = $this->getAttachmentInfoForHtml($content_html);

        //Thumbnail
        $attachment = $image_manager->getAttachment($item->attachment_id);
        $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Beitragsbild von '{$item->title}'");
        $item_copyright_infos = array_values(array_filter($item_copyright_infos));

        //Dynamisches Bild
        if(isset($item->dynamic_image_type) && isset($item->dynamic_image_id)){
            $attachment = null;
            switch($item->dynamic_image_type){
                case "attachment":
                    $attachment = $image_manager->getAttachment($item->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $attachment = $stock_image_manager->getAttachment($item->dynamic_image_id);
                    break;
            }
            if($attachment){
                $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Dynamisches-Bild von '{$item->title}'", $item->dynamic_image_type);
                $item_copyright_infos = array_values(array_filter($item_copyright_infos));
            }
        }

        if (count($item_copyright_infos)) {
            return [
                'element_type' => NewsManager::TYPE_NEWS,
                'element_id' => $item->id,
                'element_title' => $item->title,
                'element_permalink' => $news_manager->generatePermalink($item),
                'copyright_info_items' => $item_copyright_infos
            ];
        }

        return null;
    }

    public function getNewsCategoryAttachmentsCopyrightInfo()
    {
        $copyright_infos = [];

        $news_manager = new NewsManager();

        foreach ($news_manager->getTaxonomies() as $item) {
            $copyright_info = $this->getNewsCategoryItemAttachmentsCopyrightInfo($item);;

            if($copyright_info){
                $copyright_infos[] = $copyright_info;
            }
        }

        return $copyright_infos;
    }

    public function getNewsCategoryItemAttachmentsCopyrightInfo($item)
    {
        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();

        $news_manager = new NewsManager();

        //Content
        $content_html = ShortcodeHelper::doShortcode($item->content);
        $item_copyright_infos = $this->getAttachmentInfoForHtml($content_html);

        //Thumbnail
        $attachment = $image_manager->getAttachment($item->attachment_id);
        $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Beitragsbild von News-Kategorie '{$item->title}'");
        $item_copyright_infos = array_values(array_filter($item_copyright_infos));

        //Dynamisches Bild
        if(isset($item->dynamic_image_type) && isset($item->dynamic_image_id)){
            $attachment = null;
            switch($item->dynamic_image_type){
                case "attachment":
                    $attachment = $image_manager->getAttachment($item->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $attachment = $stock_image_manager->getAttachment($item->dynamic_image_id);
                    break;
            }
            if($attachment){
                $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Dynamisches-Bild von '{$item->title}'", $item->dynamic_image_type);
                $item_copyright_infos = array_values(array_filter($item_copyright_infos));
            }
        }

        if (count($item_copyright_infos)) {
            return [
                'element_type' => NewsManager::TYPE_NEWS_CATEGORY,
                'element_id' => $item->id,
                'element_title' => $item->title,
                'element_permalink' => $news_manager->getTaxonomyPermalink($item),
                'copyright_info_items' => $item_copyright_infos
            ];
        }

        return null;
    }

    public function getProductAttachmentsCopyrightInfo()
    {
        $copyright_infos = [];

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        foreach ($product_manager->getAll(PostableManager::STATUS_PUBLISH) as $item) {
            $copyright_info = $this->getProductItemAttachmentsCopyrightInfo($item);;

            if($copyright_info){
                $copyright_infos[] = $copyright_info;
            }
        }

        return $copyright_infos;
    }

    public function getProductItemAttachmentsCopyrightInfo($item)
    {
        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        //Content
        $content_html = ShortcodeHelper::doShortcode($item->content);
        $item_copyright_infos = $this->getAttachmentInfoForHtml($content_html);

        //Thumbnail
        #$attachment = $image_manager->getAttachment($item->attachment_id);
        #$item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Beitragsbild von '{$item->title}'");
        $item_copyright_infos = array_values(array_filter($item_copyright_infos));

        //Dynamisches Bild
        if(isset($item->dynamic_image_type) && isset($item->dynamic_image_id)){
            $attachment = null;
            switch($item->dynamic_image_type){
                case "attachment":
                    $attachment = $image_manager->getAttachment($item->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $attachment = $stock_image_manager->getAttachment($item->dynamic_image_id);
                    break;
            }
            if($attachment){
                $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Dynamisches-Bild von '{$item->title}'", $item->dynamic_image_type);
                $item_copyright_infos = array_values(array_filter($item_copyright_infos));
            }
        }

        if (count($item_copyright_infos)) {
            return [
                'element_type' => ProductManager::TYPE_PRODUCT,
                'element_id' => $item->id,
                'element_title' => $item->title,
                'element_permalink' => $product_manager->generatePermalink($item),
                'copyright_info_items' => $item_copyright_infos
            ];
        }

        return null;
    }

    public function getProductCategoryAttachmentsCopyrightInfo()
    {
        $copyright_infos = [];

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        foreach ($product_manager->getTaxonomies() as $item) {
            $copyright_info = $this->getProductCategoryItemAttachmentsCopyrightInfo($item);;

            if($copyright_info){
                $copyright_infos[] = $copyright_info;
            }
        }

        return $copyright_infos;
    }

    public function getProductCategoryItemAttachmentsCopyrightInfo($item)
    {
        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        //Content
        $content_html = ShortcodeHelper::doShortcode($item->content);
        $item_copyright_infos = $this->getAttachmentInfoForHtml($content_html);

        //Thumbnail
        $attachment = $image_manager->getAttachment($item->attachment_id);
        $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Beitragsbild von Produkt-Kategorie '{$item->title}'");
        $item_copyright_infos = array_values(array_filter($item_copyright_infos));

        $icon = $image_manager->getAttachment($item->icon_id);
        $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($icon, "Icon von Produkt-Kategorie '{$item->title}'");
        $item_copyright_infos = array_values(array_filter($item_copyright_infos));

        //Dynamisches Bild
        if(isset($item->dynamic_image_type) && isset($item->dynamic_image_id)){
            $attachment = null;
            switch($item->dynamic_image_type){
                case "attachment":
                    $attachment = $image_manager->getAttachment($item->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $attachment = $stock_image_manager->getAttachment($item->dynamic_image_id);
                    break;
            }
            if($attachment){
                $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Dynamisches-Bild von '{$item->title}'", $item->dynamic_image_type);
                $item_copyright_infos = array_values(array_filter($item_copyright_infos));
            }
        }

        if (count($item_copyright_infos)) {
            return [
                'element_type' => ProductManager::TYPE_PRODUCT_CATEGORY,
                'element_id' => $item->id,
                'element_title' => $item->title,
                'element_permalink' => $product_manager->getTaxonomyPermalink($item),
                'copyright_info_items' => $item_copyright_infos
            ];
        }

        return null;
    }

    public function getBrandsAttachmentsCopyrightInfo()
    {
        return $this->getAttachmentCopyrightInfoForType(new BrandManager(), BrandManager::TYPE_BRAND);
    }

    public function getPageAttachmentsCopyrightInfo()
    {
        return $this->getAttachmentCopyrightInfoForType(new PageManager(), PageManager::TYPE_PAGE);
    }

    public function getAttachmentCopyrightInfoForType($manager, $element_type)
    {
        $copyright_infos = [];

        foreach ($manager->getAll(PostableManager::STATUS_PUBLISH) as $item) {
            $copyright_info = $this->getAttachmentCopyrightInfoForTypeItem($manager, $item, $element_type);

            if($copyright_info){
                $copyright_infos[] = $copyright_info;
            }
        }

        return $copyright_infos;
    }

    public function getAttachmentCopyrightInfoForTypeItem($manager, $item, $element_type)
    {
        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();

        //Content
        $content_html = ShortcodeHelper::doShortcode($item->content);
        $item_copyright_infos = $this->getAttachmentInfoForHtml($content_html);

        //Thumbnail
        $attachment = $image_manager->getAttachment($item->attachment_id);
        $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Beitragsbild von '{$item->title}'");
        $item_copyright_infos = array_values(array_filter($item_copyright_infos));

        //Dynamisches Bild
        if(isset($item->dynamic_image_type) && isset($item->dynamic_image_id)){
            $attachment = null;
            switch($item->dynamic_image_type){
                case "attachment":
                    $attachment = $image_manager->getAttachment($item->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $attachment = $stock_image_manager->getAttachment($item->dynamic_image_id);
                    break;
            }
            if($attachment){
                $item_copyright_infos[] = $this->getCopyrightInfoForAttachment($attachment, "Dynamisches-Bild von '{$item->title}'", $item->dynamic_image_type);
                $item_copyright_infos = array_values(array_filter($item_copyright_infos));
            }
        }

        if (count($item_copyright_infos)) {
            return [
                'element_type' => $element_type,
                'element_id' => $item->id,
                'element_title' => $item->title,
                'element_permalink' => $manager->generatePermalink($item),
                'copyright_info_items' => $item_copyright_infos
            ];
        }

        return null;
    }

    public function getAttachmentInfoForHtml($html)
    {
        $pattern = '/<img[^>]+src=["\']?([^"\']+)["\']?/i';
        preg_match_all($pattern, $html, $matches);
        $image_urls = $matches[1];

        $attachment_infos = [];

        foreach ($image_urls as $image_url) {
            $attachment_info = $this->getAttachmentInfoForUrl($image_url);

            if ($attachment_info) {
                $attachment_infos[] = $attachment_info;
            }
        }

        return $attachment_infos;
    }

    public function getAttachmentInfoForUrl($url)
    {
        $url = $this->removeDimensionsFromImageUrl($url);

        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();

        $attachment = $image_manager->getAttachmentByOldUrl($url);

        $attachment_type = null;
        if (!$attachment) {
            $attachment_type = "attachment";
            $attachment = $image_manager->getAttachmentByFilepath('/attachment/' . $this->getImageFilename($url));
        }

        if (!$attachment) {
            $attachment_type = "stock_attachment";
            $attachment = $stock_image_manager->getAttachmentByFilepath('/stock-attachment/' . $this->getImageFilename($url));
        }

        return $this->getCopyrightInfoForAttachment($attachment, null, $attachment_type);
    }

    public function getCopyrightInfoForAttachment($attachment, $custom_description = null, $attachment_type = "attachment")
    {
        if (!$attachment || (!$attachment->description && !$attachment->provider_name && !$attachment->copyright_info)) {
            return null;
        }

        return [
            "attachment_type" => $attachment_type,
            "attachment_id" => $attachment->id,
            "description" => $custom_description ?: $attachment->description,
            "provider_name" => $attachment->provider_name,
            "copyright_info" => $attachment->copyright_info
        ];
    }

    function getImageFilename($url)
    {
        $path_parts = pathinfo($url);
        return $path_parts['basename'];
    }

    function removeDimensionsFromImageUrl($url)
    {
        $pattern = '/-(\d+x\d+)\.(png|jpe?g|gif)/i';

        $replacement = '.$2';

        if (preg_match($pattern, $url)) {
            $url = preg_replace($pattern, $replacement, $url);
        }

        return $url;
    }
}