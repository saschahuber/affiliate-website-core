<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\service\ApplicationAnalyticsService;
use saschahuber\saastemplatecore\service\IntervalService;

class ProductLinkClicksLastIntervalService extends IntervalService
{
    function __construct()
    {
        parent::__construct();
        $this->application_analytics_service = new ApplicationAnalyticsService();
    }

    function getStoreTypes(){
        $item = [];
        foreach($this->DB->getAll("SELECT distinct(store_type) from product__link_click") as $tag){
            $item[] = $tag->store_type;
        }
        return $item;
    }

    function getLastIntervalLogEntries($interval = DatabaseTimeSeriesHelper::INTERVAL_MINUTE, $number = 60, $product_id = null, $store_type = null)
    {
        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $sql_date_format = "%Y-%m-%d %H:%i";
                $sql_interval = "MINUTE";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $sql_date_format = "%Y-%m-%d %H";
                $sql_interval = "HOUR";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $sql_date_format = "%Y-%m-%d";
                $sql_interval = "DAY";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $sql_date_format = "%Y-%m";
                $sql_interval = "MONTH";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $sql_date_format = "%Y";
                $sql_interval = "YEAR";
                break;
        }

        $filter_string = "product__link_click.time > CURRENT_DATE - INTERVAL ".intval($number)." $sql_interval";
        if ($store_type) {
            $filter_string .= ' and store_type = "'.  $this->DB->escape($store_type) . '"';
        }
        if ($product_id) {
            $filter_string .= ' and product_id = '.  intval($product_id);
        }

        $query = "SELECT Date_format(product__link_click.time, '$sql_date_format') AS timestamp,
                                           Count(*) AS anzahl
                                    FROM product__link_click
                                    WHERE " . $filter_string . "
                                    GROUP BY timestamp;";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[$item->timestamp] = $item->anzahl;
        }

        return $items;
    }
}