<?php

namespace saschahuber\affiliatewebsitecore\service\stats_collector;

use saschahuber\saastemplatecore\service\StatisticsCollectorService;

class AffiliateClickStatisticsCollectorService extends StatisticsCollectorService
{
    const AFFILIATE_CLICKS = 'AFFILIATE_CLICKS';

    public function __construct()
    {
        parent::__construct();
    }

    public function collectAffiliateClickDailyStats()
    {
        $date_yesterday = date('Y-m-d', time() - ONE_DAY);

        $querystring = "SELECT stores.store_type as label, daily_clicks.anzahl from product__link_click stores
            LEFT JOIN (
                SELECT store_type as label, count(*) as anzahl
                from product__link_click
                where DATE_FORMAT(time, '%Y-%m-%d') = '" . $this->DB->escape($date_yesterday) . "'
            ) daily_clicks on stores.store_type = daily_clicks.label
            group by stores.store_type;";

        foreach ($this->DB->getAll($querystring) as $affiliate_link_clicks) {
            $count = $affiliate_link_clicks->anzahl;
            if (!$count) {
                $count = 0;
            }
            $label = $affiliate_link_clicks->label;
            $this->collectStatistics(self::AFFILIATE_CLICKS, $count, $date_yesterday, $label);
            echo "Affiliate-Clicks zu Statistiken hinzugefügt: $label => $count" . PHP_EOL;
        }
    }
}