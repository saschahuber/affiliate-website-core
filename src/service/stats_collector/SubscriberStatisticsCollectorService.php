<?php

namespace saschahuber\affiliatewebsitecore\service\stats_collector;

use saschahuber\saastemplatecore\service\StatisticsCollectorService;

class SubscriberStatisticsCollectorService extends StatisticsCollectorService
{
    const NEWSLETTER_SUBSCRIBERS = 'NEWSLETTER_SUBSCRIBERS';
    const PUSH_SUBSCRIBERS = 'PUSH_SUBSCRIBERS';

    public function __construct()
    {
        parent::__construct();
    }

    public function collectNewsletterSubscribersDailyStats()
    {
        $date_yesterday = date('Y-m-d', time() - ONE_DAY);

        $querystring = "SELECT count(*) as anzahl FROM newsletter_subscriber
                          left join user on newsletter_subscriber.user_id = user.user_id
        where DATE_FORMAT(registration_date, '%Y-%m-%d') = '" . $this->DB->escape($date_yesterday) . "'";

        $stats_data = $this->DB->getOne($querystring);
        $count = $stats_data ? $stats_data->anzahl : 0;
        $this->collectStatistics(self::NEWSLETTER_SUBSCRIBERS, $count, $date_yesterday);
        echo "Newsletter-Abonnenten zu Statistiken hinzugefügt: $count" . PHP_EOL;
    }

    public function collectPushSubscribersDailyStats()
    {
        $date_yesterday = date('Y-m-d', time() - ONE_DAY);

        $querystring = "SELECT count(*) as anzahl FROM push_subscriber
        where DATE_FORMAT(date_subscribed, '%Y-%m-%d') = '" . $this->DB->escape($date_yesterday) . "'";

        $stats_data = $this->DB->getOne($querystring);
        $count = $stats_data ? $stats_data->anzahl : 0;
        $this->collectStatistics(self::PUSH_SUBSCRIBERS, $count, $date_yesterday);
        echo "Push-Abonnenten zu Statistiken hinzugefügt: $count" . PHP_EOL;
    }
}