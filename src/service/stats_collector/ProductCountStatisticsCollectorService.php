<?php

namespace saschahuber\affiliatewebsitecore\service\stats_collector;

use saschahuber\saastemplatecore\service\StatisticsCollectorService;

class ProductCountStatisticsCollectorService extends StatisticsCollectorService
{
    const PRODUCT_COUNT = 'PRODUCT_COUNT';

    public function __construct()
    {
        parent::__construct();
    }

    public function collectProductCountDailyStats()
    {
        $date_yesterday = date('Y-m-d', time() - ONE_DAY);

        $querystring = "SELECT count(*) as anzahl FROM product";

        foreach ($this->DB->getAll($querystring) as $affiliate_link_clicks) {
            $count = $affiliate_link_clicks->anzahl;
            $this->collectStatistics(self::PRODUCT_COUNT, $count, $date_yesterday);
            echo "Product-Anzahl nach Feed zu Statistiken hinzugefügt: $count" . PHP_EOL;
        }
    }
}