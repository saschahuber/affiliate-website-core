<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use Nelexa\GPlay\GPlayApps;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class GoogleAppStoreDataService extends Service
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAppData($app_id, $locale = 'de_DE', $country = 'de')
    {
        try {
            $gplay = new GPlayApps($locale, $country);
            $app_data = $gplay->getAppInfo($app_id);

            //Daten und Icon cachen

            return [
                'name' => $app_data->getName(),
                'icon' => $app_data->getIcon()->getUrl()
            ];
        } catch (\Throwable $e){
            return null;
        }
    }
}