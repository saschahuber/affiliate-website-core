<?php

namespace saschahuber\affiliatewebsitecore\service\role_permission;

use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\affiliatewebsitecore\manager\NewsFeedImportManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\manager\ProductShopManager;
use saschahuber\affiliatewebsitecore\persistence\AuthorLinkRepository;
use saschahuber\affiliatewebsitecore\persistence\AuthorRepository;
use saschahuber\affiliatewebsitecore\persistence\CrmContactPersonRepository;
use saschahuber\affiliatewebsitecore\persistence\CrmContactRepository;
use saschahuber\affiliatewebsitecore\persistence\CrmNoteRepository;
use saschahuber\affiliatewebsitecore\persistence\GuideFilterRepository;
use saschahuber\affiliatewebsitecore\persistence\GuideFilterStepOptionFilterRepository;
use saschahuber\affiliatewebsitecore\persistence\GuideFilterStepOptionRepository;
use saschahuber\affiliatewebsitecore\persistence\GuideFilterStepRepository;
use saschahuber\affiliatewebsitecore\persistence\RedirectGroupItemRepository;
use saschahuber\affiliatewebsitecore\persistence\RedirectGroupRepository;
use saschahuber\affiliatewebsitecore\persistence\RedirectRepository;
use saschahuber\affiliatewebsitecore\persistence\ToastRepository;
use saschahuber\affiliatewebsitecore\persistence\VgWortPixelRepository;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\manager\TrackingCodeManager;
use saschahuber\saastemplatecore\persistence\ConfigRepository;
use saschahuber\saastemplatecore\persistence\UserRepository;
use saschahuber\saastemplatecore\service\role_permission\PermissionService;
use saschahuber\saastemplatecore\service\StripePaymentService;

class AffiliateWebsitePermissionService extends PermissionService {
    const ACCESS_ADMIN_DASHBOARD = "ACCESS_ADMIN_DASHBOARD";

    const VIEW_PAGES = "VIEW_PAGES";
    const EDIT_PAGES = "EDIT_PAGES";

    const VIEW_BLOG_POSTS = "VIEW_BLOG_POSTS";
    const EDIT_BLOG_POSTS = "EDIT_BLOG_POSTS";

    const VIEW_COMPANIES = "VIEW_COMPANIES";
    const EDIT_COMPANIES = "EDIT_COMPANIES";

    const VIEW_CONFIG = "VIEW_CONFIG";
    const EDIT_CONFIG = "EDIT_CONFIG";

    const VIEW_ATTACHMENTS = "VIEW_ATTACHMENTS";
    const EDIT_ATTACHMENTS = "EDIT_ATTACHMENTS";

    const VIEW_STOCK_ATTACHMENTS = "VIEW_STOCK_ATTACHMENTS";
    const EDIT_STOCK_ATTACHMENTS = "EDIT_STOCK_ATTACHMENTS";

    const VIEW_AI_ATTACHMENTS = "VIEW_AI_ATTACHMENTS";
    const EDIT_AI_ATTACHMENTS = "EDIT_AI_ATTACHMENTS";

    const VIEW_ICONS = "VIEW_ICONS";
    const EDIT_ICONS = "EDIT_ICONS";

    const VIEW_FILES = "VIEW_FILES";
    const EDIT_FILES = "EDIT_FILES";

    const VIEW_NEWS_POSTS = "VIEW_NEWS_POSTS";
    const EDIT_NEWS_POSTS = "EDIT_NEWS_POSTS";

    const VIEW_PRODUCTS = "VIEW_PRODUCTS";
    const EDIT_PRODUCTS = "EDIT_PRODUCTS";

    const VIEW_BRANDS = "VIEW_BRANDS";
    const EDIT_BRANDS = "EDIT_BRANDS";

    const VIEW_USERS = "VIEW_USERS";
    const EDIT_USERS = "EDIT_USERS";

    const VIEW_AUTHORS = "VIEW_AUTHORS";
    const EDIT_AUTHORS = "EDIT_AUTHORS";

    const VIEW_FAQS = "VIEW_FAQS";
    const EDIT_FAQS = "EDIT_FAQS";

    const VIEW_GUIDE_FILTER = "VIEW_GUIDE_FILTER";
    const EDIT_GUIDE_FILTER = "EDIT_GUIDE_FILTER";

    const VIEW_CRM = "VIEW_CRM";
    const EDIT_CRM = "EDIT_CRM";

    const VIEW_SOCIAL_MEDIA_POSTINGS = "VIEW_SOCIAL_MEDIA_POSTINGS";
    const EDIT_SOCIAL_MEDIA_POSTINGS = "EDIT_SOCIAL_MEDIA_POSTINGS";

    const VIEW_ADS = "VIEW_ADS";
    const EDIT_ADS = "EDIT_ADS";

    const VIEW_VG_WORT_PIXELS = "VIEW_VG_WORT_PIXELS";
    const EDIT_VG_WORT_PIXELS = "EDIT_VG_WORT_PIXELS";

    const VIEW_NEWSLETTERS = "VIEW_NEWSLETTERS";
    const EDIT_NEWSLETTERS = "EDIT_NEWSLETTERS";

    const VIEW_TOASTS = "VIEW_TOASTS";
    const EDIT_TOASTS = "EDIT_TOASTS";

    const VIEW_BOUNCERS = "VIEW_BOUNCERS";
    const EDIT_BOUNCERS = "EDIT_BOUNCERS";

    const VIEW_TRACKING_CODES = "VIEW_TRACKING_CODES";
    const EDIT_TRACKING_CODES = "EDIT_TRACKING_CODES";

    const VIEW_PUSH_NOTIFICATIONS = "VIEW_PUSH_NOTIFICATIONS";
    const EDIT_PUSH_NOTIFICATIONS = "EDIT_PUSH_NOTIFICATIONS";

    const VIEW_ANALYTICS = "VIEW_ANALYTICS";
    const VIEW_LOGS = "VIEW_LOGS";
    const VIEW_USER_PROFILES = "VIEW_USER_PROFILES";

    const ACCESS_COMPANY_DASHBOARD = "ACCESS_COMPANY_DASHBOARD";
    const ACCESS_PRIVATE_DASHBOARD = "ACCESS_PRIVATE_DASHBOARD";
    
    const VIEW_STRUCTURE = "VIEW_STRUCTURE";
    
    const VIEW_TASKS = "VIEW_TASKS";
    const EDIT_TASKS = "EDIT_TASKS";

    //Abo-Pakete
    const PACKAGE_FREE = "free";
    const PACKAGE_PREMIUM = "premium";
    const PACKAGE_PRO = "pro";

    const PACKAGES = [
        self::PACKAGE_FREE,
        self::PACKAGE_PRO,
        self::PACKAGE_PREMIUM
    ];

    const PACKAGE_PERMISSIONS = [
        self::PACKAGE_FREE => [
            self::ACCESS_COMPANY_DASHBOARD,
            self::ACCESS_PRIVATE_DASHBOARD
        ],
        self::PACKAGE_PRO => [],
        self::PACKAGE_PREMIUM => []
    ];

    public function __construct(){
        parent::__construct();
    }

    function getPackages(){
        return self::PACKAGE_PERMISSIONS;
    }

    public function loadUserPermissions($user){
        $admin_user = AuthHelper::getAdminUser();

        $user_package = StripePaymentService::getPlanFromUser($admin_user);

        $user_roles = [];
        $dbquery = $this->DB->query("SELECT * FROM user__role where user_id = ".intval($user->id));
        while($user_role = $dbquery->fetchObject()){
            $user_roles[] = $user_role->role;
        }
        $role_permissions = $this->getRolePermissions($user_roles);

        #if(AuthHelper::hasPermission(self::ACCESS_ADMIN_DASHBOARD)) {
        #    $package_permissions = $this->getPackagePermissions($user_package);
        #    return array_intersect($role_permissions, $package_permissions);
        #}
        return $role_permissions;
    }

    public function getTablePermissions(){
        return [
            PostManager::TYPE_POST => [AffiliateWebsitePermissionService::EDIT_BLOG_POSTS],
            PostManager::TYPE_POST_CATEGORY => [AffiliateWebsitePermissionService::EDIT_BLOG_POSTS],
            NewsManager::TYPE_NEWS => [AffiliateWebsitePermissionService::EDIT_NEWS_POSTS],
            NewsManager::TYPE_NEWS_CATEGORY => [AffiliateWebsitePermissionService::EDIT_NEWS_POSTS],
            NewsFeedImportManager::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_NEWS_POSTS],
            PageManager::TYPE_PAGE => [AffiliateWebsitePermissionService::EDIT_PAGES],
            LinkPageItemManager::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_PAGES],
            ProductManager::TYPE_PRODUCT => [AffiliateWebsitePermissionService::EDIT_PRODUCTS],
            ProductManager::TYPE_PRODUCT_CATEGORY => [AffiliateWebsitePermissionService::EDIT_PRODUCTS],
            ProductDataFieldManager::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_PRODUCTS],
            ProductDataFieldGroupManager::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_PRODUCTS],
            'product__data' => [AffiliateWebsitePermissionService::EDIT_PRODUCTS],
            ProductShopManager::BASE_TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_PRODUCTS],
            ProductLinkManager::BASE_TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_PRODUCTS],
            ProductFeedManager::BASE_TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_PRODUCTS],
            BrandManager::TYPE_BRAND => [AffiliateWebsitePermissionService::EDIT_BRANDS],
            AdManager::BASE_TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_ADS],
            VgWortPixelRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_VG_WORT_PIXELS],
            ToastRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_TOASTS],
            CrmContactRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_CRM],
            CrmContactPersonRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_CRM],
            CrmNoteRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_CRM],
            ConfigRepository::BASE_TABLE => [AffiliateWebsitePermissionService::EDIT_CONFIG],
            RedirectRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_CONFIG],
            RedirectGroupRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_CONFIG],
            RedirectGroupItemRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_CONFIG],
            TrackingCodeManager::BASE_TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_TRACKING_CODES],
            UserRepository::BASE_TABLE => [AffiliateWebsitePermissionService::EDIT_USERS],
            AuthorRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_AUTHORS],
            AuthorLinkRepository::TABLE_NAME => [AffiliateWebsitePermissionService::EDIT_AUTHORS],
            GuideFilterRepository::BASE_TABLE => [AffiliateWebsitePermissionService::EDIT_GUIDE_FILTER],
            GuideFilterStepRepository::BASE_TABLE => [AffiliateWebsitePermissionService::EDIT_GUIDE_FILTER],
            GuideFilterStepOptionRepository::BASE_TABLE => [AffiliateWebsitePermissionService::EDIT_GUIDE_FILTER],
            GuideFilterStepOptionFilterRepository::BASE_TABLE => [AffiliateWebsitePermissionService::EDIT_GUIDE_FILTER],
        ];
    }
}