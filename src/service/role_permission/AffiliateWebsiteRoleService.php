<?php

namespace saschahuber\affiliatewebsitecore\service\role_permission;

use saschahuber\saastemplatecore\service\role_permission\UserRoleService;

class AffiliateWebsiteRoleService extends UserRoleService {
    const SUPER_ADMIN = "SUPER_ADMIN";
    const ADMIN_PANEL_USER = "ADMIN_PANEL_USER";
    const ADMIN_CONTENT_MANAGER = "ADMIN_CONTENT_MANAGER";
    const ADMIN_MARKETING_MANAGER = "ADMIN_MARKETING_MANAGER";
    const ADMIN_SOCIAL_MEDIA_MANAGER = "ADMIN_SOCIAL_MEDIA_MANAGER";
    const ADMIN_CRM_MANAGER = "ADMIN_CRM_MANAGER";
    const COMPANY_USER = "COMPANY_USER";
    const PRIVATE_USER = "PRIVATE_USER";

    const ROLE_MAPPINGS = [
        ['key' => self::SUPER_ADMIN, 'label' => 'Super-Administrator'],
        ['key' => self::ADMIN_CRM_MANAGER, 'label' => 'Customer Manager'],
        ['key' => self::ADMIN_SOCIAL_MEDIA_MANAGER, 'label' => 'Social Media Manager'],
        ['key' => self::ADMIN_MARKETING_MANAGER, 'label' => 'Marketing Manager'],
        ['key' => self::ADMIN_CONTENT_MANAGER, 'label' => 'Content Manager'],
        ['key' => self::ADMIN_PANEL_USER, 'label' => 'Administrator'],
        ['key' => self::COMPANY_USER, 'label' => 'Firmennutzer'],
        ['key' => self::PRIVATE_USER, 'label' => 'Privater Benutzer']
    ];

    const ROLE_PERMISSIONS = [
        self::SUPER_ADMIN => [
            AffiliateWebsitePermissionService::ACCESS_ADMIN_DASHBOARD,
            AffiliateWebsitePermissionService::VIEW_USERS,
            AffiliateWebsitePermissionService::EDIT_USERS,
            AffiliateWebsitePermissionService::VIEW_BLOG_POSTS,
            AffiliateWebsitePermissionService::EDIT_BLOG_POSTS,
            AffiliateWebsitePermissionService::VIEW_NEWS_POSTS,
            AffiliateWebsitePermissionService::EDIT_NEWS_POSTS,
            AffiliateWebsitePermissionService::VIEW_COMPANIES,
            AffiliateWebsitePermissionService::EDIT_COMPANIES,
            AffiliateWebsitePermissionService::VIEW_PRODUCTS,
            AffiliateWebsitePermissionService::EDIT_PRODUCTS,
            AffiliateWebsitePermissionService::VIEW_BRANDS,
            AffiliateWebsitePermissionService::EDIT_BRANDS,
            AffiliateWebsitePermissionService::VIEW_PAGES,
            AffiliateWebsitePermissionService::EDIT_PAGES,
            AffiliateWebsitePermissionService::VIEW_ATTACHMENTS,
            AffiliateWebsitePermissionService::EDIT_ATTACHMENTS,
            AffiliateWebsitePermissionService::VIEW_STOCK_ATTACHMENTS,
            AffiliateWebsitePermissionService::EDIT_STOCK_ATTACHMENTS,
            AffiliateWebsitePermissionService::VIEW_ICONS,
            AffiliateWebsitePermissionService::VIEW_ICONS,
            AffiliateWebsitePermissionService::VIEW_AI_ATTACHMENTS,
            AffiliateWebsitePermissionService::EDIT_AI_ATTACHMENTS,
            AffiliateWebsitePermissionService::VIEW_FILES,
            AffiliateWebsitePermissionService::EDIT_FILES,
            AffiliateWebsitePermissionService::VIEW_TOASTS,
            AffiliateWebsitePermissionService::EDIT_TOASTS,
            AffiliateWebsitePermissionService::VIEW_BOUNCERS,
            AffiliateWebsitePermissionService::EDIT_BOUNCERS,
            AffiliateWebsitePermissionService::VIEW_PUSH_NOTIFICATIONS,
            AffiliateWebsitePermissionService::EDIT_PUSH_NOTIFICATIONS,
            AffiliateWebsitePermissionService::VIEW_NEWSLETTERS,
            AffiliateWebsitePermissionService::EDIT_NEWSLETTERS,
            AffiliateWebsitePermissionService::VIEW_SOCIAL_MEDIA_POSTINGS,
            AffiliateWebsitePermissionService::EDIT_SOCIAL_MEDIA_POSTINGS,
            AffiliateWebsitePermissionService::VIEW_CRM,
            AffiliateWebsitePermissionService::EDIT_CRM,
            AffiliateWebsitePermissionService::VIEW_AUTHORS,
            AffiliateWebsitePermissionService::EDIT_AUTHORS,
            AffiliateWebsitePermissionService::VIEW_CONFIG,
            AffiliateWebsitePermissionService::EDIT_CONFIG,
            AffiliateWebsitePermissionService::VIEW_ANALYTICS,
            AffiliateWebsitePermissionService::VIEW_ADS,
            AffiliateWebsitePermissionService::EDIT_ADS,
            AffiliateWebsitePermissionService::VIEW_USER_PROFILES,
            AffiliateWebsitePermissionService::VIEW_VG_WORT_PIXELS,
            AffiliateWebsitePermissionService::EDIT_VG_WORT_PIXELS,
            AffiliateWebsitePermissionService::VIEW_TRACKING_CODES,
            AffiliateWebsitePermissionService::EDIT_TRACKING_CODES,
            AffiliateWebsitePermissionService::VIEW_GUIDE_FILTER,
            AffiliateWebsitePermissionService::EDIT_GUIDE_FILTER,
            AffiliateWebsitePermissionService::VIEW_FAQS,
            AffiliateWebsitePermissionService::EDIT_FAQS,
            AffiliateWebsitePermissionService::VIEW_LOGS,
            AffiliateWebsitePermissionService::VIEW_STRUCTURE,
            AffiliateWebsitePermissionService::VIEW_TASKS,
            AffiliateWebsitePermissionService::EDIT_TASKS,
        ],
        self::ADMIN_PANEL_USER => [
            AffiliateWebsitePermissionService::ACCESS_ADMIN_DASHBOARD
        ],
        self::ADMIN_CRM_MANAGER => [
            AffiliateWebsitePermissionService::VIEW_CRM,
            AffiliateWebsitePermissionService::EDIT_CRM
        ],
        self::ADMIN_CONTENT_MANAGER => [
            AffiliateWebsitePermissionService::VIEW_BLOG_POSTS,
            AffiliateWebsitePermissionService::EDIT_BLOG_POSTS,
            AffiliateWebsitePermissionService::VIEW_NEWS_POSTS,
            AffiliateWebsitePermissionService::EDIT_NEWS_POSTS,
            AffiliateWebsitePermissionService::VIEW_COMPANIES,
            AffiliateWebsitePermissionService::EDIT_COMPANIES,
            AffiliateWebsitePermissionService::VIEW_PRODUCTS,
            AffiliateWebsitePermissionService::EDIT_PRODUCTS,
            AffiliateWebsitePermissionService::VIEW_BRANDS,
            AffiliateWebsitePermissionService::EDIT_BRANDS,
            AffiliateWebsitePermissionService::VIEW_ATTACHMENTS,
            AffiliateWebsitePermissionService::EDIT_ATTACHMENTS,
            AffiliateWebsitePermissionService::VIEW_STOCK_ATTACHMENTS,
            AffiliateWebsitePermissionService::EDIT_STOCK_ATTACHMENTS,
            AffiliateWebsitePermissionService::VIEW_ICONS,
            AffiliateWebsitePermissionService::VIEW_ICONS,
            AffiliateWebsitePermissionService::VIEW_AI_ATTACHMENTS,
            AffiliateWebsitePermissionService::EDIT_AI_ATTACHMENTS,
            AffiliateWebsitePermissionService::VIEW_FILES,
            AffiliateWebsitePermissionService::EDIT_FILES,
            AffiliateWebsitePermissionService::VIEW_TOASTS,
            AffiliateWebsitePermissionService::EDIT_TOASTS,
            AffiliateWebsitePermissionService::VIEW_BOUNCERS,
            AffiliateWebsitePermissionService::EDIT_BOUNCERS,
            AffiliateWebsitePermissionService::VIEW_PUSH_NOTIFICATIONS,
            AffiliateWebsitePermissionService::EDIT_PUSH_NOTIFICATIONS,
            AffiliateWebsitePermissionService::VIEW_NEWSLETTERS,
            AffiliateWebsitePermissionService::EDIT_NEWSLETTERS
        ],
        self::ADMIN_MARKETING_MANAGER => [
            AffiliateWebsitePermissionService::VIEW_ANALYTICS,
            AffiliateWebsitePermissionService::VIEW_ADS,
            AffiliateWebsitePermissionService::EDIT_ADS,
            AffiliateWebsitePermissionService::VIEW_VG_WORT_PIXELS,
            AffiliateWebsitePermissionService::EDIT_VG_WORT_PIXELS,
            AffiliateWebsitePermissionService::VIEW_TRACKING_CODES,
            AffiliateWebsitePermissionService::EDIT_TRACKING_CODES,
            AffiliateWebsitePermissionService::VIEW_STRUCTURE
        ],
        self::ADMIN_SOCIAL_MEDIA_MANAGER => [
            AffiliateWebsitePermissionService::VIEW_SOCIAL_MEDIA_POSTINGS,
            AffiliateWebsitePermissionService::EDIT_SOCIAL_MEDIA_POSTINGS
        ],
        self::COMPANY_USER => [
            AffiliateWebsitePermissionService::ACCESS_COMPANY_DASHBOARD
        ],
        self::PRIVATE_USER => [
            AffiliateWebsitePermissionService::ACCESS_PRIVATE_DASHBOARD
        ]
    ];

    public function __construct(){
        parent::__construct();
    }

    function getRoleMappings(){
        return self::ROLE_MAPPINGS;
    }

    function getRolePermissions(){
        return self::ROLE_PERMISSIONS;
    }
}