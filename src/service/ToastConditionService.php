<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\model\Toast;
use saschahuber\affiliatewebsitecore\persistence\ToastConditionRepository;
use saschahuber\affiliatewebsitecore\persistence\ToastRepository;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\service\Service;

class ToastConditionService extends Service {
    public function __construct()
    {
        parent::__construct();
        $this->toast_condition_repository = new ToastConditionRepository();
    }

    public function getByToastId($toast_id){
        return $this->toast_condition_repository->getByToastId($toast_id);
    }

    public function getById($id){
        return $this->toast_condition_repository->getById($id);
    }

    public function createItem($item){
        return $this->toast_condition_repository->create($item);
    }

    public function updateItem($item){
        return $this->toast_condition_repository->update($item);
    }
}