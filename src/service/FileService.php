<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use stdClass;

#[AllowDynamicProperties]
class FileService
{
    const ALLOW_ALL = 0x01;
    const ALLOW_IMAGE = 0x02;
    const ALLOW_VIDEO = 0x04;
    const ALLOW_AUDIO = 0x08;
    const ALLOW_TEXT = 0x10;
    const ALLOW_PDF = 0x20;

    function __construct()
    {
    }

    function get_new_filename($type, $prefix = null)
    {
        $filename = date('Ymd-His') . '-' . rand(10000, 99999) . '.' . $type;

        if ($prefix) {
            $filename = $prefix . '-' . $filename;
        }

        return $filename;
    }

    function uploadFile(string $post_name, float $max_filesize,
                        int    $allowed_types, string $target_dir, $filename_prefix = null)
    {
        $upload_result = new stdClass();
        $upload_result->error = false;
        $upload_result->message = false;
        $upload_result->file_name = false;

        if (!isset($_FILES[$post_name])) {
            LogHelper::error('UPLOAD', 0, 'Datei-Upload fehlgeschlagen: `' . $post_name . '` nicht in $_FILES', ERROR_ALERT);
            $upload_result->message = 'Beim Hochladen der Datei ist leider etwas schief gelaufen. ' .
                'Wir wurden bereits verständigt. Bitte versuchen Sie es später noch einmal.';
            $upload_result->error;
            return $upload_result;
        }

        $file = $_FILES[$post_name];
        if ($file['error'] > 0) {
            $upload_result->message = 'Diese Datei konnte leider nicht hochgeladen werden: ' . (['OK',
                    'Maximale Dateigröße überschritten (php.ini).',
                    'Maximale Dateigröße überschritten (HTML form).',
                    'Fragmentierter Upload.',
                    'No file was uploaded.',
                    'Unkown.',
                    'Missing a temporary folder.',
                    'Failed to write file to disk.',
                    'A PHP extension stopped the file upload.'
                ][$file['error']]);
            $upload_result->error;
            return $upload_result;
        }

        $mime2type = [
            'application/pdf' => 'pdf',
            'image/png' => 'png',
            'image/jpeg' => 'jpg',
            'image/gif' => 'gif',
            'audio/mpeg' => 'mp3',
            'video/mp4' => 'mp4',
            'application/msword' => 'doc',
            'application/CDFV2' => 'doc',
            'application/octet-stream' => 'docx',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
            'application/vnd.oasis.opendocument.text' => 'odt',
            'text/rtf' => 'rtf',
            'text/plain' => 'txt',
            'text/html' => 'html'];

        $mime = mime_content_type($file['tmp_name']);
        $type = isset($mime2type[$mime]) ? $mime2type[$mime] : false;
        $extension = pathinfo($file['name'], PATHINFO_EXTENSION);

        if ($mime == 'inode/x-empty') {
            $upload_result->message = 'Die ausgewählte Datei ist leer. Bitte überprüfen Sie, ob die Datei korrekt abgespeichert wurde.';
            $upload_result->error;
            return $upload_result;
        }

        if ($allowed_types & FileService::ALLOW_ALL) {
            if (!$type) {
                $type = $extension;
            }
        } else {
            $valid_types = array();
            if ($allowed_types & FileService::ALLOW_IMAGE) array_push($valid_types, 'jpg', 'png', 'gif');
            if ($allowed_types & FileService::ALLOW_VIDEO) array_push($valid_types, 'mp4');
            if ($allowed_types & FileService::ALLOW_AUDIO) array_push($valid_types, 'mp3', 'wav');
            if ($allowed_types & FileService::ALLOW_TEXT) array_push($valid_types, 'doc', 'docx', 'txt', 'rtf', 'odt', 'html');
            if ($allowed_types & FileService::ALLOW_PDF) array_push($valid_types, 'pdf');

            if (!$type || !in_array($type, $valid_types)) {
                LogHelper::error('UPLOAD', 1, 'Datei-Upload fehlgeschlagen: Denied Mime-Type: ' . $mime . ' for file ' . $file['name'], ERROR_ALERT);
                $upload_result->message = 'Diese Datei hat nicht das richtige Format. Gültig ist: ' . strtoupper(implode(', ', $valid_types)) . '.';
                $upload_result->error;
                return $upload_result;
            }

            // Manchmal wird HTML als Plaintext erkannt, dann soll HTML-Endung beibehalten werden
            if ($type == 'txt' && ($extension == 'htm' || $extension == 'html')) {
                $type = 'html';
            }
        }

        if ($file['size'] / 1024 / 1024 > $max_filesize) {
            $upload_result->message = 'Diese Datei überschreitet die maximale Größe von ' . $max_filesize . ' MB';
            $upload_result->error;
            return $upload_result;
        }

        // Neue Datei anlegen
        $upload_result->file_name = $this->get_new_filename($type, $filename_prefix);
        $real_dir = DATA_DIR . '/' . $target_dir;
        FileHelper::createDirIfNotExists($real_dir);

        move_uploaded_file($file['tmp_name'], $real_dir . '/' . $upload_result->file_name);

        return $upload_result;
    }
}