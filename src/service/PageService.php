<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class PageService extends Service
{

    public function __construct()
    {
        parent::__construct();
        $this->page_manager = new PageManager();
    }

    public function deletePage($page_id)
    {
        $this->page_manager->delete($page_id);
    }
}