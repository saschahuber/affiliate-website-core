<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\persistence\RedirectRepository;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class RedirectService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->redirect_repository = new RedirectRepository();
    }

    public function getById($item_id)
    {
        return $this->redirect_repository->getById($item_id);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0)
    {
        return $this->redirect_repository->getAll($cache, $cache_lifetime_minutes);
    }

    public function save($item)
    {
        return $this->redirect_repository->save($item);
    }

    public function createRedirectIfNotExists($from_url, $to_url, $is_active)
    {
        $this->createRedirectIfNotExists($from_url, $to_url, $is_active);
    }

    public function redirectIfNeccessary($from_url)
    {
        $item = null;
        foreach ($this->getAll(true, 60) as $redirect_item) {
            if ($redirect_item->from_url === $from_url) {
                $item = $redirect_item;
                break;
            }
        }

        if (!$item) {
            return;
        }

        $item = $this->redirect_repository->getByFromUrl($from_url);

        if ($item && $item->is_active) {
            UrlHelper::redirect($item->to_url, $item->is_temporary?302:301);
        }
    }
}
