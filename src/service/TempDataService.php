<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\persistence\AuthorRepository;
use saschahuber\affiliatewebsitecore\persistence\TempDataRepository;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class TempDataService extends Service{
    public function __construct(){
        parent::__construct();
        $this->temp_data_repository = new TempDataRepository();
    }

    public function getByToken($token){
        return $this->temp_data_repository->getByToken($token);
    }

    public function addTempData($temp_data, $lifetime_seconds=ONE_HOUR){
        return $this->temp_data_repository->addTempData($temp_data);
    }

    public function invalidateExpiredData(){
        $this->temp_data_repository->invalidateExpiredData();
    }
}
