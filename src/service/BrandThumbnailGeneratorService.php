<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class BrandThumbnailGeneratorService extends ThumbnailGeneratorService
{
    public function __construct(){
        parent::__construct();
    }

    public function generateBrandThumbnail($item)
    {
        $background_image = $this->getBackgroundImage($item->dynamic_image_type, $item->dynamic_image_id);

        $relative_image_path = $this->getRelativeThumbnailPath($item);

        $image_text = $item->image_text;
        if(!$image_text || strlen($image_text) < 1){
            $image_text = $item->title;
        }

        $save_path = GENERATED_IMG_DIR . $relative_image_path;
        $image_generator_service = new ThumbnailGeneratorService();
        $image_generator_service->generateThumbnail($background_image, $image_text, $save_path);
        return GENERATED_IMG_SRC . $relative_image_path;
    }

    public function getRelativeThumbnailPath($item){
        return "thumbnail/".ThumbnailHelper::TYPE_BRAND."/{$item->id}/".UrlHelper::alias($item->title).".jpg";
    }

    public function getThumbnailSrc($item){
        return GENERATED_IMG_SRC . $this->getRelativeThumbnailPath($item);
    }
}