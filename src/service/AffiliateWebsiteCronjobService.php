<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\helper\EnvHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\service\CronjobService;

class AffiliateWebsiteCronjobService extends CronjobService {

    public function __construct(){
        parent::__construct();
    }

    private function getCronjobCommand($cronjob){
        global $CONFIG;

        $task_dir = COMMONS_BASE . '/tasks/';

        # Wenn Script nicht in App-Dir existiert => Lib-Script nutzen
        if(!file_exists($task_dir . '/' . $cronjob->script_file)){
            $task_dir = AffiliateWebsiteEnvHelper::getLibPath() . '/tasks/';
        }

        # Wenn Script nicht in App-Dir existiert => Lib-Script nutzen
        if(!file_exists($task_dir . '/' . $cronjob->script_file)){
            $task_dir = EnvHelper::getLibPath() . '/tasks/';
        }

        $script_path = $cronjob->script_file;

        $command = $CONFIG->php_executable . ' ' . $task_dir . $script_path . ' ' . APP_BASE .' ' . $cronjob->script_params;

        LogHelper::varlog('cronjob', "Running command: $command");

        return $command;
    }
}
