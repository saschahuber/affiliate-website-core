<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\ImgUtils;

class ImageGeneratorService
{
    const TopLeft = 0;
    const TopCenter = 1;
    const TopRight = 2;
    const CenterLeft = 3;
    const Center = 4;
    const CenterRight = 5;
    const BottomLeft = 6;
    const BottomCenter = 7;
    const BottomRight = 8;

    public function __construct($width, $height, $background_color = "#EEEEEE")
    {
        $this->img_utils = new ImgUtils();

        $this->font = DM_SANS_FONT_DIR . '/DMSans-Bold.ttf';

        $this->width = $width;
        $this->height = $height;

        $this->resource = $this->createBlankImage($this->width, $this->height, $background_color);
    }

    public function formatText($text, $max_chars_per_line = 20)
    {
        $formatted_text = "";
        $words = explode(' ', $text);

        $line = "";
        foreach ($words as $word) {
            $word_len = strlen($word);

            if (strlen($formatted_text) > 0 && strlen($line) > 0 && $word_len > $max_chars_per_line) {
                $formatted_text .= trim($line) . "\n";
                $line = "";
            }

            $line .= ' ' . $word;

            if (strlen($line) >= $max_chars_per_line) {
                $formatted_text .= trim($line) . "\n";
                $line = "";
            }
        }

        $formatted_text .= trim($line);

        return $formatted_text;
    }

    private function createBlankImage($width, $height, $color = "#FFFFFF")
    {
        $img = imagecreatetruecolor($width, $height);
        $rgb = $this->convertHexToRgb($color);
        $bg = imagecolorallocate($img, $rgb[0], $rgb[1], $rgb[2]);
        imagefilledrectangle($img, 0, 0, $width, $height, $bg);
        return $img;
    }

    private function convertHexToRgb($hexcode)
    {
        list($r, $g, $b) = sscanf($hexcode, "#%02x%02x%02x");
        return array($r, $g, $b);
    }

    function backgroundImage($background_image_src)
    {
        if ($background_image_src != null and file_exists($background_image_src)) {
            $max_width = 0;
            $max_height = 0;

            $img = $this->img_utils->loadImgFile($background_image_src);

            $img_width = imagesx($img);
            $img_height = imagesy($img);

            $background_ratio = ((float)$img_width) / ((float)$img_height);
            $img_ratio = ((float)$this->width) / ((float)$this->height);

            if ($background_ratio < $img_ratio) {
                $max_width = $this->width;
            } else {
                $max_height = $this->height;
            }

            $this->alignedImage($background_image_src, ImageGeneratorService::Center,
                0, $max_width, $max_height);
        }
    }

    function alignedImage(string $img_filename, int $alignment = ImageGeneratorService::Center, int $margin = 0,
                          int    $max_dst_width = 0, int $max_dst_height = 0, array $background_color = NULL)
    {
        $img_src = $this->img_utils->loadImgFile($img_filename);
        $dst_width = $img_width = imagesx($img_src);
        $dst_height = $img_height = imagesy($img_src);

        if ($max_dst_width) {
            $scale = (float)$max_dst_width / (float)$dst_width;
            $dst_width *= $scale;
            $dst_height *= $scale;
        }

        if ($max_dst_height) {
            $scale = (float)$max_dst_height / (float)$dst_height;
            $dst_width *= $scale;
            $dst_height *= $scale;
        }

        imageAlphaBlending($img_src, true);
        imageSaveAlpha($img_src, true);

        $x = $y = $margin;

        switch ($alignment) {
            default:
            case ImageGeneratorService::TopCenter:
                $x = $this->width * 0.5 - $dst_width * 0.5;
                break;
            case ImageGeneratorService::TopRight:
                $x = $this->width - $dst_width - $margin;
                break;
            case ImageGeneratorService::CenterLeft:
                $y = $this->height * 0.5 - $dst_height * 0.5;
                break;
            case ImageGeneratorService::Center:
                $x = $this->width * 0.5 - $dst_width * 0.5;
                $y = $this->height * 0.5 - $dst_height * 0.5;
                break;
            case ImageGeneratorService::CenterRight:
                $x = $this->width - $dst_width - $margin;
                $y = $this->height * 0.5 - $dst_height * 0.5;
                break;
            case ImageGeneratorService::BottomLeft:
                $y = $this->height - $dst_height - $margin;
                break;
            case ImageGeneratorService::BottomCenter:
                $x = $this->width * 0.5 - $dst_width * 0.5;
                $y = $this->height - $dst_height - $margin;
                break;
            case ImageGeneratorService::BottomRight:
                $x = $this->width - $dst_width - $margin;
                $y = $this->height - $dst_height - $margin;
                break;
        }

        if ($background_color) {
            imageFilledRectangle($this->resource,
                $x - 5, $y - 5,                               # point top left coordinates
                $x + $dst_width + 5, $y + $dst_height + 5,    # point bottom right coordinates
                imageColorAllocate($this->resource, $background_color[0], $background_color[1], $background_color[2]));
        }

        if ($dst_width != $img_width || $dst_height != $img_height) {
            imageCopyResized($this->resource, $img_src,
                $x, $y,                                 # destination coordinates
                0, 0,                                   # source coordinates
                $dst_width, $dst_height,                # destination size
                $img_width, $img_height);               # source size
        } else {
            imageCopy($this->resource, $img_src,
                $x, $y,                                 # destination coordinates
                0, 0,                                   # source coordinates
                $img_width, $img_height);               # source size
        }

        imageDestroy($img_src);
    }

    function addWatermarkLogo($vertical_offset = false, $alignment = ImageGeneratorService::TopCenter, $size_multiplier = 0.4)
    {
        global $CONFIG;

        $watermark_logo = $CONFIG->watermark_logo;

        $size = $this->height * $size_multiplier;

        $margin = $size * 0.2;

        if ($vertical_offset) {
            $margin = $vertical_offset;
        }

        $this->alignedImage($watermark_logo, $alignment, $margin, $size, $size);
    }

    function addFooterWatermark($watermark_text, $color = null)
    {
        if ($color === null) {
            $color = "#666666";
        }
        $max_width = $this->width * 0.8;
        $this->alignedText($watermark_text, 36, $max_width, $color, ImageGeneratorService::BottomCenter, 32);
    }

    function centeredText(string $text, int $initial_size = 32, int $max_chars = 80, int $max_lines = 6,
                          string $text_color = "#000000", $vertical_offset = 0, $line_height_multiplier = 1.7)
    {
        $lines = explode("\n", $text);

        $color = $this->convertHexToRgb($text_color);

        $size = $initial_size;

        for ($i = 0; $i < count($lines); $i++) {
            if (strlen($lines[$i]) > $max_chars) {
                $lines[$i] = substr($lines[$i], 0, $max_chars);
            }

            $text_box = imageTTFbBox($size, 0, $this->font, $lines[$i] . '...');
            #$line_height = abs($text_box[7]) + abs($text_box[1]);
            $line_length = abs($text_box[6]) + abs($text_box[4]);

            // Reduce font size until the line fits within 80% of the bg-image's width
            if ($line_length > $this->width * 0.90) {
                $size -= 1;
                $i--;
                continue;
            }
        }

        for ($i = 0; $i < count($lines); $i++) {
            $text_box = imageTTFbBox($size, 0, $this->font, $lines[$i]);
            #$line_height = abs($text_box[7]) + abs($text_box[1]);
            $line_height = $size * $line_height_multiplier;
            $line_length = abs($text_box[6]) + abs($text_box[4]);

            $offset = $line_height * (count($lines) - 1) * 0.5;

            imageTTFText($this->resource, $size, 0,
                $this->width * 0.5 - ($text_box[4] * 0.5),                                     # x
                $this->height * 0.5 - ($text_box[7] * 0.5) - $offset + ($i * $line_height) + $vertical_offset,    # y
                imageColorAllocate($this->resource, $color[0], $color[1], $color[2]),          # color
                $this->font, $lines[$i] . Helper::ifstr($i === $max_lines, '...'));

            if ($i === $max_lines - 1) {
                break;
            }
        }
    }

    function alignedText(string $text, int $size = 32, int $max_width = null, string $hexcode = "#000000",
                         int    $alignment = ImageGeneratorService::TopLeft, int $margin = 0)
    {
        $color = $this->convertHexToRgb($hexcode);

        $text_box = imageTTFbBox($size, 0, $this->font, $text);

        while ($size) {
            #$line_height = abs($text_box[7]) + abs($text_box[1]);
            $line_length = abs($text_box[6]) + abs($text_box[4]);

            // Reduce font size until the line fits within 80% of the bg-image's width
            if ($line_length > $this->width * 0.95) {
                $size -= 1;
                $text_box = imageTTFbBox($size, 0, $this->font, $text);
            } else {
                break;
            }
        }

        $line_height = abs($text_box[7]) + abs($text_box[1]);
        $line_length = abs($text_box[6]) + abs($text_box[4]);

        $x = $margin;
        $y = $line_height + $margin;

        switch ($alignment) {
            default:
            case ImageGeneratorService::TopLeft:
                break;
            case ImageGeneratorService::TopCenter:
                $x = $this->width * 0.5 - $line_length * 0.5;
                break;
            case ImageGeneratorService::TopRight:
                $x = $this->width - $line_length - $margin;
                break;
            case ImageGeneratorService::CenterLeft:
                $y = $this->height * 0.5 - $line_height * 0.5;
                break;
            case ImageGeneratorService::CenterRight:
                $x = $this->width - $line_length - $margin;
                $y = $this->height * 0.5 - $line_height * 0.5;
                break;
            case ImageGeneratorService::BottomLeft:
                $y = $this->height - $margin;
                break;
            case ImageGeneratorService::BottomCenter:
                $x = $this->width * 0.5 - $line_length * 0.5;
                $y = $this->height - $margin;
                break;
            case ImageGeneratorService::BottomRight:
                $x = $this->width - $line_length - $margin;
                $y = $this->height - $margin;
                break;
        }

        imageTTFText($this->resource, $size, 0,
            $x, $y,
            imageColorAllocate($this->resource, $color[0], $color[1], $color[2]),
            $this->font, $text);
    }

    function tint(string $hexcode = "#000000", float $opacity = 0.5)
    {
        $color = $this->convertHexToRgb($hexcode);

        $overlay = imageCreate($this->width, $this->height);
        imageFill($overlay, 0, 0, imageColorAllocateAlpha($overlay, $color[0], $color[1], $color[2], 127 - intval($opacity * 127)));
        imageCopy($this->resource, $overlay, 0, 0, 0, 0, $this->width, $this->height);
    }

    function frame(string $hexcode = "#000000", int $thickness = 1, int $margin = 0)
    {
        $color = $this->convertHexToRgb($hexcode);
        imageSetThickness($this->resource, $thickness);
        imageRectangle($this->resource,
            $margin, $margin,                                   # point top left coordinates
            $this->width - $margin, $this->height - $margin,    # point bottom right coordinates
            imageColorAllocate($this->resource, $color[0], $color[1], $color[2]));
    }

    function ribbon(string $hexcode = "#000000", float $opacity = 0.75, int $size = 128)
    {
        $color = $this->convertHexToRgb($hexcode);
        imageFilledRectangle($this->resource,
            0, $this->height * 0.5 - $size * 0.5,               # point top left coordinates
            $this->width, $this->height * 0.5 + $size * 0.5,    # point bottom right coordinates
            imageColorAllocateAlpha($this->resource, $color[0], $color[1], $color[2], 127 - $opacity * 127));
    }

    function polygon(string $hexcode = "#000000", $corner = ImageGeneratorService::TopLeft, float $scale_x = 1.0, float $scale_y = 1.0)
    {
        $color = $this->convertHexToRgb($hexcode);
        switch ($corner) {
            default:
            case ImageGeneratorService::TopLeft:
                $points = [
                    0, 0,
                    440 * $scale_x, 0,
                    360 * $scale_x, 140 * $scale_y,
                    0, 200 * $scale_y];
                break;
            case ImageGeneratorService::TopRight:
                $points = [
                    $this->width - 440 * $scale_x, 0,
                    $this->width, 0,
                    $this->width, 200 * $scale_y,
                    $this->width - 360 * $scale_x, 140 * $scale_y];
                break;
            case ImageGeneratorService::BottomLeft:
                $points = [
                    0, $this->height - 200 * $scale_y,
                    360 * $scale_x, $this->height - 140 * $scale_y,
                    440 * $scale_x, $this->height,
                    0, $this->height];
                break;
            case ImageGeneratorService::BottomRight:
                $points = [
                    $this->width - 360 * $scale_x, $this->height - 140 * $scale_y,
                    $this->width, $this->height - 200 * $scale_y,
                    $this->width, $this->height,
                    $this->width - 440 * $scale_x, $this->height];
                break;
        }

        imageSetThickness($this->resource, 1);
        imageFilledPolygon($this->resource,
            $points, count($points) * 0.5,
            imageColorAllocate($this->resource, $color[0], $color[1], $color[2]));
    }

    function fixedBox(int   $x, int $y, int $width, int $height, string $hexcode = "#000000", float $opacity = 1.0,
                      array $border_color = [0, 0, 0], float $border_opacity = 0.0, int $border_width = 2)
    {
        $color = $this->convertHexToRgb($hexcode);
        imageFilledRectangle($this->resource,
            $x, $y,                         # point top left coordinates
            $x + $width, $y + $height,      # point bottom right coordinates
            imageColorAllocateAlpha($this->resource, $color[0], $color[1], $color[2], 127 - $opacity * 127));

        if ($border_opacity > 0.0) {
            imageSetThickness($this->resource, $border_width);
            imageRectangle($this->resource,
                $x, $y,                         # point top left coordinates
                $x + $width, $y + $height,      # point bottom right coordinates
                imageColorAllocateAlpha($this->resource,
                    $border_color[0], $border_color[1], $border_color[2], 127 - $border_opacity * 127));
        }
    }

    public function getGeneratedImage(){
        return $this->resource;
    }
}