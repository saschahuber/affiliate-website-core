<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\FileManager;
use Nelexa\GPlay\GPlayApps;
use saschahuber\affiliatewebsitecore\service\AppleAppStoreDataService;
use saschahuber\affiliatewebsitecore\service\GoogleAppStoreDataService;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\CacheHelper;
use saschahuber\saastemplatecore\helper\FileHelper;

#[AllowDynamicProperties]
class AppStoreDataService
{
    const APP_STORE_GOOGLE = 'googleplay';
    const APP_STORE_APPLE = 'appstore';

    public function __construct()
    {
    }

    public function getStoreUrl($app_store, $app_id)
    {
        switch ($app_store) {
            case "googleplay":
                return 'https://play.google.com/store/apps/details?id=' . $app_id . '&hl=de&gl=de';
            case "appstore":
                return 'https://apps.apple.com/de/app/id' . str_replace('id', '', $app_id);
        }
        return null;
    }

    public function getAppStoreIcon($app_store)
    {
        switch ($app_store) {
            case self::APP_STORE_GOOGLE:
                return 'fab fa-google';
            case self::APP_STORE_APPLE:
                return 'fab fa-apple';
        }
        return null;
    }

    public function getButtonText($app_store)
    {
        switch ($app_store) {
            case self::APP_STORE_GOOGLE:
                return 'Im Play Store herunterladen';
            case self::APP_STORE_APPLE:
                return 'Im App Store herunterladen';
        }
        return null;
    }

    public function getAppStoreData($app_store, $app_id)
    {
        global $CONFIG;

        $cache_id = md5("$app_store-$app_id");

        if (CacheHelper::cacheExists('appbox', $cache_id, 60 * 24 * 30)) {
            return CacheHelper::loadCache('appbox', $cache_id);
        }

        $data = null;
        switch ($app_store) {
            case self::APP_STORE_GOOGLE:
                $google_app_store_data_service = new GoogleAppStoreDataService();
                $data = $google_app_store_data_service->getAppData($app_id);
                break;
            case self::APP_STORE_APPLE:
                $apple_app_store_data_service = new AppleAppStoreDataService();
                $data = $apple_app_store_data_service->getAppData($app_id);
                break;
        }

        if ($data) {
            $image_url = $data['icon'];

            FileHelper::createDirIfNotExists(GENERATED_IMG_DIR . "apps");

            $image_storage_path = GENERATED_IMG_DIR . "apps";

            $filename = "$app_store-" . md5($app_id);

            $path = (new FileManager())->downloadFileFromUrl($image_url, $image_storage_path, $filename);

            $extension = null;

            if(file_exists($path)) {
                $mime_type = mime_content_type($path);
                switch ($mime_type) {
                    case "image/jpeg":
                        $extension = "jpeg";
                        break;
                    case "image/png":
                        $extension = "png";
                        break;
                }
            }

            if (file_exists($path) && $extension) {
                rename($path, GENERATED_IMG_DIR . "apps/$filename.$extension");
                $data['icon'] = $CONFIG->website_domain . GENERATED_IMG_SRC . "apps/$filename.$extension";
            }
        }

        CacheHelper::saveCache('appbox', $cache_id, $data);

        return $data;
    }

    public function getAppDownloadInfo($app_store, $app_id)
    {
        $icon = $this->getAppStoreIcon($app_store);
        $href = $this->getStoreUrl($app_store, $app_id);
        $text = $this->getButtonText($app_store);
        $app_data = $this->getAppStoreData($app_store, $app_id);

        if (!$app_data) {
            $button_code = '[button size="btn-md" block="true" color="btn-primary" icon="' . $icon . '" 
                icon_position="left" href="' . $href . '" target="_blank" rel="nofollow"]' . $text . '[/button]';
            return ShortcodeHelper::doShortcode($button_code);
        }

        return BufferHelper::buffered(function () use ($icon, $href, $text, $app_data) {
            (new Card([
                new Row([
                    new Column([
                        new Image($app_data['icon'], 100)
                    ], ['col-lg-4', 'col-md-6', 'col-12']),
                    new Column([
                        new GridContainer([
                            new Text($app_data['name'], 'strong'),
                            new BreakComponent(),
                            new LinkButton($href, $text, $icon, false, true)
                        ], 1)
                    ], ['col-lg-8', 'col-md-6', 'col-12'])
                ])
            ]))->display();
        });
    }
}