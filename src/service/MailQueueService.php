<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\service\MailService;
use saschahuber\saastemplatecore\service\Service;
use stdClass;

#[AllowDynamicProperties]
class MailQueueService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->mail_service = new MailService();
    }

    public function addScheduledMailToQueue($recipient, $cc_recipients, $subject, $content, $priority = 0, $earliest_send = null, $latest_send = null)
    {
        return $this->addMailToQueue($recipient, $cc_recipients, $subject, $content, null, $priority, $earliest_send, $latest_send);
    }

    public function addSentMailToQueue($recipient, $cc_recipients, $subject, $content, $send_time = null)
    {
        if ($send_time === null) {
            $send_time = date('Y-m-d H:i:s');
        }
        $this->addMailToQueue($recipient, $cc_recipients, $subject, $content, $send_time);
    }

    public function addMailToQueue($recipient, $cc_recipients, $subject, $content, $send_time = null, $priority = 0, $earliest_send = null, $latest_send = null)
    {
        $mail = new stdClass();
        $mail->recipient = $recipient;
        $mail->cc_recipients = $cc_recipients;
        $mail->subject = $subject;
        $mail->content = $content;
        $mail->send_time = $send_time;
        $mail->priority = $priority;
        $mail->earliest_send = $earliest_send;
        $mail->latest_send = $latest_send;
        return $this->DB->insertFromObject('mail', $mail);
    }

    public function getQueue($limit = 10, $max_fails = 3)
    {
        $conditions = [
            'send_time is null',
            'fail_count < ' . intval($max_fails)
        ];

        $query_builder = new DatabaseSelectQueryBuilder('mail');
        $query_builder->conditions($conditions)
            ->limit($limit)
            ->order('creation_time ASC');

        return $this->DB->getAll($query_builder->buildQuery());
    }

    public function markAsSent($mail_id, $send_info = null)
    {
        $this->DB->query("update mail set send_time = CURRENT_TIMESTAMP(), 
                send_info = '" . ($send_info !== null ? '"' . $send_info . '"' : 'null') . "' 
                where mail_id = " . intval($mail_id));
    }

    public function markAsFailed($mail_id, $send_info = "")
    {
        $this->DB->query("update mail set fail_count = fail_count+1,
                send_info = '" . ($send_info ? '"' . $send_info . '"' : 'null') . "' 
                where mail_id = ".intval($mail_id));
    }
}