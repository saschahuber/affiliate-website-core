<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class FaqService extends Service
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllFaqs()
    {
        $faq_query = $this->DB->query("SELECT * FROM faq order by title ASC");
        $faqs = [];
        while ($item = $faq_query->fetchObject()) {
            $faqs[] = $item;
        }
        return $faqs;
    }

    public function getFaq($faq_id)
    {
        $faq = $this->DB->query("SELECT * FROM faq where faq_id = " . intval($faq_id))->fetchObject();

        if ($faq) {
            $faq->questions = $this->getQuestionsFromId($faq_id, false);
        }

        return $faq;
    }

    public function getQuestionsFromId($faq_id, $only_active_questions = true)
    {
        $questions = [];

        $dbquery = $this->DB->query("SELECT faq__question.* FROM faq__question
                       join faq on faq__question.faq_id = faq.faq_id
                       where faq.faq_id = " . intval($faq_id) . " " . ($only_active_questions ? 'and active = 1' : '') . " 
                       order by item_order ASC, faq__question_id ASC");
        while ($item = $dbquery->fetchObject()) {
            $questions[] = ['question' => $item->question, 'answer' => $item->answer];
        }

        return $questions;
    }
}