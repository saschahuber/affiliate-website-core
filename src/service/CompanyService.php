<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class CompanyService extends Service
{

    public function __construct()
    {
        parent::__construct();
        $this->company_manager = new CompanyManager();
    }

    public function deleteCompany($company_id)
    {
        return $this->DB->doTransaction(function() use($company_id) {
            $this->DB->query('DELETE FROM company__action_log where company_id = ' . intval($company_id));

            $this->DB->query('DELETE FROM company__meta where company_id = ' . intval($company_id));

            $this->DB->query('DELETE FROM company__opening_hours where company_id = ' . intval($company_id));

            $this->DB->query('DELETE FROM company__taxonomy_mapping where company_id = ' . intval($company_id));

            $this->company_manager->delete($company_id);

            return true;
        });
    }
}