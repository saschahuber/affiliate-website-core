<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentNavLink;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\service\DashboardMenuService;

#[AllowDynamicProperties]
class AffiliateDashboardMenuService extends DashboardMenuService {
    public function __construct(){
        parent::__construct();
    }

    static function displayDashboardMenu($current_path){
        $items = self::getDashboardMenuItems();
        return self::displayMenuItems($items, $current_path);
    }

    static function displayMenuItems($items, $current_path){
        ob_start();
        foreach($items as $item){
            self::displayMenuItem($item, $current_path);
        }
        $content = ob_get_clean();

        return $content;
    }

    static function isCurrentUrl($url, $path){
        $url_parts = explode('/', trim($url, '/'));

        for($i = 0; $i < count($url_parts); $i++){
            if($i > count($path)-1){
                return true;
            }

            if($path[$i] !== $url_parts[$i]){
                return false;
            }
        }

        return true;
    }

    static function displayMenuItem($item, $current_path, $classes=[]){
        global $CONFIG;

        $has_sub_items = isset($item['sub_items']) && count($item['sub_items']) > 0;

        if(count($item['needed_permissions']) > 0 && !AuthHelper::hasAtleastOnePermission($item['needed_permissions'])){
            return;
        }

        $is_active = self::isCurrentUrl($item['url'], array_filter($current_path));

        $icon = "";
        if($item['icon'] !== null){
            if(strpos($item['icon'], 'fa') === 0) {
                $icon = '<i class="nav-item-icon '.$item['icon'].'"></i>';
            }
            #else {
            #    $icon = '<img class="menu-item-icon" src="'.$item['icon'].'">';
            #}
        }

        $badge = null;
        if(array_key_exists('needed_packages', $item)
            && count($item['needed_packages']) > 0
            && !self::hasAnyPackage($item['needed_packages'])){
            ?>
            <li class="nav-item <?=implode(' ', $classes)?> <?=(($is_active)?'active':'')?>">
                <?php
                $content = $icon . $item['title'] . '<span class="float-right"><i class="fas fa-lock"></i></span>';

                $button_classes = [];
                if($is_active){
                    $button_classes[] = 'active';
                }

                (new GlobalAsyncComponentNavLink($content, 'LockedMenuItemInfoHandler', ['item_name' => $item['title'], 'packages' => $item['needed_packages']], null, $button_classes))->display();
                ?>
            </li>
            <?php
            return;
        }

        if(array_key_exists("async", $item) && $item["async"]){
            ?>
            <li class="nav-item <?=implode(' ', $classes)?> <?=(($is_active)?'active':'')?>">
                <a class="nav-link <?=Helper::ifstr($is_active, 'active')?>" aria-current="page"
                   onclick="selectAsyncPage(this, '<?=$item['title'] . $CONFIG->title_suffix?>', '<?=$item['url']?>')">
                    <?=$icon?><?=$item['title']?>
                    <?php if($has_sub_items): ?>
                        <span class="nav-subitems-toggle inactive" onclick="toggleNavSubitems(this, event);">
                            <i class="fas fa-chevron-circle-down"></i>
                        </span>
                    <?php endif; ?>
                </a>

                <?php
                if($has_sub_items){
                    ?><ul class="nav nav-subitem-list minimized nav-pills flex-column mb-auto"><?php

                    foreach($item['sub_items'] as $sub_item){
                        self::displayMenuItem($sub_item, $current_path, ['nav-item-child']);
                    }

                    ?></ul><?php
                }
                ?>
            </li>
            <?php
        }
        else{
            ?>
            <li class="nav-item <?=implode(' ', $classes)?> <?=(($is_active)?'active':'')?>">
                <a href="<?=$item['url']?>" class="nav-link <?=Helper::ifstr($is_active, 'active')?>" aria-current="page">
                    <span><?=$icon?><?=$item['title']?></span>
                    <?php if($has_sub_items): ?>
                        <span class="nav-subitems-toggle inactive" onclick="toggleNavSubitems(this, event);">
                            <i class="fas fa-chevron-circle-down"></i>
                        </span>
                    <?php endif; ?>
                </a>

                <?php
                if($has_sub_items){
                    ?><ul class="nav nav-subitem-list minimized nav-pills flex-column mb-auto"><?php

                    foreach($item['sub_items'] as $sub_item){
                        self::displayMenuItem($sub_item, $current_path, ['nav-item-child']);
                    }

                    ?></ul><?php
                }
                ?>
            </li>
            <?php
        }
    }
}
