<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\persistence\GuideFilterRepository;
use saschahuber\affiliatewebsitecore\persistence\GuideFilterStepOptionFilterRepository;
use saschahuber\affiliatewebsitecore\persistence\GuideFilterStepOptionRepository;
use saschahuber\affiliatewebsitecore\persistence\GuideFilterStepRepository;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class GuideFilterService extends Service{
    public function __construct(){
        parent::__construct();

        $this->guide_filter_repository = new GuideFilterRepository();
        $this->guide_filter_step_repository = new GuideFilterStepRepository();
        $this->guide_filter_step_option_repository = new GuideFilterStepOptionRepository();
        $this->guide_filter_step_option_filter_repository = new GuideFilterStepOptionFilterRepository();
    }

    public function getGuideFilter($id){
        return $this->guide_filter_repository->getById($id);
    }

    public function saveGuideFilter($guide_filter){
        return $this->guide_filter_repository->save($guide_filter);
    }

    public function getGuideFilterStep($id){
        return $this->guide_filter_step_repository->getById($id);
    }

    public function saveGuideFilterStep($guide_filter_step){
        return $this->guide_filter_step_repository->save($guide_filter_step);
    }

    public function getGuideFilterOption($id){
        return $this->guide_filter_step_option_repository->getById($id);
    }

    public function saveGuideFilterOption($guide_filter_step_option){
        return $this->guide_filter_step_option_repository->save($guide_filter_step_option);
    }

    public function getGuideFilterOptionFilter($id){
        return $this->guide_filter_step_option_filter_repository->getById($id);
    }

    public function saveGuideFilterOptionFilter($guide_filter_step_option_filter){
        return $this->guide_filter_step_option_filter_repository->save($guide_filter_step_option_filter);
    }
}
