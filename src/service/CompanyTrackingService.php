<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\service\Service;
use stdClass;

#[AllowDynamicProperties]
class CompanyTrackingService extends Service
{
    const TRACKING_TYPE_CLICK_PHONE = "click_phone";
    const TRACKING_TYPE_CLICK_MAIL = "click_mail";
    const TRACKING_TYPE_CLICK_WEBSITE = "click_website";
    const TRACKING_TYPE_VIEW = "view";

    const ALLOWED_TRACKING_TYPES = [
        self::TRACKING_TYPE_CLICK_MAIL,
        self::TRACKING_TYPE_CLICK_PHONE,
        self::TRACKING_TYPE_CLICK_WEBSITE,
        self::TRACKING_TYPE_VIEW
    ];

    const TRACKING_TYPE_LABELS = [
        self::TRACKING_TYPE_CLICK_MAIL => 'Mail-Lick',
        self::TRACKING_TYPE_CLICK_PHONE => 'Telefon-Klick',
        self::TRACKING_TYPE_CLICK_WEBSITE => 'Website-Klick',
        self::TRACKING_TYPE_VIEW => 'Aufruf'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function trackCompanyAction($company_id, $action)
    {
        if (!in_array($action, self::ALLOWED_TRACKING_TYPES)) {
            ErrorHelper::http(400, "Typ nicht erlaubt");
        }

        $company_manager = new CompanyManager();
        $company = $company_manager->getById($company_id);
        if (!$company) {
            ErrorHelper::http(404, "Unternehmen nicht gefunden");
        }

        $company_action_log = new stdClass();
        $company_action_log->company_id = $company_id;
        $company_action_log->action = $action;
        $company_action_log->user_id = AuthHelper::getCurrentUserId();
        $company_action_log->user_ip = RequestHelper::getClientIp();
        $company_action_log->user_agent = ArrayHelper::getArrayValue($_SERVER, 'HTTP_USER_AGENT', null);
        $company_action_log->session_id = session_id();
        $this->DB->insertFromObject('company__action_log', $company_action_log);
    }
}