<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\persistence\AuthorLinkRepository;
use saschahuber\affiliatewebsitecore\persistence\AuthorRepository;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class AuthorService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->author_repository = new AuthorRepository();
        $this->author_link_repository = new AuthorLinkRepository();
    }

    public function getById($item_id)
    {
        $author = $this->author_repository->getById($item_id);
        $this->addAuthorLinks($author);
        return $author;
    }

    public function getByPermalink($permalink)
    {
        $author = $this->author_repository->getByPermalink($permalink);
        $this->addAuthorLinks($author);
        return $author;
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0)
    {
        $authors = $this->author_repository->getAll($cache, $cache_lifetime_minutes);
        foreach($authors as $author){
            $this->addAuthorLinks($author);
        }
        return $authors;
    }

    public function addAuthorLinks(&$author){
        if($author) {
            $author->links = $this->author_link_repository->getAllByAuthorId($author->id);
        }
    }

    public function save($item){
        return $this->author_repository->save($item);
    }
}
