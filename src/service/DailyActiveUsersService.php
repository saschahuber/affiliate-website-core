<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class DailyActiveUsersService extends Service
{
    function __construct()
    {
        parent::__construct();
    }

    function getDailyActiveUsers($day_range = 30)
    {
        $query = "SELECT TIMESTAMPDIFF(DAY, time, NOW()) as log_index,
                DATE_FORMAT(time, '%a, %d.%m.%Y') as formatted_date,
                count(DISTINCT(utm_log_id)) as hits
                FROM user__tracking_log
                where TIMESTAMPDIFF(DAY, time, NOW()) < " . intval($day_range) . "
                group by DATE_FORMAT(time, '%a, %d.%m.%Y')
                order by log_index desc, time asc";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[] = $item;
        }
        return $items;
    }
}