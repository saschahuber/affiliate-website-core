<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use DateTime;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\persistence\ApplicationAnalyticsRepository;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class TextPromotionService extends Service {
    public function __construct(){
        parent::__construct();

        $this->vg_wort_pixel_service = new VgWortPixelService();
    }

    public function getAllAvailableRedirects(){
        global $CONFIG;

        $max_views = 1500;
        $min_length = 5000;

        list($start_date, $end_date) = $this->getCurrentInterval();

        $query_builder = new DatabaseSelectQueryBuilder('vg_wort_pixel');

        $query_builder->conditions([
                'text_length >= ' . intval($min_length),
                'views_in_interval < ' . intval($max_views)
            ])
            ->groupBy('vg_wort_pixel.vg_wort_pixel_id');

        return $this->DB->getAll($query_builder->buildQuery());
    }
    
    public function redirect(){
        global $CONFIG;

        $vg_wort_item = ArrayHelper::rand($this->getAllAvailableRedirects());

        $redirect_url = $CONFIG->website_domain . $vg_wort_item->relative_page_url . ($_GET ? '?'.http_build_query($_GET) : '');

        UrlHelper::redirect($redirect_url, 302);
    }

    public function updateTextLength(){
        $mapped_pixels = $this->vg_wort_pixel_service->getAllMappedPixels();

        foreach($this->vg_wort_pixel_service->getAll() as $vg_wort_pixel){
            $pixel_id = $vg_wort_pixel->vg_wort_pixel_id;

            $content_length = 0;
            if(array_key_exists($vg_wort_pixel->vg_wort_pixel_id, $mapped_pixels)){
                $mapped_pixel = $mapped_pixels[$vg_wort_pixel->vg_wort_pixel_id];
                $content_element = getContentElementByTypeAndId($mapped_pixel['element_type'], $mapped_pixel['element_id']);

                if($content_element->status === Manager::STATUS_PUBLISH) {
                    $content = $content_element->content;
                    $cleaned_content = preg_replace('/\s+/', ' ', strip_tags(ShortcodeHelper::doShortcode($content)));
                    $content_length = strlen($cleaned_content);
                }
            }
            else if(strlen($vg_wort_pixel->relative_page_url) > 0){
                $content_length = 5000;
            }
            $this->DB->query('UPDATE vg_wort_pixel set text_length = '.intval($content_length). ' WHERE vg_wort_pixel_id = ' . intval($pixel_id));
        }
    }

    public function updateViewsForThisInterval(){
        global $CONFIG;

        list($start_date, $end_date) = $this->getCurrentInterval();

        $mapped_pixels = $this->vg_wort_pixel_service->getAllMappedPixels();

        foreach($this->vg_wort_pixel_service->getAll() as $vg_wort_pixel){
            $pixel_id = $vg_wort_pixel->vg_wort_pixel_id;

            $permalink = null;
            if(array_key_exists($vg_wort_pixel->vg_wort_pixel_id, $mapped_pixels)){
                $mapped_pixel = $mapped_pixels[$vg_wort_pixel->vg_wort_pixel_id];
                $permalink = $CONFIG->website_domain . getContentPermalinkElementByTypeAndId($mapped_pixel['element_type'], $mapped_pixel['element_id']);
            }
            else if(strlen($vg_wort_pixel->relative_page_url) > 0){
                $permalink = trim($CONFIG->website_domain . $vg_wort_pixel->relative_page_url, '/');
            }

            $item = null;
            if($permalink) {
                $query_builder = new DatabaseSelectQueryBuilder(ApplicationAnalyticsRepository::BASE_TABLE);
                $query_builder->select('count(*) as views')
                    ->conditions([
                        'application_analytics.url = "' . $this->DB->escape($permalink) . '"',
                        'log_time >= "' . $this->DB->escape($start_date) . '"',
                        'log_time <= "' . $this->DB->escape($end_date) . '"'
                    ]);
                $item = $this->DB->getOne($query_builder->buildQuery());
            }


            $view_count = 0;
            if($item) {
                $view_count = $item->views;
            }
            $this->DB->query('UPDATE vg_wort_pixel set views_in_interval = ' . intval($view_count) . ' WHERE vg_wort_pixel_id = ' . intval($pixel_id));
        }
    }

    function getCurrentInterval(){
        $intervalStartDate = "10-01";
        $intervalEndDate = "09-30";
        
        // Aktuelles Datum
        $currentDate = new DateTime();

        // Jahr des aktuellen Datums
        $currentYear = (int)$currentDate->format('Y');

        // Erstelle Start- und Enddaten der Intervalle
        $intervalStart = new DateTime("{$currentYear}-" . $intervalStartDate);
        $intervalEnd = new DateTime(($currentYear + 1) . "-" . $intervalEndDate);

        // Überprüfen, ob das aktuelle Datum vor dem 01.10. liegt
        if ($currentDate < $intervalStart) {
            $intervalStart = new DateTime(($currentYear - 1) . "-" . $intervalStartDate);
            $intervalEnd = new DateTime("{$currentYear}-" . $intervalEndDate);
        }
        
        return [$intervalStart->format('Y-m-d H:i:s'), $intervalEnd->format('Y-m-d 23:59:59')];
    }
}
