<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\persistence\RedirectGroupItemRepository;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class RedirectGroupItemService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->redirect_group_item_repository = new RedirectGroupItemRepository();
    }

    public function getById($item_id)
    {
        return $this->redirect_group_item_repository->getById($item_id);
    }

    public function getByRedirectGroupId($redirect_group_id)
    {
        return $this->redirect_group_item_repository->getByRedirectGroupId($redirect_group_id);
    }

    public function getActiveGroupItems($redirect_group_id)
    {
        return $this->redirect_group_item_repository->getActiveGroupItems($redirect_group_id);
    }

    public function getAll(bool $cache = false, int $cache_lifetime_minutes = 0)
    {
        return $this->redirect_group_item_repository->getAll($cache, $cache_lifetime_minutes);
    }

    public function save($item)
    {
        return $this->redirect_group_item_repository->save($item);
    }

    public function trackHit($redirect_group_item_id)
    {
        $this->redirect_group_item_repository->trackHit($redirect_group_item_id);
    }
}
