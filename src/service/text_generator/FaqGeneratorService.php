<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;
use stdClass;

class FaqGeneratorService extends Service
{
    function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
        $this->faq_service = new FaqService();
    }

    public function createFaqs($faq_id)
    {
        $faq = $this->faq_service->getFaq($faq_id);

        if (!$faq || count($faq->questions)) {
            ErrorHelper::api(400, 'FAQ existiert nicht oder hat bereits Fragen');
        }

        $topic = $faq->title;

        $prompt = "Erstelle mir ein FAQ zum Thema \"$topic\" in der \"Du\"-Form als JSON-Array."
            . " Das Array soll folgendes Format haben: [{'question': '', 'answer': ''}]"
            . " Gib außer dem validen JSON-Code keinen anderen Inhalt aus.";

        #$template = new TextGenerationTemplate($prompt, TextGenerationTemplate::MODEL_DAVINCI, 2000);
        #$result = $this->text_generation_service->generateTextFromTemplate($template);

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];
        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT, 2000);
        $result = $this->text_generation_service->getNextChatMessage($template);

        $result = $result->getContent();

        $faqs = json_decode($result, true);

        for ($index = 0; $index < count($faqs); $index++) {
            $faq_item = $faqs[$index];
            $faq_question = new stdClass();
            $faq_question->faq_id = $faq->id;
            $faq_question->item_order = $index;
            $faq_question->question = $faq_item['question'];
            $faq_question->answer = $faq_item['answer'];
            $this->DB->insertFromObject('faq__question', $faq_question);
        }
    }
}