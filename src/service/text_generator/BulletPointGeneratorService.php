<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\saastemplatecore\model\TextGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class BulletPointGeneratorService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
    }

    public function generateBulletPoints($topic)
    {
        $template = $this->getTemplate($topic);

        $result = $this->text_generation_service->generateTextFromTemplate($template);

        return $result->getContent();
    }

    private function getTemplate($topic)
    {
        $prompt = "Aufgabe: Erstelle einen Instagram-Post zum Thema '$topic'.
        Stelle meinen Followern eine Frage, um sie zum Kommentieren und Liken zu animieren.
        Füge am Ende passende Hashtags hinzu.";
        return new TextGenerationTemplate($prompt, TextGenerationTemplate::MODEL_DAVINCI, 2000);
    }
}
