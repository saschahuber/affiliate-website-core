<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class ProductContentGeneratorService extends Service
{
    const LINE_SEPARATOR = PHP_EOL;
    const TEXT_LENGTH = 750;

    function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
        $this->brand_manager = new BrandManager();
    }

    public function getProductTextGenerationPrompt($product_id, $product_title)
    {
        $product = $this->product_manager->getById($product_id);

        $prompt = 'Erstelle eine ausführliche und professionelle Produktbeschreibung für das Produkt "' . $product_title . '"';

        $brands = $this->brand_manager->getBrandsByProductId($product_id);
        if(count($brands)) {
            $prompt .= ' vom Hersteller ' . $brands[0]->title;
        }

        $prompt .= '.' . self::LINE_SEPARATOR;

        if(count($product->taxonomies)){
            $taxonomy_names = [];
            foreach ($product->taxonomies as $taxonomy){
                $taxonomy_names[] = $taxonomy->title;
            }

            $prompt .= 'Es gehört zu folgenden Produktkategorien: '.implode(', ', $taxonomy_names).'.' . self::LINE_SEPARATOR;
        }

        $product_data = $this->product_manager->getProductData($product);
        if ($product_data) {
            $prompt .= $this->getProductAttributesPromptPart($product_data);
        }

        $product_info = unserialize($product->meta['product_info']);
        $features = $product_info['Features']['DisplayValues'];
        if(count($features)) {
            $prompt .= 'Das ist die Produktbeschreibung bei Amazon:' . self::LINE_SEPARATOR .
                implode(';'.self::LINE_SEPARATOR, $features) . self::LINE_SEPARATOR . self::LINE_SEPARATOR;
        }

        $prompt .= "Formattiere die Beschreibung als HTML und strukturiere den Text mit Überschriften und markiere wichtige Teile als fett." . self::LINE_SEPARATOR;

        $prompt .= "Formuliere den Text in der 'Du'-Form. "
            ."Falls der Eingabetext in der 'Sie'-Form geschrieben ist, formuliere ihn bitte in die 'Du'-Form um. "
            ."Der Text sollte mindestens ".self::TEXT_LENGTH." Wörter haben.";

        return $prompt;
    }

    public function getProductAttributesPromptPart($product_data)
    {
        $product_data_items = [];
        foreach ($product_data as $data) {
            $value = $data->value;

            switch ($data->field_type) {
                case "true_false":
                    if (boolval($value)) {
                        $value = 'Ja';
                    } else {
                        $value = 'Nein';
                    }
                #case "select":
                #    $values = unserialize($value);
                #    $values = array_values($values);
                #    $prefix = (($data->field_prefix!==null && strlen($data->field_prefix)>0) ? $data->field_prefix : "");
                #    $suffix = (($data->field_suffix!==null && strlen($data->field_suffix)>0) ? $data->field_suffix : "");
                #    $value = $prefix.strval(implode(', ', $values)).$suffix;
                case "number":
                default:
                    $prefix = (($data->field_prefix !== null && strlen($data->field_prefix) > 0) ? $data->field_prefix : "");
                    $suffix = (($data->field_suffix !== null && strlen($data->field_suffix) > 0) ? $data->field_suffix : "");
                    $value = $prefix . strval($value) . $suffix;
            }

            $product_data_items[] = $data->field_name . ': '
                . ($data->field_prefix ? $data->field_prefix . ' ' : '')
                . $value
                . ($data->field_suffix ? ' ' . $data->field_suffix : '');
        }

        return 'Das Produkt hat folgende Eigenschaften: {' . implode(';' . self::LINE_SEPARATOR, $product_data_items) . '}';
    }
}