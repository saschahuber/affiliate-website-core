<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class ProductReviewGeneratorService extends Service
{
    private TextGenerationService $text_generation_service;
    private $product_manager;

    function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
    }

    public function createIntroduction($product_title)
    {
        $prompt = 'Schreibe eine Einleitung für einen Testbericht zum Produkt "' . $product_title . '". Verfasse den Bericht in der "Du"-Form.' . PHP_EOL;

        #$template = new TextGenerationTemplate($prompt, TextGenerationTemplate::MODEL_DAVINCI, 2000);
        #$result = $this->text_generation_service->generateTextFromTemplate($template);

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];
        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT, 2000);
        $result = $this->text_generation_service->getNextChatMessage($template);

        $result = $result->getContent();

        $items = [];
        foreach (array_filter(explode("\n", trim($result, "\n"))) as $item) {
            $item = preg_replace('/\d+\.\s/i', '', $item);
            $items[] = $item;
        }

        return $items;
    }

    public function getProductAttributesPromptPart($product_data)
    {
        $product_data_items = [];
        foreach ($product_data as $data) {
            $value = $data->value;

            switch ($data->field_type) {
                case "true_false":
                    if (boolval($value)) {
                        $value = 'Ja';
                    } else {
                        $value = 'Nein';
                    }
                #case "select":
                #    $values = unserialize($value);
                #    $values = array_values($values);
                #    $prefix = (($data->field_prefix!==null && strlen($data->field_prefix)>0) ? $data->field_prefix : "");
                #    $suffix = (($data->field_suffix!==null && strlen($data->field_suffix)>0) ? $data->field_suffix : "");
                #    $value = $prefix.strval(implode(', ', $values)).$suffix;
                case "number":
                default:
                    $prefix = (($data->field_prefix !== null && strlen($data->field_prefix) > 0) ? $data->field_prefix : "");
                    $suffix = (($data->field_suffix !== null && strlen($data->field_suffix) > 0) ? $data->field_suffix : "");
                    $value = $prefix . strval($value) . $suffix;
            }

            $product_data_items[] = $data->field_name . ': '
                . ($data->field_prefix ? $data->field_prefix . ' ' : '')
                . $value
                . ($data->field_suffix ? ' ' . $data->field_suffix : '');
        }

        return 'Das Produkt hat folgende Eigenschaften: {' . implode(';' . PHP_EOL, $product_data_items) . '}';
    }

    public function createAttributeDescription($product_id, $product_title, $pro, $contra)
    {
        $product = $this->product_manager->getById($product_id);

        $prompt = 'Schreibe einen Informations-Text zum Produkt "' . $product_title . '". '
            . 'Verfasse den Bericht in der "Du"-Form.' . PHP_EOL;

        $product_data = $this->product_manager->getProductData($product);
        if ($product_data) {
            $prompt .= $this->getProductAttributesPromptPart($product_data);
        }

        #return $prompt;

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];
        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT, 3500);
        $result = $this->text_generation_service->getNextChatMessage($template);

        #$template = new TextGenerationTemplate($prompt, TextGenerationTemplate::MODEL_DAVINCI, 2000);
        #$result = $this->text_generation_service->generateTextFromTemplate($template);

        return $result->getContent();
    }

    public function createReview($product_id, $product_title, $pro, $contra)
    {
        $product = $this->product_manager->getById($product_id);

        $prompt = 'Schreibe einen ausführlichen Testbericht zum Produkt "' . $product_title . '". '
            . 'Verfasse den Bericht in der "Du"-Form. Der Bericht sollte mindestens 500 Wörter enthalten.' . PHP_EOL;

        if ($pro) {
            $prompt .= 'Das Produkt hat folgende Vorteile: {' . PHP_EOL;
            $prompt .= implode(';' . PHP_EOL, $pro) . '}.' . PHP_EOL . PHP_EOL;
        }

        if ($contra) {
            $prompt .= 'Außerdem hat das Produkt die folgenden Nachteile: {' . PHP_EOL;
            $prompt .= implode(';' . PHP_EOL, $contra) . '}.' . PHP_EOL . PHP_EOL;
        }


        $product_data = $this->product_manager->getProductData($product);
        if ($product_data) {
            $prompt .= $this->getProductAttributesPromptPart($product_data);
        }

        #return $prompt;

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];
        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT, 3500);
        $result = $this->text_generation_service->getNextChatMessage($template);

        #$template = new TextGenerationTemplate($prompt, TextGenerationTemplate::MODEL_DAVINCI, 2000);
        #$result = $this->text_generation_service->generateTextFromTemplate($template);

        return $result->getContent();
    }

    public function createSummary($post_title, $review)
    {
        $prompt = 'Fasse den folgenden Testbericht in einem Fazit zusammen: ' . $review;

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];
        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT, 2000);
        $result = $this->text_generation_service->getNextChatMessage($template);

        #$template = new TextGenerationTemplate($prompt, TextGenerationTemplate::MODEL_DAVINCI, 2000);
        #$result = $this->text_generation_service->generateTextFromTemplate($template);

        return $result->getContent();
    }
}