<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class SocialPostCaptionGeneratorService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
    }

    public function generateSocialPost($content, $platform, $hashtags = false)
    {
        $prompt = "Erstelle einen $platform-Post zu folgendem Inhalt: '$content'.";

        #$prompt = "Aufgabe: Erstelle einen $platform-Post zum folgenden Inhalt.
        #Stelle meinen Followern anschließend eine Frage, um sie zum Kommentieren und Liken zu animieren.
        #Inhalt:'$topic'.";

        if ($hashtags) {
            $prompt .= "Füge am Ende passende Hashtags hinzu.";
        }

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];
        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT, 500);
        $result = $this->text_generation_service->getNextChatMessage($template);

        return trim($result->getContent());
    }
}
