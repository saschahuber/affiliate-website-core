<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class HtmlTextRewriterService extends Service
{
    function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
    }

    public function rewriteText($text)
    {
        $bullet_points = $this->getTextBulletPoints($text);

        echo json_encode($bullet_points) . PHP_EOL;

        return $this->createTextFromBulletPoints($bullet_points);
    }

    private function getTextBulletPoints($text){
        $prompt = "Erstelle mir eine möglichst ausgiebige Liste mit Stichpunkten, worum es in diesem Inhalt geht. Gib mir eine nur validen JSON-Code einer Liste mit Stichpunkten zurück. "
            . "Verwende dafür das folgende Format ['Dies ist ein Stichpunkt 1', 'Dies ist ein Stichpunkt 2', 'Dies ist noch ein Stichpunkt 3']\n\n"
            . "Hier ist der Inhalt: \n\n$text";

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT_16K, 8000);

        $result = $this->text_generation_service->getNextChatMessage($template);

        return json_decode($result->getContent(), true);
    }

    private function createTextFromBulletPoints($bullet_points){

        $prompt = "Erstelle mir einen ausgiebigen News-Artikel, der die folgenden Inhalte besitzt:\n\n" . implode("\n", $bullet_points);

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT_16K, 8000);

        $result = $this->text_generation_service->getNextChatMessage($template);

        return $result->getContent();
    }
}