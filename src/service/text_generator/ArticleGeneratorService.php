<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class ArticleGeneratorService extends Service
{
    function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
    }

    public function createArticleContentList($titel)
    {
        $prompt = 'Schreibe ein Inhaltsverzeichnis für einen Blog-Artikel mit dem Titel "' . $titel . '" mit mindestens 10 Unterpunkten.'
            . ' Gib das ganze als nummerierte Liste aus. Gib vor und nach der Liste keinen Text aus.'
            . ' Formuliere den Inhalt in der "du"-Form.';

        #$template = new TextGenerationTemplate($prompt, TextGenerationTemplate::MODEL_DAVINCI, 2000);
        #$result = $this->text_generation_service->generateTextFromTemplate($template);

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];
        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT, 2000);
        $result = $this->text_generation_service->getNextChatMessage($template);

        $result = $result->getContent();

        $items = [];
        foreach (array_filter(explode("\n", trim($result, "\n"))) as $item) {
            $item = preg_replace('/\d+\.\s/i', '', $item);
            $items[] = $item;
        }

        return $items;
    }

    public function createArticleParagraph($post_title, $headline)
    {
        $prompt = 'fFhre den inhalt der Zwischenüberschrift "' . $headline . '" eines Blog-Artikels aus.'
            . ' Das Hauptthema des Artikels lautet "' . $post_title . '".'
            . ' Der Absatz sollte 100 bis 200 Wörter besitzen. Formuliere den Inhalt in der "du"-Form.';

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];
        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT, 2000);
        $result = $this->text_generation_service->getNextChatMessage($template);

        #$template = new TextGenerationTemplate($prompt, TextGenerationTemplate::MODEL_DAVINCI, 2000);
        #$result = $this->text_generation_service->generateTextFromTemplate($template);

        return $result->getContent();
    }
}