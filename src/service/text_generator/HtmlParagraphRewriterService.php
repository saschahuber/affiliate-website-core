<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class HtmlParagraphRewriterService extends Service
{
    function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
    }

    public function rewriteParagraph($text)
    {
        $template = $this->getTemplate($text);

        $result = $this->text_generation_service->getNextChatMessage($template);

        return $result->getContent();
    }

    private function getTemplate($text)
    {
        $prompt = "Aufgabe: Formuliere den folgenden Text um. Füge die Links korrekt als HTML ein. Schreibe den Text in der 'Du'-Form.
        Inhalt: $text";

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];

        return new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT_16K, 8000);
    }
}