<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\affiliatewebsitecore\service\UrlScraperService;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class CompanyDescriptionGeneratorService extends Service {
    function __construct(){
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
    }

    public function createCompanyDescription($company_name, $address, $mail, $phone, $website, $model, $tokens, $company_id=null){
        $content_from_website = Helper::cleanTextFromUnneccessaryWords((new UrlScraperService())->parseUrlContent($website), 40000);

        $summarized_content = $this->summarizeContent($content_from_website, $model, $tokens);

        $company_description = $this->generateDescription($summarized_content, $company_name, $address, $mail, $phone, $website, $model, $tokens);

        $styled_company_description = $this->styleContent($company_description, $model, $tokens);

        return $styled_company_description;
    }

    private function summarizeContent($content, $model, $tokens){
        $prompt = "Fasse den folgenden Inhalt einer Website zusammen: $content";
        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, $model, $tokens);
        return $this->text_generation_service->getNextChatMessage($template)->getContent();
    }

        private function generateDescription($content, $company_name, $address, $mail, $phone, $website, $model, $tokens){
        $prompt = "Schreibe eine ausführliche und einzigartige Unternehmensbeschreibung (mindestens 300 Wörter) für das folgende Unternehmen.".PHP_EOL
            ."Formuliere die Beschreibung aus der 'Wir'-Perspektive und verwende die Informationen über das Unternehmen.".PHP_EOL
            ."Gehe auf die Leistungen ein und nenne die verfügbaren Kontaktinformationen. ".PHP_EOL
            ."Name des Unternehmens: $company_name; ".PHP_EOL
            ."Adresse: $address; ".PHP_EOL
            .Helper::ifstr($mail && strlen($mail), "E-Mail-Adresse: $mail; ".PHP_EOL)
            .Helper::ifstr($phone && strlen($phone), "Telefonnummer: $phone; ".PHP_EOL)
            .Helper::ifstr($website && strlen($website), "Website: $website; ".PHP_EOL)
            #."Kategorien: ".implode(', ', $categories):"; "
            ."Schreibe den Text in dem Stil, in dem es auch die Firma tun würde: $company_name".PHP_EOL
            ."Verwende folgenden Seed um die Ausgabe zu variieren: " . uniqid().PHP_EOL
            ."Weitere Informationen: $content";

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein erfahrener Marketing-Profi mit Fokus auf Unternehmensbeschreibungen.'],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, $model, $tokens, 1, 1);
        $result = $this->text_generation_service->getNextChatMessage($template);

        return $result->getContent();
    }

    private function styleContent($content, $model, $tokens){
        $prompt = "Formattiere diesen Inhalt mit HTML ansprechend. Verwende dabei nur die Tags div, p und strong:" . PHP_EOL . PHP_EOL. $content;

        $messages = [
            ['role' => 'system', 'content' => 'Du bist ein hilfreicher und kreativer Assistent.'],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, $model, $tokens);
        $result = $this->text_generation_service->getNextChatMessage($template);

        return $result->getContent();
    }
}