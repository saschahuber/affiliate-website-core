<?php

namespace saschahuber\affiliatewebsitecore\service\text_generator;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\ProductComparisonManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TextGenerationService;

class ProductComparisonGeneratorService extends Service
{
    const LINE_SEPARATOR = PHP_EOL;
    const TEXT_LENGTH = 750;

    protected TextGenerationService $text_generation_service;
    protected ProductComparisonManager $product_comparison_manager;
    protected $product_manager;
    protected BrandManager $brand_manager;

    function __construct()
    {
        parent::__construct();
        $this->text_generation_service = new TextGenerationService();
        $this->product_comparison_manager = new ProductComparisonManager();
        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
        $this->brand_manager = new BrandManager();
    }

    /*
    Erstelle einen ausführlichen und professionellen Produktvergleich für die Produkte [TITEL von HERSTELLER], [TITEL von HERSTELLER] und [TITEL von HERSTELLER].

    [PRODUKT 1] hat folgende Merkmale:
    Kategorien: [KATEGORIEN]
    Das Produkt hat folgende technische Daten:
    [PRODUKTDATEN]

    [PRODUKT 2] hat folgende Merkmale:
    Kategorien: [KATEGORIEN]
    Das Produkt hat folgende technische Daten:
    [PRODUKTDATEN]

    [PRODUKT 3] hat folgende Merkmale:
    Kategorien: [KATEGORIEN]
    Das Produkt hat folgende technische Daten:
    [PRODUKTDATEN]

    Formattiere die Beschreibung als HTML und strukturiere den Text mit Überschriften und markiere wichtige Teile als fett.
    Formuliere den Text in der 'Du'-Form.
    Falls der Eingabetext in der 'Sie'-Form geschrieben ist, formuliere ihn bitte in die 'Du'-Form um.
    Der Text sollte mindestens ".self::TEXT_LENGTH." Wörter haben.
    */

    public function getProductComparisonGenerationPrompt($product_comparison_id)
    {
        $product_comparison = $this->product_comparison_manager->getById($product_comparison_id);

        $products = [];

        $product_id_1 = $product_comparison->product_id_1;
        if($product_id_1) {
            $product_title_1 = $product_comparison->product_title_1;
            $products[] = [
                'id' => $product_id_1,
                'title' => $product_title_1,
                'product' => $this->product_manager->getById($product_id_1)
            ];
        }

        $product_id_2 = $product_comparison->product_id_2;
        if($product_id_2) {
            $product_title_2 = $product_comparison->product_title_2;
            $products[] = [
                'id' => $product_id_2,
                'title' => $product_title_2,
                'product' => $this->product_manager->getById($product_id_2)
            ];
        }

        $product_id_3 = $product_comparison->product_id_3;
        if($product_id_3) {
            $product_title_3 = $product_comparison->product_title_3;
            $products[] = [
                'id' => $product_id_3,
                'title' => $product_title_3,
                'product' => $this->product_manager->getById($product_id_3)
            ];
        }

        $product_titles = [];
        foreach($products as $product){
            $title = $product['product']->title;
            if($product['title']){
                $title = $product['title'];
            }

            $product_title = "'{$title}'";

            $brands = $this->brand_manager->getBrandsByProductId($product['id']);
            if(count($brands)) {
                $product_title .= ' vom Hersteller ' . $brands[0]->title;
            }

            $product_titles[] = $product_title;
        }

        $product_comparison_title = Helper::toReadableList($product_titles);

        $prompt = 'Erstelle einen ausführlichen und professionellen Produktvergleich für die Produkte ' . $product_comparison_title . '. '
            .'Verfasse den Produktvergleich als Fließtext und gehe detailliert auf die Unterschiede und Gemeinsamkeiten der einzelnen Produkte ein. '
            .'Stütze dich dabei vor allem auf die Eigenschaften.' . self::LINE_SEPARATOR . self::LINE_SEPARATOR;

        foreach($products as $product_container){
            $product = $product_container['product'];
            /*
            [PRODUKT 1] hat folgende Merkmale:
            Kategorien: [KATEGORIEN]
            Smarthome-Systeme: [SYSTEME]
            Das Produkt hat folgende technische Daten:
            [PRODUKTDATEN]
            */

            $title = $product->title;
            if($product_container['title']){
                $title = $product_container['title'];
            }

            $prompt_part = "'$title' hat folgende Merkmale:" . self::LINE_SEPARATOR;
            if(count($product->taxonomies)){
                $taxonomy_names = [];
                foreach ($product->taxonomies as $taxonomy){
                    $taxonomy_names[] = $taxonomy->title;
                }

                $prompt_part .= 'Kategorien: '.implode(', ', $taxonomy_names).'.' . self::LINE_SEPARATOR;
            }

            $product_data = $this->product_manager->getProductData($product);
            if ($product_data) {
                $prompt_part .= $this->getProductAttributesPromptPart($product_data);
            }

            $product_info = unserialize($product->meta['product_info']);
            $features = $product_info['Features']['DisplayValues'];
            if(count($features)) {
                $prompt_part .= self::LINE_SEPARATOR . 'Das ist die Produktbeschreibung des "'.$title.'" bei Amazon:' . self::LINE_SEPARATOR .
                    implode(';'.self::LINE_SEPARATOR, $features) . self::LINE_SEPARATOR . self::LINE_SEPARATOR;
            }

            $prompt .= $prompt_part;
        }

        $prompt .= self::LINE_SEPARATOR . "Formattiere den Text als HTML, strukturiere ihn mit Überschriften und markiere wichtige Teile als fett." . self::LINE_SEPARATOR;

        $prompt .= "Formuliere den Text in der 'Du'-Form. "
            ."Falls der Eingabetext in der 'Sie'-Form geschrieben ist, formuliere ihn bitte in die 'Du'-Form um. "
            ."Der Text sollte mindestens ".self::TEXT_LENGTH." Wörter haben.";

        return $prompt;
    }

    public function getProductAttributesPromptPart($product_data)
    {
        $product_data_items = [];
        foreach ($product_data as $data) {
            $value = $data->value;

            switch ($data->field_type) {
                case "true_false":
                    if (boolval($value)) {
                        $value = 'Ja';
                    } else {
                        $value = 'Nein';
                    }
                #case "select":
                #    $values = unserialize($value);
                #    $values = array_values($values);
                #    $prefix = (($data->field_prefix!==null && strlen($data->field_prefix)>0) ? $data->field_prefix : "");
                #    $suffix = (($data->field_suffix!==null && strlen($data->field_suffix)>0) ? $data->field_suffix : "");
                #    $value = $prefix.strval(implode(', ', $values)).$suffix;
                case "number":
                default:
                    $prefix = (($data->field_prefix !== null && strlen($data->field_prefix) > 0) ? $data->field_prefix : "");
                    $suffix = (($data->field_suffix !== null && strlen($data->field_suffix) > 0) ? $data->field_suffix : "");
                    $value = $prefix . strval($value) . $suffix;
            }

            $product_data_items[] = $data->field_name . ': '
                . ($data->field_prefix ? $data->field_prefix . ' ' : '')
                . $value
                . ($data->field_suffix ? ' ' . $data->field_suffix : '');
        }

        return 'Das Produkt hat folgende Eigenschaften: {' . implode(';' . self::LINE_SEPARATOR, $product_data_items) . '}';
    }
}