<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class AppleAppStoreDataService extends Service
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAppData($app_id, $language = "de")
    {
        $url = "https://itunes.apple.com/$language/lookup?id=" . str_replace('id', '', $app_id);

        $data = FileHelper::getFromUrl($url);
        $data = json_decode(trim($data), true);

        if (!$data || count($data['results']) < 1) {
            return null;
        }

        $app_data = $data['results'][0];

        //Daten und Icon cachen

        return [
            'name' => $app_data['trackName'],
            'icon' => $app_data['artworkUrl512']
        ];
    }
}