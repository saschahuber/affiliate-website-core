<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\service\Service;
use stdClass;

#[AllowDynamicProperties]
class AdTrackingService extends Service
{
    public function __construct()
    {
        parent::__construct();
    }

    public function trackAdImpression($ad_id, $params)
    {
        $source = ArrayHelper::getArrayValue($params, 'source');

        if ($source === null) {
            return;
        }
        $session_id = session_id();
        $fingerprint = ArrayHelper::getArrayValue($params, 'fingerprint');
        $user_ip = RequestHelper::getClientIp();

        $this->insertTrackingLogItem($ad_id, $session_id, $user_ip, $fingerprint, $source, $params);
    }

    private function insertTrackingLogItem($ad_id, $session_id, $user_ip, $fingerprint, $source, $params)
    {
        unset($params['source']);
        unset($params['session']);

        $tracking_item = new stdClass();
        $tracking_item->ad_id = $ad_id;
        $tracking_item->session_id = $session_id;
        $tracking_item->user_ip = $user_ip;
        $tracking_item->fingerprint = $fingerprint;
        $tracking_item->source = $source;

        if (count($params)) {
            $tracking_item->params = json_encode($params);
        }

        $this->DB->insertFromObject('ad__tracking', $tracking_item);
    }
}
