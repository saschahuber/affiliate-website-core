<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\DealPageManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductComparisonManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\persistence\VgWortPixelRepository;
use saschahuber\saastemplatecore\service\Service;

class VgWortPixelService extends Service {
    function __construct(){
        parent::__construct();

        $this->vg_wort_pixel_repository = new VgWortPixelRepository();
    }

    function getById($id){
        return $this->vg_wort_pixel_repository->getById($id);
    }

    function getAll(){
        return $this->vg_wort_pixel_repository->getAll();
    }

    function getMappedPixels(){
        $all_pixels = $this->vg_wort_pixel_repository->getAll();

        $mapped_pixels = $this->getAllMappedPixels();

        $unused_pixels = [];

        foreach($all_pixels as $pixel){
            $pixel->mapped_element_type = null;
            $pixel->mapped_element_id = null;

            if(array_key_exists($pixel->vg_wort_pixel_id, $mapped_pixels)){
                $pixel->mapped_element_type = $mapped_pixels[$pixel->vg_wort_pixel_id]['element_type'];
                $pixel->mapped_element_id = $mapped_pixels[$pixel->vg_wort_pixel_id]['element_id'];
            }
            $unused_pixels[] = $pixel;
        }

        return $unused_pixels;
    }

    function getAllMappingTables(){
        return [
            DealPageManager::TYPE_DEAL => DealPageManager::TABLE_NAME,
            ProductManager::TYPE_PRODUCT => ProductManager::TABLE_NAME,
            ProductManager::TYPE_PRODUCT_CATEGORY => ProductManager::TABLE_NAME . '__taxonomy',
            ProductComparisonManager::TYPE_PRODUCT_COMPARISON => ProductComparisonManager::TABLE_NAME,
            PostManager::TYPE_POST => PostManager::TABLE_NAME,
            PostManager::TYPE_POST_CATEGORY => PostManager::TABLE_NAME . '__taxonomy',
            NewsManager::TYPE_NEWS => NewsManager::TABLE_NAME,
            NewsManager::TYPE_NEWS_CATEGORY => NewsManager::TABLE_NAME . '__taxonomy',
            PageManager::TYPE_PAGE => PageManager::TABLE_NAME,
            BrandManager::TYPE_BRAND => BrandManager::TABLE_NAME
        ];
    }

    function getAllMappedPixels(){
        $mapped_pixels = [];
        foreach($this->getAllMappingTables() as $type => $table){
            foreach($this->DB->getAll("SELECT * FROM $table where vg_wort_pixel_id is not null and vg_wort_pixel_id != ''") as $item){
                $mapped_pixels[$item->vg_wort_pixel_id] = [
                    'element_type' => $type,
                    'element_id' => $item->{$table.'_id'}
                ];
            }
        }
        return $mapped_pixels;
    }
}