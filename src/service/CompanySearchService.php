<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\component\CompanyGrid;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\KeywordHelper;
use saschahuber\saastemplatecore\service\Service;

class CompanySearchService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->company_manager = new CompanyManager();
    }

    function searchCompanies($keyword, $taxonomy, $location_keyword, $radius){
        $city = null;

        if($location_keyword) {
            $city = $this->findCity($location_keyword);
        }

        $companies = $this->findCompanies($keyword, $taxonomy, $location_keyword, $city, $radius);

        return $companies;
    }

    function displayCompanies($companies){
        (new CompanyGrid($companies, 2))->display();
    }

    function findCity($location) {
        $query_builder = new DatabaseSelectQueryBuilder('city');
        $query_builder->selects([
                'name',
                'avg(latitude) as lat',
                'avg(longitude) as lng',
                'CASE 
                   WHEN name = "'.$this->DB->escape($location).'" THEN 3
                   WHEN name LIKE "'.$this->DB->escape($location).'%" THEN 2
                   WHEN name LIKE "%'.$this->DB->escape($location).'%" THEN 1
                   ELSE 0
               END AS relevance'
            ])
            ->conditions([
                'zipcode = "'.$this->DB->escape($location).'"',
                'name LIKE "%'.$this->DB->escape($location).'%"'
            ], 'or')
            ->order('relevance DESC')
            ->groupBy('name');

        return $this->DB->getOne($query_builder->buildQuery());
    }

    function findCompanies($keyword, $taxonomy, $location_keyword, $location, $radius) {
        $query_builder = new DatabaseSelectQueryBuilder('search_index');

        $conditions = ['searchable_item_type = "company"'];

        if($keyword) {
            $query_builder->selects([
                'search_index.*',
                'MATCH (search_title) AGAINST ("' . $this->getRuleset($keyword) . '" IN BOOLEAN MODE) as title_relevance',
                'MATCH (search_content_primary, search_content_primary) AGAINST ("' . $this->getRuleset($keyword) . '" IN BOOLEAN MODE) as content_relevance',
                'MATCH (search_meta) AGAINST ("' . $this->getRuleset($keyword) . '" IN BOOLEAN MODE) as meta_relevance',
            ]);

            $conditions[] = 'MATCH (search_title, search_content_primary, search_content_primary, search_meta) AGAINST ("' . $this->getRuleset($keyword) . '" IN BOOLEAN MODE)';

            $query_builder->orders([
                '(title_relevance * relevance_modifier) DESC',
                '(content_relevance * relevance_modifier) DESC',
                '(meta_relevance * relevance_modifier) DESC'
            ]);
        }

        $query_builder->conditions($conditions)->limit(25);

        $querystring = $query_builder->buildQuery();

        $items = $this->DB->getAll($querystring, true, 300);

        if(count($items) < 1){
            return [];
        }

        $companies = [];

        if($location_keyword || $location){
            $item_ids = [];
            foreach($items as $item){
                $item_ids[] = $item->searchable_item_id;
            }

            $query_builder = new DatabaseSelectQueryBuilder('company');

            $conditions = [
                'company.company_id IN (' . implode(',', $item_ids) . ')'
            ];

            if($taxonomy){
                $query_builder->join('company__taxonomy_mapping', 'company.company_id', 'company__taxonomy_mapping.company_id');
                $conditions[] = 'company__taxonomy_mapping.taxonomy_id = ' . intval($taxonomy);
            }

            $query_builder->selects([
                'company.*',
                '(6371 * acos(cos(radians('.doubleval($location->lat).')) 
                       * cos(radians(latitude)) 
                       * cos(radians(longitude) - radians('.doubleval($location->lng).')) 
                       + sin(radians('.doubleval($location->lat).')) 
                       * sin(radians(latitude)))) AS distance'
            ])->conditions($conditions)
            ->having('distance < '.intval($radius))
            ->order('distance ASC');

            $companies = $this->DB->getAll($query_builder->buildQuery(), true, 300);

            foreach($companies as $company){
                $this->company_manager->addMeta($company);
            }
        }
        else {
            foreach($items as $item){
                $company = $this->company_manager->getById($item->searchable_item_id);

                if($taxonomy) {
                    $has_taxonomy = false;
                    foreach ($company->taxonomies as $company_taxonomy) {
                        if ($company_taxonomy->taxonomy_id == $taxonomy) {
                            $has_taxonomy = true;
                            break;
                        }
                    }

                    if (!$has_taxonomy) {
                        continue;
                    }
                }

                $companies[] = $company;
            }
        }

        return $companies;
    }

    private function getRuleset($keywords)
    {
        $keywords = KeywordHelper::keywords($keywords, true, false);

        if (!isset($this->ruleset)) {
            $this->ruleset = $this->DB->escape('+' . implode('* ', explode(' ', trim($keywords))) . '*');
        }
        return $this->ruleset;
    }
}