<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\service\ApplicationAnalyticsService;
use saschahuber\saastemplatecore\service\IntervalService;

class ElementViewsLastIntervalService extends IntervalService
{
    function __construct()
    {
        parent::__construct();
        $this->application_analytics_service = new ApplicationAnalyticsService();
    }

    function getLastIntervalLogEntries($url, $interval = DatabaseTimeSeriesHelper::INTERVAL_MINUTE, $number = 60)
    {
        global $CONFIG;

        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $sql_date_format = "%Y-%m-%d %H:%i";
                $sql_interval = "MINUTE";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $sql_date_format = "%Y-%m-%d %H";
                $sql_interval = "HOUR";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $sql_date_format = "%Y-%m-%d";
                $sql_interval = "DAY";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $sql_date_format = "%Y-%m";
                $sql_interval = "MONTH";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $sql_date_format = "%Y";
                $sql_interval = "YEAR";
                break;
        }

        $filter_string = "log_time > CURRENT_DATE - INTERVAL ".intval($number)." $sql_interval";
        $filter_string .= ' and event_type = "page_view"';
        if ($url) {
            $filter_string .= ' and url = "'.$CONFIG->website_domain.$this->DB->escape($url).'"';
        }

        $query = "SELECT Date_format(log_time, '$sql_date_format') AS timestamp,
                                           Count(*) AS anzahl
                                    FROM application_analytics
                                    WHERE " . $filter_string . "
                                    GROUP BY timestamp;";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[$item->timestamp] = $item->anzahl;
        }

        return $items;
    }
}