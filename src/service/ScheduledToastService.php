<?php

namespace saschahuber\affiliatewebsitecore\service;

use DateInterval;
use saschahuber\affiliatewebsitecore\enum\ToastConditionType;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\model\Toast;
use saschahuber\affiliatewebsitecore\persistence\ToastConditionRepository;
use saschahuber\affiliatewebsitecore\persistence\ToastRepository;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\service\ApplicationAnalyticsService;
use saschahuber\saastemplatecore\service\Service;

class ScheduledToastService extends Service {
    public function __construct()
    {
        parent::__construct();
        $this->toast_repository = new ToastRepository();
        $this->toast_condition_repository = new ToastConditionRepository();
        $this->application_analytics_service = new AffiliateApplicationAnalyticsService();
    }

    public function getAll(){
        $toasts = $this->toast_repository->getAllToasts();

        foreach($toasts as $toast){
            $toast->conditions = $this->toast_condition_repository->getByToastId($toast->id);
        }

        return $toasts;
    }

    public function getAllActive(){
        $toasts = $this->toast_repository->getAllActiveToasts();

        foreach($toasts as $toast){
            $toast->conditions = $this->toast_condition_repository->getByToastId($toast->id);
        }

        return $toasts;
    }

    public function getAllToastsForElement($linked_element_type, $linked_element_id){
        $toasts = $this->toast_repository->getAllToastsForElement($linked_element_type, $linked_element_id);

        foreach($toasts as $toast){
            $toast->conditions = $this->toast_condition_repository->getByToastId($toast->id);
        }

        return $toasts;
    }

    public function getById($id){
        return $this->toast_repository->getById($id);
    }

    public function createItem($item){
        return $this->toast_repository->create($item);
    }

    public function updateItem($item){
        return $this->toast_repository->update($item);
    }

    private function checkToastCondition($condition, $fingerprint, $url){
        global $CONFIG;

        $condition_data = json_decode($condition->condition_data, true);

        switch($condition->condition_type){
            case ToastConditionType::NEW_USER:
                $min_views = intval(ArrayHelper::getArrayValue($condition_data, 'min_views'));
                $max_views = intval(ArrayHelper::getArrayValue($condition_data, 'max_views'));

                $current_time = new \DateTime();
                $interval = new DateInterval('P60D');
                $current_time->sub($interval);
                $date_some_time_ago =  $current_time->format('Y-m-d H:i:s');

                $page_view_count = $this->application_analytics_service->getEventCountByFingerprint($fingerprint, 'page_view', $date_some_time_ago);

                if($min_views <= $page_view_count && ($max_views === 0 || ($page_view_count <= $max_views))){
                    return true;
                }
                break;
            case ToastConditionType::RECURRING_USER:
                $min_days_gone = ArrayHelper::getArrayValue($condition_data, 'min_days_gone');
                $max_days_gone = ArrayHelper::getArrayValue($condition_data, 'max_days_gone');

                $last_visit_days_ago = $this->application_analytics_service->getLastEventDaysAgoByFingerprint($fingerprint, 'page_view');

                if($min_days_gone <= $last_visit_days_ago && $last_visit_days_ago <= $max_days_gone){
                    return true;
                }
                break;
            case ToastConditionType::URL_PATH:
                $url_path = ArrayHelper::getArrayValue($condition_data, 'url_path');

                $url_path = trim(str_replace($CONFIG->website_domain, '', $url_path), '/');
                $url = trim(str_replace($CONFIG->website_domain, '', $url), '/');

                if(substr($url, 0, strlen($url_path)) === $url_path){
                    return true;
                }
                break;
        }

        return false;
    }

    private function checkAllToastConditions($toast, $fingerprint, $url){
        foreach ($toast->conditions as $condition){
            if(!$this->checkToastCondition($condition, $fingerprint, $url)){
                return false;
            }
        }

        return true;
    }

    public function getFilteredScheduledToasts($toasts, $fingerprint, $url){
        global $CONFIG;

        $toasts_to_display = [];
        foreach ($toasts as $toast){
            if($toast->click_url){
                $trimmed_user_url = trim(str_replace($CONFIG->website_domain, '', $url), '/');
                $trimmed_click_url = trim(str_replace($CONFIG->website_domain, '', $toast->click_url), '/');

                if($trimmed_user_url === $trimmed_click_url){
                    continue;
                }
            }

            if($this->checkAllToastConditions($toast, $fingerprint, $url)){
                $toasts_to_display[] = $toast;
            }
        }
        return $toasts_to_display;
    }

    public function getScheduledToastToDisplay($fingerprint, $url){
        $toasts = $this->getAllActive();

        $toasts_to_display = $this->getFilteredScheduledToasts($toasts, $fingerprint, $url);

        if(count($toasts_to_display) < 1){
            return null;
        }

        $toast = ArrayHelper::rand($toasts_to_display);

        $content = $this->getScheduledToastContent($toast);

        return new Toast($toast->title, $content, $toast->small_info, $toast->hide_delay);
    }

    public function getScheduledToastContent($toast){
        return BufferHelper::buffered(function() use ($toast){
            $permalink = $toast->click_url;

            if(!$permalink && $toast->linked_element_type && $toast->linked_element_id) {
                $permalink = getContentPermalinkElementByTypeAndId($toast->linked_element_type, $toast->linked_element_id);
            }

            ?>
            <div onclick="saveAnalyticsEvent('toast_click', {toast_type: 'scheduled', id: <?=$toast->id?><?=($permalink?", url: '$permalink'":'')?>});
            <?=($permalink?" window.open('$permalink', '_blank').focus();":'');?>"
                <?=($permalink?' style="cursor: pointer;"':'');?>
            >
                <?=ShortcodeHelper::doShortcode($toast->content)?>
            </div>

            <div>
            </div>

            <script>
                saveAnalyticsEvent('toast_show', {toast_type: 'scheduled', id: <?=$toast->id?>})
            </script>
            <?php
        });
    }
}