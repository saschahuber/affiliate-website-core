<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\manager\FileManager;
use saschahuber\affiliatewebsitecore\service\AffiliateImageService;

class AttachmentImageGeneratorService
{
    function __construct()
    {
        $this->image_service = new AffiliateImageService();
        $this->file_manager = new FileManager();
        $this->image_manager = new ImageManager();
        $this->stock_image_manager = new StockImageManager();
        $this->img_utils = new ImgUtils();
    }

    function getImageTemplates()
    {
        //Default Template
        $templates = [];

        #$templates['default_attachment'] = array(
        #    'title' => "Default Attachment",
        #    'background_image' => null,
        #    'image_type' => 'default',
        #    'template' => ImageTemplateService::ATTACHMENT
        #);

        //Stock Images
        foreach ($this->stock_image_manager->getAttachments() as $attachment) {
            $templates['stock__' . $attachment->id] = [
                'title' => "(Stock) " . $attachment->description ?: $attachment->file_name,
                'image_type' => 'stock',
                'image_id' => $attachment->id,
                'image_url' => $this->stock_image_manager->getAttachmentUrl($attachment->id),
                'template' => ImageTemplateService::ATTACHMENT
            ];
        }

        //Attachments Images
        foreach ($this->image_manager->getAttachmentsForAttachmentGenerator() as $attachment) {
            $templates['attachment__' . $attachment->id] = [
                'title' => "(Attachment) " . ($attachment->title ?: ($attachment->alt_text ?: $attachment->file_name)),
                'image_type' => 'attachment',
                'image_id' => $attachment->id,
                'image_url' => $this->image_manager->getAttachmentUrl($attachment->id),
                'template' => ImageTemplateService::ATTACHMENT
            ];
        }

        return $templates;
    }

    function getTemplateData($template_id)
    {
        $templates = $this->getImageTemplates();

        return ArrayHelper::getArrayValue($templates, $template_id, null);#, $this->getImageTemplates()['default_attachment']);
    }

    function getImageTemplateOptions()
    {
        $templates = $this->getImageTemplates();
        $output_templates = [];
        foreach ($templates as $key => $template) {
            $output_templates[strval($key)] = $template['image_url'];
        }
        return $output_templates;
    }

    function generateAttachmentImage($template_id, $text, $sub_dir = null, $params = null)
    {
        global $CONFIG;

        $template_data = $this->getTemplateData($template_id);

        $name_prefix = substr(md5(time()), 0, 7);

        if (explode('__', $template_id)[0] === "stock") {
            $stock_id = ArrayHelper::getArrayValue($template_data, 'image_id', null);
            $background_image_path = APP_BASE . '/../' . $this->stock_image_manager->getAttachmentUrl($stock_id);
        } else if (explode('__', $template_id)[0] === "attachment") {
            $stock_id = ArrayHelper::getArrayValue($template_data, 'image_id', null);
            $background_image_path = APP_BASE . '/../' . $this->image_manager->getAttachmentUrl($stock_id);
        } else {
            $background_image_path = ArrayHelper::getArrayValue($template_data, 'background_image', null);
        }

        return $CONFIG->website_domain . $this->image_service->generateImage($template_data['template'], $text,
                $background_image_path, $sub_dir, $name_prefix, $params);
    }
}