<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use DOMDocument;
use DOMXPath;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class UrlScraperService extends Service
{
    function __construct()
    {
        parent::__construct();
    }

    function scrapeUrl($url)
    {
        return file_get_contents($url);
    }

    function parseUrlContent($url)
    {
        $content = $this->scrapeUrl($url);
        $dom = new DOMDocument;

        libxml_use_internal_errors(true);
        $dom->loadHTML($content);
        libxml_use_internal_errors(false);

        $dom = $this->cleanDom($dom);

        // Textinhalt extrahieren
        $content = $dom->saveHTML();
        $content = strip_tags($content);
        $content = trim(preg_replace('/\s+/', ' ', $content));

        return $content;
    }

    function cleanDom($dom)
    {
        $tags_to_remove = ['head', 'header', 'footer', 'style', 'script', 'navigation', 'nav', 'picture', 'iframe', 'svg', 'img', 'link'];
        $this->removeDomElementsByTags($dom, $tags_to_remove);

        #$classes_to_remove = ['popup', 'nav', 'navigation'];
        #$dom = $this->removeDomElementsByClasses($dom, $classes_to_remove);
        return $dom;
    }

    function removeDomElementsByTags(&$dom, $tags)
    {
        foreach ($tags as $tag) {
            $this->removeDomElementsByTag($dom, $tag);
        }
    }

    function removeDomElementsByTag(&$dom, $tag)
    {
        $list = $dom->getElementsByTagName($tag);
        while ($list->length > 0) {
            $p = $list->item(0);
            $p->parentNode->removeChild($p);
        }
    }

    function removeDomElementsByClasses($dom, $classes)
    {
        foreach ($classes as $class) {
            $dom = $this->removeDomElementsByClass($dom, $class);
        }
        return $dom;
    }

    function removeDomElementsByClass($dom, $class)
    {
        $xpath = new DOMXPath($dom);
        $elements = $xpath->query("//*[contains(@class, '" . $class . "')]");

        foreach ($elements as $element) {
            $element->parentNode->removeChild($element);
        }
        return $dom;
    }
}