<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\NewsFeedImportManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;

#[AllowDynamicProperties]
class NewsImporterService
{
    public function __construct()
    {
        global $DB;
        $this->DB = $DB;
        $this->news_manager = new NewsManager();

        $this->importers = $this->getImporters();
    }

    private function getImporters()
    {
        global $CONFIG;

        $import_templates = [];

        foreach ($CONFIG->news_importer_templates as $template_class) {
            $template = new $template_class();
            $import_templates[$template->getKey()] = $template;
        }

        return $import_templates;
    }

    public function getIntegratedFeeds()
    {
        $feeds = [];
        foreach ((new NewsFeedImportManager())->getAll() as $item) {
            $feeds[$item->id] = $item;
        }

        return $feeds;
    }

    static function getTemplateOptions()
    {
        global $CONFIG;
        $template_options = [];
        foreach ($CONFIG->news_importer_templates as $template_class) {
            $template = new $template_class();
            $template_options[$template->getKey()] = $template->getTitle();
        }
        return $template_options;
    }

    public function getImportFeeds()
    {
        $feeds = [];
        foreach ((new NewsFeedImportManager())->getAllActive() as $feed) {
            $feed->settings = json_decode($feed->settings, false);
            $feeds[] = $feed;
        }
        return $feeds;
    }

    public function refreshNewsFeedItems()
    {
        $items = [];
        foreach ($this->getImportFeeds() as $feed_item) {
            $items = array_merge($items, $this->getItemsFromFeed($feed_item));
        }
        return $items;
    }

    public function importAll()
    {
        $imported_items = [];
        foreach ($this->getImportFeeds() as $feed_item) {
            $imported_items = array_merge($imported_items, $this->importFromFeed($feed_item));
        }
        return $imported_items;
    }

    public function importFromFeed($news_feed_item)
    {
        return $this->importers[$news_feed_item->template]->runImport($news_feed_item);
    }

    public function getItemsFromFeed($news_feed_item)
    {
        return $this->importers[$news_feed_item->template]->getItemsFromFeed($news_feed_item);
    }

    public function getFeedItemsPreview($news_feed_item)
    {
        return $this->importers[$news_feed_item->template]->getFeedItemsPreview($news_feed_item);
    }
}