<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class DailyPushSubscribersService extends Service
{
    function __construct()
    {
        parent::__construct();
    }

    function getSubscribers($day_range = 30)
    {
        $query = "SELECT TIMESTAMPDIFF(DAY, timestamp, NOW()) as log_index,
                DATE_FORMAT(timestamp, '%a, %d.%m.%Y') as formatted_date,
                value as hits
                FROM statistics
                where statistics_key = 'PUSH_SUBSCRIBERS' and TIMESTAMPDIFF(DAY, timestamp, NOW()) < " . intval($day_range) . "
                order by log_index desc, timestamp asc";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[] = $item;
        }
        return $items;
    }
}