<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\persistence\ApplicationAnalyticsRepository;
use saschahuber\saastemplatecore\service\ApplicationAnalyticsService;

class AffiliateApplicationAnalyticsService extends ApplicationAnalyticsService {

    public function __construct(){
        parent::__construct();
    }

    public function findLogs($event_type=null, $url=null, $user_id=null, $ip=null, $user_agent=null, $session_id=null, $fingerpint=null, $referrer=null, $utm_source=null,
                             $utm_medium=null, $utm_campaign=null, $utm_content=null, $utm_term=null, $from_time=null, $to_time=null, $limit=1000){
        $conditions = [];

        $has_filters = false;

        if($event_type){
            $has_filters = true;
            $conditions[] = 'event_type LIKE "%'.$this->DB->escape($event_type).'%"';
        }

        if($url){
            $has_filters = true;
            $conditions[] = 'url LIKE "%'.$this->DB->escape($url).'%"';
        }

        if($ip){
            $has_filters = true;
            $conditions[] = 'user_ip LIKE "%'.$this->DB->escape($ip).'%"';
        }

        if($user_id){
            $has_filters = true;
            $conditions[] = 'user_id LIKE "%'.$this->DB->escape($user_id).'%"';
        }

        if($user_agent){
            $has_filters = true;
            $conditions[] = 'user_agent LIKE "%'.$this->DB->escape($user_agent).'%"';
        }

        if($session_id){
            $has_filters = true;
            $conditions[] = 'session_id LIKE "%'.$this->DB->escape($session_id).'%"';
        }

        if($fingerpint){
            $has_filters = true;
            $conditions[] = 'application_analytics.fingerprint LIKE "%'.$this->DB->escape($fingerpint).'%"';
        }

        if($referrer){
            $has_filters = true;
            $conditions[] = 'referrer LIKE "%'.$this->DB->escape($referrer).'%"';
        }

        if($utm_source){
            $has_filters = true;
            $conditions[] = 'utm_source LIKE "%'.$this->DB->escape($utm_source).'%"';
        }

        if($utm_medium){
            $has_filters = true;
            $conditions[] = 'utm_medium LIKE "%'.$this->DB->escape($utm_medium).'%"';
        }

        if($utm_campaign){
            $has_filters = true;
            $conditions[] = 'utm_campaign LIKE "%'.$this->DB->escape($utm_campaign).'%"';
        }

        if($utm_content){
            $has_filters = true;
            $conditions[] = 'utm_content LIKE "%'.$this->DB->escape($utm_content).'%"';
        }

        if($utm_term){
            $has_filters = true;
            $conditions[] = 'utm_term LIKE "%'.$this->DB->escape($utm_term).'%"';
        }

        if($from_time){
            $conditions[] = 'log_time >= "'.$this->DB->escape($from_time).'"';
        }

        if($to_time){
            $conditions[] = 'log_time <= "'.$this->DB->escape($to_time).'"';
        }

        $query_builder = new DatabaseSelectQueryBuilder('application_analytics');
        $query_builder->order('log_time DESC')
            ->select('application_analytics.*, user__profile.fingerprint as user_profile_fingerprint')
            ->join('user__profile', 'user__profile.fingerprint', 'application_analytics.fingerprint')
            ->conditions($conditions)
            ->limit($limit);

        if(!$has_filters){
            $query_builder->limit(1000);
        }

        return $this->DB->getAll($query_builder->buildQuery());
    }
}