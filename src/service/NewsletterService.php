<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\service\MailQueueService;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\service\MailService;
use saschahuber\saastemplatecore\service\Service;
use saschahuber\saastemplatecore\service\TemplateService;
use stdClass;

class NewsletterService extends Service{
    const NEWSLETTER_TOPIC_POSTS = 'posts';
    const NEWSLETTER_TOPIC_REVIEWS = 'reviews';
    const NEWSLETTER_TOPIC_NEWS = 'news';
    const NEWSLETTER_TOPIC_DEALS = 'deals';

    const ALL_NEWSLETTER_TOPICS = array(
        NewsletterService::NEWSLETTER_TOPIC_POSTS => 'Blog-Artikel',
        NewsletterService::NEWSLETTER_TOPIC_REVIEWS => 'Produkttests',
        NewsletterService::NEWSLETTER_TOPIC_NEWS => 'News',
        NewsletterService::NEWSLETTER_TOPIC_DEALS => 'Deals & Blitzangebote'
    );

    public function __construct(){
        parent::__construct();

        $this->mail_service = new MailService();
        $this->mail_queue_service = new MailQueueService();

        $this->template_service = new TemplateService();
        $this->newsletter_template_service = new NewsletterTemplateService();

        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
        $this->image_manager = new ImageManager();
        $this->post_manager = new PostManager();
        $this->news_manager = new NewsManager();
    }

    public function scheduleMailToSubscribers($subscribers, $subject, $content){
        foreach($subscribers as $subscriber){
            $this->scheduleMailToSubscriber($subscriber, $subject, $content);
        }
    }

    public function scheduleCustomNewsletterMails($newsletter, $subscribers){
        echo "Scheduling custom newsletter with subject '{$newsletter->subject}' for " . count($subscribers) . " subscribers" . PHP_EOL;

        foreach($subscribers as $subscriber){
            $subject = $newsletter->subject;
            $message = $newsletter->message;
            $template = $newsletter->template;

            if($template){
                $message = $this->template_service->generateMailSubTemplate($template, []);
            }

            $success = $this->scheduleMailToSubscriber($subscriber, $subject, $message);

            if($success){
                echo "Sent newsletter to {$subscriber->email}".PHP_EOL;
                #$this->DB->query('UPDATE newsletter_subscriber
                #    SET last_newsletter = CURRENT_TIMESTAMP
                #    WHERE newsletter_subscriber_id = '.intval($subscriber->newsletter_subscriber_id));
            }
            else{
                echo "Could not send newsletter to {$subscriber->email}".PHP_EOL;
            }
        }

        $newsletter->sent_to_all_receivers = true;
        $this->DB->updateFromObject('newsletter', $newsletter);
    }

    public function scheduleNewsletterMails($subscribers){
        foreach($subscribers as $subscriber){
            $content = $this->createPersonalNewsletterContent($subscriber);

            if($content === null){
                echo "Skipped sending newsletter to {$subscriber->email} => no new content since last newsletter..." . PHP_EOL;
                continue;
            }

            $subject = "Dein Smarthome-Newsletter";
            $success = $this->scheduleMailToSubscriber($subscriber, $subject, $content);

            if($success){
                echo "Sent newsletter to {$subscriber->email}".PHP_EOL;
                $this->DB->query('UPDATE newsletter_subscriber 
                    SET last_newsletter = CURRENT_TIMESTAMP 
                    WHERE newsletter_subscriber_id = '.intval($subscriber->newsletter_subscriber_id));
            }
            else{
                echo "Could not send newsletter to {$subscriber->email}".PHP_EOL;
            }
        }
    }

    private function getPostsNewsletterData($subscriber){
        global $CONFIG;

        $data = [];

        if(in_array(self::NEWSLETTER_TOPIC_POSTS, $subscriber->topics)){
            $new_items = $this->post_manager->getNewPosts($subscriber->last_newsletter);
            foreach($new_items as $new_item){
                $item = new stdClass();
                $item->title = $new_item->title;
                $item->image_url = $CONFIG->website_domain . $this->post_manager->getThumbnailSrc($new_item);
                $item->permalink = $this->post_manager->generatePermalink($new_item);
                $data[] = $item;
            }
        }

        return $data;
    }

    private function getReviewNewsletterData($subscriber){
        global $CONFIG;

        $data = [];

        if(in_array(self::NEWSLETTER_TOPIC_REVIEWS, $subscriber->topics)){
            $new_items = $this->product_manager->getNewReviews($subscriber->last_newsletter);
            foreach($new_items as $new_item){
                $item = new stdClass();
                $item->title = $new_item->title;


                if(isset($item->attachment_id)) {
                    $item->image_url = (new ImageManager())->getAttachmentUrl($new_item->attachment_id);
                }
                else {
                    $item->image_url = $CONFIG->website_domain . "/produkt-img/{$new_item->id}/" . UrlHelper::alias($new_item->title) . ".jpg";
                }

                $item->permalink = $this->product_manager->generatePermalink($new_item);
                $data[] = $item;
            }
        }

        return $data;
    }

    private function getNewsNewsletterData($subscriber){
        global $CONFIG;

        $data = [];

        if(in_array(self::NEWSLETTER_TOPIC_NEWS, $subscriber->topics)){
            $new_items = $this->news_manager->getNewNews($subscriber->last_newsletter);
            foreach($new_items as $new_item){
                $item = new stdClass();
                $item->title = $new_item->title;
                $item->image_url = $CONFIG->website_domain . $this->news_manager->getThumbnailSrc($new_item);
                $item->permalink = $this->news_manager->generatePermalink($new_item);
                $data[] = $item;
            }
        }

        return $data;
    }

    private function getDealNewsletterData($subscriber){
        $data = [];

        if(in_array(self::NEWSLETTER_TOPIC_DEALS, $subscriber->topics)){
            $new_items = $this->product_manager->getTopDeals(3);
            foreach($new_items as $new_item){
                $item = new stdClass();
                $item->title = $new_item->title;
                $item->image_url = $this->image_manager->getAbsoluteAttachmentUrl($new_item->attachment_id);
                $item->permalink = $this->product_manager->generatePermalink($new_item);
                $data[] = $item;
            }
        }

        return $data;
    }

    public function addNewsletterData($subscriber, $data){
        $token = md5(uniqid());
        while($this->DB->query('SELECT * FROM newsletter_subscriber__newsletter_data 
            where token = "'.$this->DB->escape($token).'"')->num_rows < 0){
            $token = uniqid();
        }

        $data_item = new stdClass();
        $data_item->newsletter_subscriber_id = $subscriber->id;
        $data_item->token = $token;
        $data_item->data = json_encode($data);

        $this->DB->insertFromObject('newsletter_subscriber__newsletter_data', $data_item);

        return $token;
    }

    public function createNewsletterData($subscriber){
        $newsletter_data = new stdClass();
        $newsletter_data->posts = $this->getPostsNewsletterData($subscriber);
        $newsletter_data->reviews = $this->getReviewNewsletterData($subscriber);
        $newsletter_data->news = $this->getNewsNewsletterData($subscriber);
        $newsletter_data->deals = $this->getDealNewsletterData($subscriber);

        if(count($newsletter_data->posts) < 1
         && count($newsletter_data->reviews) < 1
         && count($newsletter_data->news) < 1
         && count($newsletter_data->deals) < 1){
            return null;
        }

        return $newsletter_data;
    }

    public function getNewsletterData($token){
        $result = $this->DB->query('SELECT data FROM newsletter_subscriber__newsletter_data 
            where token = "'.$this->DB->escape($token).'"')->fetchObject();

        if($result && isset($result->data)){
            return json_decode($result->data, false);
        }

        return null;
    }

    public function getPersonalNewsletterContent($token){
        $newsletter_data = $this->getNewsletterData($token);

        if($newsletter_data == null){
            return null;
        }

        return $this->newsletter_template_service->getNewsletterDataHtml($newsletter_data);
    }

    public function createPersonalNewsletterContent($subscriber){
        global $CONFIG;

        $newsletter_data = $this->createNewsletterData($subscriber);

        if($newsletter_data === null){
            return null;
        }

        $newsletter_token = $this->addNewsletterData($subscriber, $newsletter_data);

        $params = array(
            'browser_link' => $CONFIG->website_domain . '/newsletter/ansehen?token='.$newsletter_token,
            'content' => $this->newsletter_template_service->getNewsletterDataHtml($newsletter_data)
        );

        return $this->template_service->generateMailSubTemplate('newsletter_wrapper', $params);
    }

    public function scheduleMailToSubscriber($subscriber, $subject, $mail_content){
        $unsubscribe_url = null;
        if(isset($subscriber->unsubscribe_token)) {
            $unsubscribe_url = "/newsletter/abmeldung?unsubscribe_key=" . $subscriber->unsubscribe_token;
        }

        $settings_url = null;
        if(isset($subscriber->settings_token)) {
            $settings_url = "/newsletter/einstellungen?settings_key=" . $subscriber->settings_token;
        }

        $mail_content = $this->mail_service->generateMailContent($subscriber->email, $subject, $mail_content, $unsubscribe_url, $settings_url);
        return $this->mail_queue_service->addScheduledMailToQueue($subscriber->email, [], $subject, $mail_content);
    }

    public function sendMailToSubscriber($subscriber, $subject, $mail_content){
        $unsubscribe_url = null;
        if(isset($subscriber->unsubscribe_token)) {
            $unsubscribe_url = "/newsletter/abmeldung?unsubscribe_key=" . $subscriber->unsubscribe_token;
        }

        $settings_url = null;
        if(isset($subscriber->settings_token)) {
            $settings_url = "/newsletter/einstellungen?settings_key=" . $subscriber->settings_token;
        }

        return $this->mail_service->sendEmail($subscriber->email, [], $subject, $mail_content, [], $unsubscribe_url, $settings_url);
    }

    public function sendWelcomeMail($subscriber){
        $subject = "Willkommen beim Newsletter!";
        $mail_content = $this->template_service->generateMailSubTemplate('newsletter_welcome');
        $this->sendMailToSubscriber($subscriber, $subject, $mail_content);
    }

    public function sendConfirmationMail($subscriber){
        global $CONFIG;
        $subject = "Newsletter-Anmeldung bestätigen";
        $params = array(
            'confirmation_url' => $CONFIG->website_domain .'/newsletter?confirmation_key='. $subscriber->confirmation_token
        );
        $mail_content = $this->template_service->generateMailSubTemplate('newsletter_confirmation', $params);
        return $this->sendMailToSubscriber($subscriber, $subject, $mail_content);
    }

    public function sendSettingsUpdatedMail($subscriber){
        $subject = "Newsletter-Einstellungen aktualisiert!";
        $params = array();
        $mail_content = $this->template_service->generateMailSubTemplate('newsletter_settings_updated', $params);
        $this->sendMailToSubscriber($subscriber, $subject, $mail_content);
    }

    public function sendUnsubscribedMail($subscriber){
        $subject = "Deine Abmeldung vom Newsletter!";
        $params = array();
        $mail_content = $this->template_service->generateMailSubTemplate('newsletter_unsubscribed', $params);
        $this->sendMailToSubscriber($subscriber, $subject, $mail_content);
    }
}