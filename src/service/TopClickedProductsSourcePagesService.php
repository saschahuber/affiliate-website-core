<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class TopClickedProductsSourcePagesService extends Service
{
    function __construct()
    {
        parent::__construct();
    }

    function getTopClickedProductsSourcePages($days = 7, $limit = 10)
    {
        $query = "SELECT product__link_click.*, count(*) as anzahl_klicks from product__link_click
                where product__link_click.time > NOW() - INTERVAL " . intval($days) . " DAY 
                and source_url is not null and source_url != ''
                    group by source_url
                    order by anzahl_klicks DESC
                    LIMIT " . intval($limit) . ";";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[] = $item;
        }
        return $items;
    }
}