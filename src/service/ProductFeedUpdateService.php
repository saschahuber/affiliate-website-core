<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class ProductFeedUpdateService extends Service
{
    public function __construct()
    {
        parent::__construct();
        $this->product_feed_manager = new ProductFeedManager();
    }

    public function updateProductFeedFile($feed_id)
    {
        $feed = $this->product_feed_manager->getById($feed_id);

        $feed_template = (new ProductFeedImportService())->getFeedTemplate($feed);

        return $feed_template->loadFeed($feed, true);
    }
}
