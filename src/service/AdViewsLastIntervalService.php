<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\service\IntervalService;

class AdViewsLastIntervalService extends IntervalService
{
    function getTimeSeries($interval = DatabaseTimeSeriesHelper::INTERVAL_MINUTE, $number = 60)
    {
        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $sql_date_format = "%Y-%m-%d %H:%i";
                $sql_interval = "MINUTE";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $sql_date_format = "%Y-%m-%d %H";
                $sql_interval = "HOUR";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $sql_date_format = "%Y-%m-%d";
                $sql_interval = "DAY";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $sql_date_format = "%Y-%m";
                $sql_interval = "MONTH";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $sql_date_format = "%Y";
                $sql_interval = "YEAR";
                break;
        }

        $query = "SELECT Date_format(Timestamp(Date_sub(Now(), INTERVAL (offset - 1) $sql_interval)), '$sql_date_format')
                                                AS time
                                     FROM (SELECT @i := @i + 1 AS Offset
                                           FROM information_schema.collation_character_set_applicability,
                                                (SELECT @i := 0) AS i) AS Y
                                     WHERE offset BETWEEN -1 AND " . intval($number) . "
                                     ORDER BY offset DESC";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[$item->time] = 0;
        }
        return $items;
    }

    function getLastIntervalAdViews($interval = DatabaseTimeSeriesHelper::INTERVAL_MINUTE, $number = 60, $ad_id = null)
    {
        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $sql_date_format = "%Y-%m-%d %H:%i";
                $sql_interval = "MINUTE";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $sql_date_format = "%Y-%m-%d %H";
                $sql_interval = "HOUR";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $sql_date_format = "%Y-%m-%d";
                $sql_interval = "DAY";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $sql_date_format = "%Y-%m";
                $sql_interval = "MONTH";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $sql_date_format = "%Y";
                $sql_interval = "YEAR";
                break;
        }

        $filter_string = "ad__tracking.datetime > CURRENT_DATE - INTERVAL ".intval($number)." $sql_interval";
        if ($ad_id) {
            $filter_string .= ' and ad_id = ' . intval($ad_id);
        }

        $query = "SELECT Date_format(ad__tracking.datetime, '$sql_date_format') AS timestamp,
                                           Count(*) AS anzahl
                                    FROM ad__tracking
                                    WHERE " . $filter_string . "
                                    GROUP BY timestamp;";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[$item->timestamp] = $item->anzahl;
        }

        return $items;
    }
}