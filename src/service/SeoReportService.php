<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\service\Service;
use ReflectionException;
use SeoAnalyzer\Analyzer;

#[AllowDynamicProperties]
class SeoReportService extends Service {

    public function __construct()
    {
        parent::__construct();
    }

    public function getSeoReportForUrl($url, $keyword){
        try {
            return (new Analyzer())->analyzeUrl($url, $keyword);
        } catch (ReflectionException $e) {
            echo "Error loading metric file: " . $e->getMessage();
        }
        return null;
    }

    public function getSeoReportForHtml($html, $keyword){
        try {
            return (new Analyzer())->analyzeHtml($html, $keyword);
        } catch (ReflectionException $e) {
            echo "Error loading metric file: " . $e->getMessage();
        }
        return null;
    }
}