<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\persistence\CompanyActionLogRepository;
use saschahuber\saastemplatecore\service\Service;

class CompanyActionLogService extends Service {
    const EVENT_TYPE_PAGE_VIEW = 'company__action_log';

    public function __construct(){
        parent::__construct();
        $this->company_action_log_repository = new CompanyActionLogRepository();
    }

    public function getActions(){
        return $this->company_action_log_repository->getActions();
    }
}