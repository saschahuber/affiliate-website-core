<?php

namespace saschahuber\affiliatewebsitecore\service\toast;

use DateTime;
use saschahuber\affiliatewebsitecore\service\AffiliateApplicationAnalyticsService;
use saschahuber\affiliatewebsitecore\service\ScheduledToastService;
use saschahuber\affiliatewebsitecore\service\UserProfileService;
use saschahuber\saastemplatecore\service\ConfigService;
use saschahuber\saastemplatecore\service\Service;

class ToastLoaderService extends Service {

    public function __construct(){
        parent::__construct();
    }

    public function loadToastsForUser($fingerprint, $url, $referrer, $mobile, $utm_source, $utm_medium, $utm_campaign, $ugm_content, $utm_term){
        $toasts = [];

        #$url_data = parse_url($url);
        #$path = $url_data['path'];
        #$query = $url_data['query'];

        $config_service = new ConfigService();

        $scheduled_toasts_enabled = $config_service->getConfigField('toast_enabled_scheduled', ConfigService::TYPE_BOOL);
        $user_recommendations_enabled = $config_service->getConfigField('toast_enabled_recommended', ConfigService::TYPE_BOOL);

        $application_analytics_service = new AffiliateApplicationAnalyticsService();

        $last_time = $application_analytics_service->getLastEventTimeByFingerprint($fingerprint, 'toast_show');

        //Prüfen, ob zwischen dem letzten Toast für den User genug Zeit vergangen ist oder genug weitere Seiten aufgerufen wurden
        $min_minutes_between_toasts = 10;
        $min_page_views_between_toasts = 5;
        if($last_time){
            $page_views_since_last_toast = $application_analytics_service->getEventCountByFingerprint($fingerprint, 'page_view', $last_time);

            $now = new DateTime();
            $interval = $now->diff($last_time);
            $minutes_since_last_toast = ($interval->days * 24 * 60) + ($interval->h * 60) + $interval->i;

            if($minutes_since_last_toast < $min_minutes_between_toasts && $page_views_since_last_toast < $min_page_views_between_toasts){
                return [];
            }
        }

        if($scheduled_toasts_enabled){
            $scheduled_toast = $this->getScheduledToasts($fingerprint, $url);
            if($scheduled_toast){
                return [$scheduled_toast];
            }
        }

        if($user_recommendations_enabled) {
            $recommendation_toast = $this->getUserProfileRecommendations($fingerprint, $url);

            if($recommendation_toast){
                return [$recommendation_toast];
            }
        }

        //Wenn kein Profil:
            // Wenn Nutzer auf irgendeiner Seite
                // Eine der beliebtesten Kategorien anzeigen
                // Neueste News anzeigen
                // Neueste Veröffentlichtung anzeigen
            // Wenn Nutzer auf irgendeiner Produkt-Kategorie
                // Eins der TOP 3 Produkte anzeigen
                // Wenn vorhanden => Neuesten Produkttest anzeigen

        return $toasts;
    }

    public function getScheduledToasts($fingerprint, $url){
        $scheduled_toast_service = new ScheduledToastService();
        //Gibt es eine aktuelle redaktionelle Benachrichtigung? => Diese anzeigen
        return $scheduled_toast_service->getScheduledToastToDisplay($fingerprint, $url);
    }

    public function getUserProfileRecommendations($fingerprint, $url){
        $user_profile_service = new UserProfileService();
        return $user_profile_service->getRecommendationToastForFingerprint($fingerprint, $url);
    }
}