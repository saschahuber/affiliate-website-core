<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\service\Service;

class ProductService extends Service
{
    function __construct()
    {
        parent::__construct();
    }

    public function getAll($querystring)
    {
        return $this->DB->getAll($querystring, true, 15);
    }

    public function getByOldIds($ids, $add_meta = true, $orderby = null)
    {
        if ($ids === null || count($ids) < 1) {
            return [];
        }

        $in_ids_string = implode(', ', $ids);
        if (strlen($in_ids_string) < 1) {
            return [];
        }

        return $this->getAll('SELECT * FROM product 
            where status = "publish" and product_id_old IN (' . $in_ids_string . ')');
    }

    static function getReducedFilter()
    {
        return "(
            (
                product__link.price is not null and product__link.price > 0
                    and product__link.reduced_price is not null and product__link.reduced_price > 0
            )
            and (product__link.reduced_price < product__link.price)
            and (1 - (product__link.reduced_price / product__link.price)) > " . ProductManager::MIN_DEAL_DISCOUNT . "
            and (1 - (product__link.reduced_price / product__link.price)) < " . ProductManager::MAX_DEAL_DISCOUNT . "
        )";
    }

    protected function getOrderByClause($orderby = null, $desc = true, $field = null)
    {
        if ($orderby === null) {
            return '';
        }

        $orderby_field = 'RAND()';
        switch ($orderby) {
            case 'title':
                $orderby_field = 'product.title';
                break;
            case 'date':
                $orderby_field = 'product.product_date';
                break;
            case 'price':
                $orderby_field = 'product__link.reduced_price';
                break;
            case 'rating':
                $orderby_field = 'avg_rating';
                break;
            case 'sales_rank':
                $orderby_field = '-amazon_sales_rank';
                $desc = !$desc;
                break;
            case 'meta_value_num':
                if (!$field || strlen($field) < 1) {
                    $orderby_field = '-amazon_sales_rank';
                    $desc = !$desc;
                } else {
                    //Trick, damit NULL-Werte ganz hinten landen
                    $orderby_field = '-' . $this->DB->escape($field);
                    $desc = !$desc;
                }
                break;
            default:
                return 'RAND()';
        }

        return $orderby_field . ' ' . ($desc ? 'DESC' : 'ASC');
    }

    public function getByTaxonomyAliasMulti($aliase, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        return $this->getAll('SELECT product.* FROM product 
            JOIN product__taxonomy_mapping 
                ON product__taxonomy_mapping.product_id = product.product_id
            JOIN product__taxonomy 
                ON product__taxonomy_mapping.taxonomy_id = product__taxonomy.product__taxonomy_id
            JOIN product__link ON product__link.product_id = product.product_id
            where ' . $this->getProductVisibilityQueryCondition()
            . Helper::ifstr($reduced !== false, ' and ' . $this->getReducedFilter()) . '
            and product__taxonomy.permalink IN ("' . implode('", "', $aliase) . '")'
            . (Helper::ifstr($orderby !== null, (' ORDER BY ' . $this->getOrderByClause($orderby, $desc, $field))))
            . (($limit && $limit>0) ? " LIMIT " . intval($limit) : "")
        );
    }

    public function getByFieldValue($meta_key, $meta_value, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        $product_ids_meta_value_query = 'select product__data.product_id from product__data 
            JOIN product__data_field ON 
            product__data.product__data_field_id = product__data_field.product__data_field_id 
            JOIN product__link ON product__link.product_id = product.product_id
            where product__data_field.field_key = "' . $this->DB->escape($meta_key) . '" 
            and product__data.value = "' . $this->DB->escape($meta_value) . '"';

        return $this->getAll('SELECT product.* FROM product 
            JOIN product__link ON product__link.product_id = product.product_id
            where ' . $this->getProductVisibilityQueryCondition()
            . Helper::ifstr($reduced !== false, ' and ' . $this->getReducedFilter()) . '
            and product.product_id IN (' . $product_ids_meta_value_query . ')'
            . (Helper::ifstr($orderby !== null, (' ORDER BY ' . $this->getOrderByClause($orderby, $desc, $field))))
            . (($limit && $limit>0) ? " LIMIT " . intval($limit) : "")
        );
    }

    public function getByKategorieAndFieldValue($kategorie, $meta_key, $meta_value, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        $product_ids_meta_value_query = 'select product_id from product__data 
            JOIN product__data_field ON 
            product__data.product__data_field_id = product__data_field.product__data_field_id 
            where product__data_field.field_key = "' . $this->DB->escape($meta_key) . '"
            and product__data.value = "' . $this->DB->escape($meta_value) . '"';

        return $this->getAll('SELECT product.* FROM product 
            JOIN product__taxonomy_mapping 
                ON product__taxonomy_mapping.product_id = product.product_id
            JOIN product__taxonomy 
                ON product__taxonomy.product__taxonomy_id = product__taxonomy_mapping.taxonomy_id
            where product.status = "publish" and product.hide_in_search = 0 
            and product__taxonomy.permalink = "' . $this->DB->escape($kategorie) . '"'
            . Helper::ifstr($reduced !== false, ' and ' . $this->getReducedFilter()) . '
            and product.product_id IN (' . $product_ids_meta_value_query . ')'
            . (Helper::ifstr($orderby !== null, (' ORDER BY ' . $this->getOrderByClause($orderby, $desc, $field))))
            . (($limit && $limit>0) ? " LIMIT " . intval($limit) : "")
        );
    }

    public function getByTaxonomyAliasMultiAnd($aliase, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        $product_ids_multi_taxonomy_query = 'select distinct(product.product_id) from product 
            INNER JOIN product__taxonomy_mapping ON product.product_id = product__taxonomy_mapping.product_id 
            JOIN product__link ON product__link.product_id = product.product_id
            where product__taxonomy_mapping.taxonomy_id IN (
                SELECT product__taxonomy.product__taxonomy_id 
                FROM product__taxonomy where permalink IN ("' . implode('", "', $aliase) . '")
            ) 
            group by product__taxonomy_mapping.product_id HAVING COUNT(*) >= ' . intval(count($aliase));

        return $this->getAll('SELECT product.* FROM product 
            JOIN product__link ON product__link.product_id = product.product_id
            where ' . $this->getProductVisibilityQueryCondition()
            . Helper::ifstr($reduced !== false, ' and ' . $this->getReducedFilter()) . '
            and product.product_id IN (' . $product_ids_multi_taxonomy_query . ')'
            . (Helper::ifstr($orderby !== null, (' ORDER BY ' . $this->getOrderByClause($orderby, $desc, $field))))
            . (($limit && $limit>0) ? " LIMIT " . intval($limit) : "")
        );
    }

    public function getByTaxonomyBrandAliasMulti($kategorien, $hersteller, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        #Cache integrieren

        $querystring = 'SELECT product.* FROM product 
            JOIN product__taxonomy_mapping 
                ON product__taxonomy_mapping.product_id = product.product_id
            JOIN product__taxonomy 
                ON product__taxonomy_mapping.taxonomy_id = product__taxonomy.product__taxonomy_id
            JOIN brand__product_mapping 
                ON brand__product_mapping.product_id = product.product_id
            JOIN brand 
                ON brand__product_mapping.brand_id = brand.brand_id
            JOIN product__link ON product__link.product_id = product.product_id
            where ' . $this->getProductVisibilityQueryCondition()
            . Helper::ifstr($reduced !== false, ' and ' . $this->getReducedFilter()) . '
                and product__taxonomy.permalink IN ("' . implode('", "', $kategorien) . '")
                and brand.permalink IN ("' . implode('", "', $hersteller) . '")'
            . (Helper::ifstr($orderby !== null, (' ORDER BY ' . $this->getOrderByClause($orderby, $desc, $field))))
            . (($limit && $limit>0) ? " LIMIT " . intval($limit) : "");

        return $this->getAll($querystring);
    }

    public function getByBrandAliasMulti($aliase, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        #Cache integrieren
        return $this->getAll('SELECT product.* FROM product 
            JOIN brand__product_mapping 
                ON brand__product_mapping.product_id = product.product_id
            JOIN brand 
                ON brand__product_mapping.brand_id = brand.brand_id
            JOIN product__link ON product__link.product_id = product.product_id
            where ' . $this->getProductVisibilityQueryCondition()
            . Helper::ifstr($reduced !== false, ' and ' . $this->getReducedFilter()) . '
            and brand.permalink IN ("' . implode('", "', $aliase) . '")'
            . (Helper::ifstr($orderby !== null, (' ORDER BY ' . $this->getOrderByClause($orderby, $desc, $field))))
            . (($limit && $limit>0) ? " LIMIT " . intval($limit) : "")
        );
    }

    public static function getProductVisibilityQueryCondition()
    {
        return '(
            product.status = "publish" and product.product_date <= NOW()
        )
        and product.hide_in_search = 0
        and (
            product.is_fake = 0 or product__link.is_available = 1
        )';
    }

    public function findProductsByKeyword($keyword)
    {
        $query_builder = new DatabaseSelectQueryBuilder('product');

        if ($keyword) {
            foreach (explode(' ', trim($keyword)) as $keyword_part) {
                $conditions[] = 'title LIKE "%' . $this->DB->escape($keyword_part) . '%"';
            }
        }

        $query_builder->conditions($conditions);

        $query_builder->groupBy('product.product_id');

        return $this->getAll($query_builder->buildQuery());
    }

    public function findProducts($keyword, $produkt_ids, $category_ids, $brand_ids, $system_ids, $room_ids,
                                 $min_price, $max_price, $reduced, $data_field, $data_values, $data_fields, $orderby, $order, $limit, $has_all_categories)
    {
        $query_builder = new DatabaseSelectQueryBuilder('product');
        $query_builder->selects(['product.*', 'AVG(review_value) as avg_rating']);

        $conditions = [$this->getProductVisibilityQueryCondition()];

        $query_builder->join('product__link', 'product__link.product_id', 'product.product_id');
        $query_builder->join('product__review', 'product__review.product_id', 'product.product_id', 'left');

        if ($produkt_ids) {
            $conditions[] = 'product.product_id in (' . implode(',', array_map('intval', $produkt_ids)) . ')';
        }

        if ($category_ids) {
            if($has_all_categories) {
                $category_condition_items = [];

                foreach ($category_ids as $category_id){
                    $query_builder->join('product__taxonomy_mapping ptm_'.intval($category_id), 'ptm_'.intval($category_id).'.product_id = product.product_id AND ptm_'.intval($category_id).'.taxonomy_id', intval($category_id), 'left');
                    $query_builder->join('product__taxonomy_virtual_mapping ptvm_'.intval($category_id), 'ptvm_'.intval($category_id).'.product_id = product.product_id AND ptm_'.intval($category_id).'.taxonomy_id', intval($category_id), 'left');

                    $category_condition_items[] = '(ptm_'.intval($category_id).'.product_id IS NOT NULL OR ptvm_'.intval($category_id).'.product_id IS NOT NULL)';
                }

                $conditions[] = [
                    'items' => $category_condition_items,
                    'logic' => 'and'
                ];
            }
            else {
                $query_builder->join('product__taxonomy_mapping', 'product__taxonomy_mapping.product_id', 'product.product_id', 'left');
                $query_builder->join('product__taxonomy_virtual_mapping', 'product__taxonomy_virtual_mapping.product_id', 'product.product_id', 'left');

                $conditions[] = [
                    'items' => [
                        'product__taxonomy_mapping.taxonomy_id in (' . implode(',', array_map('intval', $category_ids)) . ')',
                        'product__taxonomy_virtual_mapping.taxonomy_id in (' . implode(',', array_map('intval', $category_ids)) . ')'
                    ],
                    'logic' => 'or'
                ];
            }
        }

        if ($brand_ids) {
            $query_builder->join('brand__product_mapping', 'brand__product_mapping.product_id', 'product.product_id');

            $conditions[] = 'brand__product_mapping.brand_id in (' . implode(',', array_map('intval', $brand_ids)) . ')';
        }

        if ($keyword) {
            foreach (explode(' ', trim($keyword)) as $keyword_part) {
                $conditions[] = 'title LIKE "%' . $this->DB->escape($keyword_part) . '%"';
            }
        }

        if ($data_field && count($data_values) > 0) {
            $query_builder->join('product__data', 'product__data.product_id', 'product.product_id');

            $conditions[] = '(product__data.product__data_field_id = ' . intval($data_field) . ') 
                and product__data.value in ("' . implode('","', $data_values) . '")';
        }

        if ($data_fields) {
            global $DB;

            foreach ($data_fields as $data_field_id => $values) {
                $join_id = "product_data_$data_field_id";

                $query_builder->join("product__data $join_id", "$join_id.product_id", 'product.product_id');
                $clean_values = [];

                foreach ($values as $value) {
                    if (is_numeric($value)) {
                        $value = intval($value);
                    } else if (strpos($value, 'range__') !== false) {
                        $value_parts = explode('__', str_replace('range__', '', $value));

                        $min = $value_parts[0];
                        $max = $value_parts[1];

                        $conditions[] = "(($join_id.product__data_field_id = " . intval($data_field_id) . ") 
                            and $join_id.value >= " . intval($min) . " 
                            and $join_id.value <= " . intval($max) . ")";


                        continue 2;
                    } else {
                        $value = '"' . $DB->escape($value) . '"';
                    }

                    $clean_values[] = $value;
                }

                $conditions[] = "($join_id.product__data_field_id = " . intval($data_field_id) . ") 
                and $join_id.value " . (count($values) > 1 ? "in (" . implode(",", $clean_values) . ")" : " = " . $clean_values[0]);
            }
        }

        if ($min_price) {
            $min_price = intval($min_price);
            $conditions[] = "((product__link.reduced_price is not null and product__link.reduced_price >= $min_price) or product__link.price >= $min_price)";
        }

        if ($max_price) {
            $max_price = intval($max_price);
            $conditions[] = "((product__link.reduced_price is not null and product__link.reduced_price <= $max_price) or product__link.price <= $max_price)";
        }

        if ($reduced) {
            $conditions[] = $this->getReducedFilter();
        }

        $query_builder->conditions($conditions);

        if ($orderby) {
            $query_builder->order($this->getOrderByClause($orderby, $order, null));
        }

        if ($limit) {
            $query_builder->limit(intval($limit));
        }

        $query_builder->groupBy('product.product_id');

        return $this->getAll($query_builder->buildQuery());
    }
}