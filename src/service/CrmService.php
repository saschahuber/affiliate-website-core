<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\persistence\CrmNoteRepository;
use saschahuber\affiliatewebsitecore\persistence\CrmContactPersonRepository;
use saschahuber\affiliatewebsitecore\persistence\CrmContactRepository;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class CrmService extends Service
{
    const NOTE_TYPE_COMPANY = "company";
    const NOTE_TYPE_PERSON = "person";

    public function __construct()
    {
        parent::__construct();
        $this->crm_contact_repository = new CrmContactRepository();
        $this->crm_contact_person_repository = new CrmContactPersonRepository();
        $this->crm_note_repository = new CrmNoteRepository();
    }

    public function getContactByLinkedElement($type, $id){
        AuthHelper::hasPermission(AffiliateWebsitePermissionService::VIEW_CRM);
        return $this->crm_contact_repository->getByLinkedElement($type, $id);
    }

    public function getContact($id){
        return $this->crm_contact_repository->getById($id);
    }

    public function getAllContacts(){
        $contacts = $this->crm_contact_repository->getAllContacts();

        foreach($contacts as $contact){
            $contact->persons = $this->getContactPersonsByContact($contact->id);

            $contact->notes = $this->getNotesByTypeAndId(self::NOTE_TYPE_COMPANY, $contact->id);
        }

        return $contacts;
    }

    public function createContact($item){
        return $this->crm_contact_repository->create($item);
    }

    public function updateContact($item){
        return $this->crm_contact_repository->update($item);
    }

    public function getContactPerson($id){
        return $this->crm_contact_person_repository->getById($id);
    }

    public function getContactPersonsByContact($id){
        $persons = $this->crm_contact_person_repository->getAllByContactId($id);

        foreach($persons as $person){
            $person->notes = $this->getNotesByTypeAndId(self::NOTE_TYPE_PERSON, $person->id);
        }

        return $persons;
    }

    public function createContactPerson($item){
        return $this->crm_contact_person_repository->create($item);
    }

    public function updateContactPerson($item){
        return $this->crm_contact_person_repository->update($item);
    }

    public function getNote($id){
        return $this->crm_note_repository->getById($id);
    }

    public function getAllNotes(){
        return $this->crm_note_repository->getAll();
    }

    public function getNotesByTypeAndId($type, $id){
        return $this->crm_note_repository->getByLinkedElement($type, $id);
    }

    public function createNote($item){
        return $this->crm_note_repository->create($item);
    }

    public function updateNote($item){
        return $this->crm_note_repository->update($item);
    }
}