<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

define('AMAZON_IMG_DIR', MEDIA_DIR . '/amazon-images/');
define('AMAZON_IMG_SRC', '/data/media/amazon-images/');

#[AllowDynamicProperties]
class AmazonImageService
{
    function __construct(){}

    public function getImage($product)
    {
        $amazon_url = $product->meta['product_primary_image'];

        if (strpos($amazon_url, "https://m.media-amazon.com/images/") !== 0) {
            return false;
        }

        $path_parts = pathinfo($amazon_url);

        $asin_suffix = '';
        if (isset($product->amazon_asin) && strlen($product->amazon_asin) > 0) {
            $asin_suffix = '-' . $product->amazon_asin;
        }

        $filename = UrlHelper::alias($product->title) . $asin_suffix . '-' . substr(md5($product->id), 0, 6) . '.' . $path_parts['extension'];
        return $this->loadImage($amazon_url, $filename);
    }

    public function getAdditionalImage($product, $meta_key)
    {
        $amazon_url = $product->meta[$meta_key];

        if (strpos($amazon_url, "https://m.media-amazon.com/images/") !== 0) {
            return false;
        }

        $path_parts = pathinfo($amazon_url);

        $asin_suffix = '';
        if (isset($product->amazon_asin) && strlen($product->amazon_asin) > 0) {
            $asin_suffix = '-' . $product->amazon_asin;
        }

        $filename = UrlHelper::alias($product->title) . $asin_suffix . '-' . substr(md5($product->id), 0, 6)  . '-' . $meta_key . '.' . $path_parts['extension'];
        return $this->loadImage($amazon_url, $filename);
    }

    public function loadImage($url, $filename)
    {
        if (!file_exists(AMAZON_IMG_DIR . $filename)
            || (time() - filemtime(AMAZON_IMG_DIR . $filename)) >= AMAZON_IMG_MAX_AGE) {
            FileHelper::createDirIfNotExists(AMAZON_IMG_DIR);

            if (file_exists(AMAZON_IMG_DIR . $filename)) {
                unlink(AMAZON_IMG_DIR . $filename);
            }

            $ch = curl_init($url);
            $fp = fopen(AMAZON_IMG_DIR . $filename, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            #curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
        }

        return AMAZON_IMG_SRC.$filename;
    }
}