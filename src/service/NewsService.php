<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\saastemplatecore\service\Service;

#[AllowDynamicProperties]
class NewsService extends Service
{

    public function __construct()
    {
        parent::__construct();
        $this->news_manager = new NewsManager();
    }

    public function deleteNewsItem($news_id)
    {
        return $this->DB->doTransaction(function() use($news_id) {

            $this->DB->query('DELETE FROM news__taxonomy_mapping where news_id = ' . intval($news_id));

            $this->news_manager->delete($news_id);

            return true;
        });
    }
}