<?php

namespace saschahuber\affiliatewebsitecore\service;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\model\Toast;
use saschahuber\affiliatewebsitecore\persistence\UserProfileRepository;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\service\Service;

class UserProfileService extends Service
{
    const FIELD_INTERESTS = 'interests';

    const INTEREST_THRESHOLD = 0.05;

    public function __construct()
    {
        parent::__construct();
        $this->user_profile_repository = new UserProfileRepository();
    }

    public function findFingerprintsWithProfilesToUpdate($log_time_max_ago_days=60, $profile_update_period = 14, $min_hits = 5, $limit=50){
        return $this->user_profile_repository->findFingerprintsWithProfilesToUpdate($log_time_max_ago_days, $profile_update_period, $min_hits, $limit);
    }

    public function getUserProfiles($limit=null){
        return $this->user_profile_repository->getAllLimit($limit);
    }

    public function getProfile($fingerprint){
        return $this->user_profile_repository->getProfileByFingerprint($fingerprint);
    }

    public function getRecommendationToastForFingerprint($fingerprint, $current_url){
        $user_profile = $this->getProfile($fingerprint);

        $user_interests = ArrayHelper::getArrayValue($user_profile['profile_data'], 'interests');

        $content = null;

        $interests_data = [];
        $interests_mapping = [];
        foreach($user_interests as $type => $interests){
            foreach($interests as $interest){
                $interest['type'] = $type;
                $id = $interest['id'];
                $value = $interest['value'];

                $interests_data["{$type}__{$id}"] = $interest;
                $interests_mapping["{$type}__{$id}"] = $value;
            }
        }

        asort($interests_mapping);

        $selectable_interests = array_slice($interests_mapping, 0, 5);

        $random_interest = ArrayHelper::rand(array_keys($selectable_interests));

        $selected_interest = $interests_data[$random_interest];
        
        switch($selected_interest['type']){
            case ProductManager::TYPE_PRODUCT_CATEGORY:
                $product_manager = AffiliateInterfacesHelper::getProductManager();
                $taxonomy = $product_manager->getTaxonomyById($selected_interest['id']);

                $permalink = $product_manager->getTaxonomyPermalink($taxonomy);

                // Nicht anzeigen, wenn der Nutzer aktuell auf dieser Seite ist
                if($permalink === parse_url($current_url)['path']){
                    return null;
                }

                $content = BufferHelper::buffered(function() use ($selected_interest, $product_manager, $taxonomy, $permalink){
                    $title = $taxonomy->title;
                    $image = (new ProductTaxonomyThumbnailGeneratorService())->getThumbnailSrc($taxonomy);

                    ?>
                    <a href="<?=$permalink?>" target="_blank" onclick="saveAnalyticsEvent('toast_click', {type: '<?=$selected_interest['type']?>', id: <?=$selected_interest['id']?>})">
                        <img src="<?=$image?>">
                    </a>

                    <script>
                        saveAnalyticsEvent('toast_show', {toast_type: 'user_recommendation', type: '<?=$selected_interest['type']?>', id: <?=$selected_interest['id']?>})
                    </script>
                    <?php
                });

                return new Toast('🏆 Empfohlen für dich!', $content, 'Gerade eben', 10000);
        }

        return null;
    }

    public function updateProfile($fingerprint, $log_map){
        $total_hits = 0;
        foreach($log_map as $url => $timestamps){
            $total_hits += count($timestamps);
        }

        $interests = $this->getInterestsFromLogMap($log_map, $total_hits, self::INTEREST_THRESHOLD);

        echo "[interests] fingerprint: $fingerprint => ".json_encode($interests) . PHP_EOL;

        if(count($interests) > 0) {
            $this->user_profile_repository->updateUserProfileField($fingerprint, null, null, null, self::FIELD_INTERESTS, json_encode($interests));
        }
        else {
            $this->user_profile_repository->deleteByFingerprintAndDataKey($fingerprint, self::FIELD_INTERESTS);
        }
    }

    private function getFilteredInterestsFromLogMap($interests, $total_hits, $interest_threshold){
        #echo json_encode(array_keys($interests)) . PHP_EOL;

        $view_avg = [];

        foreach($interests as $interest_key => $views){

            $view_sum = 0;

            foreach($views as $view_id => $count){
                $view_sum += $count;
            }

            $avg = floatval($view_sum) / count($views);

            $view_avg[$interest_key] = $avg;
        }

        #echo json_encode($view_avg) . PHP_EOL;

        $output_interests = [];
        foreach($interests as $type => $ids){
            foreach($ids as $id => $count){
                #echo "$type [$id]: $count => ".($total_hits*$interest_threshold)."; $count => ".$view_avg[$type]."\n";

                #$less_views_than_threshold = $count < intval($total_hits*$interest_threshold);
                $less_views_than_average = $count < floatval($view_avg[$type]);

                if($less_views_than_average){
                    continue;
                }

                if (!array_key_exists($type, $output_interests)) {
                    $output_interests[$type] = [];
                }

                $output_interests[$type][] = ['id' => $id, 'value' => floatval($count)/floatval($view_avg[$type])];
            }
        }

        return $output_interests;
    }

    private function getInterestsFromLogMap($log_map, $total_hits, $interest_threshold){
        $interests = [];
        foreach($log_map as $url => $timestamps){
            #echo $url . PHP_EOL;

            $results = $this->getElementTypeAndIdFromUrl($url);

            #echo "Results [$url]: " . json_encode($results) . PHP_EOL;

            foreach($results as $result) {
                if (!array_key_exists($result['type'], $interests)) {
                    $interests[$result['type']] = [];
                }

                if (!array_key_exists($result['id'], $interests[$result['type']])) {
                    $interests[$result['type']][$result['id']] = 0;
                }

                $interests[$result['type']][$result['id']] += count($timestamps);
            }
        }

        #echo "Interessen: " . json_encode($interests) . PHP_EOL;

        return $this->getFilteredInterestsFromLogMap($interests, $total_hits, $interest_threshold);
    }

    public function getElementTypeAndIdFromUrl($url){
        $path_parts = explode('/', trim($url, '/'));

        $last_path_item = $path_parts[array_key_last($path_parts)];

        $output = [];

        if(count($path_parts) > 1){
            #echo $path_parts[0] . PHP_EOL;

            switch ($path_parts[0]){
                case PostManager::URL_PREFIX:
                    $post_manager = new PostManager();

                    $post = $post_manager->getByPermalink($last_path_item);
                    if($post){
                        foreach($post->taxonomies as $post_taxonomy){
                            $output[] = ['type' => PostManager::TYPE_POST_CATEGORY, 'id' => $post_taxonomy->id];
                        }
                    }
                    else {
                        $post_taxonomies = $post_manager->getTaxonomyByPermalinkMulti([$last_path_item]);
                        foreach($post_taxonomies as $post_taxonomy) {
                            $output[] = ['type' => PostManager::TYPE_POST_CATEGORY, 'id' => $post_taxonomy->id];
                        }
                    }
                    break;
                case ProductManager::URL_PREFIX:
                    $product_manager = AffiliateInterfacesHelper::getProductManager();

                    $brand_manager = new BrandManager();

                    $product = $product_manager->getByPermalink($last_path_item);
                    if($product){
                        #echo "Produkt\n";
                        $product_manager->addMeta($product);
                        foreach($product->taxonomies as $product_taxonomy){
                            $output[] = ['type' => ProductManager::TYPE_PRODUCT_CATEGORY, 'id' => $product_taxonomy->id];
                        }
                        foreach($brand_manager->getBrandsByProductId($product->id) as $product_brand){
                            $output[] = ['type' => BrandManager::TYPE_BRAND, 'id' => $product_brand->id];
                        }
                        #foreach($product->systems as $product_system){
                        #    $output[] = ['type' => SystemManager::TYPE_SYSTEM, 'id' => $product_system->id];
                        #}
                        #foreach($room_manager->getRoomsByProductId($product->id) as $product_room){
                        #    $output[] = ['type' => RoomManager::TYPE_ROOM, 'id' => $product_room->id];
                        #}
                    }
                    else {
                        #echo "Produkt-Kategorie $last_path_item\n";
                        $product_taxonomies = $product_manager->getTaxonomyByPermalinkMulti([$last_path_item]);
                        foreach($product_taxonomies as $product_taxonomy) {
                            #echo json_encode(['type' => ProductManager::TYPE_PRODUCT_CATEGORY, 'id' => $product_taxonomy->id]) . PHP_EOL;

                            $output[] = ['type' => ProductManager::TYPE_PRODUCT_CATEGORY, 'id' => $product_taxonomy->id];
                        }
                    }
                    break;
                case NewsManager::URL_PREFIX:
                    $news_manager = new NewsManager();

                    $news_item = $news_manager->getByPermalink($last_path_item);
                    if($news_item){
                        foreach($news_item->taxonomies as $news_taxonomy){
                            $output[] = ['type' => NewsManager::TYPE_NEWS_CATEGORY, 'id' => $news_taxonomy->id];
                        }
                    }
                    else {
                        $post_taxonomies = $news_manager->getTaxonomyByPermalinkMulti([$last_path_item]);
                        foreach($post_taxonomies as $post_taxonomy) {
                            $output[] = ['type' => NewsManager::TYPE_NEWS_CATEGORY, 'id' => $post_taxonomy->id];
                        }
                    }
                    break;
                case BrandManager::URL_PREFIX:
                    $brand_manager = new BrandManager();
                    $brand_item = $brand_manager->getByPermalink($last_path_item);
                    if($brand_item){
                        $output[] = ['type' => BrandManager::TYPE_BRAND, 'id' => $brand_item->id];
                    }
                    break;
                #case RoomManager::URL_PREFIX:
                #    $room_manager = new RoomManager();
                #    $room_item = $room_manager->getByPermalink($last_path_item);
                #    if($room_item){
                #        $output[] = ['type' => RoomManager::TYPE_ROOM, 'id' => $room_item->id];
                #    }
                #    break;
                #case SystemManager::URL_PREFIX:
                #    $system_manager = new SystemManager();
                #    $system_item = $system_manager->getByPermalink($last_path_item);
                #    if($system_item){
                #        $output[] = ['type' => SystemManager::TYPE_SYSTEM, 'id' => $system_item->id];
                #    }
                #    break;
            }
        }

        #echo "Output: " . json_encode($output) . PHP_EOL;

        return $output;
    }
}