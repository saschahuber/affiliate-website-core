<?php

namespace saschahuber\affiliatewebsitecore\service;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\service\IntervalService;

#[AllowDynamicProperties]
class CompanyActionLogLastIntervalService extends IntervalService
{
    function __construct()
    {
        parent::__construct();
        $this->company_action_log_service = new CompanyActionLogService();
    }

    function getActions()
    {
        return $this->company_action_log_service->getActions();
    }

    function getTimeSeries($interval = DatabaseTimeSeriesHelper::INTERVAL_MINUTE, $number = 60)
    {
        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $sql_date_format = "%Y-%m-%d %H:%i";
                $sql_interval = "MINUTE";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $sql_date_format = "%Y-%m-%d %H";
                $sql_interval = "HOUR";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $sql_date_format = "%Y-%m-%d";
                $sql_interval = "DAY";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_WEEK:
                $sql_date_format = "KW %u %x";
                $sql_interval = "WEEK";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $sql_date_format = "%Y-%m";
                $sql_interval = "MONTH";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $sql_date_format = "%Y";
                $sql_interval = "YEAR";
                break;
        }

        $query = "SELECT Date_format(Timestamp(Date_sub(Now(), INTERVAL (offset - 1) $sql_interval)), '$sql_date_format')
                                                AS time
                                     FROM (SELECT @i := @i + 1 AS Offset
                                           FROM information_schema.collation_character_set_applicability,
                                                (SELECT @i := 0) AS i) AS Y
                                     WHERE offset BETWEEN -1 AND " . intval($number) . "
                                     ORDER BY offset DESC";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[$item->time] = 0;
        }
        return $items;
    }

    function getLastIntervalLogEntries($interval = DatabaseTimeSeriesHelper::INTERVAL_MINUTE, $number = 60, $action = null, $company_id = null)
    {
        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $sql_date_format = "%Y-%m-%d %H:%i";
                $sql_interval = "MINUTE";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $sql_date_format = "%Y-%m-%d %H";
                $sql_interval = "HOUR";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $sql_date_format = "%Y-%m-%d";
                $sql_interval = "DAY";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_WEEK:
                $sql_date_format = "%x-%u";
                $sql_interval = "WEEK";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $sql_date_format = "%Y-%m";
                $sql_interval = "MONTH";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $sql_date_format = "%Y";
                $sql_interval = "YEAR";
                break;
        }

        $filter_string = "company__action_log.time > CURRENT_DATE - INTERVAL ".intval($number)." $sql_interval";
        if ($action) {
            $filter_string .= ' and action = "' . $this->DB->escape($action) . '"';
        }
        if ($company_id) {
            $filter_string .= ' and company_id = ' . intval($company_id);
        }

        $query = "SELECT Date_format(company__action_log.time, '$sql_date_format') AS timestamp,
                                           Count(*) AS anzahl
                                    FROM company__action_log
                                    WHERE " . $filter_string . "
                                    GROUP BY timestamp;";

        $db_query = $this->DB->query($query);

        $items = [];
        while ($item = $db_query->fetchObject()) {
            $items[$item->timestamp] = $item->anzahl;
        }

        return $items;
    }
}