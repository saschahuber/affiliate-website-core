<?php

namespace saschahuber\affiliatewebsitecore\model;
use JsonSerializable;

class Toast implements JsonSerializable
{
    private $title, $content, $small_info, $delay;

    public function __construct($title, $content, $small_info = null, $delay = 5000)
    {
        $this->setTitle($title);
        $this->setContent($content);
        $this->setSmallInfo($small_info);
        $this->setDelay($delay);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getSmallInfo()
    {
        return $this->small_info;
    }

    /**
     * @param mixed $small_info
     */
    public function setSmallInfo($small_info): void
    {
        $this->small_info = $small_info;
    }

    /**
     * @return mixed
     */
    public function getDelay()
    {
        return $this->delay;
    }

    /**
     * @param mixed $delay
     */
    public function setDelay($delay): void
    {
        $this->delay = $delay;
    }

    public function jsonSerialize()
    {
        return [
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'small_info' => $this->getSmallInfo(),
            'delay' => $this->getDelay()
        ];
    }
}