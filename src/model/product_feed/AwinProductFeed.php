<?php

namespace saschahuber\affiliatewebsitecore\model\product_feed;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\FileManager;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\KeywordHelper;
use ZipArchive;

#[AllowDynamicProperties]
class AwinProductFeed extends ProductFeed {

    protected function mapFeedRowToItem($feed_row){
        $price = str_replace('EUR', '', $feed_row['display_price']);

        $reduced_price = null;
        $base_price = $price;
        $product_price_old = ArrayHelper::getArrayValue($feed_row, 'product_price_old');
        if($product_price_old !== null && $product_price_old !== ""){
            $reduced_price = $price;
            $base_price = $product_price_old;
        }

        $ean = $feed_row['ean'];

        if(!$ean && array_key_exists('product_GTIN', $feed_row)){
            $ean = $feed_row['product_GTIN'];
        }

        return [
            'title' => $feed_row['product_name'],
            'external_id' => $feed_row['merchant_product_id'],
            'primary_image' => $feed_row['aw_image_url'],
            'other_images' => [],
            'is_available' => $feed_row['in_stock'],
            'price' => $base_price,
            'reduced_price' => $reduced_price,
            'ean' => ltrim($ean, '0'),
            'affiliate_link' => $feed_row['aw_deep_link'],
            'all_data' => $feed_row
        ];
    }

    protected function includeFeedItemInSearchResults($feed_item, $keyword)
    {
        $keywords = explode(' ', $keyword);

        // Check the title
        if (KeywordHelper::containsAllKeywords($feed_item['title'], $keywords)) {
            return true;
        }

        // Check the external_id
        if (KeywordHelper::containsAllKeywords($feed_item['external_id'], $keywords)) {
            return true;
        }

        // Check the ean
        if (KeywordHelper::containsAllKeywords($feed_item['ean'], $keywords)
            || (
                array_key_exists('product_GTIN', $feed_item)
                && KeywordHelper::containsAllKeywords($feed_item['product_GTIN'], $keywords)
            )) {
            return true;
        }

        return false;
    }

    public function downloadNewFeed($url){
        ob_start();
        $downloaded_feed_path = (new FileManager())->downloadTmpFile($url, 'product_feed', "product_feed_".md5($url).".zip");

        if($downloaded_feed_path === null){
            return null;
        }

        $zip_obj = new ZipArchive;
        $zip_obj->open($downloaded_feed_path);

        #echo "Status: $zip_obj->status; Filesize: ".filesize($downloaded_feed_path) . PHP_EOL;

        if($zip_obj->status === ZipArchive::ER_OPEN || filesize($downloaded_feed_path) < 100){
            unlink($downloaded_feed_path);
            return [];
        }

        $zip_obj->extractTo(TMP_DIR . '/product_feed/'.md5($url));

        $csv_file_path = FileHelper::getAbsoluteFilePaths(TMP_DIR . '/product_feed/'.md5($url))[0];

        $rows = array_map(
            function($row) {
                return str_getcsv($row, '|');
            },
            file($csv_file_path)
        );
        $header = array_shift($rows);
        foreach($rows as $row) {
            $feed[] = array_combine($header, $row);
        }
        ob_clean();

        return $this->getFeedItems($feed);
    }

    public function findByExternalId($feed_items, $external_id){
        if(!$feed_items){
            return null;
        }
        
        foreach($feed_items as $feed_item){
            if($feed_item['external_id'] == $external_id){
                return $feed_item;
            }
        }

        return null;
    }
}