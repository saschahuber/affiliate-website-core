<?php

namespace saschahuber\affiliatewebsitecore\model\product_feed;

use saschahuber\saastemplatecore\helper\KeywordHelper;
use saschahuber\saastemplatecore\manager\FileManager;

class MediaMarktSaturnProductFeed extends ProductFeed
{

    protected function mapFeedRowToItem($feed_row)
    {
        return [
            'title' => $feed_row['title_long'],
            'description' => $feed_row['title_long'],
            'external_id' => $feed_row['article_number'],
            'primary_image' => $feed_row['image_url'],
            'other_images' => [],
            'is_available' => $feed_row['availability'] === "in stock",
            'price' => $feed_row['price'],
            'reduced_price' => null,
            'ean' => ltrim($feed_row['EAN'], '0'),
            'affiliate_link' => $feed_row['default_deeplink'],
            'all_data' => $feed_row
        ];
    }

    protected function includeFeedItemInSearchResults($feed_item, $keyword)
    {
        $keywords = explode(' ', $keyword);

        // Check the title
        if (KeywordHelper::containsAllKeywords($feed_item['title'], $keywords)) {
            return true;
        }

        // Check the external_id
        if (KeywordHelper::containsAllKeywords($feed_item['external_id'], $keywords)) {
            return true;
        }

        // Check the ean
        if (KeywordHelper::containsAllKeywords($feed_item['ean'], $keywords)) {
            return true;
        }

        return false;
    }

    public function downloadNewFeed($url)
    {
        $csv_file_path = (new FileManager())->downloadTmpFile($url, 'product_feed', "product_feed_" . md5($url) . ".csv");

        $rows = array_map(
            function ($row) {
                return str_getcsv($row, ';');
            },
            file($csv_file_path)
        );
        $header = array_shift($rows);
        foreach ($rows as $row) {
            $feed[] = array_combine($header, $row);
        }

        return $this->getFeedItems($feed);
    }

    public function findByExternalId($feed_items, $external_id)
    {
        foreach ($feed_items as $feed_item) {
            if ($feed_item['external_id'] === $external_id) {
                return $feed_item;
            }
        }

        return null;
    }
}