<?php

namespace saschahuber\affiliatewebsitecore\model\product_feed;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\FileManager;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\KeywordHelper;

#[AllowDynamicProperties]
class RetailAdsProductFeed extends ProductFeed {

    protected function mapFeedRowToItem($feed_row){
        $price = $feed_row['priceText'];

        $reduced_price = null;
        $base_price = $price;
        $product_price_old = ArrayHelper::getArrayValue($feed_row, 'priceOldText');
        if($product_price_old !== null && $product_price_old !== ""){
            $reduced_price = $price;
            $base_price = $product_price_old;
        }

        return [
            'title' => $feed_row['title'],
            'external_id' => $feed_row['articleNumber'],
            'primary_image' => $feed_row['image1'],
            'other_images' => [],
            'is_available' => true,
            'price' => $base_price,
            'reduced_price' => $reduced_price,
            'ean' => $feed_row['ean'],
            'affiliate_link' => $feed_row['deeplink'],
            'all_data' => $feed_row
        ];
    }

    protected function includeFeedItemInSearchResults($feed_item, $keyword)
    {
        $keywords = explode(' ', $keyword);

        // Check the title
        if (KeywordHelper::containsAllKeywords($feed_item['title'], $keywords)) {
            return true;
        }

        // Check the external_id
        if (KeywordHelper::containsAllKeywords($feed_item['external_id'], $keywords)) {
            return true;
        }

        // Check the ean
        if (KeywordHelper::containsAllKeywords($feed_item['ean'], $keywords)) {
            return true;
        }

        return false;
    }

    public function downloadNewFeed($url)
    {
        $csv_file_path = (new FileManager())->downloadTmpFile($url, 'product_feed', "product_feed_" . md5($url) . ".csv");

        $rows = array_map(
            function ($row) {
                return str_getcsv($row, ';');
            },
            file($csv_file_path)
        );
        $header = array_shift($rows);
        foreach ($rows as $row) {
            $feed[] = array_combine($header, $row);
        }

        return $this->getFeedItems($feed);
    }

    public function findByExternalId($feed_items, $external_id){
        foreach($feed_items as $feed_item){
            if($feed_item['external_id'] == $external_id){
                return $feed_item;
            }
        }

        return null;
    }
}