<?php

namespace saschahuber\affiliatewebsitecore\model\product_feed;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\CacheHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\manager\FileManager;

#[AllowDynamicProperties]
abstract class ProductFeed {
    public function findItems($keyword, $feed){
        $feed_items = $this->loadFeed($feed);

        $feed_items_to_output = [];
        foreach($feed_items as $feed_item){
            if($this->includeFeedItemInSearchResults($feed_item, $keyword)){
                $feed_items_to_output[] = $feed_item;
            }
        }

        return $feed_items_to_output;
    }

    public function getProductDataFromExternalId($feed, $external_id){
        $feed_items = $this->loadFeed($feed); #, true);
        
        return $this->findByExternalId($feed_items, $external_id);
    }

    public abstract function findByExternalId($feed_items, $external_id);

    public function loadFeed($feed, $force_refreh=false){
        $feed_items = CacheHelper::loadCache('product_feed', md5($feed->url), 150);

        if(!$feed || $force_refreh){
            $feed_items = $this->downloadNewFeed($feed->url);

            if($feed_items!==null && count($feed_items) > 0){
                global $DB;
                $DB->query('UPDATE product__feed set last_update = CURRENT_TIMESTAMP() where product__feed_id = '.intval($feed->product__feed_id));

            }
            
            CacheHelper::saveCache('product_feed', md5($feed->url), $feed_items);
        }

        return $feed_items;
    }

    public function downloadNewFeed($url){
        LogHelper::logToMinuteLog(APP_TYPE, 'Downloading new feed from: ' . $url);

        ob_start();
        $downloaded_feed_path = (new FileManager())->downloadTmpFile($url, 'product_feed');

        $feed = [];
        $fp = fopen($downloaded_feed_path, 'r');
        while (($data = fgetcsv($fp, 0, '|', '')) !== false) {
            $feed[] = $data;
        }
        ob_clean();

        return $this->getFeedItems($feed);
    }

    public function getFeedItems($feed){
        $feed_rows = $feed;

        $feed_items = [];
        foreach($feed_rows as $feed_row){
            $feed_items[] = $this->mapFeedRowToItem($feed_row);
        }

        return $feed_items;
    }

    abstract protected function mapFeedRowToItem($feed_row);

    abstract protected function includeFeedItemInSearchResults($feed_item, $keyword);
}