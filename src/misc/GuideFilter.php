<?php

namespace saschahuber\affiliatewebsitecore\misc;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\Component;

#[AllowDynamicProperties]
class GuideFilter extends Component {
    function __construct($guide_id, $id = null){
        parent::__construct($id);
        global $DB;
        $this->DB = $DB;
        $this->guide_id = $guide_id;
        $this->guide_data = $this->loadGuideData($this->guide_id);
    }

    static function getAllowedComparators(){
        return [
            'like' => 'enthält',
            '=' => 'Ist gleich (=)',
            '<=' => 'Kleiner oder gleich (<=)',
            '<' => 'Kleiner (<)',
            '>=' => 'Größer oder gleich (>=)',
            '>' => 'Größer (>)'
        ];
    }

    static function getAllowedFilterTypes(){
        return [
            'data_field' => 'Produktdaten',
            'price' => 'Preis',
            'taxonomy' => 'Produktkategorie'
        ];
    }

    static function getAllowedResultTypes(){
        return [
            ProductManager::TYPE_PRODUCT => 'Produkt'
        ];
    }

    function loadGuideData($guide_id){
        $guide_object = $this->DB->query("SELECT * FROM guide_filter
                    where guide_filter_id = ".intval($guide_id))->fetchObject();
        $guide_data = array(
            'title' => $guide_object->title,
            'result_type' => $guide_object->result_type,
            'pre_filter_category_ids' => $guide_object->pre_filter_category_ids,
            'filter_steps' => $this->getGuideSteps($guide_id)
        );

        return $guide_data;
    }

    function getGuideSteps($guide_id){
        $dbquery = $this->DB->query("SELECT * FROM guide_filter__step
                    where guide_filter_id = ".intval($guide_id).' and active = 1 ORDER BY step_order ASC');
        $steps = array();
        while($step_data = $dbquery->fetchObject()){
            $step = array(
                'step_id' => $step_data->guide_filter__step_id,
                'title' => $step_data->title,
                'description' => $step_data->description,
                'skippable' => $step_data->skippable,
                'multiple_selections' => $step_data->multiple_selections,
                'extend_results' => $step_data->extend_results,
                'filter_progress_text' => $step_data->filter_progress_text,
                'options' => $this->getGuideStepOptions($step_data->guide_filter__step_id),
                'conditions' => $this->getGuideStepConditions($step_data->guide_filter__step_id)
            );
            $steps[] = $step;
        }
        return $steps;
    }

    function getGuideStepOptions($step_id){
        $dbquery = $this->DB->query("SELECT * FROM guide_filter__step_option
                    where guide_filter__step_id = ".intval($step_id).' and active = 1 ORDER BY option_order ASC');
        $step_options = array();
        while($step_option_data = $dbquery->fetchObject()){
            $step_option = array(
                'option_id' => $step_option_data->guide_filter__step_option_id,
                'img' => $step_option_data->img,
                'icon' => $step_option_data->icon,
                'label' => $step_option_data->label,
                'and_connections' => $step_option_data->and_connections,
                'filter' => $this->getGuideStepOptionFilter($step_option_data->guide_filter__step_option_id)
            );
            $step_options[] = $step_option;
        }
        return $step_options;
    }

    function getGuideStepConditions($step_id){
        $dbquery = $this->DB->query("SELECT * FROM guide_filter__step_condition
                    where guide_filter__step_id = ".intval($step_id));
        $step_conditions = array();
        while($step_option_data = $dbquery->fetchObject()){
            $step_condition = array(
                'condition_step_id' => $step_option_data->condition_step_id,
                'condition_option_id' => $step_option_data->condition_option_id
            );
            $step_conditions[] = $step_condition;
        }
        return $step_conditions;
    }

    function getGuideStepOptionFilter($option_id){
        $dbquery = $this->DB->query("SELECT * FROM guide_filter__step_option_filter 
                    where guide_filter__step_option_id = ".intval($option_id));
        $step_option_filters = array();
        while($step_option_filter_data = $dbquery->fetchObject()){
            $step_option_filter = array(
                'filter_type' => $step_option_filter_data->filter_type,
                'filter_key' => $step_option_filter_data->filter_key,
                'filter_comparator' => $step_option_filter_data->filter_comparator,
                'filter_value' => $step_option_filter_data->filter_value
            );
            $step_option_filters[] = $step_option_filter;
        }
        return $step_option_filters;
    }

    function buildQueryFromFilterSelections($filter_selections){
        $filter_criteria = array();

        foreach(array_keys($filter_selections) as $filter_step){
            $filter_step_data = $this->guide_data['filter_steps'][$filter_step];

            if(!isset($filter_criteria[$filter_step])){
                $filter_criteria[$filter_step] = array(
                    'extend_results' => $filter_step_data['extend_results'],
                    'options' => array()
                );
            }

            $step_options = $filter_step_data['options'];

            foreach($filter_selections[$filter_step] as $selection) {
                $filter_criteria[$filter_step]['options'][] = $step_options[$selection];
            }
        }

        return $this->buildFilterQuery($filter_criteria);
    }

    function getFilterResults($filter_selections){
        $filter_query = $this->buildQueryFromFilterSelections($filter_selections);
        return $this->displayResults($filter_query . " LIMIT 5");
    }

    function getResultCount($filter_selections){
        $filter_query = $this->buildQueryFromFilterSelections($filter_selections);
        return $this->DB->query($filter_query)->num_rows;
    }

    function displayResults($filter_query){
        $ids = [];

        $dbquery = $this->DB->query($filter_query);
        while($item = $dbquery->fetchObject()){
            $ids[] = $item->id;
        }

        $result_type = $this->guide_data['result_type'];
        switch ($result_type){
            case ProductManager::TYPE_PRODUCT:
                $product_manager = AffiliateInterfacesHelper::getProductManager();
                $products = $product_manager->getByIds($ids, true);
                $results = [];
                foreach($products as $product){
                    $results[] = $product_manager->displayProduct($product, null, null, ProductManager::LAYOUT_LIST, true);
                }
                return $this->createResultList($results);
        }

        return "Keine Ergebnisse gefunden...";
    }

    function createResultList($results){
        ob_start();
        ?>
            <?php if(count($results) > 0): ?>
                <?php $first_result = array_shift($results); ?>
                <div>
                    <div id="top-result-container">
                        <h2 id="top-result-headline">Dein Top Ergebnis:</h2>
                        <?=$first_result?>
                    </div>
                    <?php if(count($results) > 0): ?>
                    <div id="more-results-container">
                        <h3 id="more-results-headline">Weitere passende Ergebnisse:</h3>
                        <?php foreach($results as $result): ?>
                            <div class="row">
                                <?=$result?>
                            </div>
                            <br>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                    <br>
                    <div style="text-align: center">
                        <button class="btn btn-primary" onclick="resetGuideFilter()">
                            <i class="fas fa-undo"></i> Suche zurücksetzen
                        </button>
                    </div>
                    <br>
                </div>
            <?php else: ?>
                <div id="no-guide-results-container">
                    <div>
                        <i class="far fa-frown"></i>
                        <p>Es wurden keine Ergebnisse für deine Suche gefunden...</p>
                    </div>
                    <button class="btn btn-primary" onclick="resetGuideFilter()">
                        <i class="fas fa-undo"></i> Suche zurücksetzen
                    </button>
                </div>
            <?php endif; ?>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function buildFilterQuery($filter_criteria){
        $result_type = $this->guide_data['result_type'];
        $pre_filter_categories = explode(',', $this->guide_data['pre_filter_category_ids']);

        switch($result_type){
            case ProductManager::TYPE_PRODUCT:
                return $this->buildProductFilterQuery($pre_filter_categories, $filter_criteria);
        }

        return null;
    }

    function buildProductFilterQuery($pre_filter_categories, $filter_criteria){
        $filter_query_parts = array();

        $step_joins = array();

        $filter_step_num = 1;
        foreach($filter_criteria as $filter_step_data){
            $step_table = "data".$filter_step_num;
            $filter_step_num++;
            $step_joins[] = "LEFT JOIN product__data as $step_table on product.product_id = $step_table.product_id";

            $filter_option_queries = array();
            foreach($filter_step_data['options'] as $filter_option_selection){
                $filter_logic = 'and';
                if(array_key_exists('and_connections', $filter_option_selection)){
                    if($filter_option_selection['and_connections'] === 0){
                        $filter_logic = 'or';
                    }
                }
                $filter_options = array();
                foreach($filter_option_selection['filter'] as $option_filter_data){
                    $query_part = false;

                    $key = $option_filter_data['filter_key'];
                    $value = $this->DB->escape($option_filter_data['filter_value']);
                    $comparator = "=";

                    switch ($option_filter_data['filter_comparator']){
                        case "like":
                            $comparator = "like";
                            $value = $this->DB->escape($option_filter_data['filter_value']);
                            break;
                        case "=":
                            $comparator = "=";
                            $value = "'" . $this->DB->escape($option_filter_data['filter_value']) . "'";
                            break;
                        case "<=":
                            $comparator = "<=";
                            $value = floatval($option_filter_data['filter_value']);
                            break;
                        case "<":
                            $comparator = "<";
                            $value = floatval($option_filter_data['filter_value']);
                            break;
                        case ">=":
                            $comparator = ">=";
                            $value = floatval($option_filter_data['filter_value']);
                            break;
                        case ">":
                            $comparator = ">";
                            $value = floatval($option_filter_data['filter_value']);
                            break;
                    }

                    switch ($option_filter_data['filter_type']){
                        case 'data_field':
                            switch ($comparator){
                                case "like":
                                    $query_part = "($step_table.product__data_field_id = $key and $step_table.value LIKE '%$value%')";
                                    break;
                                default:
                                    $query_part = "($step_table.product__data_field_id = $key and $step_table.value $comparator $value)";
                                    break;
                            }
                            break;
                        case 'taxonomy':
                            $query_part = "(product__taxonomy_mapping.taxonomy_id = $value)";
                            break;
                        case 'price':
                                if(in_array($comparator, ['>=', '>'])){
                                    $query_part = "(
                                        (product__link.reduced_price is not null and product__link.reduced_price > 0 and (product__link.reduced_price $comparator $value))
                                            or (product__link.price $comparator $value)
                                    )";
                                }
                                else if(in_array($comparator, ['<=', '<'])){
                                    $query_part = "(
                                        (product__link.reduced_price is not null and product__link.reduced_price > 0 and (product__link.reduced_price $comparator $value)) 
                                            or (product__link.price $comparator $value)
                                    )";
                                }
                                else if($comparator === '='){
                                    $query_part = "((product__link.reduced_price = $value) or (product__link.price = $value))";
                                }
                            break;
                    }

                    if($query_part) {
                        $filter_options[] = $query_part;
                    }
                }
                if(count($filter_options) > 0) {
                    $filter_options_query = '(' . implode(" $filter_logic ", $filter_options) . ')';
                    $filter_option_queries[] = $filter_options_query;
                }
            }
            $extend_results = $filter_step_data['extend_results'];
            $filter_logic = $extend_results?'or':'and';

            if(count($filter_option_queries) > 0) {
                $filter_query_parts[] = '(' . implode(" $filter_logic ", $filter_option_queries) . ')';
            }
        }

        $filter_query = "";
        if(count($filter_query_parts) > 0){
            $filter_query = ' and (' . implode(' and ', $filter_query_parts) . ')';
        }

        $filter_query .= ' and is_available = 1';

        $pre_filter = "";
        if(count($pre_filter_categories) > 0){
            $pre_filter = "product__taxonomy_mapping.taxonomy_id in (".implode(', ', $pre_filter_categories).")";
        }

        $query = "SELECT distinct(product.product_id) as id FROM product
        LEFT JOIN product__taxonomy_mapping on product.product_id = product__taxonomy_mapping.product_id
        JOIN product__link on product.product_id = product__link.product_id
        ".implode(" ", $step_joins)."
        WHERE $pre_filter and status = 'publish' 
        and (product__link.is_available = 1 or product.is_fake = 0)
        $filter_query";

        return $query;
    }

    function build(){
        echo $this->getFilterHtml()
            . $this->getFilterStylesheet()
            . $this->getFilterScript();
    }

    function getFilterHtml(){
        ob_start();
        ?>
        <div id="guide_filter">
            <?php if($this->guide_data['title'] !== null): ?>
                <div class="filter-title-container">
                    <p><strong></strong></p>
                </div>
            <?php endif; ?>
            <div class="filter-container">
                <div id="filter-step-progress-bar">
                    <div id="filter-step-progress" style="width: 0%;"></div>
                </div>
                <div class="filter-info-container">
                    <p><strong id="filter-step-title"></strong></p>
                    <p id="filter-step-description"></p>
                </div>
                <div class="filter-options-container">
                    <form class="filter-options-form"></form>
                </div>
                <div class="filter-loading-container">
                    <p id="filter-progress-info"></p>
                    <div id="filter-finished-progress-bar-container">
                        <div id="filter-finished-progress-bar">
                            <div id="filter-finished-progress" style="width: 0%;"></div>
                        </div>
                    </div>
                </div>
                <div class="filter-result-count-container"></div>
                <div class="filter-buttons-container"></div>
                <div id="product-guide-results-list"></div>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getFilterScript(){
        ob_start();
        ?>
        <script>
            var guideFilterId = <?=$this->guide_id?>;
            var guideFilterData = <?=json_encode($this->guide_data)?>;
            <?=file_get_contents(AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/GuideFilter.js')?>
        </script>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getFilterStylesheet(){
        ob_start();
        ?>
        <style>
            <?=file_get_contents(AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/css/GuideFilter.css')?>
        </style>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}