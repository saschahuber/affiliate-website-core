<?php

namespace saschahuber\affiliatewebsitecore\modules;

use saschahuber\affiliatewebsitecore\service\RedirectService;
use saschahuber\saastemplatecore\Database;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\manager\TrackingCodeManager;
use saschahuber\saastemplatecore\service\LoggingService;
use saschahuber\saastemplatecore\service\UserAuthService;
use saschahuber\saastemplatecore\modules\AbstractHandler;

class AffiliateAppMainHandler extends AbstractHandler
{
    private $output_creation_callback;

    public function __construct($output_creation_callback, $additional_handle_function = null){
        parent::__construct($additional_handle_function);
        $this->output_creation_callback = $output_creation_callback;
    }

    public function handle(){
        global $DB, $CONFIG, $USER_AUTH_SERVICE, $ROUTER;

        if(in_array(RequestHelper::getClientIp(), $CONFIG->forbidden_ips)) {
            http_response_code(410);
            die();
        }

        #session_name($CONFIG->host);
        session_set_cookie_params(0, '/', '.' . $CONFIG->host);
        ini_set('session.cookie_domain', '.' . $CONFIG->host);
        session_start();

        // $DB: Wrapper für mysqli-Objekt
        $DB = new Database();

        $logging_service = new LoggingService();

        $url = parse_url($_SERVER['REQUEST_URI']);
        $urlPath = ($url && array_key_exists('path', $url)) ? explode("/", $url['path']) : "/";
        $isNewsletterPage = isset($urlPath[1]) && ($urlPath[1] === "newsletter");

        // Wenn nicht Root-URL => Slash am Ende entfernen und auf Version ohne Slash leiten
        if($url && $url['path']) {
            $cleaned_url_path = rtrim($url['path'], '/');
            if (strlen($url['path']) > 1 && $url['path'] !== $cleaned_url_path) {
                UrlHelper::redirect($cleaned_url_path);
            }
        }

        $USER_AUTH_SERVICE = new UserAuthService();
        $USER_AUTH_SERVICE->setup();

        // Seiten-Cache nutzen?
        if(AuthHelper::isLoggedIn()){
            $CONFIG->page_caching = false;
        }

        $canCache = ($CONFIG->page_caching
            && isset($_SERVER['REQUEST_METHOD'])
            && $_SERVER['REQUEST_METHOD'] == 'GET'
            && !$_POST
            && !$isNewsletterPage
            # Get-Parameter bei denen kein Cache erzeugt werden darf hier hinzufügen
            &&  !array_intersect_key($_GET, array_flip(['key', 'preview']))
            && (!isset($_SESSION['user_id']) or $_SESSION['user_id'] <= 0)
        );

        header('page-cache-canCache: ' . intval($canCache));
        if ($canCache) {
            $path = isset($url['path']) ? explode('/', trim($url['path'], '/')) : [];
            $id_uri = strtolower(implode('/', $path));

            $cache_path = CACHE_DIR . '/page/page_' . md5($_SERVER['HTTP_HOST'] . '/' . $id_uri);

            unset($getvars, $id_uri);

            if (file_exists($cache_path) &&
                filectime($cache_path) > time() - $CONFIG->cache_life) {
                header('page-cache-used: 1');
                $GLOBALS['page-cache-used'] = 1;

                $output = gzdecode(file_get_contents($cache_path));
                echo $output;
                return;
            }

            unset($path);
        } else {
            $GLOBALS['page-cache-used'] = 0;
            header('page-cache-used: 0');
        }

        $GLOBALS['page-cache-enabled'] = intval($CONFIG->page_caching);
        header('page-cache-enabled: ' . intval($CONFIG->page_caching));
        header('Expires: ' . gmdate('D, d M Y H:i:s', time() + (86400 * 30)) . ' GMT');

        (new RedirectService())->redirectIfNeccessary(trim(explode("?", $url['path'])[0], '/'));

        $logging_service->log();

        TrackingCodeManager::initTrackingCodes();

        $output = ($this->output_creation_callback)();

        // Relative Anchor-Links ersetzen
        $output = str_replace('href="#', 'href="'.$_SERVER['REQUEST_URI'].'#', $output);

        if (($CONFIG->page_caching && !empty($cache_path))
            && (!empty($output) && http_response_code() == 200)) {
            header('page-cache-stored: 1');
            FileHelper::createDirIfNotExists(CACHE_DIR . '/page');
            file_put_contents($cache_path, gzencode($output));
        }

        echo $output;
        unset($output);

        // Aufräumen
        unset($USER, $ROUTER, $cache_path, $doc);
    }
}