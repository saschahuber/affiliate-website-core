<?php

namespace saschahuber\affiliatewebsitecore\modules;

use saschahuber\affiliatewebsitecore\helper\ProductImageHelper;
use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\ResizedImageHelper;
use saschahuber\saastemplatecore\helper\StaticHelper;
use saschahuber\saastemplatecore\helper\WebPHelper;
use saschahuber\saastemplatecore\modules\AbstractHandler;

class AffiliateAdminStaticHandler extends AbstractHandler
{
    public function handle(){
        define('APP_TYPE', LogHelper::APP_TYPE_ADMIN);

        ProductImageHelper::handleRequest();

        WebPHelper::handleRequest();

        ResizedImageHelper::handleRequest();

        if($this->additional_handle_function){
            ($this->additional_handle_function)();
        }
        
        ThumbnailHelper::handleRequest();

        StaticHelper::handleRequest();

        LogHelper::logToMinuteLog('Static', "Datei nicht gefunden ({$_SERVER['REQUEST_URI']})", LogHelper::LOG_LEVEL_WARNING);
    }
}