<?php

namespace saschahuber\affiliatewebsitecore\modules;

use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\saastemplatecore\modules\InitHandler;
use saschahuber\saastemplatecore\ResourceProvider;

class AffiliateInitHandler extends InitHandler {
    public function __construct($additional_handle_function = null)
    {
        parent::__construct($additional_handle_function);
    }

    public function handle(){
        global $CONFIG, $ROLE_SERVICE_CLASS, $PERMISSION_SERVICE_CLASS, $MENU_ITEMS, $LAYOUT_MANAGER;

        $this->setup();

        $LAYOUT_MANAGER = new LayoutManager();

        $this->loadConfig();

        $this->loadLanguageHelpers();

        $this->loadResourceProviders();

        require_once($this->getAppConfigPath('interfaces', $CONFIG->app));
        require_once($this->getAppConfigPath('menu', $CONFIG->app));

        setHeaders();

        set_error_handler('php_error');
        register_shutdown_function('php_done');

        $this->checkFileSystem();
    }

    protected function loadLanguageHelpers($additional_translation_dirs = []){
        parent::loadLanguageHelpers(array_merge([COMMONS_BASE . "/../vendor/saschahuber/affiliatewebsitecore/src/resources/i18n"], $additional_translation_dirs));
    }

    protected function loadResourceProviders($additional_resource_directories = []){
        if(class_exists('saschahuber\saastemplatecore\ResourceProvider')) {
            ResourceProvider::setAdditionalResourceDirectories(array_merge([COMMONS_BASE . "/../vendor/saschahuber/affiliatewebsitecore/src/resources"], $additional_resource_directories));
        }
    }
}