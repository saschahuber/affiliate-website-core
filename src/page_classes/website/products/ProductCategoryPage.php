<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\products;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\component\container\ProductContainer;
use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\scaffolding\header\ProductTaxonomyHeader;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\service\TableOfContentsService;
use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class ProductCategoryPage extends AffiliateWebsitePage {
    public function __construct($path, $taxonomy) {
        parent::__construct($path);
        $this->product_manager = new ProductManager();
        $this->taxonomy = $taxonomy;
    }

    public function getPageObject(){
        return $this->taxonomy;
    }

    public function buildPage()
    {
        global $CONFIG;

        if(count($this->path) > 0) {
            if($this->taxonomy) {
                $this->setTitle($this->taxonomy->meta_title??($this->taxonomy->title . " | {$CONFIG->app_name}"));
                $this->setDescription($this->taxonomy->meta_description);
                $this->setIndex($this->taxonomy->doindex);
                $this->setFollow($this->taxonomy->dofollow);
            }
            else{
                return null;
            }
        }
        $this->is_page = isset($this->taxonomy) && $this->taxonomy !== false;

        $this->full_url = $this->product_manager->generatePermalink($this->taxonomy);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }

        $preview_token = RequestHelper::reqstr('preview');
        $this->is_preview = false;
        if($preview_token){
            $this->is_preview = true;
            $this->temp_data = (new TempDataService())->getByToken($preview_token)->temp_data;
        }

        $this->h1 = $this->taxonomy->title;
        $this->subtitle = isset($this->taxonomy->subtitle)?$this->taxonomy->subtitle:null;

        if($this->is_preview){
            $this->robots = 'index, follow';
            if($this->temp_data->title){
                $this->taxonomy->title = $this->temp_data->title;
            }

            if($this->temp_data->content_before_products){
                $this->taxonomy->content = $this->temp_data->content_before_products;
            }

            if($this->temp_data->meta_title){
                $this->setTitle($this->temp_data->meta_title);
            }

            if($this->temp_data->meta_description){
                $this->setDescription($this->temp_data->meta_description);
            }
        }

        $this->setIndex($this->taxonomy->doindex);
        $this->setFollow($this->taxonomy->dofollow);

        $this->toc_service = new TableOfContentsService();
        $this->toc_items = $this->toc_service->generateTableOfContents($this->taxonomy->content);

        $this->show_review_score = false;
        //Wenn Kategorie == Produkttests
        if($this->taxonomy->id === 8){
            $this->show_review_score = true;
        }

        if($this->taxonomy->id == 8) {
            $this->products = $this->product_manager->findProducts(null, null, [$this->taxonomy->id], null, null, null, null, null, null,
                null, null, null, 'date', true, null);
        }
        else {
            $this->products = $this->product_manager->findProducts(null, null, [$this->taxonomy->id]);
        }

        $image_manager = new ImageManager();

        $this->header = new ProductTaxonomyHeader($this->taxonomy);

        $this->toc_service = new TableOfContentsService();

        ?>
        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>">
                <?php $this->toc_service->displayTableOfContents($this->toc_items) ?>

                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, ProductManager::TYPE_PRODUCT_CATEGORY, $this->taxonomy->id))->display(); ?>
            </div>
            <div class="<?=$CONFIG->main_content_column_classes?>">
                <div class="main-content-container">
                    <div class="text-justify-container">
                        <?=$this->taxonomy->content?>
                    </div>

                    <?php
                    if($this->taxonomy->attached_faq_id){
                        (new FaqComponent((new FaqService())->getQuestionsFromId($this->taxonomy->attached_faq_id)))->display();
                    }
                    ?>

                    <hr>
                </div>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>">
                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, ProductManager::TYPE_PRODUCT_CATEGORY, $this->taxonomy->id))->display(); ?>
            </div>
        </div>

        <?php

        /*
        if($this->current_taxonomy->id == 8){
            $this->top_rated_products = $this->product_manager->findProducts(null, null, [$this->current_taxonomy->id], null, null, null, null, null, null,
                null, null, null, 'rating', true, 3);

            (new Text("Die 3 bestbewerteten Produkte", 'h2'))->display();

            (new Text("Diese drei Produkte haben im Produkttest am besten abgeschnitten! Du solltest sie dir auf jeden Fall mal ansehen.", 'p'))->display();

            (new ProductContainer($this->top_rated_products, false, ['kategorie' => $this->current_taxonomy->id], 'grid', true, 3))->display();

            ?>
            <hr>
            <?php
        }*/

        $all_products_headline = "Alle Produkte dieser Kategorie";
        if($this->taxonomy->all_products_headline){
            $all_products_headline = $this->taxonomy->all_products_headline;
        }
        (new Text($all_products_headline, 'h2', ['centered']))->display();

        (new ProductContainer($this->products, true, ['kategorie' => $this->taxonomy->id], 'grid'))->display();

        #echo $this->product_manager->displayProducts($this->taxonomy_products, false, "grid", true, false, true, $this->show_review_score);

        ?>

        <?php (new AdContainer(AdManager::AD_TYPE_CONTENT_AFTER, ProductManager::TYPE_PRODUCT_CATEGORY, $this->taxonomy->id))->display(); ?>

        <?php

        (new AdminEditButton('dashboard/produkte/kategorien/bearbeiten/' . $this->taxonomy->id))->display();
    }
}