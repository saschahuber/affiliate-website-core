<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\products;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\component\SponsoredBadge;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\service\ProductThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\TableOfContentsService;
use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\saastemplatecore\component\scaffolding\header\BasicHeader;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class SingleProductPage extends AffiliateWebsitePage {
    public function __construct($path, $product) {
        parent::__construct($path);
        $this->product_manager = new ProductManager();
        $this->product = $product;

        $this->setDescription($product->meta_description);
        $this->setIndex($product->doindex);
        $this->setFollow($product->dofollow);
        $this->setOgImage((new ProductThumbnailGeneratorService())->getThumbnailSrc($this->product));
    }

    public function getPageObject(){
        return $this->product;
    }

    public function buildPage()
    {
        global $CONFIG;

        if(count($this->path) > 0) {
            if($this->product) {
                $this->setTitle($product->meta_title??($this->product->title . " | {$CONFIG->app_name}"));
                $this->setDescription($this->product->meta_description);
                $this->setIndex($this->product->doindex);
                $this->setFollow($this->product->dofollow);
            }
            else{
                return null;
            }
        }
        $this->is_product = isset($this->product) && $this->product !== false;

        $this->full_url = $this->product_manager->generatePermalink($this->product);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }

        $preview_token = RequestHelper::reqstr('preview');
        $this->is_preview = false;
        if($preview_token){
            $this->is_preview = true;
            $this->temp_data = (new TempDataService())->getByToken($preview_token)->temp_data;
        }

        $this->h1 = $this->product->title;
        $this->subtitle = isset($this->product->subtitle)?$this->product->subtitle:null;

        $brand_manager = new BrandManager();

        $this->full_url = $this->product_manager->generatePermalink($this->product);

        $this->toc_service = new TableOfContentsService();
        $this->toc_items = $this->toc_service->generateTableOfContents($this->product->content);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }

        if($this->is_preview){
            $this->robots = 'index, follow';
            if($this->temp_data->title){
                $this->product->title = $this->temp_data->title;
            }

            if($this->temp_data->content){
                $this->product->content = $this->temp_data->content;
            }

            if($this->temp_data->meta_title){
                $this->title = $this->temp_data->meta_title;
            }

            if($this->temp_data->meta_description){
                $this->description = $this->temp_data->meta_description;
            }
        }

        $this->system_buttons = array();
        foreach($this->product->systems as $system){
            if($system->status == Manager::STATUS_PUBLISH) {
                $button = '[button href="/smart-home-systeme/' . $system->permalink . '" shape="btn-round" size="btn-sm"]' . $system->title . '[/button]';
                $this->system_buttons[] = ShortcodeHelper::doShortcode($button);
            }
        }

        $this->brand_buttons = array();
        foreach($brand_manager->getBrandsByProductId($this->product->id) as $brand){
            if($brand->status == Manager::STATUS_PUBLISH) {
                $button = '[button href="/hersteller/' . $brand->permalink . '" shape="btn-round" size="btn-sm"]' . $brand->title . '[/button]';
                $this->brand_buttons[] = ShortcodeHelper::doShortcode($button);
            }
        }

        $this->taxonomy_buttons = array();
        foreach($this->product->taxonomies as $taxonomy){
            if($taxonomy->status == Manager::STATUS_PUBLISH) {
                $button = '[button href="/produkte/' . $taxonomy->permalink . '" shape="btn-round" size="btn-sm"]' . $taxonomy->title . '[/button]';
                $this->taxonomy_buttons[] = ShortcodeHelper::doShortcode($button);
            }
        }

        $title_to_display = $this->product->title;
        #$title_to_display = $this->product->h1;
        #if($title_to_display === null || strlen($title_to_display) < 1){
        #    $title_to_display = $this->product->title;
        #}

        ob_start();
        ?>
        <div style="margin: 75px 0;">
            <div class="produkt-container" style="background-color: rgba(255, 255, 255, 0.8); width: 90%; max-width: 1600px; margin: auto; padding: 50px; box-shadow: 0px 3px 20px #0000001A; border-radius: 20px; position: relative;">
                <div class="row" id="product-details">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="product-image">
                                <?=$this->product_manager->getProductImageTag($this->product, 300, 300, false)?>
                            </div>

                            <?php

                            if($CONFIG->enable_multiple_product_images){
                                $product_images = $this->product_manager->getProductImages($this->product);

                                if(count($product_images) > 1){
                                    ?>
                                    <script>
                                        currentlySelectedProductImage = '<?=$product_images[0]?>';
                                    </script>

                                    <div class="product-image-selector">
                                        <?php foreach($product_images as $image):

                                            $id = md5(uniqid().$image);

                                            ?>
                                            <img id="<?=$id?>" onclick="selectProductImage(this)" onmouseenter="previewProductImage(this)" onmouseleave="resetProductImage()" class="product-image-item" src="<?=$image?>">
                                        <?php endforeach; ?>
                                    </div>
                                    <?php
                                }
                            }

                            ?>

                            <br>

                            <?=$this->product_manager->getFavouriteButton($this->product->id, true)?>

                            [spacer height="24px"]

                            <div>
                                <?php if(count($this->taxonomy_buttons) > 0): ?>
                                    <p>Kategorien: <?=implode(" ", $this->taxonomy_buttons)?></p>
                                <?php endif; ?>

                                <?php if(count($this->system_buttons) > 0): ?>
                                    <p>System: <?=implode(" ", $this->system_buttons)?></p>
                                <?php endif; ?>

                                <?php if(count($this->brand_buttons) > 0): ?>
                                    <p>Hersteller: <?=implode(" ", $this->brand_buttons)?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <h1 class="product-title" style="color: #434343;"><?=$title_to_display?></h1>
                            <?php if($this->product->is_sponsored): ?>
                                <?php (new SponsoredBadge())->display(); ?>
                            <?php endif; ?>
                            <hr>
                            <div class="row">
                                <div class="col-lg-8">
                                    <?= $this->product_manager->displayProductDataTable($this->product) ?>
                                </div>
                                <div class="col-md-4">
                                    <div class="product-buybox">
                                        <div class="row">
                                            <div class="col-xxs-12 col-xs-6 col-md-12">
                                                <?=$this->product_manager->getProductBuyButtonsList($this->product)?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $header_content = ob_get_clean();

        $this->header = new BasicHeader(ShortcodeHelper::doShortcode($header_content));

        ?>

        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>"></div>
            <div class="<?=$CONFIG->main_content_column_classes?>">
                <?php
                $product_reviews = $this->product_manager->getProductReviews($this->product);
                if(count($product_reviews)):
                    ?>
                    <div class="product-reviews product-reviews-number">
                        <?=$this->product_manager->displayReviewRows($product_reviews)?>
                    </div>
                    <hr>
                <?php endif;?>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>"></div>
        </div>

        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>">
                <?php $this->toc_service->displayTableOfContents($this->toc_items) ?>

                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, ProductManager::TYPE_PRODUCT, $this->product->id))->display(); ?>
            </div>
            <div class="<?=$CONFIG->main_content_column_classes?>">
                <div class="main-content-container">
                    <div class="text-justify-container">
                        <?=ShortcodeHelper::doShortcode($this->product->content)?>
                    </div>

                    <?php
                    if($this->product->attached_faq_id){
                        (new FaqComponent((new FaqService())->getQuestionsFromId($this->product->attached_faq_id)))->display();
                    }
                    ?>

                    <hr>

                    <?php if(false && $this->product_manager->hasTaxonomyPermalink($this->product, 'produkttests')): ?>
                        <div>
                            <?=ShortcodeHelper::doShortcode('[newsletter_produkttest]')?>
                        </div>
                    <?php endif; ?>

                    <div id="product-sticky-buybox">
                        <div id="product-sticky-buybox-content">
                            <div class="image-container">
                                <div class="product-thumbnail-container">
                                    <?=$this->product_manager->getProductImageTag($this->product)?>
                                </div>
                            </div>

                            <div class="title-container">
                                <p><b><?=$this->product->title?></b></p>
                            </div>

                            <div class="button-container">
                                <?=$this->product_manager->getProductBuyButton($this->product)?>
                            </div>
                        </div>
                    </div>

                    <div id="product-bottom-buybox">
                        <?=$this->product_manager->displayProduct($this->product, null, null, ProductManager::LAYOUT_LIST, true, true, false)?>
                    </div>

                    <?php (new AdContainer(AdManager::AD_TYPE_CONTENT_AFTER, ProductManager::TYPE_PRODUCT, $this->product->id))->display(); ?>
                </div>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>">
                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, ProductManager::TYPE_PRODUCT, $this->product->id))->display(); ?>
            </div>
        </div>

        <?php (new AdContainer(AdManager::AD_TYPE_PAGE_BOTTOM, ProductManager::TYPE_PRODUCT, $this->product->id))->display(); ?>

        <?php

        $this->product_manager->getSchemeOrgData($this->product)->display();

        (new AdminEditButton('dashboard/produkte/bearbeiten/' . $this->product->id))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            div.product-image {
                text-align: center;
            }

            div.product-image .product-thumbnail {
                height: 300px !important;
            }

            div.product-image-selector {
                display: flex;
                margin-top: 10px;
                column-gap: 5px;
                max-width: 100%;
                overflow-x: auto;
                overflow-y: hidden;
            }

            img.product-image-item {
                object-fit: cover;
                width: 100%;
                max-width: 75px;
                height: fit-content;
                cursor: pointer;
                margin-bottom: 4px;
                aspect-ratio: 1;
            }

            img.product-image-item:hover {
                border: solid 1px var(--primary_color);
            }

            img.product-image-item.selected {
                border: solid 3px var(--primary_color);
            }

            div.product-image-selector::-webkit-scrollbar{
                height: 4px;
                width: 4px;
                background: #fff;
                border-radius: 4px;
            }

            div.product-image-selector::-webkit-scrollbar-thumb,
            div.product-image-selector::-webkit-scrollbar-thumb:horizontal{
                background: var(--primary_color);
                border-radius: 4px;
            }

            div.product-image picture,
            div.product-image img{
                max-width: 100%;
                max-height: 300px;
                object-fit: contain;
                height: fit-content;
            }

            h1, h2, h3, h4, h5, h6, h7, h8, h9{
                margin-bottom: unset;
            }

            #product-sticky-buybox {
                position: fixed;
                z-index: 100;
                bottom: -100px;
                width: 100%;
                left: 0;
                right: 0;
                background-color: #fff;
                box-shadow: 0px -1px 10px #aaa;
                transition: all .5s ease-in-out;
            }

            #product-sticky-buybox-content {
                max-width: 1000px;
                max-height: 100px;
                margin: auto;
                display: table;
            }

            #product-sticky-buybox.visible {
                bottom: 0;
            }

            #product-sticky-buybox .image-container {
                display: table-cell;
                max-width: 100px;
                vertical-align: middle;
                padding: 10px;
            }

            #product-sticky-buybox .image-container .product-thumbnail-container img,
            #product-sticky-buybox .image-container .product-thumbnail-container picture img {
                height: 75px;
            }

            #product-sticky-buybox .title-container {
                max-width: 700px;
                display: table-cell;
                vertical-align: middle;
                padding: 10px;
            }

            #product-sticky-buybox .title-container p{
                margin-bottom: 0;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
            }

            #product-sticky-buybox .button-container {
                max-width: 200px;
                display: table-cell;
                vertical-align: middle;
                padding: 10px;
            }

            @media (max-width: 1000px){
                #product-sticky-buybox .title-container {
                    max-width: 550px;
                }
            }

            @media (max-width: 800px){
                #product-sticky-buybox .title-container {
                    display: none;
                }

                h1 {
                    font-size: 30px !important;
                    line-height: 35px !important;
                }
            }

            @media (max-width: 400px){
                #product-sticky-buybox .image-container {
                    display: none;
                }
            }

            .produkt-container .sponsored-badge {
                position: absolute;
                top: 10px;
                right: 10px;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            window.addEventListener('load', function (e) {
                let productDetailsContainer = document.getElementById('product-details');
                let productBottomBuybox = document.getElementById('product-bottom-buybox');

                let productStickyBuybox = document.getElementById('product-sticky-buybox');

                if(!productDetailsContainer || !productBottomBuybox || !productStickyBuybox){
                    return;
                }

                document.addEventListener("scroll", (event) => {
                    if(!isElementInViewport(productDetailsContainer) && !isElementInViewport(productBottomBuybox)){
                        productStickyBuybox.classList.add('visible');
                    }
                    else {
                        productStickyBuybox.classList.remove('visible');
                    }
                });
            });

            let currentlySelectedProductImage = null;

            function resetAllSelectedProductImages(){
                let images = document.querySelectorAll('.product-image-selector img');

                images.forEach(image => {
                    image.classList.remove('selected');
                });
            }

            function selectProductImage(img){
                let imageSrc = img.src;

                currentlySelectedProductImage = imageSrc;

                resetAllSelectedProductImages();
                img.classList.add('selected');

                let productImageContainer = document.querySelector('.product-image');

                productImageContainer.innerHTML = '<img class="product-thumbnail" src="'+imageSrc+'">';
            }

            function previewProductImage(img){
                let imageSrc = img.src;

                let productImageContainer = document.querySelector('.product-image');

                productImageContainer.innerHTML = '<img class="product-thumbnail" src="'+imageSrc+'">';
            }

            function resetProductImage(){
                let productImageContainer = document.querySelector('.product-image');

                productImageContainer.innerHTML = '<img class="product-thumbnail" src="'+currentlySelectedProductImage+'">';
            }
        </script>
        <?php
    }
}