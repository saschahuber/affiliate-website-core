<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\products;

use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\routing\SubRouter;

class ProductRouter extends SubRouter{
    public function __construct($path){
        parent::__construct($path);
    }

    function getPageFromRoute(){
        $this->product_manager = new ProductManager();

        if(count($this->path) > 0) {
            $this->product = $this->product_manager->getByPermalink($this->getLastPathSegment());
        }
        $this->is_product = isset($this->product) && $this->product !== false;

        $this->current_taxonomy = $this->product_manager->getTaxonomyByPermalink($this->getLastPathSegment());
        $this->is_taxonomy = $this->current_taxonomy !== null;

        if($this->is_taxonomy){
            $page = new ProductCategoryPage($this->path, $this->current_taxonomy);

            $vg_wort_pixel_service = new VgWortPixelService();
            $vg_wort_pixel = $vg_wort_pixel_service->getById($this->current_taxonomy->vg_wort_pixel_id);
            if($vg_wort_pixel) {
                $page->setVgWortMarke($vg_wort_pixel->vg_wort_pixel);
            }

            return $page;
        }
        else if($this->is_product){
            $page = new SingleProductPage($this->path, $this->product);

            $vg_wort_pixel_service = new VgWortPixelService();
            $vg_wort_pixel = $vg_wort_pixel_service->getById($this->product->vg_wort_pixel_id);
            if($vg_wort_pixel) {
                $page->setVgWortMarke($vg_wort_pixel->vg_wort_pixel);
            }

            return $page;
        }

        return null;
    }
}