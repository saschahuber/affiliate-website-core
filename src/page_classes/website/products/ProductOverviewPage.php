<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\products;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\PostableManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\saastemplatecore\helper\RequestHelper;

#[AllowDynamicProperties]
class ProductOverviewPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);

        $this->setTitle("Smarthome Produkte & Geräte für dein Zuhause | Smarthome Blogger");
        $this->setDescription("➤ Finde die besten Smarthome Produkte! ☑️ Produkttests ☑️ Funksteckdosen ☑️ WLAN-Lampen ☑️ Google Home & Amazon Alexa Zubehör ☑️ Funkthermostate ☑️");
        $this->setIndex(true);
        $this->setFollow(true);
    }

    public function isValid(){
        return true;
    }

    public function getPageObject(){
        return null;
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->product_manager = new ProductManager();

        $preview_token = RequestHelper::reqstr('preview');
        if($preview_token){
            $this->is_preview = true;
            $this->temp_data = (new TempDataService())->getByToken($preview_token)->temp_data;
        }

        $this->taxonomies = $this->product_manager->getTaxonomies('title', true, $this->is_preview?null:PostableManager::STATUS_PUBLISH);

        $this->h1 = "Smarthome Produkte & Geräte für dein Zuhause";

        ?>

        <div class="row">
            <div class="<?=$CONFIG->left_header_column_classes?>"></div>
            <div class="<?=$CONFIG->main_header_column_classes?>">
                <div class="main-content-container">
                    <div class="text-justify-container">
                        <h1 class="centered">Finde die richtigen Smarthome Produkte für dich!</h1>
                        <p>Für ein vollständiges und funktionierendes Smarthome-System braucht es verschiedenste Produkte: Sensoren, Steckdosen, Dimmer, Unterhaltungselektronik, Hubs, Sprachassistenten und mehr. Damit du bei der Vielzahl der verschiedenen Produkte auf dem Markt noch den Überblick behalten kannst, biete ich dir auf dieser Seite die Möglichkeit, dir die richtigen Smarthome Geräte für dich zu suchen. Verschiedene <strong>Filtermöglichkeiten, Testberichte und Videos</strong> helfen dir dabei, im Smarthomedschungel die richtigen Geräte für dich und dein Zuhause zu finden!</p>

                        &nbsp;
                        <h2>Hilfreiche Testberichte</h2>
                        Auf Smarthome Blogger stelle ich dir nicht nur zahlreiche Smarthome Produkte vor, sondern zeige dir in meinen <strong>Testberichten</strong>, welche Produkte gut sind - und welche nicht so.

                        Am Ende jedes Produkttests fasse ich dir meine Erfahrungen noch einmal übersichtlich in einer Pro- &amp; Kontra-Liste zusammen, damit du besser einschätzen kannst, ob das Produkt das richtige für dich ist.

                        [produkte limit="4" orderby="date" order="desc" layout="grid" review="false" show_detail_button="true" buy_button="false" details_fields="true" details_tax="true" reduced="false" align="left" slider="false" kategorie="produkttests"]

                        [row][col class="col-sm-4"][/col][col class="col-sm-4"][button color="btn-primary" size="btn-lg" shape="btn-rounded" outline="false" block="true" icon="fa-chevron-right" icon_position="right" target="" rel="" href="/produkte/produkttests/"]Zu den Smarthome Produkttests[/button][/col][col class="col-sm-4"][/col][/row]

                        &nbsp;
                        <h2>Smarthome Systeme</h2>
                        Es gibt zahlreiche Smarthome Systeme. Da ist es oft schwer, den Überblick zu behalten. Du bist auf der Suche, nach Zubehör für dein System? Dann wähle einfach hier dein System aus und du gelangst sofort zu allen kompatiblen Produkten für dein System.
                        <p style="text-align: center;">[smarthome_systeme]
                            [row][col class="col-sm-4"][/col][col class="col-sm-4"][button color="btn-primary" size="btn-lg" shape="btn-rounded" outline="false" block="true" icon="fa-chevron-right" icon_position="right" target="" rel="" href="/system/"]Mehr Informationen[/button][/col][col class="col-sm-4"][/col][/row]</p>
                        &nbsp;
                        <h2 style="text-align: center;">Smarthome im Eigenbau? - Kein Problem!</h2>
                        [row][col class="col-sm-8"]Du möchtest dein Smarthome System nicht fertig kaufen sondern so viel wie möglich daran selbst bauen? Kein Problem! Auf Smarthome Blogger findest du nicht nur <a href="/tutorials">Tutorials für ein eigenes Smarthome System</a>, sondern auch die richtigen Produkte dafür. Mit dem Raspberry Pi zum Beispiel können voll funktionsfähige und selbst programmierbare Smarthome Systeme für wenig Geld selbst zusammengestellt werden.

                        &nbsp;

                        Dank dem offenen System können so auch mehrere verschiedene Smarthome Systeme gleichzeitig auf dem Minirechner laufen. So kannst du all deine Smarthome Geräte mit nur einem Gerät ansprechen und verwalten.[/col][col class="col-sm-4"]

                        [caption id="attachment_805" align="aligncenter" width="300"]<a class="lightbox" href="/wp-content/uploads/2017/12/smarthome-produkte-raspberry-pi.jpg"><img class="size-medium wp-image-805" src="/wp-content/uploads/2017/12/smarthome-produkte-raspberry-pi-300x164.jpg" alt="Der Raspberry Pi ist bestens für dein Smarthome geeignet." width="300" height="164" /></a> Der Raspberry Pi ist bestens für dein Smarthome geeignet.[/caption]

                        [/col][/row]

                        &nbsp;

                        [produkte orderby="date" order="desc" include="1846" layout="list" review="false" detail_button="true" buy_button="true" details_fields="true" details_tax="false" reduced="false" align="left" slider="false"]

                        &nbsp;
                        <h2 id="angebote" style="text-align: center;">Schnäppchenjäger aufgepasst!</h2>
                        Smarthome muss <strong>nicht immer teuer</strong> sein! Auch im Smarthome Bereich gibt es immer wieder viele verschiedene reduzierte Produkte. Eine Auswahl der <strong>besten Schnäppchen</strong> findest du hier:

                        [produkte limit="4" orderby="rand" order="desc" layout="grid" review="false" detail_button="true" buy_button="true" details_fields="true" details_tax="true" reduced="true" align="left"]
                        <p style="text-align: center;">Wenn du stattdessen <strong>alle Angebote</strong> sehen willst, dann klicke hier:</p>
                        &nbsp;
                        <p style="text-align: center;">[button color="btn-primary" size="btn-lg" shape="btn-rounded" outline="false" block="false" icon="fa-chevron-right" icon_position="right" target="" rel="" href="/angebote"]Alle Angebote ansehen[/button]</p>
                        &nbsp;

                        &nbsp;
                        <h2 id="addon-boards" style="text-align: center;">Erweiterungen für dein DIY-Smarthome System</h2>
                        Damit du möglichst viele Geräte mit deinem Bastelrechner ansprechen kannst, sind verschiedene Erweiterungsplatinen verfügbar, die die verschiedenen Funkprotokolle beherrschen und somit die meisten Smarthome Geräte steuern können.

                        [produkte limit="4" orderby="date" order="desc" include="239,238,237,54" layout="table-x" review="false" detail_button="true" buy_button="true" details_fields="true" details_tax="true" reduced="false" align="left" slider="false" kategorie="addon-boards"]
                        <p style="text-align: center;">[button color="btn-primary" size="btn-lg" shape="btn-rounded" outline="false" block="false" icon="fa-chevron-right" icon_position="" target="" rel="" href="/kategorie/addon-boards/"]Alle Addon-Boards ansehen[/button]</p>
                        &nbsp;

                        &nbsp;
                        <h2 id="sprachassistenten" style="text-align: center;">Ein eigener Assistent?</h2>
                        [row][col class="col-sm-4"]

                        [caption id="attachment_771" align="aligncenter" width="300"]<a class="lightbox" href="/wp-content/uploads/2017/12/smarthome-produkte-google-home.jpg"><img class="size-medium wp-image-771" src="/wp-content/uploads/2017/12/smarthome-produkte-google-home-300x189.jpg" alt="Neben dem Google Home gibt es noch viele andere Smarthome Produkte." width="300" height="189" /></a> Neben dem Google Home gibt es noch viele andere Smarthome Produkte.[/caption]

                        [/col][col class="col-sm-8"]Dank Geräten wie dem <strong>Amazon Echo</strong> oder dem <strong>Google Home</strong> kann sich nun jeder einen eigenen Assistenten zulegen, der ihm jeden Wunsch regelrecht <strong>von den Lippen abliest</strong>. Ein Satz genügt und die Arbeit wird verrichtet. Wie wird das Wetter? Wo ist der nächste Baumarkt? Einfach fragen. Auch verschiedenste Smarthome Geräte steuern die Assistenten auf Zuruf.

                        Für viele Systeme sind auch Add-Ons bzw. Plugins verfügbar, mit denen sie sich von den Assistenten Steuern lassen. So kannst du zum Beispiel auch ein selbstgebautes System mit dem Raspberry Pi bequem per Zuruf bedienen.
                        <p style="text-align: center;">[button color="btn-primary" size="btn-lg" shape="btn-rounded" outline="false" block="false" icon="fa-chevron-right" icon_position="right" target="" rel="" href="/kategorie/sprachassistent/"]Alle Sprachassistenten[/button][/col][/row]</p>
                        &nbsp;

                        &nbsp;
                        <h2 id="startersets" style="text-align: center;">Smarthome Startersets für dich</h2>
                        Damit du mit deinem System so schnell wie möglich loslegen kannst, ohne dir stundenlang die benötigten Smarthome Produkte heraussuchen zu müssen, habe ich dir eine Auswahl an Startersets zusammengestellt. Dort sind bereits alle wichtigen Dinge enthalten, die du für das jeweilige System brauchst.

                        [display_products layout="table-x" limit="3" produkte="" kategorie="11" hersteller="" system="" price_min="" price_max="" data_field="" data_values="" show_detail_button="false" show_meta="false" order="asc" order_by="rand" reduced="false" show_price="false"]

                        &nbsp;

                        &nbsp;

                        &nbsp;
                        <h2 id="ipcam" style="text-align: center;">Alles im Blick!</h2>
                        Mit IP-Kameras hast du jeden Winkel in Haus und Garten <strong>immer im Blick</strong> und wirst über ungewöhnliche Bewegungen und ungebetene Gäste sofort informiert - per <strong>Benachrichtigung auf Smartphone, Tablet, Smartwatch</strong> oder per E-Mail. Dank IR-LEDs siehst du <strong>auch bei völliger Dunkelheit</strong> immer, was in deinen vier Wänden vor sich geht. Viele modernen IP-Kameras besitzen außerdem erweiterte Funktionen, wie <strong>PoE</strong> oder das <strong>Schwenken der Kamera</strong> per App.

                        [display_products layout="table-x" limit="3" produkte="" kategorie="18" hersteller="" system="" price_min="" price_max="" data_field="" data_values="" show_detail_button="false" show_meta="false" order="asc" order_by="rand" reduced="false" show_price="false"]

                        <p style="text-align: center;">[button color="btn-primary" size="btn-lg" shape="btn-rounded" outline="false" block="false" icon="fa-chevron-right" icon_position="" target="" rel="" href="/kategorie/geraete/ip-kamera/"]Alle IP-Kameras ansehen[/button]</p>

                        <h2 style="text-align: center;">Alle Produktkategorien</h2>

                        <p style="text-align: center;">
                            <?php foreach($this->taxonomies as $taxonomy): ?>
                                [button size="btn-lg" shape="btn-round" color="btn-primary" icon="fa fa-chevron-right"
                                icon_position="right" href="<?=$this->product_manager->getTaxonomyPermalink($taxonomy)?>"]<?=$taxonomy->title?>[/button]
                            <?php endforeach; ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="<?=$CONFIG->right_header_column_classes?>"></div>
        </div>

        <?php
    }
}