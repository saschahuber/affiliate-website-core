<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\component\container\GridContainer;

class LinkPage extends AffiliateWebsitePage {
    public function buildPage()
    {
        $this->title = 'Links';
        $this->h1 = 'Links';

        $this->robots = 'noindex, nofollow';

        $link_page_item_manager = new LinkPageItemManager();

        $items = $link_page_item_manager->getAll(Manager::STATUS_PUBLISH);

        $grid_items = [];

        foreach($items as $item) {
            $grid_items[] = $link_page_item_manager->getGridItem($item);
        }

        (new GridContainer($grid_items, 2, 'lg'))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>

            main {
                padding: 0 2% 20px;
            }

            .container > * {
                margin: 15px 0;
            }

            h1, h2, h3 {
                color: inherit;
            }

            @media only screen and (max-width: 640px) {
                main > h1 {
                    margin-top: 30px;
                }
            }

            main p, main li {
                font-size: 13pt;
                line-height: unset;
            }

            main p {
                margin-bottom: 8px;
                word-spacing: 3px;
            }

            main ul:not(.job-list) > li {
                line-height: 14pt;
            }

            main > h1 {
                text-align: center;
                background-color: transparent;
                height: auto;
                padding: 0;
                white-space: normal;
            }

            main h2 {
                text-align: center;
                margin-top: 50px;
            }

            main > ul,
            main > div {
                margin-bottom: 40px
            }

            main > :last-child {
                margin-bottom: 0;
            }

            main div.compact {
                margin: 0 14% 40px;
                hyphens: auto; /* Atm not supported by Chrome */
            }

            @media only screen and (max-width: 640px) {
                main div.compact {
                    margin: 0 11.5% 40px;
                    display: none;
                }
            }

            main ul.columns > li {
                display: inline-block;
            }

            main ul.columns.companies > li {
                text-align: center;
                margin: 12px 0;
                padding: 10px 38px;
                position: relative;
                vertical-align: middle;
                height: 100px;
            }

            main ul.columns.companies > li img {
                display: inline-block;
                vertical-align: middle;
                max-width: 100%;
                max-height: 80px;
                margin: 0 auto;
                transform: translate(0, -50%);
                top: 50%;
                position: relative;
                object-fit: contain;
            }

            @media only screen and (max-width: 640px) {
                main ul.columns.companies > li {
                    width: 50% !important;
                }

                main ul.columns.companies li.featured {
                    width: calc(50% - 24px) !important;
                }
            }

            main ul.columns.companies a.button {
                margin: 30px auto 40px;
                display: table;
            }

            /* Top Company label */

            main ul.columns.companies li.featured {
                border: solid 1px #ccc;
                border-radius: 6px;
                margin: 12px;
                width: calc(25% - 24px);
            }

            main ul.columns.companies li.featured > label {
                text-transform: uppercase;
                color: #fff;
                font-size: 7pt;
                background-color: #ffb71e;
                position: absolute;
                transform: rotate(-45deg);
                top: 5px;
                left: -30px;
                padding: 0 22px;
            }

            main ul.columns.companies li.featured > label > span {
                background-color: #fff;
                position: absolute;
                top: -12px;
                width: 16px;
                height: 33px;
            }

            main ul.columns.companies li.featured > label > span:first-child {
                transform: rotate(45deg);
                left: -4px;
            }

            main ul.columns.companies li.featured > label > span:last-child {
                transform: rotate(-45deg);
                right: 0;
            }

            main ul.columns.companies li.featured > label:before,
            main ul.columns.companies li.featured > label:after {
                content: "";
                background-color: #aa7714;
                width: 8px;
                height: 8px;
                display: block;
                position: absolute;
                transform: rotate(45deg);
                background: linear-gradient(135deg, #ffb71e 0%, #ffb71e 50%, #aa7714 51%, #aa7714 100%);
            }

            main ul.columns.companies li.featured > label:before {
                left: 3px;
                top: 14px;
            }

            main ul.columns.companies li.featured > label:after {
                right: 8px;
                top: 14px;
            }

            /* Favorite buttons */

            main ul.columns.companies li > i.fa-heart {
                margin-top: 14px;
                position: absolute;
                top: 0;
                right: 14px;
                font-size: 14pt;
                font-weight: normal;
                cursor: pointer;
            }

            main ul.columns.companies li > i.fa-heart.active {
                font-weight: 900;
                animation: coinspin 1.4s ease-out;
            }

            main ul.columns.five > li {
                width: 20%;
            }

            main ul.columns.four > li {
                width: 25%;
            }

            main ul.columns.three > li {
                width: 33%;
            }

            main ul.columns.two > li {
                width: 50%;
            }

            main ul.bulletpoints > li {
                padding-left: 20px;
            }

            main ul.bulletpoints > li:before {
                content: "●";
                margin-right: 10px;
            }

            main ul.beside {
                display: table;
                width: calc(100% - 520px);
            }

            @media only screen and (max-width: 640px) {
                main ul.columns.two > li {
                    width: 100%;
                }
            }

            /* GOOGLE MAPS */

            div.gmaps {
                width: 500px;
                float: right;
                position: relative;
                line-height: 0;
                margin-left: 20px;
                border-radius: 4px;
                overflow: hidden;
            }

            div.gmaps > img {
                background-color: #f2f2f2;
                border: solid 1px #ccc;
                max-width: 100%;
                border-radius: 4px;
            }

            div.gmaps:before {
                content: "";
                display: block;
                background-image: url("./img/locale_radius.png");
                background-size: 100%;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }

            @media only screen and (max-width: 940px) {
                div.gmaps {
                    float: none;
                    margin-bottom: 40px;
                    max-width: 100%;
                    margin-left: 0;
                }

                main ul.beside {
                    width: 100%;
                }
            }

            /* Top CTA */

            main div.top-cta {
                float: right;
                margin-top: -38px;
                margin-left: 15px;
                position: relative;
                z-index: 1;
                margin-right: 3%;
            }

            main div.top-cta > a {
                padding: 20px 0;
                display: block;
            }

            main div.top-cta > a > i {
                font-size: 28px;
                vertical-align: -4px;
            }

            main div.top-cta-replacement {
                height: 20px;
            }

            @media only screen and (max-width: 640px) {
                main div.top-cta {
                    display: none;
                }
            }

            /* Final CTA */

            main div.final-cta > a {
                width: calc(50% - 20px);
            }

            main div.final-cta > a:first-of-type {
                margin-right: 20px;
            }

            main div.final-cta > a:last-of-type {
                margin-left: 20px;
            }

            @media only screen and (max-width: 640px) {
                main div.final-cta > a {
                    width: calc(50% - 5px);
                }

                main div.final-cta > a.button.big-cta {
                    font-size: 14pt;
                }

                main div.final-cta > a:first-of-type {
                    margin-right: 5px;
                }

                main div.final-cta > a:last-of-type {
                    margin-left: 5px;
                }
            }

            /* facebook pagelist hint for regional2 */

            div.fb-pagelist-hint {
                background-color: #1877f2;
                width: 300px;
                margin: 0 auto;
                color: #fff;
                padding: 14px 20px;
                font-size: 13pt;
                display: flex;
                margin-bottom: 40px;
            }

            div.fb-pagelist-hint:hover {
                cursor: pointer;
                background-color: #3487f3;
            }

            div.fb-pagelist-hint > span {
                margin: 0 16px;
                text-align: center;
                line-height: 16pt;
                padding-top: 3px;
            }

            div.fb-pagelist-hint > i {
                font-size: 32pt;
                line-height: 36pt;
            }

            @media only screen and (min-width: 1280px) {
                div.fb-pagelist-hint {
                    position: absolute;
                    top: 0;
                    left: 50%;
                    z-index: 8;
                    transform: translateX(-50%);
                }
            }

        </style>
        <?php
    }
}