<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\autoren;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\service\AuthorService;

#[AllowDynamicProperties]
class AuthorPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->author_service = new AuthorService();
        $this->author = $this->author_service->getByPermalink($this->getLastPathSegment());

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $news_manager = new NewsManager();
        $post_manager = new PostManager();

        ?>

        <h1><?=$this->author->title?></h1>

        <p><?=$this->author->description?></p>

        <?php

        $posts = $post_manager->getAllByAuthor($this->author->id);
        if(count($posts)) {
            echo '<h2>Artikel von '.$this->author->title.'</h2>';
            echo $post_manager->displayPosts($posts);
        }

        $news_items = $news_manager->getAllByAuthor($this->author->id);
        if(count($news_items)) {
            echo '<h2>News-Artikel von '.$this->author->title.'</h2>';
            echo $news_manager->displayNews($news_items);
        }

        $products = $product_manager->getAllByAuthor($this->author->id);
        if(count($products)) {
            echo '<h2>Produkt-Texte von '.$this->author->title.'</h2>';
            echo $product_manager->displayProducts($products, false, ProductManager::LAYOUT_GRID, true);
        }

        (new AdminEditButton('dashboard/system/autoren/bearbeiten/' . $this->author->id))->display();
    }
}