<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\autoren;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\helper\ErrorHelper;

#[AllowDynamicProperties]
class AuthorOverviewPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->robots = 'noindex, follow';
        $this->title = "Autoren von {$CONFIG->app_name}";
        $this->description = "Übersicht unserer Autoren!";

        ErrorHelper::http(ErrorHelper::HTTP_ERROR_NOT_FOUND);
    }
}