<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\news;

use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\routing\SubRouter;

class NewsRouter extends SubRouter{
    public function __construct($path){
        parent::__construct($path);
    }

    function getPageFromRoute(){
        $this->news_manager = new NewsManager();

        if(count($this->path) > 0) {
            $this->news_post = $this->news_manager->getByPermalink($this->getLastPathSegment());
        }

        $this->is_news_post = isset($this->news_post) && $this->news_post !== false;

        if($this->is_news_post){
            $page = new NewsPostPage($this->path, $this->news_post);

            $vg_wort_pixel_service = new VgWortPixelService();
            $vg_wort_pixel = $vg_wort_pixel_service->getById($this->news_post->vg_wort_pixel_id);
            if($vg_wort_pixel) {
                $page->setVgWortMarke($vg_wort_pixel->vg_wort_pixel);
            }

            return $page;
        }

        $this->current_taxonomy = $this->news_manager->getTaxonomyByPermalink($this->getLastPathSegment());
        $this->is_taxonomy = $this->current_taxonomy !== null;

        if($this->is_taxonomy){
            $page = new NewsCategoryPage($this->path, $this->current_taxonomy);

            $vg_wort_pixel_service = new VgWortPixelService();
            $vg_wort_pixel = $vg_wort_pixel_service->getById($this->current_taxonomy->vg_wort_pixel_id);
            if($vg_wort_pixel) {
                $page->setVgWortMarke($vg_wort_pixel->vg_wort_pixel);
            }

            return $page;
        }

        return null;
    }
}