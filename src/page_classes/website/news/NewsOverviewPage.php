<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\news;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class NewsOverviewPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
        $this->news_manager = new NewsManager();

        $this->setTitle("News");
        $this->setDescription("Bleibe immer auf dem neuesten Stand!");
        $this->setIndex(false);
        $this->setFollow(true);
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->h1 = "Smart Home News"

        ?>
        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>"></div>
            <div class="<?=$CONFIG->main_content_column_classes?>">

                <?php

                echo $this->news_manager->displayNews($this->news_manager->getAll(Manager::STATUS_PUBLISH, true, 'news_date', true));

                ?>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>"></div>
        </div>

        <?php
    }
}