<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\news;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\scaffolding\header\BlogPostHeader;
use saschahuber\affiliatewebsitecore\scaffolding\header\NewsHeader;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\service\NewsThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\TableOfContentsService;
use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class NewsPostPage extends AffiliateWebsitePage {
    private $news_post;
    public function __construct($path, $post) {
        parent::__construct($path);
        $this->news_post = $post;

        $this->setTitle($post->meta_title);
        $this->setDescription($post->meta_description);
        $this->setIndex($post->doindex);
        $this->setFollow($post->dofollow);
        $this->setOgImage((new NewsThumbnailGeneratorService())->getThumbnailSrc($this->news_post));
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->news_manager = new NewsManager();

        $this->is_preview = false;
        $this->temp_data = null;
        $preview_token = RequestHelper::reqstr('preview');
        if($preview_token){
            $this->is_preview = true;
            $this->temp_data = (new TempDataService())->getByToken($preview_token)->temp_data;
        }

        $this->full_url = $this->news_manager->generatePermalink($this->news_post);

        if($this->is_preview){
            $this->robots = 'index, follow';
            if($this->temp_data->title){
                $this->news_post->title = $this->temp_data->title;
            }

            if($this->temp_data->content){
                $this->news_post->content = $this->temp_data->content;
            }

            if($this->temp_data->meta_title){
                $this->title = $this->temp_data->meta_title;
            }

            if($this->temp_data->meta_description){
                $this->description = $this->temp_data->meta_description;
            }
        }

        $this->toc_service = new TableOfContentsService();
        $this->toc_items = $this->toc_service->generateTableOfContents($this->news_post->content);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }


        $this->h1 = $this->news_post->title;

        $this->header = new NewsHeader($this->news_post, $this->news_manager);

        ?>

        <div class="news-container">
            <div class="row">
                <div class="<?=$CONFIG->left_content_column_classes?>">
                    <?php $this->toc_service->displayTableOfContents($this->toc_items) ?>

                    <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, NewsManager::TYPE_NEWS, $this->news_post->id))->display(); ?>
                </div>
                <div class="<?=$CONFIG->main_content_column_classes?>">
                    <div class="main-content-container">
                        <div class="text-justify-container">
                            <?=$this->news_post->content?>
                        </div>

                        <?php (new AdContainer(AdManager::AD_TYPE_CONTENT_AFTER, NewsManager::TYPE_NEWS, $this->news_post->id))->display(); ?>

                        <?php
                        if($this->news_post->attached_faq_id){
                            (new FaqComponent((new FaqService())->getQuestionsFromId($this->news_post->attached_faq_id)))->display();
                        }
                        ?>

                        <div>
                            <br>
                            <hr>
                            <?=ShortcodeHelper::doShortcode('[newsletter_news]')?>
                        </div>
                    </div>
                </div>
                <div class="<?=$CONFIG->right_content_column_classes?>">
                    <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, NewsManager::TYPE_NEWS, $this->news_post->id))->display(); ?>
                </div>
            </div>
            <?php (new AdContainer(AdManager::AD_TYPE_PAGE_BOTTOM, NewsManager::TYPE_NEWS, $this->news_post->id))->display(); ?>
        </div>

        <?php

        (new AdminEditButton('dashboard/news/bearbeiten/' . $this->news_post->id))->display();
    }
}