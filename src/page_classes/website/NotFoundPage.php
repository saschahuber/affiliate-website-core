<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class NotFoundPage extends AffiliateWebsitePage {

    public function __construct($path){
        parent::__construct($path);

        $this->setTitle("Die Seite wurde nicht gefunden");
    }

    public function buildPage(){
        http_response_code(404);

        $this->h1 = "404 - Die Seite wurde nicht gefunden";
        $this->title = "Die Seite wurde nicht gefunden";

        ?>

        <div style="margin: 10%;">
            <div class="not-found-content">
                <p class="centered">Die Seite, die du aufrufen wolltest, wurde leider nicht gefunden - vielleicht hilft
                    dir unsere Suche weiter.</p>

                [spacer size="md"]
                [search_form]
                [spacer size="md"]

            </div>
        </div>
        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>

            main > div.not-found-text {
                text-align: left;
                max-width: 640px;
                margin: 0 auto 60px;
            }

        </style>
        <?php
    }
}