<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\newsletter;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class NewsletterUnsubscribePage extends AffiliateWebsitePage{

    public function __construct($path){
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->setTitle("Newsletter-Abmeldung | {$CONFIG->app_name}");
        $this->h1 = "Newsletter-Abmeldung";

        $this->newsletter_manager = new NewsletterManager();

        $this->has_token = false;
        $this->token_valid = false;

        $token = false;
        if(isset($_GET['unsubscribe_key'])){
            $token = $_GET['unsubscribe_key'];
            $this->has_token = true;

            $this->token_valid = $this->newsletter_manager->checkUnsubscribeToken($token);
        }

        if(RequestHelper::isPost() && RequestHelper::reqstr('action') == "unsubscribe"){
            if(isset($_GET['unsubscribe_key']) && $this->newsletter_manager->unsubscribe($_GET['unsubscribe_key'])) {
                UrlHelper::redirect("/newsletter/abmeldung?error");
            }
            else {
                UrlHelper::redirect("/newsletter/abmeldung?success");
            }
        }

        ?>
        <h1>Newsletter Abmeldung</h1>

        <?php if(isset($_GET['success'])): ?>
            <p>Du hast dich erfolgreich vom Newsletter abgemeldet!</p>
        <?php else: ?>
            <?php if($this->has_token): ?>
                <?php if($this->token_valid): ?>
                    <form action="/newsletter/abmeldung?unsubscribe_key=<?=$_GET['unsubscribe_key']?>" method="post">
                        <input name="action" type="hidden" value="unsubscribe">
                        <div class="newsletter-form">
                            <p>Willst du dich wirklich vom Newsletter abmelden?</p>
                            <input class="btn btn-primary" style="text-align: center; height: 52px;"
                                   type="submit" value="Ich möchte nichtmehr über News, Deals & mehr benachrichtigt werden">
                        </div>
                    </form>
                <?php else: ?>
                    <p>Der Link auf den du geklickt hast ist ungültig.</p>
                    <p>Bitte klicke in einem Newsletter auf den Link "Abbestellen", um dich vom Newsletter abzumelden.</p>
                <?php endif; ?>
            <?php else: ?>
                <p>Bitte klicke in einem Newsletter auf den Link "Abbestellen", um dich vom Newsletter abzumelden.</p>
            <?php endif; ?>
        <?php endif;
    }
}