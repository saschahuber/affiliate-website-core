<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\newsletter;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\service\NewsletterService;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class NewsletterSettingsPage extends AffiliateWebsitePage{

    public function __construct($path){
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->setTitle("Newsletter-Einstellungen | {$CONFIG->app_name}");
        $this->h1 = "Newsletter-Einstellungen";

        $this->newsletter_manager = new NewsletterManager();

        $this->has_token = false;
        $this->token_valid = false;

        $token = false;
        if (isset($_GET['settings_key'])) {
            $token = $_GET['settings_key'];
            $this->has_token = true;

            $this->selected_topics = $this->newsletter_manager->getSubscriberSettings($token);

            if ($this->selected_topics) {
                $this->token_valid = true;
            }
        }

        if (RequestHelper::isPost() && RequestHelper::reqstr('action') == "settings") {
            $all_topics = array_keys(ALL_NEWSLETTER_TOPICS);

            if (RequestHelper::reqbool('all_topics')) {
                $subscriber_topics = $all_topics;
            } else {
                foreach ($all_topics as $topic) {
                    if (RequestHelper::reqbool($topic)) {
                        $subscriber_topics[] = $topic;
                    }
                }
            }

            $settings_key = RequestHelper::reqstr('settings_key');

            if (isset($settings_key) && $this->newsletter_manager->updateSettings($settings_key, $subscriber_topics)) {
                UrlHelper::redirect("/newsletter/einstellungen?success");
            } else {
                UrlHelper::redirect("/newsletter/einstellungen?error");
            }
        }

        if (isset($_GET['success'])): ?>
            <p>Deine Einstellungen für den Newsletter wurden erfolgreich gespeichert!</p>
        <?php else: ?>
            <?php if ($this->has_token): ?>
                <?php if ($this->token_valid): ?>
                    <form action="/newsletter/einstellungen?settings_key=<?= $settings_key ?>" method="post">
                        <input name="action" type="hidden" value="settings">
                        <div class="newsletter-form">
                            <p>Zu welchen Themen möchtest du Updates erhalten?</p>
                            <p>
                                <?php foreach (NewsletterService::ALL_NEWSLETTER_TOPICS as $topic => $title): ?>
                                    <label class="topic-selection">
                                        <input style="vertical-align: middle;" name="<?= $topic ?>" type="checkbox"
                                            <?= Helper::ifstr(in_array($topic, $this->selected_topics), "checked") ?>
                                               value="1">&nbsp;<?= $title ?>
                                    </label>
                                <?php endforeach; ?>
                            </p>
                            <input class="btn btn-primary" style="text-align: center; height: 52px;"
                                   type="submit" value="Einstellungen speichern">
                        </div>
                    </form>
                <?php else: ?>
                    <p>Der Link auf den du geklickt hast ist ungültig.</p>
                    <p>Bitte klicke in einem Newsletter auf den Link "Einstellungen" um deine Einstellungen hier ändern
                        zu können.</p>
                <?php endif; ?>
            <?php else: ?>
                <p>Bitte klicke in einem Newsletter auf den Link "Einstellungen" um deine Einstellungen hier ändern zu
                    können.</p>
            <?php endif; ?>
        <?php endif;
    }
}