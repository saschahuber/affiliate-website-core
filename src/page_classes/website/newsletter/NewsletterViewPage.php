<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\newsletter;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\service\NewsletterService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\LogHelper;

#[AllowDynamicProperties]
class NewsletterViewPage extends AffiliateWebsitePage{

    public function __construct($path){
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->setTitle("Newsletter ansehen | {$CONFIG->app_name}");

        $token = ArrayHelper::getArrayValue($_GET, 'token', null);

        $newsletter_service = new NewsletterService();

        $this->content = $newsletter_service = $newsletter_service->getPersonalNewsletterContent($token);

        if($token == null || $this->content == null){
            LogHelper::error("HTTP", 404, "Kein Newsletter für token '$token' gefunden", ERROR_EXIT);
        }

        ?>
        <h1>Dein persönlicher Newsletter!</h1>

        <?=$this->content?>
        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            h1{
                text-align: center;
            }
        </style>
        <?php
    }
}