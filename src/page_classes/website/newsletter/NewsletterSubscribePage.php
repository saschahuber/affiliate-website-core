<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\newsletter;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class NewsletterSubscribePage extends AffiliateWebsitePage{

    public function __construct($path){
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->setTitle("Newsletter-Anmeldung | {$CONFIG->app_name}");
        $this->h1 = "Newsletter-Anmeldung";

        $this->newsletter_manager = new NewsletterManager();

        $this->has_token = false;
        $this->token_valid = false;

        $token = false;
        if(isset($_GET['confirmation_key'])){
            $token = $_GET['confirmation_key'];
            $this->has_token = true;

            $this->token_valid = $this->newsletter_manager->confirmSubscriber($token);
        }

        if(isset($_GET['success'])): ?>
            <p>Du wurdest erfolgreich zum Newsletter angemeldet!</p>
            <p>Bitte klicke auf den Bestätigungslink in der Mail, die dir soeben gesendet wurde, um die Anmeldung zu bestätigen!</p>
        <?php elseif(isset($_GET['error'])): ?>
            <p>Deine Anmeldung zum Newsletter konnte leider nicht abgeschlossen werden.</p>
            <p>Bitte versuche es zu einem anderen Zeitpunkt erneut.</p>
        <?php else: ?>
            <?php if($this->has_token): ?>
                <?php if($this->token_valid): ?>
                    <p>Du wurdest erfolgreich zum Newsletter angemeldet!</p>
                <?php else: ?>
                    <p>Der Link auf den du geklickt hast ist ungültig.</p>
                    <p>Bitte klicke erneut den Bestätigungslink in der E-Mail oder melde dich hier erneut an:</p>
                    <p><?=ShortcodeHelper::doShortcode('[newsletter_form]')?></p>
                <?php endif; ?>
            <?php else: ?>
                <p><?=ShortcodeHelper::doShortcode('[newsletter_form]')?></p>
            <?php endif; ?>
        <?php endif;
    }

    public function getCssCode()
    {
        ?>
        <style>
            main {
                max-width: 1000px;
            }
        </style>
        <?php
    }
}