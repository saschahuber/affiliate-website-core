<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\LogHelper;

#[AllowDynamicProperties]
class TermsAndConditionsPage extends AffiliateWebsitePage{

    public function __construct($path){
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->setTitle("Allg. Geschäftsbedingungen | {$CONFIG->app_name}");

        $this->h1 = "Allg. Geschäftsbedingungen";
        $this->title = "Allg. Geschäftsbedingungen";

        $this->robots = "noindex, nofollow";

        $allowed_sub_pages = [
            "impressum",
            "datenschutz",
            "agb"
        ];

        if (!isset($this->path[1]) || !in_array($this->path[1], $allowed_sub_pages))
            LogHelper::error('HTTP', 404,
                'Legal-Unterseite: ' . (isset($this->path[1]) ? $this->path[1] : '') . "\t" . json_encode($allowed_sub_pages) .
                ArrayHelper::getArrayValue($_SERVER, 'HTTP_REFERER') . "\t" .
                $_SERVER['HTTP_USER_AGENT'], ERROR_EXIT);

        $subpage_name = $this->path[1];

        $this->title = [
            "impressum" => "Impressum",
            "datenschutz" => "Datenschutzerklärung",
            "agb" => "Allg. Geschäftsbedingungen"
        ][$subpage_name];

        $this->our_maillink = '<a href="mailto:' . $CONFIG->general_email_address . '">' . $CONFIG->general_email_address . '</a>';
        $this->our_homelink = '<a href="' . PROTOCOL . DOMAIN . '">' . $CONFIG->frontend_subdomain . '.' . $CONFIG->app_domain . '</a>';

        ?>
        <h1>
            <i class="fas fa-asterisk"></i>
            Allg. Gesch&auml;ftsbedingungen
        </h1>

        <?php

        if (AuthHelper::getUserManager()->isLoggedIn() && !AuthHelper::getUserManager()->logged_in_user->agb_accepted): ?>

            <form method="post" action="legal/agb">
                <p>
                    Bitte akzeptieren Sie zunächst die allgemeinen Geschäftsbedingungen.</p>
                <p>
                    <input name="confirm_agb" type="checkbox" id="cb1" data-mandatory>
                    <label for="cb1">Akzeptieren</label>
                </p>
                <p>
                    <input type="submit" value="Weiter" class="btn-color-2 cta" onclick="checkInput('confirm_agb', event);">
                </p>
            </form>

        <?php endif; ?>

        <div>
            <h3>§ 1 Vertragsgegenstand, Nutzung, Registrierung</h3>
            <ol>
                <li>Die <?= implode(', ', $CONFIG->legal_address) ?>, betreibt auf der Website <?= DOMAIN ?> eine Plattform, auf
                    der Kunden Stellanzeigen ansehen oder selbst schalten können und Bewerber ihre Profile einstellen und
                    verwalten können. Der Besuch der Jobbörse, das Einstellen und Verwalten der Bewerberprofile und die Ansicht
                    der Stellanzeigen sind kostenfrei. Für die Sichtung der Stellenanzeigen ist kein Benutzerkonto erforderlich.
                </li>
                <li>Funktionen, bei denen zur Erbringung der Leistungen durch uns eine Registrierung nicht zwingend erforderlich
                    ist, können die Kunden/Bewerber auch ohne vorherige Registrierung nutzen.
                </li>
                <li>Die Registrierung bei uns ist für Kunden und Bewerber kostenfrei.</li>
                <li>Nach der Registrierung erhält der Kunde/Bewerber eine E-Mail, in der er einen Bestätigungslink klicken muss,
                    um die Registrierung abzuschließen.
                </li>
                <li>Ein Anspruch auf Abschluss eines Nutzungsvertrages besteht nicht. Wir behalten uns vor, den Abschluss eines
                    Nutzungsvertrages ohne Angabe von Gründen abzulehnen, insbesondere wegen falscher Angaben bei der Anmeldung,
                    Zweifeln an der rechtlichen Existenz des Kunden/Bewerbers oder Verstößen gegen die Allgemeinen
                    Geschäftsbedingungen.
                </li>
                <li>Jeder Kunde/Bewerber darf sich nur einmal registrieren.</li>
                <li>Die Jobbörse stellt lediglich einen Marktplatz für Jobinserate und für Bewerberprofile zur Verfügung. Wir
                    gewährleisten keinen Vermittlungserfolg.
                </li>
                <li>Diese allgemeinen Geschäftsbedingungen gelten ausschließlich. Entgegenstehende oder von diesen
                    Geschäftsbedingungen abweichende Bedingungen der Kunden und/oder Bewerber werden nicht anerkannt, es sei
                    denn, wir haben diesen im Einzelfall ausdrücklich zugestimmt.
                </li>
            </ol>

            <h3>§ 2 Änderung </h3>
            <p>
                Wir behalten uns das Recht vor, die Allgemeinen Geschäftsbedingungen jederzeit ohne Nennung von Gründen zu
                ändern. Wir werden unserem Kunden und/oder dem Bewerber die Änderung der Allgemeinen Geschäftsbedingungen
                spätestens vier Wochen vor Inkrafttreten mitteilen und ihm diese übermitteln. Widerspricht der Kunde und/oder
                der Bewerber den geänderten Bedingungen nicht innerhalb von 2 Wochen ab Zugang der Änderungsmitteilung, dann
                gelten die geänderten Geschäftsbedingungen als angenommen. Widerspricht der Kunde und/oder der Bewerber den
                geänderten Bedingungen fristgemäß, so sind wir berechtigt, den Vertrag zu dem Zeitpunkt zu kündigen, an dem die
                geänderten Allgemeinen Geschäftsbedingungen in Kraft treten sollen oder zu den bisherigen Bedingungen
                fortzusetzen.
            </p>

            <h3>§ 3 Registrierung und Vertragsschluss für Kunden, Kosten, Verzug</h3>
            <ol>
                <li>Stellenangebote dürfen nur Unternehmer oder deren zur Stellenschaltung Bevollmächtigte aufgeben. Unternehmer
                    ist, wer das Stellenangebot ausschließlich für seine gewerblichen oder selbständig beruflichen Zwecke
                    aufgibt. Bevollmächtigter ist, wer das Stellenangebot im Namen des Unternehmers für dessen ausschließlich
                    gewerbliche oder selbständig berufliche Zwecke aufgeben darf. Der Registrierende muss bei der Registrierung
                    volljährig und voll geschäftsfähig sein.
                </li>
                <li>Der Kunde ist verpflichtet, die von uns angebotenen Funktionen nur für die vertragsgemäßen Zwecke zu
                    verwenden.
                </li>
                <li>Der Kunde ist verpflichtet, die Zugangsdaten und insbesondere sein Passwort ordnungsgemäß aufzubewahren,
                    geheim zu halten und nicht an unbefugte Dritte weiterzugeben. Der Bewerber hat auch dafür Sorge zu tragen,
                    dass sein Passwort ausreichend sicher gewählt ist.
                </li>
                <li>Für die Aufgabe einer Anzeige ist zunächst die vollständige und wahrheitsgemäße Registrierung auf der
                    Internetplattform notwendig. Anschließend kann der Kunde seine Stellanzeige erstellen und das gewünschte
                    Paket für die Veröffentlichung auswählen.
                </li>
                <li>Der Vertrag über ein Inserat kommt mit dem Betätigen des Kaufbuttons zustande.</li>
                <li>Die Preise ergeben sich aus dem jeweils gewählten Leistungspaket des Kunden1.</li>
                <li>Wir versenden innerhalb von 7 Tagen nach Veröffentlichung der Anzeige eine Rechnung, welche sofort zur
                    Zahlung fällig ist.
                </li>
                <li>Gerät der Kunde in Verzug, sind wir nach vorheriger Androhung unter Fristsetzung von 7 Tagen berechtigt, den
                    Eintrag des Kunden bis zum vollständigen Zahlungseingang zu sperren.
                </li>
            </ol>

            <h3>§ 4 Stellanzeige, Veröffentlichung</h3>
            <ol>
                <li>Die von den Kunden erstellte und an uns übermittelte Stellenanzeige wird von uns überprüft und innerhalb von
                    24 Stunden für die von dem Kunden gewählte Laufzeit veröffentlicht.2
                </li>
                <li>Wir behalten uns vor, die Stellenanzeige inhaltlich zu überprüfen und die Veröffentlichung abzulehnen. In
                    diesen Fällen werden wir den Kunden hiervon per E-Mail unterrichten und ihm die Möglichkeit geben, die
                    Stellenanzeige zu überarbeiten und neu einzusenden. Wir sind berechtigt, jedoch nicht verpflichtet, die
                    Stellenanzeige auf inhaltliche Richtigkeit und Vollständigkeit sowie auf Verstöße gegen geltendes Recht,
                    Rechte Dritter und/oder gegen diese AGB zu prüfen.
                </li>
                <li>Je nach gewähltem Leistungspaket werden wir die veröffentlichte Stellenanzeige auch auf Facebook, Instagram,
                    Twitter, Google-Ads, in weiteren Portalen unserer Wahl (je nach Vereinbarung) und in unserem Job-Ticker
                    veröffentlichen. Wir weisen darauf hin, dass wir für die von Nutzern zu dem Inserat und/oder dem Kunden
                    abgegebene Kommentare nicht verantwortlich sind und diese nach Kenntnis und/oder Aufforderung nur löschen,
                    wenn diese gegen geltendes Recht und/oder Rechte Dritter verstoßen. Für den Fall, dass ein Budget bei
                    kostenpflichtigen Ads (zum Beispiel Facebook oder Google) ohne das Verschulden von uns nicht voll verbraucht
                    wird, kann dieses nicht zurückerstattet werden.
                </li>
            </ol>

            <h3>§ 5 Registrierung Bewerber</h3>
            <ol>
                <li>Der Registrierende muss bei der Registrierung volljährig und voll geschäftsfähig sein, soweit er Kunde ist.
                    Soweit er Bewerber ist, muss er das 16. Lebensjahr vollendet haben.
                </li>
                <li>
                    Der Bewerber kann nach erfolgreicher Registrierung einen Eintrag mit seinem Bewerberprofil anlegen. Er ist
                    für seinen Eintrag und den dort bereitgehaltenen Informationen selbst verantwortlich. Er versichert, nur
                    wahrheitsgemäße Angaben zu machen. Er hat eine Änderung seiner Daten selbst in seinem Kundenkonto
                    vorzunehmen.
                    <br>Der Bewerber verpflichtet sich, seinen Eintrag nur für die vertragsgemäßen Zwecke zu verwenden.
                </li>
                <li>Der Bewerber ist verpflichtet, die von ihm in seinen Nutzerkonten hinterlegten Daten in regelmäßigen
                    Abständen auf dessen Richtigkeit zu überprüfen und ggfs. zu aktualisieren.
                </li>
                <li>Der Bewerber ist verpflichtet, die von uns angebotenen Funktionen nur für die vertragsgemäßen Zwecke zu
                    verwenden.
                </li>
                <li>Der Bewerber ist verpflichtet, die Zugangsdaten und insbesondere sein Passwort ordnungsgemäß aufzubewahren,
                    geheim zu halten und nicht an unbefugte Dritte weiterzugeben. Der Bewerber hat auch dafür Sorge zu tragen,
                    dass sein Passwort ausreichend sicher gewählt ist.
                </li>
                <li>Der Bewerber ist ebenfalls verpflichtet, seine hinterlegten Daten regelmäßig selbst zu sichern.</li>
                <li>Der Bewerber ist verpflichtet, die von ihm in seinem Profil hinterlegten Daten in regelmäßigen Abständen auf
                    dessen Richtigkeit zu überprüfen und ggfs. zu aktualisieren.
                </li>
                <li>Der Bewerber ist ebenfalls verpflichtet, seine hinterlegten Daten regelmäßig selbst zu sichern.</li>
            </ol>

            <h3>§ 6 Freistellung</h3>
            <ol>
                <li>Werden wir auf Grund eines Inhaltes einer Stellenanzeige und/oder eines Bewerberprofils von einem Kunden,
                    Bewerber oder Dritten auf Unterlassung, Beseitigung, Auskunftserteilung, Aufwendungsersatz oder
                    Schadensersatz von einem Dritten in Anspruch genommen, hat der Kunde/Bewerber uns von den Aufwendungen
                    einschließlich angemessenen Kosten der Rechtsverteidigung freizustellen. Ist streitig, ob ein Anspruch eines
                    Dritten besteht, und macht der Kunde das Nichtbestehen der Ansprüche geltend, kann der Kunde den
                    Regressanspruch dadurch vermeiden, indem er uns für die Abwehr der Ansprüche im Voraus ausreichende
                    Sicherheit für Verfahrenskosten und Schadensersatzansprüche stellt und dem Streit beitritt.
                </li>
                <li>Der Kunde/Bewerber stellt uns, unsere Angestellte, Beauftragte und Erfüllungsgehilfen von allen Ansprüchen
                    oder Forderungen Dritter - einschließlich angemessener Kosten zur Rechtsverteidigung - frei, die aus oder in
                    Zusammenhang mit diesem Vertrag und angeblicher Verstöße gegen diese Vereinbarung oder der angeblichen
                    Verletzung von Rechten Dritter entstehen. Wir behalten uns vor, die alleinige Verteidigung wahrzunehmen und
                    jeden möglichen Streitfall, der zu einem Freistellungsanspruch gegen den Kunden/Bewerber führen kann, allein
                    zu übernehmen. Die Freistellungspflichten des jeweiligen Kunden/Bewerber bleiben hiervon unberührt. Es
                    bleibt im Ermessen von uns, ob wir bei einer nicht offensichtlich unbegründeten Inanspruchnahme durch Dritte
                    Ansprüche anerkennen oder nicht. Der Kunde/Bewerber kann sich bei einer Akzeptanz eines solchen Anspruches
                    durch uns nicht darauf berufen, dass ein solcher Anspruch nicht besteht. Er kann diese Möglichkeit jedoch
                    abwenden, wenn er ausdrücklich die Haftung für das weitere Vorgehen übernimmt.
                </li>
                <li>Wir werden den Kunden/Bewerber im Rahmen des gesetzlich Zulässigen unverzüglich informieren, wenn Dritte
                    oder Behörden uns gegenüber Ansprüche geltend machen oder Anhaltspunkte dafür bekannt werden, dass ein dem
                    Kunden/Bewerber zuzurechnender Verstoß gegen gesetzliche und/oder behördliche Vorschriften bzw. eine
                    Verletzung von Rechten Dritter vorliegt.
                </li>
                <li>Der Kunde/Bewerber wird uns nach besten Kräften bei der Rechtsverteidigung unterstützen. Beruht die zur Last
                    gelegte Rechtsverletzung darauf, dass vom Kunden/Bewerber oder auf Veranlassung des Kunden/Bewerbers online
                    zugänglich gemachte Daten, Gestaltungen, Fotos und/oder sonstige Informationen Urheberrechte, Markenrechte
                    und/oder andere gewerbliche Schutzrechte Dritter verletzen, so können wir vom Kunden/Bewerber verlangen,
                    dass dieser neben den Kosten der angemessenen Rechtsverteidigung auch die Kosten für etwaige
                    Schadensersatzbeträge übernimmt.
                </li>
                <li>Wir sind zur sofortigen Entfernung bzw. Deaktivierung des Angebots und/ oder der Kleinanzeige berechtigt,
                    wenn Anhaltspunkte dafür vorliegen, dass diese Rechte Dritter verletzen könnten. Anhaltspunkte für eine
                    Rechtswidrigkeit und/oder eine Rechtsverletzung liegen insbesondere aber nicht ausschließlich dann vor, wenn
                    Behörden und/oder sonstige Dritte Maßnahmen gleich welcher Art gegen uns und/oder den Kunden/Bewerber
                    ergreifen und diese Maßnahmen auf den Vorwurf einer Rechtswidrigkeit und/oder einer Rechtsverletzung
                    stützen. Eine Erstattung von Gebühren wegen der vorzeitigen Entfernung von Angeboten und/oder Kleinanzeigen
                    aufgrund der vorstehenden Bestimmungen findet nicht statt.
                </li>
            </ol>

            <h3>§ 7 Nutzungsrechte</h3>
            <p>
                Der Kunde/Bewerber räumt uns für die Verarbeitung/Speicherung/Veröffentlichung/öffentliche Zugänglichmachung der
                Stellanzeige und/oder des Bewerberprofils für die gesamte Vertragslaufzeit alle zur vertragsgemäßen Nutzung
                erforderlichen Nutzungsrechte an den Inhalten, Bildern, Fotografien, Grafiken, Marken und Logos der
                Stellenanzeige ein. Der Kunde versichert, dass ihm an den Inhalten, Bildern, Fotografien, Grafiken, Marken und
                Logos die entsprechenden Nutzungsrechte zustehen. Der Kunde ist ebenfalls dafür verantwortlich, die eventuell
                für die Veröffentlichung notwendigen Zustimmungen Dritter einzuholen, bspw. wenn die Stellenanzeige ein Bild
                enthält, auf der Personen abgebildet sind.
            </p>

            <h3>§ 8 Gewährleistung und Haftung</h3>
            <ol>
                <li>Die Haftung von uns gegenüber Bewerbern für Sachmängel an unseren Leistungen ist unter Berücksichtigung der
                    Tatsache, dass das Portal für Bewerber kostenlos ist, auf den Fall beschränkt, dass wir gegenüber dem
                    Bewerber einen Sachmangel arglistig verschweigen. In diesem Fall haben wir gem. § 524 Abs. 1 BGB dem
                    Bewerber den daraus entstandenen Schaden zu ersetzen. Ansprüche des Bewerbers auf Mängelbeseitigung durch
                    uns bestehen nicht.
                </li>
                <li>Die Haftung von uns gegenüber Bewerbern für Rechtsmängel an unseren Leistungen ist unter Berücksichtigung
                    der Tatsache, dass das Portal für Bewerber kostenlos ist, auf den Fall beschränkt, dass wir gegenüber dem
                    Bewerber einen Rechtsmangel arglistig verschweigen. In diesem Fall haben wir gem. § 523 Abs. 1 BGB dem
                    Bewerber den daraus entstandenen Schaden zu ersetzen.
                </li>
                <li>Wir haften nicht für Ansprüche, die daraus entstehen, dass die Internetplattform vorübergehend, insbesondere
                    auf Grund von Wartungsarbeiten, für die Kunden/Bewerber nicht zur Verfügung steht, sofern der Ausfall eine
                    Gesamtzeit von mehr als 1 % eines Jahres pro Kalenderjahr nicht überschreitet und bei längeren Ausfällen
                    kein Vorsatz oder grobe Fahrlässigkeit vorliegen.
                </li>
                <li>Wir haften nicht für die Richtigkeit und/oder Vollständigkeit der auf der Internetplattform von Kunden
                    bereitgestellten Anzeigen. Insbesondere distanzieren wir uns von allen mit der Anzeige verbunden
                    Internet-Links, deren Inhalte und Urheber und übernimmt für Inhalte, Geschäfte oder Schäden, die über solche
                    Links begründet werden, keine Haftung.
                </li>
                <li>Wir haften grundsätzlich nicht für leicht fahrlässig verursachte Schäden.</li>
                <li>Die Haftungseinschränkungen nach der vorangegangenen Nummer gilt nicht für Schäden aus der Verletzung des
                    Lebens, des Körpers oder der Gesundheit, beim arglistigen Verschweigen von Mängeln, Ansprüchen aus dem
                    Produkthaftungsgesetz, im Falle des Vorsatzes und der groben Fahrlässigkeit sowie bei Verletzung von
                    Pflichten, deren Erfüllung die ordnungsgemäße Durchführung des Vertrages überhaupt erst ermöglichen und auf
                    deren Einhaltung der Kunde regelmäßig vertrauen darf.
                </li>
            </ol>

            <h3>§ 9 Sperrung, Kündigung, Vertragslaufzeit</h3>
            <ol>
                <li>Wir können das Konto/Profil eines Kunden/Bewerbers sperren, wenn dieser gegen die AGBs oder gegen geltendes
                    Recht verstößt.
                </li>
                <li>Wir können einen Kunden insbesondere sperren, wenn dieser bei der Anmeldung falsche Angaben gemacht hat,
                    gegen Rechte Dritter verstößt, Leistungen von uns missbraucht oder ein anderer wichtiger Grund vorliegt.
                </li>
                <li>Der Vertrag für Inserate läuft für die vereinbarte Zeit und endet automatisch. Die Vereinbarung über die
                    Nutzung unseres Portals läuft auf unbestimmte Zeit und kann jederzeit ohne Frist von beiden Seiten gekündigt
                    werden. Das Recht der Parteien zur außerordentlichen fristlosen Kündigung aus wichtigem Grund bleibt hiervon
                    unberührt.
                </li>
                <li>Wir sind berechtigt, unsere kostenlosen Leistungen jederzeit ohne Nennung von Gründen mit einer
                    Ankündigungsfrist von 2 Wochen einzustellen.
                </li>
            </ol>

            <h3>§ 10 Datenschutz, Datenspeicherung, Datenweiterleitung</h3>
            <ol>
                <li>Mit Anmeldung und Registrierung auf unserer Internetplattform stimmt der Kunde/Bewerber der Speicherung und
                    Verarbeitung seiner persönlichen und geschäftlichen Kundendaten, seiner veröffentlichten Angebote, des
                    systeminternen E-Mail Verkehrs zu, soweit diese im Rahmen des Geschäftszwecks von uns erhoben und zu
                    Dokumentations- und Sicherungszwecken archiviert werden müssen. Wir verweisen ergänzend auf unsere
                    Datenschutzbestimmungen.
                </li>
                <li>Dem Kunden ist bewusst, dass seine Kontaktdaten in den veröffentlichten Anzeigen für alle anderen Nutzer und
                    Besucher der Internetplattform sichtbar sind.
                </li>
            </ol>

            <h3>§ 11 Höhere Gewalt</h3>
            <p>
                Wir sind von der Leistungspflicht in Fällen höherer Gewalt (insbesondere Krieg, Streik und Naturkatastrophen)
                befreit. Als höhere Gewalt gelten auch alle unvorhergesehenen Ereignisse sowie solche Ereignisse, deren
                Auswirkungen auf die Vertragserfüllung von keiner Partei zu vertreten sind. Zu diesen Ereignissen zählen
                insbesondere rechtmäßige Arbeitskampfmaßnahmen, auch in Drittbetrieben, behördliche Maßnahmen, der Ausfall von
                Kommunikationsnetzen oder Gateways anderer Betreiber sowie Störungen im Bereich anderer Telekommunikations- oder
                Diensteanbieter. Für den Fall, dass wir trotz aller zumutbaren Anstrengungen die geschuldete Leistung aufgrund
                höherer Gewalt nicht erbringen können, sind wir für die Dauer der Hinderung von unseren Leistungspflichten
                befreit.
            </p>

            <h3>§ 12 Schlussbestimmungen</h3>
            <ol>
                <li>Sollten einzelne Bestimmungen dieses Vertrages ganz oder teilweise nicht rechtswirksam sein oder ihre
                    Rechtswirksamkeit später verlieren, so soll hierdurch die Gültigkeit des Vertrages im Übrigen nicht berührt
                    werden.
                </li>
                <li>Es wird deutsches Recht unter Ausschluss des UN-Kaufrechts vereinbart.</li>
            </ol>

            <p class="last-change-date">Stand: 29.10.2021</p>
        </div>

        <?php
    }

    public function getCssCode(){
        ?>
        <style>
            main form {
                border-radius: 6px;
                background-color: #f2f2f2;
                padding: 20px;
            }

            main form p {
                margin-bottom: 8px;
            }

            main > div > ol,
            main > div > p {
                margin-bottom: 30px;
            }
        </style>
        <?php
    }
}