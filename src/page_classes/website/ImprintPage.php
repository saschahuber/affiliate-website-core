<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\LogHelper;

#[AllowDynamicProperties]
class ImprintPage extends AffiliateWebsitePage{

    public function __construct($path){
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->setTitle("Impressum | {$CONFIG->app_name}");

        $this->h1 = "Impressum";
        $this->title = "Impressum";

        $this->robots = "noindex, nofollow";

        $allowed_sub_pages = [
            "impressum",
            "datenschutz",
            "agb"
        ];

        if (!isset($this->path[1]) || !in_array($this->path[1], $allowed_sub_pages))
            LogHelper::error('HTTP', 404,
                'Legal-Unterseite: ' . (isset($this->path[1]) ? $this->path[1] : '') . "\t" . json_encode($allowed_sub_pages) .
                ArrayHelper::getArrayValue($_SERVER, 'HTTP_REFERER') . "\t" .
                $_SERVER['HTTP_USER_AGENT'], ERROR_EXIT);

        $subpage_name = $this->path[1];

        $this->title = [
            "impressum" => "Impressum",
            "datenschutz" => "Datenschutzerklärung",
            "agb" => "Allg. Geschäftsbedingungen"
        ][$subpage_name];

        $this->our_maillink = '<a href="mailto:' . $CONFIG->general_email_address . '">' . $CONFIG->general_email_address . '</a>';
        $this->our_homelink = '<a href="' . PROTOCOL . DOMAIN . '">' . $CONFIG->frontend_subdomain . '.' . $CONFIG->app_domain . '</a>';

        ?>
        <?php

        $this->h1 = "Impressum";

        ?>

        <p>Angaben gemäß § 5 TMG</p>

        <p><?=$CONFIG->imprint_name?></p>
        <p><?=$CONFIG->imprint_street?></p>
        <p><?=$CONFIG->imprint_city?></p>

        <h2>Vertreten durch: </h2>
        <p><?=$CONFIG->imprint_name?></p>

        <h2>Kontakt:</h2>
        <p>Telefon: <?=$CONFIG->general_phone_number?></p>
        <p>E-Mail: <a href="mailto:<?=$CONFIG->general_email_address?>"><?=$CONFIG->general_email_address?></a></p>

        <h2>Umsatzsteuer-ID: </h2>
        <p>Umsatzsteuer-Identifikationsnummer gemäß §27a Umsatzsteuergesetz: <?=$CONFIG->imprint_ustid?></p>
        [imprint_copyright_info]

        <h2>Teilnahme am Amazon Partnerprogramm</h2>
        <p>Die Website <?=$CONFIG->app_name?> ist Teilnehmer des Partnerprogramms von Amazon Europe S.à r.l., ein Partnerwerbeprogramm, das für Websites konzipiert wurde, mittels dessen durch die Platzierung von Werbeanzeigen und Links zu amazon.co.uk / Javari.co.uk / amazon.de / amazon.fr / Javari.fr / amazon.it Werbekostenerstattungen verdient werden können.</p>
        <h2>Haftungsausschluss (Disclaimer)</h2>
        <h3>Haftung für Inhalte</h3>
        <p>Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.</p>

        <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>
        <h3>Haftung für Links</h3>
        <p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar.</p>

        <p>Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
        <h3>Urheberrecht</h3>
        <p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.</p>

        <p>Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
        <?php
    }
}