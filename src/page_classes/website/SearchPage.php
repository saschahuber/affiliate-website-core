<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class SearchPage extends AffiliateWebsitePage{

    public function __construct($path){
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->setTitle("Suchergebnisse | {$CONFIG->app_name}");

        $this->h1 = "Suche";
        $this->title = "Suche";

        ?>
        <h1 class="responsive-content">
            <span>Suche</span>
            <span>Suche</span>
        </h1>

        <div class="center">
            <p>Durchsuche <?=$CONFIG->app_name?> nach Beiträgen, Produkten, Kategorien und noch viel mehr!</p>
        </div>

        [search_form]

        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            main {
                padding: 0 2% 20px;
            }

            h1, h2, h3 {
                color: inherit;
            }

            div.center{
                text-align: center;
            }

            @media only screen and (max-width: 640px) {
                main > h1 {
                    margin-top: 30px;
                }
            }

            main p, main li {
                font-size: 13pt;
                line-height: unset;
            }

            main p {
                margin-bottom: 8px;
                word-spacing: 3px;
            }

            main > h1 {
                text-align: center;
                background-color: transparent;
                height: auto;
                padding: 0;
                white-space: normal;
            }

            main h2 {
                text-align: center;
                margin-top: 50px;
            }

            main > ul,
            main > div {
                margin-bottom: 40px
            }

            main > :last-child {
                margin-bottom: 0;
            }
        </style>
        <?php
    }
}