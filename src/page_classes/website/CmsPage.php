<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class CmsPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
        $this->page_manager = new PageManager();
        $this->page = $this->page_manager->getByPermalink($this->getLastPathSegment());

        if($this->page) {
            $this->setTitle($this->page->meta_title);
            $this->setDescription($this->page->meta_description);
            $this->setIndex($this->page->doindex);
            $this->setFollow($this->page->dofollow);
        }
    }

    public function isValid(){
        return $this->getPageObject() !== null;
    }

    public function getPageObject(){
        return $this->page;
    }

    public function buildPage()
    {
        global $CONFIG;

        if(count($this->path) > 0) {
            if($this->page) {
                $this->robots = ($this->page->doindex ? 'index' : 'noindex') . ', ' . ($this->page->dofollow ? 'follow' : 'nofollow');
                $this->title = $this->page->meta_title;
                $this->description = $this->page->meta_description;
            }
            else{
                return null;
            }
        }
        $this->is_page = isset($this->page) && $this->page !== false;

        $this->full_url = $this->page_manager->generatePermalink($this->page);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }

        $this->h1 = $this->page->title;
        $this->subtitle = $this->page->subtitle;

        ?>

        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>">
                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, 'page', $this->page->id))->display(); ?>
            </div>
            <div class="<?=$CONFIG->main_content_column_classes?>">
                <div class="main-content-container page-container">
                    <div class="row text-justify-container">
                        <?=$this->page->content?>
                    </div>
                </div>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>">
                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, 'page', $this->page->id))->display(); ?>
            </div>
        </div>

        <?php

        (new AdminEditButton('dashboard/seiten/bearbeiten/' . $this->page->id))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            div.post-image picture,
            div.post-image img{
                max-width: 100%;
                object-fit: contain;
                height: fit-content;
                max-height: 400px;
            }

            h1, h2, h3, h4, h5, h6, h7, h8, h9{
                margin-bottom: unset;
            }

            .main-content-container {
                text-align: justify;
            }
        </style>
        <?php
    }
}