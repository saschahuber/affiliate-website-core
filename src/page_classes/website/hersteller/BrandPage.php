<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\hersteller;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\component\container\ProductContainer;
use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\scaffolding\header\BrandHeader;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\service\TableOfContentsService;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class BrandPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->product_manager = new ProductManager();
        $this->brand_manager = new BrandManager();

        $this->brand = $this->brand_manager->getByPermalink($this->getLastPathSegment());

        $this->setTitle($this->brand->meta_title??($this->brand->title . " | {$CONFIG->app_name}"));
        $this->setDescription($this->brand->meta_description);

        $this->setIndex($this->brand->doindex);
        $this->setFollow($this->brand->dofollow);

        $this->full_url = '/hersteller/'.$this->brand->permalink;

        $this->brand_produkte = $this->product_manager->findProducts(null, null, null, [$this->brand->id]);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }

        $this->toc_service = new TableOfContentsService();
        $this->toc_items = $this->toc_service->generateTableOfContents($this->brand->content);

        $this->brand_taxonomies = $this->brand_manager->getTaxonomiesByBrand($this->brand->id);

        $this->taxonomy_buttons = array();
        foreach($this->brand_taxonomies as $taxonomy){
            $button = '[button href="/produkte/'.$taxonomy->permalink.'" shape="btn-round" size="btn-sm"]'.$taxonomy->title.'[/button]';
            $this->taxonomy_buttons[] = ShortcodeHelper::doShortcode($button);
        }

        $vg_wort_pixel_service = new VgWortPixelService();
        $vg_wort_pixel = $vg_wort_pixel_service->getById($this->brand->vg_wort_pixel_id);
        if($vg_wort_pixel) {
            $this->vg_wort_marke = $vg_wort_pixel->vg_wort_pixel;
        }

        $title_to_display = $this->brand->h1;
        if($title_to_display === null || strlen($title_to_display) < 1){
            $title_to_display = $this->brand->title;
        }

        $title_to_display = $this->brand->h1;
        if($title_to_display === null || strlen($title_to_display) < 1){
            $title_to_display = $this->brand->title;
        }

        $this->h1 = $title_to_display;
        $this->subtitle = $this->brand->subtitle;

        $this->header = new BrandHeader($this->brand, $this->brand_manager);

        $vg_wort_pixel_service = new VgWortPixelService();
        $vg_wort_pixel = $vg_wort_pixel_service->getById($this->brand->vg_wort_pixel_id);
        if($vg_wort_pixel) {
            $this->vg_wort_marke = $vg_wort_pixel->vg_wort_pixel;
        }

        ?>
        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>">
                <?php $this->toc_service->displayTableOfContents($this->toc_items) ?>

                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, BrandManager::TYPE_BRAND, $this->brand->id))->display(); ?>
            </div>
            <div class="<?=$CONFIG->main_content_column_classes?>">
                <div class="main-content-container">
                    <p><?=$this->brand->title?> hat Produkte in den folgenden Kategorien: <?=implode(" ", $this->taxonomy_buttons)?></p>

                    <div class="text-justify-container">
                        <?=$this->brand->content?>
                    </div>

                    <?php
                    if($this->brand->attached_faq_id){
                        (new FaqComponent((new FaqService())->getQuestionsFromId($this->brand->attached_faq_id)))->display();
                    }
                    ?>

                    <hr>
                </div>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>">
                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, BrandManager::TYPE_BRAND, $this->brand->id))->display(); ?>
            </div>
        </div>

        <?php
        $all_products_headline = "Alle Geräte von " . $this->brand->title;
        if($this->brand->all_products_headline){
            $all_products_headline = $this->brand->all_products_headline;
        }
        (new Text($all_products_headline, 'h2'))->display();

        $this->getProductContainer()->display();
        ?>

        <?php

        (new AdContainer(AdManager::AD_TYPE_CONTENT_AFTER, BrandManager::TYPE_BRAND, $this->brand->id))->display();

        (new AdminEditButton('dashboard/produkte/hersteller/bearbeiten/' . $this->brand->id))->display();
    }
    
    public function getProductContainer(){
        return new ProductContainer($this->brand_produkte, true, ['hersteller' => $this->brand->id]);
    }
}