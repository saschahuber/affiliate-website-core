<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\hersteller;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class BrandOverviewPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->h1 = "Hersteller in der Übersicht";

        $this->title = "Hersteller 🏅 in der Übersicht";
        $this->description = "Hersteller im Überblick!";


        ?>
        <h2>Hersteller in der Übersicht</h2>
        <br>
        <p style="text-align: center;">[hersteller anchor_link="true"]</p>

        [spacer size="lg"]

        <p style="text-align: center;">[brand_overview hide_links="true"]</p>

        [spacer size="lg"]
        <?php
    }
}