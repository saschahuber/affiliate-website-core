<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\dienstleister;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\component\CompanyGrid;
use saschahuber\affiliatewebsitecore\component\CompanyTaxonomyGrid;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\scaffolding\header\CompanyOverviewHeader;


#[AllowDynamicProperties]
class CompanyOverviewPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
        $this->company_manager = new CompanyManager();

        $this->setTitle("🏅 Dienstleister in deiner Nähe!");
        $this->setDescription("Dienstleister 🏅 | Finde jetzt Dienstleister!");
        $this->setIndex(false);
        $this->setFollow(true);
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->h1 = "Dienstleister";
        $this->header = new CompanyOverviewHeader();
        $this->robots = 'noindex, follow';
        $this->title = "🏅 Dienstleister in deiner Nähe!";
        $this->description = "Dienstleister 🏅 | Finde jetzt Dienstleister!";

        $this->featured_companies = $this->company_manager->getFeatured();

        $this->newest_companies = $this->company_manager->getNewest();

        ?>
        <div id="header-container">
            <h2 class="text-center">Dienstleister</h2>

            <p class="text-center">Hier finden Sie alle Dienstleister auf einen Blick!</p>

            <div class="company-taxonomy-container">
                <?php

                (new CompanyTaxonomyGrid($this->company_manager->getTaxonomies()))->display();
                ?>
            </div>
        </div>

        <?php if(count($this->featured_companies)): ?>
        <div id="featured-companies-container">
            <h2 class="text-center">Empfohlene Dienstleister</h2>

            <?php
            (new CompanyGrid($this->featured_companies))->display();
            ?>
        </div>
    <?php endif; ?>

        <div id="info-container">
            <h2 class="text-center">Alle Dienstleister auf einem Blick!</h2>

            <p class="text-center">Hier finden Sie eine Übersicht regionaler Dienstleister aus ganz Deutschland</p>
        </div>

        <div id="info-container-2">
            <h2 class="text-center">Alle Dienstleister auf einem Blick!</h2>

            <p class="text-center">Hier finden Sie eine Übersicht regionaler Dienstleister aus ganz Deutschland</p>
        </div>

        <?php if(count($this->newest_companies)): ?>
        <div id="featured-companies-container">
            <h2 class="text-center">Neueste Dienstleister</h2>

            <?php
            (new CompanyGrid($this->newest_companies))->display();
            ?>
        </div>
    <?php endif; ?>

        <div id="submit-listing-container">
            <h2 class="text-center">Sie möchten auch hier aufgelistet werden?</h2>
            <p class="text-center">Gerne listen wir auch Ihr Unternehmen hier auf.<p>
            <p class="text-center">Melden Sie sich einfach unter <a href="mailto:<?=$CONFIG->general_email_address?>"><?=$CONFIG->general_email_address?></a>.</p>
        </div>
        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            span.icon i{
                border-radius: 100px;
                padding: 10px;
                background-color: var(--primary_color);
                color: #fff;
                height: 35px;
                width: 35px;
                text-align: center;
            }

            #header-container {
                margin-top: 150px;
                margin-bottom: 250px;
            }

            #featured-companies-container,
            #info-container,
            #info-container-2,
            #submit-listing-container {
                margin-top: 250px;
                margin-bottom: 250px;
            }

            #company-search-form {
                margin: 0 auto;
                margin-top: 150px;
                max-width: 900px;
            }

            #featured-companies-container{
                max-width: 1200px;
                margin: 0 auto;
            }

            #info-items-grid {
                margin: 0 auto;
                margin-top: 50px;
                max-width: 1200px;
            }

            .company-taxonomy-container {
                margin: 0 auto;
                margin-top: 150px;
                max-width: 1000px;
            }

            .company-taxonomy-container .row > div {
                margin-top: 10px;
                margin-bottom: 10px;
            }
        </style>
        <?php
    }
}