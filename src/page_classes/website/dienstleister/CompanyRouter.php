<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\dienstleister;

use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\routing\SubRouter;

class CompanyRouter extends SubRouter{
    public function __construct($path){
        parent::__construct($path);
    }

    function getPageFromRoute(){
        $this->company_manager = new CompanyManager();

        if(count($this->path) > 0) {
            $this->company = $this->company_manager->getByPermalink($this->getLastPathSegment());
        }
        $this->is_company = isset($this->company) && $this->company !== false;

        $this->current_taxonomy = $this->company_manager->getTaxonomyByPermalink($this->getLastPathSegment());
        $this->is_taxonomy = $this->current_taxonomy !== null;

        if($this->is_taxonomy){
            $page = new CompanyCategoryPage($this->path, $this->current_taxonomy);

            $vg_wort_pixel_service = new VgWortPixelService();
            $vg_wort_pixel = $vg_wort_pixel_service->getById($this->current_taxonomy->vg_wort_pixel_id);
            if($vg_wort_pixel) {
                $page->setVgWortMarke($vg_wort_pixel->vg_wort_pixel);
            }

            return $page;
        }
        else if($this->is_company){
            $page = new SingleCompanyPage($this->path, $this->company);

            $vg_wort_pixel_service = new VgWortPixelService();
            $vg_wort_pixel = $vg_wort_pixel_service->getById($this->company->vg_wort_pixel_id);
            if($vg_wort_pixel) {
                $page->setVgWortMarke($vg_wort_pixel->vg_wort_pixel);
            }

            return $page;
        }

        return null;
    }
}