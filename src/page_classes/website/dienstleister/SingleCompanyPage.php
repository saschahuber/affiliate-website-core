<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\dienstleister;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\AuthorComponent;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\component\item\PostGridItem;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\scaffolding\header\BlogPostHeader;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\affiliatewebsitecore\service\BlogTaxonomyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\BlogThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\service\TableOfContentsService;
use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\RowContainer;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\affiliatewebsitecore\component\CompanySidebarComponent;

#[AllowDynamicProperties]
class SingleCompanyPage extends AffiliateWebsitePage {
    private $company;
    public function __construct($path, $company) {
        parent::__construct($path);
        $this->company = $company;

        $this->setDescription($company->meta_description);
        $this->setIndex($company->doindex);
        $this->setFollow($company->dofollow);
        $this->setOgImage((new BlogThumbnailGeneratorService())->getThumbnailSrc($this->company));
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->company_manager = new CompanyManager();

        $this->is_preview = false;
        $this->temp_data = null;
        $preview_token = RequestHelper::reqstr('preview');

        $this->setTitle($this->company->meta_title ?? ($this->company->title . " | {$CONFIG->app_name}"));
        $this->setDescription($this->company->meta_description);
        $this->setIndex($this->company->doindex);
        $this->setFollow($this->company->dofollow);

        if($preview_token){
            $this->is_preview = true;
            $this->temp_data = (new TempDataService())->getByToken($preview_token)->temp_data;
        }

        $this->full_url = $this->company_manager->generatePermalink($this->company);

        if($this->is_preview){
            $this->robots = 'index, follow';
            if($this->temp_data->title){
                $this->company->title = $this->temp_data->title;
            }

            if($this->temp_data->content){
                $this->company->content = $this->temp_data->content;
            }

            if($this->temp_data->meta_title){
                $this->title = $this->temp_data->meta_title;
            }

            if($this->temp_data->meta_description){
                $this->description = $this->temp_data->meta_description;
            }
        }

        $this->toc_service = new TableOfContentsService();
        $this->toc_items = $this->toc_service->generateTableOfContents($this->company->content);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }


        $this->h1 = $this->company->title;

        $this->header = new BlogPostHeader($this->company, $this->company_manager);

        ?>

        <div class="dienstleister-container">
            <div class="row">
                <div class="<?=$CONFIG->left_header_column_classes?>"></div>
                <div class="<?=$CONFIG->main_header_column_classes?>">
                    <!--
            <div class="company-image centered">
                <?=$this->company_manager->getThumbnail($this->company)?>
            </div>
            -->
                </div>
                <div class="<?=$CONFIG->right_header_column_classes?>"></div>

                <div class="<?=$CONFIG->left_content_column_classes?>">
                    <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, 'company', $this->company->id))->display(); ?>
                </div>
                <div class="<?=$CONFIG->main_content_column_classes?>">
                    <div class="main-content-container">
                        <div class="text-justify-container">
                            <?=$this->company_manager->prepareContent($this->company)?>
                        </div>

                        <?php
                        #if($this->company->attached_faq_id){
                        #    (new FaqComponent((new FaqService())->getQuestionsFromId($this->company->attached_faq_id)))->display();
                        #}
                        ?>

                        <?php (new AdContainer(AdManager::AD_TYPE_CONTENT_AFTER, 'company', $this->company->id))->display(); ?>
                    </div>
                </div>
                <div class="<?=$CONFIG->right_content_column_classes?>">
                    <?=$this->company_manager->getFavouriteButton($this->company->id)?>
                    <?php (new CompanySidebarComponent($this->company_manager, $this->company))->display() ?>

                    <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, 'company', $this->company->id))->display(); ?>
                </div>
            </div>
            <?php (new AdContainer(AdManager::AD_TYPE_PAGE_BOTTOM, 'company', $this->company->id))->display(); ?>
        </div>

        <?php

        (new AdminEditButton('dashboard/dienstleister/bearbeiten/' . $this->company->id))->display();
    }
}