<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\location;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\LocationSpinner;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\LocationManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\scaffolding\NearCompaniesContainer;
use saschahuber\affiliatewebsitecore\scaffolding\NearLocationsContainer;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class LocationPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        $this->location_manager = new LocationManager();
        $this->company_manager = new CompanyManager();

        $this->location = $this->location_manager->getByPermalink($this->getLastPathSegment());

        $this->full_url = $this->location_manager->generatePermalink($this->location);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }

        #require_once(__DIR__ . '/location_spinner.php');
        $location_spinner = new LocationSpinner($this->location);

        $this->near_companies = $this->company_manager->findNearCompanies($this->location->latitude, $this->location->longitude, 25);

        $this->near_locations = $this->location_manager->findNearLocations($this->location->latitude, $this->location->longitude, 25, 10, $this->location->id);

        #$this->content = $this->location->content;
        $this->content = $location_spinner->getSpinnedContent();

        $this->title = "Smarthome in {$this->location->title}";
        $this->h1 = $this->title;

        /*
        $vg_wort_pixel_service = new VgWortPixelService();
        $vg_wort_pixel = $vg_wort_pixel_service->getById($this->location->vg_wort_pixel_id);
        if($vg_wort_pixel) {
            $this->vg_wort_marke = $vg_wort_pixel->vg_wort_pixel;
        }
        */

        ?>
        <div class="location-container">
            <div class="row">
                <div class="col-md-9">
                    <div class="text-justify-container">
                        <?=ShortcodeHelper::doShortcode($this->content)?>
                    </div>

                    <br>

                    <?php
                    (new NearCompaniesContainer($this->location, $this->near_companies))->display();
                    ?>
                </div>
                <div class="col-sm-3">
                    <?php
                    (new NearLocationsContainer($this->location_manager, $this->location, $this->near_locations))->display();
                    ?>
                </div>
            </div>
        </div>
        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            p {
                text-align: justify;
                hyphens: auto;
            }
        </style>
        <?php
    }
}