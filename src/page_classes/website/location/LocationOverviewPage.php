<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\location;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\LocationManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class LocationOverviewPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
    }

    public function buildPage(){
        global $CONFIG;

        if(LIVE_ENV){
            UrlHelper::redirect("/dienstleister", 302);
        }

        $this->location_manager = new LocationManager();
        $location_manager = $this->location_manager;

        $this->h1 = $this->getTitle();

        $this->bundeslaender = $this->location_manager->getStates();
        $bundeslaender_contents = [];
        foreach($this->bundeslaender as $bundesland){
            $bundeslaender_contents[] = BufferHelper::buffered(function () use ($location_manager, $bundesland){
                ?>
                <a class="location-item-container" href="<?=$location_manager->generatePermalink($bundesland)?>">
                    <div class="card text-center location-item">
                        <?=$bundesland->title?>
                    </div>
                </a>
                <?php
            });
        }
        $this->bundeslaender_container = new GridContainer($bundeslaender_contents);

        $this->featured_locations = $this->location_manager->getFeatured();
        $featured_locations_contents = [];
        foreach($this->featured_locations as $location){
            $featured_locations_contents[] = BufferHelper::buffered(function () use ($location_manager, $location){
                ?>
                <a class="location-item-container" href="<?=$location_manager->generatePermalink($location)?>">
                    <div class="card text-center location-item">
                        <?=$location->title?>
                    </div>
                </a>
                <?php
            });
        }
        $this->featured_locations_container = new GridContainer($featured_locations_contents);

        ?>
        <div id="header-container">
            <div id="states-container">
                <?php
                $this->bundeslaender_container->display();
                ?>
            </div>
        </div>

        <div>
            <h2>Großstädte in Deutschland</h2>

            <div id="featured-locations-container">
                <?php
                $this->featured_locations_container->display();
                ?>
            </div>
        </div>


        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            #header-container {
                margin-top: 150px;
                margin-bottom: 150px;
            }

            #states-container,
            #featured-locations-container {
                max-width: 1200px;
                margin: 0 auto;
                margin-top: 100px;
            }

            h1, h2, h3 {
                text-align: center;
            }
        </style>
        <?php
    }

    public function getTitle(){
        return "Dienstleister in deiner Nähe";
    }
}