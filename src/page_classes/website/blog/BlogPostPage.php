<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\blog;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\AuthorComponent;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\scaffolding\header\BlogPostHeader;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\affiliatewebsitecore\service\BlogThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\service\TableOfContentsService;
use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class BlogPostPage extends AffiliateWebsitePage {
    private $post;
    public function __construct($path, $post) {
        parent::__construct($path);
        $this->post = $post;

        $this->setDescription($post->meta_description);
        $this->setIndex($post->doindex);
        $this->setFollow($post->dofollow);
        $this->setOgImage((new BlogThumbnailGeneratorService())->getThumbnailSrc($this->post));
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->post_manager = new PostManager();

        $this->is_preview = false;
        $this->temp_data = null;
        $preview_token = RequestHelper::reqstr('preview');

        $this->setTitle($this->post->meta_title ?? ($this->post->title . " | {$CONFIG->app_name}"));
        $this->setDescription($this->post->meta_description);
        $this->setIndex($this->post->doindex);
        $this->setFollow($this->post->dofollow);

        if($preview_token){
            $this->is_preview = true;
            $this->temp_data = (new TempDataService())->getByToken($preview_token)->temp_data;
        }

        $this->full_url = $this->post_manager->generatePermalink($this->post);

        if($this->is_preview){
            $this->robots = 'index, follow';
            if($this->temp_data->title){
                $this->post->title = $this->temp_data->title;
            }

            if($this->temp_data->content){
                $this->post->content = $this->temp_data->content;
            }

            if($this->temp_data->meta_title){
                $this->title = $this->temp_data->meta_title;
            }

            if($this->temp_data->meta_description){
                $this->description = $this->temp_data->meta_description;
            }
        }

        $vg_wort_pixel_service = new VgWortPixelService();
        $vg_wort_pixel = $vg_wort_pixel_service->getById($this->post->vg_wort_pixel_id);
        if($vg_wort_pixel) {
            $this->vg_wort_marke = $vg_wort_pixel->vg_wort_pixel;
        }

        $this->toc_service = new TableOfContentsService();
        $this->toc_items = $this->toc_service->generateTableOfContents($this->post->content);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }

        $this->h1 = $this->post->title;

        $this->header = new BlogPostHeader($this->post, $this->post_manager);

        ?>

        <div class="blog-container">
            <div class="row">
                <div class="<?=$CONFIG->left_content_column_classes?>">
                    <?php $this->toc_service->displayTableOfContents($this->toc_items)?>

                    <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, PostManager::TYPE_POST, $this->post->id))->display(); ?>
                </div>
                <div class="<?=$CONFIG->main_content_column_classes?>">
                    <div class="main-content-container">
                        <div class="text-justify-container">
                            <?=$this->post->content?>
                        </div>

                        <?php (new AdContainer(AdManager::AD_TYPE_CONTENT_AFTER, PostManager::TYPE_POST, $this->post->id))->display(); ?>

                        <?php
                        if($this->post->attached_faq_id){
                            (new FaqComponent((new FaqService())->getQuestionsFromId($this->post->attached_faq_id)))->display();
                        }
                        ?>

                        <?php
                        if($this->post->author_id){
                            (new AuthorComponent(((new AuthorService())->getById($this->post->author_id))))->display();
                        }
                        ?>

                        <div>
                            <br>
                            <hr>
                            <?=ShortcodeHelper::doShortcode('[newsletter_blogpost]')?>
                        </div>
                    </div>
                </div>
                <div class="<?=$CONFIG->right_content_column_classes?>">
                    <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, PostManager::TYPE_POST, $this->post->id))->display(); ?>
                </div>
            </div>

            <?php (new AdContainer(AdManager::AD_TYPE_PAGE_BOTTOM, PostManager::TYPE_POST, $this->post->id))->display(); ?>
        </div>

        <?php

        (new AdminEditButton('dashboard/blog/bearbeiten/' . $this->post->id))->display();
    }
}