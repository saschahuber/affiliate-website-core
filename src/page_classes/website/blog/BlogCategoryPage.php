<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\blog;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\component\item\PostGridItem;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\service\BlogTaxonomyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\service\TableOfContentsService;
use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\RowContainer;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class BlogCategoryPage extends AffiliateWebsitePage {
    private $taxonomy;

    public function __construct($path, $taxonomy) {
        parent::__construct($path);
        $this->taxonomy = $taxonomy;
        $this->setOgImage((new BlogTaxonomyThumbnailGeneratorService())->getThumbnailSrc($this->taxonomy));
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->post_manager = new PostManager();

        $this->is_preview = false;
        $this->temp_data = null;
        $preview_token = RequestHelper::reqstr('preview');

        $this->setTitle($this->taxonomy->meta_title ?? ($this->taxonomy->title . " | {$CONFIG->app_name}"));
        $this->setDescription($this->taxonomy->meta_description);

        $this->setIndex($this->taxonomy->doindex);
        $this->setFollow($this->taxonomy->dofollow);

        if($preview_token){
            $this->is_preview = true;
            $this->temp_data = (new TempDataService())->getByToken($preview_token)->temp_data;
        }

        $title_to_display = $this->taxonomy->h1;
        if($title_to_display === null || strlen($title_to_display) < 1){
            $title_to_display = $this->taxonomy->title;
        }

        $this->h1 = $title_to_display;
        $this->subtitle = $this->taxonomy->subtitle;

        $ad_manager = new AdManager();

        $this->full_url = $this->post_manager->getTaxonomyPermalink($this->taxonomy);

        $this->toc_service = new TableOfContentsService();
        $this->toc_items = $this->toc_service->generateTableOfContents($this->taxonomy->content);

        if ($this->full_url !== ('/' . implode('/', $this->path))) {
            UrlHelper::redirect($this->full_url, 301);
        }

        if($this->is_preview){
            $this->robots = 'index, follow';
            if($this->temp_data->title){
                $this->taxonomy->title = $this->temp_data->title;
            }

            if($this->temp_data->content){
                $this->taxonomy->content = $this->temp_data->content;
            }

            if($this->temp_data->meta_title){
                $this->title = $this->temp_data->meta_title;
            }

            if($this->temp_data->meta_description){
                $this->description = $this->temp_data->meta_description;
            }
        }

        $vg_wort_pixel_service = new VgWortPixelService();
        $vg_wort_pixel = $vg_wort_pixel_service->getById($this->taxonomy->vg_wort_pixel_id);
        if($vg_wort_pixel) {
            $this->vg_wort_marke = $vg_wort_pixel->vg_wort_pixel;
        }

        $this->taxonomy_posts = $this->post_manager->getByTaxonomyId($this->taxonomy->id);

        $post_items = [];
        foreach($this->taxonomy_posts as $post){
            $post_items[] = new PostGridItem($post);
        }

        $columns = [];
        foreach($post_items as $content){
            $columns[] = new Column($content, ['col-md-6', 'col-lg-4', 'col-12']);
        }

        $columns = $ad_manager->mixAdsInColumns($columns, AdManager::AD_TYPE_CONTENT_BETWEEN, PostManager::TYPE_POST_CATEGORY, [$this->taxonomy], null, 12, true);

        $this->rows_to_display = [new Row([$columns])];

        ob_start();

        ?>
        
        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>">
        
                <?php $this->toc_service->displayTableOfContents($this->toc_items) ?>
        
                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, PostManager::TYPE_POST_CATEGORY, $this->taxonomy->id))->display(); ?>
            </div>
            <div class="<?=$CONFIG->main_content_column_classes?>">
                <div class="main-content-container">
                    <div class="text-justify-container">
                        <?=$this->taxonomy->content?>
                    </div>
        
                    <hr>
        
                    <?php
        
                    (new RowContainer($this->rows_to_display, ['post-grid']))->display();
        
                    $main_content = ob_get_clean();
        
                    echo $main_content;
        
                    if ($this->taxonomy->attached_faq_id) {
                        (new FaqComponent((new FaqService())->getQuestionsFromId($this->taxonomy->attached_faq_id)))->display();
                    }
        
                    ?>
                </div>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>">
                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, PostManager::TYPE_POST_CATEGORY, $this->taxonomy->id))->display(); ?>
            </div>
        </div>
        
        <?php
        
        (new AdminEditButton('dashboard/blog/kategorien/bearbeiten/' . $this->taxonomy->id))->display();
        
        #$left_sidebar_column = new Column([], ['col-lg-3']);
        #$main_content_column = new Column([$main_content], ['col-lg-6']);
        #$right_sidebar_column = new Column([], ['col-lg-3']);
        #(new Row([$left_sidebar_column, $main_content_column, $right_sidebar_column]))->display();
    }
}