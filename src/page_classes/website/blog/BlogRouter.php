<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\blog;

use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\routing\SubRouter;

class BlogRouter extends SubRouter{
    public function __construct($path){
        parent::__construct($path);
    }

    function getPageFromRoute(){
        $this->post_manager = new PostManager();

        if(count($this->path) > 0) {
            $this->post = $this->post_manager->getByPermalink($this->getLastPathSegment());
        }
        $this->is_post = isset($this->post) && $this->post !== false;

        $this->current_taxonomy = $this->post_manager->getTaxonomyByPermalink($this->getLastPathSegment());
        $this->is_taxonomy = $this->current_taxonomy !== null;

        if($this->is_taxonomy){
            $page = new BlogCategoryPage($this->path, $this->current_taxonomy);

            $vg_wort_pixel_service = new VgWortPixelService();
            $vg_wort_pixel = $vg_wort_pixel_service->getById($this->current_taxonomy->vg_wort_pixel_id);
            if($vg_wort_pixel) {
                $page->setVgWortMarke($vg_wort_pixel->vg_wort_pixel);
            }

            return $page;
        }
        else if($this->is_post){
            $page = new BlogPostPage($this->path, $this->post);

            $vg_wort_pixel_service = new VgWortPixelService();
            $vg_wort_pixel = $vg_wort_pixel_service->getById($this->post->vg_wort_pixel_id);
            if($vg_wort_pixel) {
                $page->setVgWortMarke($vg_wort_pixel->vg_wort_pixel);
            }

            return $page;
        }

        return null;
    }
}