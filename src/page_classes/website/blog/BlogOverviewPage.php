<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\blog;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class BlogOverviewPage extends AffiliateWebsitePage {
    public function __construct($path) {
        parent::__construct($path);
        $this->post_manager = new PostManager();

        $this->setTitle("Blog - Tutorials, Kurztipps und mehr!");
        $this->setDescription("Im Blog findest du Tutorials, Kurztipps und viel mehr rund ums Thema Smarthome!");
        $this->setIndex(false);
        $this->setFollow(true);
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->h1 = "Blog"

        ?>
        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>"></div>
            <div class="<?=$CONFIG->main_content_column_classes?>">

                <?php

                echo $this->post_manager->displayPosts($this->post_manager->getAll(Manager::STATUS_PUBLISH, true, 'post_date', true));

                ?>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>"></div>
        </div>

        <?php
    }
}