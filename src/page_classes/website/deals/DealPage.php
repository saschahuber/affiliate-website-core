<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\deals;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\service\DealPageGeneratorService;
use saschahuber\affiliatewebsitecore\service\ImageTemplateService;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\service\ImageService;

#[AllowDynamicProperties]
class DealPage extends AffiliateWebsitePage {
    public function buildPage()
    {
        global $CONFIG, $DB;

        $this->is_deals_page = count($this->path) === 1 && $this->path[0] == "deals";

        $this->product_manager = new ProductManager();

        $this->deal_page = false;
        if(isset($this->path[1])){
            $alias = $this->path[1];
            $this->deal_page = $DB->query('SELECT *, (deal_page_expiry_date<CURRENT_TIMESTAMP()) as is_expired FROM deal_page 
                where permalink = "'.$DB->escape($alias).'"
                and deal_page_publish_date <= CURRENT_TIMESTAMP()')->fetchObject();
        }

        if(count($this->path) > 2) {
            UrlHelper::redirect($this->path[0].'/'.$this->path[1]);
        }

        if($this->deal_page->status !== Manager::STATUS_PUBLISH || $this->deal_page->is_expired){
            LogHelper::logToMinuteLog("HTTP", "[".implode('/', $this->path)."] Deal-Seite '{$this->deal_page->title}' ist nicht öffentlich oder abgelaufen", LogHelper::LOG_LEVEL_WARNING);
            UrlHelper::redirect("deals");
        }

        $this->robots = ($this->deal_page->doindex?'index':'noindex').', '.($this->deal_page->dofollow?'follow':'nofollow');
        $this->title = $this->deal_page->title . " | " . $this->title;
        #$this->description = $this->post_manager->getMetaDescription($this->post);
        #$this->og_image = $this->post_manager->getThumbnailSrc($this->post);

        $this->image_service = new ImageService();

        $this->deal_data = json_decode($this->deal_page->content_data);

        $this->deal_page_generator_service = new DealPageGeneratorService();

        $this->deal_page_thumbnail = $this->image_service->getDynamicImage(ImageTemplateService::DEAL_THUMBNAIL, $this->deal_page->title);
        $this->og_image = $this->deal_page_thumbnail;

        $all_taxonomies = $this->product_manager->getTaxonomies();

        $this->top_deals = $this->deal_data->top_deals;
        $this->category_deals = $this->deal_data->category_deals;

        $this->deal_taxonomies = [];
        foreach($all_taxonomies as $taxonomy){
            if(isset($this->category_deals->{$taxonomy->id})
                && count($this->category_deals->{$taxonomy->id})){
                $taxonomy->deals = $this->category_deals->{$taxonomy->id};
                $this->deal_taxonomies[$taxonomy->id] = $taxonomy;
            }
        }

        $vg_wort_pixel_service = new VgWortPixelService();
        $vg_wort_pixel = $vg_wort_pixel_service->getById($this->deal_page->vg_wort_pixel_id);
        if($vg_wort_pixel) {
            $this->vg_wort_marke = $vg_wort_pixel->vg_wort_pixel;
        }

        ?>

        <div class="deal-container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="deal-image">
                        <?=ImgUtils::getImageTag($this->deal_page_thumbnail)?>
                    </div>

                    <h1><?=$this->deal_page->title?></h1>

                    <div>
                        <p>Inhaltsverzeichnis</p>
                        <ul>
                            <?php if(count($this->top_deals)): ?>
                                <li><a href="#top-deals">TOP Deals</a></li>
                            <?php endif; ?>
                            <?php if(count($this->deal_taxonomies)): ?>
                                <li><a href="#kategorie-deals">Deals nach Kategorien</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="content-intro"></div>
                    <div class="content-deals">
                        <?php if(count($this->top_deals)): ?>
                            <div class="top-deals" id="top-deals">
                                <h2>Die <?=count($this->top_deals)?> besten Deals!</h2>
                                <?php for($index = 0; $index < count($this->top_deals); $index++):
                                    $top_deal = $this->top_deals[$index];
                                    ?>
                                    <div>
                                        <h3>TOP Deal #<?=($index+1)?>: <?=$top_deal->title?></h3>
                                        <?=$this->deal_page_generator_service->displayDeal($top_deal)?>
                                    </div>
                                <?php endfor; ?>
                            </div>
                        <?php endif; ?>
                        <?php if(count($this->deal_taxonomies)): ?>
                            <div class="top-deals" id="kategorie-deals">
                                <h2>Die besten Deals nach Kategorie</h2>
                                <ul>
                                    <?php foreach($this->deal_taxonomies as $deal_taxonomy): ?>
                                        <li><a href="#kategorie-<?=$deal_taxonomy->permalink?>"><?=$deal_taxonomy->title?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                                <?php foreach($this->deal_taxonomies as $deal_taxonomy): ?>
                                    <div id="kategorie-<?=$deal_taxonomy->permalink?>">
                                        <h3>Deals in der Kategorie <?=$deal_taxonomy->title?></h3>
                                        <?php foreach($deal_taxonomy->deals as $deal): ?>
                                            <h4><?=$deal->title?></h4>
                                            <?=$this->deal_page_generator_service->displayDeal($deal)?>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="content-summary"></div>

                    <div>
                        <br>
                        <hr>
                        <?=ShortcodeHelper::doShortcode('[newsletter_deals]')?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            div.deal picture,
            div.deal-image img{
                max-width: 100%;
                object-fit: contain;
                height: fit-content;
                max-height: 400px;
            }

            h1, h2, h3, h4, h5, h6, h7, h8, h9{
                margin-bottom: unset;
            }
        </style>
        <?php
    }
}