<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\deals;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\DealPageManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class DealOverviewPage extends AffiliateWebsitePage {
    public function buildPage()
    {
        $this->robots = 'noindex, follow';
        $this->h1 = "Archiv der letzten Top-Deals";
        $this->title = "Archiv der letzten Top-Deals";
        $this->description = "Hier findest du die letzten TOP-Deal-Sammlungen!";

        $this->deal_page_manager = new DealPageManager();

        ?>

        <h1>Archiv der letzten Deals</h1>

        <hr>

        <?php

        echo $this->deal_page_manager->displayPosts($this->deal_page_manager->getAll(Manager::STATUS_PUBLISH, true, 'deal_page_publish_date', true));
    }
}