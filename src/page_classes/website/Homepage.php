<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;


#[AllowDynamicProperties]
class Homepage extends AffiliateWebsitePage {

    public function __construct($path){
        parent::__construct($path);
        $this->news_manager = new NewsManager();
        $this->post_manager = new PostManager();
        $this->product_manager = (AffiliateInterfacesHelper::getProductManager());

        $this->setIndex(true);
        $this->setFollow(true);
        $this->setTitle("Affiliate Website");
        $this->setDescription("Das ist eine Affiliate-Website");
    }

    public function buildPage()
    {
        ?>
        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            main {
                padding: 0;
                max-width: 100%;
                width: 100%;
                margin-top: 0;
            }

            main > div:not(.full-width-container) {
                width: 80%;
                max-width: var(--max_page_container_width);
                margin: auto;
            }

            @media only screen and (max-width: 640px){
                main > div:not(.full-width-container) {
                    width: 95%;
                    margin: auto;
                }
            }

            h1{
                text-align: center;
            }

            div.not-found-content{
                text-align: center;
            }
        </style>

        <style>
            h1 {
                font-size: 65px;
                line-height: 65px;
                margin-top: 25px;
                margin-bottom: 15px;
            }

            h2 {
                font-size: 53px;
                line-height: 53px;
                margin-bottom: 10px;
            }

            h3 {
                font-size: 45px;
                line-height: 45px;
                margin-top: 20px;
                margin-bottom: 10px;
            }

            h4 {
                font-size: 40px;
                line-height: 40px;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            h5 {
                font-size: 35px;
                line-height: 35px;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            @media (max-width: 767px){
                #header-content-container h1 {
                    font-size: 35px;
                    line-height: 35px;
                }

                h1 {
                    font-size: 35px;
                    line-height: 35px;
                    margin-top: 25px;
                    margin-bottom: 15px;
                }

                h2 {
                    font-size: 30px;
                    line-height: 30px;
                    margin-bottom: 10px;
                }

                h3 {
                    font-size: 28px;
                    line-height: 28px;
                    margin-top: 20px;
                    margin-bottom: 10px;
                }

                h4 {
                    font-size: 25px;
                    line-height: 25px;
                    margin-top: 10px;
                    margin-bottom: 10px;
                }

                h5 {
                    font-size: 22px;
                    line-height: 22px;
                    margin-top: 10px;
                    margin-bottom: 10px;
                }
            }

            p.headline-label {
                margin-bottom: 0;
                text-transform: uppercase;
            }

            p.blue-headline-label {
                margin-bottom: 0;
                color: #0099cc;
                text-transform: uppercase;
            }

            #ratgeber, #news {
                padding: 75px 0;
            }

            .search-bar-col {
                max-width: 720px;
                margin: auto;
            }

            @media (min-width: 1200px){
                .search-bar-col {
                    float: right;
                }
            }

            #header-content .row {
                max-width: 100%;
            }
        </style>
        <?php
    }
}