<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\LogHelper;

#[AllowDynamicProperties]
class PrivacyPage extends AffiliateWebsitePage
{

    public function __construct($path)
    {
        parent::__construct($path);

        $this->setTitle("Die Seite wurde nicht gefunden");
    }

    public function buildPage()
    {
        global $CONFIG;

        $this->h1 = "Datenschutzerklärung";
        $this->title = "Datenschutzerklärung";

        $this->robots = "noindex, nofollow";

        $allowed_sub_pages = [
            "impressum",
            "datenschutz",
            "agb"
        ];

        if (!isset($this->path[1]) || !in_array($this->path[1], $allowed_sub_pages))
            LogHelper::error('HTTP', 404,
                'Legal-Unterseite: ' . (isset($this->path[1]) ? $this->path[1] : '') . "\t" . json_encode($allowed_sub_pages) .
                ArrayHelper::getArrayValue($_SERVER, 'HTTP_REFERER') . "\t" .
                $_SERVER['HTTP_USER_AGENT'], ERROR_EXIT);

        $subpage_name = $this->path[1];

        $this->title = [
            "impressum" => "Impressum",
            "datenschutz" => "Datenschutzerklärung",
            "agb" => "Allg. Geschäftsbedingungen"
        ][$subpage_name];

        $this->our_maillink = '<a href="mailto:' . $CONFIG->general_email_address . '">' . $CONFIG->general_email_address . '</a>';
        $this->our_homelink = '<a href="' . PROTOCOL . DOMAIN . '">' . $CONFIG->frontend_subdomain . '.' . $CONFIG->app_domain . '</a>';

        ?>
        <?php

        $this->h1 = "Datenschutzerklärung";

        ?>
        <h2>Einleitung</h2><p>Mit der folgenden Datenschutzerklärung möchten wir Sie darüber
        aufklären, welche Arten Ihrer personenbezogenen Daten (nachfolgend auch kurz als "Daten“ bezeichnet) wir zu
        welchen
        Zwecken und in welchem Umfang verarbeiten. Die Datenschutzerklärung gilt für alle von uns durchgeführten
        Verarbeitungen personenbezogener Daten, sowohl im Rahmen der Erbringung unserer Leistungen als auch insbesondere
        auf
        unseren Webseiten, in mobilen Applikationen sowie innerhalb externer Onlinepräsenzen, wie z.B. unserer
        Social-Media-Profile (nachfolgend zusammenfassend bezeichnet als "Onlineangebot“).</p><p>Die verwendeten
        Begriffe
        sind nicht geschlechtsspezifisch.</p><p>Stand: 19. Oktober 2021</p><h2>Inhaltsübersicht</h2>
        <ul class="index">
            <li><a class="index-link" href="#m14">Einleitung</a></li>
            <li><a class="index-link" href="#m3">Verantwortlicher</a></li>
            <li><a class="index-link" href="#mOverview">Übersicht der Verarbeitungen</a></li>
            <li><a class="index-link" href="#m13">Maßgebliche Rechtsgrundlagen</a></li>
            <li><a class="index-link" href="#m27">Sicherheitsmaßnahmen</a></li>
            <li><a class="index-link" href="#m25">Übermittlung von personenbezogenen Daten</a></li>
            <li><a class="index-link" href="#m24">Datenverarbeitung in Drittländern</a></li>
            <li><a class="index-link" href="#m12">Löschung von Daten</a></li>
            <li><a class="index-link" href="#m134">Einsatz von Cookies</a></li>
            <li><a class="index-link" href="#m317">Geschäftliche Leistungen</a></li>
            <li><a class="index-link" href="#m326">Zahlungsverfahren</a></li>
            <li><a class="index-link" href="#m225">Bereitstellung des Onlineangebotes und Webhosting</a></li>
            <li><a class="index-link" href="#m367">Registrierung, Anmeldung und Nutzerkonto</a></li>
            <li><a class="index-link" href="#m104">Blogs und Publikationsmedien</a></li>
            <li><a class="index-link" href="#m182">Kontakt- und Anfragenverwaltung</a></li>
            <li><a class="index-link" href="#m406">Chatbots und Chatfunktionen</a></li>
            <li><a class="index-link" href="#m17">Newsletter und elektronische Benachrichtigungen</a></li>
            <li><a class="index-link" href="#m638">Werbliche Kommunikation via E-Mail, Post, Fax oder Telefon</a></li>
            <li><a class="index-link" href="#m408">Umfragen und Befragungen</a></li>
            <li><a class="index-link" href="#m263">Webanalyse, Monitoring und Optimierung</a></li>
            <li><a class="index-link" href="#m264">Onlinemarketing</a></li>
            <li><a class="index-link" href="#m135">Affiliate-Programme und Affiliate-Links</a></li>
            <li><a class="index-link" href="#m136">Präsenzen in sozialen Netzwerken (Social Media)</a></li>
            <li><a class="index-link" href="#m328">Plugins und eingebettete Funktionen sowie Inhalte</a></li>
            <li><a class="index-link" href="#m15">Änderung und Aktualisierung der Datenschutzerklärung</a></li>
            <li><a class="index-link" href="#m10">Rechte der betroffenen Personen</a></li>
            <li><a class="index-link" href="#m42">Begriffsdefinitionen</a></li>
        </ul><h2 id="m3">Verantwortlicher</h2>
        <p><?= $CONFIG->imprint_name ?>
            <br>
            <?= $CONFIG->imprint_street ?>
            <br>
            <?= $CONFIG->imprint_city ?>
        </p>
        <p>E-Mail-Adresse:
        <?= $CONFIG->general_email_address ?>.</p>
        <p>Impressum: <a rel="noopener noreferrer nofollow"
                                                                   href="/legal/impressum"
                                                                   target="_blank"><?= $CONFIG->app_domain ?>/legal/impressum</a>.
        </p>
        <h2 id="mOverview">Übersicht der Verarbeitungen</h2><p>Die nachfolgende Übersicht fasst die Arten der
        verarbeiteten
        Daten und die Zwecke ihrer Verarbeitung zusammen und verweist auf die betroffenen Personen.</p><h3>Arten der
        verarbeiteten Daten</h3>
        <ul>
            <li>Bestandsdaten (z.B. Namen, Adressen).</li>
            <li>Inhaltsdaten (z.B. Eingaben in Onlineformularen).</li>
            <li>Kontaktdaten (z.B. E-Mail, Telefonnummern).</li>
            <li>Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).</li>
            <li>Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten).</li>
            <li>Vertragsdaten (z.B. Vertragsgegenstand, Laufzeit, Kundenkategorie).</li>
            <li>Zahlungsdaten (z.B. Bankverbindungen, Rechnungen, Zahlungshistorie).</li>
        </ul><h3>Kategorien betroffener Personen</h3>
        <ul>
            <li>Geschäfts- und Vertragspartner.</li>
            <li>Interessenten.</li>
            <li>Kommunikationspartner.</li>
            <li>Kunden.</li>
            <li>Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
        </ul><h3>Zwecke der Verarbeitung</h3>
        <ul>
            <li>Affiliate-Nachverfolgung.</li>
            <li>Bereitstellung unseres Onlineangebotes und Nutzerfreundlichkeit.</li>
            <li>Konversionsmessung (Messung der Effektivität von Marketingmaßnahmen).</li>
            <li>Büro- und Organisationsverfahren.</li>
            <li>Content Delivery Network (CDN).</li>
            <li>Direktmarketing (z.B. per E-Mail oder postalisch).</li>
            <li>Feedback (z.B. Sammeln von Feedback via Online-Formular).</li>
            <li>Marketing.</li>
            <li>Kontaktanfragen und Kommunikation.</li>
            <li>Profile mit nutzerbezogenen Informationen (Erstellen von Nutzerprofilen).</li>
            <li>Reichweitenmessung (z.B. Zugriffsstatistiken, Erkennung wiederkehrender Besucher).</li>
            <li>Sicherheitsmaßnahmen.</li>
            <li>Erbringung vertraglicher Leistungen und Kundenservice.</li>
            <li>Verwaltung und Beantwortung von Anfragen.</li>
        </ul><h3 id="m13">Maßgebliche Rechtsgrundlagen</h3><p>Im Folgenden erhalten Sie eine Übersicht der
        Rechtsgrundlagen der
        DSGVO, auf deren Basis wir personenbezogene Daten verarbeiten. Bitte nehmen Sie zur Kenntnis, dass neben den
        Regelungen der DSGVO nationale Datenschutzvorgaben in Ihrem bzw. unserem Wohn- oder Sitzland gelten können.
        Sollten
        ferner im Einzelfall speziellere Rechtsgrundlagen maßgeblich sein, teilen wir Ihnen diese in der
        Datenschutzerklärung mit.</p>
        <ul>
            <li><strong>Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO)</strong> - Die betroffene Person hat ihre
                Einwilligung
                in die Verarbeitung der sie betreffenden personenbezogenen Daten für einen spezifischen Zweck oder
                mehrere
                bestimmte Zwecke gegeben.
            </li>
            <li><strong>Vertragserfüllung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO)</strong> - Die
                Verarbeitung ist für die Erfüllung eines Vertrags, dessen Vertragspartei die betroffene Person ist, oder
                zur
                Durchführung vorvertraglicher Maßnahmen erforderlich, die auf Anfrage der betroffenen Person erfolgen.
            </li>
            <li><strong>Rechtliche Verpflichtung (Art. 6 Abs. 1 S. 1 lit. c. DSGVO)</strong> - Die Verarbeitung ist zur
                Erfüllung einer rechtlichen Verpflichtung erforderlich, der der Verantwortliche unterliegt.
            </li>
            <li><strong>Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO)</strong> - Die Verarbeitung ist zur
                Wahrung
                der berechtigten Interessen des Verantwortlichen oder eines Dritten erforderlich, sofern nicht die
                Interessen
                oder Grundrechte und Grundfreiheiten der betroffenen Person, die den Schutz personenbezogener Daten
                erfordern,
                überwiegen.
            </li>
        </ul><p><strong>Nationale Datenschutzregelungen in Deutschland</strong>: Zusätzlich zu den Datenschutzregelungen
        der
        Datenschutz-Grundverordnung gelten nationale Regelungen zum Datenschutz in Deutschland. Hierzu gehört
        insbesondere
        das Gesetz zum Schutz vor Missbrauch personenbezogener Daten bei der Datenverarbeitung (Bundesdatenschutzgesetz
        –
        BDSG). Das BDSG enthält insbesondere Spezialregelungen zum Recht auf Auskunft, zum Recht auf Löschung, zum
        Widerspruchsrecht, zur Verarbeitung besonderer Kategorien personenbezogener Daten, zur Verarbeitung für andere
        Zwecke und zur Übermittlung sowie automatisierten Entscheidungsfindung im Einzelfall einschließlich Profiling.
        Des
        Weiteren regelt es die Datenverarbeitung für Zwecke des Beschäftigungsverhältnisses (§ 26 BDSG), insbesondere im
        Hinblick auf die Begründung, Durchführung oder Beendigung von Beschäftigungsverhältnissen sowie die Einwilligung
        von
        Beschäftigten. Ferner können Landesdatenschutzgesetze der einzelnen Bundesländer zur Anwendung gelangen.</p><h2
            id="m27">Sicherheitsmaßnahmen</h2><p>Wir treffen nach Maßgabe der gesetzlichen Vorgaben unter
        Berücksichtigung
        des Stands der Technik, der Implementierungskosten und der Art, des Umfangs, der Umstände und der Zwecke der
        Verarbeitung sowie der unterschiedlichen Eintrittswahrscheinlichkeiten und des Ausmaßes der Bedrohung der Rechte
        und
        Freiheiten natürlicher Personen geeignete technische und organisatorische Maßnahmen, um ein dem Risiko
        angemessenes
        Schutzniveau zu gewährleisten.</p><p>Zu den Maßnahmen gehören insbesondere die Sicherung der Vertraulichkeit,
        Integrität und Verfügbarkeit von Daten durch Kontrolle des physischen und elektronischen Zugangs zu den Daten
        als
        auch des sie betreffenden Zugriffs, der Eingabe, der Weitergabe, der Sicherung der Verfügbarkeit und ihrer
        Trennung.
        Des Weiteren haben wir Verfahren eingerichtet, die eine Wahrnehmung von Betroffenenrechten, die Löschung von
        Daten
        und Reaktionen auf die Gefährdung der Daten gewährleisten. Ferner berücksichtigen wir den Schutz
        personenbezogener
        Daten bereits bei der Entwicklung bzw. Auswahl von Hardware, Software sowie Verfahren entsprechend dem Prinzip
        des
        Datenschutzes, durch Technikgestaltung und durch datenschutzfreundliche Voreinstellungen.</p><p><strong>SSL-Verschlüsselung
            (https)</strong>: Um Ihre via unserem Online-Angebot übermittelten Daten zu schützen, nutzen wir eine
        SSL-Verschlüsselung. Sie erkennen derart verschlüsselte Verbindungen an dem Präfix https:// in der Adresszeile
        Ihres
        Browsers.</p><h2 id="m25">Übermittlung von personenbezogenen Daten</h2><p>Im Rahmen unserer Verarbeitung von
        personenbezogenen Daten kommt es vor, dass die Daten an andere Stellen, Unternehmen, rechtlich selbstständige
        Organisationseinheiten oder Personen übermittelt oder sie ihnen gegenüber offengelegt werden. Zu den Empfängern
        dieser Daten können z.B. mit IT-Aufgaben beauftragte Dienstleister oder Anbieter von Diensten und Inhalten, die
        in
        eine Webseite eingebunden werden, gehören. In solchen Fall beachten wir die gesetzlichen Vorgaben und schließen
        insbesondere entsprechende Verträge bzw. Vereinbarungen, die dem Schutz Ihrer Daten dienen, mit den Empfängern
        Ihrer
        Daten ab.</p><h2 id="m24">Datenverarbeitung in Drittländern</h2><p>Sofern wir Daten in einem Drittland (d.h.,
        außerhalb der Europäischen Union (EU), des Europäischen Wirtschaftsraums (EWR)) verarbeiten oder die
        Verarbeitung im
        Rahmen der Inanspruchnahme von Diensten Dritter oder der Offenlegung bzw. Übermittlung von Daten an andere
        Personen,
        Stellen oder Unternehmen stattfindet, erfolgt dies nur im Einklang mit den gesetzlichen Vorgaben. </p><p>
        Vorbehaltlich ausdrücklicher Einwilligung oder vertraglich oder gesetzlich erforderlicher Übermittlung
        verarbeiten
        oder lassen wir die Daten nur in Drittländern mit einem anerkannten Datenschutzniveau, vertraglichen
        Verpflichtung
        durch sogenannte Standardschutzklauseln der EU-Kommission, beim Vorliegen von Zertifizierungen oder
        verbindlicher
        internen Datenschutzvorschriften verarbeiten (Art. 44 bis 49 DSGVO, Informationsseite der EU-Kommission: <a
                rel="noopener noreferrer nofollow"
                href="https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection_de"
                target="_blank">https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection_de</a>).
    </p><h2 id="m12">Löschung von Daten</h2><p>Die von uns verarbeiteten Daten werden nach Maßgabe der gesetzlichen
        Vorgaben
        gelöscht, sobald deren zur Verarbeitung erlaubten Einwilligungen widerrufen werden oder sonstige Erlaubnisse
        entfallen (z.B. wenn der Zweck der Verarbeitung dieser Daten entfallen ist oder sie für den Zweck nicht
        erforderlich
        sind).</p><p>Sofern die Daten nicht gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke
        erforderlich sind, wird deren Verarbeitung auf diese Zwecke beschränkt. D.h., die Daten werden gesperrt und
        nicht
        für andere Zwecke verarbeitet. Das gilt z.B. für Daten, die aus handels- oder steuerrechtlichen Gründen
        aufbewahrt
        werden müssen oder deren Speicherung zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder
        zum
        Schutz der Rechte einer anderen natürlichen oder juristischen Person erforderlich ist.</p><p>Unsere
        Datenschutzhinweise können ferner weitere Angaben zu der Aufbewahrung und Löschung von Daten beinhalten, die für
        die
        jeweiligen Verarbeitungen vorrangig gelten.</p><h2 id="m134">Einsatz von Cookies</h2><p>Cookies sind
        Textdateien,
        die Daten von besuchten Websites oder Domains enthalten und von einem Browser auf dem Computer des Benutzers
        gespeichert werden. Ein Cookie dient in erster Linie dazu, die Informationen über einen Benutzer während oder
        nach
        seinem Besuch innerhalb eines Onlineangebotes zu speichern. Zu den gespeicherten Angaben können z.B. die
        Spracheinstellungen auf einer Webseite, der Loginstatus, ein Warenkorb oder die Stelle, an der ein Video
        geschaut
        wurde, gehören. Zu dem Begriff der Cookies zählen wir ferner andere Technologien, die die gleichen Funktionen
        wie
        Cookies erfüllen (z.B. wenn Angaben der Nutzer anhand pseudonymer Onlinekennzeichnungen gespeichert werden, auch
        als
        "Nutzer-IDs" bezeichnet)</p><p><strong>Die folgenden Cookie-Typen und Funktionen werden unterschieden:</strong>
    </p>
        <ul>
            <li><strong>Temporäre Cookies (auch: Session- oder Sitzungs-Cookies):</strong>&nbsp;Temporäre Cookies werden
                spätestens gelöscht, nachdem ein Nutzer ein Online-Angebot verlassen und seinen Browser geschlossen hat.
            </li>
            <li><strong>Permanente Cookies:</strong>&nbsp;Permanente Cookies bleiben auch nach dem Schließen des
                Browsers
                gespeichert. So kann beispielsweise der Login-Status gespeichert oder bevorzugte Inhalte direkt
                angezeigt
                werden, wenn der Nutzer eine Website erneut besucht. Ebenso können die Interessen von Nutzern, die zur
                Reichweitenmessung oder zu Marketingzwecken verwendet werden, in einem solchen Cookie gespeichert
                werden.
            </li>
            <li><strong>First-Party-Cookies:</strong>&nbsp;First-Party-Cookies werden von uns selbst gesetzt.</li>
            <li><strong>Third-Party-Cookies (auch: Drittanbieter-Cookies)</strong>: Drittanbieter-Cookies werden
                hauptsächlich
                von Werbetreibenden (sog. Dritten) verwendet, um Benutzerinformationen zu verarbeiten.
            </li>
            <li><strong>Notwendige (auch: essentielle oder unbedingt erforderliche) Cookies:</strong> Cookies können zum
                einen
                für den Betrieb einer Webseite unbedingt erforderlich sein (z.B. um Logins oder andere Nutzereingaben zu
                speichern oder aus Gründen der Sicherheit).
            </li>
            <li><strong>Statistik-, Marketing- und Personalisierungs-Cookies</strong>: Ferner werden Cookies im
                Regelfall auch
                im Rahmen der Reichweitenmessung eingesetzt sowie dann, wenn die Interessen eines Nutzers oder sein
                Verhalten
                (z.B. Betrachten bestimmter Inhalte, Nutzen von Funktionen etc.) auf einzelnen Webseiten in einem
                Nutzerprofil
                gespeichert werden. Solche Profile dienen dazu, den Nutzern z.B. Inhalte anzuzeigen, die ihren
                potentiellen
                Interessen entsprechen. Dieses Verfahren wird auch als "Tracking", d.h., Nachverfolgung der potentiellen
                Interessen der Nutzer bezeichnet. Soweit wir Cookies oder "Tracking"-Technologien einsetzen, informieren
                wir Sie
                gesondert in unserer Datenschutzerklärung oder im Rahmen der Einholung einer Einwilligung.
            </li>
        </ul><p><strong>Hinweise zu Rechtsgrundlagen: </strong> Auf welcher Rechtsgrundlage wir Ihre personenbezogenen
        Daten mit
        Hilfe von Cookies verarbeiten, hängt davon ab, ob wir Sie um eine Einwilligung bitten. Falls dies zutrifft und
        Sie
        in die Nutzung von Cookies einwilligen, ist die Rechtsgrundlage der Verarbeitung Ihrer Daten die erklärte
        Einwilligung. Andernfalls werden die mithilfe von Cookies verarbeiteten Daten auf Grundlage unserer berechtigten
        Interessen (z.B. an einem betriebswirtschaftlichen Betrieb unseres Onlineangebotes und dessen Verbesserung)
        verarbeitet oder, wenn der Einsatz von Cookies erforderlich ist, um unsere vertraglichen Verpflichtungen zu
        erfüllen.</p><p><strong>Speicherdauer: </strong>Sofern wir Ihnen keine expliziten Angaben zur Speicherdauer von
        permanenten Cookies mitteilen (z. B. im Rahmen eines sog. Cookie-Opt-Ins), gehen Sie bitte davon aus, dass die
        Speicherdauer bis zu zwei Jahre betragen kann.</p><p><strong>Allgemeine Hinweise zum Widerruf und Widerspruch
            (Opt-Out): </strong> Abhängig davon, ob die Verarbeitung auf Grundlage einer Einwilligung oder gesetzlichen
        Erlaubnis erfolgt, haben Sie jederzeit die Möglichkeit, eine erteilte Einwilligung zu widerrufen oder der
        Verarbeitung Ihrer Daten durch Cookie-Technologien zu widersprechen (zusammenfassend als "Opt-Out" bezeichnet).
        Sie
        können Ihren Widerspruch zunächst mittels der Einstellungen Ihres Browsers erklären, z.B., indem Sie die Nutzung
        von
        Cookies deaktivieren (wobei hierdurch auch die Funktionsfähigkeit unseres Onlineangebotes eingeschränkt werden
        kann). Ein Widerspruch gegen den Einsatz von Cookies zu Zwecken des Onlinemarketings kann auch mittels einer
        Vielzahl von Diensten, vor allem im Fall des Trackings, über die Webseiten <a rel="noopener noreferrer nofollow"
                                                                                      href="https://optout.aboutads.info"
                                                                                      target="_blank">https://optout.aboutads.info</a>
        und <a rel="noopener noreferrer nofollow" href="https://www.youronlinechoices.com/" target="_blank">https://www.youronlinechoices.com/</a>
        erklärt werden. Daneben können Sie weitere Widerspruchshinweise im Rahmen der Angaben zu den eingesetzten
        Dienstleistern und Cookies erhalten.</p><p><strong>Verarbeitung von Cookie-Daten auf Grundlage einer
            Einwilligung</strong>: Wir setzen ein Verfahren zum Cookie-Einwilligungs-Management ein, in dessen Rahmen
        die
        Einwilligungen der Nutzer in den Einsatz von Cookies, bzw. der im Rahmen des
        Cookie-Einwilligungs-Management-Verfahrens genannten Verarbeitungen und Anbieter eingeholt sowie von den Nutzern
        verwaltet und widerrufen werden können. Hierbei wird die Einwilligungserklärung gespeichert, um deren Abfrage
        nicht
        erneut wiederholen zu müssen und die Einwilligung entsprechend der gesetzlichen Verpflichtung nachweisen zu
        können.
        Die Speicherung kann serverseitig und/oder in einem Cookie (sogenanntes Opt-In-Cookie, bzw. mithilfe
        vergleichbarer
        Technologien) erfolgen, um die Einwilligung einem Nutzer, bzw. dessen Gerät zuordnen zu können. Vorbehaltlich
        individueller Angaben zu den Anbietern von Cookie-Management-Diensten, gelten die folgenden Hinweise: Die Dauer
        der
        Speicherung der Einwilligung kann bis zu zwei Jahren betragen. Hierbei wird ein pseudonymer Nutzer-Identifikator
        gebildet und mit dem Zeitpunkt der Einwilligung, Angaben zur Reichweite der Einwilligung (z. B. welche
        Kategorien
        von Cookies und/oder Diensteanbieter) sowie dem Browser, System und verwendeten Endgerät gespeichert.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten,
                Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte
                Interessen (Art.
                6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
        </ul><h2 id="m317">Geschäftliche Leistungen</h2><p>Wir verarbeiten Daten unserer Vertrags- und Geschäftspartner,
        z.B.
        Kunden und Interessenten (zusammenfassend bezeichnet als "Vertragspartner") im Rahmen von vertraglichen und
        vergleichbaren Rechtsverhältnissen sowie damit verbundenen Maßnahmen und im Rahmen der Kommunikation mit den
        Vertragspartnern (oder vorvertraglich), z.B., um Anfragen zu beantworten.</p><p>Diese Daten verarbeiten wir zur
        Erfüllung unserer vertraglichen Pflichten, zur Sicherung unserer Rechte und zu Zwecken der mit diesen Angaben
        einhergehenden Verwaltungsaufgaben sowie der unternehmerischen Organisation. Die Daten der Vertragspartner geben
        wir
        im Rahmen des geltenden Rechts nur insoweit an Dritte weiter, als dies zu den vorgenannten Zwecken oder zur
        Erfüllung gesetzlicher Pflichten erforderlich ist oder mit Einwilligung der betroffenen Personen erfolgt (z.B.
        an
        beteiligte Telekommunikations-, Transport- und sonstige Hilfsdienste sowie Subunternehmer, Banken, Steuer- und
        Rechtsberater, Zahlungsdienstleister oder Steuerbehörden). Über weitere Verarbeitungsformen, z.B. zu Zwecken des
        Marketings, werden die Vertragspartner im Rahmen dieser Datenschutzerklärung informiert.</p><p>Welche Daten für
        die
        vorgenannten Zwecke erforderlich sind, teilen wir den Vertragspartnern vor oder im Rahmen der Datenerhebung,
        z.B. in
        Onlineformularen, durch besondere Kennzeichnung (z.B. Farben) bzw. Symbole (z.B. Sternchen o.ä.), oder
        persönlich
        mit.</p><p>Wir löschen die Daten nach Ablauf gesetzlicher Gewährleistungs- und vergleichbarer Pflichten, d.h.,
        grundsätzlich nach Ablauf von 4 Jahren, es sei denn, dass die Daten in einem Kundenkonto gespeichert werden,
        z.B.,
        solange sie aus gesetzlichen Gründen der Archivierung aufbewahrt werden müssen (z.B. für Steuerzwecke im
        Regelfall
        10 Jahre). Daten, die uns im Rahmen eines Auftrags durch den Vertragspartner offengelegt wurden, löschen wir
        entsprechend den Vorgaben des Auftrags, grundsätzlich nach Ende des Auftrags.</p><p>Soweit wir zur Erbringung
        unserer Leistungen Drittanbieter oder Plattformen einsetzen, gelten im Verhältnis zwischen den Nutzern und den
        Anbietern die Geschäftsbedingungen und Datenschutzhinweise der jeweiligen Drittanbieter oder Plattformen.</p><p>
        <strong>Kundenkonto</strong>: Vertragspartner können innerhalb unseres Onlineangebotes ein Konto anlegen (z.B.
        Kunden- bzw. Nutzerkonto, kurz "Kundenkonto"). Falls die Registrierung eines Kundenkontos erforderlich ist,
        werden
        Vertragspartner hierauf ebenso hingewiesen wie auf die für die Registrierung erforderlichen Angaben. Die
        Kundenkonten sind nicht öffentlich und können von Suchmaschinen nicht indexiert werden. Im Rahmen der
        Registrierung
        sowie anschließender Anmeldungen und Nutzungen des Kundenkontos speichern wir die IP-Adressen der Kunden nebst
        den
        Zugriffszeitpunkten, um die Registrierung nachweisen und etwaigem Missbrauch des Kundenkontos vorbeugen zu
        können.
    </p><p>Wenn Kunden ihr Kundenkonto gekündigt haben, werden die das Kundenkonto betreffenden Daten gelöscht,
        vorbehaltlich, deren Aufbewahrung ist aus gesetzlichen Gründen erforderlich. Es obliegt den Kunden, ihre Daten
        bei
        erfolgter Kündigung des Kundenkontos zu sichern.</p><p><strong>Shop und E-Commerce</strong>: Wir verarbeiten die
        Daten unserer Kunden, um ihnen die Auswahl, den Erwerb, bzw. die Bestellung der gewählten Produkte, Waren sowie
        verbundener Leistungen, als auch deren Bezahlung und Zustellung, bzw. Ausführung zu ermöglichen. Sofern für die
        Ausführung einer Bestellung erforderlich, setzen wir Dienstleister, insbesondere Post-, Speditions- und
        Versandunternehmen ein, um die Lieferung, bzw. Ausführung gegenüber unseren Kunden durchzuführen. Für die
        Abwicklung
        der Zahlungsvorgänge nehmen wir die Dienste von Banken und Zahlungsdienstleistern in Anspruch. Die
        erforderlichen
        Angaben sind als solche im Rahmen des Bestell- bzw. vergleichbaren Erwerbsvorgangs gekennzeichnet und umfassen
        die
        zur Auslieferung, bzw. Zurverfügungstellung und Abrechnung benötigten Angaben sowie Kontaktinformationen, um
        etwaige
        Rücksprache halten zu können.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Zahlungsdaten (z.B.
                Bankverbindungen, Rechnungen, Zahlungshistorie), Kontaktdaten (z.B. E-Mail, Telefonnummern),
                Vertragsdaten (z.B.
                Vertragsgegenstand, Laufzeit, Kundenkategorie), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an
                Inhalten,
                Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Interessenten, Geschäfts- und Vertragspartner, Kunden.</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Erbringung vertraglicher Leistungen und Kundenservice,
                Kontaktanfragen
                und Kommunikation, Büro- und Organisationsverfahren, Verwaltung und Beantwortung von Anfragen,
                Sicherheitsmaßnahmen.
            </li>
            <li><strong>Rechtsgrundlagen:</strong> Vertragserfüllung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1
                lit. b.
                DSGVO), Rechtliche Verpflichtung (Art. 6 Abs. 1 S. 1 lit. c. DSGVO), Berechtigte Interessen (Art. 6 Abs.
                1 S. 1
                lit. f. DSGVO).
            </li>
        </ul><h2 id="m326">Zahlungsverfahren</h2><p>Im Rahmen von Vertrags- und sonstigen Rechtsbeziehungen, aufgrund
        gesetzlicher Pflichten oder sonst auf Grundlage unserer berechtigten Interessen bieten wir den betroffenen
        Personen
        effiziente und sichere Zahlungsmöglichkeiten an und setzen hierzu neben Banken und Kreditinstituten weitere
        Dienstleister ein (zusammenfassend "Zahlungsdienstleister").</p><p>Zu den durch die Zahlungsdienstleister
        verarbeiteten Daten gehören Bestandsdaten, wie z.B. der Name und die Adresse, Bankdaten, wie z.B. Kontonummern
        oder
        Kreditkartennummern, Passwörter, TANs und Prüfsummen sowie die Vertrags-, Summen- und empfängerbezogenen
        Angaben.
        Die Angaben sind erforderlich, um die Transaktionen durchzuführen. Die eingegebenen Daten werden jedoch nur
        durch
        die Zahlungsdienstleister verarbeitet und bei diesen gespeichert. D.h., wir erhalten keine konto- oder
        kreditkartenbezogenen Informationen, sondern lediglich Informationen mit Bestätigung oder Negativbeauskunftung
        der
        Zahlung. Unter Umständen werden die Daten seitens der Zahlungsdienstleister an Wirtschaftsauskunfteien
        übermittelt.
        Diese Übermittlung bezweckt die Identitäts- und Bonitätsprüfung. Hierzu verweisen wir auf die AGB und die
        Datenschutzhinweise der Zahlungsdienstleister.</p><p>Für die Zahlungsgeschäfte gelten die Geschäftsbedingungen
        und
        die Datenschutzhinweise der jeweiligen Zahlungsdienstleister, welche innerhalb der jeweiligen Webseiten bzw.
        Transaktionsapplikationen abrufbar sind. Wir verweisen auf diese ebenfalls zwecks weiterer Informationen und
        Geltendmachung von Widerrufs-, Auskunfts- und anderen Betroffenenrechten.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Zahlungsdaten (z.B.
                Bankverbindungen, Rechnungen, Zahlungshistorie), Vertragsdaten (z.B. Vertragsgegenstand, Laufzeit,
                Kundenkategorie), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten),
                Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Kunden, Interessenten.</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Erbringung vertraglicher Leistungen und Kundenservice.</li>
            <li><strong>Rechtsgrundlagen:</strong> Vertragserfüllung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1
                lit. b.
                DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
        </ul><p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>
        <ul class="m-elements">
            <li><strong>PayPal:</strong> Zahlungsdienstleistungen und -Lösungen (z.B. PayPal, PayPal Plus, Braintree);
                Dienstanbieter: PayPal (Europe) S.à r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg;
                Website: <a
                        rel="noopener noreferrer nofollow" href="https://www.paypal.com/de" target="_blank">https://www.paypal.com/de</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow"
                                         href="https://www.paypal.com/de/webapps/mpp/ua/privacy-full" target="_blank">https://www.paypal.com/de/webapps/mpp/ua/privacy-full</a>.
            </li>
        </ul><h2 id="m225">Bereitstellung des Onlineangebotes und Webhosting</h2><p>Um unser Onlineangebot sicher und
        effizient
        bereitstellen zu können, nehmen wir die Leistungen von einem oder mehreren Webhosting-Anbietern in Anspruch, von
        deren Servern (bzw. von ihnen verwalteten Servern) das Onlineangebot abgerufen werden kann. Zu diesen Zwecken
        können
        wir Infrastruktur- und Plattformdienstleistungen, Rechenkapazität, Speicherplatz und Datenbankdienste sowie
        Sicherheitsleistungen und technische Wartungsleistungen in Anspruch nehmen.</p><p>Zu den im Rahmen der
        Bereitstellung des Hostingangebotes verarbeiteten Daten können alle die Nutzer unseres Onlineangebotes
        betreffenden
        Angaben gehören, die im Rahmen der Nutzung und der Kommunikation anfallen. Hierzu gehören regelmäßig die
        IP-Adresse,
        die notwendig ist, um die Inhalte von Onlineangeboten an Browser ausliefern zu können, und alle innerhalb
        unseres
        Onlineangebotes oder von Webseiten getätigten Eingaben.</p><p><strong>E-Mail-Versand und -Hosting</strong>: Die
        von
        uns in Anspruch genommenen Webhosting-Leistungen umfassen ebenfalls den Versand, den Empfang sowie die
        Speicherung
        von E-Mails. Zu diesen Zwecken werden die Adressen der Empfänger sowie Absender als auch weitere Informationen
        betreffend den E-Mailversand (z.B. die beteiligten Provider) sowie die Inhalte der jeweiligen E-Mails
        verarbeitet.
        Die vorgenannten Daten können ferner zu Zwecken der Erkennung von SPAM verarbeitet werden. Wir bitten darum, zu
        beachten, dass E-Mails im Internet grundsätzlich nicht verschlüsselt versendet werden. Im Regelfall werden
        E-Mails
        zwar auf dem Transportweg verschlüsselt, aber (sofern kein sogenanntes Ende-zu-Ende-Verschlüsselungsverfahren
        eingesetzt wird) nicht auf den Servern, von denen sie abgesendet und empfangen werden. Wir können daher für den
        Übertragungsweg der E-Mails zwischen dem Absender und dem Empfang auf unserem Server keine Verantwortung
        übernehmen.
    </p><p><strong>Erhebung von Zugriffsdaten und Logfiles</strong>: Wir selbst (bzw. unser Webhostinganbieter) erheben
        Daten zu jedem Zugriff auf den Server (sogenannte Serverlogfiles). Zu den Serverlogfiles können die Adresse und
        Name
        der abgerufenen Webseiten und Dateien, Datum und Uhrzeit des Abrufs, übertragene Datenmengen, Meldung über
        erfolgreichen Abruf, Browsertyp nebst Version, das Betriebssystem des Nutzers, Referrer URL (die zuvor besuchte
        Seite) und im Regelfall IP-Adressen und der anfragende Provider gehören.</p><p>Die Serverlogfiles können zum
        einen
        zu Zwecken der Sicherheit eingesetzt werden, z.B., um eine Überlastung der Server zu vermeiden (insbesondere im
        Fall
        von missbräuchlichen Angriffen, sogenannten DDoS-Attacken) und zum anderen, um die Auslastung der Server und
        ihre
        Stabilität sicherzustellen.</p><p><strong>Content-Delivery-Network</strong>: Wir setzen ein
        "Content-Delivery-Network" (CDN) ein. Ein CDN ist ein Dienst, mit dessen Hilfe Inhalte eines Onlineangebotes,
        insbesondere große Mediendateien, wie Grafiken oder Programm-Skripte, mit Hilfe regional verteilter und über das
        Internet verbundener Server schneller und sicherer ausgeliefert werden können.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Inhaltsdaten (z.B. Eingaben in Onlineformularen),
                Nutzungsdaten (z.B.
                besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B.
                Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Bereitstellung unseres Onlineangebotes und
                Nutzerfreundlichkeit,
                Content Delivery Network (CDN).
            </li>
            <li><strong>Rechtsgrundlagen:</strong> Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
        </ul><p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>
        <ul class="m-elements">
            <li><strong>ALL-INKL:</strong> Leistungen auf dem Gebiet der Bereitstellung von informationstechnischer
                Infrastruktur und verbundenen Dienstleistungen (z.B. Speicherplatz und/oder Rechenkapazitäten);
                Dienstanbieter:
                ALL-INKL.COM - Neue Medien Münnich, Inhaber: René Münnich, Hauptstraße 68, 02742 Friedersdorf,
                Deutschland;
                Website: <a rel="noopener noreferrer nofollow" href="https://all-inkl.com/" target="_blank">https://all-inkl.com/</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow"
                                         href="https://all-inkl.com/datenschutzinformationen/" target="_blank">https://all-inkl.com/datenschutzinformationen/</a>;
                Auftragsverarbeitungsvertrag: mit Anbieter abgeschlossen .
            </li>
            <li><strong>netcup:</strong> Leistungen auf dem Gebiet der Bereitstellung von informationstechnischer
                Infrastruktur
                und verbundenen Dienstleistungen (z.B. Speicherplatz und/oder Rechenkapazitäten); Dienstanbieter: netcup
                GmbH,
                Daimlerstraße 25, D-76185 Karlsruhe, Deutschland; Website: <a rel="noopener noreferrer nofollow"
                                                                              href="https://www.netcup.de/"
                                                                              target="_blank">https://www.netcup.de/</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow"
                                         href="https://www.netcup.de/kontakt/datenschutzerklaerung.php" target="_blank">https://www.netcup.de/kontakt/datenschutzerklaerung.php</a>;
                Auftragsverarbeitungsvertrag: <a rel="noopener noreferrer nofollow"
                                                 href="https://www.netcup-wiki.de/wiki/Zusatzvereinbarung_zur_Auftragsverarbeitung"
                                                 target="_blank">https://www.netcup-wiki.de/wiki/Zusatzvereinbarung_zur_Auftragsverarbeitung</a>.
            </li>
        </ul><h2 id="m367">Registrierung, Anmeldung und Nutzerkonto</h2><p>Nutzer können ein Nutzerkonto anlegen. Im
        Rahmen der
        Registrierung werden den Nutzern die erforderlichen Pflichtangaben mitgeteilt und zu Zwecken der Bereitstellung
        des
        Nutzerkontos auf Grundlage vertraglicher Pflichterfüllung verarbeitet. Zu den verarbeiteten Daten gehören
        insbesondere die Login-Informationen (Nutzername, Passwort sowie eine E-Mail-Adresse).</p><p>Im Rahmen der
        Inanspruchnahme unserer Registrierungs- und Anmeldefunktionen sowie der Nutzung des Nutzerkontos speichern wir
        die
        IP-Adresse und den Zeitpunkt der jeweiligen Nutzerhandlung. Die Speicherung erfolgt auf Grundlage unserer
        berechtigten Interessen als auch jener der Nutzer an einem Schutz vor Missbrauch und sonstiger unbefugter
        Nutzung.
        Eine Weitergabe dieser Daten an Dritte erfolgt grundsätzlich nicht, es sei denn, sie ist zur Verfolgung unserer
        Ansprüche erforderlich oder es besteht eine gesetzliche Verpflichtung hierzu.</p><p>Die Nutzer können über
        Vorgänge,
        die für deren Nutzerkonto relevant sind, wie z.B. technische Änderungen, per E-Mail informiert werden.</p><p>
        <strong>Löschung von Daten nach Kündigung</strong>: Wenn Nutzer ihr Nutzerkonto gekündigt haben, werden deren
        Daten
        im Hinblick auf das Nutzerkonto, vorbehaltlich einer gesetzlichen Erlaubnis, Pflicht oder Einwilligung der
        Nutzer,
        gelöscht.</p><p>Es obliegt den Nutzern, ihre Daten bei erfolgter Kündigung vor dem Vertragsende zu sichern. Wir
        sind
        berechtigt, sämtliche während der Vertragsdauer gespeicherte Daten des Nutzers unwiederbringlich zu löschen.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B.
                E-Mail,
                Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen), Meta-/Kommunikationsdaten (z.B.
                Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Erbringung vertraglicher Leistungen und Kundenservice,
                Sicherheitsmaßnahmen, Verwaltung und Beantwortung von Anfragen.
            </li>
            <li><strong>Rechtsgrundlagen:</strong> Vertragserfüllung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1
                lit. b.
                DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
        </ul><h2 id="m104">Blogs und Publikationsmedien</h2><p>Wir nutzen Blogs oder vergleichbare Mittel der
        Onlinekommunikation und Publikation (nachfolgend "Publikationsmedium"). Die Daten der Leser werden für die
        Zwecke
        des Publikationsmediums nur insoweit verarbeitet, als es für dessen Darstellung und die Kommunikation zwischen
        Autoren und Lesern oder aus Gründen der Sicherheit erforderlich ist. Im Übrigen verweisen wir auf die
        Informationen
        zur Verarbeitung der Besucher unseres Publikationsmediums im Rahmen dieser Datenschutzhinweise.</p><p><strong>Kommentare
            und Beiträge</strong>: Wenn Nutzer Kommentare oder sonstige Beiträge hinterlassen, können ihre IP-Adressen
        auf
        Grundlage unserer berechtigten Interessen gespeichert werden. Das erfolgt zu unserer Sicherheit, falls jemand in
        Kommentaren und Beiträgen widerrechtliche Inhalte hinterlässt (Beleidigungen, verbotene politische Propaganda
        etc.).
        In diesem Fall können wir selbst für den Kommentar oder Beitrag belangt werden und sind daher an der Identität
        des
        Verfassers interessiert.</p><p>Des Weiteren behalten wir uns vor, auf Grundlage unserer berechtigten Interessen
        die
        Angaben der Nutzer zwecks Spamerkennung zu verarbeiten.</p><p>Auf derselben Rechtsgrundlage behalten wir uns
        vor, im
        Fall von Umfragen die IP-Adressen der Nutzer für deren Dauer zu speichern und Cookies zu verwenden, um
        Mehrfachabstimmungen zu vermeiden.</p><p>Die im Rahmen der Kommentare und Beiträge mitgeteilten Informationen
        zur
        Person, etwaige Kontakt- sowie Webseiteninformationen als auch die inhaltlichen Angaben werden von uns bis zum
        Widerspruch der Nutzer dauerhaft gespeichert.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B.
                E-Mail,
                Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen), Nutzungsdaten (z.B. besuchte
                Webseiten,
                Interesse an Inhalten, Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Geräte-Informationen,
                IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Erbringung vertraglicher Leistungen und Kundenservice,
                Feedback (z.B.
                Sammeln von Feedback via Online-Formular), Sicherheitsmaßnahmen, Verwaltung und Beantwortung von
                Anfragen.
            </li>
            <li><strong>Rechtsgrundlagen:</strong> Vertragserfüllung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1
                lit. b.
                DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
        </ul><h2 id="m182">Kontakt- und Anfragenverwaltung</h2><p>Bei der Kontaktaufnahme mit uns (z.B. per
        Kontaktformular,
        E-Mail, Telefon oder via soziale Medien) sowie im Rahmen bestehender Nutzer- und Geschäftsbeziehungen werden die
        Angaben der anfragenden Personen verarbeitet soweit dies zur Beantwortung der Kontaktanfragen und etwaiger
        angefragter Maßnahmen erforderlich ist.</p><p>Die Beantwortung der Kontaktanfragen sowie die Verwaltung von
        Kontakt-
        und Anfragedaten im Rahmen von vertraglichen oder vorvertraglichen Beziehungen erfolgt zur Erfüllung unserer
        vertraglichen Pflichten oder zur Beantwortung von (vor)vertraglichen Anfragen und im Übrigen auf Grundlage der
        berechtigten Interessen an der Beantwortung der Anfragen und Pflege von Nutzer- bzw. Geschäftsbeziehungen.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B.
                E-Mail,
                Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen).
            </li>
            <li><strong>Betroffene Personen:</strong> Kommunikationspartner.</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Kontaktanfragen und Kommunikation.</li>
            <li><strong>Rechtsgrundlagen:</strong> Vertragserfüllung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1
                lit. b.
                DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
        </ul><h2 id="m406">Chatbots und Chatfunktionen</h2><p>Wir bieten als Kommunikationsmöglichkeit Online-Chats und
        Chatbot-Funktionen an (zusammen als "Chat-Dienste" bezeichnet). Bei einem Chat handelt es sich um eine mit
        gewisser
        Zeitnähe geführte Online-Unterhaltung. Bei einem Chatbot handelt es sich um eine Software, die Fragen der Nutzer
        beantwortet oder sie über Nachrichten informiert. Wenn Sie unsere Chat-Funktionen nutzen, können wir Ihre
        personenbezogenen Daten verarbeiten.</p><p>Falls Sie unsere Chat-Dienste innerhalb einer Online-Plattform
        nutzen,
        wird zusätzlich Ihre Identifikationsnummer innerhalb der jeweiligen Plattform gespeichert. Wir können zudem
        Informationen darüber erheben, welche Nutzer wann mit unseren Chat-Diensten interagieren. Ferner speichern wir
        den
        Inhalt Ihrer Konversationen über die Chat-Dienste und protokollieren Registrierungs- und Einwilligungsvorgänge,
        um
        diese nach gesetzlichen Vorgaben nachweisen zu können. </p><p>Wir weisen Nutzer darauf hin, dass der jeweilige
        Plattformanbieter in Erfahrung bringen kann, dass und wann Nutzer mit unseren Chat-Diensten kommunizieren sowie
        technische Informationen zum verwendeten Gerät der Nutzer und je nach Einstellungen ihres Gerätes auch
        Standortinformationen (sogenannte Metadaten) zu Zwecken der Optimierung der jeweiligen Dienste und Zwecken der
        Sicherheit erheben kann. Ebenfalls könnten die Metadaten der Kommunikation via Chat-Diensten (d.h. z.B., die
        Information, wer mit wem kommuniziert hat) durch die jeweiligen Plattformanbieter nach Maßgabe ihrer
        Bestimmungen,
        auf die wir zwecks weiterer Information verweisen, für Zwecke des Marketings oder zur Anzeige auf Nutzer
        zugeschnittener Werbung verwendet werden.</p><p>Sofern Nutzer sich gegenüber einem Chatbot bereiterklären,
        Informationen mit regelmäßigen Nachrichten zu aktivieren, steht ihnen jederzeit die Möglichkeit zur Verfügung,
        die
        Informationen für die Zukunft abzubestellen. Der Chatbot weist Nutzer darauf hin, wie und mit welchen Begriffen
        sie
        die Nachrichten abbestellen können. Mit dem Abbestellen der Chatbotnachrichten werden Daten der Nutzer aus dem
        Verzeichnis der Nachrichtenempfänger gelöscht.</p><p>Die vorgenannten Angaben nutzen wir, um unsere Chat-Dienste
        zu
        betreiben, z.B., um Nutzer persönlich anzusprechen, um ihre Anfragen zu beantworten, etwaige angeforderte
        Inhalte zu
        übermitteln und auch, um unsere Chat-Dienste zu verbessern (z.B., um Chatbots Antworten auf häufig gestellte
        Fragen
        "beizubringen“ oder unbeantwortete Anfragen zu erkennen).</p><p><strong>Hinweise zu Rechtsgrundlagen:</strong>
        Wir
        setzen die Chat-Dienste auf Grundlage einer Einwilligung ein, wenn wir zuvor eine Erlaubnis der Nutzer in die
        Verarbeitung ihrer Daten im Rahmen unserer Chat-Dienste eingeholt haben (dies gilt für die Fälle, in denen
        Nutzer um
        eine Einwilligung gebeten werden, z.B., damit ein Chatbot ihnen regelmäßig Nachrichten zusendet). Sofern wir
        Chat-Dienste einsetzen, um Anfragen der Nutzer zu unseren Leistungen oder unserem Unternehmen zu beantworten,
        erfolgt dies zur vertraglichen und vorvertraglichen Kommunikation. Im Übrigen setzen wir Chat-Dienste auf
        Grundlage
        unserer berechtigten Interessen an einer Optimierung der Chat-Dienste, ihrer Betriebswirtschaftlichkeit sowie
        einer
        Steigerung der positiven Nutzererfahrung ein.</p><p><strong>Widerruf, Widerspruch und Löschung: </strong>Sie
        können
        jederzeit eine erteilte Einwilligung widerrufen oder der Verarbeitung Ihrer Daten im Rahmen unserer Chat-Dienste
        widersprechen.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B.
                Eingaben in Onlineformularen), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten,
                Zugriffszeiten),
                Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Kommunikationspartner, Nutzer (z.B. Webseitenbesucher, Nutzer von
                Onlinediensten).
            </li>
            <li><strong>Zwecke der Verarbeitung:</strong> Kontaktanfragen und Kommunikation, Direktmarketing (z.B. per
                E-Mail
                oder postalisch), Profile mit nutzerbezogenen Informationen (Erstellen von Nutzerprofilen).
            </li>
            <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Vertragserfüllung
                und
                vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1
                lit. f.
                DSGVO).
            </li>
        </ul><p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>
        <ul class="m-elements">
            <li><strong>Alexa (Amazon):</strong> Assistenz-Software und -Dienste; Dienstanbieter: Amazon Europe Core
                S.à.r.l.,
                die Amazon EU S.à.r.l, die Amazon Services Europe S.à.r.l. und die Amazon Media EU S.à.r.l., alle vier
                ansässig
                in 38, avenue John F. Kennedy, L-1855 Luxemburg, sowie Amazon Instant Video Germany GmbH, Domagkstr. 28,
                80807
                München (zusammen "Amazon Europe"), Mutterunternehmen: Amazon.com, Inc., 2021 Seventh Ave, Seattle,
                Washington
                98121, USA; Website: <a rel="noopener noreferrer nofollow" href="https://developer.amazon.com/de/alexa"
                                        target="_blank">https://developer.amazon.com/de/alexa</a>; Datenschutzerklärung:
                <a
                        rel="noopener noreferrer nofollow"
                        href="https://www.amazon.de/gp/help/customer/display.html?nodeId=201909010" target="_blank">https://www.amazon.de/gp/help/customer/display.html?nodeId=201909010</a>.
            </li>
            <li><strong>ManyChat:</strong> Chatbot- und Assistenz-Software sowie verbundene Dienste; Dienstanbieter:
                ManyChat,
                Inc.,535 Everett Ave, Palo Alto, CA 94301, USA; Website: <a rel="noopener noreferrer nofollow"
                                                                            href="https://manychat.com" target="_blank">https://manychat.com</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://manychat.com/privacy.html"
                                         target="_blank">https://manychat.com/privacy.html</a>.
            </li>
        </ul><h2 id="m17">Newsletter und elektronische Benachrichtigungen</h2><p>Wir versenden Newsletter, E-Mails und
        weitere
        elektronische Benachrichtigungen (nachfolgend "Newsletter“) nur mit der Einwilligung der Empfänger oder einer
        gesetzlichen Erlaubnis. Sofern im Rahmen einer Anmeldung zum Newsletter dessen Inhalte konkret umschrieben
        werden,
        sind sie für die Einwilligung der Nutzer maßgeblich. Im Übrigen enthalten unsere Newsletter Informationen zu
        unseren
        Leistungen und uns.</p><p>Um sich zu unseren Newslettern anzumelden, reicht es grundsätzlich aus, wenn Sie Ihre
        E-Mail-Adresse angeben. Wir können Sie jedoch bitten, einen Namen, zwecks persönlicher Ansprache im Newsletter,
        oder
        weitere Angaben, sofern diese für die Zwecke des Newsletters erforderlich sind, zu tätigen.</p><p><strong>Double-Opt-In-Verfahren:</strong>
        Die Anmeldung zu unserem Newsletter erfolgt grundsätzlich in einem sogenannte Double-Opt-In-Verfahren. D.h., Sie
        erhalten nach der Anmeldung eine E-Mail, in der Sie um die Bestätigung Ihrer Anmeldung gebeten werden. Diese
        Bestätigung ist notwendig, damit sich niemand mit fremden E-Mail-Adressen anmelden kann. Die Anmeldungen zum
        Newsletter werden protokolliert, um den Anmeldeprozess entsprechend den rechtlichen Anforderungen nachweisen zu
        können. Hierzu gehört die Speicherung des Anmelde- und des Bestätigungszeitpunkts als auch der IP-Adresse.
        Ebenso
        werden die Änderungen Ihrer bei dem Versanddienstleister gespeicherten Daten protokolliert.</p><p><strong>Löschung
            und Einschränkung der Verarbeitung: </strong> Wir können die ausgetragenen E-Mail-Adressen bis zu drei
        Jahren
        auf Grundlage unserer berechtigten Interessen speichern, bevor wir sie löschen, um eine ehemals gegebene
        Einwilligung nachweisen zu können. Die Verarbeitung dieser Daten wird auf den Zweck einer möglichen Abwehr von
        Ansprüchen beschränkt. Ein individueller Löschungsantrag ist jederzeit möglich, sofern zugleich das ehemalige
        Bestehen einer Einwilligung bestätigt wird. Im Fall von Pflichten zur dauerhaften Beachtung von Widersprüchen
        behalten wir uns die Speicherung der E-Mail-Adresse alleine zu diesem Zweck in einer Sperrliste (sogenannte
        "Blocklist") vor.</p><p>Die Protokollierung des Anmeldeverfahrens erfolgt auf Grundlage unserer berechtigten
        Interessen zu Zwecken des Nachweises seines ordnungsgemäßen Ablaufs. Soweit wir einen Dienstleister mit dem
        Versand
        von E-Mails beauftragen, erfolgt dies auf Grundlage unserer berechtigten Interessen an einem effizienten und
        sicheren Versandsystem.</p><p><strong>Hinweise zu Rechtsgrundlagen:</strong> Der Versand der Newsletter erfolgt
        auf
        Grundlage einer Einwilligung der Empfänger oder, falls eine Einwilligung nicht erforderlich ist, auf Grundlage
        unserer berechtigten Interessen am Direktmarketing, sofern und soweit diese gesetzlich, z.B. im Fall von
        Bestandskundenwerbung, erlaubt ist. Soweit wir einen Dienstleister mit dem Versand von E-Mails beauftragen,
        geschieht dies auf der Grundlage unserer berechtigten Interessen. Das Registrierungsverfahren wird auf der
        Grundlage
        unserer berechtigten Interessen aufgezeichnet, um nachzuweisen, dass es in Übereinstimmung mit dem Gesetz
        durchgeführt wurde.</p><p>Inhalte: Informationen zu uns, unseren Leistungen, Aktionen und Angeboten.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B.
                E-Mail,
                Telefonnummern), Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Kommunikationspartner.</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Direktmarketing (z.B. per E-Mail oder postalisch).</li>
            <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte
                Interessen (Art.
                6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
            <li><strong>Widerspruchsmöglichkeit (Opt-Out):</strong> Sie können den Empfang unseres Newsletters jederzeit
                kündigen, d.h. Ihre Einwilligungen widerrufen, bzw. dem weiteren Empfang widersprechen. Einen Link zur
                Kündigung
                des Newsletters finden Sie entweder am Ende eines jeden Newsletters oder können sonst eine der oben
                angegebenen
                Kontaktmöglichkeiten, vorzugswürdig E-Mail, hierzu nutzen.
            </li>
        </ul><h2 id="m638">Werbliche Kommunikation via E-Mail, Post, Fax oder Telefon</h2><p>Wir verarbeiten
        personenbezogene
        Daten zu Zwecken der werblichen Kommunikation, die über diverse Kanäle, wie z.B. E-Mail, Telefon, Post oder Fax,
        entsprechend den gesetzlichen Vorgaben erfolgen kann.</p><p>Die Empfänger haben das Recht, erteilte
        Einwilligungen
        jederzeit zu widerrufen oder der werblichen Kommunikation jederzeit zu widersprechen.</p><p>Nach Widerruf bzw.
        Widerspruch können wir die zum Nachweis der Einwilligung erforderlichen Daten bis zu drei Jahren auf Grundlage
        unserer berechtigten Interessen speichern, bevor wir sie löschen. Die Verarbeitung dieser Daten wird auf den
        Zweck
        einer möglichen Abwehr von Ansprüchen beschränkt. Ein individueller Löschungsantrag ist jederzeit möglich,
        sofern
        zugleich das ehemalige Bestehen einer Einwilligung bestätigt wird.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen, Adressen), Kontaktdaten (z.B.
                E-Mail,
                Telefonnummern).
            </li>
            <li><strong>Betroffene Personen:</strong> Kommunikationspartner.</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Direktmarketing (z.B. per E-Mail oder postalisch).</li>
            <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte
                Interessen (Art.
                6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
        </ul><h2 id="m408">Umfragen und Befragungen</h2><p>Die von uns durchgeführten Umfragen und Befragungen
        (nachfolgend
        "Befragungen") werden anonym ausgewertet. Eine Verarbeitung personenbezogener Daten erfolgt nur insoweit, als
        dies
        zu Bereitstellung und technischen Durchführung der Umfragen erforderlich ist (z.B. Verarbeitung der IP-Adresse,
        um
        die Umfrage im Browser des Nutzers darzustellen oder mithilfe eines temporären Cookies (Session-Cookie) eine
        Wiederaufnahme der Umfrage zu ermöglichen) oder Nutzer eingewilligt haben.</p><p><strong>Hinweise zu
            Rechtsgrundlagen:</strong> Sofern wir die Teilnehmer um eine Einwilligung in die Verarbeitung iherer Daten
        bitten, ist diese Rechtsgrundlage der Verarbeitung, ansonsten erfolgt die Verarbeitung der Daten der Teilnehmer
        auf
        Grundlage unserer berechtigten Interessen an der Durchführung einer objektiven Umfrage.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B.
                Eingaben in Onlineformularen), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten,
                Zugriffszeiten),
                Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Kommunikationspartner.</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Kontaktanfragen und Kommunikation, Direktmarketing (z.B. per
                E-Mail
                oder postalisch).
            </li>
            <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte
                Interessen (Art.
                6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
        </ul><h2 id="m263">Webanalyse, Monitoring und Optimierung</h2><p>Die Webanalyse (auch als "Reichweitenmessung"
        bezeichnet) dient der Auswertung der Besucherströme unseres Onlineangebotes und kann Verhalten, Interessen oder
        demographische Informationen zu den Besuchern, wie z.B. das Alter oder das Geschlecht, als pseudonyme Werte
        umfassen. Mit Hilfe der Reichweitenanalyse können wir z.B. erkennen, zu welcher Zeit unser Onlineangebot oder
        dessen
        Funktionen oder Inhalte am häufigsten genutzt werden oder zur Wiederverwendung einladen. Ebenso können wir
        nachvollziehen, welche Bereiche der Optimierung bedürfen. </p><p>Neben der Webanalyse können wir auch
        Testverfahren
        einsetzen, um z.B. unterschiedliche Versionen unseres Onlineangebotes oder seiner Bestandteile zu testen und
        optimieren.</p><p>Zu diesen Zwecken können sogenannte Nutzerprofile angelegt und in einer Datei (sogenannte
        "Cookie") gespeichert oder ähnliche Verfahren mit dem gleichen Zweck genutzt werden. Zu diesen Angaben können
        z.B.
        betrachtete Inhalte, besuchte Webseiten und dort genutzte Elemente und technische Angaben, wie der verwendete
        Browser, das verwendete Computersystem sowie Angaben zu Nutzungszeiten gehören. Sofern Nutzer in die Erhebung
        ihrer
        Standortdaten eingewilligt haben, können je nach Anbieter auch diese verarbeitet werden.</p><p>Es werden
        ebenfalls
        die IP-Adressen der Nutzer gespeichert. Jedoch nutzen wir ein IP-Masking-Verfahren (d.h., Pseudonymisierung
        durch
        Kürzung der IP-Adresse) zum Schutz der Nutzer. Generell werden die im Rahmen von Webanalyse, A/B-Testings und
        Optimierung keine Klardaten der Nutzer (wie z.B. E-Mail-Adressen oder Namen) gespeichert, sondern Pseudonyme.
        D.h.,
        wir als auch die Anbieter der eingesetzten Software kennen nicht die tatsächliche Identität der Nutzer, sondern
        nur
        den für Zwecke der jeweiligen Verfahren in deren Profilen gespeicherten Angaben.</p><p><strong>Hinweise zu
            Rechtsgrundlagen:</strong> Sofern wir die Nutzer um deren Einwilligung in den Einsatz der Drittanbieter
        bitten,
        ist die Rechtsgrundlage der Verarbeitung von Daten die Einwilligung. Ansonsten werden die Daten der Nutzer auf
        Grundlage unserer berechtigten Interessen (d.h. Interesse an effizienten, wirtschaftlichen und
        empfängerfreundlichen
        Leistungen) verarbeitet. In diesem Zusammenhang möchten wir Sie auch auf die Informationen zur Verwendung von
        Cookies in dieser Datenschutzerklärung hinweisen.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten,
                Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Reichweitenmessung (z.B. Zugriffsstatistiken, Erkennung
                wiederkehrender Besucher), Profile mit nutzerbezogenen Informationen (Erstellen von Nutzerprofilen).
            </li>
            <li><strong>Sicherheitsmaßnahmen:</strong> IP-Masking (Pseudonymisierung der IP-Adresse).</li>
            <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte
                Interessen (Art.
                6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
        </ul><p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>
        <ul class="m-elements">
            <li><strong>Google Analytics:</strong> Reichweitenmessung und Webanalyse; Dienstanbieter: Google Ireland
                Limited,
                Gordon House, Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway,
                Mountain View, CA 94043, USA; Website: <a rel="noopener noreferrer nofollow"
                                                          href="https://marketingplatform.google.com/intl/de/about/analytics/"
                                                          target="_blank">https://marketingplatform.google.com/intl/de/about/analytics/</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://policies.google.com/privacy"
                                         target="_blank">https://policies.google.com/privacy</a>; Arten der Verarbeitung
                sowie
                der verarbeiteten Daten: <a rel="noopener noreferrer nofollow"
                                            href="https://privacy.google.com/businesses/adsservices" target="_blank">https://privacy.google.com/businesses/adsservices</a>;
                Datenverarbeitungsbedingungen für Google Werbeprodukte und Standardvertragsklauseln für
                Drittlandtransfers von
                Daten: <a rel="noopener noreferrer nofollow" href="https://business.safety.google/adsprocessorterms"
                          target="_blank">https://business.safety.google/adsprocessorterms</a>.
            </li>
            <li><strong>Google Tag Manager:</strong> Google Tag Manager ist eine Lösung, mit der wir sog. Website-Tags
                über eine
                Oberfläche verwalten und so andere Dienste in unser Onlineangebot einbinden können (hierzu wird auf
                weitere
                Angaben in dieser Datenschutzerklärung verwiesen). Mit dem Tag Manager selbst (welches die Tags
                implementiert)
                werden daher z. B. noch keine Profile der Nutzer erstellt oder Cookies gespeichert. Google erfährt
                lediglich die
                IP-Adresse des Nutzers, was notwendig ist, um den Google Tag Manager auszuführen. Dienstanbieter: Google
                Ireland
                Limited, Gordon House, Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre
                Parkway, Mountain View, CA 94043, USA; Website: <a rel="noopener noreferrer nofollow"
                                                                   href="https://marketingplatform.google.com"
                                                                   target="_blank">https://marketingplatform.google.com</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://policies.google.com/privacy"
                                         target="_blank">https://policies.google.com/privacy</a>; Arten der Verarbeitung
                sowie
                der verarbeiteten Daten: <a rel="noopener noreferrer nofollow"
                                            href="https://privacy.google.com/businesses/adsservices" target="_blank">https://privacy.google.com/businesses/adsservices</a>;
                Datenverarbeitungsbedingungen für Google Werbeprodukte und Standardvertragsklauseln für
                Drittlandtransfers von
                Daten: <a rel="noopener noreferrer nofollow" href="https://business.safety.google/adsprocessorterms"
                          target="_blank">https://business.safety.google/adsprocessorterms</a>.
            </li>
            <li><strong>Hotjar: </strong> Unsere Webseite nutzt die Analyse-Software Hotjar der Firma Hotjar Ltd.
                (Hotjar Ltd, Level 2, St Julians Business Centre,3, Elia Zammit Street,St Julians STJ 1000, Malta,
                Europa). Mittels dieser Software können wir das Nutzungsverhalten unserer Webseiten-Besucher
                analysieren, da die Klicks, Mausbewegungen und ähnliche Verhaltensweisen auf unserer Webseite gemessen
                und ausgewertet werden. Ziel dieser Software ist, Verbesserungsmöglichkeiten unserer Webseite
                aufzuzeigen.

                Hotjar verwendet dabei Cookies, die auf Ihrem Endgerät gespeichert werden und die eine Analyse der
                Benutzung der Webseite durch Sie ermöglichen. Um eine direkte Personenbeziehbarkeit auszuschließen,
                werden IP-Adressen nur anonymisiert gespeichert und weiterverarbeitet. Außerdem werden Informationen zum
                Betriebssystem, Browser, eingehende und ausgehende Verweise (Links), geografische Herkunft sowie
                Auflösung und Art des Geräts zu statistischen Zwecken ausgewertet. Diese Informationen sind nicht
                personenbezogen und werden von uns oder von Hotjar nicht an Dritte weitergegeben.

                Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software
                verhindern. Wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche
                Funktionen dieser Webseite vollumfänglich werden nutzen können.

                Darüber hinaus können Sie die Aufzeichnung verhindern bzw. wieder aktivieren, wenn Sie der Anleitung
                unter <a rel="noopener noreferrer nofollow" href="https://www.hotjar.com/opt-out">https://www.hotjar.com/opt-out</a>
                folgen.
            </li>
        </ul><h2 id="m264">Onlinemarketing</h2><p>Wir verarbeiten personenbezogene Daten zu Zwecken des
        Onlinemarketings,
        worunter insbesondere die Vermarktung von Werbeflächen oder Darstellung von werbenden und sonstigen Inhalten
        (zusammenfassend als "Inhalte" bezeichnet) anhand potentieller Interessen der Nutzer sowie die Messung ihrer
        Effektivität fallen kann. </p><p>Zu diesen Zwecken werden sogenannte Nutzerprofile angelegt und in einer Datei
        (sogenannte "Cookie") gespeichert oder ähnliche Verfahren genutzt, mittels derer die für die Darstellung der
        vorgenannten Inhalte relevante Angaben zum Nutzer gespeichert werden. Zu diesen Angaben können z.B. betrachtete
        Inhalte, besuchte Webseiten, genutzte Onlinenetzwerke, aber auch Kommunikationspartner und technische Angaben,
        wie
        der verwendete Browser, das verwendete Computersystem sowie Angaben zu Nutzungszeiten gehören. Sofern Nutzer in
        die
        Erhebung ihrer Standortdaten eingewilligt haben, können auch diese verarbeitet werden.</p><p>Es werden ebenfalls
        die
        IP-Adressen der Nutzer gespeichert. Jedoch nutzen wir zur Verfügung stehende IP-Masking-Verfahren (d.h.,
        Pseudonymisierung durch Kürzung der IP-Adresse) zum Schutz der Nutzer. Generell werden im Rahmen des
        Onlinemarketingverfahren keine Klardaten der Nutzer (wie z.B. E-Mail-Adressen oder Namen) gespeichert, sondern
        Pseudonyme. D.h., wir als auch die Anbieter der Onlinemarketingverfahren kennen nicht die tatsächlich Identität
        der
        Nutzer, sondern nur die in deren Profilen gespeicherten Angaben.</p><p>Die Angaben in den Profilen werden im
        Regelfall in den Cookies oder mittels ähnlicher Verfahren gespeichert. Diese Cookies können später generell auch
        auf
        anderen Webseiten die dasselbe Onlinemarketingverfahren einsetzen, ausgelesen und zu Zwecken der Darstellung von
        Inhalten analysiert als auch mit weiteren Daten ergänzt und auf dem Server des
        Onlinemarketingverfahrensanbieters
        gespeichert werden.</p><p>Ausnahmsweise können Klardaten den Profilen zugeordnet werden. Das ist der Fall, wenn
        die
        Nutzer z.B. Mitglieder eines sozialen Netzwerks sind, dessen Onlinemarketingverfahren wir einsetzen und das
        Netzwerk
        die Profile der Nutzer mit den vorgenannten Angaben verbindet. Wir bitten darum, zu beachten, dass Nutzer mit
        den
        Anbietern zusätzliche Abreden, z.B. durch Einwilligung im Rahmen der Registrierung, treffen können.</p><p>Wir
        erhalten grundsätzlich nur Zugang zu zusammengefassten Informationen über den Erfolg unserer Werbeanzeigen.
        Jedoch
        können wir im Rahmen sogenannter Konversionsmessungen prüfen, welche unserer Onlinemarketingverfahren zu einer
        sogenannten Konversion geführt haben, d.h. z.B., zu einem Vertragsschluss mit uns. Die Konversionsmessung wird
        alleine zur Analyse des Erfolgs unserer Marketingmaßnahmen verwendet.</p><p>Solange nicht anders angegeben,
        bitten
        wir Sie davon auszugehen, dass verwendete Cookies für einen Zeitraum von zwei Jahren gespeichert werden.</p><p>
        <strong>Hinweise zu Rechtsgrundlagen:</strong> Sofern wir die Nutzer um deren Einwilligung in den Einsatz der
        Drittanbieter bitten, ist die Rechtsgrundlage der Verarbeitung von Daten die Einwilligung. Ansonsten werden die
        Daten der Nutzer auf Grundlage unserer berechtigten Interessen (d.h. Interesse an effizienten, wirtschaftlichen
        und
        empfängerfreundlichen Leistungen) verarbeitet. In diesem Zusammenhang möchten wir Sie auch auf die Informationen
        zur
        Verwendung von Cookies in dieser Datenschutzerklärung hinweisen.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten,
                Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Marketing, Profile mit nutzerbezogenen Informationen
                (Erstellen von
                Nutzerprofilen), Konversionsmessung (Messung der Effektivität von Marketingmaßnahmen).
            </li>
            <li><strong>Sicherheitsmaßnahmen:</strong> IP-Masking (Pseudonymisierung der IP-Adresse).</li>
            <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Berechtigte
                Interessen (Art.
                6 Abs. 1 S. 1 lit. f. DSGVO).
            </li>
            <li><strong>Widerspruchsmöglichkeit (Opt-Out):</strong> Wir verweisen auf die Datenschutzhinweise der
                jeweiligen
                Anbieter und die zu den Anbietern angegebenen Widerspruchsmöglichkeiten (sog. "Opt-Out"). Sofern keine
                explizite
                Opt-Out-Möglichkeit angegeben wurde, besteht zum einen die Möglichkeit, dass Sie Cookies in den
                Einstellungen
                Ihres Browsers abschalten. Hierdurch können jedoch Funktionen unseres Onlineangebotes eingeschränkt
                werden. Wir
                empfehlen daher zusätzlich die folgenden Opt-Out-Möglichkeiten, die zusammenfassend auf jeweilige
                Gebiete
                gerichtet angeboten werden: a) Europa: <a rel="noopener noreferrer nofollow"
                                                          href="https://www.youronlinechoices.eu" target="_blank">https://www.youronlinechoices.eu</a>.
                b) Kanada: <a rel="noopener noreferrer nofollow" href="https://www.youradchoices.ca/choices"
                              target="_blank">https://www.youradchoices.ca/choices</a>.
                c) USA: <a rel="noopener noreferrer nofollow" href="https://www.aboutads.info/choices" target="_blank">https://www.aboutads.info/choices</a>.
                d) Gebietsübergreifend: <a rel="noopener noreferrer nofollow" href="https://optout.aboutads.info"
                                           target="_blank">https://optout.aboutads.info</a>.
            </li>
        </ul><p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>
        <ul class="m-elements">
            <li><strong>Google Analytics:</strong> Onlinemarketing und Webanalyse; Dienstanbieter: Google Ireland
                Limited,
                Gordon House, Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway,
                Mountain View, CA 94043, USA; Website: <a rel="noopener noreferrer nofollow"
                                                          href="https://marketingplatform.google.com/intl/de/about/analytics/"
                                                          target="_blank">https://marketingplatform.google.com/intl/de/about/analytics/</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://policies.google.com/privacy"
                                         target="_blank">https://policies.google.com/privacy</a>;
                Widerspruchsmöglichkeit
                (Opt-Out): Opt-Out-Plugin: <a rel="noopener noreferrer nofollow"
                                              href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=de</a>,
                Einstellungen für die Darstellung von Werbeeinblendungen: <a rel="noopener noreferrer nofollow"
                                                                             href="https://adssettings.google.com/authenticated"
                                                                             target="_blank">https://adssettings.google.com/authenticated</a>;
                Arten der Verarbeitung sowie der verarbeiteten Daten: <a rel="noopener noreferrer nofollow"
                                                                         href="https://privacy.google.com/businesses/adsservices"
                                                                         target="_blank">https://privacy.google.com/businesses/adsservices</a>;
                Datenverarbeitungsbedingungen für Google Werbeprodukte und Standardvertragsklauseln für
                Drittlandtransfers von
                Daten: <a rel="noopener noreferrer nofollow" href="https://business.safety.google/adsprocessorterms"
                          target="_blank">https://business.safety.google/adsprocessorterms</a>.
            </li>
            <li><strong>Google Ads und Konversionsmessung:</strong> Wir nutzen das Onlinemarketingverfahren "Google
                Ads", um
                Anzeigen im Google-Werbe-Netzwerk zu platzieren (z.B., in Suchergebnissen, in Videos, auf Webseiten,
                etc.),
                damit sie Nutzern angezeigt werden, die ein mutmaßliches Interesse an den Anzeigen haben. Ferner messen
                wir die
                Konversion der Anzeigen. Wir erfahren jedoch nur die anonyme Gesamtanzahl der Nutzer, die auf unsere
                Anzeige
                geklickt haben und zu einer mit einem sog "Conversion-Tracking-Tag" versehenen Seite weitergeleitet
                wurden. Wir
                selbst erhalten jedoch keine Informationen, mit denen sich Nutzer identifizieren lassen; Dienstanbieter:
                Google
                Ireland Limited, Gordon House, Barrow Street, Dublin 4, Ireland, parent company: Google LLC, 1600
                Amphitheatre
                Parkway, Mountain View, CA 94043, USA; Website: <a rel="noopener noreferrer nofollow"
                                                                   href="https://marketingplatform.google.com"
                                                                   target="_blank">https://marketingplatform.google.com</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://policies.google.com/privacy"
                                         target="_blank">https://policies.google.com/privacy</a>; Arten der Verarbeitung
                sowie
                der verarbeiteten Daten: <a rel="noopener noreferrer nofollow"
                                            href="https://privacy.google.com/businesses/adsservices" target="_blank">https://privacy.google.com/businesses/adsservices</a>;
                Datenverarbeitungsbedingungen für Google Werbeprodukte: Informationen zu den Diensten
                Datenverarbeitungsbedingungen zwischen Verantwortlichen und Standardvertragsklauseln für
                Drittlandtransfers von
                Daten: <a rel="noopener noreferrer nofollow" href="https://business.safety.google/adscontrollerterms"
                          target="_blank">https://business.safety.google/adscontrollerterms</a>.
            </li>
        </ul><h2 id="m135">Affiliate-Programme und Affiliate-Links</h2><p>In unser Onlineangebot binden wir sogenannte
        Affiliate-Links oder andere Verweise (zu denen z.B. Suchmasken, Widgets oder Rabatt-Codes gehören können) auf
        die
        Angebote und Leistungen von Drittanbietern ein (zusammenfassend bezeichnet als "Affiliate-Links"). Wenn Nutzer
        den
        Affiliate-Links folgen, bzw. anschließend die Angebote wahrnehmen, können wir von diesen Drittanbietern eine
        Provision oder sonstige Vorteile erhalten (zusammenfassend bezeichnet als "Provision").</p><p>Um nachverfolgen
        zu
        können, ob die Nutzer die Angebote eines von uns eingesetzten Affiliate-Links wahrgenommen haben, ist es
        notwendig,
        dass die jeweiligen Drittanbieter erfahren, dass die Nutzer einem innerhalb unseres Onlineangebotes eingesetzten
        Affiliate-Link gefolgt sind. Die Zuordnung der Affiliate-Links zu den jeweiligen Geschäftsabschlüssen oder zu
        sonstigen Aktionen (z.B. Käufen) dient alleine dem Zweck der Provisionsabrechnung und wird aufgehoben, sobald
        sie
        für den Zweck nicht mehr erforderlich ist.</p><p>Für die Zwecke der vorgenannten Zuordnung der Affiliate-Links
        können die Affiliate-Links um bestimmte Werte ergänzt werden, die ein Bestandteil des Links sind oder
        anderweitig,
        z.B. in einem Cookie, gespeichert werden können. Zu den Werten können insbesondere die Ausgangswebseite
        (Referrer),
        der Zeitpunkt, eine Online-Kennung der Betreiber der Webseite, auf der sich der Affiliate-Link befand, eine
        Online-Kennung des jeweiligen Angebotes, die Art des verwendeten Links, die Art des Angebotes und eine
        Online-Kennung des Nutzers gehören.</p><p><strong>Hinweise zu Rechtsgrundlagen:</strong> Sofern wir die Nutzer
        um
        deren Einwilligung in den Einsatz der Drittanbieter bitten, ist die Rechtsgrundlage der Verarbeitung von Daten
        die
        Einwilligung. Ferner kann deren Einsatz ein Bestandteil unserer (vor)vertraglichen Leistungen sein, sofern der
        Einsatz der Drittanbieter in diesem Rahmen vereinbart wurde. Ansonsten werden die Daten der Nutzer auf Grundlage
        unserer berechtigten Interessen (d.h. Interesse an effizienten, wirtschaftlichen und empfängerfreundlichen
        Leistungen) verarbeitet. In diesem Zusammenhang möchten wir Sie auch auf die Informationen zur Verwendung von
        Cookies in dieser Datenschutzerklärung hinweisen.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Vertragsdaten (z.B. Vertragsgegenstand, Laufzeit,
                Kundenkategorie),
                Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten),
                Meta-/Kommunikationsdaten (z.B.
                Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Affiliate-Nachverfolgung.</li>
            <li><strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1 lit. a. DSGVO), Vertragserfüllung
                und
                vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b. DSGVO), Berechtigte Interessen (Art. 6 Abs. 1 S. 1
                lit. f.
                DSGVO).
            </li>
        </ul><p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>
        <ul class="m-elements">
            <li><strong>Amazon-Partnerprogramm:</strong> Amazon - Partnerprogramm - Amazon und das Amazon-Logo sind
                Warenzeichen
                von Amazon.com, Inc. oder eines seiner verbundenen Unternehmen. Dienstanbieter: Amazon Europe Core
                S.à.r.l., die
                Amazon EU S.à.r.l, die Amazon Services Europe S.à.r.l. und die Amazon Media EU S.à.r.l., alle vier
                ansässig in
                38, avenue John F. Kennedy, L-1855 Luxemburg, sowie Amazon Instant Video Germany GmbH, Domagkstr. 28,
                80807
                München (zusammen "Amazon Europe"), Mutterunternehmen: Amazon.com, Inc., 2021 Seventh Ave, Seattle,
                Washington
                98121, USA.; Website: <a rel="noopener noreferrer nofollow" href="https://www.amazon.de"
                                         target="_blank">https://www.amazon.de</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow"
                                         href="https://www.amazon.de/gp/help/customer/display.html?nodeId=201909010"
                                         target="_blank">https://www.amazon.de/gp/help/customer/display.html?nodeId=201909010</a>.
            </li>
        </ul><h2 id="m136">Präsenzen in sozialen Netzwerken (Social Media)</h2><p>Wir unterhalten Onlinepräsenzen
        innerhalb
        sozialer Netzwerke und verarbeiten in diesem Rahmen Daten der Nutzer, um mit den dort aktiven Nutzern zu
        kommunizieren oder um Informationen über uns anzubieten.</p><p>Wir weisen darauf hin, dass dabei Daten der
        Nutzer
        außerhalb des Raumes der Europäischen Union verarbeitet werden können. Hierdurch können sich für die Nutzer
        Risiken
        ergeben, weil so z.B. die Durchsetzung der Rechte der Nutzer erschwert werden könnte.</p><p>Ferner werden die
        Daten
        der Nutzer innerhalb sozialer Netzwerke im Regelfall für Marktforschungs- und Werbezwecke verarbeitet. So können
        z.B. anhand des Nutzungsverhaltens und sich daraus ergebender Interessen der Nutzer Nutzungsprofile erstellt
        werden.
        Die Nutzungsprofile können wiederum verwendet werden, um z.B. Werbeanzeigen innerhalb und außerhalb der
        Netzwerke zu
        schalten, die mutmaßlich den Interessen der Nutzer entsprechen. Zu diesen Zwecken werden im Regelfall Cookies
        auf
        den Rechnern der Nutzer gespeichert, in denen das Nutzungsverhalten und die Interessen der Nutzer gespeichert
        werden. Ferner können in den Nutzungsprofilen auch Daten unabhängig der von den Nutzern verwendeten Geräte
        gespeichert werden (insbesondere, wenn die Nutzer Mitglieder der jeweiligen Plattformen sind und bei diesen
        eingeloggt sind).</p><p>Für eine detaillierte Darstellung der jeweiligen Verarbeitungsformen und der
        Widerspruchsmöglichkeiten (Opt-Out) verweisen wir auf die Datenschutzerklärungen und Angaben der Betreiber der
        jeweiligen Netzwerke.</p><p>Auch im Fall von Auskunftsanfragen und der Geltendmachung von Betroffenenrechten
        weisen
        wir darauf hin, dass diese am effektivsten bei den Anbietern geltend gemacht werden können. Nur die Anbieter
        haben
        jeweils Zugriff auf die Daten der Nutzer und können direkt entsprechende Maßnahmen ergreifen und Auskünfte
        geben.
        Sollten Sie dennoch Hilfe benötigen, dann können Sie sich an uns wenden.</p><p><strong>Facebook</strong>: Wir
        sind
        gemeinsam mit Facebook Irland Ltd. für die Erhebung (jedoch nicht die weitere Verarbeitung) von Daten der
        Besucher
        unserer Facebook-Seite (sog. "Fanpage") verantwortlich. Zu diesen Daten gehören Informationen zu den Arten von
        Inhalten, die Nutzer sich ansehen oder mit denen sie interagieren, oder die von ihnen vorgenommenen Handlungen
        (siehe unter „Von dir und anderen getätigte und bereitgestellte Dinge“ in der Facebook-Datenrichtlinie: <a
                rel="noopener noreferrer nofollow" href="https://www.facebook.com/policy" target="_blank">https://www.facebook.com/policy</a>),
        sowie Informationen über die von den Nutzern genutzten Geräte (z. B. IP-Adressen, Betriebssystem, Browsertyp,
        Spracheinstellungen, Cookie-Daten; siehe unter „Geräteinformationen“ in der Facebook-Datenrichtlinie-erklärung:
        <a
                rel="noopener noreferrer nofollow" href="https://www.facebook.com/policy" target="_blank">https://www.facebook.com/policy</a>).
        Wie in der Facebook-Datenrichtlinie unter „Wie verwenden wir diese Informationen?“ erläutert, erhebt und
        verwendet
        Facebook Informationen auch, um Analysedienste, so genannte "Seiten-Insights", für Seitenbetreiber
        bereitzustellen,
        damit diese Erkenntnisse darüber erhalten, wie Personen mit ihren Seiten und mit den mit ihnen verbundenen
        Inhalten
        interagieren. Wir haben mit Facebook eine spezielle Vereinbarung abgeschlossen ("Informationen zu
        Seiten-Insights",
        <a rel="noopener noreferrer nofollow" href="https://www.facebook.com/legal/terms/page_controller_addendum"
           target="_blank">https://www.facebook.com/legal/terms/page_controller_addendum</a>), in der insbesondere
        geregelt
        wird, welche Sicherheitsmaßnahmen Facebook beachten muss und in der Facebook sich bereit erklärt hat die
        Betroffenenrechte zu erfüllen (d. h. Nutzer können z. B. Auskünfte oder Löschungsanfragen direkt an Facebook
        richten). Die Rechte der Nutzer (insbesondere auf Auskunft, Löschung, Widerspruch und Beschwerde bei zuständiger
        Aufsichtsbehörde), werden durch die Vereinbarungen mit Facebook nicht eingeschränkt. Weitere Hinweise finden
        sich in
        den "Informationen zu Seiten-Insights" (<a rel="noopener noreferrer nofollow"
                                                   href="https://www.facebook.com/legal/terms/information_about_page_insights_data"
                                                   target="_blank">https://www.facebook.com/legal/terms/information_about_page_insights_data</a>).
    </p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B.
                Eingaben in Onlineformularen), Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten,
                Zugriffszeiten),
                Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Kontaktanfragen und Kommunikation, Feedback (z.B. Sammeln von
                Feedback
                via Online-Formular), Marketing.
            </li>
            <li><strong>Rechtsgrundlagen:</strong> Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO).</li>
        </ul><p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>
        <ul class="m-elements">
            <li><strong>Instagram:</strong> Soziales Netzwerk; Dienstanbieter: Instagram Inc., 1601 Willow Road, Menlo
                Park, CA,
                94025, USA, Mutterunternehmen: Facebook, 1 Hacker Way, Menlo Park, CA 94025, USA; Website: <a
                        rel="noopener noreferrer nofollow" href="https://www.instagram.com" target="_blank">https://www.instagram.com</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow"
                                         href="https://instagram.com/about/legal/privacy"
                                         target="_blank">https://instagram.com/about/legal/privacy</a>.
            </li>
            <li><strong>Facebook:</strong> Soziales Netzwerk; Dienstanbieter: Facebook Ireland Ltd., 4 Grand Canal
                Square, Grand
                Canal Harbour, Dublin 2, Irland, Mutterunternehmen: Facebook, 1 Hacker Way, Menlo Park, CA 94025, USA;
                Website:
                <a rel="noopener noreferrer nofollow" href="https://www.facebook.com"
                   target="_blank">https://www.facebook.com</a>; Datenschutzerklärung: <a
                        rel="noopener noreferrer nofollow"
                        href="https://www.facebook.com/about/privacy"
                        target="_blank">https://www.facebook.com/about/privacy</a>;
                Widerspruchsmöglichkeit (Opt-Out): Einstellungen für Werbeanzeigen: <a
                        rel="noopener noreferrer nofollow"
                        href="https://www.facebook.com/adpreferences/ad_settings"
                        target="_blank">https://www.facebook.com/adpreferences/ad_settings</a>
                (Login bei Facebook ist erforderlich).
            </li>
            <li><strong>Twitter:</strong> Soziales Netzwerk; Dienstanbieter: Twitter International Company, One
                Cumberland
                Place, Fenian Street, Dublin 2 D02 AX07, Irland, Mutterunternehmen: Twitter Inc., 1355 Market Street,
                Suite 900,
                San Francisco, CA 94103, USA; Datenschutzerklärung: <a rel="noopener noreferrer nofollow"
                                                                       href="https://twitter.com/de/privacy"
                                                                       target="_blank">https://twitter.com/de/privacy</a>,
                (Einstellungen) <a rel="noopener noreferrer nofollow" href="https://twitter.com/personalization"
                                   target="_blank">https://twitter.com/personalization</a>.
            </li>
            <li><strong>YouTube:</strong> Soziales Netzwerk und Videoplattform; Dienstanbieter: Google Ireland Limited,
                Gordon
                House, Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway,
                Mountain View,
                CA 94043, USA; Datenschutzerklärung: <a rel="noopener noreferrer nofollow"
                                                        href="https://policies.google.com/privacy" target="_blank">https://policies.google.com/privacy</a>;
                Widerspruchsmöglichkeit (Opt-Out): <a rel="noopener noreferrer nofollow"
                                                      href="https://adssettings.google.com/authenticated"
                                                      target="_blank">https://adssettings.google.com/authenticated</a>.
            </li>
        </ul><h2 id="m328">Plugins und eingebettete Funktionen sowie Inhalte</h2><p>Wir binden in unser Onlineangebot
        Funktions-
        und Inhaltselemente ein, die von den Servern ihrer jeweiligen Anbieter (nachfolgend bezeichnet als
        "Drittanbieter”)
        bezogen werden. Dabei kann es sich zum Beispiel um Grafiken, Videos oder Stadtpläne handeln (nachfolgend
        einheitlich
        bezeichnet als "Inhalte”).</p><p>Die Einbindung setzt immer voraus, dass die Drittanbieter dieser Inhalte die
        IP-Adresse der Nutzer verarbeiten, da sie ohne die IP-Adresse die Inhalte nicht an deren Browser senden könnten.
        Die
        IP-Adresse ist damit für die Darstellung dieser Inhalte oder Funktionen erforderlich. Wir bemühen uns, nur
        solche
        Inhalte zu verwenden, deren jeweilige Anbieter die IP-Adresse lediglich zur Auslieferung der Inhalte verwenden.
        Drittanbieter können ferner sogenannte Pixel-Tags (unsichtbare Grafiken, auch als "Web Beacons" bezeichnet) für
        statistische oder Marketingzwecke verwenden. Durch die "Pixel-Tags" können Informationen, wie der
        Besucherverkehr
        auf den Seiten dieser Webseite, ausgewertet werden. Die pseudonymen Informationen können ferner in Cookies auf
        dem
        Gerät der Nutzer gespeichert werden und unter anderem technische Informationen zum Browser und zum
        Betriebssystem,
        zu verweisenden Webseiten, zur Besuchszeit sowie weitere Angaben zur Nutzung unseres Onlineangebotes enthalten
        als
        auch mit solchen Informationen aus anderen Quellen verbunden werden.</p><p><strong>Hinweise zu
            Rechtsgrundlagen:</strong> Sofern wir die Nutzer um deren Einwilligung in den Einsatz der Drittanbieter
        bitten,
        ist die Rechtsgrundlage der Verarbeitung von Daten die Einwilligung. Ansonsten werden die Daten der Nutzer auf
        Grundlage unserer berechtigten Interessen (d.h. Interesse an effizienten, wirtschaftlichen und
        empfängerfreundlichen
        Leistungen) verarbeitet. In diesem Zusammenhang möchten wir Sie auch auf die Informationen zur Verwendung von
        Cookies in dieser Datenschutzerklärung hinweisen.</p><p><strong>Instagram-Plugins und -Inhalte</strong>: Wir
        sind
        gemeinsam mit Facebook Irland Ltd. für die Erhebung oder den Erhalt im Rahmen einer Übermittlung (jedoch nicht
        die
        weitere Verarbeitung) von "Event-Daten", die Facebook mittels Funktionen von Instagram (z. B.
        Einbettungsfunktionen
        für Inhalte), die auf unserem Onlineangebot ausgeführt werden, erhebt oder im Rahmen einer Übermittlung zu
        folgenden
        Zwecken erhält, gemeinsam verantwortlich: a) Anzeige von Inhalten sowie Werbeinformationen, die den mutmaßlichen
        Interessen der Nutzer entsprechen; b) Zustellung kommerzieller und transaktionsbezogener Nachrichten (z. B.
        Ansprache von Nutzern via Facebook-Messenger); c) Verbesserung der Anzeigenauslieferung und Personalisierung von
        Funktionen und Inhalten (z. B. Verbesserung der Erkennung, welche Inhalte oder Werbeinformationen mutmaßlich den
        Interessen der Nutzer entsprechen). Wir haben mit Facebook eine spezielle Vereinbarung abgeschlossen ("Zusatz
        für
        Verantwortliche", <a rel="noopener noreferrer nofollow"
                             href="https://www.facebook.com/legal/controller_addendum"
                             target="_blank">https://www.facebook.com/legal/controller_addendum</a>), in der
        insbesondere
        geregelt wird, welche Sicherheitsmaßnahmen Facebook beachten muss (<a rel="noopener noreferrer nofollow"
                                                                              href="https://www.facebook.com/legal/terms/data_security_terms"
                                                                              target="_blank">https://www.facebook.com/legal/terms/data_security_terms</a>)
        und in der Facebook sich bereit erklärt hat die Betroffenenrechte zu erfüllen (d. h. Nutzer können z. B.
        Auskünfte
        oder Löschungsanfragen direkt an Facebook richten). Hinweis: Wenn Facebook uns Messwerte, Analysen und Berichte
        bereitstellt (die aggregiert sind, d. h. keine Angaben zu einzelnen Nutzern erhalten und für uns anonym sind),
        dann
        erfolgt diese Verarbeitung nicht im Rahmen der gemeinsamen Verantwortlichkeit, sondern auf Grundlage eines
        Auftragsverarbeitungsvertrages ("Datenverarbeitungsbedingungen ", <a rel="noopener noreferrer nofollow"
                                                                             href="https://www.facebook.com/legal/terms/dataprocessing"
                                                                             target="_blank">https://www.facebook.com/legal/terms/dataprocessing</a>)
        , der "Datensicherheitsbedingungen" (<a rel="noopener noreferrer nofollow"
                                                href="https://www.facebook.com/legal/terms/data_security_terms"
                                                target="_blank">https://www.facebook.com/legal/terms/data_security_terms</a>)
        sowie im Hinblick auf die Verarbeitung in den USA auf Grundlage von Standardvertragsklauseln
        ("Facebook-EU-Datenübermittlungszusatz, <a rel="noopener noreferrer nofollow"
                                                   href="https://www.facebook.com/legal/EU_data_transfer_addendum"
                                                   target="_blank">https://www.facebook.com/legal/EU_data_transfer_addendum</a>).
        Die Rechte der Nutzer (insbesondere auf Auskunft, Löschung, Widerspruch und Beschwerde bei zuständiger
        Aufsichtsbehörde), werden durch die Vereinbarungen mit Facebook nicht eingeschränkt.</p>
        <ul class="m-elements">
            <li><strong>Verarbeitete Datenarten:</strong> Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten,
                Zugriffszeiten), Meta-/Kommunikationsdaten (z.B. Geräte-Informationen, IP-Adressen), Bestandsdaten (z.B.
                Namen,
                Adressen), Kontaktdaten (z.B. E-Mail, Telefonnummern), Inhaltsdaten (z.B. Eingaben in Onlineformularen).
            </li>
            <li><strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher, Nutzer von Onlinediensten).</li>
            <li><strong>Zwecke der Verarbeitung:</strong> Bereitstellung unseres Onlineangebotes und
                Nutzerfreundlichkeit,
                Erbringung vertraglicher Leistungen und Kundenservice, Marketing, Profile mit nutzerbezogenen
                Informationen
                (Erstellen von Nutzerprofilen).
            </li>
            <li><strong>Rechtsgrundlagen:</strong> Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f. DSGVO),
                Einwilligung (Art.
                6 Abs. 1 S. 1 lit. a. DSGVO), Vertragserfüllung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b.
                DSGVO).
            </li>
        </ul><p><strong>Eingesetzte Dienste und Diensteanbieter:</strong></p>
        <ul class="m-elements">
            <li><strong>Font Awesome:</strong> Darstellung von Schriftarten und Symbolen; Dienstanbieter: Fonticons,
                Inc. ,6
                Porter Road Apartment 3R, Cambridge, MA 02140, USA; Website: <a rel="noopener noreferrer nofollow"
                                                                                href="https://fontawesome.com/"
                                                                                target="_blank">https://fontawesome.com/</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://fontawesome.com/privacy"
                                         target="_blank">https://fontawesome.com/privacy</a>.
            </li>
            <li><strong>Google Fonts:</strong> Wir binden die Schriftarten ("Google Fonts") des Anbieters Google ein,
                wobei die
                Daten der Nutzer allein zu Zwecken der Darstellung der Schriftarten im Browser der Nutzer verwendet
                werden. Die
                Einbindung erfolgt auf Grundlage unserer berechtigten Interessen an einer technisch sicheren,
                wartungsfreien und
                effizienten Nutzung von Schriftarten, deren einheitlicher Darstellung sowie unter Berücksichtigung
                möglicher
                lizenzrechtlicher Restriktionen für deren Einbindung. Dienstanbieter: Google Ireland Limited, Gordon
                House,
                Barrow Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway, Mountain
                View, CA
                94043, USA; Website: <a rel="noopener noreferrer nofollow" href="https://fonts.google.com/"
                                        target="_blank">https://fonts.google.com/</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://policies.google.com/privacy"
                                         target="_blank">https://policies.google.com/privacy</a>.
            </li>
            <li><strong>Google Maps:</strong> Wir binden die Landkarten des Dienstes “Google Maps” des Anbieters Google
                ein. Zu
                den verarbeiteten Daten können insbesondere IP-Adressen und Standortdaten der Nutzer gehören, die jedoch
                nicht
                ohne deren Einwilligung (im Regelfall im Rahmen der Einstellungen ihrer Mobilgeräte vollzogen), erhoben
                werden;
                Dienstanbieter: Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland,
                Mutterunternehmen: Google
                LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; Website: <a
                        rel="noopener noreferrer nofollow"
                        href="https://cloud.google.com/maps-platform"
                        target="_blank">https://cloud.google.com/maps-platform</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://policies.google.com/privacy"
                                         target="_blank">https://policies.google.com/privacy</a>;
                Widerspruchsmöglichkeit
                (Opt-Out): Opt-Out-Plugin: <a rel="noopener noreferrer nofollow"
                                              href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=de</a>,
                Einstellungen für die Darstellung von Werbeeinblendungen: <a rel="noopener noreferrer nofollow"
                                                                             href="https://adssettings.google.com/authenticated"
                                                                             target="_blank">https://adssettings.google.com/authenticated</a>.
            </li>
            <li><strong>Instagram-Plugins und -Inhalte:</strong> Instagram Plugins und -Inhalte - Hierzu können z.B.
                Inhalte wie
                Bilder, Videos oder Texte und Schaltflächen gehören, mit denen Nutzer Inhalte dieses Onlineangebotes
                innerhalb
                von Instagram teilen können. Dienstanbieter: <a rel="noopener noreferrer nofollow"
                                                                href="https://www.instagram.com" target="_blank">https://www.instagram.com</a>,
                Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA; Website: <a
                        rel="noopener noreferrer nofollow"
                        href="https://www.instagram.com"
                        target="_blank">https://www.instagram.com</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow"
                                         href="https://instagram.com/about/legal/privacy"
                                         target="_blank">https://instagram.com/about/legal/privacy</a>.
            </li>
            <li><strong>YouTube-Videos:</strong> Videoinhalte; Dienstanbieter: Google Ireland Limited, Gordon House,
                Barrow
                Street, Dublin 4, Irland, Mutterunternehmen: Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA
                94043,
                USA; Website: <a rel="noopener noreferrer nofollow" href="https://www.youtube.com" target="_blank">https://www.youtube.com</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://policies.google.com/privacy"
                                         target="_blank">https://policies.google.com/privacy</a>;
                Widerspruchsmöglichkeit
                (Opt-Out): Opt-Out-Plugin: <a rel="noopener noreferrer nofollow"
                                              href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank">https://tools.google.com/dlpage/gaoptout?hl=de</a>,
                Einstellungen für die Darstellung von Werbeeinblendungen: <a rel="noopener noreferrer nofollow"
                                                                             href="https://adssettings.google.com/authenticated"
                                                                             target="_blank">https://adssettings.google.com/authenticated</a>.
            </li>
            <li><strong>YouTube-Videos:</strong> Videoinhalte; YouTube-Videos werden über eine spezielle Domain
                (erkennbar an
                dem Bestandteil "youtube-nocookie") im sogenannten "Erweiterten Datenschutzmodus" eingebunden, wodurch
                keine
                Cookies zu Nutzeraktivitäten erhoben werden, um die Videowiedergabe zu personalisieren. Dennoch können
                Angaben
                zur Interaktion der Nutzer mit dem Video (z.B. Merken der letzten Wiedergabestelle), gespeichert werden;
                Dienstanbieter: Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland,
                Mutterunternehmen: Google
                LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; Website: <a
                        rel="noopener noreferrer nofollow"
                        href="https://www.youtube.com"
                        target="_blank">https://www.youtube.com</a>;
                Datenschutzerklärung: <a rel="noopener noreferrer nofollow" href="https://policies.google.com/privacy"
                                         target="_blank">https://policies.google.com/privacy</a>.
            </li>
        </ul><h2 id="m15">Änderung und Aktualisierung der Datenschutzerklärung</h2><p>Wir bitten Sie, sich regelmäßig
        über den
        Inhalt unserer Datenschutzerklärung zu informieren. Wir passen die Datenschutzerklärung an, sobald die
        Änderungen
        der von uns durchgeführten Datenverarbeitungen dies erforderlich machen. Wir informieren Sie, sobald durch die
        Änderungen eine Mitwirkungshandlung Ihrerseits (z.B. Einwilligung) oder eine sonstige individuelle
        Benachrichtigung
        erforderlich wird.</p><p>Sofern wir in dieser Datenschutzerklärung Adressen und Kontaktinformationen von
        Unternehmen
        und Organisationen angeben, bitten wir zu beachten, dass die Adressen sich über die Zeit ändern können und
        bitten
        die Angaben vor Kontaktaufnahme zu prüfen.</p><h2 id="m10">Rechte der betroffenen Personen</h2><p>Ihnen stehen
        als
        Betroffene nach der DSGVO verschiedene Rechte zu, die sich insbesondere aus Art. 15 bis 21 DSGVO ergeben:</p>
        <ul>
            <li><strong>Widerspruchsrecht: Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation
                    ergeben,
                    jederzeit gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Art.
                    6 Abs.
                    1 lit. e oder f DSGVO erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen
                    gestütztes Profiling. Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um
                    Direktwerbung zu
                    betreiben, haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung der Sie betreffenden
                    personenbezogenen Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling,
                    soweit
                    es mit solcher Direktwerbung in Verbindung steht.</strong></li>
            <li><strong>Widerrufsrecht bei Einwilligungen:</strong> Sie haben das Recht, erteilte Einwilligungen
                jederzeit zu
                widerrufen.
            </li>
            <li><strong>Auskunftsrecht:</strong> Sie haben das Recht, eine Bestätigung darüber zu verlangen, ob
                betreffende
                Daten verarbeitet werden und auf Auskunft über diese Daten sowie auf weitere Informationen und Kopie der
                Daten
                entsprechend den gesetzlichen Vorgaben.
            </li>
            <li><strong>Recht auf Berichtigung:</strong> Sie haben entsprechend den gesetzlichen Vorgaben das Recht, die
                Vervollständigung der Sie betreffenden Daten oder die Berichtigung der Sie betreffenden unrichtigen
                Daten zu
                verlangen.
            </li>
            <li><strong>Recht auf Löschung und Einschränkung der Verarbeitung:</strong> Sie haben nach Maßgabe der
                gesetzlichen
                Vorgaben das Recht, zu verlangen, dass Sie betreffende Daten unverzüglich gelöscht werden, bzw.
                alternativ nach
                Maßgabe der gesetzlichen Vorgaben eine Einschränkung der Verarbeitung der Daten zu verlangen.
            </li>
            <li><strong>Recht auf Datenübertragbarkeit:</strong> Sie haben das Recht, Sie betreffende Daten, die Sie uns
                bereitgestellt haben, nach Maßgabe der gesetzlichen Vorgaben in einem strukturierten, gängigen und
                maschinenlesbaren Format zu erhalten oder deren Übermittlung an einen anderen Verantwortlichen zu
                fordern.
            </li>
            <li><strong>Beschwerde bei Aufsichtsbehörde:</strong> Sie haben unbeschadet eines anderweitigen
                verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs das Recht auf Beschwerde bei einer
                Aufsichtsbehörde,
                insbesondere in dem Mitgliedstaat ihres gewöhnlichen Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts
                des
                mutmaßlichen Verstoßes, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden
                personenbezogenen
                Daten gegen die Vorgaben der DSGVO verstößt.
            </li>
        </ul><h2 id="m42">Begriffsdefinitionen</h2><p>In diesem Abschnitt erhalten Sie eine Übersicht über die in dieser
        Datenschutzerklärung verwendeten Begrifflichkeiten. Viele der Begriffe sind dem Gesetz entnommen und vor allem
        im
        Art. 4 DSGVO definiert. Die gesetzlichen Definitionen sind verbindlich. Die nachfolgenden Erläuterungen sollen
        dagegen vor allem dem Verständnis dienen. Die Begriffe sind alphabetisch sortiert.</p>
        <ul class="glossary">
            <li><strong>Affiliate-Nachverfolgung:</strong> Im Rahmen der Affiliate-Nachverfolgung werden Links, mit
                deren Hilfe
                die verlinkenden Webseiten Nutzer zu Webseiten mit Produkt- oder sonstigen Angeboten verweisen,
                protokolliert.
                Die Betreiber der jeweils verlinkenden Webseiten können eine Provision erhalten, wenn Nutzer diesen
                sogenannten
                Affiliate-Links folgen und anschließend die Angebote wahrnehmen (z.B. Waren kaufen oder Dienstleistungen
                in
                Anspruch nehmen). Hierzu ist es erforderlich, dass die Anbieter nachverfolgen können, ob Nutzer, die
                sich für
                bestimmte Angebote interessieren, diese anschließend auf die Veranlassung der Affiliate-Links
                wahrnehmen. Daher
                ist es für die Funktionsfähigkeit von Affiliate-Links erforderlich, dass sie um bestimmte Werte ergänzt
                werden,
                die ein Bestandteil des Links werden oder anderweitig, z.B. in einem Cookie, gespeichert werden. Zu den
                Werten
                gehören insbesondere die Ausgangswebseite (Referrer), der Zeitpunkt, eine Online-Kennung der Betreiber
                der
                Webseite, auf der sich der Affiliate-Link befand, eine Online-Kennung des jeweiligen Angebotes, eine
                Online-Kennung des Nutzers als auch nachverfolgungsspezifische Werte, wie, z.B. Werbemittel-ID,
                Partner-ID und
                Kategorisierungen
            </li>
            <li><strong>Content Delivery Network (CDN):</strong> Ein "Content Delivery Network" (CDN) ist ein Dienst,
                mit dessen
                Hilfe Inhalte eines Onlineangebotes, insbesondere große Mediendateien, wie Grafiken oder
                Programm-Skripte mit
                Hilfe regional verteilter und über das Internet verbundener Server, schneller und sicherer ausgeliefert
                werden
                können.
            </li>
            <li><strong>IP-Masking:</strong> Als "IP-Masking” wird eine Methode bezeichnet, bei der das letzte Oktett,
                d.h., die
                letzten beiden Zahlen einer IP-Adresse, gelöscht wird, damit die IP-Adresse nicht mehr der eindeutigen
                Identifizierung einer Person dienen kann. Daher ist das IP-Masking ein Mittel zur Pseudonymisierung von
                Verarbeitungsverfahren, insbesondere im Onlinemarketing
            </li>
            <li><strong>Konversionsmessung:</strong> Die Konversionsmessung (auch als "Besuchsaktionsauswertung"
                bezeichnet) ist
                ein Verfahren, mit dem die Wirksamkeit von Marketingmaßnahmen festgestellt werden kann. Dazu wird im
                Regelfall
                ein Cookie auf den Geräten der Nutzer innerhalb der Webseiten, auf denen die Marketingmaßnahmen
                erfolgen,
                gespeichert und dann erneut auf der Zielwebseite abgerufen. Beispielsweise können wir so nachvollziehen,
                ob die
                von uns auf anderen Webseiten geschalteten Anzeigen erfolgreich waren.
            </li>
            <li><strong>Personenbezogene Daten:</strong> "Personenbezogene Daten“ sind alle Informationen, die sich auf
                eine
                identifizierte oder identifizierbare natürliche Person (im Folgenden "betroffene Person“) beziehen; als
                identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels
                Zuordnung
                zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung (z.B.
                Cookie)
                oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen,
                physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser
                natürlichen Person sind.
            </li>
            <li><strong>Profile mit nutzerbezogenen Informationen:</strong> Die Verarbeitung von "Profilen mit
                nutzerbezogenen
                Informationen", bzw. kurz "Profilen" umfasst jede Art der automatisierten Verarbeitung personenbezogener
                Daten,
                die darin besteht, dass diese personenbezogenen Daten verwendet werden, um bestimmte persönliche
                Aspekte, die
                sich auf eine natürliche Person beziehen (je nach Art der Profilbildung können dazu unterschiedliche
                Informationen betreffend die Demographie, Verhalten und Interessen, wie z.B. die Interaktion mit
                Webseiten und
                deren Inhalten, etc.) zu analysieren, zu bewerten oder, um sie vorherzusagen (z.B. die Interessen an
                bestimmten
                Inhalten oder Produkten, das Klickverhalten auf einer Webseite oder den Aufenthaltsort). Zu Zwecken des
                Profilings werden häufig Cookies und Web-Beacons eingesetzt.
            </li>
            <li><strong>Reichweitenmessung:</strong> Die Reichweitenmessung (auch als Web Analytics bezeichnet) dient
                der
                Auswertung der Besucherströme eines Onlineangebotes und kann das Verhalten oder Interessen der Besucher
                an
                bestimmten Informationen, wie z.B. Inhalten von Webseiten, umfassen. Mit Hilfe der Reichweitenanalyse
                können
                Webseiteninhaber z.B. erkennen, zu welcher Zeit Besucher ihre Webseite besuchen und für welche Inhalte
                sie sich
                interessieren. Dadurch können sie z.B. die Inhalte der Webseite besser an die Bedürfnisse ihrer Besucher
                anpassen. Zu Zwecken der Reichweitenanalyse werden häufig pseudonyme Cookies und Web-Beacons eingesetzt,
                um
                wiederkehrende Besucher zu erkennen und so genauere Analysen zur Nutzung eines Onlineangebotes zu
                erhalten.
            </li>
            <li><strong>Verantwortlicher:</strong> Als "Verantwortlicher“ wird die natürliche oder juristische Person,
                Behörde,
                Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der
                Verarbeitung von personenbezogenen Daten entscheidet, bezeichnet.
            </li>
            <li><strong>Verarbeitung:</strong> "Verarbeitung" ist jeder mit oder ohne Hilfe automatisierter Verfahren
                ausgeführte Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten. Der
                Begriff
                reicht weit und umfasst praktisch jeden Umgang mit Daten, sei es das Erheben, das Auswerten, das
                Speichern, das
                Übermitteln oder das Löschen.
            </li>
        </ul>
        <p class="seal"><a href="https://datenschutz-generator.de/?l=de"
                           title="Rechtstext von Dr. Schwenke - für weitere Informationen bitte anklicken."
                           target="_blank"
                           rel="noopener noreferrer nofollow">Erstellt mit kostenlosem Datenschutz-Generator.de von Dr.
                Thomas Schwenke</a></p>
        <?php
    }
}