<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\product_comparison;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\ProductComparisonManager;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\service\TableOfContentsService;
use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;

#[AllowDynamicProperties]
class ProductComparisonPage extends AffiliateWebsitePage {
    public function buildPage()
    {
        global $CONFIG;

        $this->is_preview = false;
        $this->temp_data = null;
        $preview_token = RequestHelper::reqstr('preview');
        if($preview_token){
            $this->is_preview = true;
            $this->temp_data = (new TempDataService())->getByToken($preview_token)->temp_data;
        }

        $this->product_comparison_manager = new ProductComparisonManager();

        $this->product_comparison = $this->product_comparison_manager->getByPermalink($this->getLastPathSegment());

        $is_published = $this->product_comparison->status == Manager::STATUS_PUBLISH && (isset($this->product_comparison->is_already_published) && $this->product_comparison->is_already_published == true);

        if(!$is_published && !($preview_token && AuthHelper::isAdminPanelUser())){
            LogHelper::logToMinuteLog("HTTP", "[".implode('/', $this->path)."] Product '{$this->product_comparison->title}' ist nicht öffentlich", LogHelper::LOG_LEVEL_WARNING);
            UrlHelper::redirect('/nicht-gefunden', 307);
        }

        $this->robots = ($this->product_comparison->doindex?'index':'noindex').', '.($this->product_comparison->dofollow?'follow':'nofollow');
        $this->title = $this->product_comparison_manager->getMetaTitle($this->product_comparison);
        $this->description = $this->product_comparison_manager->getMetaDescription($this->product_comparison);

        $this->full_url = $this->product_comparison_manager->generatePermalink($this->product_comparison);

        $this->toc_service = new TableOfContentsService();
        $this->toc_items = $this->toc_service->generateTableOfContents($this->product_comparison->content);

        if($this->full_url !== ('/'.implode('/', $this->path))){
            UrlHelper::redirect($this->full_url, 301);
        }

        $vg_wort_pixel_service = new VgWortPixelService();
        $vg_wort_pixel = $vg_wort_pixel_service->getById($this->product_comparison->vg_wort_pixel_id);
        if($vg_wort_pixel) {
            $this->vg_wort_marke = $vg_wort_pixel->vg_wort_pixel;
        }

        if($this->is_preview){
            $this->robots = 'index, follow';
            if($this->temp_data->title){
                $this->product_comparison->title = $this->temp_data->title;
            }

            if($this->temp_data->content){
                $this->product_comparison->content = $this->temp_data->content;
            }

            if($this->temp_data->meta_title){
                $this->title = $this->temp_data->meta_title;
            }

            if($this->temp_data->meta_description){
                $this->description = $this->temp_data->meta_description;
            }
        }

        $this->product_comparison_ids = array_filter([
            $this->product_comparison->product_id_1,
            $this->product_comparison->product_id_2,
            $this->product_comparison->product_id_3
        ]);

        $vg_wort_pixel_service = new VgWortPixelService();
        $vg_wort_pixel = $vg_wort_pixel_service->getById($this->product_comparison->vg_wort_pixel_id);
        if($vg_wort_pixel) {
            $this->vg_wort_marke = $vg_wort_pixel->vg_wort_pixel;
        }

        $this->h1 = $this->product_comparison->title;

        ?>
        <div class="row">
            <div class="<?=$CONFIG->left_content_column_classes?>">
                <?php $this->toc_service->displayTableOfContents($this->toc_items) ?>

                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_LEFT, ProductComparisonManager::TYPE_PRODUCT_COMPARISON, $this->product_comparison->id))->display(); ?>
            </div>
            <div class="<?=$CONFIG->main_content_column_classes?>">
                <div class="main-content-container">
                    [display_products layout="table-x" produkte="<?=implode(',', $this->product_comparison_ids)?>" show_detail_button="true" show_meta="true"]

                    <div class="text-justify-container">
                        <?=ShortcodeHelper::doShortcode($this->product_comparison->content)?>
                    </div>

                    <?php
                    if($this->product_comparison->attached_faq_id){
                        (new FaqComponent((new FaqService())->getQuestionsFromId($this->product_comparison->attached_faq_id)))->display();
                    }
                    ?>

                    <?php (new AdContainer(AdManager::AD_TYPE_CONTENT_AFTER, ProductComparisonManager::TYPE_PRODUCT_COMPARISON, $this->product_comparison->id))->display(); ?>
                </div>
            </div>
            <div class="<?=$CONFIG->right_content_column_classes?>">
                <?php (new AdContainer(AdManager::AD_TYPE_SIDEBAR_RIGHT, ProductComparisonManager::TYPE_PRODUCT_COMPARISON, $this->product_comparison->id))->display(); ?>
            </div>
        </div>

        <?php (new AdContainer(AdManager::AD_TYPE_PAGE_BOTTOM, ProductComparisonManager::TYPE_PRODUCT_COMPARISON, $this->product_comparison->id))->display(); ?>

        <?php

        (new AdminEditButton('dashboard/produkte/vergleichsseiten/bearbeiten/' . $this->product_comparison->id))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            div.product-image {
                text-align: center;
            }

            div.product-image picture,
            div.product-image img{
                max-width: 100%;
                max-height: 300px;
                object-fit: contain;
                height: fit-content;
            }

            h1, h2, h3, h4, h5, h6, h7, h8, h9{
                margin-bottom: unset;
            }

            #product-sticky-buybox {
                position: fixed;
                z-index: 100;
                bottom: -100px;
                width: 100%;
                left: 0;
                right: 0;
                background-color: #fff;
                box-shadow: 0px -1px 10px #aaa;
                transition: all .5s ease-in-out;
            }

            #product-sticky-buybox-content {
                max-width: 1000px;
                max-height: 100px;
                margin: auto;
                display: table;
            }

            #product-sticky-buybox.visible {
                bottom: 0;
            }

            #product-sticky-buybox .image-container {
                display: table-cell;
                max-width: 100px;
                vertical-align: middle;
                padding: 10px;
            }

            #product-sticky-buybox .image-container .product-thumbnail-container img,
            #product-sticky-buybox .image-container .product-thumbnail-container picture img {
                height: 75px;
            }

            #product-sticky-buybox .title-container {
                max-width: 700px;
                display: table-cell;
                vertical-align: middle;
                padding: 10px;
            }

            #product-sticky-buybox .title-container p{
                margin-bottom: 0;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
            }

            #product-sticky-buybox .button-container {
                max-width: 200px;
                display: table-cell;
                vertical-align: middle;
                padding: 10px;
            }

            @media (max-width: 1000px){
                #product-sticky-buybox .title-container {
                    max-width: 550px;
                }
            }

            @media (max-width: 800px){
                #product-sticky-buybox .title-container {
                    display: none;
                }
            }

            @media (max-width: 400px){
                #product-sticky-buybox .image-container {
                    display: none;
                }
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            window.addEventListener('load', function (e) {
                let productDetailsContainer = document.getElementById('product-details');
                let productBottomBuybox = document.getElementById('product-bottom-buybox');

                let productStickyBuybox = document.getElementById('product-sticky-buybox');

                if(!productDetailsContainer || !productBottomBuybox || !productStickyBuybox){
                    return;
                }

                document.addEventListener("scroll", (event) => {
                    if(!isElementInViewport(productDetailsContainer) && !isElementInViewport(productBottomBuybox)){
                        productStickyBuybox.classList.add('visible');
                    }
                    else {
                        productStickyBuybox.classList.remove('visible');
                    }
                });
            });
        </script>
        <?php
    }
}