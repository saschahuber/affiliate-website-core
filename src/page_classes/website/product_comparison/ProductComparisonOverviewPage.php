<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website\product_comparison;

use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class ProductComparisonOverviewPage extends AffiliateWebsitePage {
    public function buildPage()
    {
        //Temp
        LogHelper::logToMinuteLog("HTTP", "[".implode('/', $this->path)."] Produktvergleichs-Pfad existiert nicht...", LogHelper::LOG_LEVEL_WARNING);
        UrlHelper::redirect('/nicht-gefunden', 307);

        $this->robots = 'index, follow';
        $this->title = "Produktvergleiche | {$CONFIG->app_name}";
        $this->description = "➤ Vergleiche Produkte!";
    }
}