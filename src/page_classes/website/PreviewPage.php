<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

#[AllowDynamicProperties]
class PreviewPage extends AffiliateWebsitePage{

    public function __construct($path){
        parent::__construct($path);

        $this->setTitle("Die Seite wurde nicht gefunden");
    }

    public function buildPage(){
        global $CONFIG;

        $default_separator = '
            [spacer height="128px"]
            <p style="text-align: center; font-size: 56px;">#### TRENNER ####</p>
            [spacer height="128px"]
            ';

        if(AuthHelper::isAdminPanelUser() || RequestHelper::reqstr('token') === $CONFIG->preview_token){
            $this->title = ShortcodeHelper::doShortcode(RequestHelper::reqstr('title'));

            $this->content_before = ShortcodeHelper::doShortcode(RequestHelper::reqstr('content_before'));

            $this->content_separator = ShortcodeHelper::doShortcode(RequestHelper::reqstr('content_separator', $default_separator));

            $this->content_after = ShortcodeHelper::doShortcode(RequestHelper::reqstr('content_after'));

            $this->sidebar = RequestHelper::reqbool("sidebar", false);
        }
        else{
            LogHelper::error("HTTP", 401, "Keine Berechtigung!", ERROR_EXIT);
        }

        ?>
        <h1><?=$this->title?></h1>

        <?=$this->content_before?>

        <?=$this->content_separator?>

        <?=$this->content_after?>
        <?php
    }
}