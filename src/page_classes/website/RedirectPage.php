<?php

namespace saschahuber\affiliatewebsitecore\page_classes\website;

use saschahuber\affiliatewebsitecore\page_classes\AffiliateWebsitePage;
use saschahuber\affiliatewebsitecore\service\RedirectGroupItemService;
use saschahuber\affiliatewebsitecore\service\RedirectGroupService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class RedirectPage extends AffiliateWebsitePage {
    public function buildPage()
    {
        if(count($this->path) > 1) {

            $alias = $this->path[1];

            $redirect_group_service = new RedirectGroupService();
            $redirect_group_item_service = new RedirectGroupItemService();

            $redirect_group = $redirect_group_service->getByAlias($alias);

            if ($redirect_group && $redirect_group->is_active) {
                $redirect_group_items = $redirect_group_item_service->getActiveGroupItems($redirect_group->id);

                $selected_group_item = ArrayHelper::rand($redirect_group_items);

                $redirect_group_service->trackHit($redirect_group->id);
                $redirect_group_item_service->trackHit($selected_group_item->id);

                $params = $_GET;

                $url_parts = parse_url($selected_group_item->target_url);

                parse_str(ArrayHelper::getArrayValue($url_parts, 'query', ''), $query_params);

                $query_params = array_merge($query_params, $params);

                $target_url = $url_parts['scheme'] . '://' . $url_parts['host'] . $url_parts['path'] . (count($query_params) ? '?' . http_build_query($query_params) : '');

                UrlHelper::redirect($target_url, 302);
            }
        }

        UrlHelper::redirect("/", 302);
    }
}