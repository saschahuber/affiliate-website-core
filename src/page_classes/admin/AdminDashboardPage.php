<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin;

use saschahuber\affiliatewebsitecore\page_classes\admin\AffiliateDashboardPage;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\service\DashboardMenuService;

class AdminDashboardPage extends AffiliateDashboardPage {
    public function buildPage(){
        $this->title = "Dashboard";

        // Nur für eingeloggte Nutzer
        if (!AuthHelper::isLoggedIn() || !AuthHelper::isAdminPanelUser()) {
            // Zum Login-Formular weiterleiten
            AuthHelper::logout("/login");
        }

        $menu_items = (new DashboardMenuService())->getDashboardMenuItems();

        $allowed_urls = array(
            'settings',
            #'packages',
            'subscription'
        );

        foreach($menu_items as $menu_item){
            $url = explode('/', trim($menu_item['url'], '/'))[1];
            $allowed_urls[] = $url;
        }

        if(count($this->path) > 1 && in_array($this->path[1], $allowed_urls)){
            $subdir = $this->path[1];
        }
        else{
            UrlHelper::redirect($menu_items[0]['url']);
        }
    }
}