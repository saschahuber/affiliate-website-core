<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\AsyncSearchComponent;

#[AllowDynamicProperties]
class AdminProductFeedImportDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produkte importieren");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $feed_manager = new ProductFeedManager();

        $item_id = null;
        if(count($this->path) > 4){
            $item_id = $this->path[4];

            if($item_id !== "amazon"){
                $item_id = intval($item_id);
            }
        }

        $this->is_amazon_import = false;
        if($item_id == "amazon") {
            $this->is_amazon_import = true;
        }
        else{
            $this->feed_item = $feed_manager->getById($item_id);
        }

        if($this->is_amazon_import) {
            (new AsyncSearchComponent('AmazonProductSearchHandler', [], 'Produkt suchen'))->display();
        }
        else {
            (new AsyncSearchComponent('FeedProductSearchHandler', ['feed_id' => $this->feed_item->id], 'Produkt im Feed "'.$this->feed_item->title.'" suchen'))->display();
        }
    }

    public function getCssCode()
    {
        ?>
        <style>
            #search-container{
                width: 100%;
            }

            img{
                object-fit: contain;
            }
        </style>
        <?php
    }
}