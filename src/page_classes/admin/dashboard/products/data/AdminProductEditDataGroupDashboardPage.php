<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\data;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;

class AdminProductEditDataGroupDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produktdaten bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $product_data_field_group_manager = new ProductDataFieldGroupManager();
        $product_data_field_manager = new ProductDataFieldManager();

        $item_id = null;
        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }

        $product_data_group = $product_data_field_group_manager->getById($item_id);

        (new LinkButton("/dashboard/produkte/daten", "Zurück zu den Produkt-Daten", "fas fa-arrow-left", false))->display();

        $allowed_taxonomies = [null => "-- Keine Zuweisung --"];
        foreach($product_manager->getTaxonomies() as $taxonomy){
            $allowed_taxonomies[$taxonomy->id] = $taxonomy->title;
        }

        $input_rows = [
            [new HiddenInput("id", $item_id)],
            [new TextInput("group_name", "Name der Gruppe", null, $product_data_group?$product_data_group->group_name:null)],
            [new Select("product__taxonomy_id", "Produkt-Kategorie", $allowed_taxonomies, $product_data_group?$product_data_group->product__taxonomy_id:null, false, false, $item_id)]
        ];
        (new SimpleFabSaveForm($input_rows, 'product/datagroup/save'))->display();

        if($item_id) {
            BreakComponent::break();

            $fields = $product_data_field_manager->getDataFields($item_id);

            $column_labels = [
                'Feldname',
                'In Filter anzeigen',
                'Beschreibung',
                'Sortierung',
                'Typ',
                'Werte',
                'Präfix',
                'Suffix',
                'Aktionen',
            ];

            $rows = [];

            foreach ($fields as $item) {
                $cells = [
                    $item->field_name,
                    new EditableToggle('product__data_field', 'show_in_filter', $item->id, $item->show_in_filter, true),
                    $item->field_description,
                    $item->field_order,
                    $item->field_type,
                    $item->field_values,
                    $item->field_prefix,
                    $item->field_suffix,
                    new FloatContainer([
                        new LinkButton("/dashboard/produkte/daten/feld-bearbeiten/{$item->id}", "Bearbeiten", 'fas fa-edit', false, true)
                        #new LinkButton("/dashboard/produkte/daten/bearbeiten/{$group->id}", "Bearbeiten", 'fas fa-edit', false, true)
                    ])
                ];
                $rows[] = new TableRow($cells);
            }

            (new ElevatedCard([
                new Text("Datenfelder dieser Gruppe", "label"),
                new BreakComponent(),
                new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']),
                new BreakComponent(),
                new LinkButton("/dashboard/produkte/daten/feld-bearbeiten?group_id={$product_data_group->id}", "Neues Feld hinzufügen", "fas fa-plus", false, true)
            ]))->display();
        }
    }
}