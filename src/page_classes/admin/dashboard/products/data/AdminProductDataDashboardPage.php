<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\data;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;

class AdminProductDataDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produktdaten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $product_data_group_manager = new ProductDataFieldGroupManager();
        $product_data_field_group_manager = new ProductDataFieldGroupManager();
        $product_data_field_manager = new ProductDataFieldManager();

        $groups = $product_data_field_group_manager->getDataFieldGroups();

        $fields = $product_data_field_manager->getDataFields();

        (new LinkButton("/dashboard/produkte", "Zurück zu den Produkten", "fas fa-arrow-left", false))->display();

        (LinkButton::fab("/dashboard/produkte/daten/bearbeiten", "fas fa-plus"))->display();

        $column_labels = [
            'Gruppenname',
            'Kategorie',
            'Aktionen',
        ];

        $rows = [];

        $allowed_taxonomies = [];
        foreach($product_manager->getTaxonomies() as $taxonomy){
            $allowed_taxonomies[$taxonomy->id] = $taxonomy->title;
        }

        foreach($groups as $group) {
            $cells = [
                new Text($group->group_name),
                #new EditableSelect('product__data_field_group', 'product__taxonomy_id', $group->id, $group->product__taxonomy_id, $allowed_taxonomies),
                $group->taxonomy_name,
                new FloatContainer([
                    new LinkButton("/dashboard/produkte/daten/bearbeiten/{$group->id}", "Bearbeiten", 'fas fa-edit', false, true)
                    #new LinkButton("/dashboard/produkte/daten/bearbeiten/{$group->id}", "Bearbeiten", 'fas fa-edit', false, true)
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            div#module-output > div {
                display: inline-block;
                width: 30%;
                background-color: #f2f2f2;
                border-radius: 6px;
                overflow: hidden;
                vertical-align: top;
                font-size: 13pt;
            }

            div#module-output > div + div {
                margin-left: 1%;
                width: 69%;
            }

            div#module-output > div > strong {
                background-color: #ffb71e;
                color: #b50000;
                display: block;
                text-align: center;
                line-height: 18pt;
            }

            div#module-output > div > ul,
            div#module-output > div > p {
                margin: 20px;
                list-style-type: none;
            }

            div#module-output > div > ul > li {
                padding: 4px 0;
                border-bottom: dashed 1px #ccc;
            }

            div#module-output > div > ul > li:last-child {
                border-bottom: none;
            }

            div#module-output > div tr > td {
                vertical-align: top;
                border-bottom: dashed 1px #ccc;
                border-right: dashed 1px #ccc;
            }

            div#module-output > div tr > td > button + div {
                display: none;
            }

            div#module-output > div tr > td hr {
                background-color: #000;
                height: 1px;
            }

            div#module-output > div tr > td:first-child + td {
                white-space: nowrap;
            }

            div#module-output > div tr > td:last-child {
                border-right: none;
            }

            div#module-output > div tr:last-child > td {
                border-bottom: none;
            }

            div#module-output > div tr > td:first-child {
                width: 120px;
            }

            div#module-output > div tr > td:first-child > img {
                object-fit: cover;
                object-position: 0% 0%;
                width: 120px;
                height: 80px;
            }

            div#module-output > div tr.entry > td:first-child > button {
                display: block;
                width: 120px;
                position: relative;
                top: -20px;
                margin-bottom: -20px;
                visibility: hidden;
            }

            div#module-output > div tr.entry > td:first-child:hover > button {
                visibility: visible;
            }

            div#module-output > div tr.entry > td:first-child > button + button {
                top: -45px;
            }

            div#module-output > div tr > td p.postpone {
                border-top: dashed 1px #ccc;
                margin-top: 4px;
                padding-top: 4px;
            }

            div#module-output > div tr > td p.postpone > input[type="number"] {
                width: 50px;
            }

            div#module-output > div tr > td p.postpone > button {
                display: block;
                margin-top: 4px;
                width: 100%;
            }

            div#module-output > div tr > td.agent-filter > a {
                opacity: 0.8;
            }

            div#module-output > div tr > td.agent-filter > a.selected,
            div#module-output > div tr > td.agent-filter > a:hover {
                opacity: 1.0;
                background-color: #325386;
            }
        </style>
        <?php
    }
}