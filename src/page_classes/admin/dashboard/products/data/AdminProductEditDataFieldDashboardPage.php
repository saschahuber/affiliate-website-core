<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\data;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TagInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminProductEditDataFieldDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produktdaten bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $item_id = null;
        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $product_data_field_group_manager = new ProductDataFieldGroupManager();
        $product_data_field_manager = new ProductDataFieldManager();

        $product_data_field = $product_data_field_manager->getById($item_id);

        $group_id = $product_data_field?$product_data_field->group_id:RequestHelper::reqint('group_id');
        (new LinkButton("/dashboard/produkte/daten/bearbeiten/" . $group_id, "Zurück zur Daten-Gruppe", "fas fa-arrow-left", false))->display();

        $allowed_groups = [];
        foreach($product_data_field_group_manager->getDataFieldGroups() as $groups){
            $allowed_groups[$groups->id] = $groups->group_name;
        }

        $input_rows = [
            [new HiddenInput("id", $item_id)],
            [new TextInput("field_name", "Name des Feldes", null, $product_data_field?$product_data_field->field_name:null)],
            [new TextInput("field_description", "Beschreibung des Feldes", null, $product_data_field?$product_data_field->field_description:null)],
            [new Select("field_type", "Feld-Typ", ProductDataFieldManager::ALLOWED_FIELD_TYPES, $product_data_field?$product_data_field->field_type:null)],
            [new TagInput("field_values", "Erlaubte Werte (Komma-separiert) (Nur bei Feld-Typ 'Auswahl')", null, $product_data_field?$product_data_field->field_values:null)],
            [new TextInput("field_prefix", "Präfix", null, $product_data_field?$product_data_field->field_prefix:null)],
            [new TextInput("field_suffix", "Suffix", null, $product_data_field?$product_data_field->field_suffix:null)],
            [$item_id
                ?[new HiddenInput('group_id', $group_id),
                    new TextInput('group_name', 'Gruppe', null, $allowed_groups[$product_data_field->group_id], true)]
                :(new Select("group_id", "Daten-Gruppe", $allowed_groups, $product_data_field?$product_data_field->group_id:$group_id, false, false, $product_data_field!==null))
            ]
        ];

        (new SimpleFabSaveForm($input_rows, 'product/datafield/save'))->display();
    }
}