<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\data;

use saschahuber\affiliatewebsitecore\component\AiProductDataLoaderButton;
use saschahuber\affiliatewebsitecore\component\form\ProductLinkTable;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductCustomDataFieldManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableNumberInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;

class AdminProductEditDataDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produktdaten bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        $item_id = null;
        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }

        $product = $product_manager->getById($item_id);

        (new LinkButton("/dashboard/produkte/bearbeiten/$item_id", "Zurück zum Produkt '{$product->title}'", "fas fa-arrow-left", false))->display();

        $product_custom_data_field_manager = new ProductCustomDataFieldManager();
        $product_data_field_group_manager = new ProductDataFieldGroupManager();
        $product_data_field_manager = new ProductDataFieldManager();
        $product_link_manager = new ProductLinkManager();

        $taxonomy_ids = [];
        foreach($product->taxonomies as $taxonomy){
            $taxonomy_ids[] = $taxonomy->id;
        }

        $groups = [];
        $taxonomy_groups = [];
        foreach($product_data_field_group_manager->getDataFieldGroups($taxonomy_ids) as $group){
            $groups[$group->product__data_field_group_id] = $group;

            if(!array_key_exists($group->product__data_field_group_id, $taxonomy_groups)){
                $taxonomy_groups[$group->product__data_field_group_id] = [];
            }
            $fields = $product_data_field_manager->getDataFields($group->id);
            foreach($fields as $field) {
                $field->value = null;
                $taxonomy_groups[$group->id][$field->id] = $field;
            }
        }

        $data_fields = $product_data_field_manager->getProductDataFields($product);
        foreach($data_fields as $group_id => $fields) {
            if(!array_key_exists($group_id, $taxonomy_groups)){
                $taxonomy_groups[$group_id] = [];
            }
            foreach($fields as $field) {
                $taxonomy_groups[$group_id][$field->product__data_field_id] = $field;
            }
        }

        $input_rows = [
            [new HiddenInput("id", $item_id)],
        ];

        $column_labels = ['Gruppe', 'Feld', 'Beschreibung', 'Typ', 'Inhalt'];

        $data_field_mappings = [];

        $data_field_group_rows = [];
        foreach($taxonomy_groups as $group_id => $fields){
            $group = $groups[$group_id];
            foreach($fields as $product__data_field_id => $field){
                $input = new TextInput('test', 'test');

                $input_name = 'data_field__'.$group_id.'__'.$field->product__data_field_id;

                $data_field_mappings[$field->product__data_field_id] = [
                    'input_name' => $input_name,
                    'field_type' => $field->field_type,
                    'field_values' => $field->field_values
                ];

                switch($field->field_type){
                    case "enum":
                        $allowed_values = [];
                        foreach(array_unique(array_merge([$field->value], array_map('trim', explode(',', $field->field_values)))) as $value){
                            $allowed_values[$value] = $value;
                        }

                        #$input = new Select($input_name, $field->field_name, $allowed_values, $field->value);
                        $input = new TextInput($input_name, $field->field_name, null, $field->value);
                        break;
                    case "boolean":
                        $allowed_values = [
                            null => 'Kein Wert',
                            '0' => 'Nein',
                            '1' => 'Ja'
                        ];
                        $input = new Select($input_name, $field->field_name, $allowed_values, $field->value);
                        break;
                    case "string":
                    case "float":
                    default:
                        $input = new TextInput($input_name, $field->field_name, '', $field->value);
                        break;
                }

                $cells = [
                    $group->group_name,
                    $field->field_name,
                    $field->field_description,
                    $field->field_type,
                    new FloatContainer([
                        $field->field_prefix,
                        $input,
                        $field->field_suffix
                    ])
                ];
                $data_field_group_rows[] = new TableRow($cells);
            }
        }

        $input_rows = [
            [new HiddenInput("product_id", $item_id)],
            [new Text("Felder", 'label')],
            [new Table($column_labels, $data_field_group_rows, ['table-bordered', 'table-hover', 'centered'])],
            [new FloatContainer([
                new AiProductDataLoaderButton($product->id, $data_field_mappings, ChatGenerationTemplate::MODEL_CHAT_GPT_16K),
                #new AiProductDataLoaderButton($product->id, $data_field_mappings, ChatGenerationTemplate::MODEL_CHAT_GPT_4),
                new AiProductDataLoaderButton($product->id, $data_field_mappings, ChatGenerationTemplate::MODEL_CHAT_GPT_4_TURBO),
                new AiProductDataLoaderButton($product->id, $data_field_mappings, ChatGenerationTemplate::MODEL_CHAT_GPT_4_O),
                new AiProductDataLoaderButton($product->id, $data_field_mappings, ChatGenerationTemplate::MODEL_CHAT_GPT_O3_MINI),
            ])]
        ];

        (new ElevatedCard([new SimpleFabSaveForm($input_rows, "product/data/save")]))->display();

        BreakComponent::break();

        $column_labels = ['Reihenfolge', 'Name', 'Wert', 'Aktionen'];

        $data_field_mappings = [];

        $custom_data_field_rows = [];
        foreach($product_custom_data_field_manager->getDataFields($product) as $field){
            $cells = [
                new EditableNumberInput('product__custom_data_field', 'sort_order', $field->id, $field->sort_order?:0),
                new EditableTextInput('product__custom_data_field', 'name', $field->id, $field->name),
                new EditableTextInput('product__custom_data_field', 'value', $field->id, $field->value),
                new DeleteButton('product__custom_data_field', $field->id, 2)
            ];
            $custom_data_field_rows[] = new TableRow($cells);
        }

        $input_config = [
            'sort_order' => ['type' => 'number', 'label' => 'Sortierung'],
            'name' => ['type' => 'text', 'label' => 'Name'],
            'value' => ['type' => 'text', 'label' => 'Wert'],
            'product_id' => ['type' => 'hidden', 'value' => $product->id]
        ];
        $insert_entry_button = new InsertEntryButton('product__custom_data_field', $input_config, "Neue Eigenschaft");

        $input_rows = [
            [new Text("Benutzerdefinierte Felder", 'label')],
            [new Table($column_labels, $custom_data_field_rows, ['table-bordered', 'table-hover', 'centered'])],
            [new FloatContainer([$insert_entry_button])]
        ];

        (new ElevatedCard([$input_rows]))->display();

        BreakComponent::break();

        $product_links = [];
        if($product) {
            $product_links = $product_link_manager->getAllByProductId($product->id, false);
        }

        (new ProductLinkTable($product, $product_links, false))->display();
    }
}