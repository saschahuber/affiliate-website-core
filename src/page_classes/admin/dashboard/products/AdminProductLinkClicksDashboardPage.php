<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ProductFeedImportService;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\service\ApplicationAnalyticsService;

class AdminProductLinkClicksDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Link-Klicks analysieren");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $product_id = RequestHelper::reqint('product_id');
        $this->start_date = RequestHelper::reqint('start_date', (new \DateTime())->modify('-30 Days')->format('Y-m-d H:i:s'));
        $this->end_date = RequestHelper::reqint('end_date');

        $application_analytics_service = new ApplicationAnalyticsService();

        $log_items = $application_analytics_service->findLogs('product_click', null, null, null,
            null, null, null, null, null, null, null,
            null, null, $this->start_date, $this->end_date);

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        $this->clicks = [];
        foreach($log_items as $log_item){
            $data = json_decode($log_item->event_data, true);
            $log_product_id = ArrayHelper::getArrayValue($data, 'product_id');
            if($product_id && $log_product_id !== $product_id){
                continue;
            }
            $product = $product_manager->getById($log_product_id);
            $log_item->title = $product->title;
            $this->clicks[] = $log_item;
        }

        $column_labels = [
            'Zeit',
            'Produkt',
            'Identifier'
        ];

        $rows = [];
        foreach($this->clicks as $click){
            $identifier_buttons = [];
            if($click->session_id && strlen($click->session_id)){
                $identifier_buttons[] = new LinkButton('/dashboard/system/analytics/details?session_id='.$click->session_id, 'Session', null, false, true);
            }
            if($click->fingerprint && strlen($click->fingerprint)){
                $identifier_buttons[] = new LinkButton('/dashboard/system/analytics/details?fingerprint='.$click->fingerprint, 'Fingerprint', null, false, true);
            }
            if($click->user_ip && strlen($click->user_ip)){
                $identifier_buttons[] = new LinkButton('/dashboard/system/analytics/details?ip='.$click->user_ip, 'IP: '.$click->user_ip, null, false, true);
            }

            $cells = [
                $click->log_time,
                $click->title,
                new FloatContainer($identifier_buttons)
            ];
            $rows[] = new TableRow($cells);
        }

        (new Card([
            new Text('Link-Klicks analysieren', 'h1'),
            new Form([
                [
                    new DateTimePicker('start_date', 'Start', null, $this->start_date),
                    new DateTimePicker('end_date', 'Ende', null, $this->end_date),
                    new SubmitButton('Filtern')
                ]
            ], '/dashboard/produkte/link-clicks', Form::METHOD_GET),
            new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered'])
        ]))->display();
    }
}