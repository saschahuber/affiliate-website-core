<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ProductFeedImportService;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class AdminProductFeedsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Feed-Übersicht");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $feed_manager = new ProductFeedManager();

        $items = $feed_manager->getAll();

        $column_labels = [
            'Titel',
            'Aktiviert?',
            'Feed-Template',
            'Cron-Regel',
            'Letztes Update',
            'Aktionen',
        ];

        $feed_templates = ProductFeedImportService::getFeedTemplates();

        $rows = [];

        # Amazon hardcoden
        $cells = [
            "Amazon",
            new Toggle(null, null, true, true, true),
            "Amazon",
            "*/10 * * * *",
            "",
            new FloatContainer([
                new LinkButton("/dashboard/produkte/feeds/import/amazon", "Importieren", "fas fa-search")
            ])
        ];

        $rows[] = new TableRow($cells);

        foreach($items as $item){
            $cells = [
                $item->title,
                new EditableToggle('product__feed', 'is_active', $item->id, $item->is_active, true),
                ArrayHelper::getArrayValue($feed_templates, $item->template, 'Kein Feed-Template'),
                new EditableTextInput('product__feed', 'import_cron_rule', $item->id, '0 * * * *'),
                DateHelper::displayDateTime($item->last_update),
                new FloatContainer([
                    new LinkButton("/dashboard/produkte/feeds/bearbeiten/".$item->id, "Bearbeiten", "fas fa-pen"),
                    new LinkButton("/dashboard/produkte/feeds/import/".$item->id, "Importieren", "fas fa-search"),
                    new JsButton('<i class="fas fa-download"></i> Feed aktualisieren', "updateProductFeed({$item->id}, this)", ['btn-lg']),
                    new LinkButton($item->url, "Feed-Datei herunterladen", "fas fa-download"),
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (LinkButton::fab("/dashboard/produkte/feeds/bearbeiten", 'fas fa-plus'))->display();
    }

    public function getJsCode(){
        ?>
        <script>
            function updateProductFeed(feedId, button){
                let originalButtonContent = button.innerHTML;

                button.innerHTML = '<i class="fas fa-spinner fa-spin"></i> Aktualisiere...';

                doApiCallWithResponseCode('product_feed/update_feed', {feed_id: feedId}, function (responseCode, response) {
                    if(responseCode == 200) {
                        let data = JSON.parse(response);
                        let productCount = data!==null ? data.length : 0;

                        if(productCount > 0) {
                            displayStatusToast('Aktualisierung erfolgreich: ' + productCount + ' Produkte!', 'var(--primary_color)', '#fff');
                        }
                        else {
                            displayStatusToast('Aktualisieren fehlgeschlagen - Keine Produkte!', '#f00', '#fff');
                        }
                    }
                    else {
                        displayStatusToast('Aktualisieren fehlgeschlagen!', '#f00', '#fff');
                    }
                    button.innerHTML = originalButtonContent;
                });
            }
        </script>
        <?php
    }
}