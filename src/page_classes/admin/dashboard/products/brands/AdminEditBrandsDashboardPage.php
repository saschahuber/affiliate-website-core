<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\brands;

use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\AdditionalSettings;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditBrandsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Hersteller bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        $brand_manager = new BrandManager();
        $image_manager = new ImageManager();

        $item_id = null;
        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }

        $item = $brand_manager->getById($item_id);

        (new LinkButton("/dashboard/produkte/hersteller", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel der Kategorie', isset($item->title) ? $item->title : null);
        $title_input->display();

        TextInput::required("permalink", 'Permalink', 'Permalink der Kategorie', isset($item->permalink) ? $item->permalink : null)->display();

        (new TextInput("h1", 'H1 (Optional - Default: Titel)', 'H1 (Optional - Default: Titel)', isset($item->h1)?$item->h1:null))->display();
        (new TextInput("subtitle", 'Unterüberschrit', 'Unterüberschrit', isset($item->subtitle)?$item->subtitle:null))->display();

        ?>

        <div class="editor">
            <strong>Kurzer Inhalt</strong>
            <textarea id="short_content" name="short_content"><?= $item?$item->short_content:null ?></textarea>

            <?php

            $editor_before = new CkEditorTextArea("content", "Inhalt", null, isset($item->content)?$item->content:null, 300);
            $editor_before->display();

            if($item){
                (new ContentPreviewButton($CONFIG->app_domain . $brand_manager->generatePermalink($item), [
                    'title' => [
                        'id' => $title_input->getId(),
                        'tinymce' => false
                    ],
                    'content_before_products' => [
                        'id' => $editor_before->getId(),
                        'ckeditor' => true
                    ],
                    'meta_title' => [
                        'id' => 'component_meta_title_input',
                        'tinymce' => false
                    ],
                    'meta_description' => [
                        'id' => 'component_meta_description_input',
                        'tinymce' => false
                    ]
                ]))->display();
            }

            (new TextInput('all_products_headline', 'Titel vor allen Produkten', null, isset($item->all_products_headline) ? $item->all_products_headline : null))->display();

            ?>
        </div>

        <?php
        (new AdditionalSettings($item))->display();
        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();
        $form_content = ob_get_clean();
        (new SimpleFabSaveForm($form_content, 'brand/save'))->display();

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(BrandManager::TYPE_BRAND, $item_id)->display();
        }

        ?>

        <div id="search_products_popup_container">
            <div id="search_products_popup">
                <i onclick="hideProductBulkAddPopup()" style="font-size: 32px;"
                   class="fas fa-times-circle close-button"></i>
                <div>
                    <p>
                        <label>Produkte suchen:</label>
                        <input id="search_products_input" name="search_products_keyword" placeholder="<?=$item?$item->title:null?>">
                        <input name="brand_id" type="hidden" value="<?=$item?$item->id:null?>">
                    </p>
                    <div id="brand_product_add_list"></div>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" onclick="showProductBulkAddPopup()"><i class="fas fa-plus-circle"></i> Produkte hinzufügen</button>
        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            /* editor view */

            div#module-output > form > div.editor {
                display: flex;
                flex-wrap: wrap;
                background-color: var(--light_grey2);
                padding: 10px;
                margin-top: 10px;
            }

            div#module-output > button#generate-preview{
                position: relative;
                font-size: 14px;
                top: unset;
                right: unset;
            }

            div#module-output > form > div.editor > textarea {
                width: 100%;
                resize: vertical;
                min-height: 420px;
                padding: 10px;
            }

            div#module-output > form > div.editor > div {
                width: 100%;
                margin-left: 10px;
                background-color: #fff;
                border: solid 1px #c1c1c1;
                padding: 10px;
            }

            div#module-output > form > div.editor > div > span.date {
                float: right;
            }

            div#module-output > form > div.editor > div > h1 + img {
                float: left;
                margin-right: 10px;
                margin-bottom: 10px;
                max-width: 100%;
            }

            div#module-output > form > div.editor > div > h1,
            div#preview > h1, div#preview > h2, div#preview > h3, div#preview > h4, div#preview > h5 {
                font-family: DM Sans Thick;
                text-transform: uppercase;
                color: inherit;
            }

            div#module-output > form > div.editor > div > h1,
            div#preview > h1 {
                font-size: 30pt;
                line-height: 36pt;
                margin-bottom: 40px;
            }

            div#preview > h2 {
                font-size: 24pt;
                line-height: 30pt;
                margin-bottom: 30px;
                margin: 20px 0 10px 0;
            }

            div#preview > h3 {
                line-height: 22pt;
                margin-bottom: 20px;
                margin: 20px 0 10px 0;
                text-align: left;
                font-size: 14pt;
            }

            div#preview > h4 {
                font-size: 16pt;
                line-height: 20pt;
                margin-bottom: 10px;
            }

            div#preview > h5 {
                font-size: 15pt;
                line-height: 19pt;
                margin-bottom: 10px;
            }

            div#preview > p {
                margin: 12px 0;
            }

            div#preview > h3:before {
                font-family: Font Awesome\ 5 Pro;
                content: "\f101";
                color: var(--secondary_color);
                margin-right: 8px;
                font-size: 16pt;
                vertical-align: top;
            }

            div#preview img {
                max-width: 100%;
            }

            div#preview ul {
                padding-left: 30px;
                margin: 12px 0;
            }

            /* title */

            div#module-output > form {
                position: relative;
            }

            div#module-output > form > p {
                margin-top: 20px;
            }

            div#module-output > form > p > input {
                width: 100%;
                min-width: 250px;
                padding: 2px 4px;
                font-size: 14pt;
                line-height: 18pt;
            }

            div#module-output > form > input[type="submit"] {
                font-size: 24pt;
                line-height: 24pt;
                position: absolute;
                right: 0;
                top: -40px;
            }

            /* settings block */

            div#module-output details.settings {
                margin: 10px 0 10px;
                padding: 10px;
                background-color: var(--light_grey2);
            }

            div#module-output details.settings p + p {
                margin-top: 10px;
            }

            div#module-output > form > p > label,
            div#module-output > form > details.settings label {
                font-weight: bold;
                display: block;
                margin-bottom: 2px;
            }

            div#module-output > form > details.settings label.checkbox-label{
                display: inline-block;
            }

            div#module-output > form > details.settings summary {
                margin: 10px 0;
                cursor: pointer;
                display: inline-block;
            }

            div#module-output > form > details.settings input[type="text"],
            div#module-output > form > details.settings input[type="file"],
            div#module-output > form > details.settings textarea {
                width: 40%;
                min-width: 250px;
                padding: 2px 4px;
            }

            div#module-output > form > details.settings textarea {
                resize: vertical;
                min-height: 80px;
            }

            div#module-output > form > details.settings input[type="checkbox"] {
                width: 24px;
                height: 24px;
                vertical-align: middle;
            }

            div#module-output > form > details.settings input[type="checkbox"] + select {
                height: 24px;
                vertical-align: middle;
            }

            /* thumbnail & og:image preview */

            div#module-output > form > details.settings > div {
                width: 100%;
                margin-left: 10px;
                padding-left: 20px;
                float: right;
            }

            div#module-output > form > details.settings > div > img {
                max-width: 100%;
            }

            /* list view */

            div#module-output > button {
                font-size: 24pt;
                position: absolute;
                top: 20px;
                right: 20px;
            }

            div#module-output > div#all-locales-json {
                display: none;
            }

            div#module-output > table tr:nth-child(odd) {
                background-color: var(--light_grey2);
            }

            div#module-output > table tr > th > input,
            div#module-output > table tr > th > select {
                float: right;
            }

            div#module-output > table tr.entry.hidden {
                display: none;
            }

            div#module-output > table tr.entry td > form {
                color: #ccc;
                display: block;
            }

            div#module-output > table tr.entry td > form > i:hover {
                cursor: pointer;
                color: #434343;
            }

            div#module-output > table tr.entry td > form > input[type="text"] {
                width: calc(80% - 24px);
            }

            div#module-output > table tr.entry td > form > input[type="submit"] {
                width: 20%;
            }

            div#module-output > table tr.entry td > form > input:disabled {
                color: #ccc;
                background-color: transparent;
                border: none;
            }

            div#module-output > table tr.entry td > form > input[type="text"]:disabled + input[type="submit"],
            div#module-output > table tr.entry td > form > input[type="text"]:not(:disabled) ~ i {
                visibility: hidden;
            }

            #search_products_popup_container{
                display: none;
                top: 0;
                left: 0;
                z-index: 9999;
                position: absolute;
                width: 100%;
                height: 100%;
                background-color: rgba(0, 0, 0, 0.5);
            }

            #search_products_popup_container.show{
                display: block;
            }

            #search_products_popup_container .close-button{
                position: absolute;
                top: 16px;
                right: 16px;
                cursor: pointer;
            }

            #search_products_popup{
                padding: 16px;
                border-radius: 8px;
                background-color: #fff;
                border-bottom: 3px solid #ddd;
                box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.4);
                position: absolute;
                top: 50%;
                left: 0;
                right: 0;
                margin: auto;
                transform: translateY(-50%);
                max-width: 95%;
                width: 1600px;
                max-height: 95%;
                overflow: hidden;
                overflow-y: auto;
            }

        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            function showProductBulkAddPopup(){
                let productBulkAddPopup = document.querySelector('#search_products_popup_container');
                productBulkAddPopup.classList.add("show");
            }

            function hideProductBulkAddPopup(){
                let productBulkAddPopup = document.querySelector('#search_products_popup_container');
                productBulkAddPopup.classList.remove("show");
            }

            function addBrandToProduct(button, brandId, productId){
                let params = {
                    brand_id: brandId,
                    product_id: productId
                }

                doApiCall('brand/add_to_product', params, function (response) {
                    button.innerHTML = 'Bereits hinzugefügt <i class="fas fa-check-circle"></i> (entfernen)';
                    button.removeAttribute("click");
                    button.addEventListener("click", function () {
                        removeBrandFromProduct(button, brandId, productId)
                    })
                });
            }

            function removeBrandFromProduct(button, brandId, productId){
                let params = {
                    brand_id: brandId,
                    product_id: productId
                }

                doApiCall('brand/remove_from_product', params, function (response) {
                    button.innerHTML = '<i class="fas fa-plus-circle"></i> Marke hinzufügen';
                    button.removeAttribute("click");
                    button.addEventListener("click", function () {
                        addBrandToProduct(button, brandId, productId)
                    })
                });
            }

            var searchIsLoading = false;
            function search(keywords){
                if(searchIsLoading){
                    return;
                }

                let brand_id = document.querySelector('input[name="brand_id"]').value;

                searchIsLoading = true;

                let params = {
                    keyword: keywords,
                    brand_id: brand_id
                }

                doApiCall('brand/find_products', params, function (response) {
                    searchIsLoading = false;
                    if (keywords.length===0) {
                        document.getElementById("brand_product_add_list").innerHTML="";
                        return;
                    }
                    document.getElementById("brand_product_add_list").innerHTML=response;
                })
            }

            function delay(callback, ms) {
                var timer = 0;
                return function() {
                    var context = this, args = arguments;
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        callback.apply(context, args);
                    }, ms || 0);
                };
            }

            $(document).ready(function(e) {
                $('#search_products_input').keyup(delay(function (e) {
                    search(this.value);
                }, 500));
            });
        </script>
        <?php
    }
}