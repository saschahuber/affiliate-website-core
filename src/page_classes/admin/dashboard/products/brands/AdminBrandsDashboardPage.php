<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\brands;

use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class AdminBrandsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Hersteller-Übersicht");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $image_manager = new ImageManager();
        $brand_manager = new BrandManager();

        $items = $brand_manager->getAllWithProductNum();

        $column_labels = [
            'Titel',
            'Status',
            'Auf Übersichtsseite anzeigen',
            'In Filter anzeigen',
            'Veröffentlichungsdatum',
            'Verknüpfte Produkte',
            'Index/Follow',
            'In Suche verstecken',
            'Inhalte',
            'VG-Wort',
            'Aktionen',
        ];

        $rows = [];

        $items = $brand_manager->getAllWithProductNum();

        foreach($items as $item){
            $cells = [
                $item->title,
                new EditableSelect('brand', 'status', $item->id, $item->status, Manager::getStatusTypes()),
                new EditableToggle('brand', 'visible_on_brand_page', $item->id, $item->visible_on_brand_page, true, null),
                new EditableToggle('brand', 'show_in_filter', $item->id, $item->show_in_filter, true),
                new EditableDateTimePicker('brand', 'brand_date', $item->id, $item->brand_date),
                new HTMLElement(BufferHelper::buffered(function() use ($item){
                    ?>
                    <span class="product_num">
                        <?=$item->produkt_anzahl?>
                    </span>
                    <?php
                })),
                BufferHelper::buffered(function() use ($item){
                    (new EditableToggle('brand', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                    (new BreakComponent())->display();
                    (new EditableToggle('brand', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
                }),
                new EditableToggle('brand', 'hide_in_search', $item->id, $item->hide_in_search, true, "Verstecken"),
                new HTMLElement(BufferHelper::buffered(function() use ($item){
                    ?>
                    <span>Kurz:
                            <span>
                                <?php if(strlen($item->short_content)): ?>
                                    <span style="background-color: #0f0; padding: 0 2px;">Ja</span>
                                <?php else: ?>
                                    <span style="background-color: #f00; padding: 0 2px;">Nein</span>
                                <?php endif; ?>
                            </span>
                        </span>
                    <br>
                    <span>Vor P:
                            <span>
                                <?php if(strlen($item->content)): ?>
                                    <span style="background-color: #0f0; padding: 0 2px;">Ja</span>
                                <?php else: ?>
                                    <span style="background-color: #f00; padding: 0 2px;">Nein</span>
                                <?php endif; ?>
                            </span>
                        </span>
                    <br>
                    <?php
                })),
                BufferHelper::buffered(function() use ($item){
                    if($item->vg_wort_pixel_id) {
                        echo '<i class="fas fa-check-circle" style="color: green; font-size: 24px;"></i>';
                    }
                    else {
                        echo '<i class="fas fa-times-circle" style="color: darkred; font-size: 24px;"></i>';
                    }
                }),
                new FloatContainer([
                    new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => BrandManager::TYPE_BRAND, 'id' => $item->id]),
                    new LinkButton($CONFIG->app_domain . $brand_manager->generatePermalink($item), "Ansehen", "fas fa-eye", false, true),
                    new LinkButton("/dashboard/produkte/hersteller/bearbeiten/".$item->id, "Bearbeiten", "fas fa-pen"),
                    new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(BrandManager::TYPE_BRAND, $item->id), 'item_type' => BrandManager::TYPE_BRAND, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90])
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (LinkButton::fab("/dashboard/produkte/hersteller/bearbeiten", 'fas fa-plus'))->display();
    }
}