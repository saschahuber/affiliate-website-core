<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\manager\ProductShopManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ProductFeedImportService;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\service\ApplicationAnalyticsService;

class AdminShopsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Shop-Übersicht");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $shop_manager = new ProductShopManager();

        $items = $shop_manager->getAllBySortOrder();

        $column_labels = [
            'Titel',
            'Verstecken',
            'Reihenfolge',
            'Aktionen',
        ];

        $rows = [];

        foreach($items as $item){
            $cells = [
                $item->title,
                new EditableToggle('product__shop', 'is_hidden', $item->id, $item->is_hidden, true),
                $item->sort_order,
                new FloatContainer([
                    new LinkButton("/dashboard/produkte/shops/bearbeiten/".$item->id, "Bearbeiten", "fas fa-pen")
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (LinkButton::fab("/dashboard/produkte/shops/bearbeiten", 'fas fa-plus'))->display();
    }
}