<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\AdditionalSettings;
use saschahuber\affiliatewebsitecore\component\form\input\IconSelector;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class AdminEditProductCategoryDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produkt-Kategorie bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        $item_id = null;
        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }

        $image_manager = new ImageManager();
        $product_manager = AffiliateInterfacesHelper::getProductManager();

        $all_taxonomies = $product_manager->getTaxonomies('title', true, null);

        $item = $product_manager->getTaxonomyById(intval($item_id));

        $taxonomy_options = array(null => '--- Keine ---');
        foreach($all_taxonomies as $taxonomy){
            if($taxonomy->id !== $item_id && $taxonomy->taxonomy_parent_id !== $item_id) {
                $taxonomy_options[$taxonomy->id] = $taxonomy->title;
            }
        }

        (new LinkButton("/dashboard/produkte/kategorien", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel der Kategorie', isset($item->title)?$item->title:null);
        $title_input->display();

        TextInput::required("permalink", 'Permalink', 'Permalink der Kategorie', isset($item->permalink)?$item->permalink:null)->display();

        (new TextInput("h1", 'H1 (Optional - Default: Titel)', 'H1 (Optional - Default: Titel)', isset($item->h1)?$item->h1:null))->display();
        (new TextInput("subtitle", 'Unterüberschrit', 'Unterüberschrit', isset($item->subtitle)?$item->subtitle:null))->display();

        (new Select("parent_taxonomy", "Übergeordnete Kategorie", $taxonomy_options, $item?$item->taxonomy_parent_id:null))->display();

        $editor_before = new CkEditorTextArea("content", "Inhalt", null, isset($item->content)?$item->content:null, 300);
        $editor_before->display();

        if($item){
            (new ContentPreviewButton($CONFIG->app_domain . $product_manager->getTaxonomyPermalink($item), [
                'title' => [
                    'id' => $title_input->getId(),
                    'tinymce' => false
                ],
                'content_before_products' => [
                    'id' => $editor_before->getId(),
                    'ckeditor' => true
                ],
                'meta_title' => [
                    'id' => 'component_meta_title_input',
                    'tinymce' => false
                ],
                'meta_description' => [
                    'id' => 'component_meta_description_input',
                    'tinymce' => false
                ]
            ]))->display();
        }

        (new TextInput('all_products_headline', 'Titel vor allen Produkten', null, isset($item->all_products_headline) ? $item->all_products_headline : null))->display();

        (new IconSelector($item?$item->icon_id:null))->display();

        (new AdditionalSettings($item))->display();

        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();

        if($item_id) {
            $parent_taxonomies = [];

            $taxonomy_parent_id = $item->taxonomy_parent_id;
            while ($taxonomy_parent_id) {
                $parent_taxonomy = $product_manager->getTaxonomyById($taxonomy_parent_id);
                $taxonomy_parent_id = $parent_taxonomy->taxonomy_parent_id;
                $parent_taxonomies[] = $parent_taxonomy->id;
            }

            $product_data_field_group_manager = new ProductDataFieldGroupManager();
            $product_data_field_manager = new ProductDataFieldManager();
            $data_field_groups = $product_data_field_group_manager->getDataFieldGroups(array_filter(array_unique($parent_taxonomies)));

            $column_labels = [
                'Gruppen-Name',
                'Feld-Name',
                'Beschreibung',
                'Typ',
                'Wert 1',
                'Wert 2'
            ];

            $existing_mappings = [];
            foreach($DB->getAll("SELECT * FROM product__taxonomy_virtual_mapping_rule WHERE product__taxonomy_id = ".intval($item_id)) as $mapping){
                $existing_mappings[$mapping->product__data_field_id] = $mapping;
            }

            $rows = [];
            foreach($data_field_groups as $data_field_group){
                $data_fields = $product_data_field_manager->getDataFields($data_field_group->id);
                foreach($data_fields as $data_field) {
                    $mapping = ArrayHelper::getArrayValue($existing_mappings, $data_field->id);

                    $field_input_prefix = "virtual_product_data_field__{$data_field->id}__";
                    $all_available_values = [];
                    foreach ($product_data_field_manager->getDataFieldValueOptions($data_field->id) as $value){
                        $all_available_values[$value->value] = $value->value;
                    }

                    $first_value = isset($mapping->first_field_value)?$mapping->first_field_value:null;
                    $second_value = isset($mapping->second_field_value)?$mapping->second_field_value:null;

                    $second_value_input = null;
                    switch($data_field->field_type){
                        case ProductDataFieldManager::TYPE_BOOLEAN:
                            $first_value_input = new Toggle($field_input_prefix."first_field_value", null, $first_value, false, true);
                            break;
                        case ProductDataFieldManager::TYPE_FLOAT:
                            $first_value_input = new NumberInput($field_input_prefix."first_field_value", "Minimum", null, $first_value);
                            $second_value_input = new NumberInput($field_input_prefix."second_field_value", "Maximum", null, $second_value);
                            break;
                        case ProductDataFieldManager::TYPE_STRING:
                        case ProductDataFieldManager::TYPE_ENUM:
                        default:
                            $first_value_input = new Select($field_input_prefix."first_field_value", "Erster Wert", $all_available_values, $first_value?explode(',', $first_value):null, true);
                            break;
                    }

                    $cells = [
                        [
                            $data_field_group->group_name,
                            new HiddenInput($field_input_prefix."field_type", $data_field->field_type)
                        ],
                        $data_field->field_name,
                        $data_field->field_description,
                        ProductDataFieldManager::ALLOWED_FIELD_TYPES[$data_field->field_type],
                        $first_value_input,
                        $second_value_input
                    ];
                    $rows[] = new TableRow($cells);
                }
            }

            (new ElevatedCard([
                new Text('Virtuelle Kategorie konfigurieren', 'strong'),
                new Text('Definiere die Kriterien der Produkte, die automatisch zu dieser Kategorie hinzugefügt werden sollen. Bei Zahlenwerten kann ein Wertebereich angegeben werden.', 'p'),
                new Toggle('is_virtual', "Ist virtuelle Kategorie", $item->is_virtual, false, true),
                new BreakComponent(),
                new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered'])
            ]))->display();
        }
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'product/category/save'))->display();

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(ProductManager::TYPE_PRODUCT_CATEGORY, $item_id)->display();
        }
    }
}