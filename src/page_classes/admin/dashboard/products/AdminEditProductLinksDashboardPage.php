<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\manager\ProductShopManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditProductLinksDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produkt-Link erstellen/bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $link_manager = new ProductLinkManager();
        $shop_manager = new ProductShopManager();
        $feed_manager = new ProductFeedManager();

        $product_id = intval($this->path[3]);

        $product_link_id = null;
        if(count($this->path) > 4){
            $product_link_id = intval($this->path[4]);
        }

        $allowed_product_shops = [null => "Kein Shop gewählt"];
        foreach($shop_manager->getAllBySortOrder() as $shop){
            $allowed_product_shops[$shop->id] = $shop->title;
        }

        $allowed_product_feeds = [null => "Kein Feed gewählt"];
        foreach($feed_manager->getAll() as $feed){
            $allowed_product_feeds[$feed->id] = $feed->title;
        }

        $product = $product_manager->getById($product_id);

        $product_link = $link_manager->getById($product_link_id);

        (new LinkButton("/dashboard/produkte/bearbeiten/".$product_id, "Zurück zum Produkt '{$product->title}'", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $product_link_id))->display();
        (new HiddenInput('product_id', $product_id))->display();

        (new NumberInput("sort_order", 'Reihenfolge', 'Reihenfolge', isset($product_link->sort_order) ? $product_link->sort_order : null, 0, null, 1))->display();

        (new TextInput("price", 'Regulärer Preis', 'Regulärer Preis', isset($product_link->price) ? $product_link->price : null))->display();

        (new TextInput("reduced_price", 'Angebotspreis', 'Angebotspreis', isset($product_link->reduced_price) ? $product_link->reduced_price : null))->display();

        (new TextInput("external_id", 'Externe ID des Produkts', 'Externe ID des Produkts', isset($product_link->external_id) ? $product_link->external_id : null))->display();

        TextInput::required("url", 'Link-URL', 'Link-URL', isset($product_link->url) ? $product_link->url : null)->display();

        (new TextInput("button_text", 'HTML-Inhalt des Buttons (optional)', 'HTML-Inhalt des Buttons (optional)', isset($product_link->button_text) ? $product_link->button_text : null))->display();

        (new Toggle('is_available', 'Ist verfügbar', isset($product_link->is_available)?$product_link->is_available:false, false, true))->display();

        (new Toggle('is_active', 'Ist Aktiv', isset($product_link->is_active)?$product_link->is_active:false, false, true))->display();

        (new Select('shop_id', 'Shop', $allowed_product_shops, isset($product_link->product__shop_id)?$product_link->product__shop_id:null))->display();

        (new Select('feed_id', 'Feed', $allowed_product_feeds, isset($product_link->product__feed_id)?$product_link->product__feed_id:null))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'product/link/save'))->display();
    }

    public function getJsCode()
    {
        ?>
        <script>
            function updateProductReviewProContra(productId){
                let proTitleInputField = document.querySelector('input[name="product_pro_title"]');
                let proListInputField = document.querySelector('textarea[name="product_pro_list"]');
                let contraTitleInputField = document.querySelector('input[name="product_contra_title"]');
                let contraListInputField = document.querySelector('textarea[name="product_contra_list"]');

                let productProTitle = proTitleInputField.value;
                let productProList = proListInputField.value;
                let productContraTitle = contraTitleInputField.value;
                let productContraList = contraListInputField.value;

                let params = {
                    product_id: productId,
                    pro_title: productProTitle,
                    pro_list: productProList,
                    contra_title: productContraTitle,
                    contra_list: productContraList,
                }

                doApiCall("product/update_pro_contra_list", params, function (response) {
                    if (response.length > 0) {
                        popup(response)
                    }
                });
                return false;
            }
        </script>
        <?php
    }
}