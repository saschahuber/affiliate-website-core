<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\AdditionalSettings;
use saschahuber\affiliatewebsitecore\component\form\input\AuthorSelector;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\component\form\ProductDataTable;
use saschahuber\affiliatewebsitecore\component\form\ProductLinkTable;
use saschahuber\affiliatewebsitecore\component\form\ProductReviewTable;
use saschahuber\affiliatewebsitecore\component\form\ProductTestResult;
use saschahuber\affiliatewebsitecore\component\ProductReviewWriterButton;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\manager\ProductShopManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\saastemplatecore\component\async\AsyncComponent;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class AdminEditProductDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produkt bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $image_manager = new ImageManager();
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $brand_manager = new BrandManager();
        $product_data_field_group_manager = new ProductDataFieldGroupManager();
        $product_data_field_manager = new ProductDataFieldManager();
        $product_link_manager = new ProductLinkManager();
        $product_shop_manager = new ProductShopManager();

        $item = $product_manager->getById($item_id);

        $all_taxonomies = $product_manager->getTaxonomies('title', true, null);

        $product_data_fields = $product_data_field_manager->getProductDataFields($item);

        $taxonomy_options = array(null => '--- Keine ---');
        foreach($all_taxonomies as $taxonomy){
            if($taxonomy->id !== $item_id && $taxonomy->taxonomy_parent_id !== $item_id) {
                $taxonomy_options[$taxonomy->id] = $taxonomy->title;
            }
        }

        $main_category = null;
        $selected_taxonomies = array();
        if($item) {
            foreach ($item->taxonomies as $taxonomy) {
                $selected_taxonomies[] = $taxonomy->id;
                if($taxonomy->is_primary_taxonomy) {
                    $main_category = $taxonomy;
                }
            }
        }

        $selected_brands = [];
        if($item){
            foreach($brand_manager->getBrandsByProductId($item->id, null) as $brand){
                $selected_brands[] = $brand->id;
            }
        }

        $brand_options = [];
        foreach($brand_manager->getAll() as $brand){
            $brand_options[$brand->id] = $brand->title;
        }

        (new LinkButton("/dashboard/produkte", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel des Produkts', isset($item->title)?$item->title:null);
        $title_input->display();

        (new TextInput("image_text", 'Bild-Text (Thumbnail, OG, etc.) Default: Titel', 'Bild-Text (Thumbnail, OG, etc.) Default: Titel', isset($item->image_text)?$item->image_text:null))->display();

        TextInput::required("permalink", 'Permalink', 'Permalink des Produkts', isset($item->permalink)?$item->permalink:null)->display();

        (new TextInput("ean", 'EAN', 'EAN des Produkts', isset($item->ean)?$item->ean:null))->display();

        (new Select("kategorien", "Kategorien", $taxonomy_options, $selected_taxonomies, true))->display();

        (new Select("main_category", "Primäre Kategorie", $taxonomy_options, $main_category?$main_category->id:null, false))->display();

        (new Select("hersteller", "Hersteller", $brand_options, $selected_brands, true))->display();

        $product_content_field = new CkEditorTextArea("content", "Inhalt", null, $item?$item->content:null, 300);
        $product_content_field->display();

        if($item){
            (new ContentPreviewButton($CONFIG->website_domain . $product_manager->generatePermalink($item), [
                'title' => [
                    'id' => $title_input->getId(),
                    'tinymce' => false
                ],
                'content' => [
                    'id' => $product_content_field->getId(),
                    'ckeditor' => true
                ],
                'meta_title' => [
                    'id' => 'component_meta_title_input',
                    'tinymce' => false
                ],
                'meta_description' => [
                    'id' => 'component_meta_description_input',
                    'tinymce' => false
                ]
            ]))->display();
        }

        if($item_id) {
            $product_test_result_container = new ProductTestResult($item_id, $item);
            (new ProductReviewWriterButton($item->id, $title_input->getId(), $product_content_field->getId(), $product_test_result_container->getId()))->display();

            (new GlobalAsyncComponentButton('<i class="fas fa-robot" aria-hidden="true"></i> Produkt-Text mit KI schreiben', 'ProductContentGeneratorAsyncHandler', ['productId' => $item->id, 'title' => $item->title]))->display();
        }

        (new AuthorSelector($item?$item->author_id:null))->display();

        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();

        (new AdditionalSettings($item))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'product/save'))->display();

        ?>
        <?php if($item_id): ?>
            <div>
                <label>Produktdaten</label>

                <?php

                $group_data_mappings = [];
                foreach ($product_data_field_group_manager->getDataFieldGroups($selected_taxonomies) as $group) {
                    if (!in_array($group->product__taxonomy_id, $selected_taxonomies)) {
                        continue;
                    }
                    if (!array_key_exists($group->id, $group_data_mappings)) {
                        $group_data_mappings[$group->id] = ['group' => $group, 'data' => []];
                    }

                    foreach ($product_data_field_manager->getDataFields($group->id) as $data_field) {
                        $group_data_mappings[$group->id]['data'][] = $data_field;
                    }
                }

                (new ProductDataTable($item, $group_data_mappings, $product_data_fields))->display();

                ?>
            </div>

            <div>
                <label>Produkttest</label>

                <div>
                    <?php

                    $querystring = "SELECT * FROM product__review where product_id = " . intval($item_id);

                    $reviews = $DB->getAll($querystring, true, 60);

                    (new ProductReviewTable($item_id, $reviews))->display();

                    $product_test_result_container->display();

                    ?>
                </div>
            </div>

            <div>
                <?php

                $product_links = [];
                if ($item) {
                    $product_links = $product_link_manager->getAllByProductId($item->id, false);
                }

                (new ProductLinkTable($item, $product_links))->display();

                ?>
            </div>
        <?php endif; ?>

        <br>

        <?php

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(ProductManager::TYPE_PRODUCT, $item_id)->display();

            $daily_affiliate_link_clicks_component = new AsyncComponent('ProductLinkClicksLastIntervalAsyncHandler',
                array('height' => 350, 'product_id' => $item_id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 7), 350);
            (new ElevatedCard($daily_affiliate_link_clicks_component))->display();

            (new LinkButton('/dashboard/produkte/link-clicks?product_id='.$item_id, 'Link-Klicks prüfen', 'fas fa-history', false, true))->display();
        }
    }

    public function getJsCode()
    {
        ?>
        <script>
            function updateProductReviewProContra(productId){
                let proTitleInputField = document.querySelector('input[name="product_pro_title"]');
                let proListInputField = document.querySelector('textarea[name="product_pro_list"]');
                let contraTitleInputField = document.querySelector('input[name="product_contra_title"]');
                let contraListInputField = document.querySelector('textarea[name="product_contra_list"]');

                let productProTitle = proTitleInputField.value;
                let productProList = proListInputField.value;
                let productContraTitle = contraTitleInputField.value;
                let productContraList = contraListInputField.value;

                let params = {
                    product_id: productId,
                    pro_title: productProTitle,
                    pro_list: productProList,
                    contra_title: productContraTitle,
                    contra_list: productContraList,
                }

                doApiCallWithResponseCode("product/update_pro_contra_list", params, function (responseCode, response) {
                    if(responseCode >= 200 && responseCode <= 300){
                        displayStatusToast('Speichern erfolgreich!', 'var(--primary_color)', '#fff');
                    }
                    else{
                        displayStatusToast('Speichern fehlgeschlagen!', '#f00', '#fff');
                    }
                });
                return false;
            }
        </script>
        <?php
    }
}