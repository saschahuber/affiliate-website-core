<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\component\PaginatedProductTable;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

#[AllowDynamicProperties]
class AdminProductsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produkte");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $this->items_per_page = 50;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->product_manager = AffiliateInterfacesHelper::getProductManager();

        $this->total_item_count = $this->product_manager->getTotalItemCount();

        $this->offset = ($this->page_number - 1) * $this->items_per_page;

        $this->keyword = RequestHelper::reqstr('s');
        $this->status = RequestHelper::reqstr('status');
        $this->taxonomies = RequestHelper::reqval('taxonomies');
        $this->has_content = RequestHelper::reqint('has_content');
        $this->has_multiple_links = RequestHelper::reqint('has_multiple_links');
        $this->is_available = RequestHelper::reqint('is_available');
        $this->is_fake = RequestHelper::reqint('is_fake');

        $this->products = $this->product_manager->getFilteredProducts($this->keyword, $this->status, $this->taxonomies, $this->is_available, 'product_date', true, $this->items_per_page, $this->offset, $this->has_content, $this->is_fake);

        (new PaginatedProductTable($this->products, $this->offset, $this->items_per_page, $this->page_number, $this->total_item_count))->display();

        (LinkButton::fab("/dashboard/produkte/bearbeiten", 'fas fa-plus'))->display();

        $search_params = [
            'keyword' => $this->keyword,
            'status' => $this->status,
            'taxonomies' => $this->taxonomies,
            'has_content' => $this->has_content,
            'has_multiple_links' => $this->has_multiple_links,
            'is_available' => $this->is_available,
            'is_fake' => $this->is_fake
        ];

        (new GlobalAsyncComponentButton('<i class="fas fa-search"></i>', 'ProductSearchAsyncHandler', $search_params, null, ['fab', 'sticky']))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            .product-image {
                max-width: 100px;
                max-height: 200px;
                object-fit: contain;
            }

            .product-title-container {
                max-width: 250px;
                padding: 0 10px;
            }

            .product-categories {
                max-width: 300px;
                margin: auto;
            }

            .product-actions {
                max-width: 200px;
                margin: auto;
            }
        </style>
        <?php
    }
}