<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\IconManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class AdminProductCategoriesDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Produkt-Kategorien");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $column_labels = [
            'Titel',
            'Icon',
            'Thumbnail',
            'Übergeordnete Kategorie',
            'Status',
            'In Filter anzeigen',
            'Verknüpfte Produkte',
            'Index/Follow',
            'In Suche verstecken',
            'Ist Virtuell',
            'Meta',
            'VG-Wort',
            'Aktionen',
        ];

        $rows = [];

        $image_manager = new ImageManager();
        $icon_manager = new IconManager();

        $this->product_manager = AffiliateInterfacesHelper::getProductManager();

        $all_taxonomies = $this->product_manager->getTaxonomiesWithProductNum();

        $taxonomies = [];
        foreach ($all_taxonomies as $taxonomy) {
            $taxonomies[$taxonomy->id] = $taxonomy;
        }

        foreach ($all_taxonomies as $item) {

            $cells = [
                $item->title,
                new HTMLElement(BufferHelper::buffered(function () use ($icon_manager, $item) {
                    if ($item->icon_id) {
                        ?>
                        <div style="background-color: #0099cc; max-width: 100px; padding: 15px; border-radius: 100px;">
                            <?=$icon_manager->getIconImageTag($item->icon_id)?>
                        </div>
                        <?php
                    }
                })),
                new HTMLElement(BufferHelper::buffered(function () use ($image_manager, $item) {
                    if ($item->attachment_id) {
                        ?>
                        <div class="product-category-attachment-container">
                            <?=$image_manager->getAttachmentImageTag($item->attachment_id)?>
                        </div>
                        <?php
                    }
                })),
                $item->taxonomy_parent_id ? $taxonomies[$item->taxonomy_parent_id]->title : '-',
                new EditableSelect('product__taxonomy', 'status', $item->id, $item->status, Manager::getStatusTypes()),
                new EditableToggle('product__taxonomy', 'show_in_filter', $item->id, $item->show_in_filter, true),
                new HTMLElement(BufferHelper::buffered(function () use ($item) {
                    ?>
                    <span class="product_num">
                        <?= $item->produkt_anzahl ?>
                    </span>
                    <?php
                })),
                BufferHelper::buffered(function () use ($item) {
                    (new EditableToggle('product__taxonomy', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                    (new BreakComponent())->display();
                    (new EditableToggle('product__taxonomy', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
                }),
                new EditableToggle('product__taxonomy', 'hide_in_search', $item->id, $item->hide_in_search, true, "Verstecken"),
                new Toggle('is_virtual', "Ist virtuell", $item->is_virtual, true, true),
                BufferHelper::buffered(function () use ($item) {
                    (new Toggle('has_meta_title', "Titel", isset($item->meta_title) && strlen($item->meta_title), true, true))->display();
                    (new BreakComponent())->display();
                    (new Toggle('has_meta_description', "Description", isset($item->meta_description) && strlen($item->meta_description), true, true))->display();
                }),
                BufferHelper::buffered(function() use ($item){
                    if($item->vg_wort_pixel_id) {
                        echo '<i class="fas fa-check-circle" style="color: green; font-size: 24px;"></i>';
                    }
                    else {
                        echo '<i class="fas fa-times-circle" style="color: darkred; font-size: 24px;"></i>';
                    }
                }),
                new FloatContainer([
                    new LinkButton("/dashboard/produkte?taxonomies[]=" . $item->id, 'Produkte ansehen', 'fas fa-eye', false, true),
                    new LinkButton($CONFIG->app_domain . $this->product_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true),
                    new LinkButton("/dashboard/produkte/kategorien/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false),
                    new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(ProductManager::TYPE_PRODUCT_CATEGORY, $item->id), 'item_type' => ProductManager::TYPE_PRODUCT_CATEGORY, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90])
                ]),
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (LinkButton::fab("/dashboard/produkte/kategorien/bearbeiten", 'fas fa-plus'))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            .product-category-attachment-container {
                max-width: 150px;
            }

            .product-category-attachment-container img {
                width: 100%;
            }
        </style>
        <?php
    }
}