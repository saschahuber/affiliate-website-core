<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\AdditionalSettings;
use saschahuber\affiliatewebsitecore\component\form\input\AuthorSelector;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\ProductComparisonManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;

class AdminEditComparisonDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Vergleichsseite bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;
        }

        $image_manager = new ImageManager();
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $product_comparison_manager = new ProductComparisonManager();

        $selectable_products = ['null' => '--- Kein Produkt gewählt ---'];
        foreach($product_manager->getAll(ProductManager::STATUS_PUBLISH, false) as $product){
            $selectable_products[$product->id] = $product->title;
        }

        $item = $product_comparison_manager->getById($item_id);

        (new LinkButton("/dashboard/produkte/vergleichsseiten", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel des Beitrags', isset($item->title)?$item->title:null);

        $title_input->display();

        TextInput::required("permalink", 'Permalink', 'Permalink des Beitrags', isset($item->permalink)?$item->permalink:null)->display();

        (new TextInput("h1", 'H1 (Optional - Default: Titel)', 'H1 (Optional - Default: Titel)', isset($item->h1)?$item->h1:null))->display();
        (new TextInput("subtitle", 'Unterüberschrit', 'Unterüberschrit', isset($item->subtitle)?$item->subtitle:null))->display();

        BreakComponent::break();

        (new Text("Produkte zum Vergleich", "strong"))->display();

        (new Row([
            new Column(new Card([
                new TextInput("product_title_1", 'Titel Produkt 1 (Optional)', 'Titel Produkt 1 (Optional)', isset($item->product_title_1)?$item->product_title_1:null),
                new Select("product_id_1", "Erstes Produkt", $selectable_products, isset($item->product_id_1)?$item->product_id_1:null, false, true),
                isset($item->product_id_1)?(new LinkButton('/dashboard/produkte/bearbeiten/'.$item->product_id_1, 'Produkt bearbeiten', 'fas fa-edit', false, true)):null
            ]), ['col-md-4']),
            new Column(new Card([
                new TextInput("product_title_2", 'Titel Produkt 2 (Optional)', 'Titel Produkt 2 (Optional)', isset($item->product_title_2)?$item->product_title_2:null),
                new Select("product_id_2", "Erstes Produkt", $selectable_products, isset($item->product_id_2)?$item->product_id_2:null, false, true),
                isset($item->product_id_2)?(new LinkButton('/dashboard/produkte/bearbeiten/'.$item->product_id_2, 'Produkt bearbeiten', 'fas fa-edit', false, true)):null
            ]), ['col-md-4']),
            new Column(new Card([
                new TextInput("product_title_3", 'Titel Produkt 3 (Optional)', 'Titel Produkt 3 (Optional)', isset($item->product_title_3)?$item->product_title_3:null),
                new Select("product_id_3", "Erstes Produkt", $selectable_products, isset($item->product_id_3)?$item->product_id_3:null, false, true),
                isset($item->product_id_3)?(new LinkButton('/dashboard/produkte/bearbeiten/'.$item->product_id_3, 'Produkt bearbeiten', 'fas fa-edit', false, true)):null
            ]), ['col-md-4'])
        ]))->display();

        BreakComponent::break();

        $ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, $item?$item->content:"", 300);
        $ckeditor_area->display();

        (new GlobalAsyncComponentButton('<i class="fas fa-robot" aria-hidden="true"></i> Produkt-Vergleich mit KI schreiben', 'ProductComparisonGeneratorAsyncHandler', ['productComparisonId' => $item->id, 'title' => $item->title]))->display();

        if($item){
            (new ContentPreviewButton($CONFIG->website_domain . $product_comparison_manager->generatePermalink($item), [
                'title' => [
                    'id' => $title_input->getId(),
                    'tinymce' => false
                ],
                'content' => [
                    'id' => $ckeditor_area->getId(),
                    'ckeditor' => true
                ],
                'meta_title' => [
                    'id' => 'component_meta_title_input',
                    'tinymce' => false
                ],
                'meta_description' => [
                    'id' => 'component_meta_description_input',
                    'tinymce' => false
                ]
            ]))->display();
        }

        (new AuthorSelector($item?$item->author_id:null))->display();

        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();

        ?>

        <br>

        <?php
        (new AdditionalSettings($item))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'product/comparison/save'))->display();

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(ProductComparisonManager::TYPE_PRODUCT_COMPARISON, $item_id)->display();
        }
    }

    public function getCssCode()
    {
        ?>
        <style>
            .select2-container .select2-selection--single {
                height: 44px;
            }
        </style>
        <?php
    }
}