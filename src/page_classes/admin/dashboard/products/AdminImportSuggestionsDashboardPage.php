<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ProductImportSuggestionService;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class AdminImportSuggestionsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Import-Vorschläge");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $product_import_suggestions_service = new ProductImportSuggestionService();

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        $product_feed_manager = new ProductFeedManager();
        $feeds = [];
        foreach($product_feed_manager->getAll() as $feed){
            $feeds[$feed->id] = $feed;
        }

        $column_labels = [
            'Bild',
            'Titel',
            'Produkt',
            'Feed',
            'Zeit',
            'Aktionen',
        ];

        $rows = [];

        foreach($product_import_suggestions_service->getAll() as $suggestion){
            $product = $product_manager->getById($suggestion->product_id);

            $actions = [
                new LinkButton('/dashboard/produkte/bearbeiten/'.$suggestion->product_id, 'Zielprodukt ansehen', 'fas fa-eye', false, true),
                new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Zu diesem Produkt importieren', "ImportProductSuggestionHandler", ['product_suggestion_id' => $suggestion->id])
            ];

            $cells = [
                BufferHelper::buffered(function() use ($suggestion){
                    ?>
                    <div style="max-width: 100px; max-height: 100px; margin: auto; text-align: center;">
                        <img src="<?=$suggestion->thumbnail?>">
                    </div>
                    <?php
                }),
                $suggestion->title,
                BufferHelper::buffered(function() use ($product){
                    (new Text("ID: {$product->product_id}"))->display();
                    (new Text("Content: " . (strlen($product->content)?'Ja':'Nein')))->display();
                    (new Text("Fake: ". ($product->is_fake?'Ja':'Nein')))->display();
                }),
                $suggestion->feed_id?$feeds[$suggestion->feed_id]->title:'Amazon',
                DateHelper::displayDateTime($suggestion->suggestion_time),
                new FloatContainer($actions),
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}