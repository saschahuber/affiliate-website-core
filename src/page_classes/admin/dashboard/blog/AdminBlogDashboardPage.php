<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\blog;

use saschahuber\affiliatewebsitecore\component\PaginatedBlogPostTable;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminBlogDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Blog-Posts");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $this->items_per_page = 25;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->post_manager = new PostManager();

        $this->total_item_count = $this->post_manager->getTotalItemCount();

        $this->offset = ($this->page_number - 1) * $this->items_per_page;

        $this->keyword = RequestHelper::reqstr('s');
        $this->status = RequestHelper::reqstr('status');
        $this->taxonomies = RequestHelper::reqval('taxonomies');

        $this->posts = $this->post_manager->getFiltered($this->keyword, $this->status, $this->taxonomies, 'post_date', true, $this->items_per_page, $this->offset);

        (new PaginatedBlogPostTable($this->posts, $this->offset, $this->items_per_page, $this->page_number, $this->total_item_count))->display();

        (LinkButton::fab("/dashboard/blog/bearbeiten", 'fas fa-plus'))->display();

        $search_params = [
            'keyword' => $this->keyword,
            'status' => $this->status,
            'taxonomies' => $this->taxonomies
        ];
        (new GlobalAsyncComponentButton('<i class="fas fa-search"></i>', 'BlogPostSearchAsyncHandler', $search_params, null, ['fab', 'sticky']))->display();
    }
}