<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\blog;

use saschahuber\affiliatewebsitecore\component\BlogWriterButton;
use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\AdditionalSettings;
use saschahuber\affiliatewebsitecore\component\form\input\AuthorSelector;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\affiliatewebsitecore\service\LinkedContentScheduleItemSelectorService;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditBlogPostDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Blog-Post bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $image_manager = new ImageManager();
        $post_manager = new PostManager();

        $item = $post_manager->getById($item_id);

        $all_taxonomies = $post_manager->getTaxonomies('title', true, null);

        $taxonomy_options = array(null => '--- Keine ---');
        foreach($all_taxonomies as $taxonomy){
            $taxonomy_options[$taxonomy->id] = $taxonomy->title;
        }

        $main_category = null;
        $selected_taxonomies = array();
        if($item) {
            foreach ($item->taxonomies as $taxonomy) {
                $selected_taxonomies[] = $taxonomy->id;
                if($taxonomy->is_primary_taxonomy) {
                    $main_category = $taxonomy;
                }
            }
        }

        (new LinkButton("/dashboard/blog", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        ?>
        <?php
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel des Beitrags', isset($item->title)?$item->title:null);
        $title_input->display();

        (new TextInput("image_text", 'Bild-Text (Thumbnail, OG, etc.) Default: Titel', 'Bild-Text (Thumbnail, OG, etc.) Default: Titel', isset($item->image_text)?$item->image_text:null))->display();

        TextInput::required("permalink", 'Permalink', 'Permalink des Beitrags', isset($item->permalink)?$item->permalink:null)->display();

        (new TextInput("h1", 'H1 (Optional - Default: Titel)', 'H1 (Optional - Default: Titel)', isset($item->h1)?$item->h1:null))->display();
        (new TextInput("subtitle", 'Unterüberschrit', 'Unterüberschrit', isset($item->subtitle)?$item->subtitle:null))->display();

        (new Select("kategorien", "Kategorien", $taxonomy_options, $selected_taxonomies, true))->display();

        (new Select("main_category", "Primäre Kategorie", $taxonomy_options, $main_category?$main_category->id:null, false))->display();

        $ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, $item?$item->content:"", 300);
        $ckeditor_area->display();
        if($item_id === null){
            (new BlogWriterButton($title_input->getId(), $ckeditor_area->getId()))->display();
        }

        if($item){
            (new ContentPreviewButton($CONFIG->website_domain . $post_manager->generatePermalink($item), [
                'title' => [
                    'id' => $title_input->getId(),
                    'tinymce' => false
                ],
                'content' => [
                    'id' => $ckeditor_area->getId(),
                    'ckeditor' => true
                ],
                'meta_title' => [
                    'id' => 'component_meta_title_input',
                    'tinymce' => false
                ],
                'meta_description' => [
                    'id' => 'component_meta_description_input',
                    'tinymce' => false
                ]
            ]))->display();
        }

        (new AuthorSelector($item?$item->author_id:null))->display();

        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();

        if($item_id) {
            LinkedContentScheduleItemSelectorService::getLinkedContentScheduleItemSelector(PostManager::TYPE_POST, $item_id)->display();
        }

        ?>

        <br>

        <?php
        (new AdditionalSettings($item))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'blog/post/save'))->display();

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(PostManager::TYPE_POST, $item_id)->display();
        }
    }
}