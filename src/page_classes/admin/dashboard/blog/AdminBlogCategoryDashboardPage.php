<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\blog;

use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class AdminBlogCategoryDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Blog-Kategorien");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $this->post_manager = new PostManager();

        $column_labels = [
            'Titel',
            'Übergeordnete Kategorie',
            'Status',
            'Index/Follow',
            'In Suche verstecken',
            'Meta',
            'VG-Wort',
            'Aktionen',
        ];

        $rows = [];

        $all_taxonomies = $this->post_manager->getTaxonomies('title', true, null);

        $taxonomies = [];
        foreach($all_taxonomies as $taxonomy){
            $taxonomies[$taxonomy->id] = $taxonomy;
        }

        foreach($taxonomies as $item){
            $cells = [
                $item->title,
                $item->taxonomy_parent_id?$taxonomies[$item->taxonomy_parent_id]->title:'-',
                new EditableSelect('post__taxonomy', 'status', $item->id, $item->status, Manager::getStatusTypes()),
                BufferHelper::buffered(function() use ($item){
                    (new EditableToggle('post__taxonomy', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                    (new BreakComponent())->display();
                    (new EditableToggle('post__taxonomy', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
                }),
                new EditableToggle('post__taxonomy', 'hide_in_search', $item->id, $item->hide_in_search, true, "Verstecken"),
                BufferHelper::buffered(function() use ($item){
                    (new Toggle('has_meta_title', "Titel", isset($item->meta_title) && strlen($item->meta_title), true, true))->display();
                    (new BreakComponent())->display();
                    (new Toggle('has_meta_description', "Description", isset($item->meta_description) && strlen($item->meta_description), true, true))->display();
                }),
                BufferHelper::buffered(function() use ($item){
                    if($item->vg_wort_pixel_id) {
                        echo '<i class="fas fa-check-circle" style="color: green; font-size: 24px;"></i>';
                    }
                    else {
                        echo '<i class="fas fa-times-circle" style="color: darkred; font-size: 24px;"></i>';
                    }
                }),
                new FloatContainer([
                    new LinkButton("/dashboard/blog?taxonomies=" . $item->id, 'Beiträge ansehen', 'fas fa-eye', false, true),
                    new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => PostManager::TYPE_POST_CATEGORY, 'id' => $item->id]),
                    new LinkButton($CONFIG->website_domain . $this->post_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true),
                    new LinkButton("/dashboard/blog/kategorien/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false),
                    new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(PostManager::TYPE_POST_CATEGORY, $item->id), 'item_type' => PostManager::TYPE_POST_CATEGORY, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90])
                ]),
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (LinkButton::fab("/dashboard/blog/kategorien/bearbeiten", 'fas fa-plus'))->display();
    }
}