<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\blog;

use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\AdditionalSettings;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditBlogCategoryDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Blog-Kategorie bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;
        }

        $image_manager = new ImageManager();
        $post_manager = new PostManager();

        $all_taxonomies = $post_manager->getTaxonomies('title', true, null);

        $item = $post_manager->getTaxonomyById(intval($item_id));

        $taxonomy_options = array(null => '--- Keine ---');
        foreach($all_taxonomies as $taxonomy){
            if($taxonomy->id !== $item_id && $taxonomy->taxonomy_parent_id !== $item_id) {
                $taxonomy_options[$taxonomy->id] = $taxonomy->title;
            }
        }

        (new LinkButton("/dashboard/blog/kategorien", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel der Kategorie', isset($item->title)?$item->title:null);
        $title_input->display();

        TextInput::required("permalink", 'Permalink', 'Permalink der Kategorie', isset($item->permalink)?$item->permalink:null)->display();

        (new TextInput("h1", 'H1 (Optional - Default: Titel)', 'H1 (Optional - Default: Titel)', isset($item->h1)?$item->h1:null))->display();
        (new TextInput("subtitle", 'Unterüberschrit', 'Unterüberschrit', isset($item->subtitle)?$item->subtitle:null))->display();

        (new Select("parent_taxonomy", "Übergeordnete Kategorie", $taxonomy_options, $item?$item->taxonomy_parent_id:null))->display();

        $ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, isset($item->content)?$item->content:null, 300);
        $ckeditor_area->display();

        if($item){
            (new ContentPreviewButton($CONFIG->website_domain . $post_manager->getTaxonomyPermalink($item), [
                'title' => [
                    'id' => $title_input->getId(),
                    'tinymce' => false
                ],
                'content' => [
                    'id' => $ckeditor_area->getId(),
                    'ckeditor' => true
                ],
                'meta_title' => [
                    'id' => 'component_meta_title_input',
                    'tinymce' => false
                ],
                'meta_description' => [
                    'id' => 'component_meta_description_input',
                    'tinymce' => false
                ]
            ]))->display();
        }

        (new AdditionalSettings($item))->display();

        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'blog/category/save'))->display();

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(PostManager::TYPE_POST_CATEGORY, $item_id)->display();
        }
    }
}