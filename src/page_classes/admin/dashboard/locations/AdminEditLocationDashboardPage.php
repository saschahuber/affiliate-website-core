<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\locations;

use saschahuber\affiliatewebsitecore\component\form\IndexFollowSettingsContainer;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\LocationManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use stdClass;

class AdminEditLocationDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Ort bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        if(RequestHelper::isPost()){
            $location_manager = new LocationManager();

            $new_item = false;

            $item_id = RequestHelper::reqint('id');

            $item = $location_manager->getById($item_id);

            if($item === null){
                $new_item = true;
                $item = new stdClass();
                $item->status = Manager::STATUS_DRAFT;
            }

            $item->title = RequestHelper::reqstr('title');

            $item->permalink = RequestHelper::reqstr('permalink');
            if($item->permalink === null || strlen($item->permalink) < 1) {
                $item->permalink = UrlHelper::alias($item->title);
            }
            else{
                $item->permalink = UrlHelper::alias($item->permalink);
            }

            $item->meta_title = RequestHelper::reqstr('meta_title');
            $item->meta_description = RequestHelper::reqstr('meta_description');
            $item->og_title = RequestHelper::reqstr('og_title');
            $item->og_description = RequestHelper::reqstr('og_description');

            #$item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

            $item->doindex = RequestHelper::reqbool('doindex');
            $item->dofollow = RequestHelper::reqbool('dofollow');

            $item->featured = RequestHelper::reqbool('featured');

            #$item->content = RequestHelper::reqstr('content');

            if($new_item){
                $item->id = $location_manager->createItem($item);
                $item_id = $item->id;
            }
            else {
                $location_manager->updateItem($item);
            }

            $cache_cleared = false;#clear_page_cache($item_permalink);

            UrlHelper::redirect("/dashboard/ort/bearbeiten/" . $item_id, 302);
        }

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $image_manager = new ImageManager();
        $location_manager = new LocationManager();

        $item = $location_manager->getById($item_id);

        (new LinkButton("/dashboard/ort", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ?>

        <form action="/dashboard/ort/bearbeiten<?=$item_id?'/'.$item_id:''?>" method="post" enctype="multipart/form-data">
            <?php
            (new SubmitButton('<i class="fas fa-save"></i> Beitrag speichern'))->display();

            (new HiddenInput('id', $item_id))->display();

            $title_input = TextInput::required("title", 'Titel', 'Titel des Beitrags', isset($item->title)?$item->title:null);

            $title_input->display();

            #TextInput::required("permalink", 'Permalink', 'Permalink des Beitrags', isset($item->permalink)?$item->permalink:null)->display();

            #$ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, $item?$item->content:null, 300);
            #$ckeditor_area->display();
            #if($item_id === null){
            #    (new BlogWriterButton($title_input->getId(), $ckeditor_area->getId()))->display();
            #}

            (new Toggle("featured", "Hervorgehoben", isset($item->featured)?$item->featured:false, false, true))->display();

            ?>

            <br>

            <div class="settings">
                <!--
            <strong>Weitere Einstellungen</strong>
            <div>
                <?php if (isset($item->attachment_id)): ?>
                    <label>Derzeitiges Titelbild</label>
                    <img src="<?= $CONFIG->app_domain . $image_manager->getAttachmentUrl($item->attachment_id) ?>">
                <?php endif;
                if (!empty($item->og_image)): ?>
                    <label>Derzeitige Open-Graph Grafik</label>
                    <img src="<?= $CONFIG->app_domain . '/media/ogimg/' . $item->og_image ?>">
                <?php endif; ?>
            </div>
            -->

                <?php

                #(new MetaOgBoxes($item))->display();

                #(new AttachedFaqSelect($item?$item->attached_faq_id:null))->display();

                (new IndexFollowSettingsContainer($item))->display();

                ?>
            </div>
        </form>
        <?php
    }
}