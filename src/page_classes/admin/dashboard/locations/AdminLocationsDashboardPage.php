<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\locations;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\LocationManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\scaffolding\PaginatedLocationTable;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

#[AllowDynamicProperties]
class AdminLocationsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Orte");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $this->items_per_page = 250;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->location_manager = new LocationManager();

        $this->offset = ($this->page_number - 1) * $this->items_per_page;

        $this->keyword = RequestHelper::reqstr('s');
        $this->status = RequestHelper::reqstr('status');

        $this->posts = $this->location_manager->getFiltered($this->keyword, $this->status, null, 'title', false);

        (new PaginatedLocationTable($this->posts, $this->offset, $this->items_per_page, $this->page_number))->display();

        (LinkButton::fab("/dashboard/ort/bearbeiten", 'fas fa-plus'))->display();
    }
}