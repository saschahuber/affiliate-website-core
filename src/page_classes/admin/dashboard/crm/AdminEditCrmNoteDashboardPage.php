<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\CrmService;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditCrmNoteDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("CRM-Notiz bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;

            $contact_id = RequestHelper::reqstr('contact_id');
            $person_id = RequestHelper::reqstr('person_id');
        }

        $image_manager = new ImageManager();
        $crm_service = new CrmService();

        $item = $crm_service->getNote($item_id);

        (new LinkButton("/dashboard/crm/notes", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        (new HiddenInput('contact_id', $contact_id))->display();
        (new HiddenInput('person_id', $person_id))->display();

        (TextInput::required("title", 'Name', 'Name', isset($item->title)?$item->title:null))->display();

        (new CkEditorTextArea("content", "Inhalt", "Inhalt", isset($item->content)?$item->content:null))->display();
        $form_content = ob_get_clean();
        (new SimpleFabSaveForm($form_content, 'crm/note/save'))->display();
    }
}