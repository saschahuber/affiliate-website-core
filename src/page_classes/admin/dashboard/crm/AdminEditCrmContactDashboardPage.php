<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\CrmService;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TagInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditCrmContactDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("CRM-Kontakt bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;

            $linked_element_type = RequestHelper::reqstr('linked_element_type');
            $linked_element_id = RequestHelper::reqstr('linked_element_id');
            $title = RequestHelper::reqstr('title');
            $website = RequestHelper::reqstr('website');
            $email = RequestHelper::reqstr('email');
        }

        $image_manager = new ImageManager();
        $crm_service = new CrmService();

        $item = $crm_service->getContact($item_id);

        (new LinkButton("/dashboard/crm/contacts", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        (TextInput::required("name", 'Name', 'Name', isset($item->name)?$item->name:($title?:null)))->display();
        (TextInput::required("website", 'Website', 'Website', isset($item->website)?$item->website:($website?:null)))->display();
        (TextInput::required("email", 'E-Mail', 'E-Mail', isset($item->email)?$item->email:($email?:null)))->display();
        (new TextInput("status", 'Status', 'Status', isset($item->status)?$item->status:null))->display();

        (new TagInput("tags", 'Tags', 'Tags', isset($item->tags)?$item->tags:null))->display();

        $type_options = array_merge([null => "-- Nichts verknüpft --"], getContentTypes());

        $linked_element_type = isset($item->linked_element_type) ? $item->linked_element_type : $linked_element_type;
        $linked_element_id = isset($item->linked_element_id) ? $item->linked_element_id : $linked_element_id;

        (new Select('linked_element_type', 'Typ des verknüpften Elements', $type_options, $linked_element_type, false))->display();

        (new NumberInput('linked_element_id', 'ID des verknüpften Elements', null, $linked_element_id, 0, null))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'crm/contact/save'))->display();

        if($item){
            $contact_persons = $crm_service->getContactPersonsByContact($item->id);
            $contacts_card = new Card(BufferHelper::buffered(function() use($item, $contact_persons){
                (new Text('Kontakte', 'h2'))->display();

                $items = [];
                foreach($contact_persons as $person) {
                    $items[] = BufferHelper::buffered(function () use ($person){
                        ?>
                        <h3><?=$person->first_name?> <?=$person->last_name?></h3>
                        <p><strong>Position: <?=$person->position?></strong></p>
                        <p><strong>E-Mail: <?=$person->email?></strong></p>
                        <?php
                        (new LinkButton("/dashboard/crm/persons/edit/".intval($person->id), 'Person bearbeiten', 'fas fa-edit', false, true))->display();
                    });
                }

                (new ListContainer($items))->display();

                BreakComponent::break();

                (new LinkButton("/dashboard/crm/persons/edit?contact_id=".intval($item->id), 'Kontakt hinzufügen', 'fas fa-plus-circle', false, true))->display();
            }));

            $notes = $crm_service->getNotesByTypeAndId(CrmService::NOTE_TYPE_COMPANY, $item->id);

            $notes_card = new Card(BufferHelper::buffered(function() use($item, $notes){
                (new Text('Notizen', 'h2'))->display();

                $items = [];
                foreach($notes as $note) {
                    $items[] = BufferHelper::buffered(function () use ($note){
                        ?>
                        <h3><?=$note->title?></h3>
                        <p><strong>Datum: <?= DateHelper::displayDateTime($note->timestamp)?></strong></p>
                        <div>
                            <?=$note->content?>
                        </div>
                        <?php
                        (new LinkButton("/dashboard/crm/notes/edit/".intval($note->id), 'Notiz bearbeiten', 'fas fa-edit', false, true))->display();
                    });
                }

                (new ListContainer($items))->display();

                BreakComponent::break();

                (new LinkButton("/dashboard/crm/notes/edit?contact_id=".intval($item->id), 'Notiz hinzufügen', 'fas fa-plus-circle', false, true))->display();
            }));

            BreakComponent::break();

            (new Row([
                new Column($contacts_card, ['col-lg-6', 'col-12']),
                new Column($notes_card, ['col-lg-6', 'col-12'])
            ]))->display();
        }
    }
}