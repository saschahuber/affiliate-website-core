<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\affiliatewebsitecore\service\CrmService;
use saschahuber\saastemplatecore\helper\DateHelper;

class AdminCrmNotesDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("CRM-Notizen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $crm_service = new CrmService();

        $notes = $crm_service->getAllNotes();

        $column_labels = [
            'Titel',
            'Datum',
            'Typ',
            'Inhalt',
            'Aktionen',
        ];

        $rows = [];
        foreach($notes as $note){
            $post_actions = [];

            if($note->linked_element_type === CrmService::NOTE_TYPE_COMPANY){
                $post_actions[] = new LinkButton('/dashboard/crm/contacts/edit/' . $note->linked_element_id, 'Kontakt bearbeiten', 'fas fa-edit', false, true);
            }
            else {
                $post_actions[] = new LinkButton('/dashboard/crm/persons/edit/' . $note->linked_element_id, 'Person bearbeiten', 'fas fa-edit', false, true);
            }

            $post_actions[] = new LinkButton('/dashboard/crm/notes/edit/' . $note->id, 'Notiz bearbeiten', 'fas fa-edit', false, true);
            $post_actions[] = new DeleteButton('crm__note', $note->id, 5);

            $cells = [
                $note->title,
                DateHelper::displayDateTime($note->timestamp),
                $note->linked_element_type,
                $note->content,
                new FloatContainer($post_actions)
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}