<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\CrmService;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditCrmPersonDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("CRM-Person bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;

            $crm_contact_id = RequestHelper::reqint('contact_id');
        }

        $image_manager = new ImageManager();
        $crm_service = new CrmService();

        $item = $crm_service->getContactPerson($item_id);

        $contacts = [null => "-- Nichts verknüpft --"];
        foreach($crm_service->getAllContacts() as $contact){
            $contacts[$contact->id] = $contact->name;
        }

        (new LinkButton("/dashboard/crm/persons", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        (TextInput::required("first_name", 'Vorname', 'Vorname', isset($item->first_name)?$item->first_name:null))->display();
        (TextInput::required("last_name", 'Nachname', 'Nachname', isset($item->last_name)?$item->last_name:null))->display();
        (TextInput::required("email", 'E-Mail', 'E-Mail', isset($item->email)?$item->email:null))->display();
        (new TextInput("position", 'Position', 'Position', isset($item->position)?$item->position:null))->display();

        (new Select('crm__contact_id', 'Verknüpfter Kontakt (Firma)', $contacts, isset($item->crm__contact_id)?$item->crm__contact_id:$crm_contact_id, false))->display();
        $form_content = ob_get_clean();
        (new SimpleFabSaveForm($form_content, 'crm/person/save'))->display();

        if($item){
            $notes = $crm_service->getNotesByTypeAndId(CrmService::NOTE_TYPE_PERSON, $item->id);
            $notes_card = new Card(BufferHelper::buffered(function() use($item, $notes){
                (new Text('Notizen', 'h2'))->display();

                $items = [];
                foreach($notes as $note) {
                    $items[] = BufferHelper::buffered(function () use ($note){
                        ?>
                        <h3><?=$note->title?></h3>
                        <p><strong>Datum: <?= DateHelper::displayDateTime($note->timestamp)?></strong></p>
                        <div>
                            <?=$note->content?>
                        </div>
                        <?php
                        (new LinkButton("/dashboard/crm/notes/edit/".intval($note->id), 'Notiz bearbeiten', 'fas fa-edit', false, true))->display();
                    });
                }

                (new ListContainer($items))->display();

                BreakComponent::break();

                (new LinkButton("/dashboard/crm/notes/edit?person_id=".intval($item->id), 'Notiz hinzufügen', 'fas fa-plus-circle', false, true))->display();
            }));

            BreakComponent::break();

            $notes_card->display();
        }
    }
}