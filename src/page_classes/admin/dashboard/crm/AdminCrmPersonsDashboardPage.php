<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\CrmService;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AdminCrmPersonsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("CRM-Personen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (LinkButton::fab("/dashboard/crm/persons/edit", 'fas fa-plus'))->display();

        $crm_service = new CrmService();

        $contacts = $crm_service->getAllContacts();

        $items = [];
        foreach($contacts as $contact){
            $items[] = new Card(BufferHelper::buffered(function() use($contact){
                (new Text($contact->name, 'h2'))->display();

                $items = [];
                foreach($contact->persons as $person) {
                    $items[] = BufferHelper::buffered(function () use ($person){
                        ?>
                        <h3><?=$person->first_name?> <?=$person->last_name?></h3>
                        <p><strong>Position: <?=$person->position?></strong></p>
                        <p><strong>E-Mail: <?=$person->email?></strong></p>
                        <?php
                        (new LinkButton("/dashboard/crm/persons/edit/".intval($person->id), 'Person bearbeiten', 'fas fa-edit', false, true))->display();
                    });
                }

                (new ListContainer($items))->display();

                BreakComponent::break();

                (new LinkButton("/dashboard/crm/persons/edit?contact_id=".intval($contact->id), 'Kontakt hinzufügen', 'fas fa-plus-circle', false, true))->display();
            }));
        }
        (new MasonryContainer($items, ['col-md-4', 'col-lg-3']))->display();
    }
}