<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\affiliatewebsitecore\service\CrmService;

class AdminCrmDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("CRM");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (LinkButton::fab("/dashboard/crm/contacts/edit", 'fas fa-plus'))->display();

        $crm_service = new CrmService();
        $contacts = $crm_service->getAllContacts();

        $contact_cards = [];
        foreach ($contacts as $contact) {
            $contact_cards[] = new Card(BufferHelper::buffered(function () use ($contact) {
                (new Text($contact->name, 'h3'))->display();

                (new Text("Website: " . $contact->website?:'Keine Website'))->display();

                (new Text("E-Mail: " . $contact->email?:'Keine E-Mail'))->display();

                (new Text("Status: " . $contact->status?:'Kein Status'))->display();

                (new Text('Kontakte: ' . count($contact->persons) . ', ' . 'Notizen: ' . count($contact->notes)))->display();

                (new Text('Tags: ' . $contact->tags))->display();

                (new FloatContainer([
                    new LinkButton('/dashboard/crm/contacts/edit/' . $contact->id, 'Bearbeiten', 'fas fa-pen', false, true),
                    new LinkButton('/dashboard/crm/notes/edit/?contact_id=' . $contact->id, 'Notiz', 'fas fa-plus-circle', false, true),
                    new LinkButton('/dashboard/crm/persons/edit/?contact_id=' . $contact->id, 'Kontakt', 'fas fa-plus-circle', false, true)
                ]))->display();

                ?>
                <?php
            }));
        }

        (new MasonryContainer($contact_cards, ['col-lg-3', 'col-md-4']))->display();
    }
}