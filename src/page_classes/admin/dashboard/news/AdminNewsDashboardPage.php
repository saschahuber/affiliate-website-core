<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news;

use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\NewsImporterService;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\ConfirmButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminNewsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("News-Posts");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $this->items_per_page = 25;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->news_manager = new NewsManager();
        $this->news_importer_service = new NewsImporterService();

        $this->offset = ($this->page_number - 1) * $this->items_per_page;

        $this->keyword = RequestHelper::reqstr('s');
        $this->status = RequestHelper::reqstr('status');
        $this->taxonomies = RequestHelper::reqval('taxonomies');

        $this->posts = $this->news_manager->getFiltered($this->keyword, $this->status, $this->taxonomies, 'news_date');

        $displayed_posts = array_slice($this->posts, $this->offset, $this->items_per_page);

        (new Pagination('/dashboard/news/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], count($this->posts)/$this->items_per_page, $this->page_number))->display();

        $column_labels = [
            'Thumbnail',
            'Titel',
            'Status',
            'Datum',
            'Kategorien',
            'Feed-URL',
            'Index/Follow',
            'In Suche verstecken',
            'In Newsletter verstecken',
            'Gesponsert',
            'Aktionen',
        ];

        $feeds = $this->news_importer_service->getIntegratedFeeds();

        $rows = [];
        foreach($displayed_posts as $item){
            $post_actions = [];

            $post_category_buttons = [];
            foreach($item->taxonomies as $taxonomy){
                $button = new LinkButton("/dashboard/news/kategorien/bearbeiten/" . $taxonomy->id, $taxonomy->title, 'fas fa-pen', false, false);
                $post_category_buttons[] = $button;
            }

            $feed_url = "Kein Feed";
            if(isset($feeds[$item->imported_feed_id])){
                $feed_url = $feeds[$item->imported_feed_id]->feed_url;
            }

            if($item->status == Manager::STATUS_PUBLISH) {
                $post_actions[] = new LinkButton($CONFIG->website_domain . $this->news_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true);
            }
            $post_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => 'news', 'id' => $item->id]);
            $post_actions[] = new LinkButton("/dashboard/news/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

            $post_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(NewsManager::TYPE_NEWS, $item->id), 'item_type' => NewsManager::TYPE_NEWS, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

            if($item->status == Manager::STATUS_DRAFT) {
                $confirm_button_id = uniqid();
                $javascript_code = BufferHelper::buffered(function() use ($item, $confirm_button_id){
                    $parent_node_selector = "document.getElementById('component_{$confirm_button_id}').parentNode.parentNode.parentNode.parentNode.parentNode";

                    ?>
                    doApiCall('news/post/delete', {item_id: <?=$item->id?>}, function(responseCode, response){
                    /*
                    if (displayContainer) {
                    displayContainer.removeNode();
                    }
                    */
                    <?=$parent_node_selector?>.removeNode();
                    });
                    <?php
                });
                $post_actions[] = new ConfirmButton('<i class="fas fa-trash"></i> Löschen', "Möchtest du diesen News-Artikel unwiderruflich löschen?", $javascript_code, $confirm_button_id);
            }

            $cells = [
                new Image($this->news_manager->getThumbnailSrc($item), 200),
                $item->title,
                new EditableSelect('news', 'status', $item->id, $item->status, Manager::getStatusTypes()),
                new EditableDateTimePicker('news', 'news_date', $item->id, $item->news_date),
                new FloatContainer($post_category_buttons),
                $feed_url,
                BufferHelper::buffered(function() use ($item){
                    (new EditableToggle('news', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                    (new BreakComponent())->display();
                    (new EditableToggle('news', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
                }),
                new EditableToggle('news', 'hide_in_search', $item->id, $item->hide_in_search, true, "Verstecken"),
                new EditableToggle('news', 'hide_in_newsletter', $item->id, $item->hide_in_newsletter, true, "Verstecken"),
                new EditableToggle('news', 'is_sponsored', $item->id, $item->is_sponsored, true, "Gesponsert"),
                new FloatContainer($post_actions)
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (LinkButton::fab("/dashboard/news/bearbeiten", 'fas fa-plus'))->display();

        $search_params = [
            'keyword' => $this->keyword,
            'status' => $this->status,
            'taxonomies' => $this->taxonomies
        ];
        (new GlobalAsyncComponentButton('<i class="fas fa-search"></i>', 'NewsPostSearchAsyncHandler', $search_params, null, ['fab', 'sticky']))->display();
    }
}