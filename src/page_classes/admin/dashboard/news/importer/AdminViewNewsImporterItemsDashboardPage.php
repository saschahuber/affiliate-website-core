<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\importer;

use saschahuber\affiliatewebsitecore\component\NewsImporterPreviewTable;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\NewsFeedImportManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\NewsImporterService;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;

class AdminViewNewsImporterItemsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("News-Feed Items");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        $this->items_per_page = 25;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->product_manager = AffiliateInterfacesHelper::getProductManager();

        $this->total_item_count = $this->product_manager->getTotalItemCount();

        $this->offset = ($this->page_number - 1) * $this->items_per_page;

        $this->keyword = RequestHelper::reqstr('s');

        $this->feed_id = RequestHelper::reqint('feed_id', null);

        $feeds = [];
        foreach((new NewsFeedImportManager())->getAll() as $feed){
            $feeds[$feed->id] = $feed;
        }

        $template_options = NewsImporterService::getTemplateOptions();

        $query_builder = new DatabaseSelectQueryBuilder('news__feed_import_item');

        if($this->keyword){
            $query_builder->conditions(['title LIKE "%'.$DB->escape($this->keyword).'%"']);
        }
        else {
            $query_builder->limit(100);
        }

        if($this->feed_id){
            $query_builder->conditions(['news__feed_import_id = ' . intval($this->feed_id)]);
        }

        $query_builder->order('DATE DESC');

        $column_labels = [
            'Datum',
            'Titel',
            'Kategorien',
            'Feed',
            'Aktionen'
        ];

        $template_options = NewsImporterService::getTemplateOptions();

        $rows = [];
        foreach ($DB->getAll($query_builder->buildQuery()) as $item){
            $actions = [
                new LinkButton($item->link, 'Ansehen', 'fas fa-eye', false, true),
                new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Importieren', "NewsItemImportHandler", ['feed_item_id' => $item->news__feed_import_item_id]),
                new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Importieren (gpt-4-turbo)', "NewsItemImportHandler", ['feed_item_id' => $item->news__feed_import_item_id, 'model' => ChatGenerationTemplate::MODEL_CHAT_GPT_4_TURBO, 'tokens' => 4096]),
            ];

            $cells = [
                DateHelper::displayDateTime($item->date),
                $item->title,
                $item->category?json_decode($item->category):'',
                $template_options[$feeds[$item->news__feed_import_id]->template],
                new FloatContainer($actions)
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        $search_params = [
            'keyword' => $this->keyword,
            'feed_id' => $this->feed_id
        ];
        (new GlobalAsyncComponentButton('<i class="fas fa-search"></i>', 'NewsFeedItemSearchAsyncHandler', $search_params, null, ['fab', 'sticky']))->display();
    }
}