<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\importer;

use saschahuber\affiliatewebsitecore\component\NewsImporterPreviewTable;
use saschahuber\affiliatewebsitecore\manager\NewsFeedImportManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\NewsImporterService;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class AdminViewNewsImporterDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("News-Feeds ansehen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        if(count($this->path) > 4){
            $this->item_id = intval($this->path[4]);
        }
        else{
            $this->item_id = null;
        }

        $this->feed_importer_item = $DB->query('SELECT * 
        FROM news__feed_import 
        where news__feed_import_id = '.intval($this->item_id))->fetchObject();

        $news_importer_service = new NewsImporterService();

        $this->feed_items = $news_importer_service->getFeedItemsPreview($this->feed_importer_item);

        (new LinkButton("/dashboard/news/importer", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        (new NewsImporterPreviewTable($this->feed_items))->display();

    }
}