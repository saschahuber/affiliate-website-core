<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\importer;

use saschahuber\affiliatewebsitecore\manager\NewsFeedImportManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\NewsImporterService;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class AdminEditNewsImporterDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("News-Feeds bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;
        }

        (new LinkButton("/dashboard/news/importer", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $feed_importer_item = $DB->query('SELECT * 
        FROM news__feed_import 
        where news__feed_import_id = '.intval($item_id))->fetchObject();

        ?>
        <br>

        <table>
            <?=json_encode($feed_importer_item)?>
        </table>

        <?php
    }
}