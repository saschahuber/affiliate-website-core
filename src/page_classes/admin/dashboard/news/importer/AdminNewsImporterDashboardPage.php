<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\importer;

use saschahuber\affiliatewebsitecore\manager\NewsFeedImportManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\NewsImporterService;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class AdminNewsImporterDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("News-Feeds importieren");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $this->news_manager = new NewsManager();
        $this->taxonomies = $this->news_manager->getTaxonomies();

        $news_importer_service = new NewsImporterService();

        $input_config = [
            'template' => ['type' => 'select', 'allowed_values' => NewsImporterService::getTemplateOptions(), 'label' => 'Template'],
            'feed_url' => ['type' => 'text', 'label' => 'URL des Feeds']
        ];
        (new InsertEntryButton('news__feed_import', $input_config, "Feed hinzufügen"))->display();

        $column_labels = [
            'Template',
            'Feed-Url',
            'Settings',
            'Aktiv',
            'Aktionen'
        ];

        $template_options = NewsImporterService::getTemplateOptions();

        $rows = [];
        foreach ((new NewsFeedImportManager())->getAll() as $item){
            $cells = [
                new EditableSelect('news__feed_import', 'template', $item->id, $item->template, $template_options),
                new EditableTextInput('news__feed_import', 'feed_url', $item->id, $item->feed_url),
                new EditableTextInput('news__feed_import', 'settings', $item->id, $item->settings),
                new EditableToggle('news__feed_import', 'active', $item->id, $item->active),
                new FloatContainer([
                    new LinkButton('/dashboard/news/importer/ansehen/' . $item->id, 'Ansehen', 'fas fa-eye', false, true),
                    new LinkButton('/dashboard/news/importer/bearbeiten/' . $item->id, 'Bearbeiten', 'fas fa-edit', false, true)
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}