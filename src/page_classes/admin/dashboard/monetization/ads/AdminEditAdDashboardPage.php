<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization\ads;

use saschahuber\affiliatewebsitecore\component\EditAdRulesComponent;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use stdClass;

class AdminEditAdDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Ad bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $new_item = false;

            $ad_manager = new AdManager();

            $item_id = RequestHelper::reqint('ad_id');

            $item = $ad_manager->getAdById($item_id);

            if($item === null){
                $new_item = true;
                $item = new stdClass();
            }

            $item->title = RequestHelper::reqstr('title');

            $item->content = RequestHelper::reqstr('content');

            $item->type = RequestHelper::reqstr('type');

            $item->active = RequestHelper::reqbool('active');

            $item->start_date = RequestHelper::reqstr('start_date');

            $item->end_date = RequestHelper::reqstr('end_date');

            if($new_item){
                $item->id = $ad_manager->createItem($item);
                $item_id = $item->id;
            }
            else {
                $ad_manager->updateItem($item);
            }

            UrlHelper::redirect("/dashboard/monetization/ads/bearbeiten/" . $item_id, 302);
        }

        $ad_manager = new AdManager();

        $ad_id = null;
        if(count($this->path) > 4 && in_array($this->path[3], ['bearbeiten', 'regel-bearbeiten'])){
            $ad_id = $this->path[4];
        }

        $ad = $ad_manager->getAdById($ad_id);

        (new LinkButton("/dashboard/monetization/ads", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $input_rows = [
            [new TextInput('title', 'Titel', 'Titel der Anzeige', $ad?$ad->title:null)],
            [new TextArea('content', 'Inhalt', 'Inhalt der Anzeige', $ad?$ad->content:null)],
            [new TextArea('ads_txt', 'Eintrag in Ads.txt', 'Eintrag in Ads.txt', $ad?$ad->ads_txt:null)],
            [new DateTimePicker('start_date', 'Start-Datum', 'Start-Datum', $ad?$ad->start_date:null)],
            [new DateTimePicker('end_date', 'End-Datum', 'End-Datum', $ad?$ad->end_date:null)],
            [new Select('type', 'Typ', AdManager::getAdTypes(), $ad?$ad->type:null)],
            [new Toggle('active', 'Ist die Anzeige aktiv?', $ad?$ad->active:false, false, true)],
            [new NumberInput('prio', "Priorität", null, $ad?$ad->prio:null, 0, null, 1)],
            [new HiddenInput('ad_id', $ad_id)]
        ];

        (new SimpleFabSaveForm($input_rows, 'ads/save'))->display();

        if($ad_id){
            (new EditAdRulesComponent($ad))->display();
        }
    }
}