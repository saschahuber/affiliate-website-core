<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization\ads;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\async\inputs\EditableNumberInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class AdminAdsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Ads");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $taxonomy_mappings = [
            PostManager::TYPE_POST => [],
            ProductManager::TYPE_PRODUCT => [],
            NewsManager::TYPE_NEWS => [],
        ];

        $post_manager = new PostManager();
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $news_manager = new NewsManager();

        foreach($post_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[PostManager::TYPE_POST][$taxonomy->id] = $taxonomy;
        }

        foreach($product_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[ProductManager::TYPE_PRODUCT][$taxonomy->id] = $taxonomy;
        }

        foreach($news_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[NewsManager::TYPE_NEWS][$taxonomy->id] = $taxonomy;
        }

        $ad_manager = new AdManager();

        $items = $ad_manager->getAll();

        $column_labels = [
            'Titel',
            'Aktiv',
            'Prio',
            'Anzeigentyp',
            'Startdatum',
            'Enddatum',
            'Anzeigen in...',
            'Inhalt der Anzeige',
            'Aktionen'
        ];

        $rows = [];

        foreach ($items as $item){
            $cells = [
                new EditableTextInput('ad', 'title', $item->id, $item->title),
                new EditableToggle('ad', 'active', $item->id, $item->active, true),
                new EditableNumberInput('ad', 'prio', $item->id, $item->prio),
                new EditableSelect('ad', 'type', $item->id, $item->type, AdManager::getAdTypes()),
                new EditableDateTimePicker('ad', 'start_date', $item->id, $item->start_date),
                new EditableDateTimePicker('ad', 'end_date', $item->id, $item->end_date),
                new HTMLElement(BufferHelper::buffered(function() use ($item){
                    ?>
                    <ul>
                        <?php foreach($item->mappings as $mapping): ?>
                            <li>
                                <?=$mapping->taxonomy_type?>:
                                <?php

                                if(isset($taxonomy_mappings[$mapping->taxonomy_type])
                                    && isset($taxonomy_mappings[$mapping->taxonomy_type][$mapping->taxonomy_id])){
                                    echo $taxonomy_mappings[$mapping->taxonomy_type][$mapping->taxonomy_id]->title;
                                }
                                else{
                                    echo "In allen Kategorien";
                                }

                                ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php
                })),
                new HTMLElement(BufferHelper::buffered(function() use ($item){
                    ?>
                    <div class="ad-preview">
                        <?=$item->content?>
                    </div>
                    <?php
                })),
                new FloatContainer([
                    new LinkButton("/dashboard/monetization/ads/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false),
                    new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Aufrufe der letzten 24 Stunden', 'AdViewsLastIntervalAsyncHandler',
                        array('height' => 350, 'ad_id' => $item->id, 'title' => "Ad-Aufrufe der letzten 24 Stunden", 'interval' => DatabaseTimeSeriesHelper::INTERVAL_HOUR, 'number' => 24), 350),
                    new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Aufrufe der letzten 30 Tage', 'AdViewsLastIntervalAsyncHandler',
                        array('height' => 350, 'ad_id' => $item->id, 'title' => "Ad-Aufrufe der letzten 30 Tage", 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 30), 350)
                ]),
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (LinkButton::fab("/dashboard/monetization/ads/bearbeiten", 'fas fa-plus'))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            .ad-preview img {
                object-fit: contain;
                max-height: 300px;
            }

            /* editor view */

            div#module-output > form > div.editor {
                display: flex;
                flex-wrap: wrap;
                background-color: var(--light_grey2);
                padding: 10px;
                margin-top: 10px;
            }

            div#module-output > button#generate-preview{
                position: relative;
                font-size: 14px;
                top: unset;
                right: unset;
            }

            div#module-output > form > div.editor > textarea {
                width: 100%;
                resize: vertical;
                min-height: 420px;
                padding: 10px;
            }

            div#module-output > form > div.editor > div {
                width: 100%;
                margin-left: 10px;
                background-color: #fff;
                border: solid 1px #c1c1c1;
                padding: 10px;
            }

            div#module-output > form > div.editor > div > span.date {
                float: right;
            }

            div#module-output > form > div.editor > div > h1 + img {
                float: left;
                margin-right: 10px;
                margin-bottom: 10px;
                max-width: 100%;
            }

            div#module-output > form > div.editor > div > h1,
            div#preview > h1, div#preview > h2, div#preview > h3, div#preview > h4, div#preview > h5 {
                font-family: DM Sans Thick;
                text-transform: uppercase;
                color: inherit;
            }

            div#module-output > form > div.editor > div > h1,
            div#preview > h1 {
                font-size: 30pt;
                line-height: 36pt;
                margin-bottom: 40px;
            }

            div#preview > h2 {
                font-size: 24pt;
                line-height: 30pt;
                margin-bottom: 30px;
                margin: 20px 0 10px 0;
            }

            div#preview > h3 {
                line-height: 22pt;
                margin-bottom: 20px;
                margin: 20px 0 10px 0;
                text-align: left;
                font-size: 14pt;
            }

            div#preview > h4 {
                font-size: 16pt;
                line-height: 20pt;
                margin-bottom: 10px;
            }

            div#preview > h5 {
                font-size: 15pt;
                line-height: 19pt;
                margin-bottom: 10px;
            }

            div#preview > p {
                margin: 12px 0;
            }

            div#preview > h3:before {
                font-family: Font Awesome\ 5 Pro;
                content: "\f101";
                color: var(--secondary_color);
                margin-right: 8px;
                font-size: 16pt;
                vertical-align: top;
            }

            div#preview img {
                max-width: 100%;
            }

            div#preview ul {
                padding-left: 30px;
                margin: 12px 0;
            }

            /* title */

            div#module-output > form {
                position: relative;
            }

            div#module-output > form > p {
                margin-top: 20px;
            }

            div#module-output > form > p > input {
                width: 100%;
                min-width: 250px;
                padding: 2px 4px;
                font-size: 14pt;
                line-height: 18pt;
            }

            div#module-output > form > input[type="submit"] {
                font-size: 24pt;
                line-height: 24pt;
                position: absolute;
                right: 0;
                top: -40px;
            }

            /* settings block */

            div#module-output > form > details.settings {
                margin: 10px 0 10px;
                padding: 10px;
                background-color: var(--light_grey2);
            }

            div#module-output > form > details.settings p + p {
                margin-top: 10px;
            }

            div#module-output > form > p > label,
            div#module-output > form > details.settings label {
                font-weight: bold;
                display: block;
                margin-bottom: 2px;
            }

            div#module-output > form > details.settings label.checkbox-label{
                display: inline-block;
            }

            div#module-output > form > details.settings summary {
                margin: 10px 0;
                cursor: pointer;
                display: inline-block;
            }

            div#module-output > form > details.settings input[type="text"],
            div#module-output > form > details.settings input[type="file"],
            div#module-output > form > details.settings textarea {
                width: 40%;
                min-width: 250px;
                padding: 2px 4px;
            }

            div#module-output > form > details.settings textarea {
                resize: vertical;
                min-height: 80px;
            }

            div#module-output > form > details.settings input[type="checkbox"] {
                width: 24px;
                height: 24px;
                vertical-align: middle;
            }

            div#module-output > form > details.settings input[type="checkbox"] + select {
                height: 24px;
                vertical-align: middle;
            }

            /* thumbnail & og:image preview */

            div#module-output > form > details.settings > div {
                width: 100%;
                margin-left: 10px;
                padding-left: 20px;
                float: right;
            }

            div#module-output > form > details.settings > div > img {
                max-width: 100%;
            }

            /* list view */

            div#module-output > button {
                font-size: 24pt;
                position: absolute;
                top: 20px;
                right: 20px;
            }

            div#module-output > div#all-locales-json {
                display: none;
            }

            div#module-output > table tr:nth-child(odd) {
                background-color: var(--light_grey2);
            }

            div#module-output > table tr > th > input,
            div#module-output > table tr > th > select {
                float: right;
            }

            div#module-output > table tr.entry.hidden {
                display: none;
            }

            div#module-output > table tr.entry td > form {
                color: #ccc;
                display: block;
            }

            div#module-output > table tr.entry td > form > i:hover {
                cursor: pointer;
                color: #434343;
            }

            div#module-output > table tr.entry td > form > input[type="text"] {
                width: calc(80% - 24px);
            }

            div#module-output > table tr.entry td > form > input[type="submit"] {
                width: 20%;
            }

            div#module-output > table tr.entry td > form > input:disabled {
                color: #ccc;
                background-color: transparent;
                border: none;
            }

            div#module-output > table tr.entry td > form > input[type="text"]:disabled + input[type="submit"],
            div#module-output > table tr.entry td > form > input[type="text"]:not(:disabled) ~ i {
                visibility: hidden;
            }
        </style>
        <?php
    }
}