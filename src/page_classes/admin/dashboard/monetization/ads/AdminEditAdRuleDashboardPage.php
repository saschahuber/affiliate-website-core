<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization\ads;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditAdRuleDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Ad-Regel bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $taxonomy_mappings = [
            PostManager::TYPE_POST => [],
            ProductManager::TYPE_PRODUCT => [],
            NewsManager::TYPE_NEWS => [],
        ];

        $post_manager = new PostManager();
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $news_manager = new NewsManager();

        foreach($post_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[PostManager::TYPE_POST][$taxonomy->id] = $taxonomy;
        }

        foreach($product_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[ProductManager::TYPE_PRODUCT][$taxonomy->id] = $taxonomy;
        }

        foreach($news_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[NewsManager::TYPE_NEWS][$taxonomy->id] = $taxonomy;
        }

        $ad_manager = new AdManager();

        $ad_id = RequestHelper::reqint('id');

        $ad = $ad_manager->getAdById($ad_id);
        $column_labels = [
            'Taxonomie-Typ',
            'Taxonmomie',
            'Positiv-Keywords',
            'Negativ-Keywords'
        ];

        $rows = [];

        foreach($ad->mappings as $mapping){
            $taxonomy_type = null;
            $taxonomy_title = null;

            $selectable_ids_text = null;
            $selectable_ids_info_text = null;

            if(isset($taxonomy_mappings[$mapping->taxonomy_type])) {
                $taxonomy_type = $mapping->taxonomy_type;
            }

            if(isset($taxonomy_mappings[$mapping->taxonomy_type][$mapping->taxonomy_id])){
                $taxonomy = $taxonomy_mappings[$mapping->taxonomy_type][$mapping->taxonomy_id];
                $taxonomy_title = $taxonomy->title;
            }

            if(isset($taxonomy_mappings[$mapping->taxonomy_type])){
                $selectable_ids = [];
                $selectable_ids_info = [];
                foreach($taxonomy_mappings[$mapping->taxonomy_type] as $taxonomy){
                    $selectable_ids[] = $taxonomy->id;
                    $selectable_ids_info[] = $taxonomy->id . ' => ' . $taxonomy->title;
                }

                $selectable_ids_text = "'".implode("', '", $selectable_ids)."'";
                $selectable_ids_info_text = implode("<br>", $selectable_ids_info);
            }

            $cells = [
                new HTMLElement(BufferHelper::buffered(function() use ($taxonomy_type){
                    ?>
                    <button onclick="editField(['<?=PostManager::TYPE_POST?>', '<?=ProductManager::TYPE_PRODUCT?>', '<?=NewsManager::TYPE_NEWS?>'], 'ad__taxonomy_mapping', 'taxonomy_type', <?= $mapping->id ?>, this,
                        '<b>post:</b> Im Frontend sichtbar insofern das Veröffentlichungsdatum nicht in der Zukunft liegt.<br>' +
                        '<b>product:</b> Ein Entwurf, der nur hier im Backend angezeigt wird.<br>' +
                        '<b>news:</b> Dauerhaft ausgeblendet.'
                        );"><?= ($taxonomy_type?:" - ") ?></button>
                    <?php
                })),
                new HTMLElement(BufferHelper::buffered(function() use ($taxonomy_title, $selectable_ids_text, $selectable_ids_info_text, $mapping){
                    ?>
                    <button onclick="editField([<?=Helper::ifstr($selectable_ids_text!==null, $selectable_ids_text)?>], 'ad__taxonomy_mapping', 'taxonomy_id', <?= $mapping->id ?>, this,
                        '<?=Helper::ifstr($selectable_ids_info_text!==null, $selectable_ids_info_text)?>');
                        "><?= ($taxonomy_title?:" - ") ?></button>
                    <?php
                })),
                new HTMLElement(BufferHelper::buffered(function() use ($mapping){
                    ?>
                    <span><?=$mapping->positive_keywords?></span>
                    <button onclick="editField('textarea', 'ad__taxonomy_mapping',
                        'positive_keywords', <?= $mapping->id ?>, this.prevNode());">
                        <i class="fas fa-edit"></i>
                    </button>
                    <?php
                })),
                new HTMLElement(BufferHelper::buffered(function() use ($mapping){
                    ?>
                    <span><?=$mapping->negative_keywords?></span>
                    <button onclick="editField('textarea', 'ad__taxonomy_mapping',
                        'negative_keywords', <?= $mapping->id ?>, this.prevNode());">
                        <i class="fas fa-edit"></i>
                    </button>
                    <?php
                })),
                new HTMLElement(BufferHelper::buffered(function() use ($mapping){
                    ?>
                    <button onclick="deleteEntry('ad__taxonomy_mapping', '<?=$mapping->id?>', this.parentNode.parentNode);">löschen</button>
                    <?php
                }))
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}