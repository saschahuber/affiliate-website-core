<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminMonetizationDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Monetarisierung");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        echo "Leer...";
    }
}