<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\TextPromotionService;
use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextarea;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class AdminVgWortDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("VG-Wort");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $vg_wort_pixel_service = new VgWortPixelService();
        $text_promotion_service = new TextPromotionService();
        $mapped_pixels = $vg_wort_pixel_service->getAllMappedPixels();

        $input_config = [
            'vg_wort_pixel' => ['type' => 'text', 'label' => 'Pixel'],
            'relative_page_url' => ['type' => 'text', 'label' => 'Relative URL der Seite']
        ];
        (new InsertEntryButton('vg_wort_pixel', $input_config, "Pixel hinzufügen"))->display();

        $column_labels = [
            'Pixel',
            'URL',
            'Verknüpftes Element',
            'Länge',
            'Aufrufe in aktuellem Interval',
            'Redirect aktiv',
            'Aktionen'
        ];

        $rows = [];

        $available_redirects_for_pixels = [];
        foreach($text_promotion_service->getAllAvailableRedirects() as $redirect){
            $available_redirects_for_pixels[$redirect->vg_wort_pixel_id] = $redirect;
        }

        $content_types = getContentTypes();

        foreach($vg_wort_pixel_service->getAll() as $vg_wort_pixel){
            global $CONFIG;
            $linked_element_info = null;
            $pixel_url = $vg_wort_pixel->relative_page_url;

            if(array_key_exists($vg_wort_pixel->vg_wort_pixel_id, $mapped_pixels)){
                $element_type = $mapped_pixels[$vg_wort_pixel->vg_wort_pixel_id]['element_type'];
                $element_id = $mapped_pixels[$vg_wort_pixel->vg_wort_pixel_id]['element_id'];

                $content_element = getContentElementByTypeAndId($element_type, $element_id);

                $content_type = $content_types[$element_type];

                $pixel_url = getContentPermalinkElementByTypeAndId($element_type, $element_id);

                $linked_element_info = new LinkButton(getContentEditUrlByTypeAndId($element_type, $element_id), "$content_type: {$content_element->title}", 'fas fa-edit', false, true);
            }

            $cells = [
                new EditableTextarea('vg_wort_pixel', 'vg_wort_pixel', $vg_wort_pixel->id, $vg_wort_pixel->vg_wort_pixel),
                new LinkButton($CONFIG->app_domain . $pixel_url, $pixel_url, null, false, true),
                $linked_element_info,
                $vg_wort_pixel->text_length??0,
                $vg_wort_pixel->views_in_interval??0,
                new Toggle('redirect_active', null, array_key_exists($vg_wort_pixel->id, $available_redirects_for_pixels), true, true),
                new FloatContainer([
                    new LinkButton($CONFIG->app_domain . $vg_wort_pixel->url, "Ansehen", "fas fa-eye", false, true),
                    new DeleteButton('vg_wort_pixel', $vg_wort_pixel->id, 5)
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            td {
                text-align: center;
            }
        </style>
        <?php
    }
}