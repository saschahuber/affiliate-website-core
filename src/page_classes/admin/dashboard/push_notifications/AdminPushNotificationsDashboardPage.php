<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\push_notifications;

use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\manager\PushNotificationManager;

class AdminPushNotificationsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Push-Notifications");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (LinkButton::fab("/dashboard/push-notifications/bearbeiten", 'fas fa-plus'))->display();

        $push_manager = new PushNotificationManager();

        $column_labels = [
            'Betreff',
            'Text',
            'Bild',
            'Url',
            'Zeit',
            'Verschickt?',
            'Aktionen'
        ];

        $rows = [];
        foreach($push_manager->getAll() as $item){
            $actions = [];

            if(!$item->sent_to_all_receivers){
                $actions[] = new LinkButton("/dashboard/push-notifications/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, true);
                $actions[] = new DeleteButton('push_notification', $item->id, 5);
            }

            $cells = [
                $item->subject,
                $item->message,
                new Image($item->image_url, 150),
                new LinkButton($item->click_url, $item->click_url, null, false, true),
                $item->scheduled_time,
                new Toggle('sent_to_all_receivers', null, $item->sent_to_all_receivers, true, true),
                new FloatContainer($actions)
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}