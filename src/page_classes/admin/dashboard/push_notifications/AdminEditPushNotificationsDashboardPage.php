<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\push_notifications;

use DateTime;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\manager\PushNotificationManager;

class AdminEditPushNotificationsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Push-Notification bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (new LinkButton("/dashboard/push-notifications", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $this->title = "Push-Benachrichtigung bearbeiten/erstellen";

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;

            $linked_item_type = RequestHelper::reqstr('linked_element_type');
            $linked_item_id = RequestHelper::reqint('linked_element_id');
        }

        $push_manager = new PushNotificationManager();

        $item = $push_manager->getById($item_id);

        $item_data = [];
        if($item){
            $item_data = json_decode($item->additional_data, true);

            $linked_item_type = $item->linked_element_type;
            $linked_item_id = $item->linked_element_id;
        }

//Default-Werte füllen
        $linked_item = getContentElementByTypeAndId($linked_item_type, $linked_item_id);
        $default_link = getContentPermalinkElementByTypeAndId($linked_item_type, $linked_item_id);
        if($default_link) {
            $default_link = $CONFIG->app_domain . $default_link;
        }

        $preset_date = (new DateTime())->format('Y-m-d\T14:00:00');
        if($linked_item){
            $preset_date = $linked_item->{$linked_item_type."_date"};

            $dateTime = new DateTime($preset_date);

            $newHour = 14;
            $newMinute = 0;

            $dateTime->setTime($newHour, $newMinute);

            // Das neue Datum und Uhrzeit als String ausgeben
            $preset_date = $dateTime->format('Y-m-d H:i');
        }

        $default_image_url = getContentThumbnailByTypeAndId($linked_item_type, $linked_item_id);
        if($default_image_url) {
            $default_image_url = $CONFIG->app_domain . $default_image_url;

            $default_image_url = str_replace($CONFIG->app_domain . $CONFIG->app_domain, $CONFIG->app_domain, $default_image_url);
        }

        (new SimpleFabSaveForm([
            [new HiddenInput('id', $item_id)],
            [new HiddenInput('linked_element_type', $linked_item_type)],
            [new HiddenInput('linked_element_id', $linked_item_id)],
            [TextInput::required('subject', 'Titel', null, $item?$item->subject:($linked_item?$linked_item->title:null))],
            [TextArea::required('message', 'Nachricht', null, $item?$item->message:null)],
            [TextInput::required('image_url', 'Bild-URL', null, $item?$item->image_url:$default_image_url)],
            [TextInput::required('click_url', 'Klick-URL', null, $item?$item->click_url:$default_link)],
            [new DateTimePicker('scheduled_time', 'Versandzeit', null, $item?$item->scheduled_time:$preset_date)]
        ], 'push_notification/save'))->display();
    }
}