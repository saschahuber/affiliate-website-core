<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\push_notifications;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\service\PushNotificationService;

class AdminPushNotificationSubscribersDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Push-Abonnenten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (new LinkButton("/dashboard/push-notifications", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $push_service = new PushNotificationService();

        $column_labels = [
            'Name',
            'Vorname',
            'Nachname',
            'Datum',
            'Letzter Erfolg',
            'Letzter Fehler',
            'Fehler',
            'Aktionen'
        ];

        $rows = [];
        foreach($push_service->getSubscribers() as $item){
            $cells = [
                $item->name,
                $item->first_name,
                $item->last_name,
                $item->date_subscribed,
                $item->last_success,
                $item->last_error,
                $item->consecutive_errors,
                new FloatContainer([])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}