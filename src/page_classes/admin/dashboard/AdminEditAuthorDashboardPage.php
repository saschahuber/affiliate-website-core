<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use saschahuber\affiliatewebsitecore\component\form\AuthorLinkTable;
use saschahuber\affiliatewebsitecore\component\form\input\AttachmentSelector;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditAuthorDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Autor bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $author_service = new AuthorService();

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
            $item = $author_service->getById($item_id);
        }
        else{
            $item_id = null;
            $item = null;
        }

        (new LinkButton("/dashboard/system/autoren", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        (TextInput::required("title", 'Name', 'Name des Autors', isset($item->title)?$item->title:null))->display();

        (TextInput::required("permalink", 'Permalink', 'Permalink des Autors', isset($item->permalink)?$item->permalink:null))->display();

        (TextInput::required("job_title", 'Jobbezeichnung', 'Jobbezeichnung des Autors', isset($item->job_title)?$item->job_title:null))->display();

        (TextInput::required("organization", 'Organisation/Firma', 'Organisation/Firma des Autors', isset($item->organization)?$item->organization:null))->display();

        (new CkEditorTextArea("description", "Beschreibung", 'Beschreibung des Autors', $item?$item->description:"", 300))->display();

        (new AttachmentSelector($item?$item->attachment_id:null))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'author/save'))->display();

        ?>

        <div>
            <label>Links</label>

            <div>
                <?php
                $author = (new AuthorService())->getById($item_id);
                (new AuthorLinkTable($author))->display();
                ?>
            </div>
        </div>
        <?php
    }
}