<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\toasts;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\component\PaginatedToastTable;
use saschahuber\saastemplatecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\affiliatewebsitecore\service\ScheduledToastService;
use saschahuber\saastemplatecore\component\async\inputs\EditableConfigToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\service\ConfigService;

class AdminToastsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Toasts");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $this->items_per_page = 25;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->toast_service = new ScheduledToastService();

        $this->offset = ($this->page_number - 1) * $this->items_per_page;

        $this->keyword = RequestHelper::reqstr('s');
        $this->status = RequestHelper::reqstr('status');

        $this->toasts = $this->toast_service->getAll();

        $config_service = new ConfigService();

        (new Card([
            new Text('Einstellungen', 'strong'),
            new FloatContainer([
                new EditableConfigToggle('toast_enabled_scheduled', $config_service->getConfigField('toast_enabled_scheduled', ConfigService::TYPE_BOOL), true, 'Geplante Toasts aktiv?'),
                new EditableConfigToggle('toast_enabled_recommended', $config_service->getConfigField('toast_enabled_recommended', ConfigService::TYPE_BOOL), true, 'Empfohlene Toasts (nach Nutzer Profil) aktiv?')
            ])
        ]))->display();

        BreakComponent::break();

        (new PaginatedToastTable($this->toasts, $this->offset, $this->items_per_page, $this->page_number))->display();

        (LinkButton::fab("/dashboard/toasts/bearbeiten", 'fas fa-plus'))->display();
    }
}