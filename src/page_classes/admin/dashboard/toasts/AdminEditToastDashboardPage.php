<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\toasts;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ScheduledToastService;
use saschahuber\affiliatewebsitecore\service\ToastConditionService;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditToastDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Toast bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $default_click_url = null;
        $default_content = null;
        $default_title = "🏆 Empfohlen für dich";
        $default_small_info = "Gerade eben";

        $default_start = null;
        $default_end = null;

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;

            $linked_element_type = RequestHelper::reqstr('linked_element_type');
            $linked_element_id = RequestHelper::reqint('linked_element_id');

            if($linked_element_type && $linked_element_id){
                $default_click_url = getContentPermalinkElementByTypeAndId($linked_element_type, $linked_element_id);
                $image = getContentThumbnailByTypeAndId($linked_element_type, $linked_element_id);

                $linked_element = getContentElementByTypeAndId($linked_element_type, $linked_element_id);

                if($linked_element){
                    $default_start = $linked_element->{$linked_element_type.'_date'};

                    //3 Tage hinzufügen
                    $hours_to_add = 24 * 3;

                    $date = new DateTime($default_start);
                    $date->add(new DateInterval('PT'.$hours_to_add.'H'));
                    $default_end = $date->format('Y-m-d H:i:s');
                }

                if($image){
                    $default_content = '<img src="'.$image.'">';
                }
            }
        }

        $toast_service = new ScheduledToastService();

        $item = $toast_service->getById($item_id);

        (new LinkButton("/dashboard/toasts", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel der Seite', isset($item->title)?$item->title:$default_title);
        $title_input->display();

        (new TextInput("small_info", 'Kleine Info', 'Kleine Info', isset($item->small_info)?$item->small_info:$default_small_info))->display();

        (new DateTimePicker("active_start", 'Start-Datum', 'Kleine Info', isset($item->active_start)?$item->active_start:$default_start))->display();
        (new DateTimePicker("active_end", 'End-Datum', 'Kleine Info', isset($item->active_end)?$item->active_end:$default_end))->display();

        $ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, $item?$item->content:$default_content, 300);
        $ckeditor_area->display();
        $type_options = array_merge([null => "-- Nichts verknüpft --"], getContentTypes());

        (new TextInput("click_url", 'Klick URL', 'Klick URL', isset($item->click_url)?$item->click_url:$default_click_url))->display();

        $linked_element_type = isset($item->linked_element_type) ? $item->linked_element_type : $linked_element_type;
        $linked_element_id = isset($item->linked_element_id) ? $item->linked_element_id : $linked_element_id;

        (new Select('linked_element_type', 'Typ des verknüpften Elements', $type_options, $linked_element_type))->display();
        (new NumberInput('linked_element_id', 'ID des verknüpften Elements', null, $linked_element_id, 0, null))->display();

        if($item_id){
            BreakComponent::break();

            (new Card(BufferHelper::buffered(function() use ($item_id){
                (new Text('Bedingungen', 'strong'))->display();
                BreakComponent::break();
                (new Text('Alle Bedingungen müssen erfüllt sein, damit der Toast dargestellt wird.', 'span'))->display();

                BreakComponent::break();
                BreakComponent::break();

                $toast_condition_service = new ToastConditionService();

                $conditions = $toast_condition_service->getByToastId($item_id);

                $condition_items = [];
                foreach($conditions as $condition){
                    $condition_items[] = BufferHelper::buffered(function() use ($item_id, $condition){
                        (new Row([
                            new Column($condition->condition_type, 'col-md-3'),
                            new Column($condition->condition_data, 'col-md-6'),
                            new Column([
                                new FloatContainer([
                                    new LinkButton("/dashboard/toasts/$item_id/condition/{$condition->id}", 'Bearbeiten', 'fas fa-edit', false, true),
                                    new DeleteButton('toast__condition', $condition->id, 7)
                                ])
                            ], 'col-md-3')
                        ]))->display();
                    });
                }

                (new ListContainer($condition_items))->display();

                BreakComponent::break();

                (new LinkButton("/dashboard/toasts/$item_id/condition", 'Bedingung hinzufügen', 'fas fa-plus-circle', false, true))->display();
            })))->display();
        }
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'toast/save'))->display();
    }
}