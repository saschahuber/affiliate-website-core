<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\toasts;

use saschahuber\affiliatewebsitecore\enum\ToastConditionType;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ToastConditionService;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use stdClass;

class AdminEditToastConditionDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Toast-Bedingung bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $toast_condition_service = new ToastConditionService();

            $new_item = false;

            $toast_id = intval($this->path[2]);

            $item_id = RequestHelper::reqint('id');

            $item = $toast_condition_service->getById($item_id);

            if($item === null){
                $new_item = true;
                $item = new stdClass();
                $item->toast_id = $toast_id;
            }

            $item->condition_type = RequestHelper::reqstr('condition_type');

            $condition_data = [];
            foreach($_REQUEST as $key => $value) {
                $key_parts = explode('__', $key);

                if(count($key_parts) === 2 && $key_parts[0] === 'condition') {
                    $condition_data[$key_parts[1]] = $value;
                }
            }
            $item->condition_data = json_encode($condition_data);

            if($new_item){
                $item->id = $toast_condition_service->createItem($item);
                $item_id = $item->id;
            }
            else {
                $toast_condition_service->updateItem($item);
            }

            $cache_cleared = false;#clear_page_cache($item_permalink);

            UrlHelper::redirect("/dashboard/toasts/$toast_id/condition/" . $item_id, 302);
        }

        $toast_id = intval($this->path[2]);

        if(count($this->path) > 3){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;

            $linked_element_type = RequestHelper::reqstr('linked_element_type');
            $linked_element_id = RequestHelper::reqint('linked_element_id');
        }

        $toast_condition_service = new ToastConditionService();

        $item = $toast_condition_service->getById($item_id);

        (new LinkButton("/dashboard/toasts/bearbeiten/$toast_id", "Zurück zum Toast", "fas fa-arrow-left", false))->display();

        ?>

        <form action="/dashboard/toasts/<?=$toast_id?>/condition<?=$item_id?'/'.$item_id:''?>" method="post" enctype="multipart/form-data">
            <?php
            (new SubmitButton('<i class="fas fa-save"></i> Bedingung speichern'))->display();

            (new HiddenInput('id', $item_id))->display();
            (new HiddenInput('toast_id', $toast_id))->display();

            (new Select('condition_type', 'Typ', ToastConditionType::allConditionTypes(), isset($item->condition_type)?$item->condition_type:null,
                false, false, false, false, "onTypeUpdate(this.value, ".($item?"{$item->id}":'null').")"))->display();
            ?>

            <div id="condition-data" style="margin-top: 10px;"></div>

            <script>
                window.addEventListener('load', function (e) {
                    onTypeUpdate('<?=$item?$item->condition_type:array_keys(ToastConditionType::allConditionTypes())[0]?>', <?=($item?"{$item->id}":'null')?>);
                });
            </script>
        </form>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            function onTypeUpdate(type, condition_id){
                let params = {
                    condition_type: type,
                    condition_id: condition_id
                };

                asyncHandler('ToastConditionSettingsAsyncHandler', encodeQueryData(params), null, 'condition-data', 250, null, false, true);
            }
        </script>
        <?php
    }
}