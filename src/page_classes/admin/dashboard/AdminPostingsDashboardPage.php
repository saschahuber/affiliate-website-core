<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;

use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;

class AdminPostingsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Posting-Übersicht");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        (new LinkButton("/dashboard/calendar", "Zur Kalender-Ansicht", "fas fa-calendar", false))->display();

        (new LinkButton("/dashboard/postings/bearbeiten", "Beitrag erstellen", "fas fa-plus", false))->display();

        BreakComponent::break();

        $posts = [];
        $dbquery = $DB->query('SELECT * FROM social_posting_feed_item order by posting_time DESC');
        while ($post = $dbquery->fetchObject()) {
            $posts[] = $post;
        }

        $all_targets = [
            SocialPostingFeedItemManager::TARGET_FACEBOOK_PAGE => ['name' => 'Facebook-Seite', 'icon' => 'fab fa-facebook'],
            SocialPostingFeedItemManager::TARGET_FACEBOOK_GROUP => ['name' => 'Facebook-Gruppe', 'icon' => 'fab fa-facebook'],
            SocialPostingFeedItemManager::TARGET_INSTAGRAM => ['name' => 'Instagram', 'icon' => 'fab fa-instagram'],
            SocialPostingFeedItemManager::TARGET_TWITTER => ['name' => 'Twitter', 'icon' => 'fab fa-twitter'],
            SocialPostingFeedItemManager::TARGET_REDDIT => ['name' => 'Reddit', 'icon' => 'fab fa-reddit'],
            SocialPostingFeedItemManager::TARGET_BLUESKY => ['name' => 'Bluesky', 'icon' => null],
            SocialPostingFeedItemManager::TARGET_PINTEREST => ['name' => 'Pinterest', 'icon' => 'fab fa-pinterest'],
            SocialPostingFeedItemManager::TARGET_THREADS => ['name' => 'Threads', 'icon' => null],
            SocialPostingFeedItemManager::TARGET_LINKEDIN => ['name' => 'LinkedIn', 'icon' => 'fab fa-linkedin']
        ];

        $targets = [];
        $dbquery = $DB->query('SELECT distinct(target) FROM social_posting_feed_item');
        while ($target = $dbquery->fetchObject()) {
            $targets[] = $target->target;
        }

        foreach($targets as $target){
            $platform_data = $all_targets[$target];
            (new LinkButton($CONFIG->api_domain . "/rss/autoposting/$target", 'Feed für ' . $platform_data['name'], $platform_data['icon'], false, true))->display();
        }

        BreakComponent::break();

        $column_labels = [
            'App',
            'Posting-Datum',
            'Titel',
            'Beschreibung',
            'Link',
            'Bild',
            'Verknüpftes Element',
            'Aktionen'
        ];

        $rows = [];

        foreach($posts as $post){
            $actions = [
            ];

            if($post->posting_time === null) {
                $actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'SocialPostFeedItemPreviewHandler', ['id' => $post->id], null);
                $actions[] = new LinkButton('/dashboard/postings/bearbeiten/' . $post->id, 'Bearbeiten', 'fas fa-pen', false, true);
                $actions[] = new DeleteButton('social_posting_feed_item', $post->id, 5);
            }

            $posting_interval_options = [
                'once' => "Einmalig",
                'weekly' => "Wöchentlich",
                'monthly' => "Monatlich",
                'yearly' => "Jährlich"
            ];

            $actions[] = new JsButton('<i class="fas fa-eye"></i> Ansehen', 'showPostingPreview(this)');

            if(isset($post->post_data)){
                $actions[] = new HTMLElement(BufferHelper::buffered(function() use ($post){
                    ?>
                    <div class="post-data hidden">
                        <?=$post->post_data?>
                    </div>
                    <?php
                }));
            }

            $item_link_info = 'Nichts verknüpft';
            if($post->linked_element_type && $post->linked_element_id){
                $url = getContentEditUrlByTypeAndId($post->linked_element_type, $post->linked_element_id);
                $item_link_info = new LinkButton($url, "{$post->linked_element_type} - {$post->linked_element_id}", null, false, true);
            }

            $cells = [
                $post->target,
                $post->posting_time?DateHelper::displayDateTime($post->posting_time):null,
                $post->title,
                $post->description,
                $post->link,
                $post->image_url,
                $item_link_info,
                new FloatContainer($actions)
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            div#module-output table td {
                text-align: center;
            }

            div.preview-container{
                text-align: center;
            }

            td.today{
                background-color: #eee;
            }

            div.preview-container img{
                object-fit: contain;
                max-width: 100%;
                max-height: 400px;
            }

            td li{
                list-style-type: none;
            }

            td ul{
                padding-left: unset;
            }

            td.calendar_container{
                padding-left: 2px;
            }

            td.calendar_container{
                vertical-align: top;
            }

            td.calendar_container p:first-of-type{
                text-align: left;
                padding: 5px;
            }

            td.calendar_container ul li p > a.button{
                width: unset !important;
                font-size: 16px !important;
                font-weight: 200 !important;
            }

            td.calendar_container ul li button,
            td.calendar_container ul li a.button {
                width: 100%;
                margin: 2px;
            }

            table {
                width: 100%;
                border-collapse: collapse;
            }

            table td {
                width: calc(100% / 8);
            }
        </style>
        <?php
    }
}