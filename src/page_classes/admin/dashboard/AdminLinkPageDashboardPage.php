<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use saschahuber\affiliatewebsitecore\component\PaginatedLinkPageItemTable;
use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminLinkPageDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Links");
    }
    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $this->title = "Links";

        $link_page_item_manager = new LinkPageItemManager();

        $items_per_page = 50;

        if(count($this->path) > 2){
            $page_number = intval($this->path[2]);
        }
        else{
            $page_number = 1;
        }
        $total_item_count = $link_page_item_manager->getTotalItemCount();

        $offset = ($page_number - 1) * $items_per_page;

        $items = $link_page_item_manager->getAll();

        (new PaginatedLinkPageItemTable($items, $offset, $items_per_page, $page_number))->display();

        (LinkButton::fab("/dashboard/seiten/links/bearbeiten", 'fas fa-plus'))->display();
    }
}