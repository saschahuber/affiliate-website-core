<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AdminGuideFilterDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Guide-Filter");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        $guide_filters = array();
        $dbquery = $DB->query("SELECT * FROM guide_filter");
        while($guide_filter = $dbquery->fetchObject()){
            $guide_filters[] = $guide_filter;
        }

        (new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Neuer Filter', 'CreateGuideFilterFormHandler', []))->display();

        $column_labels = [
            'Titel',
            'Shortcode',
            'Ergebnis-Typ',
            'Filter-Kategorien',
            'Filter-Schritte',
            'Aktionen'
        ];

        $rows = [];

        foreach($guide_filters as $guide_filter){
            $cells = [
                new EditableTextInput('guide_filter', 'title', $guide_filter->id, $guide_filter->title),
                new Text('[guide_filter id="'.$guide_filter->id.'"]'),
                new EditableTextInput('guide_filter', 'result_type', $guide_filter->id, $guide_filter->result_type),
                new EditableTextInput('guide_filter', 'pre_filter_category_ids', $guide_filter->id, $guide_filter->pre_filter_category_ids),
                new HTMLElement(BufferHelper::buffered(function() use ($guide_filter){
                    global $DB;

                    $guide_filter_steps = array();
                    $dbquery = $DB->query("SELECT * FROM guide_filter__step
                        where guide_filter_id = ".intval($guide_filter->guide_filter_id)." ORDER BY step_order ASC");
                    while($guide_filter_step = $dbquery->fetchObject()){
                        $guide_filter_steps[] = $guide_filter_step;
                    }
                    ?>
                    <ul>
                        <?php foreach($guide_filter_steps as $guide_filter_step): ?>
                            <li>
                                <a class="btn btn-primary" href="/dashboard/guide-filter/step-bearbeiten/<?=$guide_filter_step->id?>">
                                    <?=$guide_filter_step->title?> <i class="fas fa-edit"></i>
                                </a>
                            </li>
                        <?php endforeach; ?>
                        <li>
                            <?php
                            (new LinkButton('/dashboard/guide-filter/step-bearbeiten?guide_filter_id=' . $guide_filter->id, 'Schritt hinzufügen', 'fas fa-plus-circle', false, true))->display();
                            ?>
                        </li>
                    </ul>
                    <?php
                })),
                new FloatContainer([
                    new LinkButton('/dashboard/guide-filter/bearbeiten/' . $guide_filter->id, 'Bearbeiten', 'fas fa-edit', false, true),
                    new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'GuideFilterPreviewHandler', ['id' => $guide_filter->id]),
                    new DeleteButton('guide_filter', $guide_filter->id, 5)
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        #(LinkButton::fab("/dashboard/guide-filter/bearbeiten", 'fas fa-plus'))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            table {
                width: 100%;
                border-collapse: collapse;
            }

            table td {
                text-align: center;
            }
        </style>
        <?php
    }
}