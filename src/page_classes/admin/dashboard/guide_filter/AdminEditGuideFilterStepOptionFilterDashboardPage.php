<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter;

use saschahuber\affiliatewebsitecore\misc\GuideFilter;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditGuideFilterStepOptionFilterDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Guide-Filter Step-Option-Filter bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        if(count($this->path) > 5){
            $item_id = intval($this->path[5]);
        }
        else{
            $item_id = null;
        }

        $option_id = RequestHelper::reqint('option_id');

        $guide_filter_step_option_filter = $DB->query("SELECT * FROM guide_filter__step_option_filter
        where guide_filter__step_option_filter_id = ".intval($item_id) . " LIMIT 1")->fetchObject();

        (new LinkButton("/dashboard/guide-filter/step-bearbeiten/option-bearbeiten/".($guide_filter_step_option_filter?$guide_filter_step_option_filter->guide_filter__step_option_id:$option_id), "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $input_rows = [
            [new HiddenInput('guide_step_id', $guide_filter_step_option_filter->id)],
            [new HiddenInput('guide_step_option_id', $option_id)],
            [new Select('filter_type', 'Typ', GuideFilter::getAllowedFilterTypes(), $guide_filter_step_option_filter->filter_type)],
            [new TextInput('filter_key', 'Key', 'Key', $guide_filter_step_option_filter->filter_key)],
            [new Select('filter_comparator', 'Vergleichsoperator', GuideFilter::getAllowedComparators(), $guide_filter_step_option_filter->filter_comparator)],
            [new TextInput('filter_value', 'Wert', 'Wert', $guide_filter_step_option_filter->filter_value)]
        ];

        (new SimpleFabSaveForm($input_rows, "guide_filter/step/option/filter/save"))->display();
    }
}