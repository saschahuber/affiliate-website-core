<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter;

use saschahuber\affiliatewebsitecore\misc\GuideFilter;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditGuideFilterDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Guide-Filter bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $guide_filter = $DB->query("SELECT * FROM guide_filter
        where guide_filter_id = ".intval($item_id))->fetchObject();

        (new LinkButton("/dashboard/guide-filter", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $input_rows = [
            [new HiddenInput('guide_filter_id', $guide_filter->id)],
            [new TextInput('title', 'Titel', 'Titel', $guide_filter->title)],
            [new Select('result_type','Ergebnis-Type', GuideFilter::getAllowedResultTypes(), $guide_filter->result_type)],
            [new TextInput('pre_filter_category_ids', 'Kategorie-Vorfilterung', 'Kategorie-Vorfilterung', $guide_filter->pre_filter_category_ids)]
        ];

        (new SimpleFabSaveForm($input_rows, "guide_filter/save"))->display();
    }
}