<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter;

use saschahuber\affiliatewebsitecore\misc\GuideFilter;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditGuideFilterStepDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Guide-Filter Step bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $guide_filter_id = RequestHelper::reqint('guide_filter_id');

        $guide_filter_step = $DB->query("SELECT * FROM guide_filter__step
        where guide_filter__step_id = ".intval($item_id) . " LIMIT 1")->fetchObject();

        (new LinkButton("/dashboard/guide-filter/bearbeiten/".($guide_filter_step?$guide_filter_step->guide_filter_id:$guide_filter_id), "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $guide_filter_options = array();
        $dbquery = $DB->query("SELECT * FROM guide_filter__step_option
        where guide_filter__step_id = ".intval($guide_filter_step->id)." ORDER BY option_order ASC");
        while($guide_filter_option = $dbquery->fetchObject()){
            $guide_filter_options[] = $guide_filter_option;
        }

        $guide_filter_conditions = array();
        $dbquery = $DB->query("SELECT * FROM guide_filter__step_condition
        where guide_filter__step_id = ".intval($guide_filter_step->id));
        while($guide_filter_condition = $dbquery->fetchObject()){
            $guide_filter_conditions[] = $guide_filter_condition;
        }

        $input_config = [
            'guide_filter__step_id' => ['type' => 'hidden', 'value' => $guide_filter_step->id],
            'img' => ['type' => 'text', 'label' => 'Bildpfad'],
            'icon' => ['type' => 'text', 'label' => 'Icon'],
            'label' => ['type' => 'text', 'label' => 'Label'],
            'and_connections' => ['type' => 'boolean', 'label' => 'UND-Verknüpfung'],
            'option_order' => ['type' => 'text', 'label' => 'Sortier-Reihenfolge'],
        ];
        $add_review_button = new InsertEntryButton('guide_filter__step_option', $input_config, 'Neue Option');

        $input_rows = [
            [new HiddenInput('guide_step_id', $guide_filter_step->id)],
            [new HiddenInput('guide_filter_id', $guide_filter_id)],
            [new TextInput('title', 'Titel', 'Titel', $guide_filter_step->title)],
            [new TextInput('description','Beschreibung', 'Beschreibung', $guide_filter_step->description)],
            [new TextInput('filter_progress_text', 'Filter-Text', 'Filter-Text', $guide_filter_step->filter_progress_text)],
            [new Toggle('multiple_selections', 'Mehrfachauswahl', $guide_filter_step->multiple_selections, false, true)],
            [new Toggle('extend_results', 'Ergebnisse erweitern', $guide_filter_step->extend_results, false, true)],
            [new Toggle('skippable', 'Überspringbar', $guide_filter_step->skippable, false, true)],
            [new NumberInput('step_order', 'Step-Sortierung', 'Step-Sortierung', $guide_filter_step->step_order, 0, null, 1)],
            [new Toggle('active', 'Aktiv', $guide_filter_step->active, false, true)]
        ];

        (new SimpleFabSaveForm($input_rows, "guide_filter/step/save"))->display();

        if($guide_filter_step){
            $column_labels = [
                'Bildpfad',
                'Icon',
                'Label',
                'Sortier-Reihenfolge',
                'UND-Verknüpfung',
                'Aktiv',
                'Filter',
                'Aktionen'
            ];

            $rows = [];

            foreach ($guide_filter_options as $guide_filter_option){
                $cells = [
                    new EditableTextInput('guide_filter__step_option', 'img', $guide_filter_option->id, $guide_filter_option->img),
                    new EditableTextInput('guide_filter__step_option', 'icon', $guide_filter_option->id, $guide_filter_option->icon),
                    new EditableTextInput('guide_filter__step_option', 'label', $guide_filter_option->id, $guide_filter_option->label),
                    new EditableTextInput('guide_filter__step_option', 'option_order', $guide_filter_option->id, $guide_filter_option->option_order),
                    new EditableToggle('guide_filter__step_option', 'and_connections', $guide_filter_option->id, $guide_filter_option->and_connections),
                    new EditableToggle('guide_filter__step_option', 'active', $guide_filter_option->id, $guide_filter_option->active),
                    new HTMLElement(BufferHelper::buffered(function() use ($guide_filter_option, $guide_filter_step){
                        global $DB;
                        $guide_filter_option_filters = array();
                        $dbquery = $DB->query("SELECT * FROM guide_filter__step_option_filter
                            where guide_filter__step_option_id = ".intval($guide_filter_option->id));
                        while($guide_filter_option_filter = $dbquery->fetchObject()){
                            $guide_filter_option_filters[] = $guide_filter_option_filter;
                        }
                        ?>
                        <ul>
                            <?php foreach($guide_filter_option_filters as $guide_filter_option_filter): ?>
                                <li>
                                    <a class="btn btn-primary"
                                       href="/dashboard/guide-filter/step-bearbeiten/option-bearbeiten/filter-bearbeiten/<?=$guide_filter_option_filter->id?>?option_id=<?=$guide_filter_option->id?>&step_id=<?=$guide_filter_step->id?>">
                                        (<?=$guide_filter_option_filter->filter_type?>) <?=$guide_filter_option_filter->filter_key?>
                                        <?=$guide_filter_option_filter->filter_comparator?> <?=$guide_filter_option_filter->filter_value?>
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                            <li>
                                <?php

                                $input_config = [
                                    'guide_filter__step_option_id' => ['type' => 'hidden', 'value' => $guide_filter_option->id],
                                    'filter_type' => ['type' => 'select', 'allowed_values' => GuideFilter::getAllowedFilterTypes(), 'label' => 'Filter-Typ'],
                                    'filter_key' => ['type' => 'text', 'label' => 'Filter-Key'],
                                    'filter_comparator' => ['type' => 'select', 'allowed_values' => GuideFilter::getAllowedComparators(), 'label' => 'Vergleichsoperator'],
                                    'filter_value' => ['type' => 'text', 'label' => 'Filter-Wert'],
                                ];
                                (new InsertEntryButton('guide_filter__step_option_filter', $input_config, 'Neuer Filter'))->display();

                                ?>
                            </li>
                        </ul>
                        <?php
                    })),
                    new DeleteButton("guide_filter__step_option", $guide_filter_option->id, 2)
                ];
                $rows[] = new TableRow($cells);
            }

            (new ElevatedCard([
                new Text('Filter-Optionen', 'strong'),
                new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']),
                new BreakComponent(),
                new FloatContainer([
                    new LinkButton('/dashboard/guide-filter/step-bearbeiten/option-bearbeiten?step_id='.$guide_filter_step->id, 'Neue Option', 'fas fa-plus-circle')
                ])
            ]))->display();

            BreakComponent::break();

            $column_labels = [
                'Schritt',
                'Auswahl',
                'Aktionen'
            ];

            $rows = [];

            $allowed_condition_steps = [];
            $allowed_condition_options = [];

            $query = 'SELECT guide_filter__step_option_id, guide_filter__step.guide_filter__step_id, guide_filter__step_option.label as option_label, guide_filter__step.title as step_label
        FROM guide_filter__step_option
         LEFT JOIN guide_filter__step on guide_filter__step.guide_filter__step_id = guide_filter__step_option.guide_filter__step_id
         where guide_filter_id = '.intval($guide_filter_step->guide_filter_id);

            foreach($DB->getAll($query) as $item){
                $allowed_condition_steps[$item->guide_filter__step_id] = $item->step_label;
                $allowed_condition_options[$item->guide_filter__step_option_id] = "[{$item->step_label}] $item->option_label";
            }

            foreach ($guide_filter_conditions as $guide_filter_condition){
                $cells = [
                    new EditableSelect('guide_filter__step_condition', 'condition_step_id', $guide_filter_condition->id, $guide_filter_condition->condition_step_id, $allowed_condition_steps),
                    new EditableSelect('guide_filter__step_condition', 'condition_option_id', $guide_filter_condition->id, $guide_filter_condition->condition_option_id, $allowed_condition_options),
                    new DeleteButton("guide_filter__step_option", $guide_filter_condition->id, 2)
                ];
                $rows[] = new TableRow($cells);
            }

            $input_config = [
                'guide_filter__step_id' => ['type' => 'hidden', 'value' => $guide_filter_step->id],
                'condition_step_id' => ['type' => 'select', 'allowed_values' => $allowed_condition_steps, 'label' => 'Filter-Schritt'],
                'condition_option_id' => ['type' => 'select', 'allowed_values' => $allowed_condition_options, 'label' => 'Gewählte Option'],
            ];

            (new ElevatedCard([
                new Text('Bedingungen für diese Filter-Schritt', 'strong'),
                new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']),
                new BreakComponent(),
                new FloatContainer([
                    new InsertEntryButton('guide_filter__step_condition', $input_config, 'Neue Bedingung')
                ])
            ]))->display();
        }
    }
}