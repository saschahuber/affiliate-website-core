<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditGuideFilterStepOptionDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Guide-Filter Step-Option bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;
        }

        $filter_id = RequestHelper::reqint('filter_id');
        $step_id = RequestHelper::reqint('step_id');

        $guide_filter_step_option = $DB->query("SELECT * FROM guide_filter__step_option
        where guide_filter__step_option_id = ".intval($item_id) . " LIMIT 1")->fetchObject();

        (new LinkButton("/dashboard/guide-filter/step-bearbeiten/".($guide_filter_step_option?$guide_filter_step_option->guide_filter__step_id:$step_id), "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $input_rows = [
            [new HiddenInput('guide_step_option_id', $guide_filter_step_option->id)],
            [new HiddenInput('guide_step_id', $step_id)],
            [new TextInput('label', 'Label', 'Label', $guide_filter_step_option->label)],
            [new TextInput('icon', 'Icon', 'Icon', $guide_filter_step_option->icon)],
            [new Toggle('active', 'Ist aktiv?', $guide_filter_step_option->active, false, true)]
        ];

        (new SimpleFabSaveForm($input_rows, "guide_filter/step/option/save"))->display();
    }
}