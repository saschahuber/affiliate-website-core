<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use DateTime;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\component\form\social_post_generator\BlueskyPostGeneratorForm;
use saschahuber\affiliatewebsitecore\component\form\social_post_generator\FacebookPagePostGeneratorForm;
use saschahuber\affiliatewebsitecore\component\form\social_post_generator\InstagramPostGeneratorForm;
use saschahuber\affiliatewebsitecore\component\form\social_post_generator\LinkedInPostGeneratorForm;
use saschahuber\affiliatewebsitecore\component\form\social_post_generator\PinterestPostGeneratorForm;
use saschahuber\affiliatewebsitecore\component\form\social_post_generator\RedditPostGeneratorForm;
use saschahuber\affiliatewebsitecore\component\form\social_post_generator\ThreadsPostGeneratorForm;
use saschahuber\affiliatewebsitecore\component\form\social_post_generator\TwitterPostGeneratorForm;
use saschahuber\saastemplatecore\component\Alert;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;
use stdClass;

class AdminEditPostingsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Social Media Beitrag erstellen/bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }


    public function buildInnerContent(){
        global $CONFIG, $DB;

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $linked_element_type = RequestHelper::reqstr('linked_element_type');
        $linked_element_id = RequestHelper::reqint('linked_element_id');

        $post_item = null;
        if($item_id) {
            $post_item = $DB->query("SELECT * FROM social_posting_feed_item where social_posting_feed_item_id = " . intval($item_id))->fetchObject();
        }

        $post_data = null;
        if($post_item){
            $app_type = $post_item->target;
            $publish_time = $post_item->posting_time;
        }
        else{
            $app_type = RequestHelper::reqstr('app', null);

            //Default-Werte füllen
            $post_item = new stdClass();
            $linked_item = getContentElementByTypeAndId($linked_element_type, $linked_element_id);
            $link = getContentPermalinkElementByTypeAndId($linked_element_type, $linked_element_id);
            $default_image_url = getContentThumbnailByTypeAndId($linked_element_type, $linked_element_id);
            if($link) {
                $post_item->title = $linked_item->title;
                $post_item->link = $CONFIG->website_domain . $link;
                $post_item->image_url = $default_image_url;

                if(strpos($post_item->image_url, $CONFIG->app_domain) !== 0){
                    $post_item->image_url = $CONFIG->app_domain . $post_item->image_url;
                }
            }

            $publish_time = (new DateTime())->format('Y-m-d\T15:00:00');
            if($linked_item){
                $publish_time = $linked_item->{$linked_element_type."_date"};

                $dateTime = new DateTime($publish_time);

                $newHour = 15;
                $newMinute = 0;

                $dateTime->setTime($newHour, $newMinute);

                // Das neue Datum und Uhrzeit als String ausgeben
                $publish_time = $dateTime->format('Y-m-d H:i');
            }
        }

        (new LinkButton("/dashboard/postings", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $platforms = [
            SocialPostingFeedItemManager::TARGET_FACEBOOK_PAGE => ['name' => 'Facebook-Seite', 'icon' => 'fab fa-facebook', 'form_class' => FacebookPagePostGeneratorForm::class],
            #SocialPostingFeedItemManager::TARGET_FACEBOOK_GROUP => ['name' => 'Facebook-Gruppe', 'icon' => 'fab fa-facebook', 'form_class' => FacebookGroupPostGeneratorForm::class],
            SocialPostingFeedItemManager::TARGET_INSTAGRAM => ['name' => 'Instagram', 'icon' => 'fab fa-instagram', 'form_class' => InstagramPostGeneratorForm::class],
            SocialPostingFeedItemManager::TARGET_TWITTER => ['name' => 'Twitter', 'icon' => 'fab fa-twitter', 'form_class' => TwitterPostGeneratorForm::class],
            SocialPostingFeedItemManager::TARGET_REDDIT => ['name' => 'Reddit', 'icon' => 'fab fa-reddit', 'form_class' => RedditPostGeneratorForm::class],
            SocialPostingFeedItemManager::TARGET_BLUESKY => ['name' => 'Bluesky', 'icon' => null, 'form_class' => BlueskyPostGeneratorForm::class],
            SocialPostingFeedItemManager::TARGET_LINKEDIN => ['name' => 'LinkedIn', 'icon' => 'fab fa-linkedin', 'form_class' => LinkedInPostGeneratorForm::class],
            SocialPostingFeedItemManager::TARGET_PINTEREST => ['name' => 'Pinterest', 'icon' => 'fab fa-pinterest', 'form_class' => PinterestPostGeneratorForm::class],
            SocialPostingFeedItemManager::TARGET_THREADS => ['name' => 'Threads', 'icon' => null, 'form_class' => ThreadsPostGeneratorForm::class]
        ];

        foreach($platforms as $target => $platform){
            (new LinkButton("/dashboard/postings/bearbeiten?app=$target&publish_time=$publish_time&linked_element_type=$linked_element_type&linked_element_id=$linked_element_id", $platform['name'], $platform['icon'], false))->display();
        }

        if(array_key_exists($app_type, $platforms)){
            $platform = $platforms[$app_type];
            $form = new $platform['form_class']($post_item);
            $form->display();
        }
        else{
            (new Alert("Bitte wähle eine App, für die du einen Post erstellen möchtest."))->display();
        }
    }
    public function getCssCode()
    {
        ?>
        <style>
            div form textarea{
                width: 100%
            }

            div#preview img{
                max-width: 100%;
                text-align: center;
                object-fit: contain;
                max-height: 400px;
            }

            .img-settings-container .image-input-option.hidden{
                display: none;
            }

            #post-image{
                max-width: 100%;
                max-height: 400px;
                object-fit: contain;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            function updatePostPreview(){
                //Get values
                let form = document.getElementById("img-settings-container");
                let appType = form.querySelector('input[name="app"]').value;
                let message = form.querySelector('textarea[name="message"]').value;

                //Update Preview
                let previewContainer = document.getElementById("preview");
                let messageField = document.getElementById("post-content");
                messageField.innerText = message;

                if(appType == "instagram" || appType == "facebook"){
                    let imageUrl = findImageSrc();
                    let imageField = document.getElementById("post-image");
                    imageField.setAttribute("src", imageUrl);
                }
            }

            function changeImageSelection(optionId, reset=true){
                //Inputs leeren
                if(reset) {
                    document.querySelector('input[name="image-background-url"]').value = null;
                    document.querySelector('input[name="image-generated"]').value = null;
                    document.querySelector('input[name="image-file"]').value = null;
                }

                //Optionen anzeigen
                for (let imgInputOption of document.getElementsByClassName("image-input-option")) {
                    imgInputOption.classList.add("hidden");
                }
                document.getElementById(optionId).classList.remove("hidden");
            }

            function findImageSrc(){
                let form = document.getElementById("create-post-form");
                let imgInputOption =  form.querySelector('select[name="post-image-selector"]').value;

                switch (imgInputOption){
                    case 'image-input-url':
                    case 'image-input-generated':
                        let generatedImgInput = document.querySelector('input[name="image-generated"]');
                        return generatedImgInput.value;
                    case 'image-input-file':
                        let fileInput = form.querySelector('input[name="image-file"]')
                        const [file] = fileInput.files
                        if (file) {
                            return URL.createObjectURL(file)
                        }
                        return null;
                }
            }

            function getFormValue(container, querySelector){
                let input = container.querySelector(querySelector);

                if(input){
                    return input.value;
                }
                return null;
            }

            function generateTempImage(e, appType){
                e.preventDefault();

                let formWrapper = document.getElementById("img-settings-container");

                let imgText = getFormValue(formWrapper, 'textarea[name="image-content"]');

                let imgTemplate = getFormValue(formWrapper, 'select[name="image-template"]');
                let imgBackgroundUrl = getFormValue(formWrapper, 'input[name="image-background-url"]');
                let verticalTextOffset = getFormValue(formWrapper, 'input[name="vertical-text-offset"]');
                let imageOverlayOpacity = getFormValue(formWrapper, 'input[name="image-overlay-opacity"]');

                let params = {
                    'app': appType,
                    'vertical-text-offset': verticalTextOffset,
                    'image-overlay-opacity': imageOverlayOpacity,
                    'image-background-url': imgBackgroundUrl,
                    'image-text': imgText,
                    'image-template': imgTemplate
                };

                doApiCall('social_posts/generateSocialImage', params, function(response){
                    if (response.length > 0) {
                        let generatedImgInput = document.querySelector('input[name="image_url"]');
                        generatedImgInput.value = response;

                        updateSocialPostPreview('image_url', response);

                        //updatePostPreview()
                    }
                });

                /*
                ajax('/dashboard?query',
                    '&task=generateSocialImage' +
                    '&app=' + appType +
                    '&vertical-text-offset=' + verticalTextOffset +
                    '&image-overlay-opacity=' + imageOverlayOpacity +
                    '&image-background-url=' + imgBackgroundUrl +
                    '&template=' + imgTemplate +
                    '&text=' + encodeURIComponent(imgText),
                    function (response) {
                        if (response.length > 0) {
                            let generatedImgInput = document.querySelector('input[name="image-generated"]');
                            generatedImgInput.value = response;
                            updatePostPreview()
                        }
                    });
                 */
            }

            document.addEventListener("DOMContentLoaded", () => {
                let postImageSelector = document.getElementById("post-image-selector")

                if(!postImageSelector){
                    return;
                }

                changeImageSelection(postImageSelector.value, false);
            });
        </script>
        <?php
    }
}