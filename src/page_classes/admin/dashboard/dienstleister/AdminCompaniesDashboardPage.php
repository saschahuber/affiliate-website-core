<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\component\PaginatedCompanyTable;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminCompaniesDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Dienstleister");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $this->items_per_page = 100;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->company_manager = new CompanyManager();

        $this->total_item_count = $this->company_manager->getTotalItemCount();

        $this->offset = ($this->page_number - 1) * $this->items_per_page;

        $this->keyword = RequestHelper::reqstr('s');
        $this->status = RequestHelper::reqstr('status');
        $this->taxonomies = RequestHelper::reqval('taxonomies');

        $this->companies = $this->company_manager->getFiltered($this->keyword, $this->status, $this->taxonomies, 'company_date', true, $this->items_per_page, $this->offset);

        (new PaginatedCompanyTable($this->companies, $this->offset, $this->items_per_page, $this->page_number, $this->total_item_count))->display();

        (LinkButton::fab("/dashboard/dienstleister/bearbeiten", 'fas fa-plus'))->display();

        $search_params = [
            'keyword' => $this->keyword,
            'status' => $this->status,
            'taxonomies' => $this->taxonomies
        ];
        (new GlobalAsyncComponentButton('<i class="fas fa-search"></i>', 'CompanySearchAsyncHandler', $search_params, null, ['fab', 'sticky']))->display();
    }
}