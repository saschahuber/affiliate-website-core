<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister;

use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\IndexFollowSettingsContainer;
use saschahuber\affiliatewebsitecore\component\form\input\DynamicImageSelector;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\component\form\MetaOgBoxes;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditCompanyCategoryDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Dienstleister-Kategorie bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;
        }

        $image_manager = new ImageManager();
        $company_manager = new CompanyManager();

        $all_taxonomies = $company_manager->getTaxonomies('title', true, null);

        $item = $company_manager->getTaxonomyById(intval($item_id));

        $taxonomy_options = array(null => '--- Keine ---');
        foreach($all_taxonomies as $taxonomy){
            if($taxonomy->id !== $item_id && $taxonomy->taxonomy_parent_id !== $item_id) {
                $taxonomy_options[$taxonomy->id] = $taxonomy->title;
            }
        }

        (new LinkButton("/dashboard/dienstleister/kategorien", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel der Kategorie', isset($item->title)?$item->title:null);
        $title_input->display();

        TextInput::required("permalink", 'Permalink', 'Permalink der Kategorie', isset($item->permalink)?$item->permalink:null)->display();

        (new TextInput("icon", 'Icon', 'Icon der Kategorie', isset($item->icon)?$item->icon:null))->display();

        (new Select("parent_taxonomy", "Übergeordnete Kategorie", $taxonomy_options, $item?$item->taxonomy_parent_id:null))->display();

        $ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, isset($item->content)?$item->content:null, 300);
        $ckeditor_area->display();

        if($item){
            (new ContentPreviewButton($CONFIG->app_domain . $company_manager->getTaxonomyPermalink($item), [
                'title' => [
                    'id' => $title_input->getId(),
                    'tinymce' => false
                ],
                'content' => [
                    'id' => $ckeditor_area->getId(),
                    'ckeditor' => true
                ],
                'meta_title' => [
                    'id' => 'component_meta_title_input',
                    'tinymce' => false
                ],
                'meta_description' => [
                    'id' => 'component_meta_description_input',
                    'tinymce' => false
                ]
            ]))->display();
        }

        ?>

        <div class="settings">
        <strong>Weitere Einstellungen</strong>
        <div>
            <?php if ($item_id  && isset($item->attachment_id)): ?>
                <label>Derzeitiges Titelbild</label>
                <img src="<?= $CONFIG->app_domain . $image_manager->getAttachmentUrl($item->attachment_id) ?>">
            <?php endif;
            if (!empty($item->og_image)): ?>
                <label>Derzeitige Open-Graph Grafik</label>
                <img src="<?= $CONFIG->app_domain . '/media/ogimg/' . $item->og_image ?>">
            <?php endif; ?>
        </div>
        <?php

        (new DynamicImageSelector(isset($item->dynamic_image_type)?$item->dynamic_image_type:null, isset($item->dynamic_image_id)?$item->dynamic_image_id:null, 'dynamic_image_type', 'dynamic_image_id', 'Dynamisches Bild'))->display();

        (new MetaOgBoxes($item))->display();

        ?>


        <?php

        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();

        (new IndexFollowSettingsContainer($item))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'company/category/save'))->display();

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(CompanyManager::TYPE_COMPANY_CATEGORY, $item_id)->display();
        }
    }
}