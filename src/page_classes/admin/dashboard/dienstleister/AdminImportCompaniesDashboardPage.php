<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminImportCompaniesDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Dienstleister importieren");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        ?>

        <div id="drop-zone" class="drop-zone">
            Ziehen Sie Ihre JSON-Datei hierher oder klicken Sie, um eine Datei auszuwählen.
            <input type="file" id="file-input" accept=".json">
        </div>

        <div class="centered">
            <?php
            (new JsButton('Dienstleister importieren', 'startCompanyImport()', ['hidden'], 'start_company_import_button'))->display();
            ?>
        </div>

        <pre id="json-output"></pre>

        <?php
    }

    public function getCssCode()
    {
        ?>
        <style>
            .drop-zone {
                width: 100%;
                max-width: 800px;
                margin: 20px auto;
                padding: 100px 10%;
                border: 2px dashed #ccc;
                border-radius: 10px;
                text-align: center;
                cursor: pointer;
                transition: background-color 0.3s ease;
            }

            .drop-zone:hover {
                background-color: #f0f0f0;
            }

            #file-input {
                display: none;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            let startCompanyImportButton = null;
            let isImportRunning = false;
            let companiesToImport = [];
            function startCompanyImport(){
                if(isImportRunning){
                    return;
                }

                isImportRunning = true;
                let importedCompanies = 0;
                let newCompanies = 0;
                let updatedCompanies = 0;
                let skippedCompanies = 0;
                let errors = 0;
                startCompanyImportButton.disabled = true;

                for (let index = 0; index < companiesToImport.length; index++) {
                    let company = companiesToImport[index];

                    let companyDataBase64 = btoa(unescape(encodeURIComponent(JSON.stringify(company))));

                    doApiCallWithResponseCode('company/import', {company_data: companyDataBase64}, function (responseCode, response) {
                        importedCompanies++;

                        try{
                            let data = JSON.parse(response)

                            if(responseCode !== 200){
                                errors++;
                            }
                            else {
                                switch (data['status']) {
                                    case "created":
                                        newCompanies++;
                                        break;
                                    case "updated":
                                        updatedCompanies++;
                                        break;
                                    case "skipped":
                                        skippedCompanies++;
                                        break;
                                }
                            }
                        }
                        catch (e) {
                            errors++;
                        }

                        startCompanyImportButton.innerText = 'Dienstleister importiert [' + importedCompanies + '/' + companiesToImport.length + '] ' +
                            '[Neu: '+newCompanies+'; Aktualisiert: '+updatedCompanies+'; Übersprungen: '+skippedCompanies+'; Fehler: '+errors+']';
                    });
                }
                startCompanyImportButton.disabled = false;
                isImportRunning = false;
            }

            document.addEventListener('DOMContentLoaded', function (){
                const dropZone = document.getElementById('drop-zone');
                const fileInput = document.getElementById('file-input');
                const jsonOutput = document.getElementById('json-output');

                startCompanyImportButton = document.getElementById('component_start_company_import_button');
                startCompanyImportButton.disabled = true;

                dropZone.addEventListener('click', () => {
                    fileInput.click();
                });

                dropZone.addEventListener('dragover', (event) => {
                    event.preventDefault();
                    dropZone.style.backgroundColor = '#e0e0e0';
                });

                dropZone.addEventListener('dragleave', () => {
                    dropZone.style.backgroundColor = '#fff';
                });

                dropZone.addEventListener('drop', (event) => {
                    event.preventDefault();
                    dropZone.style.backgroundColor = '#fff';
                    const files = event.dataTransfer.files;
                    handleFiles(files);
                });

                fileInput.addEventListener('change', (event) => {
                    const files = event.target.files;
                    handleFiles(files);
                });

                function handleFiles(files) {
                    if(isImportRunning){
                        return;
                    }

                    const file = files[0];
                    if (file && file.type === "application/json") {
                        const reader = new FileReader();
                        reader.onload = function (event) {
                            try {
                                companiesToImport = JSON.parse(event.target.result);

                                startCompanyImportButton.innerText = companiesToImport.length + ' Dienstleister importieren';
                                startCompanyImportButton.disabled = false;

                                //jsonOutput.textContent = JSON.stringify(companiesToImport, null, 2);

                                startCompanyImportButton.classList.remove('hidden');
                            } catch (error) {
                                jsonOutput.textContent = 'Fehler beim Verarbeiten der Datei: ' + error.message;
                            }
                        };
                        reader.readAsText(file);
                    } else {
                        jsonOutput.textContent = 'Bitte eine gültige JSON-Datei hochladen.';
                    }
                }
            });
        </script>
        <?php
    }
}