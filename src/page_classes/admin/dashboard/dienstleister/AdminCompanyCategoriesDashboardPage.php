<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister;

use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AdminCompanyCategoriesDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Dienstleister-Kategorien");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $this->company_manager = new CompanyManager();
        $this->taxonomies = $this->company_manager->getTaxonomies('title', true, null);

        $this->title = "Blog-Kategorien";

        $column_labels = [
            'Titel',
            'Übergeordnete Kategorie',
            'Status',
            'In Suche verstecken',
            'VG-Wort',
            'Aktionen',
        ];

        $rows = [];

        $all_taxonomies = $this->company_manager->getTaxonomies('title', true, null);

        $taxonomies = [];
        foreach($all_taxonomies as $taxonomy){
            $taxonomies[$taxonomy->id] = $taxonomy;
        }

        foreach($this->taxonomies as $item){
            $cells = [
                $item->title,
                $item->taxonomy_parent_id?$taxonomies[$item->taxonomy_parent_id]->title:'-',
                new EditableSelect('company__taxonomy', 'status', $item->id, $item->status, Manager::getStatusTypes()),
                new EditableToggle('company__taxonomy', 'hide_in_search', $item->id, $item->hide_in_search, true, "Verstecken"),
                BufferHelper::buffered(function() use ($item){
                    if($item->vg_wort_pixel_id) {
                        echo '<i class="fas fa-check-circle" style="color: green; font-size: 24px;"></i>';
                    }
                    else {
                        echo '<i class="fas fa-times-circle" style="color: darkred; font-size: 24px;"></i>';
                    }
                }),
                new FloatContainer([
                    new LinkButton("/dashboard/dienstleister?taxonomies=" . $item->id, 'Beiträge ansehen', 'fas fa-eye', false, true),
                    new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => CompanyManager::TYPE_COMPANY_CATEGORY, 'id' => $item->id]),
                    new LinkButton($CONFIG->app_domain . $this->company_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true),
                    new LinkButton("/dashboard/dienstleister/kategorien/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false)
                ]),
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (LinkButton::fab("/dashboard/dienstleister/kategorien/bearbeiten", 'fas fa-plus'))->display();
    }
}