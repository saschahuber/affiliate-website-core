<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister;

use saschahuber\affiliatewebsitecore\component\CompanyDescriptionWriterButton;
use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\CompanyOpeningHoursEditor;
use saschahuber\affiliatewebsitecore\component\form\IndexFollowSettingsContainer;
use saschahuber\affiliatewebsitecore\component\form\input\AttachmentSelector;
use saschahuber\affiliatewebsitecore\component\form\input\DynamicImageSelector;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\component\form\MetaOgBoxes;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\LimitedSelect;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;

class AdminEditCompanyDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Dienstleister bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $image_manager = new ImageManager();
        $company_manager = new CompanyManager();

        $item = $company_manager->getById($item_id);

        $all_taxonomies = $company_manager->getTaxonomies('title', true, null);

        $taxonomy_options = array(null => '--- Keine ---');
        foreach($all_taxonomies as $taxonomy){
            $taxonomy_options[$taxonomy->id] = $taxonomy->title;
        }

        $main_category = null;
        $selected_taxonomies = array();
        if($item) {
            foreach ($item->taxonomies as $taxonomy) {
                $selected_taxonomies[] = $taxonomy->id;
                if($taxonomy->is_primary_taxonomy) {
                    $main_category = $taxonomy;
                }
            }
        }

        (new LinkButton("/dashboard/dienstleister", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel des Dienstleisters', isset($item->title)?$item->title:null);
        $title_input->display();

        (TextInput::required("permalink", 'Permalink', 'Permalink des Dienstleisters', isset($item->permalink)?$item->permalink:null))->display();

        (new Toggle('is_featured', "Hervorgehoben", (($item && isset($item->is_featured))?$item->is_featured:false), false, true))->display();

        $address_input = new TextInput("address", 'Adresse', 'Adresse des Dienstleisters', isset($item->address)?$item->address:null);
        $address_input->display();

        (new Row([
            new Column(new TextInput('latitude', 'Latitude', null, isset($item->latitude)?str_replace(',', '.', $item->latitude):null), ["col-12", "col-md-6"]),
            new Column(new TextInput('longitude', 'Longitude', null, isset($item->longitude)?str_replace(',', '.', $item->longitude):null), ["col-12", "col-md-6"])
        ]))->display();

        $website_input = new TextInput("website", 'Website', 'Website des Dienstleisters', isset($item->website)?$item->website:null);
        $website_input->display();

        $email_input = new TextInput("email", 'E-Mail Adresse', 'E-Mail-Adresse des Dienstleisters', isset($item->email)?$item->email:null);
        $email_input->display();

        $phone_input = new TextInput("phone_number", 'Telefonnummer', 'Telefonnummer des Dienstleisters', isset($item->phone_number)?$item->phone_number:null);
        $phone_input->display();

        (new LimitedSelect("kategorien", "Kategorien", $taxonomy_options, $selected_taxonomies, true, false, false, false, null, CompanyManager::getMaxCategories($item?$item->package:null)))->display();

        (new Select("main_category", "Primäre Kategorie", $taxonomy_options, $main_category?$main_category->id:null, false))->display();

        $ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, $item?$item->content:null, 300);
        $ckeditor_area->display();

        if($item_id !== null) {
            #(new CompanyDescriptionWriterButton($title_input->getId(), $address_input->getId(), $email_input->getId(), $phone_input->getId(),
            #    $website_input->getId(), '', $item_id, $ckeditor_area->getId(), ChatGenerationTemplate::MODEL_CHAT_GPT, 4096))->display();

            #(new CompanyDescriptionWriterButton($title_input->getId(), $address_input->getId(), $email_input->getId(), $phone_input->getId(),
            #    $website_input->getId(), '', $item_id, $ckeditor_area->getId(), ChatGenerationTemplate::MODEL_CHAT_GPT_4_TURBO, 4096))->display();

            (new CompanyDescriptionWriterButton($title_input->getId(), $address_input->getId(), $email_input->getId(), $phone_input->getId(),
                $website_input->getId(), '', $item_id, $ckeditor_area->getId(), ChatGenerationTemplate::MODEL_CHAT_GPT_4_O, 4096))->display();
        }

        if($item){
            (new ContentPreviewButton($CONFIG->app_domain . $company_manager->generatePermalink($item), [
                'title' => [
                    'id' => $title_input->getId(),
                    'tinymce' => false
                ],
                'content' => [
                    'id' => $ckeditor_area->getId(),
                    'ckeditor' => true
                ],
                'meta_title' => [
                    'id' => 'component_meta_title_input',
                    'tinymce' => false
                ],
                'meta_description' => [
                    'id' => 'component_meta_description_input',
                    'tinymce' => false
                ]
            ]))->display();
        }

        ?>

        <br>
        <br>

        <?php
        (new CompanyOpeningHoursEditor($item))->display();

        ?>

        <br>

        <div class="settings">
        <strong>Weitere Einstellungen</strong>

        <?php

        (new MetaOgBoxes($item))->display();

        #(new AttachedFaqSelect($item?$item->attached_faq_id:null))->display();

        ?>


        <?php

        (new AttachmentSelector($item?$item->attachment_id:null))->display();

        (new DynamicImageSelector(isset($item->dynamic_image_type)?$item->dynamic_image_type:null, isset($item->dynamic_image_id)?$item->dynamic_image_id:null, 'dynamic_image_type', 'dynamic_image_id', 'Dynamisches Bild'))->display();

        (new IndexFollowSettingsContainer($item))->display();

        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'company/save'))->display();

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(CompanyManager::TYPE_COMPANY_CATEGORY, $item_id)->display();
        }
    }
}