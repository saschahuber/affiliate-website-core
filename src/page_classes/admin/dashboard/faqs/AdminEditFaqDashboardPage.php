<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\faqs;

use saschahuber\affiliatewebsitecore\component\FaqGeneratorButton;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableNumberInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextarea;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;

class AdminEditFaqDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("FAQ Fragen/Antworten bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        if($item_id) {
            (new FaqGeneratorButton($item_id))->display();
        }

        $faq = $DB->query("SELECT * FROM faq where faq_id = ".intval($item_id))->fetchObject();

        $faq_question_query = $DB->query("SELECT faq__question.* FROM faq__question
                       join faq on faq__question.faq_id = faq.faq_id
                       where faq.faq_id = ".intval($item_id)."
                       order by item_order ASC, faq__question_id ASC");
        $faq_questions = [];
        while($item = $faq_question_query->fetchObject()){
            $faq_questions[] = $item;
        }

        $column_labels = [
            'Order',
            'Frage',
            'Antwort',
            'Aktionen'
        ];

        $rows = [];

        foreach($faq_questions as $faq_question){
            $cells = [
                new EditableNumberInput('faq__question', 'item_order', $faq_question->id, $faq_question->item_order?:0),
                new EditableTextInput('faq__question', 'question', $faq_question->id, $faq_question->question),
                new FloatContainer([
                    new EditableTextarea('faq__question', 'answer', $faq_question->id, $faq_question->answer)
                ], ['answer-container']),
                new DeleteButton('faq__question', $faq_question->id, 2)
            ];
            $rows[] = new TableRow($cells);
        }

        $table = new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']);

        $input_config = [
            'item_order' => ['type' => 'number', 'label' => 'Sortierung der Fragen'],
            'question' => ['type' => 'text', 'label' => 'Frage'],
            'answer' => ['type' => 'wysiwyg', 'label' => 'Antwort'],
            'faq_id' => ['type' => 'hidden', 'value' => $faq->id]
        ];
        $insert_entry_button = new InsertEntryButton('faq__question', $input_config, "Neue Frage mit Antwort");

        (new ElevatedCard([
            new LinkButton("/dashboard/faq", "Zurück zur Übersicht", "fas fa-arrow-circle-left", false),
            new Text("FAQ: {$faq->title}", 'h1'),
            new BreakComponent(),
            $table,
            new BreakComponent(),
            $insert_entry_button
        ]))->display();
    }
}