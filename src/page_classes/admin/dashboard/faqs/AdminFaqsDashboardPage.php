<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\faqs;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class AdminFaqsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("FAQs");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        $faqs = array();
        $dbquery = $DB->query("SELECT * FROM faq order by title ASC");
        while($faq = $dbquery->fetchObject()){
            $faqs[] = $faq;
        }

        $input_config = [
            'title' => ['type' => 'text', 'label' => 'Titel']
        ];
        (new InsertEntryButton('faq', $input_config, "FAQ hinzufügen"))->display();


        $column_labels = [
            'Titel',
            'Aktiv',
            'Bearbeiten',
        ];

        $rows = [];

        foreach($faqs as $faq){
            $cells = [
                new EditableTextInput('faq', 'title', $faq->id, $faq->title),
                new EditableToggle('faq', 'active', $faq->id, $faq->active),
                new FloatContainer([
                    new LinkButton("/dashboard/faq/bearbeiten/" . $faq->id, 'Bearbeiten', 'fas fa-pen', false, false)
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}