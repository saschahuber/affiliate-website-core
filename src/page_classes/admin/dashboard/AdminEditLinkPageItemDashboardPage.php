<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use DateTime;
use saschahuber\affiliatewebsitecore\block\LinkPageEditingBlock;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminEditLinkPageItemDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Link-Seiten-Element bearbeiten/erstellen");
    }
    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $this->title = "Link-Seiten-Element bearbeiten/erstellen";


        $linked_item_type = null;
        $linked_item_id = null;

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
        }
        else{
            $item_id = null;

            $linked_item_type = RequestHelper::reqstr('linked_element_type');
            $linked_item_id = RequestHelper::reqint('linked_element_id');
        }

        $image_manager = new ImageManager();
        $link_page_item_manager = new LinkPageItemManager();

        $item = $link_page_item_manager->getById($item_id);

        (new LinkButton("/dashboard/seiten/links", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        //Default-Werte füllen
        $linked_item = getContentElementByTypeAndId($linked_item_type, $linked_item_id);
        $default_link = getContentPermalinkElementByTypeAndId($linked_item_type, $linked_item_id);
        if($default_link) {
            $default_link = $CONFIG->website_domain . $default_link;
        }

        $preset_date = (new DateTime())->format('Y-m-d\TG:i');
        if($linked_item){
            $preset_date = $linked_item->{$linked_item_type."_date"};

            $dateTime = new DateTime($preset_date);

            $newHour = 12;
            $newMinute = 0;

            $dateTime->setTime($newHour, $newMinute);

            // Das neue Datum und Uhrzeit als String ausgeben
            $preset_date = $dateTime->format('Y-m-d H:i');
        }

        $item_title = isset($item->title)?$item->title:($linked_item?$linked_item->title:null);
        $item_date = isset($item->link_page_item_date)?$item->link_page_item_date:$preset_date;
        $item_content = $item?$item->content:null;
        $item_button_text = $item?$item->button_text:null;
        $item_button_url = $item?$item->button_url:$default_link;
        $linked_item_type = $item?$item->linked_item_type:($linked_item_type??null);
        $linked_item_id = $item?$item->linked_item_id:($linked_item_id??null);
        $attachment_id = $item?$item->attachment_id:null;

        (new LinkPageEditingBlock($item_id, $item_title, $item_date, $item_content, $item_button_text, $item_button_url, $linked_item_type, $linked_item_id, $attachment_id))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'page/link/save'))->display();
    }
}