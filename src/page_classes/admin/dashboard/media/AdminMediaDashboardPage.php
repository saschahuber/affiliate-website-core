<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\builder\ui\FormBuilder;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentElement;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\StaticHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class AdminMediaDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Medien");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB, $CONFIG;

        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $image_manager = new ImageManager();

            $title = RequestHelper::reqstr("title");
            $file_name = RequestHelper::reqstr("file_name");
            $alt_text = RequestHelper::reqstr("alt_text");
            $description = RequestHelper::reqstr("description");
            $provider_name = RequestHelper::reqstr("provider_name");
            $copyright_info = RequestHelper::reqstr("copyright_info");
            $attachment_id = RequestHelper::reqstr("attachment_id");
            $use_in_attachment_generator = RequestHelper::reqbool("use_in_attachment_generator");

            if($attachment_id) {
                $image_manager->updateAttachment($attachment_id, $description, $provider_name, $copyright_info, $title, $alt_text, $use_in_attachment_generator);
            }
            else {
                $image_file = $_FILES['image-file'];
                $success = $image_manager->addAttachmentFromFile($image_file, $description, $provider_name, $copyright_info, $title, $alt_text, $use_in_attachment_generator, $file_name);
                if(!$success){
                    ErrorHelper::http(ErrorHelper::HTTP_ERROR_BAD_REQUEST, 'Medien-Datei konnte nicht gespeichert werden');
                }
            }

            UrlHelper::redirect("/dashboard/medien/", 302);
        }

        $this->items_per_page = 50;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->offset = $this->page_number * $this->items_per_page;

        $LAYOUT_MANAGER = new LayoutManager();
        $image_manager = new ImageManager();

        (GlobalAsyncComponentButton::fab("fas fa-plus", 'AddEditMediaHandler', []))->display();

        $keyword = RequestHelper::reqstr('s');

        $page_count = max(($DB->query('SELECT count(*) as anzahl from attachment')->fetchObject()->anzahl), 1) / $this->items_per_page;

        if($page_count > 1) {
            (new Pagination('/dashboard/medien/' . Pagination::PAGINATION_PLACEHOLDER . '?' . $_SERVER['QUERY_STRING'], $page_count, $this->page_number))->display();
        }

        $image_contents = array();
        foreach($image_manager->getAttachments($keyword, $this->items_per_page, ($this->page_number-1)*$this->items_per_page) as $attachment){
            #$image = new Image($CONFIG->website_domain . $attachment->src . '?width=350');

            $image = BufferHelper::buffered(function () use ($attachment){
                global $CONFIG;

                ?>
                <div style="position: relative; cursor: pointer;">
                    <img src="<?=(StaticHelper::getResizedImgUrl($CONFIG->app_domain . $attachment->src, 350))?>" loading="lazy">

                    <?php if($attachment->use_in_attachment_generator): ?>
                        <span style="position: absolute; bottom: 10px; right: 10px;"
                              class="badge bg-primary">
                        <i style="color: #fff; padding: 5px;" class="fas fa-photo-video"></i>
                </span>
                    <?php endif; ?>

                    <?php if($attachment->generator_data !== null): ?>
                        <span style="position: absolute; left: 10px; top: 10px; cursor: pointer;"
                              class="badge bg-primary"
                              onclick="window.open('/dashboard/medien/bild-generator/<?=$attachment->id?>', 'blank')">
                        <i style="color: #fff; padding: 5px;" class="fas fa-edit"></i>
                            </a>
                </span>
                    <?php endif; ?>

                    <?php if(($attachment->provider_name !== null && strlen($attachment->provider_name))
                        || ($attachment->copyright_info !== null && strlen($attachment->copyright_info))): ?>
                        <span style="position: absolute; bottom: 10px; left: 10px;"
                              class="badge bg-primary">
                        <i style="color: #fff; padding: 5px;" class="fas fa-copyright"></i>
                </span>
                    <?php endif; ?>
                </div>
                <?php
            });

            $image_content = new GlobalAsyncComponentElement($image, 'AddEditMediaHandler', ['id' => $attachment->id]);

            $image_contents[] = new Card($image_content, ["media-grid-item"]);
        }
        (new FormBuilder())
            ->withInputRow(new TextInput("s", 'Suchbegriff', null, $keyword))
            ->withMethod(Form::METHOD_GET)
            ->build()
            ->display();

        (new BreakComponent())->display();

        (new MasonryContainer($image_contents, ['col-12', 'col-md-4', 'col-lg-3', 'col-xl-2', 'padding-5']))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            div.media-grid-item{
                display: table-cell;
                text-align: center;
            }

            div.media-grid-item a{
                vertical-align: middle;
                max-height: 200px;
            }

            div.media-grid-item img{
                max-width: 100%;
                max-height: 100%;
                object-fit: contain;
                padding: 5px;
            }

            div.media-grid-item:hover{
                transform: scale(1.05);
                box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 16px 22px 0 rgb(0 0 0 / 19%) !important;
                z-index: 1;
            }

            form img{
                width: 100%;
                height: 100%;
                max-width: 400px;
                max-height: 300px;
                object-fit: contain;
            }

            .card-body {
                padding: 0;
            }

            img.img-media-editor {
                max-height: 600px;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            var refreshCounter = 0;

            var intervalId = window.setInterval(function(){
                refreshMasonry();

                if(refreshCounter > 10){
                    clearInterval(intervalId);
                }

                refreshCounter++;
            }, 1000);
        </script>
        <?php
    }
}