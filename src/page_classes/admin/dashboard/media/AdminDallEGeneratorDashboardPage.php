<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\builder\ui\FormBuilder;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentElement;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\StaticHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\model\ImageGenerationTemplate;

class AdminDallEGeneratorDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Dall-E Bild erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $image_manager = new ImageManager();

            $title = RequestHelper::reqstr("title");
            $alt_text = RequestHelper::reqstr("alt_text");
            $description = RequestHelper::reqstr("description");
            $provider_name = RequestHelper::reqstr("provider_name");
            $copyright_info = RequestHelper::reqstr("copyright_info");
            $attachment_id = RequestHelper::reqstr("attachment_id");
            $use_in_attachment_generator = RequestHelper::reqbool("use_in_attachment_generator");

            if($attachment_id) {
                $image_manager->updateAttachment($attachment_id, $description, $provider_name, $copyright_info, $title, $alt_text, $use_in_attachment_generator);
            }
            else {
                $image_file = $_FILES['image-file'];
                $success = $image_manager->addAttachmentFromFile($image_file, $description, $provider_name, $copyright_info, $title, $alt_text, $use_in_attachment_generator);
                if(!$success){
                    ErrorHelper::http(ErrorHelper::HTTP_ERROR_BAD_REQUEST, 'Medien-Datei konnte nicht gespeichert werden');
                }
            }

            UrlHelper::redirect("/dashboard/medien/", 302);
        }

        $form = BufferHelper::buffered(function(){
            $textarea = new TextArea('prompt_input', 'Prompt');

            $size_values = [
                #ImageGenerationTemplate::SIZE_256X256 => ImageGenerationTemplate::SIZE_256X256,
                #ImageGenerationTemplate::SIZE_512X512 => ImageGenerationTemplate::SIZE_512X512,
                ImageGenerationTemplate::SIZE_1024X1024 => ImageGenerationTemplate::SIZE_1024X1024,
                ImageGenerationTemplate::SIZE_1792X1024 => ImageGenerationTemplate::SIZE_1792X1024,
                ImageGenerationTemplate::SIZE_1024X1792 => ImageGenerationTemplate::SIZE_1024X1792
            ];

            $size_selection = new Select('size_selector', 'Bildformat', $size_values, ImageGenerationTemplate::SIZE_1024X1024);

            $container = new GridContainer([
                $textarea,
                $size_selection
            ], 1);

            $container->display();

            BreakComponent::break();

            (new JsButton('Bild generieren', "generateAiImage(this)"))->display();
        });

        (new Row([
            new Column(new ElevatedCard($form), ['col col-lg-6']),
            new Column(new ElevatedCard('<img id="image-preview"><br><p id="revised-prompt"></p>'), ['col col-lg-6'])
        ]))->display();
    }

    public function getJsCode()
    {
        ?>
        <script>
            function generateAiImage(button){
                let textarea = document.querySelector('textarea[name="prompt_input"]');
                let sizeDropdown = document.querySelector('select[name="size_selector"]');

                button.innerHTML = '<i class="fas fa-spinner fa-spin"></i> Bild generieren';

                let params = {
                    prompt: btoa(textarea.value),
                    size: sizeDropdown.value
                };

                let previewImage = document.getElementById('image-preview');
                previewImage.style.display = 'none';

                let revisedPrompt = document.getElementById('revised-prompt');
                revisedPrompt.style.display = 'none';

                doApiCall('ai_image_generator/generate', params, function(response){
                    let jsonData = JSON.parse(response);

                    previewImage.src = jsonData.url;
                    revisedPrompt.innerText = jsonData.revised_prompt;

                    previewImage.style.display = 'block';
                    revisedPrompt.style.display = 'block';

                    button.innerHTML = "Bild generieren";
                });
            }
        </script>
        <?php
    }
}