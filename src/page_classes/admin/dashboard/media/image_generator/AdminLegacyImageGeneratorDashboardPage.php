<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator;

use saschahuber\affiliatewebsitecore\component\form\AttachmentImageSettings;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AttachmentImageGeneratorService;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use stdClass;

class AdminLegacyImageGeneratorDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Neues Attachment erstellen/bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    private function getImageTemplateOptions(){
        $attachment_generator_service = new AttachmentImageGeneratorService();
        return $attachment_generator_service->getImageTemplateOptions();
    }

    public function buildInnerContent(){
        global $CONFIG;

        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $image_data = $_REQUEST;

            $attachment_id = RequestHelper::reqint('attachment_id');
            $filename = UrlHelper::alias(RequestHelper::reqstr('filename', null));
            $image_content = RequestHelper::reqstr('image-content', null);

            $attachment_image_generator_service = new AttachmentImageGeneratorService();

            $stock_attachment_manager = new StockImageManager();
            $attachment_manager = new ImageManager();

            $attachment = $attachment_manager->getAttachment($attachment_id);

            if($attachment && $attachment->generator_data === null){
                ErrorHelper::http(ErrorHelper::HTTP_ERROR_NO_PERMISSION, 'Attachment darf nicht bearbeitet werden!');
            }

            $create_attachment = false;
            if(!$attachment){
                $create_attachment = true;
                $attachment = new stdClass();
            }

            $new_attachment_title = UrlHelper::alias($filename?:$image_content);

            //Attachment umbenennen
            $new_attachment_path = "/attachment/$filename.png";

            $template = RequestHelper::reqstr('image-template');
            $attachment_generator_service = new AttachmentImageGeneratorService();


            $image_template = $attachment_image_generator_service->getImageTemplates()[$template];

            $stock_source_url = null;
            $stock_description = null;
            $stock_provider_name = null;
            $stock_copyright_info = null;
            switch($image_template['image_type']){
                case "stock":
                    $source_attachment = $stock_attachment_manager->getAttachment($image_template['image_id']);
                    $stock_source_url = $source_attachment->source_url;
                    $stock_description = $source_attachment->description;
                    $stock_provider_name = $source_attachment->provider_name;
                    $stock_copyright_info = $source_attachment->copyright_info;
                    break;
            }

            $image_data['image-overlay-opacity'] = floatval(ArrayHelper::getArrayValue($image_data, 'image-overlay-opacity', 0))/100;
            $image_data['show_logo'] = RequestHelper::reqflag('show_logo');

            $generated_path = $attachment_generator_service->generateAttachmentImage($template, $image_content, 'tmp', $image_data);
            $generated_path = str_replace($CONFIG->app_domain . GENERATED_IMG_SRC, GENERATED_IMG_DIR, $generated_path);

            if(file_exists(MEDIA_DIR . $new_attachment_path)){
                unlink(MEDIA_DIR . $new_attachment_path);
            }

            rename($generated_path, MEDIA_DIR . $new_attachment_path);

            $attachment->title = $filename;
            $attachment->alt_text = $image_content;
            $attachment->generator_data = json_encode($image_data);
            $attachment->description = $stock_description;
            $attachment->provider_name = $stock_provider_name;
            $attachment->copyright_info = $stock_copyright_info;
            $attachment->file_path = $new_attachment_path;
            $attachment->file_name = "$filename.png";

            if($create_attachment) {
                $attachment_id = $attachment_manager->createItem($attachment);
            }
            else{
                $attachment_manager->updateItem($attachment);
            }

            UrlHelper::redirect("/dashboard/medien/bild-generator/$attachment_id", 302);
        }

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $attachment_manager = new ImageManager();

        $attachment = $attachment_manager->getById($item_id);

        if($attachment && $attachment->generator_data === null){
            (new Text("Dieses Attachment kann nicht bearbeitet werden", "h2"))->display();
            (new Image($attachment_manager->getAttachmentUrl($item_id)))->display();
        }
        else {
            $form_column = new Column(new ElevatedCard([
                new Text("Attachment erstellen/bearbeiten", 'h3'),
                new Form([
                    [new HiddenInput("task", "create-image-post")],
                    [new AttachmentImageSettings($this->getImageTemplateOptions(), $attachment)],
                    [new SubmitButton('<i class="fas fa-save"></i> Bild speichern')]
                ], '/dashboard/medien/bild-generator-alt', Form::METHOD_POST)
            ]), ["col-12", "col-md-6"]);

            $preview_content = BufferHelper::buffered(function() use ($attachment_manager, $attachment){
                ?>
                <h3>Vorschau deines Bildes</h3>
                <img <?= $attachment!==null?'src="'.$attachment_manager->getAttachmentUrl($attachment->id).'?v='.time().'"':''?> id="post-image" style="max-height: unset;">
                <?php
            });

            $preview_column = new Column(new ElevatedCard($preview_content), ["col-12", "col-md-6"]);


            $columns = [$form_column, $preview_column];

            (new Row($columns))->display();
        }
    }

    public function getCssCode()
    {
        ?>
        <style>
            div form textarea{
                width: 100%
            }

            div#preview img{
                max-width: 100%;
                text-align: center;
                object-fit: contain;
                max-height: 400px;
            }

            .img-settings-container .image-input-option.hidden{
                display: none;
            }

            #post-image{
                max-width: 100%;
                max-height: 400px;
                object-fit: contain;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            function updatePostPreview(){
                let imageUrl = findImageSrc();
                let imageField = document.getElementById("post-image");
                imageField.setAttribute("src", imageUrl);
            }

            function findImageSrc(){
                let generatedImgInput = document.querySelector('input[name="image-generated"]');
                return generatedImgInput.value;
            }

            function getFormValue(container, querySelector){
                let input = container.querySelector(querySelector);

                if(input){
                    return input.value;
                }
                return null;
            }

            function generateTempAttachmentImage(button, e){
                e.preventDefault();

                button.innerHTML = '<i class="fas fa-cog fa-spin"></i> Bild wird generiert...';

                let formWrapper = document.getElementById("img-settings-container");

                let imgText = getFormValue(formWrapper, 'textarea[name="image-content"]');
                let imgTemplate = getFormValue(formWrapper, 'input[name="image-template"]');
                let imgBackgroundUrl = getFormValue(formWrapper, 'input[name="image-background-url"]');
                let verticalTextOffset = getFormValue(formWrapper, 'input[name="vertical-text-offset"]');
                let verticalLogoOffset = getFormValue(formWrapper, 'input[name="vertical-logo-offset"]');
                let imageOverlayOpacity = getFormValue(formWrapper, 'input[name="image-overlay-opacity"]');

                let showLogo = formWrapper.querySelector('input[name="show_logo"]').checked;
                let showLogoAsWatermark = formWrapper.querySelector('input[name="show_logo_as_watermark"]').checked;

                let params = {
                    'app': 'image',
                    'vertical-text-offset': verticalTextOffset,
                    'vertical-logo-offset': verticalLogoOffset,
                    'image-overlay-opacity': imageOverlayOpacity,
                    'image-background-url': imgBackgroundUrl,
                    'image-content': imgText,
                    'image-template': imgTemplate,
                    'show_logo': showLogo,
                    'show_logo_as_watermark': showLogoAsWatermark,
                };

                doApiCall('attachment_generator/generateAttachment', params, function(response){
                    if (response.length > 0) {
                        let generatedImgInput = document.querySelector('input[name="image-generated"]');
                        generatedImgInput.value = response;
                        updatePostPreview()
                        button.innerHTML = '<i class="fas fa-cog"></i> Bild generieren';
                    }
                });
            }
        </script>
        <?php
    }
}