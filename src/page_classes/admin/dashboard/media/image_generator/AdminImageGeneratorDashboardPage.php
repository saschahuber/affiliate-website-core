<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator;

use saschahuber\affiliatewebsitecore\component\image_generator\ImageGeneratorForm;
use saschahuber\affiliatewebsitecore\component\image_generator\layer\AttachmentLayer;
use saschahuber\affiliatewebsitecore\component\image_generator\layer\TextImageLayer;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class AdminImageGeneratorDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Neues Attachment erstellen/bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $generated_image_url = RequestHelper::reqstr('generated_image_url');
            $file_name = RequestHelper::reqstr('file_name');
            $title = RequestHelper::reqstr('title');
            $alt_text = RequestHelper::reqstr('alt_text');
            $description = RequestHelper::reqstr('description');
            $provider_name = RequestHelper::reqstr('provider_name');
            $copyright_info = RequestHelper::reqstr('copyright_info');

            if($generated_image_url === null || strlen($generated_image_url) < 1){
                ErrorHelper::http(403, 'generated_image_url darf nicht leer sein');
            }

            if($file_name === null || strlen($file_name) < 1){
                ErrorHelper::http(403, 'file_name darf nicht leer sein');
            }

            //Attachment hinzufügen

            $generated_image_path = str_replace($CONFIG->app_domain . '/data', DATA_DIR, $generated_image_url);

            $attachment_file_path = "/attachment/$file_name.png";
            if(file_exists($attachment_file_path . $attachment_file_path)){
                $attachment_file_path = "/attachment/$file_name-".uniqid().".png";
            }

            //Datei kopieren
            copy($generated_image_path, MEDIA_DIR . $attachment_file_path);

            $image_manager = new ImageManager();
            $attachment_id = $image_manager->addAttachment($attachment_file_path, $title, $alt_text);
            $image_manager->updateAttachment($attachment_id, $description, $provider_name, $copyright_info, $title, $alt_text, false);
            UrlHelper::redirect('/dashboard/medien');
        }

        $width = null;
        $height = null;
        $background_color = null;

        $this->image_generator_form = new ImageGeneratorForm([
            new AttachmentLayer(),
            new TextImageLayer()
        ], $width, $height, $background_color);

        (new LinkButton('/dashboard/medien/bild-generator/produkt-kategorie', 'Produkt-Kategorie'))->display();
        (new LinkButton('/dashboard/medien/bild-generator/fancy-header', 'Fancy-Header'))->display();
        (new LinkButton('/dashboard/medien/bild-generator/thumbnail', 'Thumbnail'))->display();

        $this->image_generator_form->displayForm();
    }
}