<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator;

use saschahuber\affiliatewebsitecore\component\image_generator\ImageGeneratorForm;
use saschahuber\affiliatewebsitecore\component\image_generator\layer\AttachmentLayer;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextInput;

class AdminFancyHeaderImageGeneratorDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Fancy-Header erstellen/bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (new Form([
            [new HiddenInput('generated_image_url', null)],
            [new TextInput('file_name', 'Name der Datei (ohne Dateiendung [.png/.jpg])', null, null, false, true)],
            [new TextInput('title', 'Titel des Bildes', null, null, false, false)],
            [new TextInput('alt_text', 'Alt-Text des Bildes', null, null, false, false)],
            [new TextInput('description', 'Beschreibung des Bildes', null, null, false, false)],
            [new TextInput('provider_name', 'Stock-Anbieter', null, null, false, false)],
            [new TextInput('copyright_info', 'Copyright-Info', null, null, false, false)],
            [new SubmitButton('<i class="fas fa-save"></i> Bild speichern')]
        ], '/dashboard/medien/bild-generator', Form::METHOD_POST))->display();

        (new ImageGeneratorForm([
            new AttachmentLayer(),
        ], 1920, 650, '#000000', 'generated_image_url'))->displayForm();
    }
}