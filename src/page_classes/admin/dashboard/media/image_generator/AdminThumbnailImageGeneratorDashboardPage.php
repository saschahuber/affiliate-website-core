<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator;

use saschahuber\affiliatewebsitecore\component\image_generator\ImageGeneratorForm;
use saschahuber\affiliatewebsitecore\component\image_generator\layer\AttachmentLayer;
use saschahuber\affiliatewebsitecore\component\image_generator\layer\BoxImageLayer;
use saschahuber\affiliatewebsitecore\component\image_generator\layer\FooterImageLayer;
use saschahuber\affiliatewebsitecore\component\image_generator\layer\TextImageLayer;
use saschahuber\affiliatewebsitecore\component\image_generator\layer\WatermarkLogoImageLayer;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class AdminThumbnailImageGeneratorDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Thumbnail erstellen/bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (new ImageGeneratorForm([
            new AttachmentLayer(),
            new BoxImageLayer(),
            new WatermarkLogoImageLayer(),
            new FooterImageLayer()
        ]))->displayForm();
    }
}