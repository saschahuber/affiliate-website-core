<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\manager\IconManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\builder\ui\FormBuilder;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentElement;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\StaticHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class AdminIconDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Icons");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB, $CONFIG;

        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $icon_manager = new IconManager();

            $title = RequestHelper::reqstr("title");
            $file_name = RequestHelper::reqstr("file_name");
            $alt_text = RequestHelper::reqstr("alt_text");
            $source_url = RequestHelper::reqstr("source_url");
            $description = RequestHelper::reqstr("description");
            $provider_name = RequestHelper::reqstr("provider_name");
            $copyright_info = RequestHelper::reqstr("copyright_info");
            $attachment_id = RequestHelper::reqstr("attachment_id");

            if($attachment_id) {
                $icon_manager->updateIcon($attachment_id, $source_url, $description, $provider_name, $copyright_info, $title, $alt_text);
            }
            else {
                $image_file = $_FILES['image-file'];
                $success = $icon_manager->addIconFromFile($image_file, $source_url, $description, $provider_name, $copyright_info, $title, $alt_text, $file_name);
                if(!$success){
                    ErrorHelper::http(ErrorHelper::HTTP_ERROR_BAD_REQUEST, 'Icon konnte nicht gespeichert werden');
                }
            }

            UrlHelper::redirect("/dashboard/medien/icon/", 302);
        }

        $this->items_per_page = 50;

        if(count($this->path) > 3){
            $this->page_number = intval($this->path[3]);
        }
        else{
            $this->page_number = 1;
        }

        $this->offset = $this->page_number * $this->items_per_page;

        $LAYOUT_MANAGER = new LayoutManager();
        $icon_manager = new IconManager();

        (GlobalAsyncComponentButton::fab("fas fa-plus", 'AddEditIconHandler', []))->display();

        $keyword = RequestHelper::reqstr('s');
        $page_number = RequestHelper::reqint('page', 1);

        $page_count = max(($DB->query('SELECT count(*) as anzahl from icon')->fetchObject()->anzahl), 1) / $this->items_per_page;

        if($page_count > 1) {
            (new Pagination('/dashboard/medien/icon/' . Pagination::PAGINATION_PLACEHOLDER . '?' . $_SERVER['QUERY_STRING'], $page_count, $this->page_number))->display();
        }

        $image_contents = array();
        foreach($icon_manager->getIcons($keyword, $this->items_per_page, ($this->page_number-1)*$this->items_per_page) as $attachment){
            $image = new Image($CONFIG->website_domain . $attachment->src, 150);

            $image_content = new GlobalAsyncComponentElement($image, 'AddEditIconHandler', ['id' => $attachment->id]);

            $image_contents[] = new Card($image_content, ["media-grid-item"]);
        }

        (new FormBuilder())
            ->withInputRow(new TextInput("s", 'Suchbegriff', null, $keyword))
            ->withMethod(Form::METHOD_GET)
            ->build()
            ->display();

        (new BreakComponent())->display();

        (new MasonryContainer($image_contents, ['col-12', 'col-md-4', 'col-lg-2', 'col-xl-1', 'padding-5']))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            div.media-grid-item{
                display: table-cell;
                text-align: center;
                background-color: #eee;
            }

            div.media-grid-item a{
                vertical-align: middle;
                max-height: 100px;
            }

            div.media-grid-item img{
                max-width: 100%;
                max-height: 100%;
                height: 150px;
                width: 150px;
                object-fit: contain;
                padding: 5px;
            }

            div.media-grid-item:hover{
                transform: scale(1.05);
                box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 16px 22px 0 rgb(0 0 0 / 19%) !important;
                z-index: 1;
            }

            form img{
                width: 100%;
                height: 100%;
                max-width: 400px;
                max-height: 300px;
                object-fit: contain;
            }

            .card-body {
                padding: 0;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            var refreshCounter = 0;

            var intervalId = window.setInterval(function(){
                refreshMasonry();

                if(refreshCounter > 10){
                    clearInterval(intervalId);
                }

                refreshCounter++;
            }, 1000);
        </script>
        <?php
    }
}