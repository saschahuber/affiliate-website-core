<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\builder\ui\FormBuilder;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentElement;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\StaticHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class AdminDallEOverviewDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Dall-E");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG, $DB;

        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $ai_image_manager = new AiImageManager();

            $title = RequestHelper::reqstr("title");
            $prompt = RequestHelper::reqstr("prompt");
            $alt_text = RequestHelper::reqstr("alt_text");
            $description = RequestHelper::reqstr("description");
            $provider_name = RequestHelper::reqstr("provider_name");
            $copyright_info = RequestHelper::reqstr("copyright_info");
            $attachment_id = RequestHelper::reqstr("attachment_id");
            $use_in_attachment_generator = RequestHelper::reqbool("use_in_attachment_generator");

            if($attachment_id) {
                $ai_image_manager->updateAttachment($attachment_id, $description, $provider_name, $alt_text, $use_in_attachment_generator);
            }
            else {
                $image_file = $_FILES['image-file'];
                $success = $ai_image_manager->addAttachmentFromFile($image_file, $description, $title, $alt_text, $use_in_attachment_generator);
                if(!$success){
                    ErrorHelper::http(ErrorHelper::HTTP_ERROR_BAD_REQUEST, 'Medien-Datei konnte nicht gespeichert werden');
                }
            }

            UrlHelper::redirect("/dashboard/medien/dalle", 302);
        }

        $this->items_per_page = 50;

        if(count($this->path) > 3){
            $this->page_number = intval($this->path[3]);
        }
        else{
            $this->page_number = 1;
        }

        $this->offset = $this->page_number * $this->items_per_page;

        $ai_image_manager = new AiImageManager();

        (LinkButton::fab("/dashboard/medien/dalle/erstellen", "fas fa-plus"))->display();

        $keyword = RequestHelper::reqstr('s');
        $page_number = RequestHelper::reqint('page', 1);

        $page_count = max(($DB->query('SELECT count(*) as anzahl from ai_attachment')->fetchObject()->anzahl), 1) / $this->items_per_page;

        if($page_count > 1) {
            (new Pagination('/dashboard/medien/dalle/' . Pagination::PAGINATION_PLACEHOLDER . '?' . $_SERVER['QUERY_STRING'], $page_count, $this->page_number))->display();
        }

        $image_contents = array();
        foreach($ai_image_manager->getAttachments($keyword, $this->items_per_page, ($this->page_number-1)*$this->items_per_page) as $attachment){
            #$image = new Image($CONFIG->website_domain . $attachment->src . '?width=350');

            $image = BufferHelper::buffered(function () use ($attachment){
                global $CONFIG;

                ?>
                <div style="position: relative; cursor: pointer;">

                    <i class="fas fa-trash" onclick="deleteAiImage(<?=$attachment->id?>); event.stopPropagation()"
                       style="color: #fff; background-color: #0099cc; border-radius: 50px; padding: 10px 11px; position: absolute; top: -15px; left: -15px;"></i>

                    <img src="<?=(StaticHelper::getResizedImgUrl($CONFIG->app_domain . $attachment->src, 350))?>">

                    <?php if($attachment->generator_data !== null): ?>
                        <span style="position: absolute; left: 10px; top: 10px; cursor: pointer;"
                              class="badge bg-primary"
                              onclick="window.open('/dashboard/medien/bild-generator/<?=$attachment->id?>', 'blank')">
                        <i style="color: #fff; padding: 5px;" class="fas fa-edit"></i>
                            </a>
                </span>
                    <?php endif; ?>

                    <?php if(($attachment->provider_name !== null && strlen($attachment->provider_name))
                        || ($attachment->copyright_info !== null && strlen($attachment->copyright_info))): ?>
                        <span style="position: absolute; bottom: 10px; left: 10px;"
                              class="badge bg-primary">
                        <i style="color: #fff; padding: 5px;" class="fas fa-copyright"></i>
                </span>
                    <?php endif; ?>
                </div>
                <?php
            });

            $image_content = new GlobalAsyncComponentElement($image, 'AddEditAiMediaHandler', ['id' => $attachment->id]);

            $image_contents[] = new Card($image_content, ["media-grid-item"]);
        }
        (new FormBuilder())
            ->withInputRow(new TextInput("s", 'Suchbegriff', null, $keyword))
            ->withMethod(Form::METHOD_GET)
            ->build()
            ->display();

        (new BreakComponent())->display();

        (new MasonryContainer($image_contents, ['col-12', 'col-md-4', 'col-lg-3', 'col-xl-2', 'padding-5']))->display();
    }
}