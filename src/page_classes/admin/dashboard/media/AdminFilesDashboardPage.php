<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media;

use saschahuber\affiliatewebsitecore\component\PaginatedAuthorTable;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\IconManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\affiliatewebsitecore\manager\UploadManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\builder\ui\FormBuilder;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentElement;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\StaticHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

class AdminFilesDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Dateien");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB, $CONFIG;

        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $upload_manager = new UploadManager();

            $title = RequestHelper::reqstr("title");
            $alt_text = RequestHelper::reqstr("alt_text");
            $description = RequestHelper::reqstr("description");
            $provider_name = RequestHelper::reqstr("provider_name");
            $copyright_info = RequestHelper::reqstr("copyright_info");
            $upload_id = RequestHelper::reqstr("upload_id");

            if($upload_id) {
                $upload_manager->updateUpload($upload_id, $description, $provider_name, $copyright_info, $title, $alt_text);
            }
            else {
                $image_file = $_FILES['file'];
                $upload_manager->addUploadFromFile($image_file, $description, $provider_name, $copyright_info, $title, $alt_text);
            }

            UrlHelper::redirect("/dashboard/medien/dateien", 302);
        }

        $this->items_per_page = 25;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->post_manager = new PostManager();

        $LAYOUT_MANAGER = new LayoutManager();
        $upload_manager = new UploadManager();

        (GlobalAsyncComponentButton::fab("fas fa-plus", 'AddEditFileHandler', []))->display();

        $keyword = RequestHelper::reqstr('s');

        $image_contents = array();
        foreach($upload_manager->getUploads($keyword) as $upload){
            #$image = new Image($CONFIG->website_domain . $upload->src . '?width=350');

            $image = BufferHelper::buffered(function () use ($upload){
                global $CONFIG;

                ?>
                <div style="position: relative; cursor: pointer;">
                    <?=ShortcodeHelper::doShortcode('[upload upload_id="'.$upload->id.'"]')?>

                    <?php if(($upload->provider_name !== null && strlen($upload->provider_name))
                        || ($upload->copyright_info !== null && strlen($upload->copyright_info))): ?>
                        <span style="position: absolute; bottom: 10px; left: 10px;"
                              class="badge bg-primary">
                        <i style="color: #fff; padding: 5px;" class="fas fa-copyright"></i>
                </span>
                    <?php endif; ?>
                </div>
                <?php
            });

            $image_content = new GlobalAsyncComponentElement($image, 'AddEditFileHandler', ['id' => $upload->id]);

            $image_contents[] = new Card($image_content, ["media-grid-item"]);
        }
        (new FormBuilder())
            ->withInputRow(new TextInput("s", 'Suchbegriff', null, $keyword))
            ->withMethod(Form::METHOD_GET)
            ->build()
            ->display();

        (new BreakComponent())->display();

        (new MasonryContainer($image_contents, ['col-12', 'col-md-4', 'col-lg-3', 'col-xl-2', 'padding-5']))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            div.media-grid-item{
                display: table-cell;
                text-align: center;
            }

            div.media-grid-item a{
                vertical-align: middle;
                max-height: 200px;
            }

            div.media-grid-item img{
                max-width: 100%;
                max-height: 100%;
                object-fit: contain;
                padding: 5px;
            }

            div.media-grid-item:hover{
                transform: scale(1.05);
                box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 16px 22px 0 rgb(0 0 0 / 19%) !important;
                z-index: 1;
            }

            form img{
                width: 100%;
                height: 100%;
                max-width: 400px;
                max-height: 300px;
                object-fit: contain;
            }

            .card-body {
                padding: 0;
            }

            img.img-media-editor {
                max-height: 600px;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            var refreshCounter = 0;

            var intervalId = window.setInterval(function(){
                refreshMasonry();

                if(refreshCounter > 10){
                    clearInterval(intervalId);
                }

                refreshCounter++;
            }, 1000);
        </script>
        <?php
    }
}