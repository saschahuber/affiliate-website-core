<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use saschahuber\affiliatewebsitecore\component\ContentSchedulingCalendar;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ContentScheduleItemService;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminContentScheduleDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Content Schedule");
    }

    public function getContent()
    {
        ob_start();
        $this->getCssCode();
        $this->getJsCode();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $month = RequestHelper::reqint("month", intval(date('m')));
        $year = RequestHelper::reqint("year", intval(date('Y')));

        $content_schedule_items_service = new ContentScheduleItemService();

        $scheduled_content_items = $content_schedule_items_service->getAllForMonth($month, $year);

        $unscheduled_content_items = $content_schedule_items_service->getUnscheduled();

        (new ContentSchedulingCalendar($scheduled_content_items, $month, $year))->display();

        ob_start();
        ?>
        <h3>Elemente ohne Datum</h3>
        <div class="float-container" style="display: flex; max-width: 100%;">
            <div class="drop-zone item_list float-container-content" style="width: 100%; gap: 10px; padding: 50px 20px" ondrop="dropWithoutDate(this, event)" ondragover="allowDrop(this, event)" ondragenter="onDragEnter(this, event)" ondragleave="onDragLeave(this, event)">
                <?php
                foreach($unscheduled_content_items as $item){
                    echo ContentSchedulingCalendar::buildCalendarItem($item);
                }
                ?>
            </div>
        </div>
        <?php
        $unscheduled_card_content = ob_get_clean();

        BreakComponent::break();

        (new ElevatedCard($unscheduled_card_content))->display();

        BreakComponent::break();

        $content_schedule_items = $content_schedule_items_service->getAllPlannedInFuture();


        $column_labels = [
            'Datum',
            'Content-Eintrag',
            'Verknüpftes Element',
            'Aktionen'
        ];

        $rows = [];

        foreach($content_schedule_items as $content_schedule_item){
            $actions = [
                new GlobalAsyncComponentButton("Content-Eintrag bearbeiten", 'AddEditContentScheduleItemHandler', ['content_schedule_item_id' => $content_schedule_item->content_schedule_item_id])
            ];

            $linked_element_info = "---";
            if($content_schedule_item->linked_element_type && $content_schedule_item->linked_element_id){
                $linked_element = getContentElementByTypeAndId($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);
                $linked_element_info = "[{$content_schedule_item->linked_element_type}] {$linked_element->title}";
                $actions[] = new LinkButton(getContentEditUrlByTypeAndId($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id), 'Element bearbeiten', 'fas fa-edit', false, true);
            }

            $actions[] = new DeleteButton('content_schedule_item', $content_schedule_item->id, 5);

            $cells = [
                DateHelper::displayDateTime($content_schedule_item->content_schedule_item_date),
                $content_schedule_item->title,
                $linked_element_info,
                new FloatContainer($actions),
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedCard([
            new Text('Alle zukünftigen Elemente', 'h3'),
            new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered'])
        ]))->display();

        (GlobalAsyncComponentButton::fab('fas fa-plus', 'AddEditContentScheduleItemHandler', []))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            table {
                min-width: 700px;
            }

            th, td {
                width: calc(100% / 7);
                position: relative;
            }

            p.day_num {
                position: absolute;
                top: 4px;
                z-index: 1;
            }

            td.calendar_container div.drop-zone {
                min-height: 75px;
            }

            .day_num.is_today {
                background-color: var(--primary_color);
                color: #fff;
                border-radius: 50px;
                padding: 4px;
            }

            ul.item_list {
                list-style-type: none;
            }

            .dead-drop-zone {
                margin: 20px auto;
                padding: 25px 10%;
            }

            .drop-zone {
                margin: 20px auto;
                padding: 25px 10%;
                border: 2px dashed #ccc;
                border-radius: 10px;
                text-align: center;
                cursor: pointer;
                transition: background-color 0.3s ease;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            function allowDrop(container, ev) {
                ev.preventDefault();
                onDragEnter(container, ev)
            }

            function drag(draggableContainer, ev) {
                ev.dataTransfer.setData("text", ev.target.id);
            }

            function onDragEnter(container, ev){
                container.style.backgroundColor = "#eee";
            }

            function onDragLeave(container, ev){
                container.style.backgroundColor = "transparent";
            }

            function drop(dropContainer, ev) {
                console.log(dropContainer);
                ev.preventDefault();
                var data = ev.dataTransfer.getData("text");

                let droppedItem = document.getElementById(data);

                //Task und evtl. Zugehörigen Artikel inkl. Social-Posts, Push, Link auf neues Datum aktualisieren
                let droppedItemElementId = droppedItem.dataset.itemId;

                dropContainer.style.backgroundColor = "transparent";

                let params = {
                    rescheduled_year: dropContainer.dataset.year,
                    rescheduled_month: dropContainer.dataset.month,
                    rescheduled_day: dropContainer.dataset.day,
                    element_id_to_reschedule: droppedItemElementId
                }

                doApiCallWithResponseCode('content_schedule/reschedule', params, function (responseCode, response) {
                    if (responseCode >= 200 && responseCode <= 299) {
                        displayStatusToast('Speichern erfolgreich!', 'var(--primary_color)', '#fff');

                        dropContainer.appendChild(droppedItem);
                    }
                    else {
                        displayStatusToast('Speichern fehlgeschlagen!', '#f00', '#fff');
                    }
                });
            }

            function dropWithoutDate(dropContainer, ev) {
                console.log(dropContainer);
                ev.preventDefault();
                var data = ev.dataTransfer.getData("text");

                let droppedItem = document.getElementById(data);

                //Task und evtl. Zugehörigen Artikel inkl. Social-Posts, Push, Link auf neues Datum aktualisieren
                let droppedItemElementId = droppedItem.dataset.itemId;

                dropContainer.style.backgroundColor = "transparent";

                let params = {
                    element_id_to_reschedule: droppedItemElementId
                }

                doApiCallWithResponseCode('content_schedule/reschedule_without_date', params, function (responseCode, response) {
                    if (responseCode >= 200 && responseCode <= 299) {
                        displayStatusToast('Speichern erfolgreich!', 'var(--primary_color)', '#fff');

                        dropContainer.appendChild(droppedItem);
                    }
                    else {
                        displayStatusToast('Speichern fehlgeschlagen!', '#f00', '#fff');
                    }
                });
            }
        </script>
        <?php
    }
}