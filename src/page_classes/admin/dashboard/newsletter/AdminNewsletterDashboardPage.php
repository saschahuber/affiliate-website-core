<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\newsletter;

use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class AdminNewsletterDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Newsletter");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (LinkButton::fab("/dashboard/newsletter/bearbeiten", 'fas fa-plus'))->display();

        $newsletter_manager = new NewsletterManager();

        $column_labels = [
            'Betreff',
            'Geplante Zeit',
            'Gruppen',
            'Themen',
            'Template',
            'Verschickt?',
            'Aktionen'
        ];

        $rows = [];
        foreach($newsletter_manager->getAll() as $item){
            $cells = [
                $item->subject,
                $item->scheduled_time,
                $item->receiver_groups,
                $item->topics,
                $item->template,
                new Toggle('sent_to_all_receivers', null, $item->sent_to_all_receivers, true, true),
                new FloatContainer([
                    new LinkButton("/dashboard/newsletter/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, true),
                    new DeleteButton('newsletter', $item->id, 5)
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}