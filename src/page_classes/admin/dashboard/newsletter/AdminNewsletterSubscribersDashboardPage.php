<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\newsletter;

use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminNewsletterSubscribersDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Newsletter-Abonnenten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (new LinkButton("/dashboard/newsletter", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $newsletter_manager = new NewsletterManager();

        $column_labels = [
            'Name',
            'E-Mail',
            'Registrier-Datum des Nutzers',
            'Themen',
            'Bestätigt',
            'Mail valide'
        ];

        $rows = [];
        foreach($newsletter_manager->getSubscribers(RequestHelper::reqint('confirmed')) as $item){
            $cells = [
                $item->name,
                $item->email,
                $item->registration_date,
                implode(', ', $item->topics),
                new Toggle(null, null, $item->confirmation_token===null, true, true),
                new HTMLElement(BufferHelper::buffered(function() use ($item){
                    if($item->email_valid_datetime) {
                        (new Toggle(null, null, $item->email_valid, true, true))->display();
                        echo "({$item->email_valid_datetime})";
                    }
                    else {
                        echo "-";
                    }
                }))
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}