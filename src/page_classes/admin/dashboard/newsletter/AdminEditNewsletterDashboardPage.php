<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\newsletter;

use DateTime;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AdminEditNewsletterDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Newsletter bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (new LinkButton("/dashboard/newsletter", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        $this->title = "Newsletter bearbeiten/erstellen";

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $newsletter_manager = new NewsletterManager();

        $item = $newsletter_manager->getById($item_id);

        $messageContainer = new CkEditorTextArea('message', 'Inhalt', '', $item?$item->message:null, null, false);

        $newsletter_content_form = new GridContainer([
            [TextInput::required('subject', 'Betreff', null, $item?$item->subject:null)],
            [$messageContainer]
        ], 1);

        $default_send_time = (new DateTime())->format('Y-m-d\T14:00');

        $templates = [
            null => 'Standard-Template'
        ];

        $receiver_groups = [
            'all' => 'Alle bestätigten Benutzer',
            'registered' => 'Alle registrierten Benutzer',
            'companies' => 'Alle registrierten Dienstleister'
        ];

        $newsletter_form = new SimpleFabSaveForm([
            [new HiddenInput('id', $item_id)],
            [$newsletter_content_form],
            [new DateTimePicker('scheduled_time', 'Versandzeit', null, $item?$item->scheduled_time:$default_send_time)],
            [new Select('receiver_groups', 'Empfänger-Art', $receiver_groups, $item?explode(',', $item->receiver_groups):null, true, false, false, true)],
            [new Select('topics', 'Themen', ALL_NEWSLETTER_TOPICS, $item?explode(',', $item->topics):null, true, false, false, true)],
            [new Select('template', 'Template', $templates, $item?$item->template:null, false, false, false, true)]
        ], 'newsletter/save');

        (new Row([
            new Column([new Card([$newsletter_form])], ['col-lg-6', 'col-12']),
            new Column([new Card([
                BufferHelper::buffered(function(){
                    ?>
                    <div id="newsletter-preview-container">
                        <iframe src=""></iframe>
                    </div>
                    <?php
                }),
                new FloatContainer([new JsButton('Vorschau aktualisieren', "refreshNewsletterPreview(this, '{$messageContainer->getId()}')")])
            ])], ['col-lg-6', 'col-12'])
        ]))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            #newsletter-preview-container iframe {
                width: 100%;
                height: 85vh;
            }
        </style>
        <?php
    }

    public function getJsCode()
    {
        ?>
        <script>
            function refreshNewsletterPreview(button, ckEditorContentId){
                let previewIframe = document.querySelector('#newsletter-preview-container iframe');

                let subject = document.querySelector('input[name="subject"]').value;
                let message = ckeditor_instances[ckEditorContentId].getData();
                let template = document.querySelector('select[name="template"]').value;

                previewIframe.src = apiDomain + "/newsletter_preview/getNewsletterPreview?subject=" + subject + "&message=" + message + "&template=" + template;
            }
        </script>
        <?php
    }
}