<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use saschahuber\affiliatewebsitecore\component\PaginatedPageTable;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AdminPagesDashboardPage extends AdminDashboardSubPage {
    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $this->items_per_page = 25;

        if(count($this->path) > 2){
            $this->page_number = intval($this->path[2]);
        }
        else{
            $this->page_number = 1;
        }

        $this->page_manager = new PageManager();

        $this->offset = ($this->page_number - 1) * $this->items_per_page;

        $this->keyword = RequestHelper::reqstr('s');
        $this->status = RequestHelper::reqstr('status');

        $this->pages = $this->page_manager->getFiltered($this->keyword, $this->status, null, 'page_date');

        $this->title = "Seiten";

        (new PaginatedPageTable($this->pages, $this->offset, $this->items_per_page, $this->page_number))->display();

        (LinkButton::fab("/dashboard/seiten/bearbeiten", 'fas fa-plus'))->display();
    }
}