<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\structure;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\helper\AuthHelper;

class AdminStatusCodesDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Status-Codes");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $permission_service = AuthHelper::getPermissionService();
    }
}