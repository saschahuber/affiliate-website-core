<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\structure;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\AsyncComponent;
use saschahuber\saastemplatecore\component\async\inputs\EditableConfigToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\service\ConfigService;

class AdminStructureDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Struktur");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $permission_service = AuthHelper::getPermissionService();

        $contents = [];

        $scheduled_elements_component = new AsyncComponent('PagespeedDistributionAsyncHandler', array('height' => 350), 350);
        $contents[] = new ElevatedCard($scheduled_elements_component);

        $imported_news_component = new AsyncComponent('StatusCodeDistributionAsyncHandler', array('height' => 350), 350);
        $contents[] = new ElevatedCard($imported_news_component);

        $imported_news_component = new AsyncComponent('NotFoundPagesAsyncHandler', array('height' => 350), 350);
        $contents[] = new ElevatedCard($imported_news_component);

        ?>

        <h3>Übersicht der Seitenstruktur</h3>

        <?php

        if(count($contents) > 0) {
            (new MasonryContainer($contents, ['col-lg-4']))->display();
        }
        else{
            $content = '<p>Keine Daten für Übersicht vorhanden.</p>';

            (new ElevatedCard($content, ['position_unset', 'no-margin']))->display();
        }

        BreakComponent::break();

        $config_service = new ConfigService();
        (new EditableConfigToggle('crawling_workers_enabled', $config_service->getConfigField('crawling_workers_enabled', ConfigService::TYPE_BOOL), true, 'Crawling-Worker aktiv?'))->display();
    }
}