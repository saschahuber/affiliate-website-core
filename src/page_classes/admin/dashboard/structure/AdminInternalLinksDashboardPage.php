<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\structure;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\persistence\CrawlUrlLinkRepository;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\AuthHelper;

class AdminInternalLinksDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Interne Links");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $permission_service = AuthHelper::getPermissionService();

        $crawl_url_link_repository = new CrawlUrlLinkRepository();

        $crawl_url_links = $crawl_url_link_repository->getAllCrawlUrlsWithIncomingAndOutgoingLinks();

        $column_labels = [
            'URL',
            'URL-Type',
            'Eingehende Links',
            'Outgoing Links'
        ];

        $rows = [];

        foreach($crawl_url_links as $crawl_url_link){
            $cells = [
                $crawl_url_link->url,
                $crawl_url_link->url_type,
                ($crawl_url_link->incoming_links??0),
                ($crawl_url_link->outgoing_links??0)
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}