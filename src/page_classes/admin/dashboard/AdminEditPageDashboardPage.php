<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use saschahuber\affiliatewebsitecore\component\ContentPreviewButton;
use saschahuber\affiliatewebsitecore\component\form\AdditionalSettings;
use saschahuber\affiliatewebsitecore\component\form\input\VgWortPixelSelector;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\ElementLaunchBlockService;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditPageDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Seite bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $CONFIG;

        $this->title = "Seite bearbeiten/erstellen";

        if(count($this->path) > 3){
            $item_id = intval($this->path[3]);
        }
        else{
            $item_id = null;
        }

        $image_manager = new ImageManager();
        $page_manager = new PageManager();

        $item = $page_manager->getById($item_id);

        (new LinkButton("/dashboard/seiten", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        $title_input = TextInput::required("title", 'Titel', 'Titel der Seite', isset($item->title)?$item->title:null);

        $title_input->display();

        TextInput::required("permalink", 'Permalink', 'Permalink der Seite', isset($item->permalink)?$item->permalink:null)->display();

        (new TextInput("h1", 'H1 (Optional - Default: Titel)', 'H1 (Optional - Default: Titel)', isset($item->h1)?$item->h1:null))->display();
        (new TextInput("subtitle", 'Unterüberschrit', 'Unterüberschrit', isset($item->subtitle)?$item->subtitle:null))->display();

        $ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, $item?$item->content:null, 300);
        $ckeditor_area->display();

        #if($item_id === null){
        #    (new BlogWriterButton($title_input->getId(), $ckeditor_area->getId()))->display();
        #}

        if($item){
            (new ContentPreviewButton($CONFIG->website_domain . $page_manager->generatePermalink($item), [
                'title' => [
                    'id' => $title_input->getId(),
                    'tinymce' => false
                ],
                'content' => [
                    'id' => $ckeditor_area->getId(),
                    'ckeditor' => true
                ],
                'meta_title' => [
                    'id' => 'component_meta_title_input',
                    'tinymce' => false
                ],
                'meta_description' => [
                    'id' => 'component_meta_description_input',
                    'tinymce' => false
                ]
            ]))->display();
        }

        (new AdditionalSettings($item))->display();

        (new VgWortPixelSelector($item?$item->vg_wort_pixel_id:null))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'page/save'))->display();

        if($item_id) {
            (new ElementLaunchBlockService())->getElementLaunchBlock(PageManager::TYPE_PAGE, $item_id)->display();
        }
    }
}