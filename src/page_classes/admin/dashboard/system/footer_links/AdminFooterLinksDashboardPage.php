<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\footer_links;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\service\FooterLinkGroupService;

class AdminFooterLinksDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Footer-Links");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $footer_link_group_service = new FooterLinkGroupService();

        $footer_link_groups = $footer_link_group_service->getGroups();

        ?>
        <div class="row">
            <?php
            foreach($footer_link_groups as $footer_link_group) {
                ?>
                <div class="col col-md-<?=$footer_link_group->span?>">
                    <?php
                    (new Card(BufferHelper::buffered(function() use ($footer_link_group){
                        (new LinkButton('/dashboard/system/footer-links/gruppe-bearbeiten/'.$footer_link_group->footer_link_group_id, $footer_link_group->title, null, null, false, ['centered', 'btn-block']))->display();
                        ?>
                        <hr>
                        <?php
                        foreach($footer_link_group->items as $item){
                            (new LinkButton('/dashboard/system/footer-links/link-bearbeiten/'.$footer_link_group->footer_link_group_id.'/'.$item->id, $item->title, null, null, false, ['centered', 'btn-block']))->display();
                        }
                        ?>
                        <hr>
                        <?php
                        (new LinkButton('/dashboard/system/footer-links/link-bearbeiten/'.$footer_link_group->footer_link_group_id, 'Neuen Link erstellen', 'fas fa-plus-circle', null, false, ['centered', 'btn-block']))->display();
                    })))->display();
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <?php

        (new LinkButton('/dashboard/system/footer-links/gruppe-bearbeiten', 'Neue Gruppe erstellen', 'fas fa-plus-circle'))->display();
    }
}