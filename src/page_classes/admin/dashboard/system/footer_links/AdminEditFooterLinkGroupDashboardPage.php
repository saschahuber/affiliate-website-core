<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\footer_links;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\manager\FooterLinkGroupManager;

class AdminEditFooterLinkGroupDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Footer-Link-Gruppe bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $footer_link_group_manager = new FooterLinkGroupManager();

        $group_id = null;
        if(count($this->path) > 4){
            $group_id = intval($this->path[4]);
        }

        $group = $footer_link_group_manager->getById($group_id);

        (new LinkButton("/dashboard/system/footer-links", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $group_id))->display();

        TextInput::required("title", 'Titel', 'Titel', isset($group->title) ? $group->title : null)->display();

        (new NumberInput("sort_order", 'Reihenfolge', 'Reihenfolge', isset($group->sort_order) ? $group->sort_order : null, 0, null, 1))->display();

        (new NumberInput("span", 'Spaltenbreite', 'Spaltenbreite', isset($group->span) ? $group->span : null, 0, null, 1))->display();

        (new Toggle('active', 'Ist Aktiv', isset($group->active)?$group->active:false, false, true))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'footer/linkgroup/save'))->display();
    }
}