<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\footer_links;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\manager\FooterLinkGroupItemManager;
use saschahuber\saastemplatecore\manager\FooterLinkGroupManager;

class AdminEditFooterLinkDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Footer-Link bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $footer_link_group_manager = new FooterLinkGroupManager();
        $footer_link_group_item_manager = new FooterLinkGroupItemManager();

        $group_id = intval($this->path[4]);

        $link_id = null;
        if(count($this->path) > 5){
            $link_id = intval($this->path[5]);
        }

        $link = $footer_link_group_item_manager->getById($link_id);

        (new LinkButton("/dashboard/system/footer-links", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $link_id))->display();
        (new HiddenInput('group_id', $group_id))->display();

        TextInput::required("title", 'Titel', 'Titel', isset($link->title) ? $link->title : null)->display();
        TextInput::required("url", 'URL', 'URL', isset($link->url) ? $link->url : null)->display();

        (new NumberInput("sort_order", 'Reihenfolge', 'Reihenfolge', isset($link->sort_order) ? $link->sort_order : null, 0, null, 1))->display();

        (new Toggle('active', 'Ist Aktiv', isset($link->active)?$link->active:false, false, true))->display();
        $form_content = ob_get_clean();

        (new SimpleFabSaveForm($form_content, 'footer/link/save'))->display();
    }
}