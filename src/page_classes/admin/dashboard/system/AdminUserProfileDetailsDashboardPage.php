<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system;

use saschahuber\affiliatewebsitecore\component\ApplicationAnalyticsTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\AffiliateApplicationAnalyticsService;
use saschahuber\affiliatewebsitecore\service\UserProfileService;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class AdminUserProfileDetailsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Benutzer-Profil Details");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $fingerprint = $this->path[2];

        $user_profile_service = new UserProfileService();
        $application_analytics_service = new AffiliateApplicationAnalyticsService();

        $user_profile = $user_profile_service->getProfile($fingerprint);

        $user_logs = $application_analytics_service->findLogs(null, null, null, null, null, null, $fingerprint);

        $content = BufferHelper::buffered(function() use ($user_profile){
            (new Text("Letztes Update: " . DateHelper::displayDateTime($user_profile['update_time']), 'strong'))->display();

            $interest_groups = ArrayHelper::getArrayValue($user_profile['profile_data'], 'interests');
            if($interest_groups){
                BreakComponent::break();

                (new Text("Interessen:", 'strong'))->display();

                $list_items = [];
                foreach($interest_groups as $type => $interests){
                    $list_items[] = BufferHelper::buffered(function() use($type, $interests){
                        $interest_items = [];
                        (new Text($type, 'strong'))->display();
                        foreach($interests as $interest){
                            $interest_item = BufferHelper::buffered(function() use ($type, $interest){
                                global $CONFIG;

                                $item = getContentElementByTypeAndId($type, $interest['id']);
                                $item_url = getContentPermalinkElementByTypeAndId($type, $interest['id']);

                                (new LinkButton($CONFIG->website_domain . $item_url, "{$item->title} (Score: {$interest['value']})", null, null, true))->display();
                            });

                            $interest_items[] = $interest_item;
                        }
                        (new FloatContainer($interest_items))->display();
                    });
                }
                (new ListContainer($list_items))->display();
            }
        });

        (new ElevatedCard($content))->display();

        BreakComponent::break();

        (new ApplicationAnalyticsTable($user_logs))->display();
    }
}