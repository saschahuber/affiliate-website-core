<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\redirects;

use saschahuber\affiliatewebsitecore\component\PaginatedRedirectTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\RedirectService;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminRedirectsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Weiterleitungen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $redirect_service = new RedirectService();

        (new PaginatedRedirectTable($redirect_service->getAll()))->display();

        (LinkButton::fab("/dashboard/system/redirects/bearbeiten", 'fas fa-plus'))->display();
    }
}