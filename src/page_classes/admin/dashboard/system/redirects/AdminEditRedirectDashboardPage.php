<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\redirects;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\RedirectService;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditRedirectDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Weiterleitung bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $redirect_service = new RedirectService();

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
            $item = $redirect_service->getById($item_id);
        }
        else{
            $item_id = null;
            $item = null;
        }

        (new LinkButton("/dashboard/system/redirects", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        (TextInput::required("from_url", 'Von-URL', 'Start des Redirects', isset($item->from_url)?$item->from_url:null))->display();
        (TextInput::required("to_url", 'Ziel-URL', 'Ziel des Redirects', isset($item->to_url)?$item->to_url:null))->display();
        (new Toggle("is_temporary", "Temporär (HTTP 302, ansonsten 301)", isset($item->is_temporary)?$item->is_temporary:false, false, true))->display();
        (new Toggle("is_active", "Redirect aktiv", isset($item->is_active)?$item->is_active:false, false, true))->display();
        $form_content = ob_get_clean();
        (new SimpleFabSaveForm($form_content, 'redirect/save'))->display();
    }
}