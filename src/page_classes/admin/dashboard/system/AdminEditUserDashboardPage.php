<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\service\UserService;

class AdminEditUserDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Benutzer bearbeiten");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $user_service = new UserService();

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
            $item = $user_service->getByUserId($item_id);
            $item->roles = $user_service->getUserRoles($item_id);
        }
        else{
            $item_id = null;
            $item = null;
        }

        (new LinkButton("/dashboard/system/users", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        (new TextInput('email', 'E-Mail', 'E-Mail', $item->email, $item !== null))->display();

        (new TextInput('name', 'Name', 'Name', $item->name))->display();
        (new TextInput('first_name', 'Vorname', 'Vorname', $item->first_name))->display();
        (new TextInput('last_name', 'Lastname', 'Lastname', $item->last_name))->display();

        (new Select('roles', 'Rollen', (AuthHelper::getRoleService())->getRoleMappingsAsDict(), $item->roles, true))->display();

        $form_content = ob_get_clean();
        (new SimpleFabSaveForm($form_content, 'user/save'))->display();
    }
}