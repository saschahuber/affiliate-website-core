<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system;

use saschahuber\affiliatewebsitecore\component\ApplicationAnalyticsTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\service\ApplicationAnalyticsService;

class AdminAffiliateAnalyticsDetailsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Analytics");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $application_analytics_service = new ApplicationAnalyticsService();

        $this->event_type = RequestHelper::reqstr('event_type');
        $this->url = RequestHelper::reqstr('url');
        $this->user_id = RequestHelper::reqval('user_id');
        $this->ip = RequestHelper::reqval('ip');
        $this->user_agent = RequestHelper::reqval('user_agent');
        $this->session_id = RequestHelper::reqval('session_id');
        $this->fingerprint = RequestHelper::reqval('fingerprint');
        $this->referrer = RequestHelper::reqval('referrer');
        $this->utm_source = RequestHelper::reqval('utm_source');
        $this->utm_medium = RequestHelper::reqval('utm_medium');
        $this->utm_campaign = RequestHelper::reqval('utm_campaign');
        $this->utm_content = RequestHelper::reqval('utm_content');
        $this->utm_term = RequestHelper::reqval('utm_term');
        $this->from_time = RequestHelper::reqstr('from_time');
        $this->to_time = RequestHelper::reqstr('to_time');

        $this->log_items = $application_analytics_service->findLogs($this->event_type, $this->url, $this->user_id, $this->ip,
            $this->user_agent, $this->session_id, $this->fingerprint, $this->referrer, $this->utm_source, $this->utm_medium,
            $this->utm_campaign, $this->utm_content, $this->utm_term, $this->from_time, $this->to_time);

        (new Card([
            new Text('Events analysieren', 'h1'),
            new Form([
                [
                    new TextInput('event_type', 'Typ', null, $this->event_type),
                    new TextInput('url', 'URL', null, $this->url),
                    new TextInput('ip', 'IP', null, $this->ip),
                    new TextInput('user_agent', 'User Agent', null, $this->user_agent),
                    new TextInput('session_id', 'Session ID', null, $this->session_id)
                ],
                [
                    new TextInput('fingerprint', 'Fingerprint', null, $this->fingerprint),
                    new TextInput('referrer', 'Referrer', null, $this->referrer),
                    new TextInput('utm_source', 'UTM-Source', null, $this->utm_source),
                    new TextInput('utm_medium', 'UTM-Medium', null, $this->utm_medium),
                    new TextInput('utm_campaign', 'UTM-Kampagne', null, $this->utm_campaign)
                ],
                [
                    new TextInput('utm_content', 'UTM-Content', null, $this->utm_content),
                    new TextInput('utm_term', 'UTM-Term', null, $this->utm_term),
                    new DateTimePicker('from_time', 'Start', null, $this->from_time),
                    new DateTimePicker('to_time', 'Ende', null, $this->to_time),
                    new SubmitButton('Filtern')
                ]
            ], '/dashboard/system/analytics/details', Form::METHOD_GET),
            new ApplicationAnalyticsTable($this->log_items)
        ]))->display();
    }

    public function getCssCode()
    {
        ?>
        <style>
            .log-content-container {
                text-align: center;
            }

            .log-content-container p{
                max-width: 750px;
                margin:auto;
            }
        </style>
        <?php
    }
}