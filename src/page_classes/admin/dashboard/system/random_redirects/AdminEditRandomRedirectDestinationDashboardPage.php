<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\random_redirects;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\RedirectGroupItemService;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditRandomRedirectDestinationDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Zufällige Weiterleitung Ziel bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $redirect_group_item_service = new RedirectGroupItemService();

        $redirect_group_id = $this->path[4];

        if(count($this->path) > 5){
            $item_id = intval($this->path[5]);
            $item = $redirect_group_item_service->getById($item_id);
        }
        else{
            $item_id = null;
            $item = null;
        }

        (new LinkButton("/dashboard/system/random-redirects/bearbeiten/".$redirect_group_id, "Zurück zur Redirect-Gruppe", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();
        (new HiddenInput('redirect_group_id', $redirect_group_id))->display();

        (TextInput::required("title", 'Titel', 'Titel des Redirects', isset($item->title)?$item->title:null))->display();
        (TextInput::required("target_url", 'Ziel-URL', 'Ziel-URL des Redirects', isset($item->target_url)?$item->target_url:null))->display();
        (new Toggle("is_active", "Redirect aktiv", isset($item->is_active)?$item->is_active:false, false, true))->display();
        $form_content = ob_get_clean();
        (new SimpleFabSaveForm($form_content, 'redirect/random/save'))->display();
    }
}