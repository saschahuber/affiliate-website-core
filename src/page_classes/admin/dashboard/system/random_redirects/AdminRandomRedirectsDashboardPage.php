<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\random_redirects;

use saschahuber\affiliatewebsitecore\component\RedirectGroupTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\RedirectGroupService;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminRandomRedirectsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Zufällige Weiterleitungen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $redirect_group_service = new RedirectGroupService();

        (new RedirectGroupTable($redirect_group_service->getAll()))->display();

        (LinkButton::fab("/dashboard/system/random-redirects/bearbeiten", 'fas fa-plus'))->display();
    }
}