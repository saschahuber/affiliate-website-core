<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\random_redirects;

use saschahuber\affiliatewebsitecore\component\RedirectGroupItemTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\RedirectGroupItemService;
use saschahuber\affiliatewebsitecore\service\RedirectGroupService;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\LinkButton;

class AdminEditRandomRedirectDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Zufällige Weiterleitung bearbeiten/erstellen");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $redirect_group_service = new RedirectGroupService();
        $redirect_group_item_service = new RedirectGroupItemService();

        if(count($this->path) > 4){
            $item_id = intval($this->path[4]);
            $item = $redirect_group_service->getById($item_id);
        }
        else{
            $item_id = null;
            $item = null;
        }

        (new LinkButton("/dashboard/system/random-redirects", "Zurück zur Übersicht", "fas fa-arrow-left", false))->display();

        ob_start();
        (new HiddenInput('id', $item_id))->display();

        (TextInput::required("title", 'Titel', 'Titel des Redirects', isset($item->title)?$item->title:null))->display();
        (TextInput::required("alias", 'Alias', 'Alias des Redirects', isset($item->alias)?$item->alias:null))->display();
        (new Toggle("is_active", "Redirect aktiv", isset($item->is_active)?$item->is_active:false, false, true))->display();
        $form_content = ob_get_clean();
        (new SimpleFabSaveForm($form_content, 'redirect/random/group/save'))->display();

        ?>
        <?php if($item): ?>
            <div>
                <label>Produkt-Links</label>

                <?php

                $redirect_group_items = [];
                if($item) {
                    $redirect_group_items = $redirect_group_item_service->getByRedirectGroupId($item->id);
                    if($redirect_group_items === null){
                        $redirect_group_items = [];
                    }
                }

                (new RedirectGroupItemTable($item, $redirect_group_items))->display();

                ?>
            </div>
        <?php endif;
    }
}