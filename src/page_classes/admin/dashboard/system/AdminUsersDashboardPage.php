<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system;

use saschahuber\affiliatewebsitecore\component\PaginatedUserTable;
use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\service\UserService;

class AdminUsersDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Benutzer");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        (LinkButton::fab("/dashboard/system/users/bearbeiten", 'fas fa-plus'))->display();

        $user_service = new UserService();

        $users = $user_service->getAll();
        foreach($users as $user) {
            $user->roles = $user_service->getUserRoles($user->id);
        }

        (new PaginatedUserTable($users))->display();
    }
}