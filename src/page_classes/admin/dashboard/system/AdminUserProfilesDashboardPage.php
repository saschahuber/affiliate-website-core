<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\affiliatewebsitecore\service\UserProfileService;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class AdminUserProfilesDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Benutzer-Profile");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $user_profile_service = new UserProfileService();

        $user_profiles = $user_profile_service->getUserProfiles(20);

        $items = [];
        foreach($user_profiles as $user_profile){
            $content = BufferHelper::buffered(function() use ($user_profile){
                (new LinkButton("/dashboard/system/analytics/details?fingerprint={$user_profile['fingerprint']}", $user_profile['fingerprint'], null, null, true))->display();

                BreakComponent::break();

                (new Text("Letztes Update: " . DateHelper::displayDateTime($user_profile['update_time']), 'strong'))->display();

                $interest_groups = ArrayHelper::getArrayValue($user_profile['profile_data'], 'interests');
                if($interest_groups){
                    BreakComponent::break();

                    (new Text("Interessen:", 'strong'))->display();

                    $list_items = [];
                    foreach($interest_groups as $type => $interests){
                        $list_items[] = BufferHelper::buffered(function() use($type, $interests){
                            $interest_items = [];
                            (new Text($type, 'strong'))->display();
                            foreach($interests as $interest){
                                $interest_item = BufferHelper::buffered(function() use ($type, $interest){
                                    global $CONFIG;

                                    $item = getContentElementByTypeAndId($type, $interest['id']);
                                    $item_url = getContentPermalinkElementByTypeAndId($type, $interest['id']);

                                    (new LinkButton($CONFIG->website_domain . $item_url, "{$item->title} (Score: {$interest['value']})", null, null, true))->display();
                                });

                                $interest_items[] = $interest_item;
                            }
                            (new FloatContainer($interest_items))->display();
                        });
                    }
                    (new ListContainer($list_items))->display();
                }

                BreakComponent::break();

                (new FloatContainer([
                    new LinkButton("/dashboard/user-profiles/{$user_profile['fingerprint']}/details", 'Details', 'fas fa-info-circle', false, true)
                ]))->display();
            });

            $items[] = new Card($content);
        }

        if(count($items)){
            (new MasonryContainer($items, ['col-md-6', 'col-lg-4']))->display();
        }
        else {
            (new Text("Noch keine Profile vorhanden..."))->display();
        }
    }
}