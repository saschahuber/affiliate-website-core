<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\subscriptions;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\service\UserSubscriptionService;

class AdminSubscriptionsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("Abonnements");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        global $DB;

        if(RequestHelper::isPost() && !RequestHelper::isApiCall()){
            $user_subscription_service = new UserSubscriptionService();

            switch (RequestHelper::reqstr('task')){
                case 'create_subscription':
                    $start_date = RequestHelper::reqstr('start_date');
                    $end_date = RequestHelper::reqstr('end_date');
                    $packages = array_keys((AuthHelper::getPermissionService())->getPackages());
                    $selected_package = $packages[RequestHelper::reqint('selected_package')];
                    $user_id = RequestHelper::reqint('user_id');

                    $user_subscription_service->addManualSubscription($user_id, $selected_package, $start_date, $end_date);
                    UrlHelper::redirect("/dashboard/subscriptions/$user_id", 302);
                    break;
                case 'update_subscription':
                    $subscription_id = RequestHelper::reqint('subscription_id');

                    $user_subscription = $user_subscription_service->getById($subscription_id);

                    $start_date = RequestHelper::reqstr('start_date');
                    $end_date = RequestHelper::reqstr('end_date');
                    $packages = array_keys((AuthHelper::getPermissionService())->getPackages());
                    $selected_package = $packages[RequestHelper::reqint('selected_package')];
                    $user_id = RequestHelper::reqint('user_id');

                    //Werte aktualisieren
                    $user_subscription->current_period_start = $start_date;
                    $user_subscription->current_period_end = $end_date;
                    $user_subscription->plan = $selected_package;
                    $user_subscription_service->update($user_subscription);

                    UrlHelper::redirect("/dashboard/subscriptions/$user_id", 302);

                    break;
            }
        }

        $dbquery = $DB->query('SELECT user.email, user.user_id, user__subscription_id, plan, stripe_subscription_id, current_period_start, current_period_end 
            from user 
            join user__subscription on user.user_id = user__subscription.user_id and user__subscription_id
            where current_period_end in (
                select MAX(current_period_end) 
                    from user__subscription 
                    where current_period_end is not null
                    group by user_id)');

        $user_subscriptions = [];
        while ($user_subscription = $dbquery->fetchObject()){
            $user_subscriptions[] = $user_subscription;
        }

        $column_labels = [
            'Benutzer',
            'Paket',
            'Stripe-ID',
            'Von',
            'Bis',
            'Aktion'
        ];

        $rows = [];

        foreach($user_subscriptions as $user_subscription){
            $cells = [
                $user_subscription->email,
                $user_subscription->plan,
                $user_subscription->stripe_subscription_id,
                DateHelper::displayDateTime($user_subscription->current_period_start),
                DateHelper::displayDateTime($user_subscription->current_period_end),
                new LinkButton('/dashboard/subscriptions/' . $user_subscription->user_id, '<i class="fas fa-eye"></i>'),
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();

        (GlobalAsyncComponentButton::fab('fas fa-plus', 'AddSubscriptionHandler', []))->display();
    }
}