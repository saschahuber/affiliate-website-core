<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\subscriptions;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\service\UserSubscriptionService;

class AdminUserSubscriptionsDashboardPage extends AdminDashboardSubPage {
    public function __construct($path)
    {
        parent::__construct($path);
        $this->setTitle("User-Abonnements");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $user_subscription_service = new UserSubscriptionService();

        $this->user_id = intval($this->path[2]);

        $user_subscriptions = $user_subscription_service->getAllSubscriptions($this->user_id);

        $column_labels = [
            'Laufend',
            'Von',
            'Bis',
            'Paket',
            'Aktionen'
        ];

        $rows = [];

        foreach($user_subscriptions as $user_subscription){
            $cells = [
                new Toggle('active', null, $user_subscription->current, true, true),
                DateHelper::displayDateTime($user_subscription->current_period_start),
                DateHelper::displayDateTime($user_subscription->current_period_end),
                $user_subscription->plan,
                new GlobalAsyncComponentButton('<i class="fas fa-edit"></i>',
                    'EditSubscriptionHandler', ['id' => $user_subscription->user__subscription_id, 'user_id' => $this->user_id])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }
}