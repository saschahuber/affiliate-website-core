<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin\dashboard;

use saschahuber\affiliatewebsitecore\page_classes\admin\AdminDashboardSubPage;
use saschahuber\saastemplatecore\component\async\AsyncComponent;
use saschahuber\saastemplatecore\component\container\MasonryContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class AdminOverviewDashboardPage extends AdminDashboardSubPage {
    public function __construct($path){
        parent::__construct($path);
        $this->setTitle("Übersicht");
    }

    public function getContent()
    {
        ob_start();
        $this->buildInnerContent();
        return ob_get_clean();
    }

    public function buildInnerContent(){
        $permission_service = AuthHelper::getPermissionService();

        $contents = [];

        $scheduled_elements_component = new AsyncComponent('ScheduledElementsAsyncHandler', array('height' => 350), 350);
        $contents[] = new ElevatedCard($scheduled_elements_component);

        $imported_news_component = new AsyncComponent('ImportedNewsAsyncHandler', array('height' => 350), 350);
        $contents[] = new ElevatedCard($imported_news_component);

        #$toolbar_component = new AsyncComponent('ToolbarAsyncHandler', array('height' => 350), 350);
        #$contents[] = new ElevatedCard($toolbar_component);

        #$daily_active_users_component = new AsyncComponent('DailyActiveUsersAsyncHandler', array('height' => 350), 350);
        #$contents[] = new ElevatedCard($daily_active_users_component);

        $daily_affiliate_link_clicks_component = new AsyncComponent('ProductLinkClicksLastIntervalAsyncHandler',
            array('height' => 350, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 17), 350);
        $contents[] = new ElevatedCard($daily_affiliate_link_clicks_component);

        #$daily_affiliate_link_clicks_component = new AsyncComponent('DailyAffiliateLinkClicksAsyncHandler', array('height' => 350), 350);
        #$contents[] = new ElevatedCard($daily_affiliate_link_clicks_component);

        $daily_newsletter_subscribers_component = new AsyncComponent('DailyNewsletterSubscribersAsyncHandler', array('height' => 350), 350);
        $contents[] = new ElevatedCard($daily_newsletter_subscribers_component);

        #$daily_push_subscribers_component = new AsyncComponent('DailyPushSubscribersAsyncHandler', array('height' => 350), 350);
        #$contents[] = new ElevatedCard($daily_push_subscribers_component);

        #$daily_social_media_followers_component = new AsyncComponent('DailySocialMediaFollowersAsyncHandler', array('height' => 350), 350);
        #$contents[] = new ElevatedCard($daily_social_media_followers_component);

        $top_clicked_products_component = new AsyncComponent('TopClickedProductsAsyncHandler', array('height' => 350, 'days' => 7, 'limit' => 10), 350);
        $contents[] = new ElevatedCard($top_clicked_products_component);

        $top_clicked_products_source_pages_component = new AsyncComponent('TopClickedProductsSourcePagesAsyncHandler', array('height' => 350, 'days' => 7, 'limit' => 10), 350);
        $contents[] = new ElevatedCard($top_clicked_products_source_pages_component);

        $log_items_component = new AsyncComponent('LogEntriesLastIntervalAsyncHandler', array('height' => 350, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_HOUR, 'number' => 24, 'group' => 'log_level', 'title' => 'Logs nach Log-Level (letzte 24 Stunden)'), 350);
        $contents[] = new ElevatedCard($log_items_component);

        $log_items_component = new AsyncComponent('AnalyticsEntriesLastIntervalAsyncHandler', array('height' => 350, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 30, 'group' => 'event_type', 'title' => 'Events nach Typ (letzte 30 Tage)'), 350);
        $contents[] = new ElevatedCard($log_items_component);

        ?>

        <h3>Übersicht</h3>

        <?php

        if(count($contents) > 0) {
            (new MasonryContainer($contents, ['col-xxl-3', 'col-xl-4', 'col-lg-6']))->display();
        }
        else{
            $content = '<p>Keine Daten für Übersicht vorhanden.</p>';

            (new ElevatedCard($content, ['position_unset', 'no-margin']))->display();
        }
    }
}