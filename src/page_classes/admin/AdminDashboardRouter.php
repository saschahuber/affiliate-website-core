<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin;

use saschahuber\affiliatewebsitecore\component\ToolbarButton;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminAuthorDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminContentScheduleDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminEditAuthorDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminEditLinkPageItemDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminEditPageDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminEditPostingsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminLinkPageDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminOverviewDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminPagesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\AdminPostingsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\blog\AdminBlogCategoryDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\blog\AdminBlogDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\blog\AdminEditBlogCategoryDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\blog\AdminEditBlogPostDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm\AdminCrmContactsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm\AdminCrmDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm\AdminCrmNotesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm\AdminCrmPersonsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm\AdminEditCrmContactDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm\AdminEditCrmNoteDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\crm\AdminEditCrmPersonDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister\AdminCompaniesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister\AdminCompanyCategoriesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister\AdminEditCompanyCategoryDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister\AdminEditCompanyDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\dienstleister\AdminImportCompaniesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter\AdminEditGuideFilterDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter\AdminEditGuideFilterStepDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter\AdminEditGuideFilterStepOptionDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter\AdminEditGuideFilterStepOptionFilterDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\guide_filter\AdminGuideFilterDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\AdminDallEGeneratorDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\AdminDallEOverviewDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\AdminFilesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\AdminIconDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\AdminMediaDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\AdminStockMediaDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator\AdminFancyHeaderImageGeneratorDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator\AdminImageGeneratorDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator\AdminLegacyImageGeneratorDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator\AdminProductCategoryImageGeneratorDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\media\image_generator\AdminThumbnailImageGeneratorDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization\AdminMonetizationDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization\AdminVgWortDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization\ads\AdminAdsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization\ads\AdminEditAdDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\monetization\ads\AdminEditAdRuleDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\AdminEditNewsCategoryDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\AdminEditNewsPostDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\AdminNewsCategoryDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\AdminNewsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\importer\AdminEditNewsImporterDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\importer\AdminNewsImporterDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\importer\AdminViewNewsImporterDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\news\importer\AdminViewNewsImporterItemsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\newsletter\AdminEditNewsletterDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\newsletter\AdminNewsletterDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\newsletter\AdminNewsletterSubscribersDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminComparisonsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminEditComparisonDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminEditProductCategoryDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminEditProductDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminEditProductFeedDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminEditProductLinksDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminEditShopDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminImportSuggestionsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminProductCategoriesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminProductFeedImportDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminProductFeedsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminProductLinkClicksDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminProductsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\AdminShopsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\brands\AdminBrandsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\brands\AdminEditBrandsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\data\AdminProductDataDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\data\AdminProductEditDataDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\data\AdminProductEditDataFieldDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\products\data\AdminProductEditDataGroupDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\push_notifications\AdminEditPushNotificationsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\push_notifications\AdminPushNotificationsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\push_notifications\AdminPushNotificationSubscribersDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\structure\AdminInternalLinksDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\structure\AdminPagespeedDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\structure\AdminStatusCodesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\structure\AdminStructureDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\AdminAffiliateAnalyticsDetailsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\AdminEditUserDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\AdminUserProfileDetailsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\AdminUserProfilesDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\AdminUsersDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\footer_links\AdminEditFooterLinkDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\footer_links\AdminEditFooterLinkGroupDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\footer_links\AdminFooterLinksDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\random_redirects\AdminEditRandomRedirectDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\random_redirects\AdminEditRandomRedirectDestinationDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\random_redirects\AdminRandomRedirectsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\redirects\AdminEditRedirectDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\system\redirects\AdminRedirectsDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\toasts\AdminEditToastConditionDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\toasts\AdminEditToastDashboardPage;
use saschahuber\affiliatewebsitecore\page_classes\admin\dashboard\toasts\AdminToastsDashboardPage;
use saschahuber\affiliatewebsitecore\routing\AffiliateWebsiteRouter;
use saschahuber\saastemplatecore\component\GlobalPopup;
use saschahuber\saastemplatecore\component\LoadingComponent;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\page_classes\admin\AdminLoginPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\bouncer\AdminBouncersDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\bouncer\AdminEditBouncerConditionDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\bouncer\AdminEditBouncerDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminClearCacheDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminConfigDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminCronjobInfoDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminCronjobsDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminEditTrackingCodeDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminPhpInfoDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminSystemDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminTasksDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\AdminTrackingCodesDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\analytics\AdminAnalyticsDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\analytics\AdminCustomAnalyticsDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\logs\AdminLogDetailsDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\logs\AdminLogsDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\query_logs\AdminQueryLogDetailsDashboardPage;
use saschahuber\saastemplatecore\page_classes\admin\dashboard\system\query_logs\AdminQueryLogsDashboardPage;
use saschahuber\saastemplatecore\page_classes\NotFoundDashboardPage;
use saschahuber\saastemplatecore\routing\RedirectMapping;
use saschahuber\saastemplatecore\service\DashboardMenuService;

class AdminDashboardRouter extends AffiliateDashboardPage {

    public function getContent()
    {
        ob_start();
        $this->buildPage();
        $content = ob_get_clean();

        return parent::getWrappedContent($content);
    }

    public function buildPage(){
        global $CONFIG, $ROUTER;

        if(!AuthHelper::isLoggedIn()){
            UrlHelper::redirect('/login');
        }

        $dashboard_menu_service = new DashboardMenuService();

        ?>
        <div class="wrapper" id="main-wrapper">
            <div id="sidebar-wrapper" class="bg-dark">
                <div class="column d-flex flex-column flex-shrink-0 p-3 text-white" id="sidebar">
                    <div class="dropdown">
                        <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php

                            if(AuthHelper::getCurrentUser() && isset(AuthHelper::getCurrentUser()->profile_picture_path)
                                && AuthHelper::getCurrentUser()->profile_picture_path) :?>
                                <img src="<?=AuthHelper::getUserManager()->logged_in_user->profile_picture_path?>"
                                     alt="" width="32" height="32" class="rounded-circle me-2">
                            <?php else: ?>
                                <span class="default_profile_image">
                            <i class="fas fa-cogs"></i>
                        </span>
                            <?php endif; ?>
                            <strong class="dashboard-user-label"><?=AuthHelper::getCurrentUserLabel()?></strong>
                        </a>
                        <i class="fas fa-bars" id="sidebar_close" onclick="showSidebar(false)"></i>
                        <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
                            <li><a class="dropdown-item" onclick="selectAsyncPage(this, 'Einstellungen <?=$CONFIG->title_suffix?>', 'dashboard/settings')">Einstellungen</a></li>
                            <?php #<li><a class="dropdown-item" href="/dashboard/packages">Preispakete</a></li> ?>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="<?=$CONFIG->api_domain?>/auth/logout?app=admin">Abmelden</a></li>
                        </ul>
                    </div>
                    <hr>

                    <div id="menu-items">
                        <ul class="nav nav-pills flex-column mb-auto">
                            <?=$dashboard_menu_service->displayDashboardMenu($ROUTER->getPath())?>
                        </ul>

                        <hr>

                        <div class="full-width">
                            <?php (new ToolbarButton(['full-width']))->display(); ?>
                        </div>
                    </div>

                    <hr>

                    <div class="nav-footer">
                <span class="privacy-links">
                    <a href="<?=$CONFIG->impressum_url?>" target="_blank">Impressum</a> & <a href="<?=$CONFIG->datenschutz_url?>" target="_blank">Datenschutz</a>
                </span>
                    </div>
                </div>
            </div>

            <div id="dashboard-wrapper">
                <div id="navbar">
                    <div id="navbar_content">
                        <p>
                            <i class="fas fa-bars" id="sidebar_open" onclick="showSidebar(true)"></i>
                            <?php if($CONFIG->app_logo !== null): ?>
                                <img src="<?=$CONFIG->app_logo?>">
                            <?php else: ?>
                                <span id="navbar_label"><?=$CONFIG->app_name?></span>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>

                <div id="dashboard-container">
                    <?=(new LoadingComponent(false, 'main-content-loading-animation'))->getContent() ?>
                    <div id="dashboard-container-inner">
                        <?php
                        echo $this->getSubPageContent();
                        ?>

                        <div id="query-log">
                            <?php #echo $DB->printQueryLog(); ?>
                        </div>
                        <div class="toast-container" id="toast-container"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="global-popup-container position_unset">
            <?php (new GlobalPopup())->display(); ?>
        </div>
        <?php
    }

    public function getSubPageContent(){
        $sub_router = new AffiliateWebsiteRouter();
        $sub_router->setRoutes($this->getRoutes());
        $page = $sub_router->getCurrentPage();

        if(!$page){
            $page = (new NotFoundDashboardPage($this->path));
        }

        //Set meta data
        $this->setTitle($page->getTitle());
        $this->setDescription($page->getDescription());
        $this->setIndex($page->getIndex());
        $this->setFollow($page->getFollow());
        $this->setOgImage($page->getOgImage());

        return $page->getCssCode()
            . $page->getJsCode()
            . $page->getContent();
    }

    public static function getRoutes(){
        return [
            'login' => AdminLoginPage::class,
            '' => new RedirectMapping('dashboard/overview'),
            'dashboard' => new RedirectMapping('dashboard/overview'),
            'dashboard/overview' => AdminOverviewDashboardPage::class,
            'dashboard/structure/pagespeed' => AdminPagespeedDashboardPage::class,
            'dashboard/structure/internal-links' => AdminInternalLinksDashboardPage::class,
            'dashboard/structure/status-codes' => AdminStatusCodesDashboardPage::class,
            'dashboard/structure' => AdminStructureDashboardPage::class,
            'dashboard/medien/stock' => AdminStockMediaDashboardPage::class,
            'dashboard/medien/stock/*' => AdminStockMediaDashboardPage::class,
            'dashboard/medien/icon' => AdminIconDashboardPage::class,
            'dashboard/medien/icon/*' => AdminIconDashboardPage::class,
            'dashboard/medien/dateien' => AdminFilesDashboardPage::class,
            'dashboard/medien/dateien/*' => AdminFilesDashboardPage::class,
            'dashboard/medien/dalle' => AdminDallEOverviewDashboardPage::class,
            'dashboard/medien/dalle/erstellen' => AdminDallEGeneratorDashboardPage::class,
            'dashboard/medien/bild-generator-alt' => AdminLegacyImageGeneratorDashboardPage::class,
            'dashboard/medien/bild-generator/produkt-kategorie' => AdminProductCategoryImageGeneratorDashboardPage::class,
            'dashboard/medien/bild-generator/fancy-header' => AdminFancyHeaderImageGeneratorDashboardPage::class,
            'dashboard/medien/bild-generator/thumbnail' => AdminThumbnailImageGeneratorDashboardPage::class,
            'dashboard/medien/bild-generator' => AdminImageGeneratorDashboardPage::class,
            'dashboard/medien' => AdminMediaDashboardPage::class,
            'dashboard/medien/*' => AdminMediaDashboardPage::class,
            'dashboard/newsletter' => AdminNewsletterDashboardPage::class,
            'dashboard/newsletter/abonnenten' => AdminNewsletterSubscribersDashboardPage::class,
            'dashboard/newsletter/bearbeiten' => AdminEditNewsletterDashboardPage::class,
            'dashboard/newsletter/bearbeiten/*' => AdminEditNewsletterDashboardPage::class,
            'dashboard/push-notifications' => AdminPushNotificationsDashboardPage::class,
            'dashboard/push-notifications/abonnenten' => AdminPushNotificationSubscribersDashboardPage::class,
            'dashboard/push-notifications/bearbeiten' => AdminEditPushNotificationsDashboardPage::class,
            'dashboard/push-notifications/bearbeiten/*' => AdminEditPushNotificationsDashboardPage::class,
            'dashboard/blog/bearbeiten' => AdminEditBlogPostDashboardPage::class,
            'dashboard/blog/bearbeiten/*' => AdminEditBlogPostDashboardPage::class,
            'dashboard/blog/kategorien/bearbeiten' => AdminEditBlogCategoryDashboardPage::class,
            'dashboard/blog/kategorien/bearbeiten/*' => AdminEditBlogCategoryDashboardPage::class,
            'dashboard/blog/kategorien' => AdminBlogCategoryDashboardPage::class,
            'dashboard/blog/kategorien/*' => AdminBlogCategoryDashboardPage::class,
            'dashboard/blog' => AdminBlogDashboardPage::class,
            'dashboard/blog/*' => AdminBlogDashboardPage::class,
            'dashboard/news/importer' => AdminNewsImporterDashboardPage::class,
            'dashboard/news/importer/ansehen/*' => AdminViewNewsImporterDashboardPage::class,
            'dashboard/news/importer/bearbeiten/*' => AdminEditNewsImporterDashboardPage::class,
            'dashboard/news/importer/items' => AdminViewNewsImporterItemsDashboardPage::class,
            'dashboard/news/bearbeiten' => AdminEditNewsPostDashboardPage::class,
            'dashboard/news/bearbeiten/*' => AdminEditNewsPostDashboardPage::class,
            'dashboard/news/kategorien/bearbeiten' => AdminEditNewsCategoryDashboardPage::class,
            'dashboard/news/kategorien/bearbeiten/*' => AdminEditNewsCategoryDashboardPage::class,
            'dashboard/news/kategorien' => AdminNewsCategoryDashboardPage::class,
            'dashboard/news/kategorien/*' => AdminNewsCategoryDashboardPage::class,
            'dashboard/news' => AdminNewsDashboardPage::class,
            'dashboard/news/*' => AdminNewsDashboardPage::class,
            'dashboard/produkte/import-suggestions' => AdminImportSuggestionsDashboardPage::class,
            'dashboard/produkte/hersteller' => AdminBrandsDashboardPage::class,
            'dashboard/produkte/hersteller/bearbeiten' => AdminEditBrandsDashboardPage::class,
            'dashboard/produkte/hersteller/bearbeiten/*' => AdminEditBrandsDashboardPage::class,
            'dashboard/produkte/kategorien' => AdminProductCategoriesDashboardPage::class,
            'dashboard/produkte/kategorien/bearbeiten' => AdminEditProductCategoryDashboardPage::class,
            'dashboard/produkte/kategorien/bearbeiten/*' => AdminEditProductCategoryDashboardPage::class,
            'dashboard/produkte/bearbeiten' => AdminEditProductDashboardPage::class,
            'dashboard/produkte/bearbeiten/*' => AdminEditProductDashboardPage::class,
            'dashboard/produkte/feeds' => AdminProductFeedsDashboardPage::class,
            'dashboard/produkte/feeds/bearbeiten' => AdminEditProductFeedDashboardPage::class,
            'dashboard/produkte/feeds/bearbeiten/*' => AdminEditProductFeedDashboardPage::class,
            'dashboard/produkte/feeds/import/*' => AdminProductFeedImportDashboardPage::class,
            'dashboard/produkte/link-bearbeiten/*' => AdminEditProductLinksDashboardPage::class,
            'dashboard/produkte/link-clicks' => AdminProductLinkClicksDashboardPage::class,
            'dashboard/produkte/shops' => AdminShopsDashboardPage::class,
            'dashboard/produkte/shops/bearbeiten' => AdminEditShopDashboardPage::class,
            'dashboard/produkte/shops/bearbeiten/*' => AdminEditShopDashboardPage::class,
            'dashboard/produkte/vergleichsseiten' => AdminComparisonsDashboardPage::class,
            'dashboard/produkte/vergleichsseiten/bearbeiten' => AdminEditComparisonDashboardPage::class,
            'dashboard/produkte/vergleichsseiten/bearbeiten/*' => AdminEditComparisonDashboardPage::class,
            'dashboard/produkte/daten' => AdminProductDataDashboardPage::class,
            'dashboard/produkte/daten/bearbeiten/*' => AdminProductEditDataGroupDashboardPage::class,
            'dashboard/produkte/daten/bearbeiten' => AdminProductEditDataGroupDashboardPage::class,
            'dashboard/produkte/daten/feld-bearbeiten/*' => AdminProductEditDataFieldDashboardPage::class,
            'dashboard/produkte/daten/feld-bearbeiten' => AdminProductEditDataFieldDashboardPage::class,
            'dashboard/produkte/daten/produkt-bearbeiten/*' => AdminProductEditDataDashboardPage::class,
            'dashboard/produkte/daten/produkt-bearbeiten' => AdminProductEditDataDashboardPage::class,
            'dashboard/produkte' => AdminProductsDashboardPage::class,
            'dashboard/produkte/*' => AdminProductsDashboardPage::class,
            'dashboard/system/tasks' => AdminTasksDashboardPage::class,
            'dashboard/system/analytics' => AdminAnalyticsDashboardPage::class,
            'dashboard/system/analytics/custom' => AdminCustomAnalyticsDashboardPage::class,
            'dashboard/system/analytics/details' => AdminAffiliateAnalyticsDetailsDashboardPage::class,
            'dashboard/system/logs' => AdminLogsDashboardPage::class,
            'dashboard/system/logs/details' => AdminLogDetailsDashboardPage::class,
            'dashboard/system/query-logs' => AdminQueryLogsDashboardPage::class,
            'dashboard/system/query-logs/details' => AdminQueryLogDetailsDashboardPage::class,
            'dashboard/system/php-info' => AdminPhpInfoDashboardPage::class,
            'dashboard/system/config' => AdminConfigDashboardPage::class,
            'dashboard/system/cache-leeren' => AdminClearCacheDashboardPage::class,
            'dashboard/system/autoren' => AdminAuthorDashboardPage::class,
            'dashboard/system/autoren/bearbeiten' => AdminEditAuthorDashboardPage::class,
            'dashboard/system/autoren/bearbeiten/*' => AdminEditAuthorDashboardPage::class,
            'dashboard/system/cronjobs' => AdminCronjobsDashboardPage::class,
            'dashboard/system/cronjobs/*' => AdminCronjobInfoDashboardPage::class,
            'dashboard/system/users' => AdminUsersDashboardPage::class,
            'dashboard/system/users/bearbeiten' => AdminEditUserDashboardPage::class,
            'dashboard/system/users/bearbeiten/*' => AdminEditUserDashboardPage::class,
            'dashboard/system/tracking-codes' => AdminTrackingCodesDashboardPage::class,
            'dashboard/system/tracking-codes/bearbeiten' => AdminEditTrackingCodeDashboardPage::class,
            'dashboard/system/tracking-codes/bearbeiten/*' => AdminEditTrackingCodeDashboardPage::class,
            'dashboard/system/redirects' => AdminRedirectsDashboardPage::class,
            'dashboard/system/redirects/bearbeiten' => AdminEditRedirectDashboardPage::class,
            'dashboard/system/redirects/bearbeiten/*' => AdminEditRedirectDashboardPage::class,
            'dashboard/system/random-redirects' => AdminRandomRedirectsDashboardPage::class,
            'dashboard/system/random-redirects/bearbeiten' => AdminEditRandomRedirectDashboardPage::class,
            'dashboard/system/random-redirects/bearbeiten/*' => AdminEditRandomRedirectDashboardPage::class,
            'dashboard/system/random-redirects/ziel-bearbeiten' => AdminEditRandomRedirectDestinationDashboardPage::class,
            'dashboard/system/random-redirects/ziel-bearbeiten/*' => AdminEditRandomRedirectDestinationDashboardPage::class,
            'dashboard/system/footer-links' => AdminFooterLinksDashboardPage::class,
            'dashboard/system/footer-links/gruppe-bearbeiten/*' => AdminEditFooterLinkGroupDashboardPage::class,
            'dashboard/system/footer-links/link-bearbeiten/*' => AdminEditFooterLinkDashboardPage::class,
            'dashboard/system' => AdminSystemDashboardPage::class,
            'dashboard/user-profiles' => AdminUserProfilesDashboardPage::class,
            'dashboard/user-profiles/*/details' => AdminUserProfileDetailsDashboardPage::class,
            'dashboard/seiten' => AdminPagesDashboardPage::class,
            'dashboard/seiten/bearbeiten' => AdminEditPageDashboardPage::class,
            'dashboard/seiten/bearbeiten/*' => AdminEditPageDashboardPage::class,
            'dashboard/seiten/links/bearbeiten' => AdminEditLinkPageItemDashboardPage::class,
            'dashboard/seiten/links/bearbeiten/*' => AdminEditLinkPageItemDashboardPage::class,
            'dashboard/seiten/links' => AdminLinkPageDashboardPage::class,
            'dashboard/seiten/links/*' => AdminLinkPageDashboardPage::class,
            'dashboard/content-schedule' => AdminContentScheduleDashboardPage::class,
            'dashboard/postings' => AdminPostingsDashboardPage::class,
            'dashboard/postings/bearbeiten' => AdminEditPostingsDashboardPage::class,
            'dashboard/postings/bearbeiten/*' => AdminEditPostingsDashboardPage::class,
            'dashboard/guide-filter' => AdminGuideFilterDashboardPage::class,
            'dashboard/guide-filter/bearbeiten/*' => AdminEditGuideFilterDashboardPage::class,
            'dashboard/guide-filter/step-bearbeiten/option-bearbeiten/filter-bearbeiten' => AdminEditGuideFilterStepOptionFilterDashboardPage::class,
            'dashboard/guide-filter/step-bearbeiten/option-bearbeiten/filter-bearbeiten/*' => AdminEditGuideFilterStepOptionFilterDashboardPage::class,
            'dashboard/guide-filter/step-bearbeiten/option-bearbeiten' => AdminEditGuideFilterStepOptionDashboardPage::class,
            'dashboard/guide-filter/step-bearbeiten/option-bearbeiten/*' => AdminEditGuideFilterStepOptionDashboardPage::class,
            'dashboard/guide-filter/step-bearbeiten' => AdminEditGuideFilterStepDashboardPage::class,
            'dashboard/guide-filter/step-bearbeiten/*' => AdminEditGuideFilterStepDashboardPage::class,
            'dashboard/monetization' => AdminMonetizationDashboardPage::class,
            'dashboard/monetization/vg-wort' => AdminVgWortDashboardPage::class,
            'dashboard/monetization/ads' => AdminAdsDashboardPage::class,
            'dashboard/monetization/ads/bearbeiten' => AdminEditAdDashboardPage::class,
            'dashboard/monetization/ads/bearbeiten/*' => AdminEditAdDashboardPage::class,
            'dashboard/monetization/ads/regel-bearbeiten' => AdminEditAdRuleDashboardPage::class,
            'dashboard/toasts' => AdminToastsDashboardPage::class,
            'dashboard/toasts/*/condition' => AdminEditToastConditionDashboardPage::class,
            'dashboard/toasts/*/condition/*' => AdminEditToastConditionDashboardPage::class,
            'dashboard/toasts/bearbeiten' => AdminEditToastDashboardPage::class,
            'dashboard/toasts/bearbeiten/*' => AdminEditToastDashboardPage::class,
            'dashboard/bouncer' => AdminBouncersDashboardPage::class,
            'dashboard/bouncer/*/condition' => AdminEditBouncerConditionDashboardPage::class,
            'dashboard/bouncer/*/condition/*' => AdminEditBouncerConditionDashboardPage::class,
            'dashboard/bouncer/bearbeiten' => AdminEditBouncerDashboardPage::class,
            'dashboard/bouncer/bearbeiten/*' => AdminEditBouncerDashboardPage::class,
            'dashboard/dienstleister' => AdminCompaniesDashboardPage::class,
            'dashboard/dienstleister/bearbeiten' => AdminEditCompanyDashboardPage::class,
            'dashboard/dienstleister/bearbeiten/*' => AdminEditCompanyDashboardPage::class,
            'dashboard/dienstleister/importer' => AdminImportCompaniesDashboardPage::class,
            'dashboard/dienstleister/kategorien' => AdminCompanyCategoriesDashboardPage::class,
            'dashboard/dienstleister/kategorien/bearbeiten' => AdminEditCompanyCategoryDashboardPage::class,
            'dashboard/dienstleister/kategorien/bearbeiten/*' => AdminEditCompanyCategoryDashboardPage::class,
            #'dashboard/faq' => AdminFaqsDashboardPage::class,
            #'dashboard/faq/bearbeiten' => AdminEditFaqDashboardPage::class,
            #'dashboard/faq/bearbeiten/*' => AdminEditFaqDashboardPage::class,
            'dashboard/crm' => AdminCrmDashboardPage::class,
            'dashboard/crm/contacts' => AdminCrmContactsDashboardPage::class,
            'dashboard/crm/contacts/edit' => AdminEditCrmContactDashboardPage::class,
            'dashboard/crm/contacts/edit/*' => AdminEditCrmContactDashboardPage::class,
            'dashboard/crm/notes' => AdminCrmNotesDashboardPage::class,
            'dashboard/crm/notes/edit' => AdminEditCrmNoteDashboardPage::class,
            'dashboard/crm/notes/edit/*' => AdminEditCrmNoteDashboardPage::class,
            'dashboard/crm/persons' => AdminCrmPersonsDashboardPage::class,
            'dashboard/crm/persons/edit' => AdminEditCrmPersonDashboardPage::class,
            'dashboard/crm/persons/edit/*' => AdminEditCrmPersonDashboardPage::class,
        ];
    }

}