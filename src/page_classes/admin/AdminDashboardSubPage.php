<?php

namespace saschahuber\affiliatewebsitecore\page_classes\admin;

use saschahuber\affiliatewebsitecore\component\ToolbarButton;
use saschahuber\saastemplatecore\component\GlobalPopup;
use saschahuber\saastemplatecore\component\LoadingComponent;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\service\DashboardMenuService;

abstract class AdminDashboardSubPage extends AffiliateDashboardPage {
    public function buildPage(){
        global $CONFIG;
        $dashboard_menu_service = new DashboardMenuService();

        ?>
        <div class="wrapper" id="main-wrapper">
            <div id="sidebar-wrapper" class="bg-dark">
                <div class="column d-flex flex-column flex-shrink-0 p-3 text-white" id="sidebar">
                    <div class="dropdown">
                        <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php

                            if(AuthHelper::getCurrentUser() && isset(AuthHelper::getCurrentUser()->profile_picture_path)
                                && AuthHelper::getCurrentUser()->profile_picture_path) :?>
                                <img src="<?=AuthHelper::getUserManager()->logged_in_user->profile_picture_path?>"
                                     alt="" width="32" height="32" class="rounded-circle me-2">
                            <?php else: ?>
                                <span class="default_profile_image">
                            <i class="fas fa-cogs"></i>
                        </span>
                            <?php endif; ?>
                            <strong class="dashboard-user-label"><?=AuthHelper::getCurrentUserLabel()?></strong>
                        </a>
                        <i class="fas fa-bars" id="sidebar_close" onclick="showSidebar(false)"></i>
                        <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
                            <li><a class="dropdown-item" onclick="selectAsyncPage(this, 'Einstellungen <?=$CONFIG->title_suffix?>', 'dashboard/settings')">Einstellungen</a></li>
                            <?php #<li><a class="dropdown-item" href="/dashboard/packages">Preispakete</a></li> ?>
                            <li><hr class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="<?=$CONFIG->api_domain?>/auth/logout?app=admin">Abmelden</a></li>
                        </ul>
                    </div>
                    <hr>

                    <div id="menu-items">
                        <ul class="nav nav-pills flex-column mb-auto">
                            <?=$dashboard_menu_service->displayDashboardMenu($this->path)?>
                        </ul>

                        <hr>

                        <div class="full-width">
                            <?php (new ToolbarButton(['full-width']))->display(); ?>
                        </div>
                    </div>

                    <hr>

                    <div class="nav-footer">
                <span class="privacy-links">
                    <a href="<?=$CONFIG->impressum_url?>" target="_blank">Impressum</a> & <a href="<?=$CONFIG->datenschutz_url?>" target="_blank">Datenschutz</a>
                </span>
                    </div>
                </div>
            </div>

            <div id="dashboard-wrapper">
                <div id="navbar">
                    <div id="navbar_content">
                        <p>
                            <i class="fas fa-bars" id="sidebar_open" onclick="showSidebar(true)"></i>
                            <?php if($CONFIG->app_logo !== null): ?>
                                <img src="<?=$CONFIG->app_logo?>">
                            <?php else: ?>
                                <span id="navbar_label"><?=$CONFIG->app_name?></span>
                            <?php endif; ?>
                        </p>
                    </div>
                </div>

                <div id="dashboard-container">
                    <?=(new LoadingComponent(false, 'main-content-loading-animation'))->getContent() ?>
                    <div id="dashboard-container-inner">
                        <?php
                        ob_start();
                        $this->buildInnerContent();
                        echo ob_get_clean();
                        ?>

                        <div id="query-log">
                            <?php #echo $DB->printQueryLog(); ?>
                        </div>
                        <div class="toast-container" id="toast-container"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="global-popup-container position_unset">
            <?php (new GlobalPopup())->display(); ?>
        </div>
        <?php
    }

    public abstract function buildInnerContent();
}