<?php

namespace saschahuber\affiliatewebsitecore\page_classes;


use saschahuber\saastemplatecore\routing\Page;

abstract class AffiliateWebsitePage extends Page
{
    private $menu, $footer, $header, $vg_wort_marke;

    /**
     * @return mixed
     */
    public function getVgWortMarke()
    {
        return $this->vg_wort_marke;
    }

    /**
     * @param mixed $vg_wort_marke
     */
    public function setVgWortMarke($vg_wort_marke): void
    {
        $this->vg_wort_marke = $vg_wort_marke;
    }


}