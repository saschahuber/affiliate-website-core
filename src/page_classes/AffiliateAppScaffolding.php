<?php

namespace saschahuber\affiliatewebsitecore\page_classes;


use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\CssHelper;
use saschahuber\saastemplatecore\helper\DependencyHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\JsHelper;
use saschahuber\saastemplatecore\manager\TrackingCodeManager;
use saschahuber\saastemplatecore\model\Resource;
use saschahuber\saastemplatecore\ResourceScheduler;
use saschahuber\saastemplatecore\routing\Scaffolding;

abstract class AffiliateAppScaffolding extends Scaffolding
{
    private $menu, $footer, $header, $vg_wort_marke;

    public function __construct($page){
        parent::__construct($page);
    }

    public function getWrappedContent($content){
        ob_start();
        ResourceScheduler::addFont('webfonts/DMSans-Light.woff2', Resource::POSITION_HEAD, false);
        ResourceScheduler::addFont('webfonts/DMSans-Regular.woff2', Resource::POSITION_HEAD, false);
        ResourceScheduler::addFont('webfonts/DMSans-Bold.woff2', Resource::POSITION_HEAD, true);

        ResourceScheduler::addCss('css/global.css', Resource::POSITION_HEAD, true);

        $head_html = BufferHelper::buffered(function () {
            global $CONFIG, $ROUTER, $DB;

            $meta_robots = "noindex, nofollow";
            if (!Helper::isAdminApp()) {
                if ($CONFIG->index !== false) {
                    $meta_robots = ($this->page->getIndex()?'index':'noindex').','.($this->page->getFollow()?'follow':'nofollow');
                }
            }

            ?>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                <meta name="robots" content="<?= $meta_robots ?>">
                <meta name="viewport" content="width=device-width,initial-scale=1.0">
                <meta name="description" content="<?= $this->page->getDescription() ?>">
                <meta name="keywords" content="<?= $this->page->getKeywords() ?>">
                <title><?= $this->page->getTitle() ?></title>

                <!-- Base path & favicon -->
                <base href="<?= $CONFIG->app_domain ?>">

                <?php if (Helper::isAdminApp()): ?>
                    <script type="text/javascript" src="<?= $CONFIG->admin_domain ?>/js/global.js"></script>

                    <script src="<?= $CONFIG->app_domain ?>/lib/ckeditor/ckeditor.js"></script>
                    <script src="<?= $CONFIG->app_domain ?>/static/js/chart-js.js"></script>
                <?php endif; ?>

                <!-- Preloads -->
                <?php ResourceScheduler::displayPreloads() ?>

                <script>window.MSInputMethodContext && document.documentMode && document.write('<script defer src="https://cdn.jsdelivr.net/gh/nuxodin/ie11CustomProperties@4.1.0/ie11CustomProperties.min.js"><\/script>');</script>

                <?= DependencyHelper::getAllCssAndJs(Helper::isWebsite(), Helper::isApp()) ?>

                <link href="<?= CssHelper::getCssFileWithVersion() ?>" as="style">

                <link href="<?= JsHelper::getJsFileWithVersion() ?>" as="script">

                <link rel="shortcut icon" href="/favicon.ico">
                <link rel="canonical" href="<?= $CONFIG->app_domain . '/' . $this->page->getCanonical() ?>">

                <meta property="og:locale" content="de_DE">
                <meta property="og:type" content="website">
                <meta property="og:title" content="<?= $this->page->getTitle() ?>">
                <meta property="og:description" content="<?= $this->page->getDescription() ?>">
                <meta property="og:url" content="<?= PROTOCOL . DOMAIN . $_SERVER['REQUEST_URI'] ?>">
                <meta property="og:site_name" content="<?= $CONFIG->app_name ?>">
                <meta property="og:image" content="<?= $this->page->getOgImage() ?>">
                <meta property="og:image:width" content="1200">
                <meta property="og:image:height" content="627">
                <meta name="twitter:card" content="summary_large_image">
                <meta name="twitter:title" content="<?= $this->page->getTitle() ?>">
                <meta name="twitter:description" content="<?= $this->page->getDescription() ?>">
                <meta name="twitter:image" content="<?= $this->page->getOgImage() ?>">

                <?php # Google Site Verification
                if (!empty($CONFIG->google_site_verification)): ?>
                    <meta name="google-site-verification" content="<?= $CONFIG->google_site_verification ?>">
                <?php endif; ?>

                <!-- CSS & JS -->

                <meta name="viewport" content="width=device-width, initial-scale=1">

                <script type="text/javascript">

                    function getHeader() {
                        return document.getElementsByTagName('header')[0];
                    }

                    function getMain() {
                        return document.getElementsByTagName('main')[0];
                    }

                    function getFooter() {
                        return document.getElementsByTagName('footer')[0];
                    }

                </script>

                <meta name="viewport" content="width=device-width, initial-scale=1">

                <script type="text/javascript" defer>

                    'use strict';

                    /* Dynamic JS globals */
                    var userIsLoggedin = <?=(AuthHelper::isLoggedIn() ? 'true' : 'false')?>;
                    var gaProperty = '<?=$CONFIG->ga_property?>';
                    var publicPushKey = '<?=$CONFIG->push_public_key?>';
                    var websiteDomain = '<?=$CONFIG->website_domain?>';
                    var appDomain = '<?=$CONFIG->app_domain?>';
                    var apiDomain = '<?=$CONFIG->api_domain?>';
                    var adminDomain = '<?=$CONFIG->admin_domain?>';
                    var bouncerInfo = {
                        enabled: <?=($CONFIG->enable_bouncer ? 'true' : 'false')?>,
                        cooldown: <?=$CONFIG->bouncer_cooldown?>,
                        excludePaths: [],
                        iframePath: '/registrieren?minimal'
                    }

                    // TEMP TEST
                    if (!('keyState' in window)) {
                        throw new Error('global.js missing')
                    }

                    // TEMP TEST
                    if (!('getMain' in window)) {
                        throw new Error('getMain/etc. missing')
                    }

                </script>

                <?php
                if (!Helper::isAdminApp()) {
                    TrackingCodeManager::displayTrackingCodesHead();

                    #if(!AuthHelper::isLoggedIn()) {
                    #    (new Bouncer())->display();
                    #}
                }
                ?>
            </head>
            <?php
        });

        ?>
        <!DOCTYPE html>
        <html lang="de">
            <?= $head_html ?>
            <?= $content ?>

            <?php if (isset($this->page->vg_wort_marke) && $this->page->vg_wort_marke !== null): ?>
                <img src="https://vg08.met.vgwort.de/na/<?= $this->page->vg_wort_marke ?>" width="1" height="1" alt="">
            <?php endif; ?>
        </html>
        <?php
        return ob_get_clean();
    }

    /**
     * @return mixed
     */
    public function getVgWortMarke()
    {
        return $this->vg_wort_marke;
    }

    /**
     * @param mixed $vg_wort_marke
     */
    public function setVgWortMarke($vg_wort_marke): void
    {
        $this->vg_wort_marke = $vg_wort_marke;
    }


}