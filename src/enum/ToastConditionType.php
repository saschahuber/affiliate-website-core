<?php

namespace saschahuber\affiliatewebsitecore\enum;

class ToastConditionType
{
    const NEW_USER = "new_user";
    const RECURRING_USER = "recurring_user";
    const URL_PATH = "url_path";

    public static function allConditionTypes(){
        return [
            self::NEW_USER => 'Neuer Nutzer',
            self::RECURRING_USER => 'Wiederkehrender Nutzer',
            self::URL_PATH => 'Unterverzeichnis in URL'
        ];
    }
}