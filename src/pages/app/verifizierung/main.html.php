<div class="verify-container">
    <h1>Bestätigung Ihrer E-Mail-Adresse</h1>

    <?php

    if ($this->success) {
        ?>
        <p>Ihre E-Mail-Adresse wurde erfolgreich bestätigt.</p>

        <br>

        <a class="btn btn-primary btn-md btn-rouded centered" href="/dashboard">Zum Dashboard</a>
        <?php
    } else {
        ?>

        <p>Die Bestätigung Ihrer E-Mail-Adresse ist fehlgeschlagen. Der Schlüssel war nicht mehr gültig.</p>

        <br>

        <p>Bitte loggen Sie sich ein, um einen neuen Bestätigungslink anzufordern:</p>

        <br>

        <a class="btn btn-primary btn-md btn-rouded centered" href="/dashboard">Zum Dashboard</a>

        <?php
    }

    ?>

</div>