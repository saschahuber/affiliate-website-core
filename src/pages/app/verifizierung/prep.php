<?php

use saschahuber\affiliatewebsitecore\manager\NewsletterManager;

if (!empty($_GET["key"])) {
    $confirmation_token_validity = 30 * 60 * 60 * 24; # 30 Tage

    $user = null;
    $dbquery = $DB->query('SELECT * FROM user WHERE confirmation_token = "' . $DB->escape($_GET["key"]) . '"
                                        AND (
                                            UNIX_TIMESTAMP(registration_date) >=
                                            (UNIX_TIMESTAMP(CURRENT_TIMESTAMP)-('.intval($confirmation_token_validity).'))
                                        )');

    if ($dbquery->num_rows > 0) {
        $user = $dbquery->fetchObject();
    }

    $this->success = false;

    if ($user) {
        $DB->query('UPDATE user SET status = "confirmed",
                confirmation_token = null,
                confirmation_date = CURRENT_TIMESTAMP() WHERE id = ' . intval($user->id));
        $this->success = true;
        $this->user_auth_service->login_by_id($user->id);

        $newsletter_manager = new NewsletterManager();
        $newsletter_manager->confirmSubscriberByUser($user, false);
    }
}