<h3>
    <i class="fas fa-envelope"></i>
    Postfach
</h3>

<?php use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\AuthHelper;

if (!$this->dbquery_inbox->num_rows): ?>

    <p class="empty">
        <?=sprintf("%s Postfach ist im Augenblick leer", (AuthHelper::hasPermission(AffiliateWebsitePermissionService::ACCESS_COMPANY_DASHBOARD) ? 'Ihr' : 'Dein'))?>
    </p>

<?php else: ?>

    <ul class="text">
        <?php $item_no = 0;
        while ($email = $this->dbquery_inbox->fetchObject()):
            if ($item_no++ == 10): ?>
                <a onclick="showMore(this, 10);">
                    Mehr anzeigen
                    <i class="fas fa-angle-down"></i>
                </a>
            <?php endif; ?>

            <li class="<?= $email->status ?>" data-id="<?= $email->id ?>">
                <div class="status">
                    <i class="fas fa-circle"></i>
                </div>
                <div class="subject">
                    <?php if (is_null($email->html_file)){
                        echo $email->subject;
                    } ?>
                </div>
                <div class="date">
                    <?= date($email->date_sent > ONE_DAY_AGO ? 'H:i' : 'd.m.Y', $email->date_sent) ?>
                </div>
            </li>
        <?php endwhile; ?>
    </ul>

    <hr>

<?php endif;
