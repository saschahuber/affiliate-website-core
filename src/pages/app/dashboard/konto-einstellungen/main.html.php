<?php

use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\AuthHelper;

?>
<h3>
    <i class="fas fa-cogs"></i>
    Account-Einstellungen
</h3>

<?= $this->main_html ?>

<?php if (true || $USER->service_registered == "email"): ?>

    <div class="row">
        <div class="col-lg-12">
            <form method="post" action="" autocomplete="off">
                <h5>
                    E-Mail-Adresse Ändern
                </h5>
                <?php #echo $this->get_msgs("email") ?>
                <p>
                    <label>
                        Passwort
                    </label>
                    <input type="password" name="changeemail_pw" value="">
                </p>
                <p>
                    <label>
                        Bisherige E-Mail-Adresse
                    </label>
                    <input type="text" value="<?= $this->user_auth_service->logged_in_user->email ?>" disabled>
                </p>
                <p>
                    <label>
                        Neue E-Mail-Adresse
                    </label>
                    <input type="text" name="changeemail_new" value="" data-validate="email-create"
                           data-lock="changeemail_confirm">
                </p>
                <p>
                    <label>
                        Neue E-Mail-Adresse wiederholen
                    </label>
                    <input type="text" name="changeemail_confirm" data-validate="identical">
                </p>
                <input type="submit" name="changeemail_submit" value="Ändern" class="btn btn-primary btn-rouded btn-md"
                       onclick="checkForm(this, event);">
                <hr>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form method="post" action="" autocomplete="off">
                <h5>
                    Passwort ändern
                </h5>
                <p class="explanation">
                    <?php if (AuthHelper::hasPermission(AffiliateWebsitePermissionService::ACCESS_COMPANY_DASHBOARD)): ?>
                        Haben Sie Ihr Benutzerkonto erst kürzlich angelegt, so wurde Ihnen ein erstes, generiertes Passwort per E-Mail zugesandt.
                        Wenn Sie bereits ein eigenes Passwort gesetzt haben aber dieses nicht mehr wissen, können Sie sich ausloggen und
                        anschließend die "Passwort vergessen"-Funktion nutzen.
                    <?php else: ?>
                        Wenn du bereits ein eigenes Passwort gesetzt hast aber dieses nicht mehr weißt, kannst du dich ausloggen und
                        anschließend die "Passwort vergessen"-Funktion nutzen.
                    <?php endif; ?>
                </p>
                <?php #echo $this->get_msgs("password") ?>
                <p>
                    <label>
                        Bisheriges Passwort
                    </label>
                    <input type="password" name="changepw_old" value="">
                </p>
                <p>
                    <label>
                        Neues Passwort
                    </label>
                    <input type="password" name="changepw_new" value="" data-validate="password" data-lock="changepw_confirm">
                </p>
                <p>
                    <label>
                        Neues Passwort wiederholen
                    </label>
                    <input type="password" name="changepw_confirm" value="" data-validate="identical">
                </p>
                <input type="submit" name="changepw_submit" value="Ändern" class="btn btn-primary btn-rouded btn-md"
                       onclick="checkForm(this, event);">
                <hr>
            </form>
        </div>
    </div>

<?php else: ?>

    <form autocomplete="off">
        <h5>
            Änderung von E-Mail-Adresse und Passwort
        </h5>
        <p>
            <label>
                Bisherige E-Mail-Adresse
            </label>
            <input type="text" value="<?= $this->user_auth_service->logged_in_user->email ?>" disabled>
        </p>
        <p>
            Dieses Konto ist an ein <?=ucfirst($USER->service_registered)?>-Konto gebunden.
            Daher ist es nicht möglich, die E-Mail Adresse oder das Passwort zu ändern.
            <?=(AuthHelper::hasPermission(AffiliateWebsitePermissionService::ACCESS_COMPANY_DASHBOARD) ? "Sie können sich" : "Du kannst dich")?>
            stattdessen einfach über den "Via <?=ucfirst($USER->service_registered)?> einloggen"-Button anmelden.
        </p>
    </form>

<?php endif; ?>
