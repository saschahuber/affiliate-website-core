<?php

use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\AuthHelper;

$this->title = "Einstellungen";
$this->h1 = $this->title;

if (AuthHelper::hasPermission(AffiliateWebsitePermissionService::ACCESS_COMPANY_DASHBOARD)) {
    $this->subbase = APP_BASE . "/pages/dashboard/gewerbekunden-einstellungen";
}