<?php

/*
if ($USER->service_registered != "email") {
    return;
}
*/

use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\AuthHelper;

if (isset($_POST["changepw_submit"])) {
    if ($this->user_auth_service->verify($_POST["changepw_old"], $this->logged_in_user)) {
        $this->user_auth_service->updatePassword($this->user_auth_service->logged_in_user, $_POST["changepw_new"]);
        $this->addMsg("Das Passwort wurde aktualisiert.", "success", "password");
    } else {
        $this->addMsg("Dieses Passwort ist nicht korrekt.", "failure", "password");
    }
}

if (isset($_POST["changeemail_submit"])) {
    if (!validate_password($_POST["changeemail_pw"], $USER)) {
        $this->addMsg("Dieses Passwort ist nicht korrekt.", "failure", "email");
        return;
    }

    $new_email = $DB->escape($_POST["changeemail_new"]);
    if (!filter_var($new_email, FILTER_VALIDATE_EMAIL)) {
        $this->addMsg("Diese E-Mail-Adresse ist nicht gültig.", "failure", "email");
        return;
    }

    if ($this->user_auth_service->get_by_mail($new_email)) {
        $this->addMsg("Diese E-Mail-Adresse ist bereits in Verwendung.", "failure", "email");
        return;
    }

    $this->user_auth_service->logged_in_user->email = $new_email;
    $this->user_auth_service->logged_in_user->status = "registered";

    $this->user_auth_service->save_user();

    // Sende Mail zur verifizierung der E-Mail-Adresse
    $this->user_auth_service->load_settings();

    $this->auth_mail_service->sendConfirmationMail($this->user_auth_service->logged_in_user,
        AuthHelper::hasPermission(AffiliateWebsitePermissionService::ACCESS_COMPANY_DASHBOARD));
}
