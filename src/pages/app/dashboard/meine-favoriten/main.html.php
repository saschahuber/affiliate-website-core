<?php

use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\UrlHelper;

if (!$this->user_auth_service->logged_in_user->status=="confirmed"): ?>

    <p class="msg warning">
        Bitte bestätige zunächst deine E-Mail Adresse um auf diesen Bereich zugreifen zu können.
    </p>

    <?php
    #return;
endif;

$this->subdir = $this->path[count($this->path)-1];

if($this->subdir === null || !in_array($this->subdir, $this->categories)){
    UrlHelper::redirect('/dashboard/meine-favoriten/' . PostManager::URL_PREFIX);
}

?>

<p>
    Das ist dein ganz persönliches Dashboard. Hier findest du deine als Favoriten markierte Beiträge, News-Artikel und Produkte.
</p>

<div class="menu-favourites">
    <div>
        <ul class="nav nav-pills nav-fill">
            <?php foreach ($this->categories as $category): ?>
                <li class="nav-item">
                    <a class="nav-link <?= Helper::ifstr($category == $this->subdir, " active") ?>" aria-current="page" href="<?= '/dashboard/meine-favoriten/' . $category ?>">
                        <?= [
                            PostManager::URL_PREFIX => 'Blog-Artikel',
                            NewsManager::URL_PREFIX => 'News',
                            ProductManager::URL_PREFIX => 'Produkte'
                        ][$category] ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>

<br>

<div id="dashboard-subcontent">
    <?= $this->main_html ?>
</div>
