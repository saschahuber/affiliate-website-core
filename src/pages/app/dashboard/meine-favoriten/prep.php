<?php

$this->title = "Meine Favoriten";

// Mindestens eine Kategorie muss existieren

use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;

$this->categories = [PostManager::URL_PREFIX, NewsManager::URL_PREFIX, ProductManager::URL_PREFIX];

$this->setSubbase(
    isset($this->path[2]) && in_array($this->path[2], $this->categories) ?
        $this->path[2] : $this->categories[0]);
