<?php if (count($this->favoriten) == 0): ?>

    <p class="empty">
        Dieser Bereich ist noch leer. Klicke in der Suche auf das <i class="fas fa-heart"></i>-Symbol
        um interessante News-Artikel als Favoriten zu markieren.
    </p>

<?php else: ?>

    <p>
        Klicke erneut auf das Herz, um einen News-Artikel aus deiner Favoritenliste zu entfernen.
    </p>

    <?=$this->news_manager->displayPosts($this->favoriten, false, 'list', true, true);?>

    <hr>
<?php endif;