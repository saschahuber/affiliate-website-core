<?php if (count($this->favoriten) == 0): ?>

    <p class="empty">
        Dieser Bereich ist noch leer. Klicke in der Suche auf das <i class="fas fa-heart"></i>-Symbol
        um interessante Produkte als Favoriten zu markieren.
    </p>

<?php else: ?>

    <p>
        Klicke erneut auf das Herz, um ein Produkt aus deiner Favoritenliste zu entfernen.
    </p>

    <?=$this->product_manager->displayProducts($this->favoriten, false, 'list', true, true, true);?>

    <hr>
<?php endif;