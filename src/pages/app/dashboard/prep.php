<?php

use saschahuber\affiliatewebsitecore\scaffolding\header\DashboardHeader;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

$this->title = "Dashboard";

if(!$CONFIG->allow_user_login && !$CONFIG->allow_customer_login){
    UrlHelper::redirect("/");
}

function buildGaHints()
{
    $ga_hints = array();

    if (isset($_GET["neu-registriert"]))
        $ga_hints["neu-registriert"] = $_GET["neu-registriert"];

    $str = preg_replace(["@=$@", "@=&@"], ["", "&"], http_build_query($ga_hints));
    return !empty($str) ? "/?" . $str : "";
}

// Nur für eingeloggte Nutzer
if (!AuthHelper::isLoggedIn()) {
    // Zum Login-Formular weiterleiten
    UrlHelper::redirect("/login?loginaction=/" . implode("/", $this->path));
}

// AGB akzeptiert?
if (!AuthHelper::getCurrentUser()->agb_accepted) {
    // need to pass along full query string information, e.g. for e-mail confirmation
    $_SESSION["after_confirming_terms"] = $_SERVER['REQUEST_URI']; # implode("/", $this->path);
    UrlHelper::redirect("/legal/agb" . buildGaHints());
}

// Direkt ein Favorit hinzufügen?
if (!empty($_SESSION["add_fav_after_login"])) {
    $dbquery = $DB->query("SELECT id FROM job
        WHERE status = 'public'
          AND id = " . intval($_SESSION["add_fav_after_login"]));

    unset($_SESSION["add_fav_after_login"]);
    unset($_SESSION["add_fav_company_after_login"]);

    if ($dbquery->num_rows > 0) {
        $job = $dbquery->fetchObject();
        $DB->query("INSERT INTO user__favorite (user_id, job_id) VALUES (" . AuthHelper::getUserManager()->logged_in_user->user_id . ", " . $job->id . ")");

        // Leite zu den Favoriten weiter
        UrlHelper::redirect("/dashboard/nutzer/meine-favoriten/jobs" . buildGaHints());
    }
}

// Direkt eine favorisierte Firma hinzufügen?
if (!empty($_SESSION["add_fav_company_after_login"])) {
    $dbquery = $DB->query("SELECT id FROM company
        WHERE id = " . intval($_SESSION["add_fav_company_after_login"]));

    unset($_SESSION["add_fav_after_login"]);
    unset($_SESSION["add_fav_company_after_login"]);

    if ($dbquery->num_rows > 0) {
        $company = $dbquery->fetchObject();
        $DB->query("INSERT INTO user__favorite_company (user_id, company_id) VALUES (" . AuthHelper::getUserManager()->logged_in_user->user_id . ", " . $company->id . ")");

        // Leite zu den Favoriten weiter
        UrlHelper::redirect("/dashboard/nutzer/meine-favoriten/unternehmen" . buildGaHints());
    }
}

// Direkt weiterleiten?
if (!empty($_SESSION["after_confirming_terms"])) {
    $new_page = $_SESSION["after_confirming_terms"];
    unset($_SESSION["after_confirming_terms"]);
    UrlHelper::redirect($new_page);
}

if (empty($this->path[1])) $this->path[1] = "";
if (empty($this->path[2])) $this->path[2] = "";

$this->customer_data = null; #AuthHelper::getUserManager()->getCustomerData();

/*
// Welches Dashboard soll angezeigt werden?
if (AuthHelper::hasPermission(AffiliateWebsitePermissionService::ACCESS_COMPANY_DASHBOARD)) {
    $this->dashboard_type = !$this->ifpath(1, "nutzer") ? "gewerbekunde" : "nutzer";
}
else {
    $this->dashboard_type = "nutzer";
}
*/

// Ausgewählte Firma
if ($this->dashboard_type == "gewerbekunde") {
    if (isset($_POST['add_company_submit'])) {
        // Firma
        $company_profile_name = $DB->escape($_POST['company_profile_name']);

        // Zahlungsadresse
        $payment_company = $DB->escape($_POST['company_name']);
        $payment_gender = $DB->escape($_POST['company_gender']);
        $payment_firstname = $DB->escape($_POST['company_firstname']);
        $payment_lastname = $DB->escape($_POST['company_lastname']);
        $payment_streetname = $DB->escape($_POST['company_streetname']);
        $payment_streetno = $DB->escape($_POST['company_streetno']);
        $payment_zipcode = intval($_POST['company_zipcode']);
        $payment_city = $DB->escape($_POST['company_city']);
        $payment_country = $DB->escape($_POST['company_country']);

        // Telefonnummer
        $phone_number = $DB->escape($_POST['company_phone']);

        #$company_id = create_company_profile($company_profile_name, $payment_company, $payment_gender,
        #    $payment_firstname, $payment_lastname, $payment_streetname, $payment_streetno, $payment_zipcode,
        #    $payment_city, $payment_country, $phone_number);

        $_SESSION['current_company_id'] = $company_id;

        UrlHelper::redirect();
    }

    $this->company_options = [];
    /*
    foreach ($USER->companies as $company) {
        if ((isset($_GET['change_company']) && $_GET['change_company'] == $company->id) ||
            (empty($this->company) && isset($_SESSION['current_company_id']) && $_SESSION['current_company_id'] == $company->id)) {
            $this->company = $company;
        }

        $this->company_options[$company->id] = $company->name;
    }
    */

    /*
    if (empty($this->company)) {
        $this->company = $USER->companies[0];
    }
    */

    #$_SESSION['current_company_id'] = $this->company->id;
}

// Get subdir name from URL
if (empty($this->path[2]) && !empty($this->path[1]) && !in_array($this->path[1], ["gewerbekunde", "nutzer"])) {
    $try_subdir = $this->path[1];
} else {
    $try_subdir = $this->path[2];
}

// E-Mail Bestätigung
if (isset(AuthHelper::getCurrentUser()->status) && !AuthHelper::getCurrentUser()->status=="confirmed") {
    if (!empty($_GET["key"])) {
        $salutation = $this->dashboard_type == 'gewerbekunde' ? 'Ihre' : 'deine';
        if (get_mailkey(AuthHelper::getUserManager()->logged_in_user->email, 'today') == $_GET["key"] ||
            get_mailkey(AuthHelper::getUserManager()->logged_in_user->email, 'thiscalweek') == $_GET["key"] ||
            get_mailkey(AuthHelper::getUserManager()->logged_in_user->email, 'thismonth') == $_GET["key"]) {
            $DB->query("UPDATE user SET email_confirmed = 1 WHERE id = " . AuthHelper::getUserManager()->logged_in_user->user_id);
            $this->addMsg(ucfirst($salutation) . " E-Mail-Adresse wurde erfolgreich bestätigt.", "success", "top");
        } else {
            $this->addMsg(
                '<a class="btn-color-2 button" onclick="resendConfirmationEmail(this);">E-Mail erneut senden</a>' .
                sprintf('Die Bestätigung %sr E-Mail-Adresse ist fehlgeschlagen. Der Schlüssel war nicht mehr gültig.', "$salutation"),
                'failure', 'top');
        }
    } elseif ($this->dashboard_type == 'nutzer') {
        // Hinweise für Jobsuchende
        if (isset($_GET['neu-registriert'])) {
            // Direkt nach der Registrierung
            $this->addMsg(
                '<a class="btn-color-2 button" onclick="resendConfirmationEmail(this);">E-Mail erneut senden</a>' .
                sprintf('Willkommen auf %s!<br>Um alle Funktionen, die dir bei deiner Jobsuche helfen, nutzen zu können, bestätige bitte als nächstes deine E-Mail Adresse.<br>Dafür haben wir dir gerade eine E-Mail mit dem Titel "🔎 Bestätigung Ihrer E-Mail-Adresse" zugesandt.', $LOCALE->name),
                'notice', 'top');
        } else {
            // Grundsätzlich wenn E-Mail Adresse noch nicht bestätigt
            $this->addMsg(
                '<a class="btn-color-2 button" onclick="resendConfirmationEmail(this);">E-Mail erneut senden</a>' .
                'Deine E-Mail Adresse wurde noch nicht bestätigt.<br>Du kannst daher noch nicht alle Funktionen, die dir bei deiner Jobsuche helfen, nutzen.<br>Prüfe jetzt dein Postfach um deine E-Mail Adresse zu bestätigen', 'notice', 'top');
        }
    } elseif ($this->dashboard_type == 'gewerbekunde') {
        // Hinweise für Arbeitgeber
        if (isset($_GET['neu-registriert'])) {
            // Direkt nach der Registrierung
            $this->addMsg(
                '<a class="btn-color-2 button" onclick="resendConfirmationEmail(this);">E-Mail erneut senden</a>' .
                sprintf('Willkommen auf %s!<br>Um alle Funktionen unseres Portals nutzen zu können, bestätigen Sie bitte Ihre E-Mail Adresse.<br>Sie sollten hierfür eine E-Mail mit dem Titel "🔎 Bestätigung Ihrer E-Mail-Adresse" erhalten haben.', $LOCALE->name), 'notice', 'top');
        } else {
            // Grundsätzlich wenn E-Mail Adresse noch nicht bestätigt
            $this->addMsg(
                '<a class="btn-color-2 button" onclick="resendConfirmationEmail(this);">E-Mail erneut senden</a>' .
                'Deine E-Mail Adresse wurde noch nicht bestätigt.<br>Du kannst daher noch nicht alle Funktionen, die dir bei deiner Jobsuche helfen, nutzen.<br>Prüfe jetzt dein Postfach um deine E-Mail Adresse zu bestätigen', 'notice', 'top');
        }
    }
}

$this->header = new DashboardHeader($this->title);