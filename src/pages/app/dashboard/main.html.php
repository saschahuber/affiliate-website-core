<?php

/*if (!empty($this->show_guide_favorites_popup)): ?>

    <div id="firsttime-prompt">
        <h3>Neu!</h3>
        <p>
            <strong>
                Notiere dir deinen Bewerbungsstatus zu passenden Stellenanzeigen.
            </strong>
        </p>
        <p>
            Markiere deine unter Favoriten gespeicherten Jobs als beworben, indem du auf das Umschlag-Symbol klickst!
        </p>
        <p>
        <center>
            <img src="img/guide/favorite-feature.gif">
        </center>
        </p>
        <p>
            <?=sprintf('Unter dem Menüreiter <a class="icon" href="%s">Bewerbungsstatus</a>
            kannst du deine Bewerbungen verwalten,
            ihnen einen Status (Bewerbung versandt, Vorstellungsgespräch, Einstellungstest und Absage)
            zuteilen und Notizen hinzufügen!', "dashboard/nutzer/meine-favoriten/bewerbungsstatus") ?>
        </p>
    </div>

<?php elseif (!empty($this->show_guide_employee_popup)): ?>

    <div id="firsttime-prompt" data-with-buttons>
        <h4>Willkommen zu deinem neuen Karrierebereich</h4>
        <p class="responsive-content">
            In deinem neuen Karrierebereich findest du <strong>zahlreiche Funktionen</strong>,
            die dich tatkräftig bei deiner <strong>Jobsuche unterstützen!</strong>
            <span>
            Starte direkt durch und <strong>finde deinen neuen Traumjob</strong>
            oder klicke auf „MEHR ERFAHREN“ um dich über die Funktionen deines Karrierebereichs zu informieren!
        </span>
            <span>
            <!-- Mobile -->
        </span>
        </p>
        <p>
        <center class="responsive-content">
            <span><img src="img/guide/dashboard-navigation.gif"></span>
            <span><img src="img/guide/dashboard-navigation-mobile.gif"></span>
        </center>
        </p>
    </div>

<?php endif; */

?>

<div id="dashboard-content">
    <div class="headline">
        <h1>
            <?=$this->title?>
        </h1>
    </div>

    <?php #echo $this->getMsgs("top") ?>

    <?= $this->main_html ?>
</div>
