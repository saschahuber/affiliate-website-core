function adjustContainerHeight() {
    // Feste höhe für Container damit Options-Liste als auch Inhalt die selbe Höhe einnehmen können
    let container = getMain().childNode(true)
    container.style.height = ""
    let height = window.getComputedStyle(container).getPropertyValue("height")
    container.style.height = height

    fixFooter()
}

function showMore(btn, amount) {
    if (!amount) amount = 5
    var stepNode = btn
    for (var i = 0; i < amount; i++) {
        stepNode = stepNode.nextNode()
        if (!stepNode) {
            btn.removeNode()
            break
        }
    }

    if (stepNode)
        btn.parentNode.insertBefore(btn, stepNode)

    adjustContainerHeight()
}

function changeCurrentCompany(select) {
    location.href = location.origin + location.pathname + '?change_company=' + select.value
}

function addCompany() {
    let pb = promptPopup(document.getElementById('add-company-form').innerHTML,
        false, {'ok': 'Abbrechen'}, false, 'add-company-popup')

    // Attach validation events to input fields
    let inputs = pb.getElementsByTagName('input').toArray().concat(
        pb.getElementsByTagName('textarea').toArray())

    for (var i = 0; i < inputs.length; i++) {
        let input = inputs[i]
        attachValidationEvent(input)
    }
}

function requestAddCompanyAbility() {
    promptPopup(document.getElementById('request-add-company-ability-form').innerHTML,
        false, {'ok': 'Schließen'}, false, 'request-add-company-ability-popup')
}


window.addEventListener("load", function () {
    adjustContainerHeight();

    // Push Notifiction für Arbeitgeber
    if (location.search.indexOf('neu-registriert=com') >= 0) {
        showLocalPushNotifictionIfGranted(
            '👨‍💼 Jetzt Eintrag im Smarthome-Verzeichnis erstellen',
            {
                body: 'Machen Sie potenzielle Kunden auf sich aufmerksam!',
                href: '/dashboard/gewerbekunde/firmenprofil'
            })
    }
})

window.addEventListener("resize", adjustContainerHeight)
