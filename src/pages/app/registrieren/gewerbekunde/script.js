function hidePasswordContainer(hide) {
    let passwordInput = document.getElementsByName('reg_password')[0];
    let passwordRepeatInput = document.getElementsByName('reg_password_repeat')[0];

    if (hide) {
        passwordInput.classList.remove('invalid');
        passwordRepeatInput.classList.remove('invalid');

        delete(passwordInput.setAttribute('type', 'hidden'));
        delete(passwordRepeatInput.setAttribute('type', 'hidden'));

        passwordInput.dataset.validate = 'ignore';
        passwordRepeatInput.dataset.validate = 'ignore';
    }
    else{
        delete(passwordInput.setAttribute('type', 'password'));
        delete(passwordRepeatInput.setAttribute('type', 'password'));

        passwordInput.dataset.validate = 'password';
        passwordRepeatInput.dataset.validate = 'identical';

        passwordInput.dataset.lock = 'reg_password_repeat';
        passwordRepeatInput.dataset.master = 'reg_password';

        attachValidationEvent(passwordInput)
        attachValidationEvent(passwordRepeatInput)
    }

    document.getElementById('password-container').style.display = hide ? 'none' : 'block';
}

function setupShowPasswordEye(input){
    input.addEventListener('click', function (eClick) {
        if (mouseOverPwEyeToggle(input, eClick)) {
            if (input.getAttribute('type') == 'password') {
                input.classList.add('visible-password')
                input.setAttribute('type', 'text')
            } else {
                input.classList.remove('visible-password')
                input.setAttribute('type', 'password')
            }
        }
    })

    input.addEventListener('mousemove', function (eMouseMove) {
        input.style.cursor = mouseOverPwEyeToggle(input, eMouseMove) ? 'pointer' : ''
    })
}

window.addEventListener('load', function (e) {
    let passwordInput = document.getElementsByName('reg_password')[0];
    let passwordInputRepeat = document.getElementsByName('reg_password_repeat')[0];

    if(passwordInput && passwordInputRepeat) {
        setupShowPasswordEye(passwordInput);
        setupShowPasswordEye(passwordInputRepeat);
    }
});