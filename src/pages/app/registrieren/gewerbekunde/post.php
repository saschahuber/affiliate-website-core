<?php

use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsiteRoleService;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

$reg_email = $DB->escape(trim(RequestHelper::reqstr('reg_email')));
$reg_password = RequestHelper::reqstr('reg_password');
$reg_generate_password = RequestHelper::reqbool('reg_generate_password');

if (!filter_var($reg_email, FILTER_VALIDATE_EMAIL)) {
    // JS input validation failed?
    $this->addMsg('Die übermittelte E-Mail Adresse war nicht gültig.', 'failure');
    return;
}

if (strstr($_POST['company_name'], 'http')) {
    $this->addMsg('Dieser Firmenname ist nicht erlaubt.', 'failure');
    return;
}

// Sicherstellen dass im Falle einer Neuregistrierung die E-Mail Adresse noch frei ist
if ($this->user_auth_service->isLoggedOut()) {
    $existing_user = $DB->query('SELECT id FROM user WHERE email = "' . $reg_email . '"');

    if ($existing_user->num_rows > 0) {
        LogHelper::error('REGISTER', 409, 'Konflikt bei Registrierung für `' . $reg_email . '`', ERROR_EXIT);
    }
}

// Firma
$company_name = $DB->escape(strip_tags($_POST['company_name']));
$company_alias = UrlHelper::alias($company_name);
if (empty($company_alias)) return;

// Zahlungsadresse
$payment_gender = $DB->escape($_POST['company_gender'], true);
$payment_firstname = $DB->escape($_POST['company_firstname'], true);
$payment_lastname = $DB->escape($_POST['company_lastname'], true);
$payment_streetname = $DB->escape($_POST['company_streetname'], true);
$payment_streetno = $DB->escape($_POST['company_streetno'], true);
$payment_zipcode = intval($_POST['company_zipcode']);
$payment_city = $DB->escape($_POST['company_city'], true);
$payment_country = $DB->escape($_POST['company_country'], true);

$salutation = get_gender_salutation($payment_gender);

// Telefonnummer
$phone_number = $DB->escape($_POST['company_phone']);

$company_id = "TEST";

// Explicit newsletter confirmation?
$newsletter = !$this->newsletter_checkbox || !empty($_POST['newsletter_confirmed']) ? 1 : 0;

// Nutzerkonto
if ($this->account_upgradable) {
    $this->user_auth_service->logged_in_user->agb_accepted = 1;
    $this->user_auth_service->save_logged_in_user();

    $this->user_auth_service->create_customer_information($this->user_auth_service->logged_in_user->id, 'commercial',
        $salutation, $payment_firstname, $payment_lastname, $payment_streetname, $payment_streetno,
        $payment_zipcode, $payment_city, $payment_country, $phone_number, $company_name);

    // E-Mail an uns
    $this->mail_service->sendInternalMail('Gewerbekunden-Registrierung auf '.$CONFIG->app_name,
        'Methode: Ergänzung eines existierenden Benutzerkontos' . PHP_EOL .
        'Benutzer-ID: ' . $this->user_auth_service->logged_in_user->user_id . PHP_EOL .
        'Firmen-ID: ' . $company_id);

    // Speichere Flag in Session um das Nutzer Dashboard weiterhin anzuzeigen
    $this->user_auth_service->setSessvar('interested_in_user_dashboard', true);
} else if ($this->user_auth_service->isLoggedOut()) {
    // Erstelle einen neuen Account mit Personaler-Informationen
    if($reg_password === null || $reg_generate_password !== false) {
        $reg_password = bin2hex(openssl_random_pseudo_bytes(6));
    }
    $reg_date = time();

    $ip_address = $DB->escape(RequestHelper::getClientIp());

    /* START */
    $confirmation_token = md5($reg_email."-confirm_account-".time());

    $registered_user = $this->user_auth_service->createUser($reg_email, null, $reg_password,
        '/login/gewerbekunde', $confirmation_token, null, [AffiliateWebsiteRoleService::COMPANY_USER]);

    $this->user_auth_service->create_customer_information($registered_user->id, 'commercial',
        $salutation, $payment_firstname, $payment_lastname, $payment_streetname, $payment_streetno,
        $payment_zipcode, $payment_city, $payment_country, $phone_number, $company_name);


    #set_initial_values_for_new_user($registered_user->id);

    $this->auth_mail_service->sendConfirmationMail($registered_user, ['customer_user' => true]);

    // Login
    $this->user_auth_service->login_by_id($registered_user->id);

    // Company
    #$DB->query('INSERT INTO user__company (user_id, company_id) VALUES (' . $this->user_auth_service->logged_in_user->user_id . ', ' . $company_id . ')');

    // Login
    $_SESSION['user_id'] = $this->user_auth_service->logged_in_user->user_id;

    // E-Mail an uns
    $this->mail_service->sendInternalMail('Gewerbekunden-Registrierung auf ' . $LOCALE->name,
        'Methode:' . PHP_EOL . 'Erstellung eines neuen Benutzerkontos' . PHP_EOL . PHP_EOL .
        'Benutzer [ID: ' . $this->user_auth_service->logged_in_user->user_id . ']:' . PHP_EOL . $reg_email . PHP_EOL . PHP_EOL .
        'Firma [ID: ' . $company_id . ']:' . PHP_EOL . $company_name);
} else {
    LogHelper::error('REGISTER', 400, 'Versuch, Gewerbekunden-Informationen wiederholt zu hinterlegen.', ERROR_EXIT);
}

// Leite zum Inserieren oder Dashboard weiter
if (isset($_GET['checkout'])) {
    $destination = '/auftrag/erstellen';
}
elseif (isset($_GET['muster'])) {
    $destination = '/dashboard/gewerbekunde/muster';
}
elseif (isset($_GET['bms'])) {
    $destination = '/dashboard/gewerbekunde/ratgeber';
}
elseif (isset($_GET['karriereseite'])) {
    $destination = '/dashboard/gewerbekunde/firmenprofil';
}
elseif (isset($_GET['stellenanzeigengenerator'])) {
    $destination = '/dashboard/gewerbekunde/anzeigen-editor';
}
else {
    $destination = '/dashboard/gewerbekunde/jobauflistungen';
}

UrlHelper::redirect($destination . '/?neu-registriert=com');
