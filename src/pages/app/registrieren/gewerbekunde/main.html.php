<?php

use saschahuber\saastemplatecore\helper\Helper;

?>
<h1>
    <i class="fa fa-sign-in"></i>
    <?= $this->title ?>
</h1>

<?= $this->main_html ?>

<div class="row">
    <div class="col-lg-6">
        <form method="post" action="" class="register">
            <?php if (!$this->account_upgradable): ?>
                <p class="login">
                    Sie haben bereits ein Konto?
                    <a href="login/gewerbekunde<?= Helper::ifstr(isset($_GET["checkout"]), "?loginaction=checkout") ?>">Melden Sie sich
                        hier an</a>.
                </p>
            <?php endif; ?>

            <?php if ($this->account_upgradable): ?>
                <h3>Ergänzen Sie Ihr Benutzerkonto</h3>
                <br>

                <p>
                    <label>E-Mail Adresse</label>
                    <input type="text" value="<?= $this->user_auth_service->logged_in_user->email ?>" disabled>
                    <input type="hidden" name="reg_email" value="<?= $this->user_auth_service->logged_in_user->email ?>">
                </p>
            <?php else: ?>
                <h3>Als Gewerbekunde registrieren</h3>

                <?php #echo $this->get_msgs() ?>
                <br>

                <p>
                    <label>E-Mail Adresse</label>
                    <input type="text" name="reg_email" data-validate="email-create" data-lock="reg_email_confirm"
                           maxlength="250">
                </p>
                <p>
                    <label>E-Mail Adresse wiederholen</label>
                    <input type="text" name="reg_email_confirm" data-validate="identical">
                </p>

                <p>
                    <input type="checkbox" checked name="reg_generate_password" id="cb-use-generated-password"
                           onchange="hidePasswordContainer(this.checked)">
                    <label for="cb-use-generated-password">
                        Ich möchte ein automatisch generiertes Passwort verwenden.
                    </label>
                </p>

                <div id="password-container" style="display: none;">
                    <p>
                        <label>Gewünschtes Passwort</label>
                        <input type="hidden" data-validate="ignore" name="reg_password">
                    </p>

                    <p>
                        <label>Passwort wiederholen</label>
                        <input type="hidden" data-validate="ignore" name="reg_password_repeat">
                    </p>
                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-md-4">
                    <label>Anrede</label>
                    <select name="company_gender">
                        <option value="male">Herr</option>
                        <option value="female">Frau</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Vorname</label>
                    <input type="text" name="company_firstname">
                </div>
                <div class="col-md-4">
                    <label>Nachname</label>
                    <input type="text" name="company_lastname">
                </div>
            </div>
            <p>
                <label>Firmenname</label>
                <input type="text" name="company_name" data-validate="alias">
            </p>
            <p>
                <label>Telefonnummer</label>
                <input type="text" name="company_phone" data-validate="phonenumber">
            </p>
            <div class="row">
                <div class="col-md-9">
                    <p>
                        <label>Straße</label>
                        <input type="text" name="company_streetname">
                    </p>
                </div>
                <div class="col-md-3">
                    <p>
                        <label>Hausnummer</label>
                        <input type="text" name="company_streetno">
                    </p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-4">
                    <p>
                        <label>PLZ</label>
                        <input type="text" name="company_zipcode" maxlength="5" data-validate="zipcode"
                               data-put-valid-response="company_city">
                    </p>
                </div>
                <div class="col-md-8">
                    <p>
                        <label>Stadt</label>
                        <input type="text" name="company_city">
                    </p>
                </div>
            </div>
            <p>
                <label>Land</label>
                <select name="company_country">
                    <?=get_option_html(COUNTRIES, COUNTRIES[0],true)?>
                </select>
            </p>
            <p>
                <input type="checkbox" name="agb_confirmed" id="cb-agb" data-mandatory>
                <label for="cb-agb">Ich akzeptiere die <a href="legal/agb" target="_blank">AGB</a></label>
            </p>
            <?php if ($this->newsletter_checkbox): ?>
                <p>
                    <input type="checkbox" name="newsletter_confirmed" id="cb-newsletter">
                    <label for="cb-newsletter">
                        Ich möchte über besondere Angebote und Aktionen von <?=$CONFIG->app_name?> per
                        E-Mail informiert werden.
                    </label>
                </p>
            <?php elseif (empty($this->user_auth_service->logged_in_user->email_newsletter)): ?>
                <p class="legal-sub-text">
                    Wir möchten Sie über neue Blog-Artikel, Produkttests, News und Blitzangebote,
                    sowie Aktionen für Gewerbekunden per E-Mail informieren.
                    Sie können dem jederzeit kostenfrei widersprechen. Nehmen Sie hierfür einfach per E-Mail oder
                    telefonisch zu Standardtarifen Kontakt zu uns auf.
                </p>
            <?php endif; ?>
            <p>
                <input type="submit" value="Absenden" class="btn btn-primary btn-rouded btn-md" onclick="checkForm(this, event);">
            </p>
        </form>
    </div>
    <div class="col-lg-6">
        <div class="usps">
            <?php if (!empty($this->account_upgradable)): ?>
                <h3>Ihre Vorteile</h3>
            <?php else: ?>
                <h3>Wieso registrieren</h3>
            <?php endif; ?>

            <ul>
                <li>Präsentieren Ihr Unternehmen allen Nutzern von <?=$CONFIG->app_name?></li>
                <li>Schalten Sie Werbeanzeigen auf <?=$CONFIG->app_name?></li>
                <li>Erhalten Sie Statistiken zu Ihren Anzeigen</li>
            </ul>
        </div>
    </div>
</div>
