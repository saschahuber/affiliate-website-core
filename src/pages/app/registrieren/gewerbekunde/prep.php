<?php

use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

if (isset($_GET["checkout"])) {
    $this->subbase = APP_BASE . "/pages/auftrag/breadcrumbs";
}
else if(!$CONFIG->allow_customer_registration){
    UrlHelper::redirect('/registrieren');
}

$this->account_upgradable = false;
if (AuthHelper::isLoggedIn()) {
    if(AuthHelper::hasPermission(AffiliateWebsitePermissionService::ACCESS_COMPANY_DASHBOARD)) {
        if (isset($_GET['checkout'])) {
            // Nutzer ruft das Formular für Personaler-Informationen auf,
            // hat diese aber schon angegeben: Leite zum Inserieren weiter
            UrlHelper::redirect("/auftrag");
        } else
            UrlHelper::redirect("/dashboard/gewerbekunde");
    }

    // Konto kann noch mit Personaler-Informationen ergänzt werden
    $this->account_upgradable = true;
}