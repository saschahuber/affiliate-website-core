<?php

use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsiteRoleService;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;

if (AuthHelper::isLoggedIn()) {
    LogHelper::error("REGISTER", 400, "Versuch, in eingeloggtem Zustand einen Account zu erstellen.", ERROR_EXIT);
}

if (!isset($_POST["register_agb"])) {
    LogHelper::error("REGISTER", 400, "Versuch, Account ohne akzeptierte AGB zu erstellen.", ERROR_EXIT);
}

$reg_email = $DB->escape(trim($_POST["register_email"]));
$reg_password = $DB->escape(trim($_POST["register_password"]));
$reg_date = time();

if (!filter_var($reg_email, FILTER_VALIDATE_EMAIL)) {
    $this->addMsg("Die übermittelte E-Mail Adresse war nicht gültig.", "failure");
    return;
}

if (strlen($reg_password) < 6) {
    $this->addMsg("Das übermittelte Passwort muss mindestens 6 Zeichen enthalten.", "failure");
    return;
}

$destination = "/dashboard/nutzer";

// E-Mail Adresse bereits in DB?
if ($DB->query("SELECT user_id, email FROM user WHERE email = '" . $reg_email . "'")->num_rows > 0) {
    // Missbrauche Registrierungsformular als Login...
    if (empty(AuthHelper::getUserManager()->authenticateByCredentials($reg_email, $reg_password))) {
        #AuthHelper::getUserManager()->setSessvar('interested_in_user_dashboard', true);
        UrlHelper::redirect($destination);
    } else {
        // E-Mail Adresse bereits vergeben...
        $this->addMsg("Diese E-Mail Adresse ist bereits in Verwendung.", "failure");
    }
} else {
    $newsletter = !$this->newsletter_checkbox || !empty($_POST['newsletter_confirmed']) ? 1 : 0;

    $confirmation_token = md5($reg_email."-confirm_account-".time());

    $registered_user = AuthHelper::getUserManager()->createUser($reg_email, null, $reg_password, '/login/nutzer', $confirmation_token, [AffiliateWebsiteRoleService::PRIVATE_USER]);

    #set_initial_values_for_new_user($registered_user->id);

    $newsletter_manager = new NewsletterManager();
    $newsletter_manager->addSubscriber($registered_user->email, false);

    $this->auth_mail_service->sendConfirmationMail($registered_user, ['private_user' => true]);

    // Login
    AuthHelper::getUserManager()->setLoggedInUser($registered_user);

    // Leite zum Dashboard weiter
    UrlHelper::redirect($destination . "/?neu-registriert=user");
}
