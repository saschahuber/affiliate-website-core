<h1>
    <i class="fas fa-user-plus"></i>
    Registrieren
</h1>

<div class="row">
    <div class="col-lg-6">
        <form method="post" action="">
            <h3>
                Als Nutzer Registrieren
            </h3>

            <p>
                <label>E-Mail-Adresse</label>
                <input type="text" name="register_email" data-validate="email">
            </p>
            <p>
                <label>Dein gewünschtes Passwort</label>
                <input type="password" name="register_password" data-validate="password">
            </p>
            <p>
                <input type="checkbox" name="register_agb" id="cb-agb" data-mandatory>
                <label for="cb-agb">Ich akzeptiere die <a href="legal/agb" target="_blank">AGB</a></label>
            </p>
            <?php if ($this->newsletter_checkbox): ?>
                <p>
                    <input type="checkbox" name="newsletter_confirmed" id="cb-newsletter">
                    <label for="cb-newsletter">
                        Ich möchte über neue Blog-Artikel, Produkttests, News und Blitzangebote per
                        E-Mail informiert werden.
                    </label>
                </p>
            <?php else: ?>
                <p class="legal-sub-text">
                    Wir möchten dich über neue Blog-Artikel, Produkttests, News und Blitzangebote per
                    E-Mail informieren.
                    Du kannst dem jederzeit kostenfrei widersprechen. Nimm hierfür einfach per E-Mail oder
                    telefonisch zu Standardtarifen Kontakt zu uns auf.
                </p>
            <?php endif; ?>
            <p>
                <input type="submit" value="Absenden" class="btn btn-primary btn-md btn-rounded" onclick="checkForm(this, event);">
            </p>
            <p class="login">
                Du hast bereits einen Account?
                <a href="login/nutzer">Melde dich hier an.</a>.
            </p>
        </form>
    </div>
    <div class="col-lg-6">
        <div class="usps">
            <h3>Wieso registrieren?</h3>

            <ul>
                <li>Füge Beiträge & News zu deiner Merkliste hinzu</li>
                <li>Markiere Produkte als Favorit und entdecke tolle Blitzangebote</li>
                <li>Erhalte Benachrichtigungen zu allen Neuigkeiten</li>
            </ul>
        </div>
    </div>
</div>