<?php

use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;

(new Text('Jetzt registrieren und alle Vorteile nutzen', 'h1'))->display();

BreakComponent::break();

$user_login_infos = BufferHelper::buffered(function(){
    (new Text('Für Smarthome-Begeisterte', 'h3', ['centered']))->display();

    BreakComponent::break();

    if (!AuthHelper::isLoggedIn()){
        (new FloatContainer([new LinkButton('/registrieren/nutzer', 'Jetzt als Benutzer registrieren')]))->display();
    }
    else {
        (new FloatContainer([new LinkButton('/dashboard/nutzer', 'Zum Benutzer-Dashboard')]))->display();
    }
});

$company_login_infos = BufferHelper::buffered(function(){
    (new Text('Für Gewerbekunden', 'h3', ['centered']))->display();

    BreakComponent::break();

    if (!AuthHelper::isLoggedIn()){
        (new FloatContainer([new LinkButton('/registrieren/gewerbekunde', 'Jetzt als Firmenkunde registrieren')]))->display();
    }
    else {
        (new FloatContainer([new LinkButton('/dashboard/gewerbekunde', 'Jetzt Firmen-Profil anlegen')]))->display();
    }
});

(new Row([
        new Column(new ElevatedCard($user_login_infos), ['col-12', 'col-lg-6']),
        new Column(new ElevatedCard($company_login_infos), ['col-12', 'col-lg-6'])
]))->display();
