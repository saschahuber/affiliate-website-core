<?php

use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

if(!$CONFIG->allow_user_registration && !$CONFIG->allow_customer_registration){
    UrlHelper::redirect('/login');
}

$this->title = "Registrieren";

$this->og_image = $CONFIG->default_og_image;

//Noch keine Gewerbekunden erlauben
$subpages = ["auswahl", "gewerbekunde", "nutzer", "wiederherstellung"];

$subdir = isset($this->path[1]) && in_array($this->path[1], $subpages) ?
    $this->path[1] : $subpages[0];

if (AuthHelper::isLoggedIn()) {
    if ($subdir == "wiederherstellung") UrlHelper::redirect("/dashboard/konto-einstellungen");
    if ($subdir != "gewerbekunde") UrlHelper::redirect("/dashboard");
}

$this->setSubbase($subdir);

// Implicitly set newsletter or let user decide?
$this->newsletter_checkbox = empty(AuthHelper::getCurrentUser()->email_newsletter);
