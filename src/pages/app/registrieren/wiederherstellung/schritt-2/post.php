<?php

use saschahuber\saastemplatecore\helper\UrlHelper;

$dbquery = $DB->query("SELECT * FROM user
    WHERE password_reset_token = '" . $DB->escape($_POST["setpw_key"]) . "'");

$user = $dbquery->fetchObject();

/*
if ($user->service_registered != "email") {
    $this->addMsg("Das Passwort dieses Kontos kann nicht geändert werden,
        da es durch eine Vernüpfung mit " . ucfirst($user->service_registered) . "
        erstellt wurde.", "failure");
    return;
}
*/

if (!$user || $user->password_reset_token != $_POST["setpw_key"])
    $this->addMsg("Der Schlüssel ist nicht mehr gültig.
        Bedenken Sie bitte, dass nur der Schlüssel aus der E-Mail der jeweils letzten
        Passwort-Änderung gültig ist, die Sie angefordert haben.", "failure");
else {
    $new_password = trim($_POST["setpw_new"]);
    $new_password_repeat = trim($_POST["setpw_new_confirm"]);

    if($new_password !== $new_password_repeat){
        $this->addMsg("Die Passwörter stimmen nicht überein.", "failure");
        return;
    }

    $user = $this->user_auth_service->updatePassword($user, $new_password);

    $_SESSION['user_id'] = $user->id;
    UrlHelper::redirect("/dashboard");
}
