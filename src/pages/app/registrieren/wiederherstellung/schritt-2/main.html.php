<form method="post" action="">

    <p>
        Geben Sie nun Ihr gewünschtes Passwort in beide Felder ein.
    </p>

    <p>
        <input type="password" name="setpw_new" data-validate="password">
    </p>
    <p>
        <input type="password" name="setpw_new_confirm" data-validate="identical">
    </p>
    <p>
        <input type="submit" value="Absenden" class="btn-color-1" onclick="checkForm(this, event);">
        <input type="hidden" name="setpw_key" value="<?= $_GET["key"] ?>">
    </p>

</form>
