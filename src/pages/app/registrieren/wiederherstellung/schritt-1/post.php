<?php

use saschahuber\saastemplatecore\helper\UrlHelper;

$dbquery = $DB->query('SELECT * FROM user
      where user.email = "' . $DB->escape($_POST['recover_email']) . '"');

if ($dbquery->num_rows == 0)
    $this->addMsg("Die angegebene E-Mail-Adresse ist nicht bekannt.", "failure");
else {
    $user = $dbquery->fetchObject();

    $this->auth_mail_service->send_password_reset_mail($user);

    UrlHelper::redirect("/" . $this->alias . "/wiederherstellung/gesendet");
}
