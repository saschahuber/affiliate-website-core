<form method="post" action="registrieren/wiederherstellung">
    <p>
        Wenn Sie ein neues Passwort setzen möchten, geben Sie zunächst die E-Mail-Adresse an, mit der Sie sich
        registriert haben.
    </p>
    <div class="row">
        <div class="col-lg-8">
            <input type="text" name="recover_email" data-validate="email">
        </div>
        <div class="col-lg-4">
            <input type="submit" class="btn btn-primary btn-md btn-block btn-rounded"
                   value="Absenden" onclick="checkForm(this, event);">
        </div>
    </div>
</form>
