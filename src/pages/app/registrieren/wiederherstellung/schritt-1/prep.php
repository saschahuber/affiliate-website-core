<?php

if ($this->ifpath(2, "gesendet"))
    $this->addMsg("Vielen Dank. Wir haben Ihnen eine E-Mail mit weiteren Instruktionen zugesandt.<br>" .
        "Falls Sie keine E-Mail erhalten haben, überprüfen Sie bitte auch den Spam Ordner Ihres Postfachs.", "success");
