<div class="reset-container">
    <h1>
        <i class="fas fa-unlock-alt"></i>
        <?= $this->title ?>
    </h1>

    <br>

    <?php
    echo $this->get_msgs();
    echo $this->main_html;
    ?>
</div>