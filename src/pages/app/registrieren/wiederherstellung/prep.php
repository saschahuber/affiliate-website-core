<?php

use saschahuber\saastemplatecore\helper\UrlHelper;

$this->title = "Passwort zurücksetzen";

if ($this->user_auth_service->isLoggedIn()) {
    // Nutzer ruft das Formular zum zurücksetzen des Passworts auf, ist aber bereits eingelogt:
    // Das Passwort kann im Dashboard geändert werden
    UrlHelper::redirect("/dashboard/konto-einstellungen");
}

if (!empty($_GET["key"])) {
    $this->user = $DB->query('SELECT * FROM user 
        where password_reset_token = "'.$DB->escape($_GET['key']).'"')->fetchObject();
    if ($this->user)
        $this->setSubbase("schritt-2");
    else {
        $this->addMsg("Dieser Schlüssel ist nicht mehr gültig.", "failure");
        $this->setSubbase("schritt-1");
    }
} else
    $this->setSubbase("schritt-1");
