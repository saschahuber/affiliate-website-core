<?php

use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

$this->tried_registration = false;

$this->registration_success = false;

$parity = false;
$terms_accepted = false;

if(isset($_POST['action'])){
    switch($_POST['action']){
        case "register":
            if(isset($_POST['parity'])
                && $_POST['email']
                && $_POST['terms']) {
                $parity = $_POST['parity']==="";
                $terms_accepted = $_POST['terms']==="1";

                $topics = ArrayHelper::getArrayValue($_POST, 'name', null);

                $this->tried_registration = true;
            }

            $error = false;

            if($parity && $terms_accepted) {
                $mail = $_POST['email'];
                $name = ArrayHelper::getArrayValue($_POST, 'name', null);
                $days_between_newsletter = intval(ArrayHelper::getArrayValue($_POST, 'days_between', DEFAULT_DAYS_BETWEEN_NEWSLETTER));

                $all_topics = array_keys(ALL_NEWSLETTER_TOPICS);

                if(RequestHelper::reqbool('all_topics')){
                    $subscriber_topics = $all_topics;
                }
                else{
                    foreach($all_topics as $topic){
                        if(RequestHelper::reqbool($topic)){
                            $subscriber_topics[] = $topic;
                        }
                    }
                }

                //Wenn addSubscriber false zurückgibt => Fehler
                $error = !$this->newsletter_manager->addSubscriber($mail, true, $name, $subscriber_topics, $days_between_newsletter);
            }

            if($error) {
                UrlHelper::redirect("/newsletter?error");
            }
            else {
                UrlHelper::redirect("/newsletter?success");
            }
            break;
        case "unsubscribe":
            if(isset($_GET['unsubscribe_key']) && $this->newsletter_manager->unsubscribe($_GET['unsubscribe_key'])) {
                UrlHelper::redirect("/newsletter/abmeldung?error");
            }
            else {
                UrlHelper::redirect("/newsletter/abmeldung?success");
            }
            break;
        case "settings":
            $all_topics = array_keys(ALL_NEWSLETTER_TOPICS);

            if(RequestHelper::reqbool('all_topics')){
                $subscriber_topics = $all_topics;
            }
            else{
                foreach($all_topics as $topic){
                    if(RequestHelper::reqbool($topic)){
                        $subscriber_topics[] = $topic;
                    }
                }
            }

            if(isset($_GET['settings_key']) && $this->newsletter_manager->updateSettings($_GET['settings_key'], $subscriber_topics)) {
                UrlHelper::redirect("/newsletter/einstellungen?success");
            }
            else {
                UrlHelper::redirect("/newsletter/einstellungen?error");
            }
            break;
    }
}