<?php

use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;

$this->newsletter_manager = new NewsletterManager();

if(count($this->path) == 1){
    UrlHelper::redirect("newsletter/anmeldung" . (count($_GET)?'?'.http_build_query($_GET):''), 302);
}
else if(count($this->path) == 2){
    switch($this->path[1]){
        case "anmeldung":
            $this->setSubbase("anmeldung");
            break;
        case "ansehen":
            $this->setSubbase("ansehen");
            break;
        case "abmeldung":
            $this->setSubbase("abmeldung");
            break;
        case "einstellungen":
            $this->setSubbase("einstellungen");
            break;
        default:
            LogHelper::error("HTTP", 404, "[".implode('/', $this->path)."] Newsletter-Pfad existiert nicht...", ERROR_EXIT);
            break;
    }
}
else{
    LogHelper::error("HTTP", 404, "[".implode('/', $this->path)."] Newsletter-Pfad existiert nicht...", ERROR_EXIT);
}