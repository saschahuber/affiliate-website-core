<?php

use saschahuber\saastemplatecore\component\scaffolding\header\LoginHeader;

if(!$this->header) {
    $this->header = new LoginHeader(null, $this->login_type);
}