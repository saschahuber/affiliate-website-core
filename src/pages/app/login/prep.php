<?php

use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;

if(!$CONFIG->allow_user_login && !$CONFIG->allow_customer_login){
    UrlHelper::redirect("/");
}

if (AuthHelper::isLoggedIn()) {
    UrlHelper::redirect("/dashboard");
}

$this->title = "Login";

if (isset($this->path[1]) && in_array($this->path[1], ["gewerbekunde", "nutzer"])) {
    $this->login_type = [
        "gewerbekunde" => "gewerbekunde",
        "nutzer" => "nutzer"
    ][$this->path[1]];
}
else{
    $this->login_type = "unspecified";
}
