<?php

use saschahuber\affiliatewebsitecore\api\AmazonPAPIWrapper;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\Database;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\service\MailService;

define('APP_BASE', $argv[1]);
require_once(APP_BASE . '/../commons/init.php');
define('APP_TYPE', LogHelper::APP_TYPE_CRONJOB);
$DB = new Database();

$amazon_api = new AmazonPAPIWrapper();

$product_manager = AffiliateInterfacesHelper::getProductManager();
$image_manager = new ImageManager();

$asins = array();
$products_to_update = [];

$dbquery = $DB->query('SELECT *
FROM product__link
where external_id is not null and HOUR(TIMEDIFF(NOW(), last_update))>6
    and product__shop_id = 1
order by last_update ASC
LIMIT 10');

while($product_link = $dbquery->fetchObject()){
    $asin = $product_link->external_id;
    $asins[] = $asin;
    $products_to_update[$asin] = $product_link;
}

echo "Updating asins: ".json_encode($asins).PHP_EOL;

if(count($asins) === 0){
    die("DONE!");
}

$items = $amazon_api->getByAsins($asins);
$item_updates = [];
if($items != null) {
    foreach($items as $item){
        $item_updates[$item['asin']] = $item;
    }
}

$not_available_products = [];
$products_with_import_errors = [];

foreach ($asins as $asin) {
    echo "Updating product (asin: $asin)" . PHP_EOL;

    $product_link = $products_to_update[$asin];

    $product_to_update = $DB->query("select * from product where product_id = ".intval($product_link->product_id))->fetchObject();

    $item_data = null;
    if(array_key_exists($asin, $item_updates)){
        $item_data = $item_updates[$asin];
    }

    #die(json_encode($item_data, JSON_PRETTY_PRINT));

    if($item_data !== null) {
        //Preis aktualisieren
        $product_link->price = $item_data['price'];
        $product_link->reduced_price = $item_data['reduced_price'];

        //Produkt nicht verfügbar?
        $is_available = true;
        if($product_link->price === null || $item_data['is_available'] === false){
            $is_available = false;
        }

        $product_link->is_available = $is_available;

        if($product_link->is_available === false) {
            $not_available_products[] = $product_link;
        }

        $product_manager->setMeta($product_to_update->product_id, "product_info", serialize($item_data['info']));
        $other_images = $item_data['other_images'];
        for($i = 0; $i < count($other_images); $i++){
            $image = $other_images[$i];
            $product_manager->setMeta($product_to_update->product_id, "product_image_".($i+1), $image);
        }

        $product_to_update->is_prime = $item_data['is_prime'];
        
        $product_to_update->amazon_sales_rank = $item_data['sales_rank'];

        $old_primary_image = $product_manager->getMeta($product_to_update->product_id, "product_primary_image");
        $primary_image = $item_data['primary_image'];
        $product_manager->setMeta($product_to_update->product_id, "product_primary_image", $primary_image);
    }
    else{
        echo "Could not update asin: $asin".PHP_EOL;

        if($product_link->is_available === false) {
            LogHelper::logToMinuteLog("Amazon Import", "Could not update Amazon Product (ASIN: $asin)", LogHelper::LOG_LEVEL_WARNING);

            # Produkt auf "Nicht verfügbar" setzen
            $product_link->is_available = false;

            # Per Mail darüber informieren
            $products_with_import_errors[] = $product_to_update;
        }
    }

    #Zeitstempel aktualisieren
    $product_link->last_update = date('Y-m-d H:i:s', time());
    #echo "Updated object: " . json_encode($product_to_update) . PHP_EOL;
    $DB->updateFromObject("product", $product_to_update);
    $DB->updateFromObject("product__link", $product_link);
    $product_manager->addToPriceHistory($product_to_update, $product_link);
}

global $CONFIG;
$mail_service = new MailService();

if(count($products_with_import_errors)) {
    $content_items = [];
    foreach ($products_with_import_errors as $product) {
        $content_items[] = '<a href="' . $CONFIG->admin_domain . '/dashboard/produkte/bearbeiten/' . $product->id . '">' . $product->title . ' ID: (' . $product->id . ')</a>';
    }

    foreach ($CONFIG->internal_email_addresses as $recipient) {
        $mail_service->sendRawEmail($recipient, [], 'Amazon-Produkte mit Import-Fehler', implode('<br>', $content_items));
    }
}

echo "DONE!".PHP_EOL;
