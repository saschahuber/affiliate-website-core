<?php

use saschahuber\saastemplatecore\Database;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\LogHelper;

define('APP_BASE', $argv[1]);
require_once(APP_BASE . '/../commons/init.php');
define('APP_TYPE', LogHelper::APP_TYPE_CRONJOB);
$DB = new Database();

$paths_to_clean = [
    APP_BASE . '/front/data',
    APP_BASE . '/front/img',
    APP_BASE . '/front/produkt-img'
];

foreach($paths_to_clean as $path){
    $elements = FileHelper::removeDirectory($path, 6 * ONE_HOUR);

    echo "Deleted $elements items from $path" . PHP_EOL;
}