<?php

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\service\NewsImporterService;
use saschahuber\saastemplatecore\Database;
use saschahuber\saastemplatecore\helper\LogHelper;

define('APP_BASE', $argv[1]);
require_once(APP_BASE . '/../commons/init.php');
define('APP_TYPE', LogHelper::APP_TYPE_CRONJOB);
$DB = new Database();

$image_manager = new ImageManager();
$news_manager = new NewsManager();

$news_importer_service = new NewsImporterService();

$items = $news_importer_service->refreshNewsFeedItems();

foreach($items as $item){
    $imported_feed_item = new stdClass();
    $imported_feed_item->title = $item->title;
    $imported_feed_item->link = $item->post_url;
    $imported_feed_item->guid = $item->external_identifier;
    $imported_feed_item->date = $item->news_date;
    $imported_feed_item->content = $item->content;
    $imported_feed_item->news__feed_import_id = $item->imported_feed_id;

    if(isset($item->category)) {
        $imported_feed_item->category = json_encode($item->category);
    }

    $DB->insertFromObject('news__feed_import_item', $imported_feed_item);
}