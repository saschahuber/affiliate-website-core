<?php

use saschahuber\saastemplatecore\helper\LogHelper;

define('APP_BASE', $argv[1]);
require_once(APP_BASE . '/../commons/init.php');
define('APP_TYPE', LogHelper::APP_TYPE_CRONJOB);

echo "Temporarily deactivated..." . PHP_EOL;

/*
$DB = new Database();

$image_manager = new ImageManager();
$news_manager = new NewsManager();

$news_importer_service = new NewsImporterService();

$search_manager = new SearchManager();

$imported_item_ids = $news_importer_service->importAll();

$item_infos = [];

foreach($imported_item_ids as $item_id){
    $item = $news_manager->getById($item_id);
    echo "Updating search index for news (#{$item->id}): {$item->title}".PHP_EOL;
    $searchable_item_type = "news";
    $relevance_modifier = 1.0;
    $searchable_item_id = $item->id;
    $permalink = $news_manager->generatePermalink($item);
    $title = $item->title;
    $content_primary = $item->content;
    $content_secondary = null;
    $meta = null;
    $thumbnail_url = $image_manager->getAttachmentUrl($item->attachment_id);
    $search_manager->updateSearchIndex($searchable_item_id, $searchable_item_type, $permalink, $title,
        $content_primary, $content_secondary, $meta, $thumbnail_url, $relevance_modifier);

    $item_infos[] = $title;
}

if(count($item_infos) > 0) {
    $mail_service = new \saschahuber\saastemplatecore\service\MailService();

    $mail_body = "Folgende Artikel wurden importiert: <br><br>"
        . implode('<br><br>', $item_infos);

    $mail_service->sendEmail($CONFIG->general_email_address, null, count($item_infos) . ' Neue News-Artikel importiert', $mail_body);
}
*/