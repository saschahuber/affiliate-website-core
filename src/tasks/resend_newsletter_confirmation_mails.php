<?php

use saschahuber\saastemplatecore\Database;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;

define('APP_BASE', $argv[1]);
require_once(APP_BASE . '/../commons/init.php');
define('APP_TYPE', LogHelper::APP_TYPE_CRONJOB);
$DB = new Database();

$newsletter_manager = new NewsletterManager();
$newsletter_service = new NewsletterService();

$unconfirmed_subscribers = $newsletter_manager->getConfirmedSubscribers(false);

foreach ($unconfirmed_subscribers as $subscriber){
    $newsletter_service->sendConfirmationMail($subscriber);
}