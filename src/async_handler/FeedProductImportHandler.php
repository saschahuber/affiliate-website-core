<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\service\ProductFeedImportService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\builder\ui\RowBuilder;
use saschahuber\saastemplatecore\component\async\AsyncSearchComponent;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class FeedProductImportHandler extends AdminAsyncHandler
{
    const MODE_SEARCH = 'MODE_SEARCH';
    const MODE_IMPORT = 'MODE_IMPORT';

    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle()
    {
        AuthHelper::checkAdminPanelUser();

        $mode = RequestHelper::reqstr('mode');
        $ean = RequestHelper::reqstr('ean');
        $asin = RequestHelper::reqstr('asin');
        $product_id = RequestHelper::reqstr('product_id');
        $feed_id = RequestHelper::reqint('feed_id');
        $all_data_raw = RequestHelper::reqstr('data');
        $external_id = RequestHelper::reqint('external_id');

        if ($mode == self::MODE_IMPORT && ($ean || $product_id)) {
            $product_feed_import_service = new ProductFeedImportService();
            $feed_manager = new ProductFeedManager();

            $feed = $feed_manager->getById($feed_id);
            $item_data = $product_feed_import_service->getProductDataFromExternalId($feed, $external_id);

            $product = $product_feed_import_service->updateProductFromFeed($ean, $feed_id, $item_data, $product_id);

            echo BufferHelper::buffered(function () use ($product) {
                echo "Produkt {$product->title} importiert";

                (new RowBuilder())
                    ->withColumn(new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => ProductManager::TYPE_PRODUCT, 'id' => $product->id]))
                    ->withColumn((new LinkButton("/dashboard/produkte/bearbeiten/" . $product->id, 'Bearbeiten', 'fas fa-pen', false, true)))
                    ->build()->display();
            });
        } else if ($mode == self::MODE_SEARCH) {
            (new AsyncSearchComponent('ProductToImportSearchHandler', ['asin' => $asin, 'feed_id' => $feed_id, 'ean' => $ean, 'all_data' => $all_data_raw], 'Produkt zum Verknüpfen suchen'))->display();
        } else {
            echo "Konnte Produkt mit EAN $ean nicht importieren  ...";
        }
    }
}