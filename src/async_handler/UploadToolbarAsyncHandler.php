<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\component\ToolbarButton;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\affiliatewebsitecore\manager\UploadManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\async_handler\AsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\JsButton;


class UploadToolbarAsyncHandler extends AdminAsyncHandler {
    public function __construct($params){
        parent::__construct($params);
        $this->upload_manager = new UploadManager();
        $this->layout_manager = new LayoutManager();
    }

    public function handle(){
        echo $this->getHtml()
            . $this->getStylesheet()
            . $this->getScript();
    }

    private function displayUploadSelectionHtml(){
        global $CONFIG;

        $upload_contents = array();
        foreach($this->upload_manager->getUploads() as $upload){
            ob_start();

            $upload_data = array(
                'id' => $upload->id,
                'src' => $CONFIG->website_domain . '/data/media' . $upload->file_path,
                'title' => $upload->title,
                'alttext' => $upload->alt_text,
            );

            ?>
            <div class="media-grid-item">
                <a onclick="selectUpload('<?=base64_encode(json_encode($upload_data))?>')">
                    <?=ShortcodeHelper::doShortcode('[upload upload_id="'.$upload->id.'"]')?>
                </a>
            </div>
            <?php
            $upload_content = ob_get_contents();
            ob_end_clean();
            $upload_contents[] = $upload_content;
        }

        echo $this->layout_manager->grid($upload_contents, 6, "sm");
    }

    private function getUploadConfigurationHtml(){
        ob_start();
        ?>

        <?php
        (new JsButton("Zurück zur Bilderauswahl", "backToUploadSelection()"))->display();
        ?>
        <div>
            <div class="row">
                <div class="col-md-6">
                    <?php

                    (new TextInput("upload_title", "Titel"))->display();

                    (new TextArea("upload_alt_text", "Alt-Text"))->display();

                    (new TextArea("upload_caption", "Caption"))->display();

                    (new NumberInput("upload_width", "Breite", null, null, 0, null, 1))->display();

                    (new NumberInput("upload_height", "Höhe", null, null, 0, null, 1))->display();

                    (new TextInput("upload_align", "Align (left, right, center)"))->display();

                    ?>

                    <div class="shortcode_code row">
                        <div class="col-lg-3">
                            <?php
                            (new JsButton("Kopieren", "copyUploadShortcodeToClipboard()"))->display();
                            ?>
                        </div>
                        <div class="col-lg-9">
                            <div id="upload_shortcode"></div>
                        </div>
                    </div>

                    <?php
                    (new JsButton("Code generieren", "showGeneratedUploadShortcode()"))->display();
                    ?>
                </div>
                <div class="col-md-6">
                    <iframe id="configure-upload-container-preview"></iframe>
                </div>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getHtml(){
        ob_start();

        (new ToolbarButton([], "Zurück zur Toolbar"))->display();

        (new BreakComponent())->display();

        ?>
        <div>
            <div id="select-upload-container" class="selected">
                <?php $this->displayUploadSelectionHtml(); ?>
            </div>
            <div id="configure-upload-container">
                <?=$this->getUploadConfigurationHtml()?>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getScript(){
        ob_start();
        ?>
        <script>
            <?=file_get_contents(PUBLIC_DIR . '/js/upload_toolbar.js')?>
            resetUploadToolbar();
        </script>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getStylesheet(){
        ob_start();
        ?>
        <style>
            <?=file_get_contents(PUBLIC_DIR . '/css/upload_toolbar.css')?>
        </style>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}