<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use DateTime;
use saschahuber\affiliatewebsitecore\service\ProductLinkClicksLastIntervalService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\chart\ChartValue;
use saschahuber\saastemplatecore\component\chart\Dataset;
use saschahuber\saastemplatecore\component\chart\StackedBarChart;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\ColorHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class ProductLinkClicksLastIntervalAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);

        $this->product_click_entries_last_interval_service = new ProductLinkClicksLastIntervalService();
    }

    public function handle()
    {
        echo $this->getChart();
    }

    private function getChart()
    {
        $interval = ArrayHelper::getArrayValue($this->params, 'interval', DatabaseTimeSeriesHelper::INTERVAL_MINUTE);
        $number = intval(ArrayHelper::getArrayValue($this->params, 'number', 60));
        $product_id = ArrayHelper::getArrayValue($this->params, 'product_id', false);

        return $this->getChartGroupedByColumn($interval, $number, $product_id);
    }

    private function getChartGroupedByColumn($interval, $number, $product_id = null)
    {
        $data_value_groups = [];

        $datasets = [];

        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $date_format = "Y-m-d H:i";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $date_format = "Y-m-d H";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $date_format = "Y-m-d";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $date_format = "Y-m";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $date_format = "Y";
                break;
        }

        $colors = [];

        $store_types = $this->product_click_entries_last_interval_service->getStoreTypes();

        $time_series = $this->product_click_entries_last_interval_service->getTimeSeries($interval, $number);

        foreach ($store_types as $store_type) {
            $log_item_map = $this->product_click_entries_last_interval_service->getLastIntervalLogEntries($interval, $number, $product_id, $store_type);

            foreach ($time_series as $key => $value) {
                $time_series[$key] = (array_key_exists($key, $log_item_map) ? $log_item_map[$key] : 0);
            }

            $data_value_groups[$store_type] = $time_series;
        }

        foreach ($data_value_groups as $dataset_label => $items) {
            $values = [];
            foreach ($items as $time => $anzahl) {
                $time = DateTime::createFromFormat($date_format, $time)->format('Y-m-d H:i');
                $format_string = 'H:i';
                if ($interval == DatabaseTimeSeriesHelper::INTERVAL_DAY) {
                    $format_string = 'D, d.m.';
                }
                $values[] = new ChartValue(DateHelper::format($time, $format_string), $anzahl);
            }

            $color = ColorHelper::convertTextToColor($dataset_label);
            if (array_key_exists($dataset_label, $colors)) {
                $color = $colors[$dataset_label];
            }

            $datasets[] = new Dataset($dataset_label, $values, $color);
        }

        $height = intval(ArrayHelper::getArrayValue($this->params, 'height', null));
        $chart = new StackedBarChart($datasets, 0.00, 300, null, $height);

        ob_start();
        echo '<h3 class="centered">Affiliate-Klicks der letzten Zeit</h3>';

        if (count($datasets) > 0) {
            $chart->display();
        } else {
            echo '<p class="no-margin centered">Noch keine Daten</p>';
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}