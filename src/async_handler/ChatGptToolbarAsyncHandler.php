<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\component\form\ChatGenerationForm;
use saschahuber\affiliatewebsitecore\component\ToolbarButton;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;

class ChatGptToolbarAsyncHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
        $this->image_manager = new ImageManager();
        $this->layout_manager = new LayoutManager();
    }

    public function handle(){
        (new ToolbarButton([], "Zurück zur Toolbar"))->display();

        (new BreakComponent())->display();

        (new ChatGenerationForm())->display();
    }
}