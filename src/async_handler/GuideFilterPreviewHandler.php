<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\persistence\UserRepository;

class GuideFilterPreviewHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle()
    {
        AuthHelper::checkAdminPanelUser();

        $guide_filter_id = ArrayHelper::getArrayValue($this->params, 'id');

        echo ShortcodeHelper::doShortcode('[guide_filter id="'.$guide_filter_id.'"]');
    }
}