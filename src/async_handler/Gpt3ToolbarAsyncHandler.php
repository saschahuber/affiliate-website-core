<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\component\ToolbarButton;
use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\async_handler\AsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LoadingComponent;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\BufferHelper;

class Gpt3ToolbarAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
        $this->layout_manager = new LayoutManager();
    }

    public function handle()
    {
        echo $this->getHtml()
            #. $this->getStylesheet()
            . $this->getScript();
    }

    public function getHtml()
    {
        ob_start();

        (new ToolbarButton([], "Zurück zur Toolbar"))->display();

        (new BreakComponent())->display();

        $textarea = new TextArea("prompt", "Prompt");
        $loading_animation = new LoadingComponent(false);
        $result_area_id = uniqid();
        $result_area = BufferHelper::buffered(function () use ($result_area_id) {
            ?>
            <div id="<?= $result_area_id ?>"></div>
            <?php
        });
        $button = new JsButton('<i class="fas fa-magic"></i> Schreiben', "createGpt3ResultPrompt('" . $textarea->getId() . "', '" . $result_area_id . "', '" . $loading_animation->getId() . "')", ['full-width']);

        (new GridContainer([
            new Row([
                new Column([$textarea, $button, $result_area, $loading_animation])
            ])
        ], 1))->display();

        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getScript()
    {
        ob_start();
        ?>
        <script>
            <?=file_get_contents(PUBLIC_DIR . '/js/gpt3_toolbar.js')?>
        </script>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getStylesheet()
    {
        ob_start();
        ?>
        <style>
            <?=file_get_contents(PUBLIC_DIR . '/css/gpt3_toolbar.css')?>
        </style>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}