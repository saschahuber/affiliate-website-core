<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\builder\ui\FormBuilder;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;

class NewsPostSearchAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle()
    {
        AuthHelper::checkAdminPanelUser();
        echo $this->displayContent();
    }

    private function displayContent()
    {
        $news_manager = new NewsManager();

        $keyword = ArrayHelper::getArrayValue($this->params, 'keyword', null);
        $status = ArrayHelper::getArrayValue($this->params, 'status', null);
        $taxonomies = ArrayHelper::getArrayValue($this->params, 'taxonomies', []);

        $taxonomy_options = [];
        foreach ($news_manager->getTaxonomies() as $taxonomy) {
            $taxonomy_options[$taxonomy->id] = $taxonomy->title;
        }

        $form_builder = new FormBuilder();
        $form_builder->withAction('/dashboard/news')
            ->withMethod(Form::METHOD_GET)
            ->withInputRow(new TextInput('s', 'Suchbegriff', null, $keyword))
            ->withInputRow(new Select('status', 'Status', array_merge([null => '-- Kein Status gewählt --'], Manager::getStatusTypes()), $status))
            ->withInputRow(new Select('taxonomies', 'Kategorien', $taxonomy_options, $taxonomies, true))
            ->withSubmitButton('Suchen');

        return $form_builder->build()->getContent();
    }
}