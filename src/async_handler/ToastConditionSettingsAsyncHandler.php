<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\enum\ToastConditionType;
use saschahuber\affiliatewebsitecore\service\ToastConditionService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class ToastConditionSettingsAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle(){
        $condition_type = ArrayHelper::getArrayValue($this->params, 'condition_type');
        $condition_id = ArrayHelper::getArrayValue($this->params, 'condition_id');

        $toast_condition_service = new ToastConditionService();

        $toast_condition = $toast_condition_service->getById($condition_id);

        $condition_data = [];
        if($toast_condition){
            $condition_data = json_decode($toast_condition->condition_data, true);
        }

        switch($condition_type){
            case ToastConditionType::NEW_USER:
                (new NumberInput('condition__min_views', 'Minimale bisherige Aufrufe (Toast wird angezeigt, wenn der Nutzer bereits x Seitenaufrufe getätigt hat)', null, ArrayHelper::getArrayValue($condition_data, 'min_views', null), 0, null, 1))->display();
                (new NumberInput('condition__max_views', 'Maximale bisherige Aufrufe (Toast wird angezeigt, bis der Nutzer maximal x Seitenaufrufe getätigt hat)', null, ArrayHelper::getArrayValue($condition_data, 'max_views', null), 0, null, 1))->display();
                break;
            case ToastConditionType::RECURRING_USER:
                (new NumberInput('condition__min_days_gone', 'Minimale Tage abwesend (Toast wird angezeigt, wenn der Nutzer mindestens x Tage nichtmehr auf der Website war)', null, ArrayHelper::getArrayValue($condition_data, 'min_days_gone', null), 0, null, 1))->display();
                (new NumberInput('condition__max_days_gone', 'Maximale Tage abwesend (Toast wird angezeigt, wenn der Nutzer höchstens x Tage nichtmehr auf der Website war)', null, ArrayHelper::getArrayValue($condition_data, 'max_days_gone', null), 0, null, 1))->display();
                break;
            case ToastConditionType::URL_PATH:
                (new TextInput('condition__url_path', 'Beginn der URL (Domain kann weggelassen werden)', 'Beginn der URL (Domain kann weggelassen werden)', ArrayHelper::getArrayValue($condition_data, 'url_path', null)))->display();
                break;
        }
    }
}