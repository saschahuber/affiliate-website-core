<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\api\AmazonPAPIWrapper;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\helper\AuthHelper;

class AmazonProductSearchHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
    }

    public function handle(){
        AuthHelper::checkAdminPanelUser();
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $amazon_api = new AmazonPAPIWrapper();
        $results = $amazon_api->search($this->params['value']);
        echo $amazon_api->displayResults($results, $product_manager->getImportedAsins());
    }
}