<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\NewsFeedImportItemManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\form\AsyncForm;
use saschahuber\saastemplatecore\component\form\input\CkEditorTextArea;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;

class NewsItemImportHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
    }

    public function handle(){
        AuthHelper::checkAdminPanelUser();

        $news_feed_item_id = RequestHelper::reqint("feed_item_id", null);
        $model = RequestHelper::reqstr("model", ChatGenerationTemplate::MODEL_CHAT_GPT);
        $tokens = RequestHelper::reqint("tokens", 4096);

        $news_feed_import_item_manager = new NewsFeedImportItemManager();
        $news_feed_item = $news_feed_import_item_manager->getById($news_feed_item_id);

        echo BufferHelper::buffered(function () use ($news_feed_item, $model, $tokens) {
            ob_start();
            ?>
            <div id="news-item-importer-loading-animation" style="text-align: center;">
                <p style="font-size: 22px; margin: 0;">
                    <i class="fas fa-spinner fa-spin"></i> Generiere News-Artikel...
                </p>

                <div id="news-item-importer-title-loading-animation">
                    <p style="font-size: 16px; margin: 0;">
                        <i class="fas fa-spinner fa-spin"></i> Generiere Titel...
                    </p>
                </div>

                <div id="news-item-importer-content-loading-animation">
                    <p style="font-size: 16px; margin: 0;">
                        <i class="fas fa-spinner fa-spin"></i> Generiere Inhalt...
                    </p>
                </div>

                <div id="news-item-importer-meta-title-loading-animation">
                    <p style="font-size: 16px; margin: 0;">
                        <i class="fas fa-spinner fa-spin"></i> Generiere Meta-Titel...
                    </p>
                </div>

                <div id="news-item-importer-meta-description-loading-animation">
                    <p style="font-size: 16px; margin: 0;">
                        <i class="fas fa-spinner fa-spin"></i> Generiere Meta-Beschreibung...
                    </p>
                </div>

                <br>
                <br>
            </div>

            <?php

            $title_input = TextInput::required("title", 'Titel', 'Titel des Beitrags', null);
            $title_input->display();

            BreakComponent::break();

            $ckeditor_area = new CkEditorTextArea("content", "Inhalt", null, null, 300);
            $ckeditor_area->display();

            BreakComponent::break();

            $meta_title_input = new TextArea("meta_title", 'Meta Titel', '(Lasse leer für Titel)', null, 100, null, null, 'meta_title_input');
            $meta_title_input->display();

            $meta_description_input = new TextArea("meta_description", 'Meta Beschreibung', '(Lasse leer für Beschreibung)', null, 100, null, null, 'meta_description_input');
            $meta_description_input->display();

            (new SubmitButton('Speichern'))->display();

            $input_content = ob_get_clean();

            (new AsyncForm($input_content, 'news/post/save', 'onNewsImportSuccess', 'onNewsImportError'))->display();

            ?>

            <script>
                if(typeof alreadySubmittingAsyncForm === 'undefined') {
                    <?php
                    foreach (AsyncForm::getJsFiles() as $js_file) {
                        echo file_get_contents($js_file);
                    }
                    ?>
                }

                function onNewsImportSuccess(response){
                    console.log(response);

                    updateFabs();
                    isDirty = false;

                    displayStatusToast('Speichern erfolgreich!', 'var(--primary_color)', '#fff');

                    window.location.href = "/dashboard/news/bearbeiten/" + response;
                }

                function onNewsImportError(){
                    displayStatusToast('Importieren fehlgeschlagen!', '#f00', '#fff');
                    updateFabs();
                }

                var titleInput = document.getElementById('<?=$title_input->getId()?>');
                var metaTitleInput = document.getElementById('<?=$meta_title_input->getId()?>');
                var metaDescriptionInput = document.getElementById('<?=$meta_description_input->getId()?>');

                var numActiveLoadingIndicators = 4;

                doApiCall('news_article_generator/generate_title', {title: '<?=base64_encode($news_feed_item->title)?>'}, function (response) {
                    titleInput.value = response;
                    document.getElementById('news-item-importer-title-loading-animation').style.display = 'None';

                    numActiveLoadingIndicators--;

                    if (numActiveLoadingIndicators < 1) {
                        document.getElementById('news-item-importer-loading-animation').style.display = 'None';
                    }
                });

                doApiCall('news_article_generator/generate_content', {
                    content: '<?=base64_encode($news_feed_item->content)?>',
                    model: '<?=$model?>',
                    tokens: '<?=$tokens?>'
                }, function (response) {
                    ckeditor_instances['<?=$ckeditor_area->getId()?>'].setData(response);

                    document.getElementById('news-item-importer-content-loading-animation').style.display = 'None';

                    numActiveLoadingIndicators--;

                    if (numActiveLoadingIndicators < 1) {
                        document.getElementById('news-item-importer-loading-animation').style.display = 'None';
                    }
                });

                doApiCall('news_article_generator/generate_meta_title', {title: '<?=base64_encode($news_feed_item->title)?>'}, function (response) {
                    metaTitleInput.value = response;
                    document.getElementById('news-item-importer-meta-title-loading-animation').style.display = 'None';

                    numActiveLoadingIndicators--;

                    if (numActiveLoadingIndicators < 1) {
                        document.getElementById('news-item-importer-loading-animation').style.display = 'None';
                    }
                });

                doApiCall('news_article_generator/generate_meta_description', {title: '<?=base64_encode($news_feed_item->title)?>'}, function (response) {
                    metaDescriptionInput.value = response;
                    document.getElementById('news-item-importer-meta-description-loading-animation').style.display = 'None';

                    numActiveLoadingIndicators--;

                    if (numActiveLoadingIndicators < 1) {
                        document.getElementById('news-item-importer-loading-animation').style.display = 'None';
                    }
                });
            </script>
            <?php
        });
    }
}