<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\service\ProductFeedImportService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\helper\AuthHelper;

class FeedProductSearchHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
    }

    public function handle(){
        AuthHelper::checkAdminPanelUser();
        $product_link_manager = new ProductLinkManager();
        $product_feed_manager = new ProductFeedManager();
        $product_feed_import_service = new ProductFeedImportService();

        $feed_id = $this->params['feed_id'];

        $product_feed = $product_feed_manager->getById($feed_id);

        $results = $product_feed_import_service->search($this->params['value'], $product_feed);

        $imported_eans = $product_link_manager->getImportedEans($product_feed->product__shop_id);

        echo $product_feed_import_service->displayResults($results, $feed_id, $imported_eans);
    }
}