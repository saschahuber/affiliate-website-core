<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\service\ProductFeedImportService;
use saschahuber\affiliatewebsitecore\service\ProductImportSuggestionService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\builder\ui\RowBuilder;
use saschahuber\saastemplatecore\component\async\AsyncSearchComponent;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class ImportProductSuggestionHandler extends AdminAsyncHandler
{

    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle()
    {
        AuthHelper::checkAdminPanelUser();

        $product_suggestion_id = RequestHelper::reqstr('product_suggestion_id');

        $product_import_suggestion_service = new ProductImportSuggestionService();

        $product_suggestion = $product_import_suggestion_service->getById($product_suggestion_id);

        if($product_suggestion->feed_id === null){
            //Amazon
            $product_link_manager = new ProductLinkManager();
            $product_manager = AffiliateInterfacesHelper::getProductManager();

            $product_link = $product_link_manager->getByProductId($product_suggestion->product_id, null);

            if(!$product_link) {
                $product_link_manager->saveProductLink($product_suggestion->product_id, ProductManager::DEFAULT_AMAZON_SHOP_ID,
                    ProductManager::DEFAULT_AMAZON_FEED_ID, 0, 0, $product_suggestion->external_product_id,
                    ProductManager::getAmazonUrl($product_suggestion->external_product_id), true);

                $product = $product_manager->downloadAmazonProductInfo($product_suggestion->external_product_id, false);
            }
        }
        else {
            //Anderer Feed

            $product_feed_import_service = new ProductFeedImportService();
            $feed_manager = new ProductFeedManager();

            $feed = $feed_manager->getById($product_suggestion->feed_id);
            $item_data = $product_feed_import_service->getProductDataFromExternalId($feed, $product_suggestion->external_product_id);

            $product = $product_feed_import_service->updateProductFromFeed($product_suggestion->ean, $product_suggestion->feed_id, $item_data, $product_suggestion->product_id);
        }

        if($product){
            echo "Produkt {$product->title} importiert";

            (new RowBuilder())
                ->withColumn(new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => ProductManager::TYPE_PRODUCT, 'id' => $product->id]))
                ->withColumn((new LinkButton("/dashboard/produkte/bearbeiten/" . $product->id, 'Bearbeiten', 'fas fa-pen', false, true)))
                ->build()->display();

            $product_import_suggestion_service->deleteByProductId($product_suggestion->product_id);
        }
        else {
            echo "Konnte Produkt nicht importieren";
        }

    }
}