<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\component\form\ProductFilter;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;

class AsyncProductFilterHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle()
    {
        (new ProductFilter($this->params))->display();
    }
}