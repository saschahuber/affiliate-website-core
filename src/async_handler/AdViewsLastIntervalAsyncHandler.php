<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use DateTime;
use saschahuber\affiliatewebsitecore\service\AdViewsLastIntervalService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\chart\ChartValue;
use saschahuber\saastemplatecore\component\chart\Dataset;
use saschahuber\saastemplatecore\component\chart\StackedBarChart;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class AdViewsLastIntervalAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);

        $this->ad_views_last_interval_service = new AdViewsLastIntervalService();
    }

    public function handle()
    {
        echo $this->getChart();
    }

    private function getChart()
    {
        $interval = ArrayHelper::getArrayValue($this->params, 'interval', DatabaseTimeSeriesHelper::INTERVAL_DAY);
        $number = intval(ArrayHelper::getArrayValue($this->params, 'number', 30));
        $title = ArrayHelper::getArrayValue($this->params, 'title', false);
        $ad_id = ArrayHelper::getArrayValue($this->params, 'ad_id', null);

        return $this->getChartGroupedByColumn($interval, $number, $ad_id, $title);
    }

    private function getChartGroupedByColumn($interval, $number, $ad_id, $title = false)
    {
        global $VARIABLES;

        $datasets = [];

        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $date_format = "Y-m-d H:i";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $date_format = "Y-m-d H";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $date_format = "Y-m-d";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $date_format = "Y-m";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $date_format = "Y";
                break;
        }

        $time_series = $this->ad_views_last_interval_service->getTimeSeries($interval, $number);

        $log_item_map = $this->ad_views_last_interval_service->getLastIntervalAdViews($interval, $number, $ad_id);

        foreach ($time_series as $key => $value) {
            $time_series[$key] = (array_key_exists($key, $log_item_map) ? $log_item_map[$key] : 0);
        }

        foreach ($time_series as $time => $anzahl) {
            $time = DateTime::createFromFormat($date_format, $time)->format('Y-m-d H:i');
            $format_string = 'H:i';
            if ($interval == DatabaseTimeSeriesHelper::INTERVAL_DAY) {
                $format_string = 'D, d.m.';
            }
            $values[] = new ChartValue(DateHelper::format($time, $format_string), $anzahl);
        }

        $datasets[] = new Dataset("Aufrufe", $values, $VARIABLES['primary_color']);

        $height = intval(ArrayHelper::getArrayValue($this->params, 'height', null));
        $chart = new StackedBarChart($datasets, 0.00, 300, null, $height);

        ob_start();
        echo '<h3 class="centered">' . ($title ?: 'Ad-Aufrufe der letzten Zeit') . '</h3>';

        if (count($datasets) > 0) {
            $chart->display();
        } else {
            echo '<p class="no-margin centered">Noch keine Daten</p>';
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}