<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\affiliatewebsitecore\form\social_post_generator\AbstractPostGeneratorForm;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;
use saschahuber\saastemplatecore\persistence\UserRepository;

class SocialPostFeedItemPreviewHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
        $this->user_repository = new UserRepository();
    }

    public function handle()
    {
        AuthHelper::checkAdminPanelUser();
        echo $this->displayContent();
    }

    private function displayContent()
    {
        return BufferHelper::buffered(function () {
            $posting_id = ArrayHelper::getArrayValue($this->params, 'id', null);
            
            $social_posting_feed_item_manager = new SocialPostingFeedItemManager();
            
            $post = $social_posting_feed_item_manager->getById($posting_id);

            $preview_component = AbstractPostGeneratorForm::getSocialPostPreviewComponent($post, $post->target);
            $preview_component->display();
        });
    }
}