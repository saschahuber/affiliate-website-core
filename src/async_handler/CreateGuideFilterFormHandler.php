<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\async_handler\InsertEntryFormHandler;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\persistence\UserRepository;

class CreateGuideFilterFormHandler extends InsertEntryFormHandler
{
    public function __construct($params)
    {
        $product_taxonomies = [null => '--- Keine ---'];
        foreach((new ProductManager())->getTaxonomies() as $taxonomy){
            $product_taxonomies[$taxonomy->id] = $taxonomy->title;
        }
        $input_config = [
            'title' => ['type' => 'text', 'label' => 'Titel'],
            'result_type' => ['type' => 'hidden', 'value' => ProductManager::TYPE_PRODUCT, 'label' => 'Ergebnis-Typ'],
            'pre_filter_category_ids' => ['type' => 'select', 'allowed_values' => $product_taxonomies, 'label' => 'Auf Kategorie vorfiltern (optional)']
        ];
        parent::__construct(['table' => 'guide_filter', 'input_config' => $input_config]);
    }
}