<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use DateTime;
use saschahuber\affiliatewebsitecore\service\DailySocialMediaFollowersService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\chart\ChartValue;
use saschahuber\saastemplatecore\component\chart\Dataset;
use saschahuber\saastemplatecore\component\chart\StackedBarChart;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\ColorHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class DailySocialMediaFollowersAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);

        $this->daily_affiliate_link_clicks_service = new DailySocialMediaFollowersService();
    }

    public function handle()
    {
        echo $this->getChart();
    }

    private function getChart()
    {
        $interval = ArrayHelper::getArrayValue($this->params, 'interval', DatabaseTimeSeriesHelper::INTERVAL_DAY);
        $number = intval(ArrayHelper::getArrayValue($this->params, 'number', 30));
        $title = ArrayHelper::getArrayValue($this->params, 'title', false);

        return $this->getChartGroupedByColumn($interval, $number, $title);
    }

    private function getChartGroupedByColumn($interval, $number, $title = false)
    {
        $column = "app_name";
        $labels = $this->daily_affiliate_link_clicks_service->getPlatforms();

        $data_value_groups = [];

        $datasets = [];

        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $date_format = "Y-m-d H:i";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $date_format = "Y-m-d H";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $date_format = "Y-m-d";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $date_format = "Y-m";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $date_format = "Y";
                break;
        }

        $colors = [
            'event_type' => [],
            'device_os' => [],
            'screen_resolution' => []
        ];

        $time_series = $this->daily_affiliate_link_clicks_service->getTimeSeries($interval, $number);

        foreach ($labels as $label) {
            $log_item_map = $this->daily_affiliate_link_clicks_service->getDailySocialMediaFollowers($interval, $number, $label);

            $last_value = 0;
            foreach ($time_series as $key => $value) {
                if (array_key_exists($key, $log_item_map)) {
                    $last_value = $log_item_map[$key];
                }

                $time_series[$key] = $last_value;
            }

            $data_value_groups[$label] = $time_series;
        }

        foreach ($data_value_groups as $dataset_label => $items) {
            $values = [];
            foreach ($items as $time => $anzahl) {
                $time = DateTime::createFromFormat($date_format, $time)->format('Y-m-d H:i');
                $format_string = 'H:i';
                if ($interval == DatabaseTimeSeriesHelper::INTERVAL_DAY) {
                    $format_string = 'D, d.m.';
                }
                $values[] = new ChartValue(DateHelper::format($time, $format_string), $anzahl);
            }

            $color = ColorHelper::convertTextToColor($dataset_label);
            if (array_key_exists($column, $colors)) {
                if (array_key_exists($dataset_label, $colors[$column])) {
                    $color = $colors[$column][$dataset_label];
                }
            }

            $datasets[] = new Dataset($dataset_label, $values, $color);
        }

        $height = intval(ArrayHelper::getArrayValue($this->params, 'height', null));
        $chart = new StackedBarChart($datasets, null, $height);

        ob_start();
        echo '<h3 class="centered">' . ($title ?: 'Social Media Follower') . '</h3>';

        if (count($datasets) > 0) {
            $chart->display();
        } else {
            echo '<p class="no-margin centered">Noch keine Daten</p>';
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}