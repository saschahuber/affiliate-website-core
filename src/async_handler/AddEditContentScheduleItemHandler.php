<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\service\ContentScheduleItemService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\form\AsyncForm;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class AddEditContentScheduleItemHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
        $this->content_schedule_item_service = new ContentScheduleItemService();
    }

    public function handle()
    {
        $content_schedule_item_id = ArrayHelper::getArrayValue($this->params, 'content_schedule_item_id');

        $content_schedule_item = $this->content_schedule_item_service->getById($content_schedule_item_id);

        $input_rows = [
            [new Text("Eintrag für Content-Plan hinzufügen/bearbeiten", "h2")],
            [new HiddenInput("content_schedule_item_id", $content_schedule_item_id)],
            [new TextInput("title", "Titel", null, $content_schedule_item?$content_schedule_item->title:null)],
            [new DateTimePicker("date", "Geplantes Datum", null, $content_schedule_item?$content_schedule_item->content_schedule_item_date:null)]
        ];

        if($content_schedule_item && $content_schedule_item->linked_element_type && $content_schedule_item->linked_element_id){
            $linked_element = getContentElementByTypeAndId($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);

            $edit_url = getContentEditUrlByTypeAndId($content_schedule_item->linked_element_type, $content_schedule_item->linked_element_id);

            $linked_element_date = DateHelper::displayDateTime($linked_element->{$content_schedule_item->linked_element_type.'_date'});

            $input_rows[] = [new LinkButton($edit_url, "[{$content_schedule_item->linked_element_type}] {$linked_element->title} ($linked_element_date) [{$linked_element->status}]")];
        }

        $input_rows[] = [new SubmitButton('Speichern')];

        ?>
        <script>
            <?php
            foreach(AsyncForm::getJsFiles() as $js_file){
                echo file_get_contents($js_file);
            }
            ?>

            function onContentScheduleItemSaveSuccess(response){
                updateFabs();
                isDirty = false;
                displayStatusToast('Speichern erfolgreich!', 'var(--primary_color)', '#fff');
                location.reload();
            }

            function onContentScheduleItemSaveError(){
                displayStatusToast('Speichern fehlgeschlagen!', '#f00', '#fff');
                updateFabs();
            }
        </script>
        <?php

        (new AsyncForm($input_rows, 'content_schedule/save', 'onContentScheduleItemSaveSuccess', 'onContentScheduleItemSaveError'))->display();
    }
}