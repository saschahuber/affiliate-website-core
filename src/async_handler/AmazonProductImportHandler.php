<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\builder\ui\RowBuilder;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class AmazonProductImportHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
    }

    public function handle(){
        AuthHelper::checkAdminPanelUser();

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $product_link_manager = new ProductLinkManager();

        $asin = RequestHelper::reqstr("asin");
        $product_id = RequestHelper::reqint("product_id", null);

        if($product_id){
            $product_link = $product_link_manager->getByProductId($product_id, null);

            if(!$product_link) {
                $product_link_manager->saveProductLink($product_id, ProductManager::DEFAULT_AMAZON_SHOP_ID, ProductManager::DEFAULT_AMAZON_FEED_ID, 0, 0, $asin,
                    ProductManager::getAmazonUrl($asin), true);

                $product = $product_manager->downloadAmazonProductInfo($asin, false);
            }
        }
        else {
            $product = $product_manager->downloadAmazonProductInfo($asin, false);
        }

        echo BufferHelper::buffered(function() use ($product){
            echo "Produkt {$product->title} importiert";

            (new RowBuilder())
                ->withColumn(new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => ProductManager::TYPE_PRODUCT, 'id' => $product->id]))
                ->withColumn((new LinkButton("/dashboard/produkte/bearbeiten/" . $product->id, 'Bearbeiten', 'fas fa-pen', false, true)))
                ->build()->display();
        });
    }
}