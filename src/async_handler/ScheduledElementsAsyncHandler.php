<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class ScheduledElementsAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle()
    {
        echo BufferHelper::buffered(function () {
            (new Text('Geplante Beiträge', 'h3', ['centered']))->display();

            $items = [];

            $scheduled_posts = (new PostManager())->getScheduled();
            if (count($scheduled_posts)) {
                $items[] = BufferHelper::buffered(function () use ($scheduled_posts) {
                    (new Text('Beiträge:', 'strong'))->display();
                    foreach ($scheduled_posts as $post) {
                        ?>
                        <p><a href="/dashboard/blog/bearbeiten/<?= $post->id ?>"
                              target="blank"><?= DateHelper::displayDateTime($post->post_date) ?>
                                : <?= $post->title ?></a></p>
                        <?php
                    }
                });
            }

            $scheduled_products = (AffiliateInterfacesHelper::getProductManager())->getScheduled();
            if (count($scheduled_products)) {
                $items[] = BufferHelper::buffered(function () use ($scheduled_products) {
                    (new Text('Produkte:', 'strong'))->display();
                    foreach ($scheduled_products as $product) {
                        ?>
                        <p><a href="/dashboard/produkte/bearbeiten/<?= $product->id ?>"
                              target="blank"><?= DateHelper::displayDateTime($product->product_date) ?>
                                : <?= $product->title ?></a></p>
                        <?php
                    }
                });
            }

            $scheduled_news = (new NewsManager())->getScheduled();
            if (count($scheduled_news)) {
                $items[] = BufferHelper::buffered(function () use ($scheduled_news) {
                    (new Text('News-Artikel:', 'strong'))->display();
                    foreach ($scheduled_news as $news) {
                        ?>
                        <p><a href="/dashboard/news/bearbeiten/<?= $news->id ?>"
                              target="blank"><?= DateHelper::displayDateTime($news->news_date) ?>
                                : <?= $news->title ?></a></p>
                        <?php
                    }
                });
            }

            if (count($items)) {
                echo implode('<hr>', $items);
            } else {
                (new Text('Keine geplanten Elemente', 'p', ['centered']))->display();
            }
        });
    }
}