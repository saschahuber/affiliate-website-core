<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\service\TopClickedProductsSourcePagesService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class TopClickedProductsSourcePagesAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);

        $this->top_clicked_products_source_pages_service = new TopClickedProductsSourcePagesService();
    }

    public function handle()
    {
        echo $this->getChart();
    }

    private function getChart()
    {
        $days = intval(ArrayHelper::getArrayValue($this->params, 'days', 7));
        $limit = intval(ArrayHelper::getArrayValue($this->params, 'limit', 10));

        $items = [];

        foreach ($this->top_clicked_products_source_pages_service->getTopClickedProductsSourcePages($days, $limit) as $result) {
            $link = '<a href="' . $result->source_url . '" target="blank">' . $result->source_url . '</a>';
            $items[] = '<span><strong>(' . $result->anzahl_klicks . ' Klick' . ($result->anzahl_klicks > 1 ? 's' : '') . ')</strong> ' . $link . '</span>';
        }

        ob_start();
        echo '<h3 class="centered">TOP ' . $limit . ' Produkt-Klick-Quellen (' . $days . ' Tage)</h3>';

        (new BreakComponent())->display();

        if (count($items)) {
            (new ListContainer($items))->display();
        } else {
            echo '<p class="no-margin centered">' . "Keine Klicks in den letzten $days Tagen" . '</p>';
        }

        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}