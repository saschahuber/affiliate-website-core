<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\form\AsyncForm;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\persistence\UserRepository;

class AddEditAiMediaHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
        $this->user_repository = new UserRepository();
    }

    public function handle()
    {
        AuthHelper::checkAdminPanelUser();
        echo $this->displayContent();
    }

    private function displayContent()
    {
        return BufferHelper::buffered(function () {
            global $CONFIG;

            $attachment_id = ArrayHelper::getArrayValue($this->params, 'id', null);

            $image_manager = new AiImageManager();

            $attachment_prompt = null;
            $attachment_revised_prompt = null;

            if ($attachment_id) {
                $attachment = $image_manager->getAttachment($attachment_id);

                $attachment_prompt = $attachment->prompt;
                $attachment_revised_prompt = $attachment->revised_prompt;
                $use_in_attachment_generator = $attachment->use_in_attachment_generator;
            }

            $grid_items = [
                new Text("KI-Bild bearbeiten", "h2"),
                new Text("Eingegebener Prompt", "h3"),
                new Text($attachment_prompt),
                new Text("Durch Dall-E verbesserter Prompt", "h3"),
                new Text($attachment_revised_prompt),
            ];

            if(!$attachment->attachment_id){
                $grid_items[] = new JsButton('Als Attachment speichern', "saveAiImageAsAttachment($attachment_id)");
            }

            $input_items = [[
                new HiddenInput('image_id', $attachment_id),
                new TextInput('file_name', 'Dateiname', 'Dateiname', $attachment->file_name),
                new SubmitButton('Umbenennen')
            ]];

            (new GridContainer($grid_items, 1))->display();

            BreakComponent::break();

            if ($attachment_id) {
                echo $image_manager->getAttachmentImageTag($attachment_id);

                ?>
                <script>
                    <?php
                    foreach(AsyncForm::getJsFiles() as $js_file){
                        echo file_get_contents($js_file);
                    }
                    ?>

                    function onAiImageSaveSuccess(response){
                        updateFabs();
                        isDirty = false;
                        displayStatusToast('Speichern erfolgreich!', 'var(--primary_color)', '#fff');
                    }

                    function onAiImageSaveError(){
                        displayStatusToast('Speichern fehlgeschlagen!', '#f00', '#fff');
                        updateFabs();
                    }
                </script>
                <?php

                (new AsyncForm($input_items, 'ai_image/rename', 'onAiImageSaveSuccess', 'onAiImageSaveSuccess'))->display();
            }
        });
    }
}