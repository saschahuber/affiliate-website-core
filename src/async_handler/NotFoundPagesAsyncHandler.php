<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;

class NotFoundPagesAsyncHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
    }

    public function handle(){
        AuthHelper::checkAdminPanelUser();

        return '<p class="no-margin centered">Noch nicht implementiert.</p>';

        $days = intval(ArrayHelper::getArrayValue($this->params, 'last_days', 7));

        global $VARIABLES;

        $labels = [];
        $data = [];
        $values = [];

        ob_start();
        echo '<h3 class="centered">Seiten mit 404 (letzte ' . $days . ' Tage)</h3>';

        if (count($data) > 0) {
            #$chart->display();
        } else {
            echo '<p class="no-margin centered">Noch keine Daten</p>';
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}