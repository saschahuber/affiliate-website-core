<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\async_handler\AsyncHandler;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\model\TextGenerationTemplate;
use saschahuber\saastemplatecore\service\TextGenerationService;

class Gpt3TextGenerationAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle()
    {
        $text_generation_service = new TextGenerationService();

        $result = $text_generation_service->generateTextFromTemplate($this->getTemplate());

        return $result->getContent();
    }

    private function getTemplate()
    {
        $prompt = ArrayHelper::getArrayValue($this->params, 'prompt');
        $model = ArrayHelper::getArrayValue($this->params, 'model', TextGenerationTemplate::MODEL_DAVINCI);
        $max_tokens = ArrayHelper::getArrayValue($this->params, 'max_tokens', 4000);
        $temperature = ArrayHelper::getArrayValue($this->params, 'temperature', 0.9);
        $frequency_penalty = ArrayHelper::getArrayValue($this->params, 'frequency_penalty', 0);
        $presence_penalty = ArrayHelper::getArrayValue($this->params, 'presence_penalty', 0.6);

        return new TextGenerationTemplate($prompt, $model, $max_tokens, $temperature, $frequency_penalty, $presence_penalty);
    }
}