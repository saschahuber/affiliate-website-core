<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\NewsFeedImportItemManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class ImportedNewsAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
    }

    public function handle()
    {
        echo BufferHelper::buffered(function () {
            (new Text('Importierte News der letzten 3 Tage', 'h3', ['centered']))->display();

            $items = [];

            $imported_news = (new NewsFeedImportItemManager())->getImportedInPastDays(1);
            if (count($imported_news)) {
                $items[] = BufferHelper::buffered(function () use ($imported_news) {
                    foreach ($imported_news as $news) {
                        ?>
                        <p>
                        <?= DateHelper::displayDateTime($news->date) ?>: <?= $news->title ?>
                        <?php
                    }
                });
            }

            if (count($items)) {
                echo implode('<hr>', $items);
            } else {
                (new Text('Keine Importierten Elemente', 'p', ['centered']))->display();
            }
        });
    }
}