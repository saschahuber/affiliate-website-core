<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\persistence\UserRepository;

class AddEditMediaHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
        $this->user_repository = new UserRepository();
    }

    public function handle(){
        AuthHelper::checkAdminPanelUser();
        echo $this->displayContent();
    }

    private function displayContent(){
        return BufferHelper::buffered(function(){
            global $CONFIG;

            $attachment_id = ArrayHelper::getArrayValue($this->params, 'id', null);

            $image_manager = new ImageManager();

            $attachment_title = null;
            $attachment_alt_text = null;
            $attachment_file_path = null;
            $attachment_description = null;
            $attachment_provider_name = null;
            $attachment_copyright_info = null;
            $attachment_items = null;
            $use_in_attachment_generator = false;

            if($attachment_id){
                $attachment = $image_manager->getAttachment($attachment_id);

                $attachment_title = $attachment->title;
                $attachment_alt_text = $attachment->alt_text;
                $attachment_file_path = $attachment->file_path;
                $attachment_description = $attachment->description;
                $attachment_provider_name = $attachment->provider_name;
                $attachment_copyright_info = $attachment->copyright_info;
                $use_in_attachment_generator = $attachment->use_in_attachment_generator;
            }

            $input_rows = [
                [new Text("Bild hinzufügen/bearbeiten", "h2")],
                [new HiddenInput("task", "save")],
                [new HiddenInput("attachment_id", $attachment_id)],
                [new TextInput("title", "Titel-Attribut", null, $attachment_title)],
                [new TextInput("alt_text", "Alt-Text", null, $attachment_alt_text)],
                [new TextInput("description", "Beschreibung (Was ist auf dem Bild zu sehen?)", null, $attachment_description)],
                [new TextInput("provider_name", "Stock Anbieter (z.B. Adobe Stock)", null, $attachment_provider_name)],
                [new TextInput("copyright_info", "Copyright-Info", null, $attachment_copyright_info)],
                [new Toggle("use_in_attachment_generator", "In Attachment-Generator benutzen", $use_in_attachment_generator, false, true)]
            ];

            if($attachment_id){
                $input_rows[] = [new SubmitButton("Bild speichern")];

                if($attachment->generator_data) {
                    $input_rows[] = [new LinkButton('/dashboard/medien/bild-generator/'.$attachment_id, "Attachment bearbeiten")];
                }
            }
            else {
                $input_rows[] = [new BreakComponent()];
                $input_rows[] = [new HTMLElement(BufferHelper::buffered(function() {
                    ?>
                    <p>
                        <label>Dateiname: <span class="file-name-loading-indicator"></span></label>
                        <input name="file_name" type="text" onchange="checkImageFileName()" onkeyup="checkImageFileName()">
                    </p>
                    <p>
                        <label>Bild hochladen (Maximal 64 MB):</label>
                        <input name="image-file" type="file" accept=".jpg,.jpeg,.png,.gif" onchange="updateImageFileName(this)">
                    </p>

                    <script>
                        document.querySelector('.add-edit-image-submit-button').disabled = true;

                        function checkImageFileName(){
                            let fileNameLoadingIndicator = document.querySelector('.file-name-loading-indicator');
                            fileNameLoadingIndicator.innerHTML = '<i class="fas fa-spin fa-spinner"></i>';

                            let fileNameInput = document.querySelector('input[name="file_name"]')

                            let fileName = fileNameInput.value;

                            let submitButton = document.querySelector('.add-edit-image-submit-button');
                            doApiCall('file_tools/check_attachment_name', {file_name: fileName}, function (response) {
                                fileNameLoadingIndicator.innerHTML = '';

                                submitButton.disabled = true;

                                let hasMatch = JSON.parse(response).has_match;

                                if(hasMatch){
                                    let fileNameInput = document.querySelector('input[name="file_name"]');
                                    fileNameInput.style.border = "solid 3px red";
                                    submitButton.disabled = true;
                                }
                                else{
                                    fileNameInput.style.border = "solid 3px green";
                                    submitButton.disabled = false;
                                }
                            })
                        }

                        function updateImageFileName(input){
                            document.querySelector('input[name="file_name"]').value = input.files[0].name;
                            checkImageFileName();
                        }
                    </script>
                    <?php
                }))];

                $input_rows[] = [new SubmitButton("Bild hochladen", null, false, ["add-edit-image-submit-button"])];
            }

            (new Form($input_rows, "/dashboard/medien", Form::METHOD_POST, null, null, 'multipart/form-data'))->display();

            if($attachment_id){
                ?>
                <img class="img-media-editor" src="<?=$CONFIG->website_domain?><?=$image_manager->createAttachmentSrcFromFilePath($attachment_file_path)?>">
                <?php
            }

            if($attachment_items && count($attachment_items) > 0){
                ?>
                <div>
                    <h2>Zugeordnete Beiträge:</h2>
                    <?php
                    foreach($attachment_items as $post_type => $items){
                        echo '<p><strong>'.$post_type.':</strong></p>';
                        echo '<ul>';
                        foreach($items as $item){
                            echo '<li><a href="'.$item->permalink.'" target="blank">'.$item->title.'</a></li>';
                        }
                        echo '</ul>';
                    }
                    ?>
                </div>
                <?php
            }
        });
    }
}