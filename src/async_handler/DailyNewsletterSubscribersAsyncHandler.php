<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\service\DailyNewsletterSubscribersService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\chart\BarChart;
use saschahuber\saastemplatecore\component\chart\ChartValue;
use saschahuber\saastemplatecore\component\chart\Dataset;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class DailyNewsletterSubscribersAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);

        $this->daily_newsletter_subscribers_service = new DailyNewsletterSubscribersService();
    }

    public function handle()
    {
        echo $this->getChart();
    }

    private function getChart()
    {
        global $VARIABLES;

        $days = intval(ArrayHelper::getArrayValue($this->params, 'last_days', 30));

        $data_items = $this->daily_newsletter_subscribers_service->getSubscribers($days);

        $labels = [];
        $data = [];
        $values = [];
        foreach ($data_items as $data_item) {
            $labels[$data_item->formatted_date] = $data_item->formatted_date;
            $data[$data_item->formatted_date] = $data_item;

            $values[] = new ChartValue(DateHelper::format($data_item->formatted_date, 'd.m.Y'), $data_item->hits);
        }

        $dataset = new Dataset('Abonnenten', $values, $VARIABLES['primary_color']);

        $height = intval(ArrayHelper::getArrayValue($this->params, 'height', null));
        $chart = new BarChart([$dataset], null, $height);

        ob_start();
        echo '<h3 class="centered">Newsletter-Abonnenten (letzte ' . $days . ' Tage)</h3>';

        if (count($data) > 0) {
            $chart->display();
        } else {
            echo '<p class="no-margin centered">Noch keine Daten</p>';
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}