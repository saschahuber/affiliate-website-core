<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;

class SelectAttachmentHandler extends AdminAsyncHandler{
    private $attachment_id, $input_name, $type_input_name;

    public function __construct($params){
        parent::__construct($params);
        $this->setAttachmentId(ArrayHelper::getArrayValue($params, 'attachment_id', null));
        $this->setTypeInputName(ArrayHelper::getArrayValue($params, 'type_input_name', null));
        $this->setInputName(ArrayHelper::getArrayValue($params, 'input_name', null));
        $this->image_manager = new ImageManager();
        $this->layout_manager = new LayoutManager();
    }

    public function handle(){
        AuthHelper::checkAdminPanelUser();
        echo $this->getHtml()
            . $this->getStylesheet()
            . $this->getScript();
    }

    private function displayImageSelectionHtml(){
        global $CONFIG;

        $image_contents = array();
        foreach($this->image_manager->getAttachments() as $attachment){
            ob_start();

            $attachment_data = array(
                'id' => $attachment->id,
                'src' => $CONFIG->website_domain . '/data/media' . $attachment->file_path,
                'title' => $attachment->title,
                'alttext' => $attachment->alt_text,
            );

            ?>
            <div class="media-grid-item">
                <a onclick="openAttachmentSelectionForImage('<?=base64_encode(json_encode($attachment_data))?>')">
                    <img src="<?=$attachment->src?>">
                </a>
            </div>
            <?php
            $image_content = ob_get_contents();
            ob_end_clean();
            $image_contents[] = $image_content;
        }

        (new JsButton('Bild entfernen', "resetAttachmentImage('".$this->getInputName()."','".$this->getTypeInputName()."')"))->display();

        echo $this->layout_manager->grid($image_contents, 6, "sm");
    }

    private function getImageConfigurationHtml(){
        ob_start();
        ?>

        <?php
        (new JsButton("Zurück zur Bilderauswahl", "backToImageSelection()"))->display();
        ?>
        <div>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        <img id="configure-image-container-preview">
                    </p>
                </div>
            </div>
            <?php
            (new JsButton("Bild auswählen", "setImageAsAttachment('".$this->getInputName()."', '".$this->getTypeInputName()."')"))->display();
            ?>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getHtml(){
        ob_start();

        (new BreakComponent())->display();

        ?>
        <div>
            <div id="select-image-container" class="selected">
                <?php $this->displayImageSelectionHtml(); ?>
            </div>
            <div id="configure-image-container">
                <?=$this->getImageConfigurationHtml()?>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getScript(){
        ob_start();
        ?>
        <script>
            <?=file_get_contents(PUBLIC_DIR . '/js/attachment_selector.js')?>
            resetImageToolbar();
        </script>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getStylesheet(){
        ob_start();
        ?>
        <style>
            <?=file_get_contents(PUBLIC_DIR . '/css/image_toolbar.css')?>
        </style>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * @return mixed
     */
    public function getAttachmentId()
    {
        return $this->attachment_id;
    }

    /**
     * @param mixed $attachment_id
     */
    public function setAttachmentId($attachment_id): void
    {
        $this->attachment_id = $attachment_id;
    }

    /**
     * @return mixed
     */
    public function getInputName()
    {
        return $this->input_name;
    }

    /**
     * @param mixed $input_name
     */
    public function setInputName($input_name): void
    {
        $this->input_name = $input_name;
    }

    /**
     * @return mixed
     */
    public function getTypeInputName()
    {
        return $this->type_input_name;
    }

    /**
     * @param mixed $type_input_name
     */
    public function setTypeInputName($type_input_name): void
    {
        $this->type_input_name = $type_input_name;
    }
}