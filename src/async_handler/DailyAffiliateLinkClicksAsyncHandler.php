<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\service\DailyAffiliateLinkClicksService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\chart\ChartValue;
use saschahuber\saastemplatecore\component\chart\Dataset;
use saschahuber\saastemplatecore\component\chart\StackedBarChart;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\ColorHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class DailyAffiliateLinkClicksAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);

        $this->daily_affiliate_link_clicks_service = new DailyAffiliateLinkClicksService();
    }

    public function handle()
    {
        echo $this->getChart();
    }

    private function getChart()
    {
        global $VARIABLES;

        $days = intval(ArrayHelper::getArrayValue($this->params, 'last_days', 30));
        $product_id = intval(ArrayHelper::getArrayValue($this->params, 'product_id', null));
        $store = intval(ArrayHelper::getArrayValue($this->params, 'store', null));

        $data_items = $this->daily_affiliate_link_clicks_service->getDailyAffiliateLinkClicks($days, $product_id, $store);

        $labels = [];
        $value_sets = [];

        foreach ($data_items as $data_item) {
            $labels[$data_item->formatted_date] = $data_item->formatted_date;

            if (!array_key_exists($data_item->store_type, $value_sets)) {
                $value_sets[$data_item->store_type] = [];
            }
            $value_sets[$data_item->store_type][] = new ChartValue(DateHelper::format($data_item->formatted_date, 'd.m.Y'), $data_item->hits);
        }

        $datasets = [];

        foreach ($value_sets as $label => $values) {
            $datasets[] = new Dataset($label, $values, ColorHelper::convertTextToColor($label));
        }

        $height = intval(ArrayHelper::getArrayValue($this->params, 'height', null));
        $chart = new StackedBarChart($datasets, null, $height);

        ob_start();
        echo '<h3 class="centered">Affiliate-Clicks in den letzten ' . $days . ' Tagen</h3>';

        if (count($datasets) > 0) {
            $chart->display();
        } else {
            echo '<p class="no-margin centered">Noch keine Daten</p>';
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}