<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\component\ToolbarButton;
use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\JsButton;

class AiImageToolbarAsyncHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
        $this->stock_image_manager = new AiImageManager();
        $this->layout_manager = new LayoutManager();
    }

    public function handle(){
        echo $this->getHtml()
            . $this->getStylesheet()
            . $this->getScript();
    }

    private function displayImageSelectionHtml(){
        global $CONFIG;

        $image_contents = array();
        foreach($this->stock_image_manager->getAttachments() as $attachment){
            ob_start();

            $attachment_data = array(
                'id' => $attachment->id,
                'src' => $CONFIG->website_domain . '/data/media' . $attachment->file_path,
                'title' => $attachment->title,
                'alttext' => $attachment->alt_text,
            );

            ?>
            <div class="media-grid-item">
                <a onclick="selectAiImage('<?=base64_encode(json_encode($attachment_data))?>')">
                    <img src="<?=$attachment->src?>">
                </a>
            </div>
            <?php
            $image_content = ob_get_contents();
            ob_end_clean();
            $image_contents[] = $image_content;
        }

        echo $this->layout_manager->grid($image_contents, 6, "sm");
    }

    private function getImageConfigurationHtml(){
        ob_start();
        ?>

        <?php
        (new JsButton("Zurück zur Bilderauswahl", "backToImageSelection()"))->display();
        ?>
        <div>
            <div class="row">
                <div class="col-md-6">
                    <?php

                    (new TextInput("image_title", "Titel"))->display();

                    (new TextArea("image_alt_text", "Alt-Text"))->display();

                    (new TextArea("image_caption", "Caption"))->display();

                    (new NumberInput("image_width", "Breite", null, null, 0, null, 1))->display();

                    (new NumberInput("image_height", "Höhe", null, null, 0, null, 1))->display();

                    (new Select('image_align', 'Align', [null => 'Nicht zentriert', 'left' => 'Links', 'center' => 'Zentriert', 'right' => 'Rechts'], 'center'))->display();

                    ?>

                    <div class="shortcode_code row">
                        <div class="col-lg-3">
                            <?php
                            (new JsButton("Kopieren", "copyAiImageShortcodeToClipboard()"))->display();
                            ?>
                        </div>
                        <div class="col-lg-9">
                            <div id="image_shortcode"></div>
                        </div>
                    </div>

                    <?php
                    (new JsButton("Code generieren", "showAiGeneratedImageShortcode()"))->display();
                    ?>
                </div>
                <div class="col-md-6">
                    <p>
                        <img id="configure-image-container-preview">
                    </p>
                </div>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getHtml(){
        ob_start();

        (new ToolbarButton([], "Zurück zur Toolbar"))->display();

        (new BreakComponent())->display();

        ?>
        <div>
            <div id="select-image-container" class="selected">
                <?php $this->displayImageSelectionHtml(); ?>
            </div>
            <div id="configure-image-container">
                <?=$this->getImageConfigurationHtml()?>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getScript(){
        ob_start();
        ?>
        <script>
            <?=file_get_contents(PUBLIC_DIR . '/js/ai_image_toolbar.js')?>
            resetImageToolbar();
        </script>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getStylesheet(){
        ob_start();
        ?>
        <style>
            <?=file_get_contents(PUBLIC_DIR . '/css/image_toolbar.css')?>
        </style>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}