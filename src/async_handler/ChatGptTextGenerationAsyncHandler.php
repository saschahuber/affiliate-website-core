<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\service\TextGenerationService;

class ChatGptTextGenerationAsyncHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
    }

    public function handle(){
        $text_generation_service = new TextGenerationService();

        $result = $text_generation_service->getNextChatMessage($this->getTemplate());

        return $result->getContent();
    }

    private function getTemplate(){
        $system_prompt = ArrayHelper::getArrayValue($this->params, 'system_prompt', 'Du bist ein hilfreicher Assistent.');

        $prompt = ArrayHelper::getArrayValue($this->params, 'prompt');
        $messages = [
            ['role' => 'system', 'content' => $system_prompt],
            ['role' => 'user', 'content' => $prompt]
        ];
        $model = ArrayHelper::getArrayValue($this->params, 'model', ChatGenerationTemplate::MODEL_CHAT_GPT);
        #$model = ChatGenerationTemplate::MODEL_CHAT_GPT_4;
        $max_tokens = ArrayHelper::getArrayValue($this->params, 'max_tokens', 4000);
        $temperature = ArrayHelper::getArrayValue($this->params, 'temperature', 0.9);
        $frequency_penalty = ArrayHelper::getArrayValue($this->params, 'frequency_penalty', 0);
        $presence_penalty = ArrayHelper::getArrayValue($this->params, 'presence_penalty', 0.6);

        return new ChatGenerationTemplate($messages, $model, $max_tokens, $temperature, $frequency_penalty, $presence_penalty);
    }
}