<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;

#[AllowDynamicProperties]
class ProductToImportSearchHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
    }

    public function handle()
    {
        AuthHelper::checkAdminPanelUser();
        $products = $this->product_manager->findProductsByKeyword($this->params['value']);
        return $this->displayResults($products);
    }

    function displayResults($products)
    {
        ob_start();
        ?>
        <div>
            <?php
            $rows = [];

            foreach ($products as $product) {
                $rows[] = $this->getTableRow($product);
            }

            $column_labels = [
                'Bild',
                'Name',
                'Preis',
                'EAN',
                'Aktionen'
            ];

            (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
            ?>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getTableRow($product)
    {
        $reduced_price_suffix = (isset($product->product_links[0]->reduced_price) ? " (Angebot: " . str_replace(".", ",", $product->product_links[0]->reduced_price) . "€)" : "");

        $short_title = $product->title;
        if (strlen($short_title) > 128) {
            $short_title = substr($short_title, 0, 128) . "...";
        }

        $price_info = (isset($product->product_links[0]->price) ? str_replace(".", ",", $product->product_links[0]->price) . "€" . $reduced_price_suffix : "-");
        ob_start();

        $feed_id = ArrayHelper::getArrayValue($this->params, 'feed_id');
        $all_data = ArrayHelper::getArrayValue($this->params, 'all_data');
        $ean = ArrayHelper::getArrayValue($this->params, 'ean');
        $asin = ArrayHelper::getArrayValue($this->params, 'asin');
        $external_id = ArrayHelper::getArrayValue($this->params, 'external_id');

        $cells = [
            new Image($this->product_manager->getProductImageUrl($product), 75),
            $short_title,
            $price_info,
            $product->ean,
            new HTMLElement(BufferHelper::buffered(function () use ($product, $feed_id, $all_data, $ean, $asin, $external_id) {
                global $CONFIG;

                if($asin) {
                    $params = [
                        'product_id' => $product->id,
                        'asin' => $asin,
                        'feed_id' => $feed_id,
                        'external_id' => $external_id,
                    ];
                    (new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Zu diesem Produkt importieren', "AmazonProductImportHandler", $params))->display();
                }
                else {
                    $params = [
                        'feed_id' => $feed_id,
                        'data' => $all_data,
                        'ean' => $ean,
                        'asin' => $asin,
                        'mode' => FeedProductImportHandler::MODE_IMPORT,
                        'product_id' => $product->id,
                    ];
                    (new GlobalAsyncComponentButton('<i class="fas fa-plus-circle"></i> Zu diesem Produkt importieren', "FeedProductImportHandler", $params))->display();
                }

                (new LinkButton($CONFIG->website_domain . $this->product_manager->generatePermalink($product), 'Ansehen', 'fas fa-eye', false, true))->display();
                (new LinkButton("/dashboard/produkte/bearbeiten/" . $product->id, 'Bearbeiten', 'fas fa-pen', false, true))->display();
            }))
        ];
        return new TableRow($cells);
    }
}