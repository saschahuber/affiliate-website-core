<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\UploadManager;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\persistence\UserRepository;

class AddEditFileHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);
        $this->user_repository = new UserRepository();
    }

    public function handle()
    {
        AuthHelper::checkAdminPanelUser();
        echo $this->displayContent();
    }

    private function displayContent()
    {
        return BufferHelper::buffered(function () {
            global $CONFIG;

            $upload_id = ArrayHelper::getArrayValue($this->params, 'id', null);

            $upload_manager = new UploadManager();

            $upload_title = null;
            $upload_alt_text = null;
            $upload_file_path = null;
            $upload_description = null;
            $upload_provider_name = null;
            $upload_copyright_info = null;

            if ($upload_id) {
                $upload = $upload_manager->getUpload($upload_id);

                $upload_title = $upload->title;
                $upload_alt_text = $upload->alt_text;
                $upload_file_path = $upload->file_path;
                $upload_description = $upload->description;
                $upload_provider_name = $upload->provider_name;
                $upload_copyright_info = $upload->copyright_info;
            }

            $upload_manager = new ImageManager();

            $input_rows = [
                [new Text("Datei hinzufügen/bearbeiten", "h2")],
                [new HiddenInput("task", "save")],
                [new HiddenInput("upload_id", $upload_id)],
                [new TextInput("title", "Titel-Attribut", null, $upload_title)],
                [new TextInput("alt_text", "Alt-Text", null, $upload_alt_text)],
                [new TextInput("description", "Beschreibung (Was ist auf dem Bild zu sehen?)", null, $upload_description)],
                [new TextInput("provider_name", "Stock Anbieter (z.B. Adobe Stock)", null, $upload_provider_name)],
                [new TextInput("copyright_info", "Copyright-Info", null, $upload_copyright_info)]
            ];

            if ($upload_id) {
                $input_rows[] = [new SubmitButton("Datei speichern")];
            } else {
                $input_rows[] = [new HTMLElement(BufferHelper::buffered(function () {
                    ?>
                    <p>
                        <label>Datei hochladen (MP4, GIF, PDF):</label>
                        <input name="file" type="file" accept=".mp4,.gif,.pdf">
                    </p>
                    <?php
                }))];

                $input_rows[] = [new SubmitButton("Datei hochladen")];
            }

            (new Form($input_rows, "/dashboard/medien/dateien/datei-bearbeiten", Form::METHOD_POST, null, null, 'multipart/form-data'))->display();

            if ($upload_id) {
                ?>
                <img class="img-media-editor"
                     src="<?= $CONFIG->website_domain ?><?= $upload_manager->createAttachmentSrcFromFilePath($upload_file_path) ?>">
                <?php
            }
        });
    }
}