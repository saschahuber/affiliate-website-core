<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use DateTime;
use saschahuber\affiliatewebsitecore\service\CompanyActionLogLastIntervalService;
use saschahuber\affiliatewebsitecore\service\CompanyTrackingService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\chart\ChartValue;
use saschahuber\saastemplatecore\component\chart\Dataset;
use saschahuber\saastemplatecore\component\chart\StackedBarChart;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\ColorHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class CompanyActionLogLastIntervalAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);

        $this->analytics_entries_last_interval_service = new CompanyActionLogLastIntervalService();
    }

    public function handle()
    {
        echo $this->getChart();
    }

    private function getChart()
    {
        $interval = ArrayHelper::getArrayValue($this->params, 'interval', DatabaseTimeSeriesHelper::INTERVAL_MINUTE);
        $number = intval(ArrayHelper::getArrayValue($this->params, 'number', 60));
        $action = intval(ArrayHelper::getArrayValue($this->params, 'action', 60));
        $company_id = intval(ArrayHelper::getArrayValue($this->params, 'company_id', 60));
        $title = ArrayHelper::getArrayValue($this->params, 'title', false);
        $from_time = ArrayHelper::getArrayValue($this->params, 'from_time', false);

        if($from_time) {
            $number = DateHelper::timeAgoInUnit($from_time, $interval);
        }

        return $this->getChartGroupedByColumn($interval, $number, $title);
    }

    private function getChartGroupedByColumn($interval, $number, $title = false)
    {
        $action = ArrayHelper::getArrayValue($this->params, 'action', []);
        $company_id = ArrayHelper::getArrayValue($this->params, 'company_id', []);
        $from_time = ArrayHelper::getArrayValue($this->params, 'from_time', null);
        $to_time = ArrayHelper::getArrayValue($this->params, 'to_time', null);

        if(!$from_time){
            $interval_string = "minutes";
            $interval_number = 60;
            switch ($interval){
                case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                    $interval_string = "hours";
                    $interval_number = 24;
                    break;
                case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                    $interval_string = "days";
                    $interval_number = 30;
                    break;
                case DatabaseTimeSeriesHelper::INTERVAL_WEEK:
                    $interval_string = "weeks";
                    $interval_number = 52;
                    break;
                case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                    $interval_string = "months";
                    $interval_number = 12;
                    break;
                case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                    $interval_string = "years";
                    $interval_number = 5;
                    break;
            }

            $currentDateTime = new DateTime();
            $currentDateTime->modify("-$interval_number $interval_string");
            $from_time = $currentDateTime->format('Y-m-d\TH:i');
        }

        $data_value_groups = [];

        $datasets = [];

        switch ($interval) {
            case DatabaseTimeSeriesHelper::INTERVAL_MINUTE:
                $date_format = "Y-m-d H:i";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_HOUR:
                $date_format = "Y-m-d H";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_DAY:
                $date_format = "Y-m-d";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_MONTH:
                $date_format = "Y-m";
                break;
            case DatabaseTimeSeriesHelper::INTERVAL_YEAR:
                $date_format = "Y";
                break;
        }

        $colors = [
            CompanyTrackingService::TRACKING_TYPE_VIEW => "#0099cc",
            CompanyTrackingService::TRACKING_TYPE_CLICK_MAIL => "#9900cc",
            CompanyTrackingService::TRACKING_TYPE_CLICK_PHONE => "#cc9900",
            CompanyTrackingService::TRACKING_TYPE_CLICK_WEBSITE => "#00cc99",
        ];

        $time_series = $this->analytics_entries_last_interval_service->getTimeSeries($interval, $number);

        $labels = CompanyTrackingService::ALLOWED_TRACKING_TYPES;

        foreach ($labels as $label) {
            $log_item_map = $this->analytics_entries_last_interval_service->getLastIntervalLogEntries($interval, $number, $label, $company_id);

            foreach ($time_series as $key => $value) {
                $time_series[$key] = (array_key_exists($key, $log_item_map) ? $log_item_map[$key] : 0);
            }

            if(!$label){
                $label = 'Nicht gesetzt';
            }

            $data_value_groups[$label] = $time_series;
        }

        foreach ($data_value_groups as $dataset_label => $items) {
            $complete_count = 0;

            $values = [];
            foreach ($items as $time => $anzahl) {
                $complete_count += $anzahl;
                if($interval === DatabaseTimeSeriesHelper::INTERVAL_WEEK){
                    $values[] = new ChartValue($time, $anzahl);
                }
                else {
                    $time = DateTime::createFromFormat($date_format, $time)->format('Y-m-d H:i');
                    $format_string = 'H:i';
                    if ($interval == DatabaseTimeSeriesHelper::INTERVAL_DAY) {
                        $format_string = 'D, d.m.';
                    }
                    if ($interval == DatabaseTimeSeriesHelper::INTERVAL_MONTH) {
                        $format_string = 'm/Y';
                    }
                    if ($interval == DatabaseTimeSeriesHelper::INTERVAL_YEAR) {
                        $format_string = 'Y';
                    }
                    $values[] = new ChartValue(DateHelper::format($time, $format_string), $anzahl);
                }
            }

            $color = ColorHelper::convertTextToColor($dataset_label);
            if (array_key_exists($dataset_label, $colors)) {
                $color = $colors[$dataset_label];
            }

            if($complete_count > 0) {
                $label_for_dataset = CompanyTrackingService::TRACKING_TYPE_LABELS[$dataset_label];
                $datasets[] = new Dataset($label_for_dataset, $values, $color);
            }
        }

        $height = intval(ArrayHelper::getArrayValue($this->params, 'height', null));
        $chart = new StackedBarChart($datasets, 0.00, $height);

        ob_start();
        echo '<h3 class="centered">' . ($title ?: 'Logs der letzten Zeit') . '</h3>';

        if (count($datasets) > 0) {
            $chart->display();
        } else {
            echo '<p class="no-margin centered">Noch keine Daten</p>';
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}