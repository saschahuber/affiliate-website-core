<?php


namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\affiliatewebsitecore\service\TopClickedProductsService;
use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\async_handler\AsyncHandler;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class TopClickedProductsAsyncHandler extends AdminAsyncHandler
{
    public function __construct($params)
    {
        parent::__construct($params);

        $this->top_clicked_products_service = new TopClickedProductsService();
    }

    public function handle()
    {
        echo $this->getChart();
    }

    private function getChart()
    {
        $days = intval(ArrayHelper::getArrayValue($this->params, 'days', 7));
        $limit = intval(ArrayHelper::getArrayValue($this->params, 'limit', 10));

        $items = [];

        foreach ($this->top_clicked_products_service->getTopClickedProducts($days, $limit) as $result) {
            $items[] = '<span><strong>(' . $result->anzahl_klicks . ' Klick' . ($result->anzahl_klicks > 1 ? 's' : '') . ')</strong> ' . $result->title . '</span>';
        }

        ob_start();
        echo '<h3 class="centered">TOP ' . $limit . ' Produkt-Klicks (' . $days . ' Tage)</h3>';

        (new BreakComponent())->display();

        if (count($items)) {
            (new ListContainer($items))->display();
        } else {
            echo '<p class="no-margin centered">' . "Keine Klicks in den letzten $days Tagen" . '</p>';
        }

        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}