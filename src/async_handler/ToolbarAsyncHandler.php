<?php

namespace saschahuber\affiliatewebsitecore\async_handler;

use saschahuber\saastemplatecore\async_handler\AdminAsyncHandler;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\Text;

class ToolbarAsyncHandler extends AdminAsyncHandler{
    public function __construct($params){
        parent::__construct($params);
    }

    public function handle(){
        $buttons = [
            new GlobalAsyncComponentButton('<i class="fas fa-image"></i> Medien', "ImageToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-file"></i> Dateien', "UploadToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-image"></i> Stock Medien', "StockImageToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-image"></i> AI Medien', "AiImageToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-code"></i> Button Shortcode', "ButtonShortcodeToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-code"></i> Product Shortcode', "ProductShortcodeToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-code"></i> Row-Col Shortcode', "RowColShortcodeToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-code"></i> Shortcodes', "ShortcodeToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-robot"></i> GPT-3', "Gpt3ToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-robot"></i> ChatGPT', "ChatGptToolbarAsyncHandler", []),
            new GlobalAsyncComponentButton('<i class="fas fa-robot"></i> AI Consultant Chat', "AiChatOverviewAsyncHandler", [])
        ];
        (new Text("Tools", "h3", ["centered"]))->display();
        (new BreakComponent())->display();
        (new FloatContainer($buttons))->display();
    }
}