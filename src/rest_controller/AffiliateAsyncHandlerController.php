<?php

namespace saschahuber\affiliatewebsitecore\rest_controller;

use saschahuber\affiliatewebsitecore\service\AffiliateAsyncHandlerService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\rest_controller\AsyncHandlerController;
use stdClass;

class AffiliateAsyncHandlerController extends AsyncHandlerController {
    /**
     * "/async_handler/handle" Endpoint
     */
    public function getAsyncContent($segments=null, $params=null){
        $data = new stdClass();

        $data->segments = $segments;
        $data->params = $params;

        $handler_type = ArrayHelper::getArrayValue($params, 'handler');

        if(!$handler_type){
            ErrorHelper::api(404, "Handler ist null");
        }

        $async_handler = new AffiliateAsyncHandlerService();

        $content = $async_handler->handle($handler_type, $params);

        $data->content = base64_encode($content);

        $this->sendOutput($content);
    }
}