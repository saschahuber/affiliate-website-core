<?php

namespace saschahuber\affiliatewebsitecore\news_import_templates;
use DateTime;
use stdClass;

abstract class RssFeedTemplate extends NewsFeedTemplate{
    function __construct($key, $title){
        parent::__construct($key, $title);
    }

    function getFeedItems($feed_content){
        $feed_items = [];
        // TODO: Implement getFeedItems() method.

        $channel = $feed_content['channel'];

        foreach($channel->item as $item){
            $feed_item = new stdClass();
            $feed_item->title = (string) $item->title;
            $feed_item->post_url = (string) $item->link;
            $feed_item->description = (string) $item->description;
            $feed_item->external_identifier = substr(base64_encode((string) $item->link), 0, 255);
            $feed_item->news_date = (DateTime::createFromFormat('D, j M Y H:i:s e', (string) $item->pubDate))->format('Y-m-d H:i:s');
            $feed_item->category = (array) $item->category;

            $feed_items[] = $feed_item;
        }

        return $feed_items;
    }
}