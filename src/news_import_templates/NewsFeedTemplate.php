<?php

namespace saschahuber\affiliatewebsitecore\news_import_templates;

use DOMDocument;
use Exception;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\service\text_generator\HtmlParagraphRewriterService;
use saschahuber\affiliatewebsitecore\service\text_generator\HtmlTextRewriterService;
use saschahuber\saastemplatecore\helper\UrlHelper;
use stdClass;

abstract class NewsFeedTemplate{
    private $key, $title;
    private NewsManager $news_manager;
    private HtmlParagraphRewriterService $html_paragraph_rewriter_service;
    private HtmlTextRewriterService $html_text_rewriter_service;

    function __construct($key, $title){
        $this->setKey($key);
        $this->setTitle($title);
        $this->news_manager = new NewsManager();
        $this->html_paragraph_rewriter_service = new HtmlParagraphRewriterService();
        $this->html_text_rewriter_service = new HtmlTextRewriterService();
    }

    private function loadFromUrl($url, $curl=false){
        $HTTP_CONTEXT = stream_context_create([
            'http' => [
                'method' => 'GET',
                'header' => [
                    'Accept-language: de',
                    'User-Agent: RSS-Reader'
                ]
            ]
        ]);

        if ($curl) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($ch);
            curl_close($ch);
        } else {
            $content = file_get_contents($url, false, $HTTP_CONTEXT);
        }

        return $content;
    }

    function getFeedContent($url, $curl=false){
        $xml = $this->loadFromUrl($url, $curl);

        return (array)simplexml_load_string($xml, null,
            LIBXML_NOCDATA | LIBXML_COMPACT | LIBXML_PARSEHUGE);
    }

    function getHtml($url, $curl=false){
        global $CONFIG;

        $html = $this->loadFromUrl($url, $curl);

        $html = $this->replaceOrAddAmazonPartnerID($html, $CONFIG->amazon_partner_id);
        $html = $this->addNofollowToLinks($html);

        $doc = new DOMDocument();
        $doc->registerNodeClass('DOMElement', 'saschahuber\affiliatewebsitecore\JSLikeHTMLElement');
        $doc->loadHTML($html);
        return $doc;
    }

    abstract function getFeedItems($feed_content);

    function importItem($feed_item){
        $item = new stdClass();
        $item->title = $feed_item->title;
        $item->permalink = UrlHelper::alias($item->title);
        $item->content = $feed_item->content;
        $item->news_date = date_format($feed_item->news_date, 'Y-m-d H:i:s');
        $item->external_identifier = $feed_item->external_identifier;
        $item->status = Manager::STATUS_DRAFT;
        $item->imported_feed_id = $feed_item->imported_feed_id;

        return $this->news_manager->createItem($item);
    }

    abstract function getPostContent($post_url, $rewrite=false);

    function getItemsFromFeed($news_feed_item, $rewrite=false){
        libxml_use_internal_errors(true);
        echo "Fetching feed-items from url '{$news_feed_item->feed_url}'".PHP_EOL;

        $items = [];

        $feed_content = $this->getFeedContent($news_feed_item->feed_url);

        $feed_items = $this->getFeedItems($feed_content, false);

        foreach($feed_items as $feed_item){
            if($this->news_manager->isAlreadyInImportedTable($feed_item->external_identifier)){
                echo "Already imported '{$feed_item->title}': ID: {$feed_item->external_identifier}" . PHP_EOL;
                continue;
            }

            echo "Loading post content for '$feed_item->post_url'".PHP_EOL;

            try {
                $feed_item->content = $this->getPostContent($feed_item->post_url, $rewrite);

                if($feed_item->content !== null) {
                    $feed_item->imported_feed_id = $news_feed_item->news__feed_import_id;
                    $items[] = $feed_item;
                }
            }
            catch(Exception $e){
                echo "Could not load content from '$feed_item->post_url'...".PHP_EOL;
            }
        }

        return $items;
    }

    function runImport($news_feed_item){
        libxml_use_internal_errors(true);
        echo "Importing from url '{$news_feed_item->feed_url}'".PHP_EOL;

        $imported_items = [];

        $feed_content = $this->getFeedContent($news_feed_item->feed_url);

        $feed_items = $this->getFeedItems($feed_content);

        foreach($feed_items as $feed_item){
            if($this->news_manager->isAlreadyImported($feed_item->external_identifier)){
                echo "Already imported '{$feed_item->title}': ID: {$feed_item->external_identifier}" . PHP_EOL;
                continue;
            }

            echo "Loading post content for '$feed_item->post_url'".PHP_EOL;
            $feed_item->content = $this->getPostContent($feed_item->post_url, true);

            $feed_item->imported_feed_id = $news_feed_item->news__feed_import_id;
            $imported_items[] = $this->importItem($feed_item);
        }

        return $imported_items;
    }

    function getFeedItemsPreview($news_feed_item){
        $news_manager = new NewsManager();

        libxml_use_internal_errors(true);
        echo "Importing from url '{$news_feed_item->feed_url}'".PHP_EOL;

        $preview_items = [];

        $feed_content = $this->getFeedContent($news_feed_item->feed_url);

        $feed_items = $this->getFeedItems($feed_content);

        foreach($feed_items as $feed_item){
            $imported_item = $news_manager->getByExternalIdentifier($feed_item->external_identifier);
            $feed_item->imported_id = null;
            if($imported_item){
                $feed_item->imported_id = $imported_item->news_id;
                $feed_item->imported_url = $news_manager->generatePermalink($imported_item);
            }

            echo "Loading post content for '$feed_item->post_url'".PHP_EOL;
            $feed_item->content = $this->getPostContent($feed_item->post_url);

            echo "Importing post '{$feed_item->title}'".PHP_EOL;
            $feed_item->imported_feed_id = $news_feed_item->news__feed_import_id;
            $preview_items[] = $feed_item;
        }

        return $preview_items;
    }

    function replaceOrAddAmazonPartnerID($html, $partnerID) {
        $dom = new DOMDocument();
        $dom->loadHTML($html);

        $links = $dom->getElementsByTagName('a');

        foreach ($links as $link) {
            $href = $link->getAttribute('href');
            if (strpos($href, 'amazon.') !== false) {
                if (strpos($href, 'tag=') !== false) {
                    // Replace the existing tag parameter with the new partner ID
                    $newHref = preg_replace('/tag=[^&]+/', 'tag=' . $partnerID, $href);
                } else {
                    // Add the new partner ID as a new tag parameter
                    $newHref = $href . (strpos($href, '?') !== false ? '&' : '?') . 'tag=' . $partnerID;
                }
                $link->setAttribute('href', $newHref);
            }
        }

        return $dom->saveHTML();
    }

    protected function addNofollowToLinks($html) {
        // HTML-Dokument parsen
        $dom = new DOMDocument();
        @$dom->loadHTML($html);

        // Alle Links im Dokument auswählen
        $links = $dom->getElementsByTagName('a');

        // Rel-Attribut "nofollow" zu allen Links hinzufügen
        foreach ($links as $link) {
            $rel = $link->getAttribute('rel');
            if ($rel) {
                // Wenn bereits ein rel-Attribut vorhanden ist, "nofollow" hinzufügen
                $rel .= ' nofollow';
            } else {
                // Ansonsten neues rel-Attribut mit "nofollow" erstellen
                $rel = 'nofollow';
            }
            $link->setAttribute('rel', $rel);
        }

        // HTML-Code des Dokuments zurückgeben
        return $dom->saveHTML();
    }

    function getRewrittenContentNode(&$html, &$content_node){
        $ignore_tags = ['img', 'iframe'];

        foreach($content_node->getElementsByTagName('p') as $element){
            $html_content = $html->saveHTML($element);

            foreach($ignore_tags as $ignore_tag) {
                if (strpos($html_content, '<' . $ignore_tag) !== false) {
                    continue 2;
                }
            }

            $html_content = $this->getElementContent($html_content);

            $rewritten_content = $this->html_paragraph_rewriter_service->rewriteParagraph($html_content);

            #echo $rewritten_content . PHP_EOL;

            $element->innerHTML = $rewritten_content;
        }
    }

    /*
    function getRewrittenContentNode(&$html, &$content_node){
        $ignore_tags = ['img', 'iframe'];

        $text_parts = [];

        foreach($content_node->getElementsByTagName('p') as $element){
            $html_content = $html->saveHTML($element);

            foreach($ignore_tags as $ignore_tag) {
                if (strpos($html_content, '<' . $ignore_tag) !== false) {
                    continue 2;
                }
            }

            $html_content = $this->getElementContent($html_content);

            $text_parts[] = $html_content;
        }

        $rewritten_content = $this->html_text_rewriter_service->rewriteText(implode(' ', $text_parts));

        $content_node->innerHTML = $rewritten_content;
    }
    */

    function setInnerHTML($DOM, $element, $content) {
        // Erstelle ein neues DOMDocumentFragment-Objekt mit dem neuen HTML-Inhalt
        $neuerInhalt = $DOM->createDocumentFragment();
        $neuerInhalt->appendXML($content);

        $element->nodeValue = '';
        $element->appendChild($neuerInhalt);
    }

    function getElementContent($element){
        preg_match("/<p[^>]*>(.*?)<\\/p>/si", $element, $match);
        return $match[1];
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key): void
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }
}