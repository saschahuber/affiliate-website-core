<?php

namespace saschahuber\affiliatewebsitecore\manager;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;

class ProductCustomDataFieldManager extends Manager
{
    const TABLE_NAME = "product__custom_data_field";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function cleanItem($item)
    {
        return $item;
    }

    public function createDataField($product_id, $name, $value, $sort_order)
    {
        $data = new stdClass();
        $data->product_id = $product_id;
        $data->name = $name;
        $data->value = $value;
        $data->sort_order = $sort_order;
        $this->DB->insertFromObject('product__custom_data_field', $data);
        return $this->DB->insert_id;
    }

    public function getDataFields($product = null)
    {
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);
        $query_builder->conditions(['product_id = ' . intval($product->id)])
            ->order('sort_order ASC');

        $data_fields = [];
        foreach ($this->DB->getAll($query_builder->buildQuery()) as $data_field) {
            $data_fields[] = $data_field;
        }

        return $data_fields;
    }
}