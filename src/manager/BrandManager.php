<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\service\BlogThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\BrandThumbnailGeneratorService;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\Helper;
use stdClass;

#[AllowDynamicProperties]
class BrandManager extends PostableManager
{
    const TABLE_NAME = "brand";
    const URL_PREFIX = "hersteller";

    const TYPE_BRAND = "brand";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME, self::URL_PREFIX);

        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
    }

    public function generatePermalink($item)
    {
        #Pfad-Cachen für Produkt-ID
        return Helper::ifstr($this->url_prefix !== null, '/' . $this->url_prefix) . '/' . $item->permalink;
    }

    public function getAll($status = null, $add_meta = true, $order_by = null, $desc = true, $limit = null, $offset = 0, $remove_hidden = false)
    {
        $items = array();

        $query_builder = new DatabaseSelectQueryBuilder($this->base_table_name);
        $query_builder->selects([
            '*',
            '(' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published',
            'DATE_FORMAT(' . $this->base_table_name . '_date, "%d. %M %Y") as formatted_date '
        ]);

        if ($status !== null || $remove_hidden) {

            $conditions = [];

            if ($status) {
                $conditions[] = 'status = "' . $this->DB->escape($status) . '"';
            }

            if ($remove_hidden) {
                $conditions[] = 'visible_on_brand_page = 1';
            }

            $query_builder->conditions($conditions);
        }

        if ($order_by !== null) {
            $query_builder->order($this->DB->escape($order_by) . " " . ($desc ? "desc" : "asc"));
        }

        if ($limit) {
            $query_builder->limit($limit);
        }

        #if($offset){
        #    $query_builder->offset($offset);
        #}

        $querystring = $query_builder->buildQuery();

        #die($querystring);

        $dbquery = $this->DB->query($querystring);

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
        }

        return $this->getAllWithMeta($items);
    }

    public function addMeta($item, $meta = null, $taxonomies = null)
    {
        if ($item !== false) {
            if ($meta === null) {
                $meta = [];
                $querystring = 'SELECT * FROM ' . $this->base_table_name . '__meta 
                where ' . $this->base_table_name . '_id = ' . $this->DB->escape($item->id);
                $meta_items = $this->DB->getAll($querystring, true, 60);
                foreach ($meta_items as $meta_item) {
                    $meta[$meta_item->meta_key] = $meta_item->meta_value;
                }
            }
            $item->meta = $meta;
        }
    }

    public function getBrandsByIds($brand_ids)
    {
        if (count($brand_ids) < 1) {
            return [];
        }

        $brands = [];
        $dbquery = $this->DB->query('SELECT * FROM brand where brand_id in (' . $this->DB->escape(implode(',', $brand_ids)) . ')');
        while ($brand = $dbquery->fetchObject()) {
            $brands[] = $brand;
        }
        return $brands;
    }

    public function getBrandsByProductId($product_id, $status = Manager::STATUS_PUBLISH)
    {
        $query_builder = new DatabaseSelectQueryBuilder('brand');
        $query_builder->join('brand__product_mapping', 'brand__product_mapping.brand_id', 'brand.brand_id');

        $conditions = ['brand__product_mapping.product_id = ' . intval($product_id)];

        if ($status) {
            $conditions[] = 'brand.status = "' . $this->DB->escape($status) . '"';
        }

        $query_builder->conditions([$conditions]);

        return $this->DB->getAll($query_builder->buildQuery(), true, 60);
    }

    public function getTaxonomiesByBrand($brand_id)
    {
        $taxonomies_to_ignore = [8, 9];

        $dbquery = $this->DB->query('SELECT distinct(product__taxonomy_mapping.taxonomy_id) as id FROM product__taxonomy_mapping
            JOIN brand__product_mapping on brand__product_mapping.product_id 
                = product__taxonomy_mapping.product_id
            JOIN product on product__taxonomy_mapping.product_id = product.product_id
            WHERE brand__product_mapping.brand_id = ' . intval($brand_id) . ' 
                and brand__product_mapping.brand_id not in (' . implode(',', $taxonomies_to_ignore) . ') 
                and product.status = "publish"');

        $tax_ids = [];
        while ($tax_id = $dbquery->fetchObject()) {
            $tax_ids[] = $tax_id->id;
        }
        return $this->product_manager->getTaxonomiesByIds($tax_ids);
    }

    public function getBrandProductCount()
    {
        $product_counts = array();
        $dbquery = $this->DB->query("SELECT brand.brand_id as id, anzahl FROM brand
            LEFT JOIN (SELECT brand__product_mapping.brand_id, count(*) as anzahl 
                FROM brand__product_mapping group by brand__product_mapping.brand_id) a 
                on a.brand_id = brand.brand_id");
        while ($item = $dbquery->fetchObject()) {
            $product_counts[$item->id] = $item->anzahl;
        }
        return $product_counts;
    }

    public function getAllWithProductNum($status = null, $add_meta = true, $order_by = null, $desc = true)
    {
        $items = array();

        $join_clause = ' LEFT JOIN (
        SELECT brand_id, count(*) as anzahl 
        FROM brand__product_mapping group by brand_id) 
        as produkte_anzahl_mapping on brand.brand_id 
        = produkte_anzahl_mapping.brand_id
        ORDER BY title asc';

        $filter_clause = false;
        if ($status !== null) {
            $filter_clause = ' where status = "' . $this->DB->escape($status) . '"';
        }

        $order_clause = false;
        if ($order_by !== null) {
            $order_clause = ' ORDER BY ' . $this->DB->escape($order_by) . " " . ($desc ? "desc" : "asc");
        }

        $dbquery = $this->DB->query('SELECT brand.*, produkte_anzahl_mapping.anzahl as produkt_anzahl,
            DATE_FORMAT(brand_date, "%d. %M %Y") as formatted_date 
            FROM ' . $this->base_table_name . ($join_clause ?: "") . ($filter_clause ?: "") . ($order_clause ?: ""));

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
        }

        return $this->getAllWithMeta($items);
    }

    public function setProductBrands($item, $brands)
    {
        $item_brand_ids = array();
        $new_brand_ids = array();

        foreach ($this->getBrandsByProductId($item->id, null) as $brand) {
            $item_brand_ids[] = $brand->id;
        }

        foreach ($brands as $brand) {
            $new_brand_ids[] = $brand->id;
        }

        $brands_to_add = array();
        $brands_to_remove = array();


        foreach ($new_brand_ids as $brand) {
            if (!in_array($brand, $item_brand_ids)) {
                $brands_to_add[] = $brand;
            }
        }

        foreach ($item_brand_ids as $brand) {
            if (!in_array($brand, $new_brand_ids)) {
                $brands_to_remove[] = $brand;
            }
        }

        $this->removeProductBrands($item, $brands_to_remove);
        $this->addProductBrands($item, $brands_to_add);
    }

    public function addProductBrands($item, $brand_ids)
    {
        foreach ($brand_ids as $brand_id) {
            $brand_mapping = new stdClass();
            $brand_mapping->product_id = $item->id;
            $brand_mapping->brand_id = $brand_id;
            $this->DB->insertFromObject($this->base_table_name . '__product_mapping', $brand_mapping);
        }
    }

    public function removeProductBrands($item, $brand_ids)
    {
        if (count($brand_ids) < 1) {
            return;
        }

        $this->DB->query('DELETE FROM ' . $this->base_table_name . '__product_mapping 
            where product_id = ' . $item->id . ' 
            AND brand_id IN (' . implode(', ', $brand_ids) . ')');
    }

    public function getThumbnailSrc($item)
    {
        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        } else {
            $thumbnail_src = (new BrandThumbnailGeneratorService())->getThumbnailSrc($item);
        }
        return $thumbnail_src;
    }
}