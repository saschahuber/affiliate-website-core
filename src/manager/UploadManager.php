<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;

#[AllowDynamicProperties]
class UploadManager extends Manager
{
    const BASE_TABLE_NAME = "upload";
    const UPLOADS_DIR = "uploads";

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    public function getUpload($upload_id)
    {
        return $this->DB->query("SELECT * FROM upload where upload_id = " . intval($upload_id))->fetchObject();
    }

    public function getUploadByFilepath($filepath)
    {
        return $this->DB->query("SELECT * FROM upload where file_path LIKE '" . $this->DB->escape($filepath) . "'")->fetchObject();
    }

    public function updateUpload($upload_id, $description, $provider_name, $copyright_info, $title, $alt_text)
    {
        $this->DB->query("UPDATE upload set title = '" . $this->DB->escape($title) . "',
                            alt_text = '" . $this->DB->escape($alt_text) . "',
                            description = '" . $this->DB->escape($description) . "',
                            provider_name = '" . $this->DB->escape($provider_name) . "',
                            copyright_info = '" . $this->DB->escape($copyright_info) . "'
            where upload_id = " . intval($upload_id));
    }

    public function addUploadFromUrl($url, $title = null, $alt_text = null)
    {
        $file_path = $this->uploadFromUrl($url);
        $file_name = pathinfo($url)['basename'];
        $file_extension = pathinfo($url)['extension'];

        $title = $title ? '"' . $this->DB->escape($title) . '"' : 'null';
        $alt_text = $alt_text ? '"' . $this->DB->escape($alt_text) . '"' : 'null';
        $file_path = $file_path ? '"' . $this->DB->escape($file_path) . '"' : 'null';
        $file_name = $file_name ? '"' . $this->DB->escape($file_name) . '"' : 'null';
        $file_extension = $file_name ? '"' . $this->DB->escape($file_extension) . '"' : 'null';

        $this->DB->query('INSERT INTO upload (title, alt_text, file_path, file_name, file_extension) 
            VALUES (
                ' . $title . ',
                ' . $alt_text . ',
                ' . $file_path . ',
                ' . $file_name . ',
                ' . $file_extension . '
            )');

        return $this->DB->insert_id;
    }

    public function addUploadFromFile($file, $description = null, $provider_name = null, $copyright_info = null, $title = null, $alt_text = null, $use_in_upload_generator = false)
    {
        $file_path = $this->uploadFile($file);
        $file_name = pathinfo($file_path)['basename'];
        $file_extension = pathinfo($file_path)['extension'];

        $new_file_path = "/uploads/$file_extension/$file_name";
        FileHelper::createDirIfNotExists(dirname($new_file_path));
        rename(MEDIA_DIR . $file_path, MEDIA_DIR . $new_file_path);

        if ($file_path) {
            $this->DB->query('INSERT INTO upload (title, alt_text, file_path, file_name, file_extension, description, provider_name, copyright_info) 
            VALUES (
                "' . $this->DB->escape($title) . '",
                "' . $this->DB->escape($alt_text) . '",
                "' . $this->DB->escape($new_file_path) . '",
                "' . $this->DB->escape($file_name) . '",
                "' . $this->DB->escape($file_extension) . '",
                "' . $this->DB->escape($description) . '",
                "' . $this->DB->escape($provider_name) . '",
                "' . $this->DB->escape($copyright_info) . '"
            )');
            return $this->DB->insert_id;
        } else {
            die("Konnte Datei nicht hochladen: " . json_encode($file));
            return false;
        }
    }

    public function uploadFile($file)
    {
        if (!empty($file['name'])) {
            $media_dir = DATA_DIR . '/media';

            $file_info = pathinfo($file['name']);

            $filename = UrlHelper::alias($file_info['filename']) . '.' . $file_info['extension'];

            $upload_path = '/' . self::UPLOADS_DIR . '/' . $file_info['extension'] . '/' . $filename;

            if (!file_exists($media_dir . $upload_path)) {
                FileHelper::createDirIfNotExists($media_dir . '/' . self::UPLOADS_DIR . '/' . $file_info['extension']);
                move_uploaded_file($file['tmp_name'], $media_dir . $upload_path);
                return $upload_path;
            }
        }

        die("Konnte Datei nicht hochladen: " . $upload_path);

        return false;
    }

    public function uploadFromUrl($url, $filename = null)
    {
        $path_parts = pathinfo($url);
        if ($filename === null) {
            $filename = $path_parts['basename'];
        }
        $extension = $path_parts['extension'];

        $file_path = '/' . self::UPLOADS_DIR . '/' . $extension . '/' . $filename;

        $ch = curl_init($url);
        $fp = fopen(MEDIA_DIR . $file_path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        return $file_path;
    }

    public function getUploadUrl($upload_id, $default_image = null)
    {
        if ($upload_id === null) {
            return $default_image;
        }

        $item = $this->DB->getOne("SELECT file_path FROM upload where upload_id = " . intval($upload_id), true, 60);

        if ($item) {
            return $this->createUploadSrcFromFilePath($item->file_path);
        }

        return null;
    }

    public function getUploads($keyword = null)
    {
        $uploads = array();

        $query_builder = new DatabaseSelectQueryBuilder("upload");

        if ($keyword) {
            $query_builder->conditions([
                'title LIKE "%' . $this->DB->escape($keyword) . '%"',
                'alt_text LIKE "%' . $this->DB->escape($keyword) . '%"',
                'file_name LIKE "%' . $this->DB->escape($keyword) . '%"',
                'description LIKE "%' . $this->DB->escape($keyword) . '%"'
            ], 'or');
        }

        $query_builder->orders([
            "upload_time DESC",
            "upload_id DESC"
        ]);

        $querystring = $query_builder->buildQuery();

        $dbquery = $this->DB->query($querystring);
        while ($item = $dbquery->fetchObject()) {
            $item->src = $this->createUploadSrcFromFilePath($item->file_path);
            $uploads[] = $item;
        }

        return $uploads;
    }

    public function createUploadSrcFromFilePath($file_path)
    {
        return '/data/media' . $file_path;
    }
}