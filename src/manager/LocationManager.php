<?php

namespace saschahuber\affiliatewebsitecore\manager;

use saschahuber\saastemplatecore\helper\Helper;

class LocationManager extends PostableManager{
    const TABLE_NAME = "location";
    const URL_PREFIX = "in";

    public function __construct(){
        parent::__construct(self::TABLE_NAME, self::URL_PREFIX);
    }

    public function displayLocations($locations, $limit=4, $layout="grid", $show_detail_button=false, $show_meta=false){
        global $LAYOUT_MANAGER;

        $contents = [];

        foreach($locations as $location){
            $contents[] = $this->displayLocation($location);
        }

        return $LAYOUT_MANAGER->grid($contents, 4, "lg");
    }

    public function addMeta($item, $meta=null, $taxonomies=null){
        if($item !== false){
            if($meta === null) {
                $meta = [];
                $querystring = 'SELECT * FROM ' . $this->base_table_name . '__meta 
                where ' . $this->base_table_name . '_id = ' . $this->DB->escape($item->id);
                $meta_items = $this->DB->getAll($querystring, true, 60);
                foreach ($meta_items as $meta_item) {
                    $meta[$meta_item->meta_key] = $meta_item->meta_value;
                }
            }
            $item->meta = $meta;
        }
    }

    public function generatePermalink($item){
        #Pfad-Cachen für Produkt-ID

        $sub_path = $item->permalink;

        $parent = $item;
        while($parent->parent_location_id !== null){
            $parent = $this->getById($parent->parent_location_id, false);
            $sub_path = $parent->permalink . '/' . $sub_path;
        }

        return Helper::ifstr($this->url_prefix!==null, '/'.$this->url_prefix).'/'.$sub_path;
    }

    public function displayLocation($location){
        $location_link = $location->amazon_link;
        $open_external = true;

        if($location->is_fake !== 1){
            $open_external = false;
            $location_link = $this->generatePermalink($location);
        }

        ob_start();
        ?>
        <div class="location-item text-center">
            <a title="<?=$location->title?>"
               href="<?=$location_link?>"
                <?=Helper::ifstr($open_external, ' target="_blank"')?> rel="nofollow">
                <img class="lazy loaded" src="<?=$location->meta['location_primary_image']?>"
                     data-src="<?=$location->meta['location_primary_image']?>"
                     alt="<?=$location->title?>"
                     width="100%"
                     data-was-processed="true"></a>
            <span><?=$location->title?></span>
            <a href="<?=$location->amazon_link?>"
               title="<?=$location->title?>"
               class="btn btn-buy btn-block" rel="nofollow" target="_blank"><i class="fa fa-amazon"></i> Preis prüfen</a>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function generateSpinningContent($location){
        return "Content: ".$location->content;
    }

    private function buildDistanceSql($latitude, $longitude){
        return 'COALESCE(6371 * Acos(
                    Cos(Radians(' . str_replace(',', '.', $latitude) . '))
                    * Cos(Radians(location.latitude)) * Cos(Radians(location.longitude) - Radians(' . str_replace(',', '.', $longitude) . '))
                    + Sin(Radians(' . str_replace(',', '.', $latitude) . ')) * Sin(Radians(location.latitude))), 0
                )';
    }

    public function findNearLocations($latitude, $longitude, $max_distance, $max_items=25, $exclude_id=null){
        $items = array();

        $dbquery = $this->DB->query('SELECT *, '.$this->buildDistanceSql($latitude, $longitude).' as distance
                FROM location 
                where status = "publish"
                and location.latitude is not null
                and location.longitude is not null
                and ('.$this->buildDistanceSql($latitude, $longitude).') <= '.doubleval($max_distance).
            (Helper::ifstr($exclude_id !== null, ' and location_id != '.intval($exclude_id)))
            .' ORDER BY distance ASC
                LIMIT '.intval($max_items));

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getStates(){
        $items = array();

        $dbquery = $this->DB->query('SELECT * FROM location 
                where status = "publish"
                and parent_location_id in (1)
                ORDER BY title ASC');

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getFeatured(){
        $items = array();

        $dbquery = $this->DB->query('SELECT * FROM location 
                where status = "publish"
                and featured = true
                ORDER BY title ASC');

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getFiltered($keyword=null, $status=null, $taxonomies=null, $order_by=null, $desc=true, $limit=null, $offset=0){
        $items = array();

        $filter_clauses = [];

        if($keyword){
            $filter_clauses[] = 'title LIKE "%'.$this->DB->escape($keyword).'%"';
        }

        if($status){
            $filter_clauses[] = 'status = "'.$this->DB->escape($status).'"';
        }

        $order_clause = false;
        if($order_by !== null){
            $order_clause = ' ORDER BY '.$this->DB->escape($order_by)." ".($desc?"desc":"asc");
        }

        $querystring = 'SELECT '.$this->base_table_name.'.*, 
            DATE_FORMAT('.$this->base_table_name.'_date, "%d. %M %Y") as formatted_date 
            FROM '.$this->base_table_name
            .((count($filter_clauses)>0)?" WHERE ".implode(' AND ', $filter_clauses):"")
            . ' GROUP BY '.$this->base_table_name.'.'.$this->base_table_name.'_id'
            .($order_clause?:"");

        if($limit){
            $querystring .= " LIMIT " . intval($limit);
        }

        if($offset){
            $querystring .= " OFFSET " . intval($offset);
        }

        $dbquery = $this->DB->query($querystring);

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
        }

        return $this->getAllWithMeta($items);
    }
}