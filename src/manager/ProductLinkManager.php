<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use stdClass;

#[AllowDynamicProperties]
class ProductLinkManager extends Manager
{
    const BASE_TABLE_NAME = 'product__link';

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    public function cleanItem($item)
    {
        return $item;
    }

    function getAll()
    {
        $product_feeds = [];

        $dbquery = $this->DB->query("SELECT * FROM " . self::BASE_TABLE_NAME);
        while ($product_feed = $dbquery->fetchObject()) {
            $product_feeds[] = $product_feed;
        }

        return $product_feeds;
    }

    function getAllByProductId($product_id, $hide_inactive=true){
        $conditions = [
            "product_id = " . intval($product_id)
        ];

        if($hide_inactive){
            $conditions[] = 'product__link.is_active = 1';
        }

        $query_builder = new DatabaseSelectQueryBuilder($this->base_table_name);
        $querystring = $query_builder
            ->selects([
                $this->base_table_name . ".*",
                'product__shop.sort_order as shop_sort_order',
                'product__shop.product__shop_id as product__shop_id',
                'product__feed.product__feed_id as product__feed_id',
                'product__shop.title as product_shop_title',
                'product__shop.button_text as product_shop_button_text',
                'product__feed.title as product_feed_title'
            ])
            ->join("product__shop", "product__shop.product__shop_id", "product__link.product__shop_id", 'LEFT')
            ->join("product__feed", "product__feed.product__feed_id", "product__link.product__feed_id", 'LEFT')
            ->conditions($conditions)
            ->orders(['product__link.sort_order ASC', 'product__link.product__shop_id is not NULL DESC', 'product__shop.sort_order ASC'])
            ->buildQuery();

        return $this->DB->getAll($querystring, true, 30);
    }

    function getImportedEans($shop_id)
    {
        $product_feeds = [];

        $querystring = "SELECT external_id, TRIM(LEADING '0' FROM ean) as ean, product.product_id FROM product
            JOIN " . self::BASE_TABLE_NAME . " on product.product_id =  " . self::BASE_TABLE_NAME . ".product_id
            WHERE ean != '' and product__shop_id = " . intval($shop_id);

        $dbquery = $this->DB->query($querystring);
        while ($product_feed = $dbquery->fetchObject()) {
            $product_feeds[$product_feed->ean] = $product_feed->product_id;
        }

        return $product_feeds;
    }

    function saveProductLink($product_id, $product__shop_id, $product__feed_id, $price, $reduced_price, $external_id, $url, $is_available, $product_link_id = null)
    {
        $item = new stdClass();
        $item->price = $price;
        $item->reduced_price = $reduced_price;
        $item->external_id = $external_id;
        $item->url = $url;
        $item->is_available = $is_available;

        if ($product_link_id) {
            $item->id = $product_link_id;
            $item->product__link_id = $product_link_id;
            return $this->updateItem($item);
        } else {
            $item->product_id = $product_id;
            $item->product__shop_id = $product__shop_id;
            $item->product__feed_id = $product__feed_id;
            return $this->createItem($item);
        }
    }
}