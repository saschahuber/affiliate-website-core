<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;

#[AllowDynamicProperties]
class ProductFeedManager extends Manager
{
    const BASE_TABLE_NAME = 'product__feed';

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    public function cleanItem($item)
    {
        return $item;
    }

    function getAll()
    {
        $product_feeds = [];

        $dbquery = $this->DB->query("SELECT * FROM " . self::BASE_TABLE_NAME);
        while ($product_feed = $dbquery->fetchObject()) {
            $product_feeds[] = $product_feed;
        }

        return $product_feeds;
    }
}