<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\LogHelper;

#[AllowDynamicProperties]
class FileManager
{
    const DEFAULT_FILE_PATH = TMP_DIR . '/misc';

    public function __construct(){}

    public function downloadFileFromUrl($url, $path_to_store = self::DEFAULT_FILE_PATH, $filename = null)
    {
        if ($filename === null) {
            $path_parts = pathinfo($url);
            $filename = $path_parts['basename'];
        }

        $file_path = $path_to_store . '/' . $filename;

        FileHelper::createDirIfNotExists($path_to_store);

        $ch = curl_init($url);
        $fp = fopen($file_path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);

        $http_status_code = curl_getinfo($ch)['http_code'];

        curl_close($ch);
        fclose($fp);

        if($http_status_code >= 400){
            LogHelper::logToMinuteLog('download', "Could not download file from '$url' and store to '$filename' (Error: $http_status_code)");
            unlink($file_path);
            return null;
        }

        return $file_path;
    }

    public function downloadTmpFile($url, $tmp_dir = false, $filename = null)
    {
        if ($tmp_dir == false) {
            $tmp_dir = self::DEFAULT_FILE_PATH;
        } else {
            $tmp_dir = TMP_DIR . '/' . $tmp_dir;
        }

        return $this->downloadFileFromUrl($url, $tmp_dir, $filename);
    }

    public function downloadTmpImage($url, $filename = null)
    {
        return $this->downloadTmpFile($url, 'img', $filename);
    }
}