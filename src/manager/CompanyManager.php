<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\component\CompanyList;
use saschahuber\affiliatewebsitecore\component\item\CompanyGridItem;
use saschahuber\affiliatewebsitecore\component\scheme\SchemeOrgCompany;
use saschahuber\affiliatewebsitecore\service\CompanyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\CompanyTrackingService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\RequestHelper;
use stdClass;

#[AllowDynamicProperties]
class CompanyManager extends PostableManager{
    const PACKAGE_FREE = "PACKAGE_FREE";
    const PACKAGE_BASIS = "PACKAGE_BASIS";
    const PACKAGE_PRO = "PACKAGE_PRO";
    const PACKAGE_PREMIUM = "PACKAGE_PREMIUM";

    const TABLE_NAME = "company";
    const URL_PREFIX = "dienstleister";

    const TYPE_COMPANY = 'company';
    const TYPE_COMPANY_CATEGORY = 'company__category';

    public function __construct(){
        parent::__construct(self::TABLE_NAME, self::URL_PREFIX);
    }

    public function displayTaxonomies($taxonomies){
        #   return (new PostGridContainer($taxonomies))->getContent();
    }

    public function displayCompanies($companies){
        return (new CompanyList($companies))->getContent();
    }

    public function getThumbnailSrc($item)
    {
        global $CONFIG;

        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        } else {
            $thumbnail_src = (new CompanyThumbnailGeneratorService())->getThumbnailSrc($item);
        }
        return $thumbnail_src;
    }

    public function getThumbnail($item){
        return ImgUtils::getImageTag($this->getThumbnailSrc($item));
    }

    public function getNewCompanies($since_date, $limit=4){
        $items = array();

        $dbquery = $this->DB->query('SELECT * FROM company 
            where status = "publish" and company_date >= "'.$this->DB->escape($since_date).'" and company_date <= CURRENT_TIMESTAMP()
            ORDER BY company_date DESC LIMIT '.intval($limit));

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getByOldIds($ids, $add_meta=true, $orderby=null){
        if($ids === null || count($ids) < 1){
            return [];
        }

        $in_ids_string = implode(', ', $ids);
        if(strlen($in_ids_string) < 1){
            return [];
        }

        $querystring = 'SELECT * FROM company 
            where status = "publish" and company_id_old IN (' . $in_ids_string . ')';

        $items = array();

        $dbquery = $this->DB->query($querystring);

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getFeatured(){
        $querystring = 'SELECT * FROM company 
            where status = "publish" 
            and is_featured = 1
            order by company_date DESC';

        $items = array();

        $dbquery = $this->DB->query($querystring);

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getNewest($num=12){
        $querystring = 'SELECT * FROM company 
            where status = "publish"
            order by company_date DESC LIMIT '.intval($num);

        $items = array();

        $dbquery = $this->DB->query($querystring);

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function addMeta($item, $meta = null, $taxonomies = null){
        parent::addMeta($item, $meta, $taxonomies);

        $item->full_permalink = $this->generatePermalink($item);
        $item->thumbnail_src = $this->getThumbnailSrc($item);
        $item->excerpt = $this->getExcerpt($item->content, true, 300);
    }

    private function buildDistanceSql($latitude, $longitude){
        return 'COALESCE(6371 * Acos(
                    Cos(Radians(' . str_replace(',', '.', $latitude) . '))
                    * Cos(Radians(company.latitude)) * Cos(Radians(company.longitude) - Radians(' . str_replace(',', '.', $longitude) . '))
                    + Sin(Radians(' . str_replace(',', '.', $latitude) . ')) * Sin(Radians(company.latitude))), 0
                )';
    }

    public function findNearCompanies($latitude, $longitude, $max_distance, $max_items=25){
        $items = array();

        $querystring = 'SELECT *, '.$this->buildDistanceSql($latitude, $longitude).' as distance
            FROM company
            where status = "publish"
            and company.latitude is not null
            and company.longitude is not null
            and ('.$this->buildDistanceSql($latitude, $longitude).') <= '.doubleval($max_distance).'
            ORDER BY company_date DESC
            LIMIT ' . intval($max_items);

        $dbquery = $this->DB->query($querystring);

        while($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public static function getPhoneTrackingUrl($company_id, $with_domain=false){
        global $CONFIG;
        return Helper::ifstr($with_domain, $CONFIG->api_domain.'/')."company_tracking/".CompanyTrackingService::TRACKING_TYPE_CLICK_PHONE."/$company_id";
    }

    public static function getMailTrackingUrl($company_id, $with_domain=false){
        global $CONFIG;
        return Helper::ifstr($with_domain, $CONFIG->api_domain.'/')."company_tracking/".CompanyTrackingService::TRACKING_TYPE_CLICK_MAIL."/$company_id";
    }

    public static function getWebsiteTrackingUrl($company_id, $with_domain=false){
        global $CONFIG;
        return Helper::ifstr($with_domain, $CONFIG->api_domain.'/')."company_tracking/".CompanyTrackingService::TRACKING_TYPE_CLICK_WEBSITE."/$company_id";
    }

    static function getClickableAndTrackablePhoneNumber($company){
        $tracking_url = CompanyManager::getPhoneTrackingUrl($company->company_id);

        return BufferHelper::buffered(function () use ($tracking_url, $company) {
            ?>
            <a href="tel:<?=$company->phone_number?>" onclick="doApiCall('<?=$tracking_url?>')"><?=$company->phone_number?></a>
            <?php
        });
    }

    static function getClickableAndTrackableEmail($company){
        $tracking_url = CompanyManager::getMailTrackingUrl($company->company_id);

        return BufferHelper::buffered(function () use ($tracking_url, $company) {
            ?>
            <a href="mailto:<?=$company->email?>" onclick="doApiCall('<?=$tracking_url?>')"><?=$company->email?></a>
            <?php
        });
    }

    static function getClickableAndTrackableWebsite($company, $link_text=null){
        $tracking_url = CompanyManager::getWebsiteTrackingUrl($company->company_id);

        return BufferHelper::buffered(function () use ($link_text, $tracking_url, $company) {
            $rel = "nofollow noreferrer noopener";

            if(CompanyManager::hasDofollowLink($company)){
                $rel = "dofollow noreferrer noopener";
            }

            // Check if the URL starts with http:// or https://
            $url = $company->website;
            if (!preg_match('/^https?:\/\//i', $url)) {
                $url = 'https://' . $url;
            }

            ?>
            <a href="<?=$url?>" target="_blank" rel="<?=$rel?>" onclick="doApiCall('<?=$tracking_url?>')"><?=$link_text?:$company->website?></a>
            <?php
        });
    }

    public function prepareContent($company){
        // Telefnonnummern, Mail-Adressen und Website klickbar machen => Website auf nofollow
        $content = $company->content;
        $content = str_replace($company->phone_number, self::getClickableAndTrackablePhoneNumber($company), $content);
        $content = str_replace($company->email, self::getClickableAndTrackableEmail($company), $content);
        $content = str_replace($company->website, self::getClickableAndTrackableWebsite($company), $content);
        $content = str_replace("Website", self::getClickableAndTrackableWebsite($company, "Website"), $content);
        return $content;
    }

    public function getOpeningHours($company){
        $opening_hours = [
                'monday' => null,
                'tuesday' => null,
                'wednesday' => null,
                'thursday' => null,
                'friday' => null,
                'saturday' => null,
                'sunday' => null,
        ];

        if($company){
            $dbquery = $this->DB->query("SELECT * FROM company__opening_hours where company_id = " . intval($company->company_id));

            while($opening_hours_item = $dbquery->fetchObject()){
                $opening_hours[$opening_hours_item->weekday] = [
                        'is_open' => $opening_hours_item->is_open,
                        'open1' => $opening_hours_item->open1,
                        'close1' => $opening_hours_item->close1,
                        'open2' => $opening_hours_item->open2,
                        'close2' => $opening_hours_item->close2
                ];
            }
        }

        return $opening_hours;
    }

    public function getFormattedOpeningHours($company){
        $weekday_translations = [
            'monday' => 'Montag',
            'tuesday' => 'Dienstag',
            'wednesday' => 'Mittwoch',
            'thursday' => 'Donnerstag',
            'friday' => 'Freitag',
            'saturday' => 'Samstag',
            'sunday' => 'Sonntag',
        ];

        $formatted_opening_hours = [
            'monday' => null,
            'tuesday' => null,
            'wednesday' => null,
            'thursday' => null,
            'friday' => null,
            'saturday' => null,
            'sunday' => null,
        ];

        foreach($this->getOpeningHours($company) as $weekday => $opening_hours){
            $formatted_opening_hours[$weekday] = [
                    'weekday' => $weekday_translations[$weekday],
                    'is_open' => false,
                    'open1' => null,
                    'close1' => null,
                    'open2' => null,
                    'close2' => null,
                    'text' => "Geschlossen"
            ];
            if($opening_hours) {
                if($opening_hours['is_open'] && $opening_hours['open1'] && $opening_hours['close1']) {
                    $text = substr($opening_hours['open1'], 0, 5) . ' - ' . substr($opening_hours['close1'], 0, 5) . ' Uhr';

                    if($opening_hours['open2'] && $opening_hours['close2']) {
                        $text .= ' & ' . substr($opening_hours['open2'], 0, 5) . ' - ' . substr($opening_hours['close2'], 0, 5) . ' Uhr';
                    }
                }
                else{
                    $text = "Geschlossen";
                }
                $formatted_opening_hours[$weekday] = [
                    'weekday' => $weekday_translations[$weekday],
                    'is_open' => $opening_hours['is_open'],
                    'open1' => $opening_hours['open1'],
                    'close1' => $opening_hours['close1'],
                    'open2' => $opening_hours['open2'],
                    'close2' => $opening_hours['close2'],
                    'text' => $text
                ];
            }
        }

        return $formatted_opening_hours;
    }

    function updateOpeningHours($company_id, $opening_hours){
        foreach($opening_hours as $weekday => $day_opening_hours) {
            $this->updateOpeningHoursForDay($weekday, $company_id, $day_opening_hours);
        }
    }

    function updateOpeningHoursForDay($weekday, $company_id, $opening_hours){
        if(!$opening_hours){
            return;
        }

        $opening_hours_for_day = $this->DB->query("SELECT * FROM company__opening_hours 
         where weekday = '".$this->DB->escape($weekday)."' 
         and company_id = " . intval($company_id))->fetchObject();

        $new_item = false;
        if(!$opening_hours_for_day){
            $new_item = true;
            $opening_hours_for_day = new stdClass();
            $opening_hours_for_day->company_id = $company_id;
            $opening_hours_for_day->weekday = $weekday;
        }

        $opening_hours_for_day->is_open = $opening_hours['is_open'];
        $opening_hours_for_day->open1 = $opening_hours['open1'];
        $opening_hours_for_day->close1 = $opening_hours['close1'];
        $opening_hours_for_day->open2 = $opening_hours['open2'];
        $opening_hours_for_day->close2 = $opening_hours['close2'];

        if($new_item){
            $this->DB->insertFromObject('company__opening_hours', $opening_hours_for_day);
        }
        else {
            $this->DB->updateFromObject('company__opening_hours', $opening_hours_for_day);
        }
    }

    static function getOpeningHoursFromRequest(){
        $opening_hour_keys = ['open1', 'close1', 'open2', 'close2'];
        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

        $opening_hours = [];

        foreach($days as $day){
            $day_opening_hours = [];
            $day_opening_hours['is_open'] = RequestHelper::reqbool($day.'_is_open');
            foreach($opening_hour_keys as $opening_hour_key){
                $day_opening_hours[$opening_hour_key] = RequestHelper::reqstr($day.'_'.$opening_hour_key);
            }
            $opening_hours[$day] = $day_opening_hours;
        }

        return $opening_hours;
    }

    function findCompanyByGooglePlaceId($google_place_id){
        return $this->getByX('google_place_id', $google_place_id);
    }

    function findCompanyByWebsite($website){
        return $this->getByX('website', $website);
    }

    function findCompanyByPermalink($permalink){
        return $this->getByX('permalink', $permalink);
    }

    function getSchemeOrgData($company){
        global $CONFIG;

        $title = $company->title;
        $description = $company->content;
        $phone_number = $company->phone_number;
        $url = $CONFIG->website_domain . $this->generatePermalink($company);
        $opening_hours = $this->getOpeningHours($company);
        $latitude = $company->latitude;
        $longitude = $company->longitude;
        return new SchemeOrgCompany($title, $description, $phone_number, $url, $opening_hours, $latitude, $longitude);
    }

    public function getByTaxonomyId($taxonomy_id)
    {
        #Cache integrieren

        $querystring = 'SELECT ' . $this->base_table_name . '.*,
            (' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published
            FROM ' . $this->base_table_name . '
            JOIN ' . $this->base_table_name . '__taxonomy_mapping ON ' . $this->base_table_name . '.' . $this->base_table_name . '_id = ' . $this->base_table_name . '__taxonomy_mapping.' . $this->base_table_name . '_id
            where (' . $this->base_table_name . '_date <= NOW() AND status = "publish") and ' . $this->base_table_name . '__taxonomy_mapping.taxonomy_id = ' . $this->DB->escape($taxonomy_id) . '
            ORDER BY is_featured DESC, ' . $this->base_table_name . '.' . $this->base_table_name . '_date DESC';

        $items = $this->DB->getAll($querystring, true, 15);

        return $this->getAllWithMeta($items);
    }

    public static function getPackages(){
        return [
            self::PACKAGE_FREE => 'Kostenlos',
            self::PACKAGE_BASIS => 'Basis',
            self::PACKAGE_PRO => 'Pro',
            self::PACKAGE_PREMIUM => 'Premium',
        ];
    }

    public static function getMaxCategories($package){
        $data = [
            self::PACKAGE_FREE => 1,
            self::PACKAGE_BASIS => 3,
            self::PACKAGE_PRO => 5,
            self::PACKAGE_PREMIUM => null,
        ];

        return ArrayHelper::getArrayValue($data, $package, null);
    }

    public static function hasDofollowLink($company){
        if(in_array($company->package, [self::PACKAGE_PRO, self::PACKAGE_PREMIUM])){
            return true;
        }

        return false;
    }

    public static function hasThumbnail($company){
        if(in_array($company->package, [self::PACKAGE_BASIS, self::PACKAGE_PRO, self::PACKAGE_PREMIUM])){
            return true;
        }

        return false;
    }

    public static function hasCoverImage($company){
        if(in_array($company->package, [self::PACKAGE_PRO, self::PACKAGE_PREMIUM])){
            return true;
        }

        return false;
    }

    public function getTaxonomyLinks($item)
    {
        $links = [];

        $taxonomies_to_display = array_splice($item->taxonomies, 0, self::getMaxCategories($item->package));

        foreach ($taxonomies_to_display as $tax) {
            $links[] = '<a class="taxonomy-button" href="' . $this->getTaxonomyPermalink($tax) . '">' . $tax->title . '</a>';
        }

        return implode(', ', $links);
    }
}