<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\component\item\AdContainerColumn;
use saschahuber\affiliatewebsitecore\component\item\AdItem;
use saschahuber\affiliatewebsitecore\component\item\AdItemColumn;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\Helper;

#[AllowDynamicProperties]
class AdManager extends Manager
{
    const AD_TYPE_SIDEBAR_LEFT = "sidebar-left";
    const AD_TYPE_SIDEBAR_RIGHT = "sidebar-right";
    const AD_TYPE_CONTENT_BETWEEN = "content-between";
    const AD_TYPE_CONTENT_AFTER = "content-after";
    const AD_TYPE_PAGE_BOTTOM = "page-bottom";
    const AD_TYPE_PRODUCT_GRID = "product-grid";
    const AD_TYPE_PRODUCT_GRID_WIDE = "product-grid-wide";

    const BASE_TABLE_NAME = 'ad';

    function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    static function getAdTypes()
    {
        return [
            AdManager::AD_TYPE_SIDEBAR_LEFT => "Sidebar (Links)",
            AdManager::AD_TYPE_SIDEBAR_RIGHT => "Sidebar (Rechts)",
            AdManager::AD_TYPE_CONTENT_BETWEEN => "Im Inhalt (zwischen Überschriften)",
            AdManager::AD_TYPE_CONTENT_AFTER => "Nach dem Inhalt",
            AdManager::AD_TYPE_PAGE_BOTTOM => "Am Ende der Seite (vor dem Footer)",
            AdManager::AD_TYPE_PRODUCT_GRID => "Zwischen den Produkten (Ein Grid-Element)",
            AdManager::AD_TYPE_PRODUCT_GRID_WIDE => "Zwischen den Produkten (Ganze Zeile)"
        ];
    }

    public function cleanItem($item)
    {
        return $item;
    }

    function getAll()
    {
        $all_mappings = $this->getAdMappings();

        $ads = [];

        $dbquery = $this->DB->query("SELECT * FROM " . self::BASE_TABLE_NAME);
        while ($ad = $dbquery->fetchObject()) {
            if (isset($all_mappings[$ad->ad_id])) {
                $ad->mappings = $all_mappings[$ad->ad_id];
            } else {
                $ad->mappings = [];
            }
            $ads[] = $ad;
        }

        return $ads;
    }

    function getAdMappings($ad_id = null)
    {
        $ad_mappings = [];

        $filter = null;
        if ($ad_id !== null) {
            $filter = "where ad_id = " . intval($ad_id);
        }

        $querystring = "SELECT * FROM " . self::BASE_TABLE_NAME . "__taxonomy_mapping "
            . (Helper::ifstr($filter !== null, $filter));

        $mappings = $this->DB->getAll($querystring, true, 60);
        foreach ($mappings as $mapping) {
            if (!isset($ad_mappings[$mapping->ad_id])) {
                $ad_mappings[$mapping->ad_id] = [];
            }

            $ad_mappings[$mapping->ad_id][] = $mapping;
        }

        return $ad_mappings;
    }

    function getAdById($ad_id)
    {
        $all_mappings = $this->getAdMappings($ad_id);

        $ad = $this->DB->query("SELECT * FROM ad where ad_id = " . intval($ad_id))->fetchObject();
        if ($ad) {
            if (isset($all_mappings[$ad->ad_id])) {
                $ad->mappings = $all_mappings[$ad->ad_id];
            } else {
                $ad->mappings = [];
            }
            return $ad;
        } else {
            return null;
        }
    }

    function findMostFittingAd($ad_type, $taxonomy_type, $content, $taxonomies)
    {
        $all_fitting_ads = $this->findFittingAds($ad_type, $taxonomy_type, $content, $taxonomies);

        if (count($all_fitting_ads) < 1) {
            return false;
        }

        //Zufällige Ad zurückgeben
        //TODO: Nach Relevanz sortieren und Ergebnis mit bester Relevanz zurückgeben
        return $all_fitting_ads[array_rand($all_fitting_ads)];
    }

    public function findFittingAds($ad_type, $taxonomy_type, $content, $taxonomies)
    {
        $taxonomies = array_filter($taxonomies);

        $taxonomy_ids = [];
        if ($taxonomies) {
            foreach ($taxonomies as $taxonomy) {
                if($taxonomy === null || $taxonomy === ""){
                    continue;
                }
                $taxonomy_ids[] = $taxonomy->id;
            }
        }
        $taxonomy_ids = array_filter($taxonomy_ids);

        $all_fitting_ads = [];

        $conditions = [
            self::BASE_TABLE_NAME . '.type = "' . $this->DB->escape($ad_type) . '"',
            self::BASE_TABLE_NAME . '.active = true',
            self::BASE_TABLE_NAME . '__taxonomy_mapping.taxonomy_type = "' . $this->DB->escape($taxonomy_type) . '"'
        ];

        if ($taxonomies) {
            $condition_items = [
                self::BASE_TABLE_NAME . '__taxonomy_mapping.taxonomy_id is NULL'
            ];

            if(count($taxonomy_ids) > 0){
                $condition_items[] = self::BASE_TABLE_NAME . '__taxonomy_mapping.taxonomy_id IN (' . implode(', ', $taxonomy_ids) . ')';
            }

            $conditions[] = [
                'items' => $condition_items,
                'logic' => 'OR'
            ];
        }

        $query_builder = new DatabaseSelectQueryBuilder(self::BASE_TABLE_NAME);
        $query_builder->selects([self::BASE_TABLE_NAME . '.*', 'positive_keywords', 'negative_keywords'])
            ->join(self::BASE_TABLE_NAME . '__taxonomy_mapping', self::BASE_TABLE_NAME . '__taxonomy_mapping.' . self::BASE_TABLE_NAME . '_id', self::BASE_TABLE_NAME . '.' . self::BASE_TABLE_NAME . '_id')
            ->conditions($conditions);

        #die(json_encode([$querystring, $query_builder->buildQuery()]));

        $ads = $this->DB->getAll($query_builder->buildQuery(), true, 60);

        foreach ($ads as $ad) {
            $positive_keywords = [];
            $negative_keywords = [];

            if (strlen($ad->positive_keywords) > 0) {
                $positive_keywords = explode(',', $ad->positive_keywords);
            }

            if (strlen($ad->negative_keywords) > 0) {
                $negative_keywords = explode(',', $ad->negative_keywords);
            }

            if (count($negative_keywords) > 0 && $this->containsKeyword($negative_keywords, $content)) {
                continue;
            }

            if (count($positive_keywords) < 1 ||
                $this->containsKeyword($positive_keywords, $content)) {
                $all_fitting_ads[] = $ad;
            }
        }

        if(count($all_fitting_ads) < 1 && AuthHelper::isAdminPanelUser()){
            $ad = new \stdClass();
            $ad->id = 0;
            $ad->content = BufferHelper::buffered(function() use ($ad_type){
                ?>
                <div class="centered" style="background-color: #aaaaaa; border: 3px solid #000; padding: 100px 10px;">
                    <h3 style="color: #fff; margin-top: 0;">Werbung <?=$ad_type?></h3>
                    <span style="color: #fff;">(Nur für Admins sichtbar)</span>
                </div>
                <?php
            });
            $all_fitting_ads[] = $ad;
        }

        return $all_fitting_ads;
    }

    public function getAdContent($ad)
    {
        return (new AdItem($ad))->getContent();
    }

    public function mixAdsInContent($items, $ad_type, $taxonomy_type, $taxonomies, $content, $min_elements_between = 4, $reuse_elements = false)
    {
        $ads = $this->findFittingAds($ad_type, $taxonomy_type, $content, $taxonomies);

        $items_to_insert = [];
        foreach ($ads as $ad) {
            $items_to_insert[] = $this->getAdContent($ad);
        }

        return ArrayHelper::insertItemsDistributed($items, $items_to_insert, $min_elements_between, $reuse_elements);
    }

    public function mixAdsInColumns($items, $ad_type, $content_type, $taxonomies, $content, $min_elements_between = 4, $reuse_elements = false)
    {
        $ad_spots_to_insert = round((floatval(count($items))/4/3));

        $items_to_insert = [];
        for($i = 0; $i < $ad_spots_to_insert; $i++){
            $items_to_insert[] = new AdContainerColumn($ad_type, $content_type, $taxonomies);
        }

        return ArrayHelper::insertItemsDistributed($items, $items_to_insert, $min_elements_between, $reuse_elements);
    }

    private function containsKeyword($keywords, $content)
    {
        if ($content === null) {
            return false;
        }

        foreach ($keywords as $keyword) {
            $clean_keyword = trim($keyword);
            if ($clean_keyword === "") {
                continue;
            }

            if (strpos(strtolower($content), strtolower($clean_keyword)) !== false) {
                return true;
            }
        }
        return false;
    }
}