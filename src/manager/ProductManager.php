<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\api\AmazonPAPIWrapper;
use saschahuber\affiliatewebsitecore\component\container\ProductTableX;
use saschahuber\affiliatewebsitecore\component\container\ProductTableY;
use saschahuber\affiliatewebsitecore\component\scheme\SchemeOrgProduct;
use saschahuber\affiliatewebsitecore\helper\ProductImageHelper;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\service\AmazonImageService;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\affiliatewebsitecore\service\ImageTemplateService;
use saschahuber\affiliatewebsitecore\service\ProductService;
use saschahuber\affiliatewebsitecore\service\ProductTaxonomyThumbnailGeneratorService;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\component\AdminEditButton;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\ImageHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\KeywordHelper;
use saschahuber\saastemplatecore\helper\NumberFormatHelper;
use saschahuber\saastemplatecore\helper\StaticHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use stdClass;

#[AllowDynamicProperties]
class ProductManager extends PostableManager
{
    const DEFAULT_AMAZON_SHOP_ID = 1;
    const DEFAULT_AMAZON_FEED_ID = null;

    const MIN_DEAL_DISCOUNT = 0.1;
    const MAX_DEAL_DISCOUNT = 0.5;

    const TABLE_NAME = "product";
    const URL_PREFIX = "produkte";

    const STATUS_TO_IMPORT = "to_import";

    const TYPE_PRODUCT = 'product';
    const TYPE_PRODUCT_CATEGORY = 'product__category';

    const LAYOUT_GRID = 'grid';
    const LAYOUT_LIST = 'list';
    const LAYOUT_TABLE_X = 'table-x';
    const LAYOUT_TABLE_Y = 'table-y';

    const THUMBNAIL_WIDTH = 150;
    const THUMBNAIL_HEIGHT = 150;

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME, self::URL_PREFIX);
        $this->product_service = new ProductService();
        $this->product_link_manager = new ProductLinkManager();
        $this->product_custom_data_field_manager = new ProductCustomDataFieldManager();
        $this->product_feed_manager = new ProductFeedManager();
        $this->amazon_image_service = new AmazonImageService();
        $this->author_service = new AuthorService();
        $this->ad_manager = new AdManager();
    }

    public function getDealInfo($product)
    {
        $discount_percent = (($product->price) - ($product->reduced_price)) * 100 / $product->price;
        $discount_money = $product->price - $product->reduced_price;

        $score = $discount_percent;

        //Eingrenzen auf sinnvolle Rabatte
        if ($discount_percent < 10 || $discount_percent > 50
            || $discount_money < 5 || $discount_money > 250) {
            $score = 0;
        }

        #Nicht verfügbare produkte haben den Score 0
        $discount_score = $score * ($product->is_available ? 1 : 0);

        #TODO: Verlauf der letzten Monate berücksichtigen und Score bewerten
        #$average_price_3_months = 0;
        #$average_price_1_month = 0;

        return array(
            'percent' => $discount_percent,
            'money' => $discount_money,
            'score' => $discount_score,
            'price' => $product->price,
            'reduced_price' => $product->reduced_price,
        );
    }

    public function updateProductField($product_id, $field_id, $field_value)
    {
        if ($field_value !== null) {
            $formatted_field_value = '"' . $this->DB->escape($field_value) . '"';
            $this->DB->query('REPLACE INTO product__data (product_id, product__data_field_id, value)
            VALUES (' . $product_id . ', ' . $field_id . ', ' . $formatted_field_value . ')');
        } else {
            $this->DB->query('DELETE FROM product__data 
               where product_id = ' . intval($product_id) . ' 
               and product__data_field_id = ' . intval($field_id));
        }
    }

    public function getByAsin($asin, $add_meta = true, $status = Manager::STATUS_PUBLISH)
    {
        #Cache integrieren
        $conditions = ['external_id = "' . $this->DB->escape($asin) . '"'];

        if ($status !== null) {
            $conditions[] = 'status = "' . $this->DB->escape($status) . '"';
        }

        $query_builder = new DatabaseSelectQueryBuilder('product');
        $query_builder->select('product.*')
            ->conditions($conditions)
            ->limit(1)
            ->join('product__link', 'product__link.product_id', 'product.product_id', 'LEFT');
        $querystring = $query_builder->buildQuery();

        $item = $this->DB->query($querystring)->fetchObject();

        if ($add_meta === true) {
            $this->addMeta($item);
        }

        return $item;
    }

    public function getByEan($ean, $add_meta = true, $status = Manager::STATUS_PUBLISH)
    {
        #Cache integrieren
        $conditions = ['ean = "' . $this->DB->escape($ean) . '"'];

        if ($status !== null) {
            $conditions[] = 'status = "' . $this->DB->escape($status) . '"';
        }

        $query_builder = new DatabaseSelectQueryBuilder('product');
        $query_builder->conditions($conditions)->limit(1);
        $querystring = $query_builder->buildQuery();

        $item = $this->DB->query($querystring)->fetchObject();

        if ($add_meta === true) {
            $this->addMeta($item);
        }

        return $item;
    }

    public function getFilteredProducts($keyword = null, $status = null, $taxonomies = null, $is_available = null, $order_by = null, $desc = true, $limit = null, $offset = 0, $has_content = null, $has_multiple_links = null, $is_fake=null)
    {
        $items = array();

        $filter_clauses = [];

        if ($keyword) {
            $keywords = KeywordHelper::keywords($keyword, false, true);
            $keyword_clauses = [];
            foreach($keywords as $keyword){
                $keyword_clauses[] = 'title LIKE "%' . $this->DB->escape($keyword) . '%"';
            }

            $filter_clauses[] = "(".implode(' AND ', $keyword_clauses).")";
        }

        if ($status) {
            $filter_clauses[] = 'status = "' . $this->DB->escape($status) . '"';
        }

        $taxonomy_join_clause = null;
        if ($taxonomies) {
            $filter_clauses[] = $this->base_table_name . '__taxonomy_mapping.taxonomy_id IN (' . implode(', ', $taxonomies) . ')';
            $taxonomy_join_clause = ' LEFT JOIN ' . $this->base_table_name . '__taxonomy_mapping 
            on ' . $this->base_table_name . '__taxonomy_mapping.' . $this->base_table_name . '_id = ' . $this->base_table_name . '.' . $this->base_table_name . '_id';
        }

        if ($is_fake !== null) {
            $filter_clauses[] = $this->base_table_name . '.is_fake = ' . ($is_fake ? '1' : '0');
        }

        if ($has_content !== null) {
            if($has_content){
                $filter_clauses[] = '(product.content is not null and product.content != "")';
            }
            else {
                $filter_clauses[] = '(product.content is null or product.content = "")';
            }
        }

        $order_clause = false;
        if ($order_by !== null) {
            $order_clause = ' ORDER BY ' . $this->DB->escape($order_by) . " " . ($desc ? "desc" : "asc");
        }

        $having_clauses = null;
        if($has_multiple_links !== null){
            if($has_multiple_links){
                $having_clauses[] = "COUNT(product__link.product__link_id) > 1";
            }
            else {
                $having_clauses[] = "COUNT(product__link.product__link_id) <= 1";
            }
        }

        if($is_available !== null){
            if ($is_available) {
                $having_clauses[] = 'available_link_count > 0';
            }
            else {
                $having_clauses[] = 'available_link_count < 1';
            }
        }

        $querystring = 'SELECT ' . $this->base_table_name . '.*, SUM(CASE WHEN product__link.is_available = 1 THEN 1 ELSE 0 END) as available_link_count,
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d. %M %Y") as formatted_date 
            FROM ' . $this->base_table_name
            . $taxonomy_join_clause
            . ' LEFT JOIN product__link on product.product_id = product__link.product_id'
            . ((count($filter_clauses) > 0) ? " WHERE " . implode(' AND ', $filter_clauses) : "")
            . ' GROUP BY ' . $this->base_table_name . '.' . $this->base_table_name . '_id'
            . ($having_clauses ? (" HAVING " . implode(' AND ', $having_clauses)) : "")
            . ($order_clause ?: "");

        if ($limit) {
            $querystring .= " LIMIT " . intval($limit);
        }

        if ($offset) {
            $querystring .= " OFFSET " . intval($offset);
        }

        #die($querystring);

        $dbquery = $this->DB->query($querystring);

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
        }

        return $this->getAllWithMeta($items);
    }

    public function getByIds($ids, $add_meta = true)
    {
        if (count($ids) < 1 || strlen(implode(', ', $ids)) < 1) {
            return [];
        }

        return $this->getAllWithMeta(
            $this->product_service->getAll('SELECT * FROM product 
                    where status = "publish"
                    and product_id IN (' . implode(', ', $ids) . ')')
        );
    }

    public function getImportedAsins()
    {
        $dbquery = $this->DB->query('SELECT external_id as amazon_asin, product_id FROM product__link where product__shop_id = 1');
        $asins = [];
        while ($item = $dbquery->fetchObject()) {
            $asins[$item->amazon_asin] = $item->product_id;
        }
        return $asins;
    }

    public function getByOldIds($ids, $add_meta = true, $orderby = null)
    {
        return $this->getAllWithMeta($this->product_service->getByOldIds($ids, $add_meta, $orderby));
    }

    public function getReducedFilter()
    {
        return "and (
            (
                product__link.price is not null and product__link.price > 0
                    and product__link.reduced_price is not null and product__link.reduced_price > 0
            )
            and (product__link.reduced_price < product__link.price)
            and (1-(product__link.reduced_price / product__link.price) >= " . str_replace(',', '.', self::MIN_DEAL_DISCOUNT) . ")
            and (1-(product__link.reduced_price / product__link.price) <= " . str_replace(',', '.', self::MAX_DEAL_DISCOUNT) . ")
        )";
    }

    private function getOrderByClause($orderby = null, $desc = true)
    {
        if ($orderby === null) {
            return '';
        }

        $orderby_field = 'RAND()';
        switch ($orderby) {
            default:
                $orderby_field = 'RAND()';
                break;
        }

        return ' ORDER BY ' . $orderby_field . ' ' . ($desc ? 'DESC' : 'ASC');
    }

    public function getByTaxonomyAliasMulti($aliase, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        return $this->getAllWithMeta($this->product_service->getByTaxonomyAliasMulti($aliase, $reduced, $limit, $orderby, $desc, $field));
    }

    public function getByFieldValue($meta_key, $meta_value, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        return $this->getAllWithMeta($this->product_service->getByFieldValue($meta_key, $meta_value, $reduced, $limit, $orderby, $desc, $field));
    }

    public function getByKategorieAndFieldValue($kategorie, $meta_key, $meta_value, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        return $this->getAllWithMeta($this->product_service->getByKategorieAndFieldValue($kategorie, $meta_key, $meta_value, $reduced, $limit, $orderby, $desc, $field));
    }

    public function getByTaxonomyAliasMultiAnd($aliase, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        return $this->getAllWithMeta($this->product_service->getByTaxonomyAliasMultiAnd($aliase, $reduced, $limit, $orderby, $desc, $field));
    }

    public function getByTaxonomyBrandAliasMulti($kategorien, $hersteller, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        return $this->getAllWithMeta($this->product_service->getByTaxonomyBrandAliasMulti($kategorien, $hersteller, $reduced, $limit, $orderby, $desc, $field));
    }

    public function getByBrandAliasMulti($aliase, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        return $this->getAllWithMeta($this->product_service->getByBrandAliasMulti($aliase, $reduced, $limit, $orderby, $desc, $field));
    }

    public function getByTaxonomyId($taxonomy_id)
    {
        $querystring = 'SELECT ' . $this->base_table_name . '.*,
            (' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published
            FROM ' . $this->base_table_name . '
            JOIN ' . $this->base_table_name . '__taxonomy_mapping ON ' . $this->base_table_name . '.' . $this->base_table_name . '_id = ' . $this->base_table_name . '__taxonomy_mapping.' . $this->base_table_name . '_id
            JOIN product__link ON product__link.product_id = product.product_id
            where ' . $this->product_service->getProductVisibilityQueryCondition()
            . ' and ' . $this->base_table_name . '__taxonomy_mapping.taxonomy_id = ' . $this->DB->escape($taxonomy_id)
            . ' GROUP BY ' . $this->base_table_name . '.' . $this->base_table_name . '_id'
            . ' ORDER BY ' . $this->base_table_name . '.' . $this->base_table_name . '_date DESC';

        $items = $this->DB->getAll($querystring, true, 15);

        return $this->getAllWithMeta($items);
    }

    public function getReduced($limit = false)
    {
        #Cache integrieren
        $produkte = [];
        $dbquery = $this->DB->query('SELECT product.* FROM product 
            JOIN product__link on product__link.product_id = product.product_id
            where product.status = "publish" and product.hide_in_search = 0 '
            . $this->getReducedFilter() . (($limit && $limit>0) ? " LIMIT " . intval($limit) : ""));

        while ($produkt = $dbquery->fetchObject()) {
            $this->addMeta($produkt);
            $produkte[] = $produkt;
        }

        return $produkte;
    }

    public function getTaxonomies($order_by = "title", $asc = true, $status = PostableManager::STATUS_PUBLISH)
    {
        $order_by_query = false;
        switch ($order_by) {
            case "title":
                $order_by_query = "title";
        }
        if ($order_by_query) {
            $order_by_query .= ' ' . ($asc ? 'ASC' : 'DESC');
        }

        #Cache integrieren
        $taxonomies = [];

        $conditions = [/*'is_virtual = 0'*/];

        $query_builder = new DatabaseSelectQueryBuilder($this->base_table_name . '__taxonomy');
        if ($status !== null) {
            $conditions[] = 'status = "' . $this->DB->escape($status) . '"';
        }

        $query_builder->conditions($conditions);

        if ($order_by_query) {
            $query_builder->order($order_by_query);
        }

        $items = $this->DB->getAll($query_builder->buildQuery(), true, 15);

        foreach ($items as $taxonomy) {
            $taxonomies[$taxonomy->id] = $taxonomy;
        }

        return $taxonomies;
    }

    public function getTaxonomiesWithProductNum($status = null, $add_meta = true, $order_by = null, $desc = true)
    {
        $items = array();

        $join_clause = ' LEFT JOIN (
            SELECT taxonomy_id, count(*) as anzahl 
            FROM product__taxonomy_mapping group by taxonomy_id
        ) as produkte_anzahl_mapping on product__taxonomy.product__taxonomy_id = produkte_anzahl_mapping.taxonomy_id
        ORDER BY title asc';

        $filter_clause = false;
        if ($status !== null) {
            $filter_clause = ' where status = "' . $this->DB->escape($status) . '"';
        }

        $order_clause = false;
        if ($order_by !== null) {
            $order_clause = ' ORDER BY ' . $this->DB->escape($order_by) . " " . ($desc ? "desc" : "asc");
        }

        $dbquery = $this->DB->query('SELECT product__taxonomy.*, produkte_anzahl_mapping.anzahl as produkt_anzahl
            FROM ' . $this->base_table_name . "__taxonomy" . ($join_clause ?: "") . ($filter_clause ?: "") . ($order_clause ?: ""));

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
        }

        return $items;
    }

    public function getTaxonomyThumbnailSrc($item)
    {
        return (new ProductTaxonomyThumbnailGeneratorService())->generateProductTaxonomyThumbnail($item);
    }

    public function getNewReviews($since_date, $limit = 4)
    {
        $items = array();

        if (!$since_date || strlen($since_date) < 1) {
            $since_date = '2000-01-01 00:00:00';
        }

        $querystring = 'SELECT * FROM product 
         where product_id in (
                 SELECT product_id FROM product__taxonomy_mapping 
                     LEFT JOIN product__taxonomy on product__taxonomy_mapping.taxonomy_id = product__taxonomy.product__taxonomy_id 
                               where product__taxonomy.permalink = "produkttests"
             )
             and product.status = "publish"
             and product_date >= "' . $this->DB->escape($since_date) . '" and product_date <= CURRENT_TIMESTAMP()
             and hide_in_newsletter != 1
             ORDER BY product_date DESC
             LIMIT ' . intval($limit);

        $dbquery = $this->DB->query($querystring);

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getTopDeals($number = 4)
    {
        $top_deals = [];

        $deals = $this->getReduced();

        #foreach ($deals as $deal) {}

        return array_slice($top_deals, 0, $number);
    }

    public function getByBrandId($brand_id)
    {
        #Cache integrieren
        $produkte = [];
        $dbquery = $this->DB->query('SELECT product.* FROM product 
            JOIN brand__product_mapping 
                ON brand__product_mapping.product_id = product.product_id
            where status = "publish" and product.hide_in_search = 0
            and brand_id = ' . $this->DB->escape($brand_id));

        while ($produkt = $dbquery->fetchObject()) {
            $this->addMeta($produkt);
            $produkte[] = $produkt;
        }

        return $produkte;
    }

    private function getAvgReviewScore($product)
    {
        $querystring = "SELECT AVG(review_value) as avg_review FROM `product__review`
            WHERE product_id = " . intval($product->id);
        return $this->DB->getOne($querystring, true, 60)->avg_review;
    }

    private function getReviewCount($product)
    {
        return $this->DB->query("SELECT COUNT(*) as review_count FROM `product__review`
            WHERE product_id = " . intval($product->id))->fetchObject()->review_count;
    }

    private function getAuthor($product)
    {
        $author_id = $product->author_id;
        return $this->author_service->getById($author_id);
    }

    public function addMeta($item, $meta = null, $taxonomies = null)
    {
        global $CONFIG;

        if ($item !== false) {
            $item->amazon_link = null;
            if (isset($item->amazon_asin)) {
                $base64_current_url = base64_encode(UrlHelper::getCurrentUrl());

                $item->amazon_link = "{$CONFIG->api_domain}/redirect/product/{$item->product_id}/amazon?source=$base64_current_url";
            }
        }

        if ($item !== false) {
            $item->review_score = $this->getAvgReviewScore($item);

            $item->full_permalink = $this->generatePermalink($item);

            $item->product_links = $this->product_link_manager->getAllByProductId($item->id);

            parent::addMeta($item, $meta, $taxonomies);

            $taxonomies = [];
            foreach ($item->taxonomies as $taxonomy) {
                $taxonomies[$taxonomy->id] = $taxonomy;
            }

            $querystring = 'SELECT * FROM ' . $this->base_table_name . '__taxonomy
                JOIN ' . $this->base_table_name . '__taxonomy_virtual_mapping
                    ON ' . $this->base_table_name . '__taxonomy_virtual_mapping.taxonomy_id = ' . $this->base_table_name . '__taxonomy.' . $this->base_table_name . '__taxonomy_id
                where ' . $this->base_table_name . '_id = ' . intval($item->id);

            foreach ($this->DB->getAll($querystring, true, 15) as $taxonomy) {
                if (!array_key_exists($taxonomy->id, $taxonomies)) {
                    $taxonomies[$taxonomy->id] = $taxonomy;
                }
            }

            $item->taxonomies = array_values($taxonomies);
        }
    }

    public function displayProducts($products, $limit = false, $layout = "grid", $show_detail_button = false,
                                    $show_meta = false, $show_reduced_badge = true, $show_review_score = false,
                                    $show_price = false)
    {
        global $LAYOUT_MANAGER;

        $contents = [];

        switch ($layout) {
            case self::LAYOUT_TABLE_X:
                return $this->getProductTableX($products, $show_detail_button, $show_reduced_badge, $show_price);
            case self::LAYOUT_TABLE_Y:
                return $this->getProductTableY($products, $show_detail_button, $show_reduced_badge, $show_price);
        }

        foreach ($products as $product) {
            $contents[] = $this->displayProduct($product, null, null, $layout,
                $show_reduced_badge, $show_review_score, $show_detail_button, $show_meta, $show_price);
        }

        switch ($layout) {
            case self::LAYOUT_GRID:
                $column_count = 4;
                break;
            case self::LAYOUT_LIST:
            default:
                $column_count = 1;
                break;
        }

        $width = intval(12 / $column_count);

        $rows = [];
        foreach (array_chunk($contents, $column_count) as $chunk) {
            $columns = [];
            foreach ($chunk as $content) {
                $columns[] = $LAYOUT_MANAGER->column($content, $width, "lg", ['col-md-6']);
            }
            $rows[] = $LAYOUT_MANAGER->row($columns);
        }

        return $LAYOUT_MANAGER->container($rows, ['product-grid']);
    }

    public function getProductDataMap($products)
    {
        $data_product_map = [];
        $product_data_id_count = [];

        foreach ($products as $product) {
            $product_data = $this->getProductData($product);

            $product_data_map[$product->id] = [];

            foreach ($product_data as $product_data_item) {
                if ($product_data_item->value === null || $product_data_item->value === "") {
                    continue;
                }

                $product_data_map[$product->id][$product_data_item->id] = $product_data_item;

                if (!array_key_exists($product_data_item->id, $data_product_map)) {
                    $data_product_map[$product_data_item->id] = [];
                }
                $data_product_map[$product_data_item->id][$product->id] = $product_data_item;

                if (!array_key_exists($product_data_item->id, $product_data_id_count)) {
                    $product_data_id_count[$product_data_item->id] = 0;
                }
                $product_data_id_count[$product_data_item->id] += 1;
            }
        }

        return [$product_data_map, $data_product_map, $product_data_id_count];
    }

    public function getProductTableX($products, $show_detail_button = true, $show_reduced_badge = true, $show_price = false)
    {
        return (new ProductTableX($this, $products, $show_detail_button, $show_reduced_badge, $show_price))->getContent();
    }

    public function getProductTableY($products, $show_detail_button = true, $show_reduced_badge = true, $show_price = false)
    {
        return (new ProductTableY($this, $products, $show_detail_button, $show_reduced_badge, $show_price))->getContent();
    }

    public function downloadAmazonProductInfo($asin, $hide_in_search = true, $default_status = Manager::STATUS_DRAFT)
    {
        $amazon_api = new AmazonPAPIWrapper();
        $item_data = $amazon_api->getByAsin($asin);

        //Derzeit nicht verfügbar
        #if(!isset($item_data['Offers'])){
        #    return null;
        #}

        $product = $this->updateProductFromAmazon($asin, $item_data, $hide_in_search, $default_status);

        if ($product !== null) {
            $this->addMeta($product);
        }

        return $product;
    }

    public static function getAmazonUrl($asin)
    {
        global $CONFIG;
        return "https://www.amazon.de/dp/$asin?tag={$CONFIG->amazon_partner_id}&linkCode=ogi&th=1&psc=1";
    }

    public function updateProductFromAmazon($asin, $item_data, $hide_in_search = false, $default_status = Manager::STATUS_DRAFT)
    {
        global $CONFIG;

        $product_to_update = $this->getByAsin($asin, true, null);
        $is_new = false;
        if ($product_to_update === false) {
            $is_new = true;
            $product_to_update = new stdClass();
            $product_to_update->status = $default_status;
            $old_status = $product_to_update->status;
            $product_to_update->hide_in_search = true;
            #$product_to_update->amazon_asin = $asin;
        } else {
            $old_status = $product_to_update->status;
        }

        if ($old_status == ProductManager::STATUS_TO_IMPORT) {
            $product_to_update->status = $default_status;
            $product_to_update->rel = "noindex, nofollow";
        }

        if ($item_data !== null) {
            if($is_new) {
                $product_to_update->title = $item_data['title'];
                $product_to_update->permalink = UrlHelper::alias($item_data['title']);
                $product_to_update->doindex = false;
                $product_to_update->dofollow = false;
                $product_to_update->is_fake = true;
            }

            //Preis aktualisieren
            $price = ArrayHelper::getArrayValue($item_data, 'price', null);
            $reduced_price = ArrayHelper::getArrayValue($item_data, 'reduced_price', null);
            $product_to_update->ean = ArrayHelper::getArrayValue($item_data, 'ean', null);

            if ($is_new) {
                $product_to_update->hide_in_search = ($hide_in_search ? 1 : 0);
                $product_to_update->product_id = $this->createItem($product_to_update);
                $product_to_update->id = $product_to_update->product_id;
            } else {
                $this->updateItem($product_to_update);
            }

            $product_link = $this->product_link_manager->getByProductId($product_to_update->id, self::DEFAULT_AMAZON_FEED_ID);
            $product_link_id = $product_link ? $product_link->id : null;

            $url = self::getAmazonUrl($asin);
            $this->product_link_manager->saveProductLink($product_to_update->id, self::DEFAULT_AMAZON_SHOP_ID, self::DEFAULT_AMAZON_FEED_ID,
                $price, $reduced_price, $asin, $url, true, $product_link_id);

            $this->setMeta($product_to_update->product_id, "product_info", serialize($item_data['info']));

            $primary_image = $item_data['primary_image'];
            $this->setMeta($product_to_update->product_id, "product_primary_image", $primary_image);

            $other_images = $item_data['other_images'];
            for ($i = 0; $i < count($other_images); $i++) {
                $image = $other_images[$i];
                $this->setMeta($product_to_update->product_id, "product_image_" . ($i + 1), $image);
            }

            return $product_to_update;
        }

        return null;
    }

    public function updateProductFromFeed($feed_id, $ean, $item_data, $hide_in_search = false, $default_status = Manager::STATUS_DRAFT)
    {
        $feed = $this->product_feed_manager->getById($feed_id);

        $product_to_update = $this->getByEan($ean, true, null);

        $overwrite = true;
        if ($product_to_update) {
            $product_link_orders = [];
            foreach ($this->product_link_manager->getAllByProductId($product_to_update->id) as $product_link) {
                $product_link_orders[$product_link->shop_sort_order] = $product_link->product__feed_id;
            }

            ksort($product_link_orders);

            if (count($product_link_orders)) {
                // Wenn die Feed-ID != der Feed-ID mit der höchsten Prio ist => nicht überschreiben, nur Link aktualisieren
                if (!($product_link_orders[array_keys($product_link_orders)[0]] == $feed_id)) {
                    $overwrite = false;
                }
            }
        }

        $is_new = false;
        if ($product_to_update === false) {
            $is_new = true;
            $product_to_update = new stdClass();
            $product_to_update->status = $default_status;
            $old_status = $product_to_update->status;
            $product_to_update->hide_in_search = true;
            $product_to_update->ean = $ean;
        } else {
            $old_status = $product_to_update->status;
        }

        if ($old_status == ProductManager::STATUS_TO_IMPORT) {
            $product_to_update->status = $default_status;
            $product_to_update->rel = "noindex, nofollow";
        }

        if ($item_data !== null) {
            if ($overwrite) {
                $product_to_update->title = $item_data['title'];
                $product_to_update->permalink = UrlHelper::alias($item_data['title']);
                $product_to_update->doindex = false;
                $product_to_update->dofollow = false;
                $product_to_update->is_fake = true;
            }

            //Preis aktualisieren
            $price = ArrayHelper::getArrayValue($item_data, 'price', null);
            $reduced_price = ArrayHelper::getArrayValue($item_data, 'reduced_price', null);
            $ean = ArrayHelper::getArrayValue($item_data, 'ean', null);

            if ($is_new) {
                $product_to_update->hide_in_search = ($hide_in_search ? 1 : 0);
                $product_to_update->product_id = $this->createItem($product_to_update);
                $product_to_update->id = $product_to_update->product_id;
            } else if ($overwrite) {
                $this->updateItem($product_to_update);
            }

            $product_link = $this->product_link_manager->getByProductId($product_to_update->id, $feed_id);
            $product_link_id = $product_link ? $product_link->id : null;

            $url = ArrayHelper::getArrayValue($item_data, 'affiliate_link', null);
            $external_id = ArrayHelper::getArrayValue($item_data, 'external_id', null);
            $is_available = ArrayHelper::getArrayValue($item_data, 'is_available', null);

            $this->product_link_manager->saveProductLink($product_to_update->id, $feed->product__shop_id, $feed_id,
                $price, $reduced_price, $external_id, $url, $is_available, $product_link_id);

            #$this->setMeta($product_to_update->product_id, "product_info", serialize($item_data['info']));

            if ($overwrite) {
                $primary_image = $item_data['primary_image'];
                $this->setMeta($product_to_update->product_id, "product_primary_image", $primary_image);
            }

            $other_images = $item_data['other_images'];
            for ($i = 0; $i < count($other_images); $i++) {
                $image = $other_images[$i];
                $this->setMeta($product_to_update->product_id, "product_image_" . ($i + 1) . "#feed_$feed_id", $image);
            }

            return $product_to_update;
        }

        return null;
    }

    public function getProductData($product)
    {
        $product_data = [];
        $dbquery = $this->DB->query('SELECT product__data_field.product__data_field_id, field_name, field_type, field_prefix, field_suffix, value
            FROM product__data_field
            LEFT JOIN product__data 
                on product__data.product__data_field_id = product__data_field.product__data_field_id
            where product__data.product_id = ' . intval($product->id) . '
            group by field_name
            order by group_id ASC, field_order asc');
        while ($item = $dbquery->fetchObject()) {
            $product_data[] = $item;
        }

        return $product_data;
    }

    private function getFormattedField($type, $value, $prefix = null, $suffix = null)
    {
        if ($type !== "true_false" && ($value === null || strlen($value) < 1)) {
            return "—";
        }

        switch ($type) {
            case "boolean":
                if (boolval($value)) {
                    return '<i class="fas fa-check-circle"></i>';
                } else {
                    return '<i class="fas fa-times-circle"></i>';
                }
            case "enum":
                return $value;
                /*
                $values = unserialize($value);
                $values = array_values($values);
                $prefix = (($prefix !== null && strlen($prefix) > 0) ? $prefix : "");
                $suffix = (($suffix !== null && strlen($suffix) > 0) ? $suffix : "");
                return $prefix . strval(implode(', ', $values)) . $suffix;
                */
            case "float":
            case "string":
            default:
                $prefix = (($prefix !== null && strlen($prefix) > 0) ? "$prefix&nbsp;" : "");
                $suffix = (($suffix !== null && strlen($suffix) > 0) ? "&nbsp;$suffix" : "");
                return $prefix . strval($value) . $suffix;
        }
    }

    public function displayProductDataTable($product)
    {
        $product_data = $this->getProductData($product);

        $custom_product_data = $this->product_custom_data_field_manager->getDataFields($product);

        ob_start();
        ?>
        <table class="table table-details table-condensed product-data-table">
            <tbody>
            <?php
            if ($custom_product_data) {
                foreach ($custom_product_data as $data) {
                    if (strlen($data->value) < 1) {
                        continue;
                    }
                    echo "<tr>";

                    if ($data->name) {
                        echo "<td>{$data->name}</td>";
                    }
                    if ($data->value) {
                        echo "<td>{$data->value}</td>";
                    }

                    echo "</tr>";
                }
            }
            ?>

            <?php foreach ($product_data as $data):
                if (strlen($data->value) < 1) {
                    continue;
                }

                ?>
                <tr>
                    <td><?= $data->field_name ?></td>
                    <td><?= $this->getFormattedField($data->field_type, $data->value,
                            $data->field_prefix, $data->field_suffix) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getProductBuyButton($product, $alt_button_text = false)
    {
        return $this->getProductBuyButtonsList($product, 1);
    }

    function getProductBuyButtonsList($product, $max_num = null)
    {
        global $CONFIG;

        $product_links = $this->product_link_manager->getAllByProductId($product->id);

        if ($max_num !== null) {
            $product_links = array_slice($product_links, 0, $max_num);
        }

        ob_start();
        foreach ($product_links as $product_link) {
            $button_text = $product_link->button_text ?: $product_link->product_shop_button_text;

            if (isset($product_link->is_available) && !$product_link->is_available) {
                if ($product_link->product_shop_title === "Amazon") {
                    $button_text = '<i class="fab fa-amazon"></i>&nbsp; Nicht auf Lager';
                } else {
                    $button_text = "Nicht verfügbar ({$product_link->product_shop_title})";
                }
            }

            $base64_current_url = base64_encode(UrlHelper::getCurrentUrl());
            $buy_url = "{$CONFIG->api_domain}/redirect/product/{$product_link->id}?source={$base64_current_url}";

            $analytics_code = "saveAnalyticsEvent('product_click', {product_id: {$product->id}, link_id: {$product_link->id}}); /*gaEvent('Affiliate-Conversion', '<?=$product->id?>');*/";

            ob_start();
            ?>
            [button color="btn-primary" block="true" shape="btn-round" size="btn-lg" rel="nofollow" target="_blank" href="<?= $buy_url ?>"
            onclick="<?= $analytics_code ?>;"]<?= $button_text ?>*[/button]
            <?php
            $content = ob_get_contents();
            ob_end_clean();
            echo $content;
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getProductDetailButton($product)
    {
        global $CONFIG;

        if ($product->is_fake) {
            return null;
        }

        $alt_button_text = false;
        $icon_position = "left";
        $icon = "fas fa-info-circle";

        //Prüfen, ob Produkttest
        if (isset($product->taxonomies)) {
            foreach ($product->taxonomies as $tax) {
                if ($tax->permalink === "produkttests") {
                    $alt_button_text = "Zum Produkttest";
                    $icon_position = "right";
                    $icon = "fas fa-chevron-right";
                    break;
                }
            }
        }

        ob_start();
        ?>
        [button color="btn-detail btn-block" shape="btn-round" size="btn-lg" icon="<?= $icon ?>" icon_position="<?= $icon_position ?>"
        href="<?=$CONFIG->website_domain?><?= $this->generatePermalink($product) ?>"]<?= ($alt_button_text ?: "Mehr Infos") ?>[/button]

        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getProductImageUrl($product)
    {
        global $CONFIG;

        if ($product->attachment_id !== null) {
            $image_helper = (new ImageManager());

            $img_url = $image_helper->getAttachmentUrl($product->attachment_id);

            return $img_url;
        }

        $use_proxied_image = ($product->product_links[0]->product__shop_id !== null);

        if ($use_proxied_image) {
            $amazon_asin = $product->product_links[0]->external_id;
            return $CONFIG->website_domain . "/produkt-img/{$product->id}/" . UrlHelper::alias($product->title) . "." . ProductImageHelper::PRODUCT_IMAGE_EXTENSION;
        } else {
            return $product->meta['product_primary_image'];
        }
    }

    public function getProxiedProductImage($product)
    {
        global $CONFIG;

        if ($product->attachment_id !== null) {
            $external = false;
            $url = (new ImageManager())->getAttachmentUrl($product->attachment_id);
        } else if (isset($product->meta) && isset($product->meta['product_primary_image'])) {
            $image_url = $product->meta['product_primary_image'];

            if (!$CONFIG->enable_amazon_images_download) {
                $external = true;
                $url = $image_url;
            } else {
                $amazon_image = $this->amazon_image_service->getImage($product);
                if ($amazon_image) {
                    $external = false;
                    $url = $amazon_image;
                } else {
                    $external = true;
                    $url = $image_url;
                }
            }
        } else {
            $external = false;
            $url = $this->image_service->getDynamicImage(ImageTemplateService::PRODUCT_THUMBNAIL, $product->title);
        }

        return array('external' => $external, 'url' => $url);
    }

    public function getProxiedAdditionalProductImage($product, $meta_key, $meta_value){
        global $CONFIG;

        $image_url = $product->meta[$meta_key];

        if (!$CONFIG->enable_amazon_images_download) {
            $url = $image_url;
        } else {
            $amazon_image = $this->amazon_image_service->getAdditionalImage($product, $meta_key);
            if ($amazon_image) {
                $url = $amazon_image;
            } else {
                $url = $image_url;
            }
        }

        return $url;
    }

    public function clearOtherProductImages($product_id){
        $this->DB->query('DELETE FROM product__meta where product_id = '.intval($product_id).' AND meta_key LIKE "product_image_%"');
    }

    public function getProductImages($product){
        $query_builder = new DatabaseSelectQueryBuilder('product__meta');
        $query_builder->conditions([
            'meta_key LIKE "product_image_%"',
            'product_id = ' . intval($product->id)
        ]);

        $use_proxied_image = ($product->product_links[0]->product__shop_id !== null);

        $image_urls = [$this->getProductImageUrl($product)];
        foreach($this->DB->getAll($query_builder->buildQuery()) as $image){
            $image_url = $image->meta_value;

            if($use_proxied_image){
                $image_url = $this->getProxiedAdditionalProductImage($product, $image->meta_key, $image->meta_value);
            }

            $image_urls[$image->meta_key] = $image_url;
        }

        return $image_urls;
    }

    public function getProductImageTag($product, $width = null, $height = null, $lazy = true)
    {
        global $CONFIG;

        if ($product->attachment_id !== null) {
            $image_helper = (new ImageManager());

            $img_url = $image_helper->getAttachmentUrl($product->attachment_id);

            if ($width) {
                $img_url = StaticHelper::getResizedImgUrl($img_url, $width);
            }

            return ImgUtils::getImageTag($img_url, true,
                $product->title, $product->title, $width, $height,
                "product-thumbnail");
        }

        $use_proxied_image = ($product->product_links[0]->product__shop_id !== null);

        if ($use_proxied_image) {
            return $this->getProductAmazonImageTag($product, $lazy, 150, 150);
        } else {
            $product_image = $product->meta['product_primary_image'];

            if(!$product_image){
                return '';
            }

            $mime_type = "image/*";

            $image_tag = ImageHelper::getImageTag($product_image, true,
                $product->title, $product->title, $width, $height,
                "product-thumbnail");

            return $image_tag;
        }
    }

    public function getProductAmazonImageTag($product, $lazy = true, $width = null, $height = null)
    {
        global $CONFIG;

        $amazon_asin = $product->product_links[0]->external_id;

        #$image_data = $this->getProxiedProductImage($product);

        if (boolval($CONFIG->enable_amazon_images_download)) {
            $mime_type = "image/*";

            $img_url = $CONFIG->website_domain . "/produkt-img/{$product->id}/" . UrlHelper::alias($product->title) . "." . ProductImageHelper::PRODUCT_IMAGE_EXTENSION;

            if ($width) {
                $img_url = StaticHelper::getResizedImgUrl($img_url, $width);
            }

            $image_tag = ImageHelper::getImageTag($img_url, $lazy,
                $product->title, $product->title, $width, $height, "product-thumbnail");

            if ($CONFIG->disable_webp) {
                return $image_tag;
            }

            return '<picture>
                <source srcset="' . $CONFIG->app_domain . '/produkt-img/webp/' . $product->id . '/' . UrlHelper::alias($product->title) . '.webp" type="image/webp">
                <source srcset="' . $CONFIG->app_domain . "/produkt-img/{$product->id}/" . UrlHelper::alias($product->title) . "." . ProductImageHelper::PRODUCT_IMAGE_EXTENSION . '" type="' . $mime_type . '">
                ' . $image_tag . '
                </picture>';
        } else if ($product->meta && array_key_exists('product_primary_image', $product->meta)) {
            $product_image = $product->meta['product_primary_image'];
            ob_start();
            ?>
            <img class="lazy loaded product-thumbnail" src="<?= $product_image ?>"
                 data-src="<?= $product_image ?>"
                 alt="<?= $product->title ?>"
                 title="<?= $product->title ?>"
                 width="100%"
                 data-was-processed="true">
            <?php
            $content = ob_get_contents();
            ob_end_clean();
            return $content;
        }

        return '';
    }

    public function getDiscountBadge($product)
    {
        if (!isset($product->product_links) || count($product->product_links) < 1) {
            return null;
        }

        $product_link = $product->product_links[0];

        if (!isset($product_link->price) || !isset($product_link->reduced_price)) {
            return null;
        }

        if (!$product_link->price > 0 || !$product_link->reduced_price > 0) {
            return null;
        }

        //Reduzierter Preis nicht geringer als regulär
        if ($product_link->reduced_price >= $product_link->price) {
            return null;
        }

        $discount = round(((($product_link->price) - ($product_link->reduced_price)) * 100 / $product_link->price), 0);

        if ($discount < self::MIN_DEAL_DISCOUNT * 100
            || $discount > self::MAX_DEAL_DISCOUNT * 100) {
            return null;
        }

        return '<span class="discount">-' . $discount . '%</span>';
    }

    public function displayProduct($product, $alt_title = null, $alt_button_text = null,
                                   $layout = "grid", $show_discount = true, $show_review_score = false,
                                   $show_detail_button = true, $show_meta = true, $show_price = false)
    {
        global $CONFIG;

        $base64_current_url = base64_encode(UrlHelper::getCurrentUrl());
        $product_link = "{$CONFIG->api_domain}/redirect/product/{$product->product_links[0]->id}?source={$base64_current_url}";

        $open_external = true;

        $review_score_badge = false;
        if ($product->review_score !== null) {
            $review_score_badge =
                '<div class="product-review-result-badge">
                    <p><i class="fas fa-star"></i>&nbsp;' . number_format($product->review_score, 1, ',', '') . '</p>
                </div>';
        }

        $discount_badge = $this->getDiscountBadge($product);

        if ($product->is_fake !== 1) {
            $open_external = false;
            $product_link = $this->generatePermalink($product);
        }

        switch ($layout) {
            case self::LAYOUT_TABLE_X:
            case self::LAYOUT_GRID:
                $function_name = 'displayProductGrid';
                break;
            case self::LAYOUT_LIST:
            default:
                $function_name = 'displayProductList';
                break;
        }

        return $this->{$function_name}($product, $product_link, $open_external,
            $discount_badge, $alt_title, $alt_button_text, $review_score_badge,
            $show_detail_button, $show_meta, $show_price);
    }

    function getProductReviewRating($rating, $max = 10)
    {
        $empty = '<i class="far fa-star"></i>';
        $half = '<i class="fas fa-star-half-alt"></i>';
        $full = '<i class="fas fa-star"></i>';

        $output = '';

        $rating_form = number_format($rating, 1, '.', '');
        $rating_arr = explode('.', $rating_form);

        if ($rating_arr) {
            if (isset($rating_arr[1]) && $rating_arr[1] > 0) {
                $rating_arr[1] = '1';
            } else {
                $rating_arr[1] = '0';
            }

            /*
             * FULL
             */
            $output .= str_repeat($full, $rating_arr[0]);

            /*
             * HALF
             */

            if (isset($rating_arr[1]) && '0' != $rating_arr[1]) {
                $output .= $half;
            }

            /*
             * EMTPY
             */
            if (($max - $rating_arr[0]) >= '1') {
                $output .= str_repeat($empty, $max - ($rating_arr[0] + $rating_arr[1]));
            }
        }

        return $output;
    }

    function addToPriceHistory($product, $product_link)
    {
        if ($product_link->price === null) {
            return;
        }

        $history_entry = new stdClass();
        $history_entry->product_id = $product->id;
        $history_entry->product__shop_id = $product_link->product__shop_id;
        $history_entry->price = $product_link->price;
        $history_entry->reduced_price = $product_link->reduced_price;

        $this->DB->insertFromObject("product__price_history", $history_entry);
    }

    function get_price_history($product, $date = null)
    {
        #TODO: query price history
    }

    function getProductReviews($product)
    {
        $querystring = "SELECT * FROM product__review where product_id = " . intval($product->id);
        return $this->DB->getAll($querystring, true, 60);
    }

    function displayReviewRows($reviews, $product_review_max_value = 10)
    {
        ?>
        <h3>Produktbewertungen</h3>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <?php
                foreach ($reviews as $review) { ?>
                    <div class="row" style="margin-top: 5px; margin-bottom: 5px;">
                        <div class="col-sm-3 d-table"><strong
                                    class="d-table-cell align-middle"><?= $review->reviewer_name; ?></strong></div>
                        <div class="col-sm-6 d-table">
                            <div class="d-table-cell align-middle">
                                <div class="product-rating"><?= $this->getProductReviewRating($review->review_value); ?></div>
                                <span class="rating-text">(<?= $review->review_value . '/' . $product_review_max_value; ?>)</span>
                            </div>
                        </div>
                        <div class="col-sm-3 d-table">
                            <small class="rating-hint d-table-cell align-middle"><?= $review->review_summary; ?></small>
                        </div>
                    </div>
                    <?php
                } ?>
            </div>
        </div>
        <?php
    }

    function displayProductGrid($product, $product_link, $open_external, $discount_badge,
                                $alt_title = null, $alt_button_text = null, $review_score_badge = null,
                                $show_detail_button = true, $show_meta = true, $show_price = false)
    {
        ob_start();
        ?>
        <div class="product-item text-center layout-grid">
            <div class="product-item-container">

                <?php

                #if($product->is_sponsored){
                #    (new SponsoredBadge())->display();
                #}
                ?>

                <div class="product-thumbnail-container">
                    <?= $review_score_badge ?: "" ?>
                    <a title="<?= $product->title ?>"
                       href="<?= $product_link ?>"
                        <?= Helper::ifstr($open_external, 'rel="nofollow" target="_blank"') ?>
                        <?= Helper::ifstr($open_external, ' target="_blank"') ?> rel="nofollow">
                        <?= $this->getProductImageTag($product, self::THUMBNAIL_WIDTH, self::THUMBNAIL_HEIGHT, true) ?>
                    </a>
                </div>
                <div class="product-data grid">
                    <p><span class="product-title"><?= Helper::shorten(($alt_title ?: $product->title), 100) ?></span>
                    </p>
                </div>

                <?= $discount_badge ?: "" ?>
                <?php (new AdminEditButton('dashboard/produkte/bearbeiten/' . $product->id, false))->display(); ?>
                <?= $this->getFavouriteIcon($product->id) ?>

                <?php if ($show_price && $product->product_links[0]->is_available): ?>
                    <div>
                        <?php if ($discount_badge): ?>
                            <p>
                                <del><?= NumberFormatHelper::currency($product->product_links[0]->price) ?></del>&nbsp;
                                <strong style="color: red;"><?= NumberFormatHelper::currency($product->product_links[0]->reduced_price) ?></strong>
                            </p>
                        <?php else: ?>
                            <p><?= NumberFormatHelper::currency($product->product_links[0]->price) ?></p>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <div class="button-container">
                    <div class="button-container-inner">
                        <?= $this->getProductBuyButton($product, $alt_button_text) ?>
                        <?= $show_detail_button ? $this->getProductDetailButton($product) : '' ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($content);
    }

    function displayProductList($product, $product_link, $open_external, $discount_badge,
                                $alt_title = null, $alt_button_text = null, $review_score_badge = null,
                                $show_detail_button = true, $show_meta = true, $show_price = false)
    {
        ob_start();
        ?>
        <div class="product-item text-center layout-list">
            <div class="product-item-container">
                <?php
                #if($product->is_sponsored){
                #    (new SponsoredBadge())->display();
                #}

                ?>

                [row]
                [col span="lg-4"]
                <div class="product-thumbnail-container">
                    <?= $review_score_badge ?: "" ?>
                    <a title="<?= $product->title ?>"
                       href="<?= ($open_external ? $product->amazon_link : $product_link) ?>"
                        <?= Helper::ifstr($open_external, 'rel="nofollow" target="_blank"') ?>
                        <?= Helper::ifstr($open_external, ' target="_blank"') ?> rel="nofollow">
                        <?= $this->getProductImageTag($product) ?>
                    </a>
                </div>

                <?= $this->getFavouriteIcon($product->id) ?>
                <?php (new AdminEditButton('dashboard/produkte/bearbeiten/' . $product->id, false))->display(); ?>
                <?= $discount_badge ?: "" ?>

                <?php if ($show_meta): ?>
                    <div>
                        <p><span class="product-title"><?= ($alt_title ?: $product->title) ?></span></p>

                        <?php if ($show_price && $product->product_links[0]->is_available): ?>
                            <div>
                                <?php if ($discount_badge): ?>
                                    <p>
                                        <del><?= NumberFormatHelper::currency($product->product_links[0]->price) ?></del>&nbsp;
                                        <strong style="color: red;"><?= NumberFormatHelper::currency($product->product_links[0]->reduced_price) ?></strong>
                                    </p>
                                <?php else: ?>
                                    <p><?= NumberFormatHelper::currency($product->product_links[0]->price) ?></p>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                [/col]
                [col span="lg-8"]
                <div class="product-data list">
                    <?php if (!$show_meta): ?>
                        <p><span class="product-title"><?= ($alt_title ?: $product->title) ?></span></p>

                        <?php if ($show_price && $product->product_links[0]->is_available): ?>
                            <div>
                                <?php if ($discount_badge): ?>
                                    <p>
                                        <del><?= NumberFormatHelper::currency($product->product_links[0]->price) ?></del>&nbsp;
                                        <strong style="color: red;"><?= NumberFormatHelper::currency($product->product_links[0]->reduced_price) ?></strong>
                                    </p>
                                <?php else: ?>
                                    <p><?= NumberFormatHelper::currency($product->product_links[0]->price) ?></p>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if ($show_meta): ?>
                        <?= $this->displayProductDataTable($product) ?>
                    <?php endif; ?>
                </div>
                <div class="button-container">
                    <div class="button-container-inner">
                        <?= $this->getProductBuyButton($product, $alt_button_text) ?>
                        <?= $show_detail_button ? $this->getProductDetailButton($product) : '' ?>
                    </div>
                </div>
                [/col]
                [/row]
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($content);
    }

    function getSchemeOrgData($product)
    {
        global $CONFIG;

        $price = $product->product_links[0]->price;
        if ($product->product_links[0]->reduced_price
            && $product->product_links[0]->reduced_price > 0
            && $product->product_links[0]->reduced_price < $product->product_links[0]->price) {
            $price = $product->product_links[0]->reduced_price;
        }

        $title = $product->title;
        $description = $product->content;

        //Nur eigene Produktfotos benutzen
        //$image = $this->getProductImageUrl($product);
        $image = null;
        if ($product->attachment_id) {
            $image = $CONFIG->app_domain . (new ImageManager())->getAttachmentUrl($product->attachment_id);
        }

        $avg_rating = $this->getAvgReviewScore($product);
        $rating_count = $this->getReviewCount($product);
        $author = $this->getAuthor($product);

        return new SchemeOrgProduct($author, $title, $description, $image, $price, $avg_rating, $rating_count);
    }

    public function findProductsByKeyword($keyword)
    {
        $products = $this->product_service->findProductsByKeyword($keyword);

        return $this->getAllWithMeta($products);
    }

    public function findProducts($keyword = null, $produkt_ids = null, $category_ids = null, $brand_ids = null,
                                 $system_ids = null, $room_ids = null, $min_price = null, $max_price = null, $reduced = null,
                                 $data_field = null, $data_values = null, $data_fields = null, $orderby = null, $order = null, $limit = null, $has_all_categories=false)
    {
        $products = $this->product_service->findProducts($keyword, $produkt_ids, $category_ids, $brand_ids, $system_ids,
            $room_ids, $min_price, $max_price, $reduced, $data_field, $data_values, $data_fields, $orderby, $order, $limit, $has_all_categories);

        return $this->getAllWithMeta($products);
    }

    public function updateVirtualTaxonomyDataFieldMappings($taxonomy_id, $virtual_product_taxonomy_data_fields)
    {
        $existing_mappings = $this->DB->getAll('SELECT * FROM product__taxonomy_virtual_mapping_rule
         WHERE product__taxonomy_id = ' . intval($taxonomy_id));

        $existing_mapping_field_ids = [];
        foreach ($existing_mappings as $mapping) {
            $existing_mapping_field_ids[] = $mapping->product__data_field_id;
        }

        $changed_mapping_field_ids = [];
        foreach ($virtual_product_taxonomy_data_fields as $field_id => $mapping) {
            $changed_mapping_field_ids[] = $field_id;
        }

        #die(json_encode([$changed_mapping_field_ids, $existing_mapping_field_ids, $virtual_product_taxonomy_data_fields]));

        foreach ($changed_mapping_field_ids as $data_field_id) {
            $mapping = $virtual_product_taxonomy_data_fields[$data_field_id];

            $data_field_mapping = null;
            if (!in_array($data_field_id, $existing_mapping_field_ids)
                || ($mapping['first_field_value'] === null && $mapping['first_field_value'] === null)
                || ($mapping['first_field_value'] === "" && $mapping['first_field_value'] == "")) {
                $this->DB->query('DELETE FROM product__taxonomy_virtual_mapping_rule 
                        WHERE product__taxonomy_id = ' . intval($taxonomy_id) . ' 
                        AND product__data_field_id = ' . intval($data_field_id));

                if ($mapping['first_field_value'] === null && $mapping['first_field_value'] === null) {
                    continue;
                }
            }

            $data_field_mapping = $this->DB->getOne('SELECT * FROM product__taxonomy_virtual_mapping_rule 
                WHERE product__taxonomy_id = ' . intval($taxonomy_id) . ' 
                AND product__data_field_id = ' . intval($data_field_id));

            if (!$data_field_mapping) {
                $data_field_mapping = new stdClass();
                $data_field_mapping->product__taxonomy_id = $taxonomy_id;
                $data_field_mapping->product__data_field_id = $data_field_id;
            }

            $data_field_mapping->field_type = ArrayHelper::getArrayValue($mapping, 'field_type');
            $data_field_mapping->first_field_value = ArrayHelper::getArrayValue($mapping, 'first_field_value');
            $data_field_mapping->second_field_value = ArrayHelper::getArrayValue($mapping, 'second_field_value');

            switch ($data_field_mapping->field_type) {
                case ProductDataFieldManager::TYPE_BOOLEAN:
                    $data_field_mapping->first_field_value = $data_field_mapping->first_field_value == "on" ? 1 : 0;
                    $data_field_mapping->second_field_value = null;
                    break;
                case ProductDataFieldManager::TYPE_FLOAT:
                    break;
                case ProductDataFieldManager::TYPE_ENUM:
                    $data_field_mapping->first_field_value = implode(',', ArrayHelper::getArrayValue($mapping, 'first_field_value'));
                    $data_field_mapping->second_field_value = null;
                    break;
                case ProductDataFieldManager::TYPE_STRING:
                default:
                    break;
            }

            if (isset($data_field_mapping->product__taxonomy_virtual_mapping_rule_id)) {
                $this->DB->updateFromObject('product__taxonomy_virtual_mapping_rule', $data_field_mapping);
            } else {
                $this->DB->insertFromObject('product__taxonomy_virtual_mapping_rule', $data_field_mapping);
            }
        }
    }
}