<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\component\container\PostGridContainer;
use saschahuber\affiliatewebsitecore\component\container\PostListContainer;
use saschahuber\affiliatewebsitecore\service\BlogTaxonomyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\BlogThumbnailGeneratorService;
use saschahuber\saastemplatecore\helper\ImgUtils;

#[AllowDynamicProperties]
class PostManager extends PostableManager
{
    const TABLE_NAME = "post";
    const URL_PREFIX = "blog";

    const TYPE_POST = 'post';
    const TYPE_POST_CATEGORY = 'post__category';

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME, self::URL_PREFIX);
    }

    public function displayPosts($posts, $limit = 4, $layout = "grid", $show_detail_button = false, $show_meta = false)
    {
        if ($layout === "grid") {
            return (new PostGridContainer($posts))->getContent();
        } else {
            return (new PostListContainer($posts))->getContent();
        }
    }

    public function displayPost($post)
    {
        $post_link = $this->generatePermalink($post);

        ob_start();
        ?>
        <div class="post-item text-center">
            <a href="<?= $post_link ?>">
                <?=ImgUtils::getImageTag($this->getThumbnailSrc($post), true, $post->title, $post->title, 750, null, ['post-thumbnail'])?>
                <div class="post-data">
                    <span class="post-title"><?= $post->title ?></span>
                    <!--<p><?= $this->getExcerpt($post->content, true, 150) ?></p>-->
                </div>
            </a>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getThumbnailSrc($item)
    {
        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        } else {
            $thumbnail_src = (new BlogThumbnailGeneratorService())->getThumbnailSrc($item);
        }
        return $thumbnail_src;
    }

    public function getTaxonomyThumbnailSrc($item)
    {
        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        } else {
            $thumbnail_src = (new BlogTaxonomyThumbnailGeneratorService())->getThumbnailSrc($item);
        }
        return $thumbnail_src;
    }

    public function getThumbnail($item)
    {
        $alt_text = null;
        $img_title = null;

        if ($item->attachment_id !== null) {
            $attachment = $this->image_manager->getAttachment($item->attachment_id);
            $img_title = $attachment->title;
            $alt_text = $attachment->alt_text;
        }

        return ImgUtils::getImageTag($this->getThumbnailSrc($item), true, $alt_text, $img_title);
    }

    public function getNewPosts($since_date, $limit = 4)
    {
        $items = array();

        if(!$since_date || strlen($since_date) < 1){
            $since_date = '2000-01-01 00:00:00';
        }

        $dbquery = $this->DB->query('SELECT * FROM post 
            where status = "publish" and post_date >= "' . $this->DB->escape($since_date) . '" and post_date <= CURRENT_TIMESTAMP()
            and hide_in_newsletter != 1
            ORDER BY post_date DESC LIMIT ' . intval($limit));

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getByOldIds($ids, $add_meta = true, $orderby = null)
    {
        if ($ids === null || count($ids) < 1) {
            return [];
        }

        $in_ids_string = implode(', ', $ids);
        if (strlen($in_ids_string) < 1) {
            return [];
        }

        $querystring = 'SELECT * FROM post 
            where status = "publish" and post_id_old IN (' . $in_ids_string . ')';

        $items = array();

        $dbquery = $this->DB->query($querystring);

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getByTaxonomyAliasMulti($aliase, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        $querystring = 'SELECT post.* FROM post 
            JOIN post__taxonomy_mapping 
                ON post__taxonomy_mapping.post_id = post.post_id
            JOIN post__taxonomy 
                ON post__taxonomy_mapping.taxonomy_id = post__taxonomy.post__taxonomy_id
            where post__taxonomy.permalink IN ("' . implode('", "', $aliase) . '") 
            and post.status = "publish" 
            and post_date < CURRENT_TIMESTAMP()
            order by post_date desc'
            #.(Helper::ifstr($orderby!==null, (' ORDER BY '.$this->getOrderByClause($orderby, $desc, $field))))
            . (($limit && $limit>0) ? " LIMIT " . intval($limit) : "");

        return $this->getAllWithMeta($this->DB->getAll($querystring));
    }

    public function addMeta($item, $meta = null, $taxonomies = null)
    {
        parent::addMeta($item, $meta, $taxonomies);

        $item->full_permalink = $this->generatePermalink($item);
        $item->thumbnail_src = $this->getThumbnailSrc($item);
        $item->excerpt = $this->getExcerpt($item->content, true, 150);
    }
}