<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\manager\AbstractManager;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;

#[AllowDynamicProperties]
abstract class Manager extends AbstractManager
{
    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISH = 'publish';
    const STATUS_TRASH = 'trash';

    public function __construct($base_table_name)
    {
        parent::__construct($base_table_name);
    }

    function getByProductId($product_id, $feed_id)
    {
        $product_feed_id_condition = "product__link.product__feed_id is null";
        if($feed_id !== null){
            $product_feed_id_condition = "product__link.product__feed_id = " . intval($feed_id);
        }

        $query_builder = new DatabaseSelectQueryBuilder($this->getBaseTableName());
        $querystring = $query_builder#
        ->selects([
            $this->getBaseTableName() . ".*",
            'product__shop.title as product_shop_title',
            'product__feed.title as product_feed_title'
        ])
            ->conditions([
                "product_id = " . intval($product_id),
                $product_feed_id_condition
            ])
            ->join("product__shop", "product__shop.product__shop_id", "product__link.product__shop_id", 'LEFT')
            ->join("product__feed", "product__feed.product__feed_id", "product__link.product__feed_id", 'LEFT')
            ->buildQuery();

        $item = $this->DB->getOne($querystring, true, 60);

        return $item ?: null;
    }

    protected function getByX($search_col, $value, $intval = false)
    {
        if ($intval) {
            $value = intval($value);
        } else {
            $value = '"' . $this->DB->escape($value) . '"';
        }

        #Cache integrieren

        $querystring = 'SELECT *,
            (' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published,
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d.%m.%Y") as formatted_date
            FROM ' . $this->base_table_name . ' 
            where ' . $search_col . ' = ' . $value . ' LIMIT 1';

        $item = $this->DB->getOne($querystring, true, 15);

        if (!$item) {
            return null;
        }

        return $item;
    }

    protected function getAllByX($search_col, $value, $intval = false)
    {
        if ($intval) {
            $value = intval($value);
        } else {
            $value = '"' . $this->DB->escape($value) . '"';
        }

        #Cache integrieren

        $querystring = 'SELECT *,
            (' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published,
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d.%m.%Y") as formatted_date
            FROM ' . $this->base_table_name . ' 
            where ' . $search_col . ' = ' . $value;

        $items = [];
        foreach($this->DB->getAll($querystring, true, 15) as $item){
            $items[] = $item;
        }
        return $items;
    }

    public function getTotalItemCount()
    {
        $querystring = 'SELECT count(*) as anzahl FROM ' . $this->base_table_name;
        return $this->DB->getOne($querystring)->anzahl;
    }

    static function getStatusTypes()
    {
        return [
            Manager::STATUS_DRAFT => "Entwurf",
            Manager::STATUS_PUBLISH => "Öffentlich",
            Manager::STATUS_TRASH => "Papierkorb"
        ];
    }
}