<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;

#[AllowDynamicProperties]
class ProductLinkClickManager extends Manager
{
    const BASE_TABLE_NAME = 'product__link_click';

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    public function cleanItem($item)
    {
        return $item;
    }

    public function findClicks($user_id = null, $user_ip = null, $session_id = null, $product_id = null)
    {
        $conditions = [];
        if ($user_id) {
            $conditions[] = "user_id = '" . $this->DB->escape($user_id) . "'";
        }

        if ($user_ip) {
            $conditions[] = "user_ip = '" . $this->DB->escape($user_ip) . "'";
        }

        if ($session_id) {
            $conditions[] = "session_id = '" . $this->DB->escape($session_id) . "'";
        }

        if ($product_id) {
            $conditions[] = "product.product_id = '" . $this->DB->escape($product_id) . "'";
        }

        $query_builder = new DatabaseSelectQueryBuilder(self::BASE_TABLE_NAME);
        $query_builder->conditions($conditions)
            ->join('product', 'product.product_id', 'product__link_click.product_id')
            ->selects(['product.title', 'source_url', 'time', 'session_id', 'user_id', 'user_ip']);
        $query_builder->order('time DESC');

        $querystring = $query_builder->buildQuery();
        return $this->DB->getAll($querystring);
    }

    public function getAllClicksWithProducts($product_id = null, $start_date = null, $end_date = null)
    {

        $conditions = [];

        $query_builder = new DatabaseSelectQueryBuilder(self::BASE_TABLE_NAME);
        $query_builder->join('product', 'product.product_id', 'product__link_click.product_id')
            ->selects(['product.title', 'source_url', 'time', 'session_id', 'user_id', 'user_ip'])
            ->order('time DESC');

        if ($product_id) {
            $conditions[] = 'product.product_id = ' . intval($product_id);
        }

        if ($start_date) {
            $conditions[] = 'product__link_click.time > "' . $this->DB->escape($start_date) . '"';
        }

        if ($end_date) {
            $conditions[] = 'product__link_click.time < "' . $this->DB->escape($end_date) . '"';
        }

        $query_builder->conditions($conditions);

        $querystring = $query_builder->buildQuery();
        return $this->DB->getAll($querystring);
    }
}