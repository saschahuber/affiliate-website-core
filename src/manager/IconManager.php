<?php

namespace saschahuber\affiliatewebsitecore\manager;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\ImageHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\UrlHelper;

class IconManager extends Manager
{
    const BASE_TABLE_NAME = "icon";
    const ATTACHMENT_DIR = "icon";

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    public function getIcon($icon_id)
    {
        return $this->DB->query("SELECT * FROM icon where icon_id = " . intval($icon_id))->fetchObject();
    }

    public function getIconByFilepath($filepath)
    {
        return $this->DB->query("SELECT * FROM icon where file_path LIKE '" . $this->DB->escape($filepath) . "'")->fetchObject();
    }

    public function updateIcon($icon_id, $description, $provider_name, $copyright_info, $title, $alt_text, $use_in_icon_generator)
    {
        $this->DB->query("UPDATE icon set title = '" . $this->DB->escape($title) . "',
                            alt_text = '" . $this->DB->escape($alt_text) . "',
                            description = '" . $this->DB->escape($description) . "',
                            provider_name = '" . $this->DB->escape($provider_name) . "',
                            copyright_info = '" . $this->DB->escape($copyright_info) . "',
            where icon_id = " . intval($icon_id));
    }

    public function addIconFromUrl($url, $title = null, $alt_text = null, $old_url = null, $old_id = null)
    {
        $file_path = $this->uploadIconFromUrl($url);

        $file_name = pathinfo($url)['basename'];

        return $this->addIcon($file_path, $title, $alt_text, $old_url, $old_id);
    }

    public function addIcon($file_path, $title = null, $alt_text = null, $old_url = null, $old_id = null){
        $file_name = pathinfo($file_path)['basename'];

        $title = $title ? '"' . $this->DB->escape($title) . '"' : 'null';
        $alt_text = $alt_text ? '"' . $this->DB->escape($alt_text) . '"' : 'null';
        $file_path = $file_path ? '"' . $this->DB->escape($file_path) . '"' : 'null';
        $file_name = $file_name ? '"' . $this->DB->escape($file_name) . '"' : 'null';
        $old_url = $old_url ? '"' . $this->DB->escape($old_url) . '"' : 'null';
        $old_id = $old_id ? intval($old_id) : 'null';

        $querystring = 'INSERT INTO icon (title, alt_text, file_path, file_name) 
            VALUES (
                ' . $title . ',
                ' . $alt_text . ',
                ' . $file_path . ',
                ' . $file_name . '
            )';

        $this->DB->query($querystring);

        return $this->DB->insert_id;
    }

    public function addIconFromFile($file, $source_url=null, $description = null, $provider_name = null, $copyright_info = null, $title = null, $alt_text = null, $uploaded_file_name = null)
    {
        $file_path = $this->uploadIconFile($file, $uploaded_file_name);
        $file_name = pathinfo($file_path)['basename'];
        if ($file_path) {
            $this->DB->query('INSERT INTO icon (title, source_url, alt_text, file_path, file_name, description, provider_name, copyright_info) 
            VALUES (
                "' . $this->DB->escape($title) . '",
                "' . $this->DB->escape($source_url) . '",
                "' . $this->DB->escape($alt_text) . '",
                "' . $this->DB->escape($file_path) . '",
                "' . $this->DB->escape($file_name) . '",
                "' . $this->DB->escape($description) . '",
                "' . $this->DB->escape($provider_name) . '",
                "' . $this->DB->escape($copyright_info) . '"
            )');
            return $this->DB->insert_id;
        } else {
            die("Konnte Bild-Datei nicht hochladen: " . json_encode($file));
            return false;
        }
    }

    public function uploadIconFile($file, $uploaded_file_name=null)
    {
        if (!empty($file['name'])) {
            $media_dir = DATA_DIR . '/media';

            $file_info = pathinfo($file['name']);

            $filename = UrlHelper::alias($file_info['filename']) . '.' . $file_info['extension'];

            if($uploaded_file_name){
                $filename = $uploaded_file_name;
            }

            $icon_path = '/' . self::ATTACHMENT_DIR . '/' . $filename;

            if (!file_exists($media_dir . $icon_path)) {
                FileHelper::createDirIfNotExists($media_dir . '/' . self::ATTACHMENT_DIR);
                move_uploaded_file($file['tmp_name'], $media_dir . $icon_path);
                return $icon_path;
            }
        }

        die("Konnte Bild-Datei nicht hochladen: " . $icon_path);

        return false;
    }

    public function uploadIconFromUrl($url, $filename = null)
    {
        if ($filename === null) {
            $path_parts = pathinfo($url);
            $filename = $path_parts['basename'];
        }

        FileHelper::createDirIfNotExists(MEDIA_DIR . '/' . self::ATTACHMENT_DIR);

        $image = file_get_contents($url);

        $file_path = '/' . self::ATTACHMENT_DIR . '/' . $filename;

        if ($image !== false) {
            file_put_contents(MEDIA_DIR . $file_path, $image);
        } else {
            return null;
        }

        return $file_path;
    }

    public function getIconImageTag($icon_id, $alt_text = null, $title = null, $width = null, $height = null, $lazy=true)
    {
        $icon = $this->getIcon($icon_id);

        if (!$icon) {
            return null;
        }

        $img_url = $this->createIconSrcFromFilePath($icon->file_path);

        if (!$img_url) {
            return null;
        }

        if ($title === null) {
            $title = $icon->title;
        }

        if ($alt_text === null) {
            $alt_text = $icon->alt_text;
        }

        return ImageHelper::getImageTag($img_url, $lazy, $alt_text, $title, $width, $height);
    }

    public function getIconUrl($icon_id, $default_image = null)
    {
        if ($icon_id === null) {
            return $default_image;
        }

        $item = $this->DB->getOne("SELECT file_path FROM icon where icon_id = " . intval($icon_id), true, 60);

        if ($item) {
            return $this->createIconSrcFromFilePath($item->file_path);
        }

        return $default_image;
    }

    public function getAbsoluteIconUrl($icon_id, $default_image = null)
    {
        global $CONFIG;

        $image = $this->getIconUrl($icon_id);

        if ($image) {
            return $CONFIG->website_domain . $image;
        }

        return $default_image;
    }

    public function getIcons($keyword = null, $limit=null, $offset=0)
    {
        $icons = array();

        $query_builder = new DatabaseSelectQueryBuilder("icon");

        if ($keyword) {
            $query_builder->conditions([
                'title LIKE "%' . $this->DB->escape($keyword) . '%"',
                'alt_text LIKE "%' . $this->DB->escape($keyword) . '%"',
                'file_name LIKE "%' . $this->DB->escape($keyword) . '%"',
                'description LIKE "%' . $this->DB->escape($keyword) . '%"'
            ], 'or');
        }

        if($limit){
            $limit_string = intval($offset) . ", " . intval($limit);
            $query_builder->limit($limit_string);
        }

        $query_builder->orders([
            "icon_time DESC",
            "icon_id DESC"
        ]);

        $querystring = $query_builder->buildQuery();

        $dbquery = $this->DB->query($querystring);
        while ($item = $dbquery->fetchObject()) {
            $item->src = $this->createIconSrcFromFilePath($item->file_path);
            $icons[] = $item;
        }

        return $icons;
    }

    public function getIconsForIconGenerator()
    {
        $icons = array();

        $query_builder = new DatabaseSelectQueryBuilder("icon");

        $query_builder->conditions([
            'use_in_icon_generator = true'
        ]);

        $query_builder->orders([
            "icon_time DESC",
            "icon_id DESC"
        ]);

        $querystring = $query_builder->buildQuery();

        $dbquery = $this->DB->query($querystring);
        while ($item = $dbquery->fetchObject()) {
            $item->src = $this->createIconSrcFromFilePath($item->file_path);
            $icons[] = $item;
        }

        return $icons;
    }

    public function createIconSrcFromFilePath($file_path)
    {
        return '/data/media' . $file_path;
    }
}