<?php

namespace saschahuber\affiliatewebsitecore\manager;

class ProductShopManager extends Manager
{
    const BASE_TABLE_NAME = 'product__shop';

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    public function cleanItem($item)
    {
        return $item;
    }

    function getAll()
    {
        $product_shops = [];

        $dbquery = $this->DB->query("SELECT * FROM " . self::BASE_TABLE_NAME);
        while ($product_shop = $dbquery->fetchObject()) {
            $product_shops[] = $product_shop;
        }

        return $product_shops;
    }

    function getAllBySortOrder()
    {
        $product_shops = [];

        $dbquery = $this->DB->query("SELECT * FROM " . self::BASE_TABLE_NAME . " ORDER BY sort_order ASC");
        while ($product_shop = $dbquery->fetchObject()) {
            $product_shops[] = $product_shop;
        }

        return $product_shops;
    }
}