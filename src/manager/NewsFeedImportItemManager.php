<?php

namespace saschahuber\affiliatewebsitecore\manager;

class NewsFeedImportItemManager extends Manager
{
    const TABLE_NAME = "news__feed_import_item";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    function getAll()
    {
        $querystring = "SELECT * FROM " . $this->base_table_name;
        return $this->DB->getAll($querystring, true, 60);
    }

    function getImportedInPastDays($days){
        $querystring = "SELECT * FROM news__feed_import_item where date > NOW() - INTERVAL ".intval($days)." DAY ORDER BY date DESC;";
        return $this->DB->getAll($querystring);
    }
}