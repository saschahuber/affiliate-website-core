<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class ImageManager extends Manager
{
    const BASE_TABLE_NAME = "attachment";
    const ATTACHMENT_DIR = "attachment";

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    public function getAttachment($attachment_id)
    {
        return $this->DB->query("SELECT * FROM attachment where attachment_id = " . intval($attachment_id))->fetchObject();
    }

    public function getAttachmentByOldUrl($attachment_url)
    {
        return $this->DB->query("SELECT * FROM attachment where old_attachment_url = '" . $this->DB->escape($attachment_url) . "'")->fetchObject();
    }

    public function getAttachmentByFilepath($filepath)
    {
        return $this->DB->query("SELECT * FROM attachment where file_path LIKE '" . $this->DB->escape($filepath) . "'")->fetchObject();
    }

    public function updateAttachment($attachment_id, $description, $provider_name, $copyright_info, $title, $alt_text, $use_in_attachment_generator)
    {
        $this->DB->query("UPDATE attachment set title = '" . $this->DB->escape($title) . "',
                            alt_text = '" . $this->DB->escape($alt_text) . "',
                            description = '" . $this->DB->escape($description) . "',
                            provider_name = '" . $this->DB->escape($provider_name) . "',
                            copyright_info = '" . $this->DB->escape($copyright_info) . "',
                            use_in_attachment_generator = " . ($use_in_attachment_generator ? 'true' : 'false') . "
            where attachment_id = " . intval($attachment_id));
    }

    public function addAttachmentFromUrl($url, $title = null, $alt_text = null, $old_url = null, $old_id = null)
    {
        $file_path = $this->uploadAttachmentFromUrl($url);

        $file_name = pathinfo($url)['basename'];

        return $this->addAttachment($file_path, $title, $alt_text, $old_url, $old_id);
    }

    public function addAttachment($file_path, $title = null, $alt_text = null, $old_url = null, $old_id = null){
        $file_name = pathinfo($file_path)['basename'];

        $title = $title ? '"' . $this->DB->escape($title) . '"' : 'null';
        $alt_text = $alt_text ? '"' . $this->DB->escape($alt_text) . '"' : 'null';
        $file_path = $file_path ? '"' . $this->DB->escape($file_path) . '"' : 'null';
        $file_name = $file_name ? '"' . $this->DB->escape($file_name) . '"' : 'null';
        $old_url = $old_url ? '"' . $this->DB->escape($old_url) . '"' : 'null';
        $old_id = $old_id ? intval($old_id) : 'null';

        $querystring = 'INSERT INTO attachment (title, alt_text, file_path, file_name, old_attachment_url, old_attachment_id) 
            VALUES (
                ' . $title . ',
                ' . $alt_text . ',
                ' . $file_path . ',
                ' . $file_name . ',
                ' . $old_url . ',
                ' . $old_id . '
            )';

        $this->DB->query($querystring);

        return $this->DB->insert_id;
    }

    public function addAttachmentFromFile($file, $description = null, $provider_name = null, $copyright_info = null, $title = null, $alt_text = null, $use_in_attachment_generator = false, $uploaded_file_name = null)
    {
        $file_path = $this->uploadAttachmentFile($file, $uploaded_file_name);
        $file_name = pathinfo($file_path)['basename'];
        if ($file_path) {
            $this->DB->query('INSERT INTO attachment (title, alt_text, file_path, file_name, description, provider_name, copyright_info, use_in_attachment_generator) 
            VALUES (
                "' . $this->DB->escape($title) . '",
                "' . $this->DB->escape($alt_text) . '",
                "' . $this->DB->escape($file_path) . '",
                "' . $this->DB->escape($file_name) . '",
                "' . $this->DB->escape($description) . '",
                "' . $this->DB->escape($provider_name) . '",
                "' . $this->DB->escape($copyright_info) . '",
                ' . ($use_in_attachment_generator ? 'true' : 'false') . '
            )');
            return $this->DB->insert_id;
        } else {
            die("Konnte Bild-Datei nicht hochladen: " . json_encode($file));
            return false;
        }
    }

    public function uploadAttachmentFile($file, $uploaded_file_name=null)
    {
        if (!empty($file['name'])) {
            $media_dir = DATA_DIR . '/media';

            $file_info = pathinfo($file['name']);

            $filename = UrlHelper::alias($file_info['filename']) . '.' . $file_info['extension'];

            if($uploaded_file_name){
                $filename = $uploaded_file_name;
            }

            $attachment_path = '/' . self::ATTACHMENT_DIR . '/' . $filename;

            if (!file_exists($media_dir . $attachment_path)) {
                FileHelper::createDirIfNotExists($media_dir . '/' . self::ATTACHMENT_DIR);
                move_uploaded_file($file['tmp_name'], $media_dir . $attachment_path);
                return $attachment_path;
            }
        }

        die("Konnte Bild-Datei nicht hochladen: " . $attachment_path);

        return false;
    }

    public function uploadAttachmentFromUrl($url, $filename = null)
    {
        if ($filename === null) {
            $path_parts = pathinfo($url);
            $filename = $path_parts['basename'];
        }

        FileHelper::createDirIfNotExists(MEDIA_DIR . '/' . self::ATTACHMENT_DIR);

        $image = file_get_contents($url);

        $file_path = '/' . self::ATTACHMENT_DIR . '/' . $filename;

        if ($image !== false) {
            file_put_contents(MEDIA_DIR . $file_path, $image);
        } else {
            return null;
        }

        return $file_path;
    }

    public function getAttachmentImageTag($attachment_id, $alt_text = null, $title = null, $width = null, $height = null, $lazy=true)
    {
        $attachment = $this->getAttachment($attachment_id);

        if (!$attachment) {
            return null;
        }

        $img_url = $this->createAttachmentSrcFromFilePath($attachment->file_path);

        if (!$img_url) {
            return null;
        }

        if ($title === null) {
            $title = $attachment->title;
        }

        if ($alt_text === null) {
            $alt_text = $attachment->alt_text;
        }

        return ImgUtils::getImageTag($img_url, $lazy, $alt_text, $title, $width, $height);
    }

    public function getAttachmentUrl($attachment_id, $default_image = null)
    {
        if ($attachment_id === null) {
            return $default_image;
        }

        $item = $this->DB->getOne("SELECT file_path FROM attachment where attachment_id = " . intval($attachment_id), true, 60);

        if ($item) {
            return $this->createAttachmentSrcFromFilePath($item->file_path);
        }

        return $default_image;
    }

    public function getAbsoluteAttachmentUrl($attachment_id, $default_image = null)
    {
        global $CONFIG;

        $image = $this->getAttachmentUrl($attachment_id);

        if ($image) {
            return $CONFIG->website_domain . $image;
        }

        return $default_image;
    }

    public function getAttachments($keyword = null, $limit=null, $offset=0)
    {
        $attachments = array();

        $query_builder = new DatabaseSelectQueryBuilder("attachment");

        if ($keyword) {
            $query_builder->conditions([
                'title LIKE "%' . $this->DB->escape($keyword) . '%"',
                'alt_text LIKE "%' . $this->DB->escape($keyword) . '%"',
                'file_name LIKE "%' . $this->DB->escape($keyword) . '%"',
                'description LIKE "%' . $this->DB->escape($keyword) . '%"'
            ], 'or');
        }

        if($limit){
            $limit_string = intval($offset) . ", " . intval($limit);
            $query_builder->limit($limit_string);
        }

        $query_builder->orders([
            "attachment_time DESC",
            "attachment_id DESC"
        ]);

        $querystring = $query_builder->buildQuery();

        $dbquery = $this->DB->query($querystring);
        while ($item = $dbquery->fetchObject()) {
            $item->src = $this->createAttachmentSrcFromFilePath($item->file_path);
            $attachments[] = $item;
        }

        return $attachments;
    }

    public function getAttachmentsForAttachmentGenerator()
    {
        $attachments = array();

        $query_builder = new DatabaseSelectQueryBuilder("attachment");

        $query_builder->conditions([
            'use_in_attachment_generator = true'
        ]);

        $query_builder->orders([
            "attachment_time DESC",
            "attachment_id DESC"
        ]);

        $querystring = $query_builder->buildQuery();

        $dbquery = $this->DB->query($querystring);
        while ($item = $dbquery->fetchObject()) {
            $item->src = $this->createAttachmentSrcFromFilePath($item->file_path);
            $attachments[] = $item;
        }

        return $attachments;
    }

    public function createAttachmentSrcFromFilePath($file_path)
    {
        return '/data/media' . $file_path;
    }
}