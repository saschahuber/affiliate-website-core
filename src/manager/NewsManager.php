<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\component\container\NewsGridContainer;
use saschahuber\affiliatewebsitecore\component\container\NewsListContainer;
use saschahuber\affiliatewebsitecore\service\BlogTaxonomyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\NewsTaxonomyThumbnailGeneratorService;
use saschahuber\affiliatewebsitecore\service\NewsThumbnailGeneratorService;
use saschahuber\saastemplatecore\helper\ImgUtils;

#[AllowDynamicProperties]
class NewsManager extends PostableManager
{
    const TABLE_NAME = "news";
    const URL_PREFIX = "news";

    const TYPE_NEWS = 'news';
    const TYPE_NEWS_CATEGORY = 'news__category';

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME, self::URL_PREFIX);
    }

    public function displayNews($news, $limit = 4, $layout = "grid", $show_detail_button = false, $show_meta = false)
    {
        if ($layout === "grid") {
            return (new NewsGridContainer($news))->getContent();
        } else {
            return (new NewsListContainer($news))->getContent();
        }
    }

    public function displayNewsItem($news_item)
    {
        $news_item_link = $this->generatePermalink($news_item);

        ob_start();
        ?>
        <div class="post-item text-center">
            <?= $this->getFavouriteIcon($news_item->id) ?>
            <a href="<?= $news_item_link ?>">
                <?=ImgUtils::getImageTag($this->getThumbnailSrc($news_item), true, $news_item->title, $news_item->title, 750, null, ['post-thumbnail'])?>
                <div class="post-data">
                    <span class="post-title"><?= $news_item->title ?></span>
                    <!--<p><?= $this->getExcerpt($news_item->content, true, 150) ?></p>-->
                </div>
            </a>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getNewNews($since_date)
    {
        $items = array();

        if(!$since_date || strlen($since_date) < 1){
            $since_date = '2000-01-01 00:00:00';
        }

        $dbquery = $this->DB->query('SELECT * FROM news 
            where status = "publish" and news_date >= "' . $this->DB->escape($since_date) . '" and news_date <= CURRENT_TIMESTAMP()
            and hide_in_newsletter != 1
            ORDER BY news_date DESC');

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
            $this->addMeta($item);
        }
        return $items;
    }

    public function getThumbnailSrc($item)
    {
        global $CONFIG;

        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        } else {
            $thumbnail_src = (new NewsThumbnailGeneratorService())->getThumbnailSrc($item);
        }
        return $thumbnail_src;
    }

    public function getTaxonomyThumbnailSrc($item)
    {
        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        } else {
            $thumbnail_src = (new NewsTaxonomyThumbnailGeneratorService())->generateNewsTaxonomyThumbnail($item);
        }
        return $thumbnail_src;
    }

    public function getByExternalIdentifier($external_identifier)
    {
        return $this->getByX('external_identifier', $external_identifier);
    }

    public function isAlreadyImported($external_identifier)
    {
        return $this->getByExternalIdentifier($external_identifier) !== null;
    }

    public function isAlreadyInImportedTable($external_identifier)
    {
        return $this->DB->getOne('SELECT count(*) as anzahl FROM news__feed_import_item where guid = "'.$this->DB->escape($external_identifier).'"')->anzahl > 0;
    }

    public function getThumbnail($item)
    {
        return ImgUtils::getImageTag($this->getThumbnailSrc($item));
    }

    public function getByTaxonomyAliasMulti($aliase, $reduced = false, $limit = false, $orderby = null, $desc = true, $field = null)
    {
        $querystring = 'SELECT news.* FROM news 
            JOIN news__taxonomy_mapping 
                ON news__taxonomy_mapping.news_id = news.news_id
            JOIN news__taxonomy 
                ON news__taxonomy_mapping.taxonomy_id = news__taxonomy.news__taxonomy_id
            where news__taxonomy.permalink IN ("' . implode('", "', $aliase) . '") 
            and news.status = "publish" 
            and news_date < CURRENT_TIMESTAMP()
            order by news_date desc'
            #.(Helper::ifstr($orderby!==null, (' ORDER BY '.$this->getOrderByClause($orderby, $desc, $field))))
            . (($limit && $limit>0) ? " LIMIT " . intval($limit) : "");

        return $this->getAllWithMeta($this->DB->getAll($querystring));
    }

    public function addMeta($item, $meta = null, $taxonomies = null)
    {
        parent::addMeta($item, $meta, $taxonomies);

        $item->full_permalink = $this->generatePermalink($item);
        $item->thumbnail_src = $this->getThumbnailSrc($item);
        $item->excerpt = $this->getExcerpt($item->content, true, 150);
    }
}