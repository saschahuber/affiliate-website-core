<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\ProductComparisonThumbnailGeneratorService;
use saschahuber\saastemplatecore\helper\ImgUtils;

#[AllowDynamicProperties]
class ProductComparisonManager extends PostableManager
{
    const TABLE_NAME = "product_comparison";

    const TYPE_PRODUCT_COMPARISON = "product_comparison";
    const URL_PREFIX = "produktvergleich";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME, self::URL_PREFIX);
    }

    public function addMeta($item, $meta = null, $taxonomies = null){}

    public function setMeta($item_id, $meta_key, $meta_value){}

    public function getBulkMeta($item_ids)
    {
        return array();
    }

    public function getMeta($item_id, $meta_key = null)
    {
        return array();
    }

    public function displayPosts($posts, $limit = 4, $layout = "grid", $show_detail_button = false, $show_meta = false)
    {
        global $LAYOUT_MANAGER;

        $contents = [];

        foreach ($posts as $post) {
            $contents[] = $this->displayPost($post);
        }

        return $LAYOUT_MANAGER->grid($contents, 3, "lg", ['post-grid']);
    }

    public function displayPost($post)
    {
        $post_link = $this->generatePermalink($post);

        ob_start();
        ?>
        <div class="post-item text-center">
            <a href="<?= $post_link ?>">
                <?=ImgUtils::getImageTag($this->getThumbnailSrc($post), true, $post->title, $post->title, 750, null, ['post-thumbnail'])?>
                <div class="post-data">
                    <span class="post-title"><?= $post->title ?></span>
                </div>
            </a>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getThumbnailSrc($item)
    {
        global $CONFIG;

        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        } else {
            $thumbnail_src = (new ProductComparisonThumbnailGeneratorService())->getThumbnailSrc($item);
        }
        return $thumbnail_src;
    }

    public function getThumbnail($item)
    {
        return ImgUtils::getImageTag($this->getThumbnailSrc($item));
    }
}