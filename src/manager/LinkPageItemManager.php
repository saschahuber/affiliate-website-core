<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\service\ImageService;

#[AllowDynamicProperties]
class LinkPageItemManager extends Manager
{
    const TABLE_NAME = "link_page_item";
    const URL_PREFIX = "";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
        $this->image_manager = new ImageManager();
        $this->image_service = new ImageService();
    }

    public function getAll($status = null)
    {
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);
        if ($status !== null) {
            $query_builder->conditions([
                'status = "' . $this->DB->escape($status) . '"',
                'link_page_item_date <= CURRENT_TIMESTAMP()'
            ]);
        }
        $query_builder->order('link_page_item_date DESC');

        return $this->DB->getAll($query_builder->buildQuery());
    }

    public function getThumbnailSrc($item)
    {
        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        }
        return $thumbnail_src;
    }

    public function getGridItem($item)
    {
        $img_content = BufferHelper::buffered(function () use ($item) {
            ?>
            <div style="text-align: center">
                <?php
                if ($item->linked_item_type == 'product' && $item->linked_item_id && $item->attachment_id === null) {
                    $product_manager = AffiliateInterfacesHelper::getProductManager();
                    $product = $product_manager->getById($item->linked_item_id);

                    echo $product_manager->getProductImageTag($product);
                } else if ($item->linked_item_type == 'post' && $item->linked_item_id && $item->attachment_id === null) {
                    $post_manager = new PostManager();
                    $product = $post_manager->getById($item->linked_item_id);

                    (new Image($post_manager->getThumbnailSrc($product)))->display();
                } else if ($item->attachment_id !== null) {
                    echo (new ImageManager())->getAttachmentImageTag($item->attachment_id);
                }
                ?>
            </div>
            <?php
        });

        $content = new Row([
            new Column([$img_content], ['col-md-4', 'col-12']),
            new Column(new GridContainer([
                new Text($item->title, 'strong'),
                $item->content,
                new BreakComponent(),
                $this->getLinkButton($item)->getContent()
            ], 1), ['col-md-8', 'col-12'])
        ]);

        return new Card($content);
    }

    public function getLinkButton($item)
    {
        global $CONFIG;

        $new_tab = true;

        $manager = null;
        $linked_item = null;
        $button_url = "/";
        $button_text = "Zum Artikel";

        switch ($item->linked_item_type) {
            case PostManager::TYPE_POST:
                $manager = new PostManager();
                $button_text = "Zum Artikel";
                break;
            case NewsManager::TYPE_NEWS:
                $manager = new NewsManager();
                $button_text = "Zum News-Artikel";
                break;
            case ProductManager::TYPE_PRODUCT:
                $manager = AffiliateInterfacesHelper::getProductManager();
                $button_text = "Zum Produkt";
                break;
        }

        if ($manager) {
            $linked_item = $manager->getById($item->linked_item_id);
        }

        if ($item->button_text) {
            $button_text = $item->button_text;
        }

        if (!$item->button_url) {
            if ($manager && $linked_item && $linked_item->status == Manager::STATUS_PUBLISH) {
                $button_url = $CONFIG->website_domain . $manager->generatePermalink($linked_item);
            }
        } else {
            $button_url = $item->button_url;
        }

        return new LinkButton($button_url, $button_text, 'fa fa-chevron-right', true, $new_tab);
    }

    public function getByLinkedItemTypeAndId($item_type, $item_id)
    {
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder->conditions([
            'linked_item_type = "' . $this->DB->escape($item_type) . '"',
            'linked_item_id = ' . intval($item_id)
        ]);

        return $this->DB->getOne($query_builder->buildQuery());
    }

    public function getAllByLinkedItemTypeAndId($item_type, $item_id)
    {
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);

        $query_builder->conditions([
            'linked_item_type = "' . $this->DB->escape($item_type) . '"',
            'linked_item_id = ' . intval($item_id)
        ]);

        return $this->DB->getAll($query_builder->buildQuery());
    }
}