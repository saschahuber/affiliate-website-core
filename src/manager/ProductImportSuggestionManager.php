<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;

#[AllowDynamicProperties]
class ProductImportSuggestionManager extends Manager
{
    const BASE_TABLE_NAME = 'product__import_suggestion';

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
    }

    public function suggestionExists($product_id, $feed_id, $external_identifier){
        $query_builder = new DatabaseSelectQueryBuilder(self::BASE_TABLE_NAME);
        $query_builder->conditions([
            'product_id = '.intval($product_id),
            'feed_id = '.intval($feed_id),
            'external_product_id = "'.$this->DB->escape($external_identifier).'"'
        ]);

        return $this->DB->getOne($query_builder->buildQuery()) !== null;
    }

    function getAll(){
        $querystring = "SELECT * FROM " . $this->base_table_name . " ORDER BY suggestion_time ASC";
        return $this->DB->getAll($querystring);
    }

    public function deleteByProductId($id){
        $querystring = "DELETE FROM " . $this->base_table_name
            . " WHERE product_id = ".intval($id);

        return $this->DB->query($querystring);
    }
}