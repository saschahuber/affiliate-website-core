<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\Helper;

#[AllowDynamicProperties]
class LayoutManager
{
    public function __construct(){}

    public function container($rows, $additional_classes = null)
    {
        $additional_classes_str = false;
        if ($additional_classes !== null && is_array($additional_classes) === true) {
            $additional_classes_str = implode(' ', $additional_classes);
        }

        ob_start();
        ?>
        <div class="<?= ($additional_classes ? $additional_classes_str : "") ?>">
            <?php
            foreach ($rows as $row) {
                echo $row;
            }
            ?>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    /**
     * @param $columns
     * @param null $alignment => align-items-start, align-items-center, align-items-end
     * @return false|string
     */
    public function row($columns, $alignment = null)
    {
        return (new Row($columns))->getContent();
    }

    /**
     * @param $content
     * @param null $width => 2, 3, 4, 6, 8, 9, 12
     * @param null $size => sm, md, lg
     * @return false|Column|string
     */
    public function column($content, $width = null, $size = null, $additional_classes = [])
    {
        $col_class = "col" . Helper::ifstr($size !== null, "-" . $size) . Helper::ifstr($width !== null, "-" . $width);

        return (new Column($content, array_merge($additional_classes, [$col_class, 'col-12'])))->getContent();
    }

    public function columns($contents, $width = null, $size = null, $row_alignment = null)
    {
        $columns = [];
        foreach ($contents as $content) {
            $columns[] = $this->column($content, $width, $size);
        }

        return $this->container(array($this->row($columns, $row_alignment)));
    }

    /**
     * @param $contents
     * @param int $column_count
     * @param null $size => sm, md, lg
     * @param null $additional_classes
     * @return false|string
     */
    public function grid($contents, $column_count = 4, $size = null, $additional_classes = null)
    {
        $width = intval(12 / $column_count);

        $rows = [];
        foreach (array_chunk($contents, $column_count) as $chunk) {
            $columns = [];
            foreach ($chunk as $content) {
                $columns[] = $this->column($content, $width, $size);
            }
            $rows[] = $this->row($columns);
        }

        return $this->container($rows, $additional_classes);
    }
}