<?php

namespace saschahuber\affiliatewebsitecore\manager;

class NewsFeedImportManager extends Manager
{
    const TABLE_NAME = "news__feed_import";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    function getAll()
    {
        $querystring = "SELECT * FROM " . $this->base_table_name;
        return $this->DB->getAll($querystring);
    }

    public function getAllActive()
    {
        $querystring = "SELECT * FROM " . $this->base_table_name . " WHERE active = 1";
        return $this->DB->getAll($querystring);
    }
}