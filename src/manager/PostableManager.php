<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\FavouriteHelper;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\service\ImageService;
use stdClass;

#[AllowDynamicProperties]
abstract class PostableManager extends Manager
{
    public function __construct($base_table_name, $url_prefix)
    {
        parent::__construct($base_table_name);

        $this->url_prefix = $url_prefix;
        $this->image_manager = new ImageManager();
        $this->image_service = new ImageService();
        $this->ad_manager = new AdManager();
    }

    public function getScheduled($limit = 10)
    {
        $query_builder = new DatabaseSelectQueryBuilder($this->base_table_name);

        $query_builder->conditions([
            $this->base_table_name . '_date > CURRENT_TIMESTAMP()',
            'status = "' . Manager::STATUS_PUBLISH . '"'
        ])
            ->order($this->base_table_name . '_date ASC')
            ->limit($limit);

        return $this->DB->getAll($query_builder->buildQuery());
    }

    public function getAllWithMeta($item_list)
    {
        $items = [];

        $item_ids = [];
        foreach ($item_list as $item) {
            $item_ids[] = $item->{$this->base_table_name . '_id'};
        }

        $meta_data = $this->getBulkMeta($item_ids);

        foreach ($item_list as $item) {
            if (isset($meta_data[$item->{$this->base_table_name . '_id'}])) {
                $this->addMeta($item, $meta_data[$item->{$this->base_table_name . '_id'}]);
            } else {
                $this->addMeta($item);
            }
            $items[] = $item;
        }

        return $items;
    }

    public function hasTaxonomyId($item, $taxonomy_id)
    {
        foreach ($item->taxonomies as $taxonomy) {
            if ($taxonomy->id === $taxonomy_id) {
                return true;
            }
        }
        return false;
    }

    public function hasTaxonomyPermalink($item, $taxonomy_permalink)
    {
        foreach ($item->taxonomies as $taxonomy) {
            if ($taxonomy->permalink === $taxonomy_permalink) {
                return true;
            }
        }
        return false;
    }

    public function cleanItem($item)
    {
        $fields_to_remove = ['formatted_date', 'amazon_link', 'review_score', 'full_permalink', 'thumbnail_src', 'excerpt', 'is_already_published'];
        foreach ($fields_to_remove as $field) {
            unset($item->{$field});
        }

        $nullable_fields = array("meta_title", "meta_description", "og_title", "og_description");
        foreach ($nullable_fields as $nullable_field) {
            if (isset($item->{$nullable_field}) && strlen($item->{$nullable_field}) < 1) {
                $item->{$nullable_field} = null;
            }
        }

        return $item;
    }

    public function setMeta($item_id, $meta_key, $meta_value)
    {
        $this->DB->query('REPLACE INTO ' . $this->base_table_name . '__meta (' . $this->base_table_name . '_id, meta_key, meta_value) 
            VALUES (' . $this->DB->escape($item_id) . ', 
                "' . $this->DB->escape($meta_key) . '", 
                "' . $this->DB->escape($meta_value) . '")');
    }

    public function getBulkMeta($item_ids)
    {
        $meta_data = array();

        foreach ($item_ids as $id) {
            $meta_data[$id] = [];
        }

        if (count($item_ids) > 0) {
            $querystring = 'SELECT * FROM ' . $this->base_table_name . '__meta 
            where ' . $this->base_table_name . '_id IN (' . implode(', ', $item_ids) . ')';

            $items = $this->DB->getAll($querystring, true, 15);

            foreach ($items as $item) {
                $item_id = $item->{$this->base_table_name . '_id'};
                $meta_data[$item_id][$item->meta_key] = $item->meta_value;
            }
        }

        return $meta_data;
    }

    public function getMeta($item_id, $meta_key = null)
    {
        $querystring = 'SELECT * FROM ' . $this->base_table_name . '__meta where ' . $this->base_table_name . '_id = '
            . $this->DB->escape($item_id)
            . Helper::ifstr($meta_key !== null, " and meta_key = '$meta_key'");

        $meta_items = $this->DB->getAll($querystring, true, 15);

        if ($meta_key !== null) {
            if (count($meta_items) === 1) {
                return $meta_items[0]->meta_value;
            }
            if (count($meta_items) === 0) {
                return null;
            }
        }

        return $meta_items;
    }

    public function getTaxonomyMetaTitle($item)
    {
        global $CONFIG;

        $default_meta_title = "{$item->title} | {$CONFIG->app_name}";

        $meta_title = (isset($item->meta_title) && strlen($item->meta_title)) ? $item->meta_title : $default_meta_title;

        return $meta_title;
    }

    public function getTaxonomyMetaDescription($item)
    {
        global $CONFIG;

        $default_meta_description = "Stöbere in der Kategorie '{$item->title}' auf {$CONFIG->app_name}!";

        $meta_description = (isset($item->meta_description) && strlen($item->meta_description)) ? $item->meta_description : $default_meta_description;

        return $meta_description;
    }

    public function getMetaTitle($item)
    {
        global $CONFIG;

        $default_meta_title = "{$item->title} | {$CONFIG->app_name}";
        $meta_title = (isset($item->meta_title) && strlen($item->meta_title))
            ? $item->meta_title
            : $default_meta_title;

        $meta_title = str_replace("%%title%%", $item->title, $meta_title);
        $meta_title = str_replace("%%sep%%", '|', $meta_title);
        $meta_title = str_replace("%%page%%", '', $meta_title);
        $meta_title = str_replace("%%sitename%%", $CONFIG->app_name, $meta_title);

        return $meta_title;
    }

    public function getMetaDescription($item)
    {
        global $CONFIG;

        $default_meta_description = "Lies den Beitrag '{$item->title}' auf {$CONFIG->app_name}!";
        $meta_description = (isset($item->meta_description) && strlen($item->meta_description))
            ? $item->meta_description
            : $default_meta_description;

        str_replace("%%title%%", $item->title, $meta_description);
        str_replace("%%sep%%", '|', $meta_description);
        str_replace("%%page%%", '', $meta_description);
        str_replace("%%sitename%%", $CONFIG->app_name, $meta_description);

        return $meta_description;
    }

    public function getDate($item)
    {
        $date = $item->formatted_date;

        return $date;
    }

    public function getTaxonomyLinks($item)
    {
        $links = [];

        foreach ($item->taxonomies as $tax) {
            $links[] = '<a class="taxonomy-button" href="' . $this->getTaxonomyPermalink($tax) . '">' . $tax->title . '</a>';
        }

        return implode(', ', $links);
    }

    public function getFiltered($keyword = null, $status = null, $taxonomies = null, $order_by = null, $desc = true, $limit = null, $offset = 0)
    {
        $items = array();

        $filter_clauses = [];

        if ($keyword) {
            $filter_clauses[] = 'title LIKE "%' . $this->DB->escape($keyword) . '%"';
        }

        if ($status) {
            $filter_clauses[] = 'status = "' . $this->DB->escape($status) . '"';
        }


        if ($taxonomies) {
            $filter_clauses[] = $this->base_table_name . '__taxonomy_mapping.taxonomy_id IN (' . implode(', ', $taxonomies) . ')';
        }

        $order_clause = false;
        if ($order_by !== null) {
            $order_clause = ' ORDER BY ' . $this->DB->escape($order_by) . " " . ($desc ? "desc" : "asc");
        }

        $querystring = 'SELECT ' . $this->base_table_name . '.*, 
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d. %M %Y") as formatted_date 
            FROM ' . $this->base_table_name
            . ' LEFT JOIN ' . $this->base_table_name . '__taxonomy_mapping 
            on ' . $this->base_table_name . '__taxonomy_mapping.' . $this->base_table_name . '_id = ' . $this->base_table_name . '.' . $this->base_table_name . '_id'
            . ((count($filter_clauses) > 0) ? " WHERE " . implode(' AND ', $filter_clauses) : "")
            . ' GROUP BY ' . $this->base_table_name . '.' . $this->base_table_name . '_id'
            . ($order_clause ?: "");

        if ($limit) {
            $querystring .= " LIMIT " . intval($limit);
        }

        if ($offset) {
            $querystring .= " OFFSET " . intval($offset);
        }

        $items = $this->DB->getAll($querystring, true, 15);

        return $this->getAllWithMeta($items);
    }

    public function getAll($status = null, $add_meta = true, $order_by = null, $desc = true, $limit = null, $offset = 0)
    {
        $items = array();

        $filter_clause = false;
        if ($status !== null) {
            $filter_clause = ' where status = "' . $this->DB->escape($status) . '"';
        }

        $order_clause = false;
        if ($order_by !== null) {
            $order_clause = ' ORDER BY ' . $this->DB->escape($order_by) . " " . ($desc ? "desc" : "asc");
        }

        $querystring = 'SELECT *,
            (' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published,
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d. %M %Y") as formatted_date 
            FROM ' . $this->base_table_name . ($filter_clause ?: "") . ($order_clause ?: "");

        if ($limit) {
            $querystring .= " LIMIT " . intval($limit);
        }

        if ($offset) {
            $querystring .= " OFFSET " . intval($offset);
        }

        #die($querystring);

        $items = $this->DB->getAll($querystring, true, 15);

        return $this->getAllWithMeta($items);
    }

    public function getFavourites()
    {
        global $USER_AUTH_SERVICE;

        $items = array();

        $dbquery = $this->DB->query('SELECT *, 
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d. %M %Y") as formatted_date 
            FROM ' . $this->base_table_name . '
            join user__favourite 
                on user__favourite.favourite_element_id = ' . $this->base_table_name . '.' . $this->base_table_name . '_id
                and user__favourite.favourite_element_type = "' . $this->base_table_name . '"
                and user__favourite.user_id = ' . intval($USER_AUTH_SERVICE->logged_in_user->id) . '
            where status = "publish"');

        while ($item = $dbquery->fetchObject()) {
            $items[] = $item;
        }

        return $this->getAllWithMeta($items);
    }

    public function generatePermalink($item)
    {
        #Pfad-Cachen für Item-ID

        if (!$item) {
            LogHelper::error("HTTP", 404, "Das Element wurde nicht gefunden.", ERROR_EXIT);
        }

        $taxonomies = [];

        $first_taxonomy = null;

        if (isset($item->taxonomies) && count($item->taxonomies)) {
            $taxonomies = $item->taxonomies;
            $first_taxonomy = $taxonomies[count($taxonomies) - 1];
        }

        foreach ($taxonomies as $taxonomy) {
            if ($taxonomy->is_primary_taxonomy === 1) {
                $first_taxonomy = $taxonomy;
                break;
            }
        }

        return Helper::ifstr($this->url_prefix !== null, '/' . $this->url_prefix)
            . Helper::ifstr($first_taxonomy !== null, $this->getTaxonomyPath($first_taxonomy)) . '/' . $item->permalink;
    }

    public function getTaxonomyPermalink($taxonomy)
    {
        return Helper::ifstr($this->url_prefix !== null, '/' . $this->url_prefix) . $this->getTaxonomyPath($taxonomy);
    }

    private function getTaxonomyPath($taxonomy)
    {
        if (!$taxonomy) {
            return null;
        }

        $taxonomies = $this->getTaxonomies();

        #$permalink_map = [];
        $id_map = [];
        foreach ($taxonomies as $tax) {
            #$permalink_map[$tax->permalink] = $tax;
            $id_map[$tax->id] = $tax;
        }

        $path = [$taxonomy->permalink];
        while ($taxonomy->taxonomy_parent_id) {
            $taxonomy = $id_map[$taxonomy->taxonomy_parent_id];
            if (!in_array($taxonomy->permalink, $path)) {
                array_unshift($path, $taxonomy->permalink);
            }
        }

        return '/' . implode('/', $path);
    }

    public function getExcerpt($content, $strip_tags = true, $max_len = 450)
    {
        $content = ShortcodeHelper::doShortcode($content);

        if ($strip_tags) {
            $content = strip_tags($content);
        }

        return Helper::shorten($content, $max_len);
    }

    public function getByPermalink($permalink, $add_meta = true)
    {
        return $this->getByX("permalink", $permalink, $add_meta);
    }

    public function getAllByAuthor($author_id, $add_meta = true)
    {
        #Cache integrieren

        $querystring = 'SELECT *,
            (' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published,
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d.%m.%Y") as formatted_date
            FROM ' . $this->base_table_name . ' 
            where status = "'.Manager::STATUS_PUBLISH.'" and author_id = ' . intval($author_id);

        $items = [];
        foreach($this->DB->getAll($querystring, true, 15) as $item){
            if($add_meta) {
                $this->addMeta($item);
            }
            $items[] = $item;
        }
        return $items;
    }

    public function getByTitle($title, $add_meta = true)
    {
        return $this->getByX("title", $title, $add_meta);
    }

    public function getById($id, $add_meta = true)
    {
        return $this->getByX("{$this->base_table_name}_id", $id, $add_meta, $intval = true);
    }

    public function getByIds($ids, $add_meta = true)
    {
        $ids = array_map('intval', $ids);

        #Cache integrieren

        $querystring = 'SELECT *,
            (' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published,
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d.%m.%Y") as formatted_date
            FROM ' . $this->base_table_name . ' 
            where ' . $this->base_table_name . '_id IN (' . implode(',', $ids) . ')';

        $items = $this->DB->getAll($querystring, true, 60);

        if ($add_meta) {
            foreach ($items as $item) {
                $this->addMeta($item);
            }
        }

        return $items;
    }

    protected function getByX($search_col, $value, $add_meta = true, $intval = false)
    {
        $item = parent::getByX($search_col, $value, $intval);

        if (!$item) {
            return null;
        }

        if ($add_meta === true) {
            $this->addMeta($item);
        }

        setTheId($item->id);
        setTheItem($item);

        return $item;
    }

    protected function getAllByX($search_col, $value, $add_meta = true, $intval = false)
    {
        $items = [];
        foreach(parent::getAllByX($search_col, $value, $intval) as $item){
            if($add_meta) {
                $this->addMeta($item);
            }
            $items[] = $item;
        }
        return $items;
    }

    public function getByTaxonomyId($taxonomy_id)
    {
        #Cache integrieren

        $querystring = 'SELECT ' . $this->base_table_name . '.*,
            (' . $this->base_table_name . '_date <= NOW() AND status = "publish") as is_already_published
            FROM ' . $this->base_table_name . '
            JOIN ' . $this->base_table_name . '__taxonomy_mapping ON ' . $this->base_table_name . '.' . $this->base_table_name . '_id = ' . $this->base_table_name . '__taxonomy_mapping.' . $this->base_table_name . '_id
            where (' . $this->base_table_name . '_date <= NOW() AND status = "publish") and ' . $this->base_table_name . '__taxonomy_mapping.taxonomy_id = ' . $this->DB->escape($taxonomy_id) . '
            ORDER BY ' . $this->base_table_name . '.' . $this->base_table_name . '_date DESC';

        $items = $this->DB->getAll($querystring, true, 15);

        return $this->getAllWithMeta($items);
    }

    public function getByPermalinkMulti($permalink_list)
    {
        $permalink_query_list = array_map(array($this->DB, 'escape'), $permalink_list);

        $querystring = 'SELECT ' . $this->base_table_name . '.* FROM ' . $this->base_table_name . '
            where permalink in ("' . implode('","', $permalink_query_list) . '");';

        $items = $this->DB->getAll($querystring, true, 15);

        return $this->getAllWithMeta($items);
    }

    public function getTaxonomyByPermalinkMulti($permalink_list)
    {
        $permalink_query_list = array_map(array($this->DB, 'escape'), $permalink_list);

        $querystring = 'SELECT ' . $this->base_table_name . '__taxonomy.* FROM ' . $this->base_table_name . '__taxonomy
            where permalink in ("' . implode('","', $permalink_query_list) . '");';

        return $this->DB->getAll($querystring, true, 15);
    }

    public function getTaxonomyByPermalink($permalink)
    {
        $querystring = 'SELECT ' . $this->base_table_name . '__taxonomy.* FROM ' . $this->base_table_name . '__taxonomy
            where permalink = "'.$this->DB->escape($permalink).'";';

        return $this->DB->getOne($querystring, true, 15);
    }

    public function addMeta($item, $meta = null, $taxonomies = null)
    {
        if ($item !== false) {
            if ($meta === null) {
                $meta = [];

                $querystring = 'SELECT * FROM ' . $this->base_table_name . '__meta 
                where ' . $this->base_table_name . '_id = ' . $this->DB->escape($item->id);

                $meta_items = $this->DB->getAll($querystring, true, 15);
                foreach ($meta_items as $meta_item) {
                    $meta[$meta_item->meta_key] = $meta_item->meta_value;
                }
            }
            $item->meta = $meta;
        }

        if ($item !== false) {
            if ($taxonomies === null) {
                $querystring = 'SELECT * FROM ' . $this->base_table_name . '__taxonomy
                JOIN ' . $this->base_table_name . '__taxonomy_mapping
                    ON ' . $this->base_table_name . '__taxonomy_mapping.taxonomy_id = ' . $this->base_table_name . '__taxonomy.' . $this->base_table_name . '__taxonomy_id
                where ' . $this->base_table_name . '_id = ' . $this->DB->escape($item->id);

                $taxonomies = $this->DB->getAll($querystring, true, 15);
            }
            $item->taxonomies = $taxonomies;
        }
    }

    public function getTaxonomies($order_by = "title", $asc = true, $status = PostableManager::STATUS_PUBLISH)
    {
        $order_by_query = false;
        switch ($order_by) {
            case "title":
                $order_by_query = "title";
        }
        if ($order_by_query) {
            $order_by_query .= ' ' . ($asc ? 'ASC' : 'DESC');
        }

        #Cache integrieren
        $taxonomies = [];

        $query_builder = new DatabaseSelectQueryBuilder($this->base_table_name . '__taxonomy');
        if ($status !== null) {
            $query_builder->conditions('status = "' . $this->DB->escape($status) . '"');
        }

        if ($order_by_query) {
            $query_builder->order($order_by_query);
        }

        $items = $this->DB->getAll($query_builder->buildQuery(), true, 15);

        foreach ($items as $taxonomy) {
            $taxonomies[$taxonomy->id] = $taxonomy;
        }

        return $taxonomies;
    }

    public function getDataFields()
    {
        #Cache integrieren
        $data_fields = [];
        $items = $this->DB->getAll('SELECT * FROM ' . $this->base_table_name . '__data_field', true, 15);
        foreach ($items as $data_field) {
            $data_fields[$data_field->id] = $data_field;
        }

        return $data_fields;
    }

    public function getDataFieldGroups()
    {
        #Cache integrieren
        $data_field_groups = [];
        $dbquery = $this->DB->query('SELECT * FROM ' . $this->base_table_name . '__data_field_group');
        while ($data_field_group = $dbquery->fetchObject()) {
            $data_field_groups[$data_field_group->id] = $data_field_group;
        }

        return $data_field_groups;
    }

    public function getTaxonomyById($id)
    {
        $dbquery = $this->DB->query('SELECT * FROM ' . $this->base_table_name . '__taxonomy where ' . $this->base_table_name . '__taxonomy_id = ' . $this->DB->escape($id));
        while ($taxonomy = $dbquery->fetchObject()) {
            return $taxonomy;
        }
        return null;
    }

    public function getTaxonomiesByIds($ids)
    {
        if (count($ids) < 1) {
            return [];
        }

        $taxonomies = [];
        $dbquery = $this->DB->query('SELECT * FROM ' . $this->base_table_name . '__taxonomy 
            where ' . $this->base_table_name . '__taxonomy_id IN (' . implode(', ', $ids) . ')');
        while ($taxonomy = $dbquery->fetchObject()) {
            $taxonomies[$taxonomy->id] = $taxonomy;
        }

        return $taxonomies;
    }

    public function getTaxonomiesByOldIds($ids)
    {
        $taxonomies = [];
        $dbquery = $this->DB->query('SELECT * FROM ' . $this->base_table_name . '__taxonomy 
            where ' . $this->base_table_name . '__taxonomy_id_old IN (' . implode(', ', $ids) . ')');
        while ($taxonomy = $dbquery->fetchObject()) {
            $taxonomies[$taxonomy->id] = $taxonomy;
        }

        return $taxonomies;
    }

    public function createTaxonomy($item)
    {
        $item = $this->cleanItem($item);
        $this->DB->insertFromObject($this->base_table_name . '__taxonomy', $item);
        return $this->DB->insert_id;
    }

    public function updateTaxonomy($item)
    {
        $item = $this->cleanItem($item);
        $this->DB->updateFromObject($this->base_table_name . '__taxonomy', $item);
    }

    public function setTaxonomies($item, $taxonomies, $primary_taxonomy=null)
    {
        $item_taxonomy_ids = array();
        $new_taxonomy_ids = array();

        if (isset($item->taxonomies)) {
            foreach ($item->taxonomies as $taxonomy) {
                $item_taxonomy_ids[] = $taxonomy->id;
            }
        }

        foreach ($taxonomies as $taxonomy) {
            $new_taxonomy_ids[] = $taxonomy->id;
        }

        $taxonomies_to_add = array();
        $taxonomies_to_remove = array();


        foreach ($new_taxonomy_ids as $taxonomy) {
            if (!in_array($taxonomy, $item_taxonomy_ids)) {
                $taxonomies_to_add[] = $taxonomy;
            }
        }

        foreach ($item_taxonomy_ids as $taxonomy) {
            if (!in_array($taxonomy, $new_taxonomy_ids)) {
                $taxonomies_to_remove[] = $taxonomy;
            }
        }

        $this->removeTaxonomies($item, $taxonomies_to_remove);
        $this->addTaxonomies($item, $taxonomies_to_add);

        if($primary_taxonomy) {
            $this->DB->query('UPDATE ' . $this->base_table_name . '__taxonomy_mapping set is_primary_taxonomy = 0
            where ' . $this->base_table_name . '_id = ' . intval($item->id));

            $this->DB->query('UPDATE ' . $this->base_table_name . '__taxonomy_mapping set is_primary_taxonomy = 1
            where ' . $this->base_table_name . '_id = ' . intval($item->id) . ' AND taxonomy_id = ' . intval($primary_taxonomy));
        }
    }

    public function addTaxonomies($item, $taxonomy_ids)
    {
        foreach ($taxonomy_ids as $taxonomy_id) {
            $taxonomy_mapping = new stdClass();
            $taxonomy_mapping->{$this->base_table_name . '_id'} = $item->id;
            $taxonomy_mapping->taxonomy_id = $taxonomy_id;
            $this->DB->insertFromObject($this->base_table_name . '__taxonomy_mapping', $taxonomy_mapping);
        }
    }

    public function removeTaxonomies($item, $taxonomy_ids)
    {
        if (count($taxonomy_ids) < 1) {
            return;
        }

        $this->DB->query('DELETE FROM ' . $this->base_table_name . '__taxonomy_mapping 
            where ' . $this->base_table_name . '_id = ' . $item->id . ' 
            AND taxonomy_id IN (' . implode(', ', $taxonomy_ids) . ')');
    }

    public function findAd($ad_type, $content, $taxonomies){
        return $this->ad_manager->findMostFittingAd($ad_type, $this->base_table_name,
            $content, $taxonomies);
    }

    public function displayAd($ad_type, $content, $taxonomies)
    {
        $fitting_ad = $this->findAd($ad_type, $content, $taxonomies);

        if ($fitting_ad === false) {
            return null;
        }

        return $this->ad_manager->getAdContent($fitting_ad);
    }

    public function getFavouriteIcon($item_id)
    {
        global $CONFIG;

        if (!$CONFIG->allow_user_login && !$CONFIG->allow_customer_login) {
            return null;
        }

        $type = $this->base_table_name;

        global $USER_AUTH_SERVICE;
        ob_start();
        ?>
        <?php if ($USER_AUTH_SERVICE !== null && $USER_AUTH_SERVICE->isLoggedIn()): ?>
        <span class="favourite-icon" onclick="return false;">
                <i class="fa fa-heart <?= Helper::ifstr(FavouriteHelper::isFavourite($type, $item_id), 'active') ?>"
                   data-tooltip="<?= (FavouriteHelper::isFavourite($type, $item_id) ? 'Favorit entfernen' : 'Favorit hinzufügen') ?>"
                   onmouseout="if (favTooltip) {favTooltip.disappear(); favTooltip = false;}"
                   onclick="toggleFavorite(this, event, '<?= $type ?>', '<?= $item_id ?>');"></i>
            </span>
    <?php else: ?>
        <span class="favourite-icon" onclick="return false;">
                <i class="fa fa-heart"
                   data-tooltip="Registriere dich jetzt um die hilfreiche Favoriten-Funktion zu nutzen."
                   onclick="location.href = '/registrieren/nutzer?favid=<?= $item_id ?>&favtype=<?= $type ?>'; event.preventDefault(); return false;"></i>
            </span>
    <?php endif; ?>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getFavouriteButton($item_id, $full_width = false)
    {
        global $CONFIG;

        if (!$CONFIG->allow_user_login && !$CONFIG->allow_customer_login) {
            return null;
        }

        $type = $this->base_table_name;

        global $USER_AUTH_SERVICE;
        ob_start();

        $icon = "fa fa-heart";
        $text = "Favorit hinzufügen";

        if(FavouriteHelper::isFavourite($type, $item_id)){
            $text = "Favorit entfernen";
        }

        if ($USER_AUTH_SERVICE->isLoggedIn()): ?>
            <span class="favourite-button" onclick="return false;">
                <a class="btn btn-primary <?= Helper::ifstr($full_width, 'btn-block') ?> btn-rounded btn-md"
                   onclick="toggleFavorite(this.childNode(), event, '<?= $type ?>', '<?= $item_id ?>');">
                    <i class="fa fa-heart <?= Helper::ifstr(FavouriteHelper::isFavourite($type, $item_id), 'active') ?>"></i> <?=$text?></a>
            </span>
        <?php else: ?>
            <span class="favourite-button" onclick="return false;">
                <a class="btn btn-primary <?= Helper::ifstr($full_width, 'btn-block') ?> btn-rounded btn-md"
                   onclick="location.href = '/registrieren/nutzer?favid=<?= $item_id ?>&favtype=<?= $type ?>'; event.preventDefault(); return false;"
                   data-tooltip="Registriere dich jetzt um die hilfreiche Favoriten-Funktion zu nutzen.">
                    <i class="fa fa-heart"></i> <?=$text?>
                </a>
            </span>
        <?php endif;

        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}