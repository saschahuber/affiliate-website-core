<?php

namespace saschahuber\affiliatewebsitecore\manager;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\Helper;
use stdClass;

class ProductDataFieldManager extends Manager
{
    const TABLE_NAME = "product__data_field";

    const TYPE_STRING = 'string';
    const TYPE_ENUM = 'enum';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_FLOAT = 'float';

    const ALLOWED_FIELD_TYPES = [
        self::TYPE_STRING => 'Text',
        self::TYPE_ENUM => 'Auswahl',
        self::TYPE_BOOLEAN => 'Ja/Nein',
        self::TYPE_FLOAT => '(Dezimal-)Zahl'
    ];

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function cleanItem($item)
    {
        return $item;
    }

    public function createDataField($field_name, $field_key, $group_id, $type, $prefix, $suffix, $order)
    {
        $data = new stdClass();
        $data->field_order = $order;
        $data->field_name = $field_name;
        $data->field_key = $field_key;
        $data->field_type = $type;
        $data->field_prefix = $prefix;
        $data->field_suffix = $suffix;
        $data->group_id = $group_id;
        $this->DB->insertFromObject('product__data_field', $data);
        return $this->DB->insert_id;
    }

    public function getDataFields($group_id = null)
    {

        $filter = null;
        if ($group_id !== null) {
            $filter = "where group_id = " . intval($group_id);
        }

        #Cache integrieren
        $data_fields = [];
        $dbquery = $this->DB->query('SELECT product__data_field.*, product__data_field_group.group_name
            FROM product__data_field 
                LEFT JOIN product__data_field_group 
                    on product__data_field.group_id = product__data_field_group.product__data_field_group_id '
            . (Helper::ifstr($filter !== null, $filter)));
        while ($data_field = $dbquery->fetchObject()) {
            $data_fields[$data_field->id] = $data_field;
        }

        return $data_fields;
    }

    public function getNumericDataFieldMinMax($data_field_id)
    {
        $conditions = [
            'value != ""',
            'value is not NULL'
        ];

        $conditions[] = 'product__data_field_id = ' . intval($data_field_id);

        $query_builder = new DatabaseSelectQueryBuilder('product__data');
        $query_builder->select('distinct(value) as value')
            ->conditions($conditions);

        $min = null;
        $max = null;

        foreach ($this->DB->getAll($query_builder->buildQuery(), true, 60) as $value) {
            $current_value = floatval(str_replace(',', '.', $value->value));

            if ($min === null || $current_value < $min) {
                $min = $current_value;
            }

            if ($max === null || $current_value > $max) {
                $max = $current_value;
            }
        }

        $min_max_data = new stdClass();
        $min_max_data->min = $min;
        $min_max_data->max = $max;

        return $min_max_data;
    }

    public function getDataFieldValueOptions($data_field_id = null)
    {
        $conditions = [
            'value != ""',
            'value is not NULL'
        ];

        if ($data_field_id) {
            $conditions[] = 'product__data_field_id = ' . intval($data_field_id);
        }

        $query_builder = new DatabaseSelectQueryBuilder('product__data');
        $query_builder->select('distinct(value) as value')
            ->conditions($conditions)
            ->order('value ASC');

        return $this->DB->getAll($query_builder->buildQuery(), true, 60);
    }

    public function getProductDataFields($product)
    {
        $taxonomy_ids = [];
        foreach ($product->taxonomies as $taxonomy) {
            $taxonomy_ids[] = $taxonomy->id;
        }

        $data_fields = [];

        $querystring = 'SELECT group_id, product__data_field.product__data_field_id, product__data.product__data_id, field_name, 
            field_type, field_description, field_values, field_prefix, field_suffix, value
            FROM product__data_field_group
            LEFT JOIN product__data_field
            	on product__data_field_group.product__data_field_group_id = product__data_field.group_id
            LEFT JOIN product__data 
                on product__data.product__data_field_id = product__data_field.product__data_field_id
            LEFT JOIN product__taxonomy_mapping 
                on product__taxonomy_mapping.product_id = product__data.product_id
            WHERE product__data.product_id = ' . intval($product->id) . ' 
            ' .
            (
            (count($taxonomy_ids) > 0)
                ? ('and product__data_field_group.product__taxonomy_id in (' . implode(', ', $taxonomy_ids) . ')') : ""
            )
            . '
            order by group_id ASC, field_order asc';

        $items = $this->DB->getAll($querystring, true, 60);

        foreach ($items as $item) {
            if (!isset($data_fields[$item->group_id])) {
                $data_fields[$item->group_id] = [];
            }

            $data_fields[$item->group_id][$item->product__data_field_id] = $item;
        }
        return $data_fields;
    }
}