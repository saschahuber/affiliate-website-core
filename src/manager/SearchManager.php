<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\service\ImageTemplateService;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\KeywordHelper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\service\ImageService;
use stdClass;

define('SEARCH_RESULT_EXCERPT_LENGTH', 300);

#[AllowDynamicProperties]
class SearchManager
{
    public function __construct()
    {
        global $DB;
        $this->DB = $DB;
        $this->image_service = new ImageService();
    }

    public function doSearch($keywords, $search_in_types = null)
    {
        if (!$keywords || strlen($keywords) < 1) {
            return [];
        }

        $results = $this->search($keywords, $search_in_types);

        $this->logSearch($keywords, $results);

        return $results;
    }

    private function search($keywords, $search_in_types = null)
    {
        if (!$keywords || strlen($keywords) < 1) {
            return [];
        }

        $query_builder = new DatabaseSelectQueryBuilder('search_index');

        $query_builder->selects([
            'search_index.*',
            'MATCH (search_title) AGAINST ("' . $this->getRuleset($keywords) . '" IN BOOLEAN MODE) as title_relevance',
            'MATCH (search_content_primary, search_content_primary) AGAINST ("' . $this->getRuleset($keywords) . '" IN BOOLEAN MODE) as content_relevance',
            'MATCH (search_meta) AGAINST ("' . $this->getRuleset($keywords) . '" IN BOOLEAN MODE) as meta_relevance',
        ]);

        $query_builder->conditions([
            'MATCH (search_title, search_content_primary, search_content_primary, search_meta) AGAINST ("' . $this->getRuleset($keywords) . '" IN BOOLEAN MODE)'
        ])
            ->orders([
                '(title_relevance * relevance_modifier) DESC',
                '(content_relevance * relevance_modifier) DESC',
                '(meta_relevance * relevance_modifier) DESC'
            ])
            ->limit(25);

        $querystring = $query_builder->buildQuery();

        return $this->DB->getAll($querystring);
    }

    private function logSearch($keyword, $results)
    {
        $search_log_entry = new stdClass();
        $search_log_entry->keyword = $keyword;
        $search_log_entry->results = count($results);
        $search_log_entry->user_ip = RequestHelper::getClientIp();
        $search_log_entry->user_agent = ArrayHelper::getArrayValue($_SERVER, 'HTTP_USER_AGENT', null);
        $search_log_entry->session_id = session_id();

        LogHelper::logToMinuteLog('Suche', json_encode($search_log_entry));

        $this->DB->insertFromObject('search_log', $search_log_entry);
    }

    private function getRuleset($keywords)
    {
        $keywords = KeywordHelper::keywords($keywords, true, false);

        if (!isset($this->ruleset)) {
            $this->ruleset = $this->DB->escape('+' . implode('* ', explode(' ', trim($keywords))) . '*');
        }
        return $this->ruleset;
    }

    public function updateSearchIndex($searchable_item_id, $searchable_item_type, $permalink, $title, $content_primary,
                                      $content_secondary = null, $meta = null, $thumbnail_url = null, $relevance_modifier = 1.0,
                                      $is_external = false)
    {
        $content_primary = strip_tags($content_primary);
        $content_secondary = $content_secondary?strip_tags($content_secondary):null;

        $ignored_shortcodes = ["apn"];

        $content_primary = ShortcodeHelper::doShortcode($content_primary, $ignored_shortcodes);
        $content_secondary = $content_secondary?ShortcodeHelper::doShortcode($content_secondary, $ignored_shortcodes):null;
        $meta = ShortcodeHelper::doShortcode($meta, $ignored_shortcodes);

        #Add tracking parameters to search-permalink
        if ($is_external === false) {
            $query = parse_url($permalink, PHP_URL_QUERY);

            if ($query) {
                $permalink .= '&utm_source=';
            } else {
                $permalink .= '?utm_source=';
            }

            $permalink .= "search_form";
        }

        $this->DB->query('INSERT INTO search_index (searchable_item_id, searchable_item_type, permalink, search_title,
                search_content_primary, search_content_secondary, search_meta, thumbnail_url, relevance_modifier, is_external)
                VALUES (
                    "' . $this->DB->escape($searchable_item_id) . '",
                    "' . $this->DB->escape($searchable_item_type) . '",
                    "' . $this->DB->escape($permalink) . '",
                    "' . $this->DB->escape($title) . '",
                    "' . $this->DB->escape($content_primary) . '",
                    ' . ($content_secondary ? ('"' . $this->DB->escape($content_secondary) . '"') : "null") . ',
                    ' . ($meta ? ('"' . $this->DB->escape($meta) . '"') : "null") . ',
                    ' . ($thumbnail_url ? ('"' . $this->DB->escape($thumbnail_url) . '"') : "null") . ',
                    ' . str_replace(',', '.', $relevance_modifier) . ',
                    ' . ($is_external ? "true" : "false") . '
                ) ON DUPLICATE KEY UPDATE 
                    permalink="' . $this->DB->escape($permalink) . '",
                    search_title="' . $this->DB->escape($title) . '",
                    search_content_primary="' . $this->DB->escape($content_primary) . '",
                    search_content_secondary=' . ($content_secondary ? ('"' . $this->DB->escape($content_secondary) . '"') : "null") . ',
                    search_meta=' . ($meta ? ('"' . $this->DB->escape($meta) . '"') : "null") . ',
                    thumbnail_url=' . ($thumbnail_url ? ('"' . $this->DB->escape($thumbnail_url) . '"') : "null") . ',
                    relevance_modifier=' . str_replace(',', '.', $relevance_modifier) . ',
                    is_external=' . ($is_external ? "1" : "0")
        );
    }

    public function deleteSearchIndex($type, $id)
    {
        $this->DB->query('DELETE FROM search_index where searchable_item_type = "' . $this->DB->escape($type) . '" and searchable_item_id = ' . intval($id));
    }

    public function deleteAllByType($type)
    {
        $this->DB->query('DELETE FROM search_index where searchable_item_type = "' . $this->DB->escape($type) . '"');
    }

    public function getExcerpt($content, $strip_tags = true, $max_len = 450)
    {
        if ($strip_tags) {
            $content = strip_tags($content);
        }

        return Helper::shorten($content, $max_len);
    }

    public function getResultHtml($result)
    {
        switch ($result->searchable_item_type) {
            case PostManager::TYPE_POST:
                $result_type = "Blog-Beitrag";
                break;
            case PostManager::TYPE_POST_CATEGORY:
                $result_type = "Blog-Kategorie";
                break;
            case NewsManager::TYPE_NEWS:
                $result_type = "News-Artikel";
                break;
            case NewsManager::TYPE_NEWS_CATEGORY:
                $result_type = "News-Kategorie";
                break;
            case ProductManager::TYPE_PRODUCT:
                $result_type = "Produkt";
                break;
            case ProductManager::TYPE_PRODUCT_CATEGORY:
                $result_type = "Produkt-Kategorie";
                break;
            case BrandManager::TYPE_BRAND:
                $result_type = "Hersteller";
                break;
            case CompanyManager::TYPE_COMPANY:
                $result_type = "Dienstleister";
                break;
            case CompanyManager::TYPE_COMPANY_CATEGORY:
                $result_type = "Dienstleister-Kategorie";
                break;
        }

        $excerpt = $this->getExcerpt($result->search_content_primary, true, SEARCH_RESULT_EXCERPT_LENGTH);

        if ($result->thumbnail_url === null) {
            $result->thumbnail_url = $this->image_service->generateImage(ImageTemplateService::SEARCH_THUMBNAIL, $result->search_title);
        }

        $onclick = null;

        if ($result->searchable_item_type == "produkt") {
            $onclick = "saveAnalyticsEvent('product_click', {product_id: {$result->searchable_item_id}, link_id: null})";
        }

        ob_start();
        ?>
        <li>
            <a class="result-link" href="<?= $result->permalink ?>" <?= $onclick ? 'onclick="' . $onclick . '"' : '' ?>
                <?= Helper::ifstr($result->is_external, 'rel="nofollow" target="_blank"') ?>>
                <div class="row">
                    <div class="col-md-4">
                        <img width="100%" src="<?= $result->thumbnail_url ?>"/>
                    </div>
                    <div class="col-md-8">
                        <span class="search-result-title"><?= strip_tags($result->search_title) ?><?= Helper::ifstr($result->is_external, ' (<i class="fas fa-info-circle"></i>&nbsp;Externer Link)') ?></span>
                        <div class="search-result-type-container">
                            <span class="search-result-type"><?= $result_type ?></span>
                        </div>
                        <div>
                            <p><?= strip_tags($excerpt) ?></p>
                        </div>
                    </div>
                </div>
            </a>
        </li>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function getResultsHtml($results)
    {
        if (count($results) < 1) {
            return '
            <div class="no-search-results">
                <p><span><i class="fas fa-frown emoji"></i></span></p>
                <p><span>Für deine Suche wurden leider keine Suchergebnisse gefunden...</span></p>
                <p><span>Versuche es bitte noch einmal mit einem anderen Suchbegriff.</span></p>
            </div>
            ';
        }

        $result_items = array();

        foreach ($results as $result) {
            $result_items[] = $this->getResultHtml($result);
        }

        return '<div><ul>' . implode("<hr>", $result_items) . '</ul></div>';
    }

    public function displayResults($results, $format = "html")
    {
        switch ($format) {
            case "json":
                return json_encode($results);
            case "html":
            default:
                return $this->getResultsHtml($results);
        }
    }
}