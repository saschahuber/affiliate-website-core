<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\service\NewsletterService;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\LogHelper;
use saschahuber\saastemplatecore\service\UserAuthService;
use stdClass;

const ALL_NEWSLETTER_TOPICS = array(
    'posts' => 'Blog-Artikel',
    'reviews' => 'Produkttests',
    'news' => 'News',
    'deals' => 'Deals & Blitzangebote'
);

define('ALL_NEWS_LETTER_TOPICS_KEYS', array_keys(ALL_NEWSLETTER_TOPICS));

define('DEFAULT_DAYS_BETWEEN_NEWSLETTER', 7);

#[AllowDynamicProperties]
class NewsletterManager extends Manager
{
    const BASE_TABLE_NAME = 'newsletter';

    public function __construct()
    {
        parent::__construct(self::BASE_TABLE_NAME);
        global $DB, $USER_AUTH_SERVICE;
        $this->DB = $DB;

        if (isset($USER_AUTH_SERVICE)) {
            $this->user_auth_service = $USER_AUTH_SERVICE;
        } else {
            $this->user_auth_service = new UserAuthService();
            $this->user_auth_service->setup();
        }
        $this->newsletter_service = new NewsletterService();
    }

    public function addSubscriber($mail, $sendConfirmationMail = true, $name = null,
                                  $topics = ALL_NEWS_LETTER_TOPICS_KEYS,
                                  $days_between_newsletter = DEFAULT_DAYS_BETWEEN_NEWSLETTER)
    {
        $user = $this->DB->getOne('SELECT * FROM user where user.email = "' . $this->DB->escape($mail) . '"');

        $subscriber = false;
        if ($user) {
            $user_id = $user->id;
            $subscriber = $this->getSubscriberByUserId($user_id);
        } else {
            $user = $this->user_auth_service->createUser($mail, $name);
            $user_id = $user->id;
        }

        if ($user_id === null) {
            LogHelper::error("HTTP", 400, "Kein User für Mail '$mail' vorhanden oder erstellt worden", ERROR_EXIT);
            return false;
        }

        $confirmation_mail_success = false;
        if (!$subscriber) {
            $subscriber = new stdClass();
            $subscriber->days_between_newsletter = $days_between_newsletter;
            $subscriber->user_id = $user_id;
            $subscriber->confirmation_token = md5($mail . time());

            $subscriber->id = $this->DB->insertFromObject('newsletter_subscriber', $subscriber);

            //Mail von Nutzer temporär setzen
            $subscriber->email = $mail;

            foreach ($topics as $topic) {
                $subscriber_topic_item = new stdClass();
                $subscriber_topic_item->newsletter_subscriber_id = $subscriber->id;
                $subscriber_topic_item->topic = $topic;
                $this->DB->insertFromObject('newsletter_subscriber__topics', $subscriber_topic_item);
            }
            if ($sendConfirmationMail) {
                $confirmation_mail_success = $this->newsletter_service->sendConfirmationMail($subscriber);
            }
        } else if (isset($subscriber->confirmation_token)
            && $subscriber->confirmation_token !== null
            && $subscriber->confirmation_date == null
            && $sendConfirmationMail) {
            $confirmation_mail_success = $this->newsletter_service->sendConfirmationMail($subscriber);
        }

        unset($subscriber->email);
        unset($subscriber->name);
        unset($subscriber->topics);
        $subscriber->confirmation_mail_sent = $confirmation_mail_success;
        $this->DB->updateFromObject('newsletter_subscriber', $subscriber);

        return true;
    }

    public function confirmSubscriberByUser($user, $sendWelcomeMail = true)
    {
        $dbquery = $this->DB->query('SELECT newsletter_subscriber.*, user.email FROM newsletter_subscriber 
            LEFT JOIN user on newsletter_subscriber.user_id = user.user_id
            where user.user_id = ' . intval($user->id));

        if ($dbquery->num_rows) {
            $subscriber = $dbquery->fetchObject();

            $subscriber = $this->setupConfirmedSubscriber($subscriber);

            if ($sendWelcomeMail) {
                $this->newsletter_service->sendWelcomeMail($subscriber);
            }

            return true;
        }

        return false;
    }

    private function setupConfirmedSubscriber($subscriber)
    {
        $unsubscribe_token = md5($subscriber->email . "-unsubscribe-" . time());
        $settings_token = md5($subscriber->email . "-settings-" . time());

        $this->DB->query('UPDATE newsletter_subscriber 
            SET confirmation_date = CURRENT_TIMESTAMP,
            confirmation_token = null,
            unsubscribe_token = "' . $this->DB->escape($unsubscribe_token) . '",
            settings_token = "' . $this->DB->escape($settings_token) . '"
            where newsletter_subscriber_id = ' . intval($subscriber->id));

        $subscriber->unsubscribe_token = $unsubscribe_token;
        $subscriber->settings_token = $settings_token;
        return $subscriber;
    }

    public function confirmSubscriber($confirmation_token, $sendWelcomeMail = true)
    {
        $dbquery = $this->DB->query('SELECT newsletter_subscriber.*, user.email FROM newsletter_subscriber 
            LEFT JOIN user on newsletter_subscriber.user_id = user.user_id
            where newsletter_subscriber.confirmation_token = "' . $this->DB->escape($confirmation_token) . '"');

        if ($dbquery->num_rows) {
            $subscriber = $dbquery->fetchObject();

            $subscriber = $this->setupConfirmedSubscriber($subscriber);

            if ($sendWelcomeMail) {
                $this->newsletter_service->sendWelcomeMail($subscriber);
            }

            return true;
        }

        return false;
    }

    public function unsubscribe($unsubscribe_token)
    {
        $subscriber = $this->DB->query('SELECT * FROM newsletter_subscriber
            LEFT JOIN user on user.user_id = newsletter_subscriber.user_id
            where unsubscribe_token = "' . $this->DB->escape($unsubscribe_token) . '"')->fetchObject();
        $this->newsletter_service->sendUnsubscribedMail($subscriber);

        $this->DB->query('DELETE FROM newsletter_subscriber__newsletter_data 
            where newsletter_subscriber_id in (SELECT newsletter_subscriber_id 
                FROM newsletter_subscriber 
                where unsubscribe_token = "' . $this->DB->escape($unsubscribe_token) . '"
            )');

        $this->DB->query('DELETE FROM newsletter_subscriber__topics 
            where newsletter_subscriber_id in (SELECT newsletter_subscriber_id 
                FROM newsletter_subscriber 
                where unsubscribe_token = "' . $this->DB->escape($unsubscribe_token) . '"
            )');

        $this->DB->query('DELETE FROM newsletter_subscriber
            where unsubscribe_token = "' . $this->DB->escape($unsubscribe_token) . '"');

    }

    public function updateSettings($settings_token, $selected_topics)
    {
        $this->DB->query('DELETE FROM newsletter_subscriber__topics 
            where newsletter_subscriber_id in (SELECT newsletter_subscriber_id 
                FROM newsletter_subscriber 
                where settings_token = "' . $this->DB->escape($settings_token) . '"
            )');

        $subscriber = $this->DB->query('SELECT newsletter_subscriber.*, user.email FROM newsletter_subscriber 
            LEFT JOIN user on newsletter_subscriber.user_id = user.user_id
            where settings_token = "' . $this->DB->escape($settings_token) . '"')->fetchObject();

        if ($subscriber) {
            foreach ($selected_topics as $topic) {
                $subscriber_topic_item = new stdClass();
                $subscriber_topic_item->newsletter_subscriber_id = $subscriber->id;
                $subscriber_topic_item->topic = $topic;
                $this->DB->insertFromObject('newsletter_subscriber__topics', $subscriber_topic_item);
            }

            $subscriber->unsubscribe_token = md5($subscriber->email . "-unsubscribe-" . time());

            $this->newsletter_service->sendSettingsUpdatedMail($subscriber);
            unset($subscriber->email);
            $this->DB->updateFromObject('newsletter_subscriber', $subscriber);
            return true;
        } else {
            return false;
        }
    }

    public function getSubscriberSettings($settings_token)
    {
        $dbquery = $this->DB->query('SELECT topic FROM newsletter_subscriber__topics
            JOIN newsletter_subscriber
                ON newsletter_subscriber.newsletter_subscriber_id = newsletter_subscriber__topics.newsletter_subscriber_id
            where settings_token = "' . $this->DB->escape($settings_token) . '"');

        if ($dbquery->num_rows) {
            $topics = [];
            while ($topic = $dbquery->fetchObject()) {
                $topics[] = $topic->topic;
            }
            return $topics;
        }

        return false;
    }

    public function checkUnsubscribeToken($settings_token)
    {
        $dbquery = $this->DB->query('SELECT * FROM newsletter_subscriber 
            where unsubscribe_token = "' . $this->DB->escape($settings_token) . '"');

        if ($dbquery->num_rows) {
            return true;
        }

        return false;
    }

    public function getSubscriberByUserId($user_id)
    {
        return $this->DB->getOne('SELECT newsletter_subscriber.newsletter_subscriber_id, user.email, user.name, 
        newsletter_subscriber.unsubscribe_token, newsletter_subscriber.settings_token, a.topics
        FROM newsletter_subscriber
        LEFT JOIN user on user.user_id = newsletter_subscriber.user_id
        JOIN (
            SELECT newsletter_subscriber_id, GROUP_CONCAT(topic SEPARATOR ",") as topics 
                FROM newsletter_subscriber__topics 
                GROUP BY newsletter_subscriber_id
        ) a
        on a.newsletter_subscriber_id = newsletter_subscriber.newsletter_subscriber_id
        WHERE user.user_id = ' . intval($user_id));
    }

    public function getSubscriberByMail($email)
    {
        return $this->DB->query('SELECT user.email, user.name, newsletter_subscriber.unsubscribe_token,
        newsletter_subscriber.settings_token, a.topics
        FROM newsletter_subscriber
        LEFT JOIN user on user.user_id = newsletter_subscriber.user_id
        JOIN (
            SELECT newsletter_subscriber_id, GROUP_CONCAT(topic SEPARATOR ",") as topics 
                FROM newsletter_subscriber__topics 
                GROUP BY newsletter_subscriber_id
        ) a
        on a.newsletter_subscriber_id = newsletter_subscriber.newsletter_subscriber_id
        WHERE user.email = "' . $this->DB->escape($email) . '"')->fetchObject();
    }

    public function getConfirmedSubscribers($confirmed = true)
    {
        $subscribers = [];
        $dbquery = $this->DB->query('SELECT newsletter_subscriber.newsletter_subscriber_id, user.email,
        user.name, newsletter_subscriber.confirmation_token, 
        newsletter_subscriber.unsubscribe_token, newsletter_subscriber.settings_token, 
        newsletter_subscriber.last_newsletter, a.topics
        FROM newsletter_subscriber
        LEFT JOIN user on user.user_id = newsletter_subscriber.user_id
        JOIN (
            SELECT newsletter_subscriber_id, GROUP_CONCAT(topic SEPARATOR ",") as topics 
                FROM newsletter_subscriber__topics 
                GROUP BY newsletter_subscriber_id
        ) a
        on a.newsletter_subscriber_id = newsletter_subscriber.newsletter_subscriber_id
        WHERE newsletter_subscriber.confirmation_date is ' . Helper::ifstr($confirmed, 'not') . ' null');

        while ($subscriber = $dbquery->fetchObject()) {
            $subscriber->topics = explode(',', $subscriber->topics);
            $subscribers[] = $subscriber;
        }
        return $subscribers;
    }

    public function getConfirmedSubscribersSince($since)
    {
        $subscribers = [];

        $querystring = 'SELECT newsletter_subscriber.newsletter_subscriber_id, user.email,
        user.name, newsletter_subscriber.confirmation_token, 
        newsletter_subscriber.unsubscribe_token, newsletter_subscriber.settings_token, 
        newsletter_subscriber.last_newsletter, a.topics
        FROM newsletter_subscriber
        LEFT JOIN user on user.user_id = newsletter_subscriber.user_id
        JOIN (
            SELECT newsletter_subscriber_id, GROUP_CONCAT(topic SEPARATOR ",") as topics 
                FROM newsletter_subscriber__topics 
                GROUP BY newsletter_subscriber_id
        ) a
        on a.newsletter_subscriber_id = newsletter_subscriber.newsletter_subscriber_id
        WHERE newsletter_subscriber.confirmation_date is not null and newsletter_subscriber.confirmation_date > "' . $this->DB->escape($since) . '"';

        $dbquery = $this->DB->query($querystring);

        while ($subscriber = $dbquery->fetchObject()) {
            $subscriber->topics = explode(',', $subscriber->topics);
            $subscribers[] = $subscriber;
        }
        return $subscribers;
    }

    public function getSubscribers($confirmed = null)
    {
        $subscribers = [];

        $querystring = 'SELECT newsletter_subscriber.newsletter_subscriber_id, user.email,
        user.name, newsletter_subscriber.confirmation_token, user.registration_date,
        newsletter_subscriber.unsubscribe_token, newsletter_subscriber.settings_token, 
        newsletter_subscriber.last_newsletter, a.topics, user.email_valid, user.email_valid_datetime
        FROM newsletter_subscriber
        LEFT JOIN user on user.user_id = newsletter_subscriber.user_id
        JOIN (
            SELECT newsletter_subscriber_id, GROUP_CONCAT(topic SEPARATOR ",") as topics 
                FROM newsletter_subscriber__topics
                GROUP BY newsletter_subscriber_id
                ORDER BY topic ASC
        ) a
        on a.newsletter_subscriber_id = newsletter_subscriber.newsletter_subscriber_id
        order by user.registration_date DESC';

        if ($confirmed !== null) {
            $querystring .= ' where newsletter_subscriber.confirmation_token ' . ($confirmed ? 'is' : 'is not') . ' null';
        }

        $dbquery = $this->DB->query($querystring);

        while ($subscriber = $dbquery->fetchObject()) {
            $subscriber->topics = explode(',', $subscriber->topics);
            $subscribers[] = $subscriber;
        }
        return $subscribers;
    }

    public function getConfirmedSubscribersDueForNewsletter()
    {
        $subscribers = [];
        $dbquery = $this->DB->query('SELECT newsletter_subscriber.newsletter_subscriber_id, user.email,
        user.name, newsletter_subscriber.unsubscribe_token, newsletter_subscriber.settings_token, 
        newsletter_subscriber.last_newsletter, a.topics
        FROM newsletter_subscriber
        LEFT JOIN user on user.user_id = newsletter_subscriber.user_id
        JOIN (
            SELECT newsletter_subscriber_id, GROUP_CONCAT(topic SEPARATOR ",") as topics 
                FROM newsletter_subscriber__topics 
                GROUP BY newsletter_subscriber_id
        ) a
        on a.newsletter_subscriber_id = newsletter_subscriber.newsletter_subscriber_id
        WHERE newsletter_subscriber.confirmation_date is not null AND (
            (
                UNIX_TIMESTAMP(last_newsletter) < 
                (UNIX_TIMESTAMP(CURRENT_TIMESTAMP)-(days_between_newsletter*24*60*60))
            ) 
            or last_newsletter is null
        )');

        while ($subscriber = $dbquery->fetchObject()) {
            $subscriber->topics = explode(',', $subscriber->topics);
            $subscribers[] = $subscriber;
        }
        return $subscribers;
    }

    public function getNewestUnsentCustomNewsletter()
    {
        return $this->DB->getOne('select * from newsletter
         where sent_to_all_receivers = 0
         order by scheduled_time ASC
         LIMIT 1');
    }

    public function scheduleMailToSubscribers($subject, $content)
    {
        $subscribers = $this->getConfirmedSubscribers();
        $this->newsletter_service->scheduleMailToSubscribers($subscribers, $subject, $content);
    }

    public function scheduleNewsletterMails()
    {
        $subscribers_due_for_newsletter = $this->getConfirmedSubscribersDueForNewsletter();

        echo "Scheduling newsletter for " . count($subscribers_due_for_newsletter) . " subscribers" . PHP_EOL;

        $this->newsletter_service->scheduleNewsletterMails($subscribers_due_for_newsletter);
    }

    public function scheduleCustomNewsletterMails()
    {
        $newsletter = $this->getNewestUnsentCustomNewsletter();

        if (!$newsletter) {
            echo "Kein Newsletter..." . PHP_EOL;
            return;
        }

        $subscribers = $this->getConfirmedSubscribers(true);

        $this->newsletter_service->scheduleCustomNewsletterMails($newsletter, $subscribers);
    }

    function getById($id)
    {
        $querystring = "SELECT * FROM " . $this->base_table_name
            . " WHERE " . $this->base_table_name . "_id = " . intval($id);

        return $this->DB->getOne($querystring);
    }

    public function getAll()
    {
        $query_builder = new DatabaseSelectQueryBuilder('newsletter');
        return $this->DB->getAll($query_builder->buildQuery());
    }
}