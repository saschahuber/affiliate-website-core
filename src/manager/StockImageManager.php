<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\UrlHelper;

#[AllowDynamicProperties]
class StockImageManager
{
    const STOCK_ATTACHMENT_DIR = "stock-attachment";

    public function __construct()
    {
        global $DB;
        $this->DB = $DB;
    }

    public function getAttachment($stock_attachment_id)
    {
        return $this->DB->query("SELECT * FROM stock_attachment where stock_attachment_id = " . intval($stock_attachment_id))->fetchObject();
    }

    public function getAttachmentByFilepath($filepath)
    {
        return $this->DB->query("SELECT * FROM stock_attachment where file_path LIKE '" . $this->DB->escape($filepath) . "'")->fetchObject();
    }

    public function updateAttachment($stock_attachment_id, $source_url, $description, $provider_name, $copyright_info, $title = null, $alt_text = null)
    {
        $this->DB->query("UPDATE stock_attachment set 
                            title = '" . $this->DB->escape($title) . "', 
                            alt_text = '" . $this->DB->escape($alt_text) . "',
                            source_url = '" . $this->DB->escape($source_url) . "',
                            description = '" . $this->DB->escape($description) . "',
                            provider_name = '" . $this->DB->escape($provider_name) . "',
                            copyright_info = '" . $this->DB->escape($copyright_info) . "'
            where stock_attachment_id = " . intval($stock_attachment_id));
    }

    public function addAttachmentFromUrl($url, $title = null, $alt_text = null, $old_url = null, $old_id = null)
    {
        $file_path = $this->uploadAttachmentFromUrl($url);
        $file_name = pathinfo($url)['basename'];

        $this->DB->query('INSERT INTO stock_attachment (title, alt_text, file_path, file_name, old_stock_attachment_url, old_stock_attachment_id) 
            VALUES (
                "' . $this->DB->escape($title) . '",
                "' . $this->DB->escape($alt_text) . '",
                "' . $this->DB->escape($file_path) . '",
                "' . $this->DB->escape($file_name) . '",
                "' . $this->DB->escape($old_url) . '", 
                "' . $this->DB->escape($old_id) . '"
            )');

        return $this->DB->insert_id;
    }

    public function addAttachmentFromFile($file, $source_url, $description, $provider_name, $copyright_info, $title = null, $alt_text = null, $uploaded_file_name = null)
    {
        $file_path = $this->uploadAttachmentFile($file, $uploaded_file_name);
        $file_name = pathinfo($file_path)['basename'];
        if ($file_path) {
            $this->DB->query('INSERT INTO stock_attachment (title, alt_text, file_path, file_name, source_url, description, provider_name, copyright_info) 
            VALUES (
                "' . $this->DB->escape($title) . '",
                "' . $this->DB->escape($alt_text) . '",
                "' . $this->DB->escape($file_path) . '",
                "' . $this->DB->escape($file_name) . '",
                "' . $this->DB->escape($source_url) . '",
                "' . $this->DB->escape($description) . '",
                "' . $this->DB->escape($provider_name) . '",
                "' . $this->DB->escape($copyright_info) . '"
            )');
            return $this->DB->insert_id;
        } else {
            return false;
        }
    }

    public function uploadAttachmentFile($file, $uploaded_file_name=null)
    {
        if (!empty($file['name'])) {
            $media_dir = DATA_DIR . '/media';

            $file_info = pathinfo($file['name']);

            $filename = UrlHelper::alias($file_info['filename']) . '.' . $file_info['extension'];

            if($uploaded_file_name){
                $filename = $uploaded_file_name;
            }

            $stock_attachment_path = '/' . self::STOCK_ATTACHMENT_DIR . '/' . $filename;

            if (!file_exists($media_dir . $stock_attachment_path)) {
                FileHelper::createDirIfNotExists($media_dir . '/' . self::STOCK_ATTACHMENT_DIR);
                move_uploaded_file($file['tmp_name'], $media_dir . $stock_attachment_path);
                return $stock_attachment_path;
            }
        }

        return false;
    }

    public function uploadAttachmentFromUrl($url, $filename = null)
    {
        if ($filename === null) {
            $path_parts = pathinfo($url);
            $filename = $path_parts['basename'];
        }

        $file_path = '/' . self::STOCK_ATTACHMENT_DIR . '/' . $filename;

        $ch = curl_init($url);
        $fp = fopen(MEDIA_DIR . $file_path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        return $file_path;
    }

    public function getAttachmentImageTag($stock_attachment_id, $alt_text = null, $title = null, $width = null, $height = null)
    {
        $stock_attachment = $this->getAttachment($stock_attachment_id);

        if (!$stock_attachment) {
            return null;
        }

        $img_url = $this->createAttachmentSrcFromFilePath($stock_attachment->file_path);

        if (!$img_url) {
            return null;
        }

        if ($title === null) {
            $title = $stock_attachment->title;
        }

        if ($alt_text === null) {
            $alt_text = $stock_attachment->alt_text;
        }

        return ImgUtils::getImageTag($img_url, true, $alt_text, $title, $width, $height);
    }

    public function getAttachmentUrl($stock_attachment_id, $default_image = null)
    {
        if ($stock_attachment_id === null) {
            return $default_image;
        }

        $item = $this->DB->getOne("SELECT file_path FROM stock_attachment where stock_attachment_id = " . intval($stock_attachment_id), true, 60);

        if ($item) {
            return $this->createAttachmentSrcFromFilePath($item->file_path);
        }

        return null;
    }

    public function getAttachments($keyword = null, $limit=null, $offset=0)
    {
        $attachments = array();

        $query_builder = new DatabaseSelectQueryBuilder("stock_attachment");

        if ($keyword) {
            $query_builder->conditions([
                'title LIKE "%' . $this->DB->escape($keyword) . '%"',
                'alt_text LIKE "%' . $this->DB->escape($keyword) . '%"',
                'file_name LIKE "%' . $this->DB->escape($keyword) . '%"',
                'description LIKE "%' . $this->DB->escape($keyword) . '%"'
            ], 'or');
        }

        if($limit){
            $limit_string = intval($offset) . ", " . intval($limit);
            $query_builder->limit($limit_string);
        }

        $query_builder->orders([
            "stock_attachment_time DESC",
            "stock_attachment_id DESC"
        ]);

        $querystring = $query_builder->buildQuery();

        $dbquery = $this->DB->query($querystring);
        while ($item = $dbquery->fetchObject()) {
            $item->src = $this->createAttachmentSrcFromFilePath($item->file_path);
            $attachments[] = $item;
        }

        return $attachments;
    }

    public function createAttachmentSrcFromFilePath($file_path)
    {
        return '/data/media' . $file_path;
    }
}