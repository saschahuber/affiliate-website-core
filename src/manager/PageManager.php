<?php

namespace saschahuber\affiliatewebsitecore\manager;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\PageThumbnailGeneratorService;
use saschahuber\saastemplatecore\helper\LogHelper;

#[AllowDynamicProperties]
class PageManager extends PostableManager
{
    const TABLE_NAME = "page";

    const TYPE_PAGE = "page";
    const URL_PREFIX = "";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME, self::URL_PREFIX);
    }

    public function addMeta($item, $meta = null, $taxonomies = null)
    {
        //Do nothing...
    }

    public function getBulkMeta($item_ids)
    {
        return [];
    }

    public function getFiltered($keyword = null, $status = null, $taxonomies = null, $order_by = null, $desc = true, $limit = null, $offset = 0)
    {
        $items = array();

        $filter_clauses = [];

        if ($keyword) {
            $filter_clauses[] = 'title LIKE "%' . $this->DB->escape($keyword) . '%"';
        }

        if ($status) {
            $filter_clauses[] = 'status = "' . $this->DB->escape($status) . '"';
        }

        $order_clause = false;
        if ($order_by !== null) {
            $order_clause = ' ORDER BY ' . $this->DB->escape($order_by) . " " . ($desc ? "desc" : "asc");
        }

        $querystring = 'SELECT ' . $this->base_table_name . '.*, 
            DATE_FORMAT(' . $this->base_table_name . '_date, "%d. %M %Y") as formatted_date 
            FROM ' . $this->base_table_name
            . ((count($filter_clauses) > 0) ? " WHERE " . implode(' AND ', $filter_clauses) : "")
            . ' GROUP BY ' . $this->base_table_name . '.' . $this->base_table_name . '_id'
            . ($order_clause ?: "");

        if ($limit) {
            $querystring .= " LIMIT " . intval($limit);
        }

        if ($offset) {
            $querystring .= " OFFSET " . intval($offset);
        }

        $items = $this->DB->getAll($querystring, true, 60);

        return $this->getAllWithMeta($items);
    }

    public function generatePermalink($item)
    {
        #Pfad-Cachen für Item-ID

        if (!$item) {
            LogHelper::error("HTTP", 404, "Das Element wurde nicht gefunden.", ERROR_EXIT);
        }

        return '/' . trim($item->permalink, '/');
    }

    public function getThumbnailSrc($item)
    {
        if ($item->attachment_id !== null) {
            $thumbnail_src = $this->image_manager->getAttachmentUrl($item->attachment_id);
        } else {
            $thumbnail_src = (new PageThumbnailGeneratorService())->getThumbnailSrc($item);
        }
        return $thumbnail_src;
    }
}