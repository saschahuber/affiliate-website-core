<?php

namespace saschahuber\affiliatewebsitecore\manager;

use saschahuber\saastemplatecore\builder\DatabaseSelectQueryBuilder;
use stdClass;

class ProductDataFieldGroupManager extends Manager
{
    const TABLE_NAME = "product__data_field_group";

    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    public function cleanItem($item)
    {
        return $item;
    }

    public function createDataFieldGroup($group_name, $mapped_taxonomy)
    {
        $data_field_group = new stdClass();
        $data_field_group->group_name = $group_name;
        $data_field_group->product__taxonomy_id = $mapped_taxonomy;
        return $this->DB->insertFromObject(self::TABLE_NAME, $data_field_group);
    }

    public function getDataFieldGroups($taxonomy_ids = null)
    {
        $query_builder = new DatabaseSelectQueryBuilder(self::TABLE_NAME);
        $query_builder->selects([
            'product__data_field_group.*',
            'product__taxonomy.title as taxonomy_name'
        ])
            ->join('product__taxonomy', 'product__data_field_group.product__taxonomy_id', 'product__taxonomy.product__taxonomy_id', 'LEFT');

        if ($taxonomy_ids) {
            $query_builder->conditions([
                'product__data_field_group.product__taxonomy_id IN (' . implode(',', $taxonomy_ids) . ')'
            ]);
        }

        $query_builder->order('group_name ASC');

        $querystring = $query_builder->buildQuery();

        return $this->DB->getAll($querystring, true, 15);
    }
}