<?php

namespace saschahuber\affiliatewebsitecore\scaffolding;

use saschahuber\affiliatewebsitecore\manager\LocationManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;

class PaginatedLocationTable extends PaginatedItemTable {

    private LocationManager $location_manager;

    public function __construct($items, $offset=0, $items_per_page=25, $page_number=1, $id=null){
        parent::__construct($items, $offset, $items_per_page, $page_number, $id);

        $this->location_manager = new LocationManager();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Titel',
            'Hervorgehoben',
            'Status',
            'Datum',
            'Index/Follow',
            'Aktionen',
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $location_actions = [];

        if($item->status == Manager::STATUS_PUBLISH) {
            $location_actions[] = new LinkButton($CONFIG->website_domain . $this->location_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true);
        }
        #$location_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => LocationManager::TYPE_LOCATION, 'id' => $item->id]);
        $location_actions[] = new LinkButton("/dashboard/ort/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        $cells = [
            $item->title,
            new EditableToggle('location', 'featured', $item->id, $item->featured, true),
            new EditableSelect('location', 'status', $item->id, $item->status, Manager::getStatusTypes()),
            new EditableDateTimePicker('location', 'location_date', $item->id, $item->location_date),
            BufferHelper::buffered(function() use ($item){
                (new EditableToggle('location', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('location', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
            }),
            new FloatContainer($location_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/ort/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}