<?php

namespace saschahuber\affiliatewebsitecore\scaffolding;

use saschahuber\affiliatewebsitecore\component\CompanyList;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\helper\BufferHelper;

class NearCompaniesContainer extends Component {
    private $location, $near_companies;

    public function __construct($location, $near_companies, $id=null){
        $this->setLocation($location);
        $this->setNearCompanies($near_companies);
        parent::__construct($id);
    }

    protected function build(){

        if(!$this->getLocation()->latitude || !$this->getLocation()->longitude){
            return;
        }

        if(count($this->getNearCompanies())){
            # Dienstleister in Location darstellen
            (new CompanyList($this->getNearCompanies()))->display();

            echo BufferHelper::buffered(function (){
                global $CONFIG;
                ?>
                <br>
                <div class="text-center">
                    <h3 class="text-center">Ihr Unternehmen fehlt hier noch?</h3>
                    <p class="text-center">Gerne listen wir auch Ihr Unternehmen hier auf.</p>
                    <p class="text-center">Melden Sie sich einfach unter <a href="mailto:<?=$CONFIG->collective_mailbox?>"><?=$CONFIG->collective_mailbox?></a>.</p>
                </div>
                <?php
            });
        }
        else{
            echo BufferHelper::buffered(function (){
                global $CONFIG;
                ?>
                <div>
                    <h3>Es sind noch keine Unternehmen in <?=$this->getLocation()->title?> gelistet</h3>
                    <p>Gerne listen wir Ihr Unternehmen hier auf. Melden Sie sich einfach unter <a href="mailto:<?=$CONFIG->collective_mailbox?>"><?=$CONFIG->collective_mailbox?></a>.</p>
                </div>
                <?php
            });
        }
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location): void
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getNearCompanies()
    {
        return $this->near_companies;
    }

    /**
     * @param mixed $near_companies
     */
    public function setNearCompanies($near_companies): void
    {
        $this->near_companies = $near_companies;
    }


}