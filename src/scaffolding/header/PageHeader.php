<?php

namespace saschahuber\affiliatewebsitecore\scaffolding\header;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\component\scaffolding\header\BackgroundImageHeader;

class PageHeader extends BackgroundImageHeader
{
    private $page, $page_manager;

    public function __construct($page, $page_manager,  $id = null)
    {
        $this->setPage($page);
        $this->setPageManager($page_manager);

        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();
        $ai_image_manager = new AiImageManager();

        $background_image_url = null;
        if($page->dynamic_image_id){
            switch($page->dynamic_image_type){
                case "attachment":
                    $background_image_url = $image_manager->getAttachmentUrl($page->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $background_image_url = $stock_image_manager->getAttachmentUrl($page->dynamic_image_id);
                    break;
                case "ai_attachment":
                    $background_image_url = $ai_image_manager->getAttachmentUrl($page->dynamic_image_id);
                    break;
            }
        }

        parent::__construct($this->buildContent(), $background_image_url, $id);
    }

    private function buildContent()
    {
        global $CONFIG;

        $title_to_display = $this->page->h1??null;
        if($title_to_display === null || strlen($title_to_display) < 1){
            $title_to_display = $this->page->title;
        }

        ob_start();
        ?>
        <h1 style="text-align: center; color: #fff;"><?= $title_to_display ?></h1>
        <?php

        return ob_get_clean();
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     */
    public function setPage($page): void
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getPageManager()
    {
        return $this->page_manager;
    }

    /**
     * @param mixed $page_manager
     */
    public function setPageManager($page_manager): void
    {
        $this->page_manager = $page_manager;
    }
}