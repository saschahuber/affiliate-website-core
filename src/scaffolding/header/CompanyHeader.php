<?php

namespace saschahuber\affiliatewebsitecore\scaffolding\header;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\component\scaffolding\header\BackgroundImageHeader;

class CompanyHeader extends BackgroundImageHeader
{
    private $company, $company_manager;

    public function __construct($company, $company_manager,  $id = null)
    {
        $this->setCompany($company);
        $this->setCompanyManager($company_manager);

        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();
        $ai_image_manager = new AiImageManager();

        $background_image_url = null;
        if(CompanyManager::hasCoverImage($company) && $company->dynamic_image_id){
            switch($company->dynamic_image_type){
                case "attachment":
                    $background_image_url = $image_manager->getAttachmentUrl($company->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $background_image_url = $stock_image_manager->getAttachmentUrl($company->dynamic_image_id);
                    break;
                case "ai_attachment":
                    $background_image_url = $ai_image_manager->getAttachmentUrl($company->dynamic_image_id);
                    break;
            }
        }

        parent::__construct($this->buildContent(), $background_image_url, $id);
    }

    private function buildContent()
    {
        global $CONFIG;

        $title_to_display = $this->company->h1??null;
        if($title_to_display === null || strlen($title_to_display) < 1){
            $title_to_display = $this->company->title;
        }

        ob_start();
        ?>
        <h1 style="text-align: center; color: #fff;"><?= $title_to_display ?></h1>

        <br>

        <!--
        <div class="centered" style="color: #fff;">
            <?php if($CONFIG->allow_user_login || $CONFIG->allow_customer_login): ?>
                &nbsp;|&nbsp;
                <?=$this->company_manager->getFavouriteButton($this->company->id)?>
            <?php endif; ?>
        </div>
        -->

        <?php

        return ob_get_clean();
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getCompanyManager()
    {
        return $this->company_manager;
    }

    /**
     * @param mixed $company_manager
     */
    public function setCompanyManager($company_manager): void
    {
        $this->company_manager = $company_manager;
    }
}