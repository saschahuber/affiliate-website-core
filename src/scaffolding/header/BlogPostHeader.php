<?php

namespace saschahuber\affiliatewebsitecore\scaffolding\header;

use saschahuber\affiliatewebsitecore\component\SponsoredBadge;
use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\component\scaffolding\header\BackgroundImageHeader;

class BlogPostHeader extends BackgroundImageHeader
{
    private $post, $post_manager;

    public function __construct($post, $post_manager,  $id = null)
    {
        $this->setPost($post);
        $this->setPostManager($post_manager);

        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();
        $ai_image_manager = new AiImageManager();

        $background_image_url = null;
        if($post->dynamic_image_id){
            switch($post->dynamic_image_type){
                case "attachment":
                    $background_image_url = $image_manager->getAttachmentUrl($post->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $background_image_url = $stock_image_manager->getAttachmentUrl($post->dynamic_image_id);
                    break;
                case "ai_attachment":
                    $background_image_url = $ai_image_manager->getAttachmentUrl($post->dynamic_image_id);
                    break;
            }
        }

        parent::__construct($this->buildContent(), $background_image_url, $id);
    }

    private function buildContent()
    {
        global $CONFIG;

        $title_to_display = $this->post->h1??null;
        if($title_to_display === null || strlen($title_to_display) < 1){
            $title_to_display = $this->post->title;
        }

        ob_start();
        ?>
        <h1 style="text-align: center; color: #fff;"><?= $title_to_display ?></h1>

        <br>

        <div class="centered" style="color: #fff;">
            <span><i class="far fa-calendar"></i> <?=$this->post_manager->getDate($this->post)?></span>
            &nbsp;|&nbsp;
            <span><i class="far fa-folder"></i> <?=$this->post_manager->getTaxonomyLinks($this->post)?></span>

            <?php if($this->post->is_sponsored): ?>
                &nbsp;|&nbsp;
                <?php (new SponsoredBadge())->display(); ?>
            <?php endif; ?>

            <?php if($CONFIG->allow_user_login || $CONFIG->allow_customer_login): ?>
                &nbsp;|&nbsp;
                <?=$this->post_manager->getFavouriteButton($this->post->id)?>
            <?php endif; ?>
        </div>

        <?php

        return ob_get_clean();
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post): void
    {
        $this->post = $post;
    }

    /**
     * @return mixed
     */
    public function getPostManager()
    {
        return $this->post_manager;
    }

    /**
     * @param mixed $post_manager
     */
    public function setPostManager($post_manager): void
    {
        $this->post_manager = $post_manager;
    }
}