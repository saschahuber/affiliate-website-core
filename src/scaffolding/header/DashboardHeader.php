<?php

namespace saschahuber\affiliatewebsitecore\scaffolding\header;

use saschahuber\affiliatewebsitecore\service\AffiliateDashboardMenuService;
use saschahuber\saastemplatecore\component\scaffolding\header\BackgroundImageHeader;

class DashboardHeader extends BackgroundImageHeader
{
    private $title;

    public function __construct($title, $id = null)
    {
        $this->title = $title;
        parent::__construct($this->buildContent(), null, $id);
    }

    private function buildContent()
    {
        global $ROUTER;

        ob_start();
        ?>

        <style>
            :root {
                --content_header_height: 500px !important;
            }
        </style>

        <!--
        <h1 style="text-align: center; color: #fff;"><?= $this->title ?></h1>

        <br>
        -->

        <div id="dashboard-header-menu" style="margin: auto;">
            <nav class="navbar navbar-expand-lg navbar-dark justify-content-center">
                <ul class="navbar-nav">
                    <?php echo AffiliateDashboardMenuService::displayDashboardMenu($ROUTER->getPath()); ?>
                </ul>
            </nav>
        </div>

        <?php

        return ob_get_clean();
    }
}