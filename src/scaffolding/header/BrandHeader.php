<?php

namespace saschahuber\affiliatewebsitecore\scaffolding\header;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\component\scaffolding\header\BackgroundImageHeader;

class BrandHeader extends BackgroundImageHeader
{
    private $brand, $brand_manager;

    public function __construct($brand, $brand_manager,  $id = null)
    {
        $this->setBrand($brand);
        $this->setBrandManager($brand_manager);

        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();
        $ai_image_manager = new AiImageManager();

        $background_image_url = null;
        if($brand->dynamic_image_id){
            switch($brand->dynamic_image_type){
                case "attachment":
                    $background_image_url = $image_manager->getAttachmentUrl($brand->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $background_image_url = $stock_image_manager->getAttachmentUrl($brand->dynamic_image_id);
                    break;
                case "ai_attachment":
                    $background_image_url = $ai_image_manager->getAttachmentUrl($brand->dynamic_image_id);
                    break;
            }
        }

        parent::__construct($this->buildContent(), $background_image_url, $id);
    }

    private function buildContent()
    {
        global $CONFIG;

        $title_to_display = $this->brand->h1??null;
        if($title_to_display === null || strlen($title_to_display) < 1){
            $title_to_display = $this->brand->title;
        }

        ob_start();
        ?>
        <h1 style="text-align: center; color: #fff;"><?= $title_to_display ?></h1>
        <?php

        return ob_get_clean();
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getBrandManager()
    {
        return $this->brand_manager;
    }

    /**
     * @param mixed $brand_manager
     */
    public function setBrandManager($brand_manager): void
    {
        $this->brand_manager = $brand_manager;
    }
}