<?php

namespace saschahuber\affiliatewebsitecore\scaffolding\header;

use saschahuber\affiliatewebsitecore\component\CompanySearchForm;
use saschahuber\saastemplatecore\component\scaffolding\header\BasicHeader;

class CompanyOverviewHeader extends BasicHeader
{
    public function __construct($id = null)
    {
        parent::__construct($this->buildHeaderContent(), [], [], $id);
    }

    protected function buildHeaderContent()
    {
        ob_start();
        ?>
        <h1 style="text-align: center; color: #fff;">Dienstleister</h1>

        <br>
        <br>
        <br>

        <?php

        (new CompanySearchForm())->display();

        return ob_get_clean();
    }
}