<?php

namespace saschahuber\affiliatewebsitecore\scaffolding\header;

use saschahuber\affiliatewebsitecore\component\CompanySearchForm;
use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\component\scaffolding\header\BackgroundImageHeader;

class CompanyTaxonomyHeader extends BackgroundImageHeader
{
    private $taxonomy;

    public function __construct($taxonomy, $id = null)
    {
        $this->taxonomy = $taxonomy;

        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();
        $ai_image_manager = new AiImageManager();

        $background_image_url = null;
        if($taxonomy->dynamic_image_id){
            switch($taxonomy->dynamic_image_type){
                case "attachment":
                    $background_image_url = $image_manager->getAttachmentUrl($taxonomy->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $background_image_url = $stock_image_manager->getAttachmentUrl($taxonomy->dynamic_image_id);
                    break;
                case "ai_attachment":
                    $background_image_url = $ai_image_manager->getAttachmentUrl($taxonomy->dynamic_image_id);
                    break;
            }
        }

        parent::__construct($this->buildContent(), $background_image_url, $id);
    }

    private function buildContent()
    {
        ob_start();
        ?>
        <h1 style="text-align: center; color: #fff;"><?=$this->taxonomy->title?></h1>

        <br>
        <br>
        <p style="text-align: center; color: #fff; font-size: 22px;">In Kategorie "<?=$this->taxonomy->title?>" suchen:</p>

        <?php

        (new CompanySearchForm($this->taxonomy->id))->display();

        return ob_get_clean();
    }
}