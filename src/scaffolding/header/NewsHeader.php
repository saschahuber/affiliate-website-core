<?php

namespace saschahuber\affiliatewebsitecore\scaffolding\header;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\component\SponsoredBadge;
use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\component\scaffolding\header\BackgroundImageHeader;

#[AllowDynamicProperties]
class NewsHeader extends BackgroundImageHeader
{
    private $news, $news_manager;

    public function __construct($news, $news_manager, $id = null)
    {
        $this->setNews($news);
        $this->setNewsManager($news_manager);

        $image_manager = new ImageManager();
        $stock_image_manager = new StockImageManager();
        $ai_image_manager = new AiImageManager();

        $background_image_url = null;
        if($news->dynamic_image_id){
            switch($news->dynamic_image_type){
                case "attachment":
                    $background_image_url = $image_manager->getAttachmentUrl($news->dynamic_image_id);
                    break;
                case "stock_attachment":
                    $background_image_url = $stock_image_manager->getAttachmentUrl($news->dynamic_image_id);
                    break;
                case "ai_attachment":
                    $background_image_url = $ai_image_manager->getAttachmentUrl($news->dynamic_image_id);
                    break;
            }
        }

        parent::__construct($this->buildContent(), $background_image_url, $id);
    }

    private function buildContent()
    {
        global $CONFIG;

        $title_to_display = $this->news->h1;
        if ($title_to_display === null || strlen($title_to_display) < 1) {
            $title_to_display = $this->news->title;
        }

        ob_start();
        ?>
        <h1 style="text-align: center; color: #fff;"><?= $title_to_display ?></h1>

        <br>

        <div class="centered" style="color: #fff;">
            <span><i class="far fa-calendar"></i> <?= $this->news_manager->getDate($this->news) ?></span>

            <?php if(count($this->news->taxonomies) > 1): ?>
                &nbsp;|&nbsp;
                <span><i class="far fa-folder"></i> <?= $this->news_manager->getTaxonomyLinks($this->news) ?></span>
            <?php endif; ?>

            <?php if ($this->news->is_sponsored): ?>
                &nbsp;|&nbsp;
                <?php (new SponsoredBadge())->display(); ?>
            <?php endif; ?>

            <?php if ($CONFIG->allow_user_login || $CONFIG->allow_customer_login): ?>
                &nbsp;|&nbsp;
                <?= $this->news_manager->getFavouriteButton($this->news->id) ?>
            <?php endif; ?>
        </div>

        <?php

        return ob_get_clean();
    }

    /**
     * @return mixed
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @param mixed $news
     */
    public function setNews($news): void
    {
        $this->news = $news;
    }

    /**
     * @return mixed
     */
    public function getNewsManager()
    {
        return $this->news_manager;
    }

    /**
     * @param mixed $news_manager
     */
    public function setNewsManager($news_manager): void
    {
        $this->news_manager = $news_manager;
    }
}