<?php

namespace saschahuber\affiliatewebsitecore\scaffolding;

use saschahuber\saastemplatecore\component\Component;

class NearLocationsContainer extends Component {
    private $location, $near_locations, $location_manager;

    public function __construct($location_manager, $location, $near_locations, $id=null){
        $this->setLocationManager($location_manager);
        $this->setLocation($location);
        $this->setNearLocations($near_locations);
        parent::__construct($id);
    }

    protected function build(){
        if(!$this->getLocation()->latitude || !$this->getLocation()->longitude){
            return;
        }

        if(count($this->near_locations)){
            ?>
            <h2>Orte in der Nähe</h2>
            <ul>
                <?php foreach($this->near_locations as $location): ?>
                    <li><a href="<?=$this->getLocationManager()->generatePermalink($location)?>"><?=$location->title?> (<?=round($location->distance, 1)?> km)</a></li>
                <?php endforeach; ?>
            </ul>
            <?php
        }
    }

    /**
     * @return mixed
     */
    public function getNearLocations()
    {
        return $this->near_locations;
    }

    /**
     * @param mixed $near_locations
     */
    public function setNearLocations($near_locations): void
    {
        $this->near_locations = $near_locations;
    }

    /**
     * @return mixed
     */
    public function getLocationManager()
    {
        return $this->location_manager;
    }

    /**
     * @param mixed $location_manager
     */
    public function setLocationManager($location_manager): void
    {
        $this->location_manager = $location_manager;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location): void
    {
        $this->location = $location;
    }
}