<?php

namespace saschahuber\affiliatewebsitecore\block;

use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class ElementLaunchOverviewBlock extends Component {
    private $link_page_items, $push_notifications, $social_postings, $toasts, $linked_element_type, $linked_element_id;

    public function __construct($link_page_items, $push_notifications, $social_postings, $toasts, $linked_element_type, $linked_element_id, $id = null){
        parent::__construct($id);
        $this->link_page_items = $link_page_items;
        $this->push_notifications = $push_notifications;
        $this->social_postings = $social_postings;
        $this->toasts = $toasts;
        $this->linked_element_type = $linked_element_type;
        $this->linked_element_id = $linked_element_id;
    }

    protected function build(){
        $link_page_items = $this->link_page_items;
        $push_notifications = $this->push_notifications;
        $social_postings = $this->social_postings;
        $toasts = $this->toasts;

        $link_page_item_container = BufferHelper::buffered(function() use ($link_page_items){
            ?>
            <p class="centered"><strong>Link-Seite</strong></p>
            <br>
            <?php

            if($link_page_items){
                foreach($link_page_items as $link_page_item){
                    $url = "/dashboard/seiten/links/bearbeiten/{$link_page_item->id}";
                    $date = DateHelper::displayDateTime($link_page_item->link_page_item_date);
                    (new LinkButton($url, "$link_page_item->title ($date)", 'fas fa-edit', false, true, ['btn-block']))->display();
                }

                BreakComponent::break();
            }

            $url = "/dashboard/seiten/links/bearbeiten?linked_element_type={$this->linked_element_type}&linked_element_id={$this->linked_element_id}";
            (new LinkButton($url, 'Link erstellen', 'fas fa-plus-circle', false, true, ['btn-block']))->display();
        });

        $push_notification_item_container = BufferHelper::buffered(function() use ($push_notifications){
            ?>
            <p class="centered"><strong>Push-Benachrichtigungen</strong></p>
            <br>
            <?php

            if($push_notifications){
                foreach($push_notifications as $push_notification){
                    $url = "/dashboard/push-notifications/bearbeiten/{$push_notification->id}";
                    $date = DateHelper::displayDateTime($push_notification->scheduled_time);
                    (new LinkButton($url, "$push_notification->title ($date)", 'fas fa-edit', false, true, ['btn-block']))->display();
                }

                BreakComponent::break();
            }

            $url = "/dashboard/push-notifications/bearbeiten?linked_element_type={$this->linked_element_type}&linked_element_id={$this->linked_element_id}";
            (new LinkButton($url, 'Notification erstellen', 'fas fa-plus-circle', false, true, ['btn-block']))->display();
        });

        $social_posting_item_container = BufferHelper::buffered(function() use ($social_postings){
            ?>
            <p class="centered"><strong>Social-Posts</strong></p>
            <br>
            <?php

            if($social_postings){
                foreach($social_postings as $social_posting){
                    $url = "/dashboard/postings/bearbeiten/{$social_posting->id}";
                    $date = DateHelper::displayDateTime($social_posting->posting_time);
                    (new LinkButton($url, "$social_posting->target ($date)", 'fas fa-edit', false, true, ['btn-block']))->display();
                }

                BreakComponent::break();
            }

            $url = "/dashboard/postings/bearbeiten?linked_element_type={$this->linked_element_type}&linked_element_id={$this->linked_element_id}";
            (new LinkButton($url, 'Post erstellen', 'fas fa-plus-circle', false, true, ['btn-block']))->display();
        });

        $toast_item_container = BufferHelper::buffered(function() use ($toasts){
            ?>
            <p class="centered"><strong>Toasts</strong></p>
            <br>
            <?php

            if($toasts){
                foreach($toasts as $toast){
                    $url = "/dashboard/toast/bearbeiten/{$toast->id}";
                    $start_date = DateHelper::displayDateTime($toast->active_start);
                    $end_date = DateHelper::displayDateTime($toast->active_start);
                    (new LinkButton($url, "$start_date - $end_date", 'fas fa-edit', false, true, ['btn-block']))->display();
                }

                BreakComponent::break();
            }

            $url = "/dashboard/toasts/bearbeiten?linked_element_type={$this->linked_element_type}&linked_element_id={$this->linked_element_id}";
            (new LinkButton($url, 'Toast erstellen', 'fas fa-plus-circle', false, true, ['btn-block']))->display();
        });

        $columns = [
            new Column(new Card($link_page_item_container), ['col-md-3', 'col-12']),
            new Column(new Card($push_notification_item_container), ['col-md-3', 'col-12']),
            new Column(new Card($social_posting_item_container), ['col-md-3', 'col-12']),
            new Column(new Card($toast_item_container), ['col-md-3', 'col-12'])
        ];

        (new Card([
            new Text('Launch für '.getContentTypes()[$this->linked_element_type].' planen', 'h4'),
            new Row($columns)
        ]))->display();
    }

    /**
     * @return mixed
     */
    public function getLinkPageItems()
    {
        return $this->link_page_items;
    }

    /**
     * @param mixed $link_page_items
     */
    public function setLinkPageItems($link_page_items): void
    {
        $this->link_page_items = $link_page_items;
    }

    /**
     * @return mixed
     */
    public function getPushNotifications()
    {
        return $this->push_notifications;
    }

    /**
     * @param mixed $push_notifications
     */
    public function setPushNotifications($push_notifications): void
    {
        $this->push_notifications = $push_notifications;
    }

    /**
     * @return mixed
     */
    public function getSocialPostings()
    {
        return $this->social_postings;
    }

    /**
     * @param mixed $social_postings
     */
    public function setSocialPostings($social_postings): void
    {
        $this->social_postings = $social_postings;
    }

    /**
     * @return mixed
     */
    public function getLinkedElementType()
    {
        return $this->linked_element_type;
    }

    /**
     * @param mixed $linked_element_type
     */
    public function setLinkedElementType($linked_element_type): void
    {
        $this->linked_element_type = $linked_element_type;
    }

    /**
     * @return mixed
     */
    public function getLinkedElementId()
    {
        return $this->linked_element_id;
    }

    /**
     * @param mixed $linked_element_id
     */
    public function setLinkedElementId($linked_element_id): void
    {
        $this->linked_element_id = $linked_element_id;
    }
}