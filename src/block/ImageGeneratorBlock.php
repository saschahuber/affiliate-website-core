<?php

namespace saschahuber\affiliatewebsitecore\block;

use saschahuber\affiliatewebsitecore\component\form\SocialImageSettings;
use saschahuber\affiliatewebsitecore\service\SocialImageGeneratorService;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;
use SocialImageSettings;

class ImageGeneratorBlock extends Component
{

    public function __construct($id = null)
    {
        parent::__construct($id);
    }

    protected function build()
    {

        (new SocialImageSettings((new SocialImageGeneratorService())->getImageTemplateOptions('image'), null, SocialPostingFeedItemManager::TARGET_INSTAGRAM))->display();

        $platforms_supporting_images = ['facebook', 'twitter', 'instagram'];

        ?>
        <br>
        <div id="platforms_to_post_container">
            <span>Auf welchen Plattformen soll das Bild gepostet werden?</span>
            <br>
            <?php foreach ($platforms_supporting_images as $platform_supporting_images) {
                (new Toggle("platform_" . $platform_supporting_images, $platform_supporting_images, false, false, true))->display();
            }
            ?>
        </div>
        <?php
    }
}