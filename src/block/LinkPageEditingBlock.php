<?php

namespace saschahuber\affiliatewebsitecore\block;

use saschahuber\affiliatewebsitecore\component\form\input\AttachmentSelector;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\SubmitButton;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;

class LinkPageEditingBlock extends Component
{

    private $link_page_item_id, $item_title, $item_date, $item_content, $item_button_text, $item_button_url, $linked_item_type, $linked_item_id, $attachment_id;

    /**
     * @param $link_page_item_id
     * @param $item_title
     * @param $item_date
     * @param $item_content
     * @param $item_button_text
     * @param $item_button_url
     * @param $linked_item_type
     * @param $linked_item_id
     * @param $attachment_id
     */
    public function __construct($link_page_item_id, $item_title, $item_date, $item_content, $item_button_text, $item_button_url, $linked_item_type, $linked_item_id, $attachment_id, $id = null)
    {
        $this->link_page_item_id = $link_page_item_id;
        $this->item_title = $item_title;
        $this->item_date = $item_date;
        $this->item_content = $item_content;
        $this->item_button_text = $item_button_text;
        $this->item_button_url = $item_button_url;
        $this->linked_item_type = $linked_item_type;
        $this->linked_item_id = $linked_item_id;
        $this->attachment_id = $attachment_id;
        parent::__construct($id);
    }

    protected function build()
    {
        (new HiddenInput('id', $this->getLinkPageItemId()))->display();

        (TextInput::required("title", 'Titel', 'Titel der Seite', $this->getItemTitle()))->display();

        (new TextArea("content", "Inhalt", null, $this->getItemContent(), 300))->display();

        (new DateTimePicker("link_page_item_date", 'Veröffentlichungszeit', null, $this->getItemDate()))->display();

        $allowed_types = [
            null => '-- Nichts gewählt --',
            PostManager::TYPE_POST => 'Blog-Artikel',
            NewsManager::TYPE_NEWS => 'News-Artikel',
            ProductManager::TYPE_PRODUCT => 'Produkt'
        ];

        (new Card([
            new Text("Button konfigurieren", 'strong'),
            new Text("Wenn Button-URL Leer ist, wird URL des verknüpften Elements verwendet", 'p'),
            new Row([
                new Column([new TextInput('button_text', 'Text des Buttons', null, $this->getItemButtonText())], ['col-6']),
                new Column([new TextInput('button_url', 'URL des Buttons', null, $this->getItemButtonUrl())], ['col-6'])
            ])
        ]))->display();

        (new Card([
            new Text("Verlinkes Element (Optional)", 'strong'),
            new Row([
                new Column([new Select('linked_item_type', 'Typ von verlinktem Element', $allowed_types, $this->getLinkedItemType())], ['col-6']),
                new Column([new NumberInput('linked_item_id', 'ID von verlinktem Element', null, $this->getLinkedItemId())], ['col-6'])
            ])
        ]))->display();

        (new AttachmentSelector($this->getAttachmentId(), 'attachment_id', 'Verknüpftes Bild (Wenn leer, wird Thumbnail von verknüpftem Element verwendet)'))->display();
    }

    /**
     * @return mixed
     */
    public function getLinkPageItemId()
    {
        return $this->link_page_item_id;
    }

    /**
     * @param mixed $link_page_item_id
     */
    public function setLinkPageItemId($link_page_item_id): void
    {
        $this->link_page_item_id = $link_page_item_id;
    }

    /**
     * @return mixed
     */
    public function getItemTitle()
    {
        return $this->item_title;
    }

    /**
     * @param mixed $item_title
     */
    public function setItemTitle($item_title): void
    {
        $this->item_title = $item_title;
    }

    /**
     * @return mixed
     */
    public function getItemDate()
    {
        return $this->item_date;
    }

    /**
     * @param mixed $item_date
     */
    public function setItemDate($item_date): void
    {
        $this->item_date = $item_date;
    }

    /**
     * @return mixed
     */
    public function getItemContent()
    {
        return $this->item_content;
    }

    /**
     * @param mixed $item_content
     */
    public function setItemContent($item_content): void
    {
        $this->item_content = $item_content;
    }

    /**
     * @return mixed
     */
    public function getItemButtonText()
    {
        return $this->item_button_text;
    }

    /**
     * @param mixed $item_button_text
     */
    public function setItemButtonText($item_button_text): void
    {
        $this->item_button_text = $item_button_text;
    }

    /**
     * @return mixed
     */
    public function getItemButtonUrl()
    {
        return $this->item_button_url;
    }

    /**
     * @param mixed $item_button_url
     */
    public function setItemButtonUrl($item_button_url): void
    {
        $this->item_button_url = $item_button_url;
    }

    /**
     * @return mixed
     */
    public function getLinkedItemType()
    {
        return $this->linked_item_type;
    }

    /**
     * @param mixed $linked_item_type
     */
    public function setLinkedItemType($linked_item_type): void
    {
        $this->linked_item_type = $linked_item_type;
    }

    /**
     * @return mixed
     */
    public function getLinkedItemId()
    {
        return $this->linked_item_id;
    }

    /**
     * @param mixed $linked_item_id
     */
    public function setLinkedItemId($linked_item_id): void
    {
        $this->linked_item_id = $linked_item_id;
    }

    /**
     * @return mixed
     */
    public function getAttachmentId()
    {
        return $this->attachment_id;
    }

    /**
     * @param mixed $attachment_id
     */
    public function setAttachmentId($attachment_id): void
    {
        $this->attachment_id = $attachment_id;
    }
}