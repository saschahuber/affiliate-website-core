<?php

namespace saschahuber\affiliatewebsitecore;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\LogHelper;

require_once(VENDOR_BASE . '/autoload.php');

#[AllowDynamicProperties]
class GoogleLogin
{
    public $id;
    public $handle;
    private $secret;

    function __construct(string $app_id, string $callback_url = '', $access_token = false)
    {
        global $CONFIG;

        if (!isset($CONFIG->google_oauth_clients[$app_id]))
            return false;

        $this->id = $app_id;
        $this->secret = $CONFIG->google_oauth_clients[$app_id];

        try {
            $this->handle = new Google_Client();

            $this->handle->setClientId($this->id);
            $this->handle->setClientSecret($this->secret);

            $this->handle->setRedirectUri(
                substr($callback_url, 0, 4) == 'http' ? $callback_url : PROTOCOL . DOMAIN . '/' . $callback_url);

            if (!empty($access_token))
                $this->handle->setAccessToken($access_token);
        } catch (Exception $e) {
            LogHelper::error('Google', 0, 'Google App-Objekt konnte nicht erstellt werden: ' .
                $e->getMessage(), ERROR_ALERT);

            return false;
        }

        return $this->handle;
    }

    function getAuthUrl(array $scopes = ['email'])
    {
        if (!$this->handle)
            return false;

        try {
            foreach ($scopes as $scope)
                $this->handle->addScope($scope);

            $url = $this->handle->createAuthUrl();
        } catch (Exception $e) {
            LogHelper::error('GOOGLE', 1, 'Google Auth-URL konnte nicht generiert werden: ' .
                $e->getMessage(), ERROR_ALERT);

            return false;
        }

        return $url;
    }

    function getUser()
    {
        if (!$this->handle)
            return false;

        if (empty($_GET['code'])) {
            LogHelper::error('GOOGLE', 2, 'Google OAuth-Redirect failed; no `code` passed via GET.');
            return false;
        }

        try {
            $token = $this->handle->fetchAccessTokenWithAuthCode($_GET['code']);
        } catch (Exception $e) {
            LogHelper::error('GOOGLE', 3, 'Google Access-Token konnte nicht bezogen werden: ' . $e->getMessage());
            return false;
        }

        if (empty($token) || empty($token['access_token'])) {
            LogHelper::error('GOOGLE', 4, 'Google Access-Token konnte nicht bezogen werden: ' . print_r($token, true));
            return false;
        }

        try {
            $this->handle->setAccessToken($token['access_token']);
            $google_oauth = new Google_Service_Oauth2($this->handle);
            $user = $google_oauth->userinfo->get();
        } catch (Exception $e) {
            LogHelper::error('GOOGLE', 5, 'Google Nutzer-Daten konnten nicht abgefragt werden: ' .
                $e->getMessage(), ERROR_ALERT);

            return false;
        }

        return $user;
    }
}
