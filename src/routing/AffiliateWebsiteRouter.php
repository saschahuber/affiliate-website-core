<?php

namespace saschahuber\affiliatewebsitecore\routing;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\page_classes\website\CmsPage;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\routing\Router;

#[AllowDynamicProperties]
class AffiliateWebsiteRouter extends Router{
    public function getPageContent(){
        // Path
        $url = parse_url($_SERVER['REQUEST_URI']);
        $this->url_data = $url;

        $this->path = trim($url['path'], '/');
        $this->path_segments = explode('/', $this->path);

        $activated_route = $this->getPageByPath($this->path);

        if($activated_route){
            return (new $activated_route($this->path))->getContent();
        }

        $cms_page = new CmsPage($this->path);

        if($cms_page->getPageObject()){
            return $cms_page->getContent();
        }

        return ErrorHelper::http(ErrorHelper::HTTP_ERROR_NOT_FOUND, "Can't find page '{$this->path}'");
    }
}
