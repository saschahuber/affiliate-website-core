<?php

namespace saschahuber\affiliatewebsitecore;

use AllowDynamicProperties;
use Facebook\Authentication\AccessToken;
use Facebook\Facebook;
use saschahuber\saastemplatecore\helper\LogHelper;

require_once(VENDOR_BASE . '/autoload.php');

#[AllowDynamicProperties]
class FacebookLogin
{
    public $id;
    public $handle;
    private $secret;

    function __construct(string $app_id, string $app_secret, $access_token = false)
    {
        global $CONFIG;

        $this->id = $app_id;
        $this->secret = $app_secret;



        try {
            $arguments = [
                'app_id' => $this->id,
                'app_secret' => $this->secret,
                'default_graph_version' => 'v3.2'];

            if ($access_token && $access_token !== null) {
                $arguments['default_access_token'] = $access_token;
            }

            $this->handle = new Facebook($arguments);
        } catch (Exception $e) {
            LogHelper::error('FACEBOOK', 0, 'Facebook App-Objekt konnte nicht erstellt werden: ' .
                $e->getMessage(), ERROR_ALERT);

            return false;
        }

        return $this->handle;
    }

    function getAuthUrl(string $callback_url = '', array $scope = ['email'])
    {
        if (!$this->handle)
            return false;

        try {
            $url = $this->handle->getRedirectLoginHelper()->getLoginUrl(
                substr($callback_url, 0, 4) == 'http' ? $callback_url : PROTOCOL . DOMAIN . '/' . $callback_url,
                $scope);
        } catch (Exception $e) {
            LogHelper::error('FACEBOOK', 1, 'Facebook Auth-URL konnte nicht generiert werden: ' .
                $e->getMessage(), ERROR_ALERT);

            return false;
        }

        return $url;
    }

    function getUser()
    {
        if (!$this->handle)
            return false;

        try {
            $token = $this->handle->getRedirectLoginHelper()->getAccessToken();
        } catch (Exception $e) {
            LogHelper::error('FACEBOOK', 2, 'Facebook Access-Token konnte nicht bezogen werden: ' . $e->getMessage());
            return false;
        }

        if (empty($token)) {
            LogHelper::error('FACEBOOK', 3, 'Facebook Access-Token konnte nicht bezogen werden.');
            return false;
        }

        try {
            $user = $this->handle->get('/me?fields=id,name,email,picture', $token)->getGraphUser();
        } catch (Exception $e) {
            LogHelper::error('FACEBOOK', 4, 'Facebook Nutzer-Daten konnten nicht abgefragt werden: ' .
                $e->getMessage(), ERROR_ALERT);

            return false;
        }

        return $user;
    }

    function getExtendedToken(int $by_duration = ONE_WEEK * 2)
    {
        if (!$this->handle)
            return false;

        try {
            $access_token = new AccessToken(
                $this->handle->getDefaultAccessToken()->getValue(),
                NOW + $by_duration);
        } catch (Exception $e) {
            LogHelper::error('FACEBOOK', 4, 'Facebook Token konnte nicht verlängert werden: ' .
                $e->getMessage(), ERROR_ALERT);

            return false;
        }

        return $access_token->getValue();
    }
}
