<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;

class FaqGeneratorButton extends JsButton {
    public function __construct($faq_id, $id=null){
        parent::__construct('<i class="fas fa-robot"></i> FAQs mit KI generieren', null, $id);
        $this->faq_id = $faq_id;
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall(){
        return "generateFaqs(this, '{$this->faq_id}')";
    }

    static function getJsFiles(){
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/FaqGeneratorButton.js';
    }
}