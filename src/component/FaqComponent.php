<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\saastemplatecore\component\Accordion;
use saschahuber\saastemplatecore\component\AccordionItem;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;

class FaqComponent extends Component{
    public function __construct($questions, $id = null){
        $this->questions = $questions;
        parent::__construct($id);
    }

    protected function build(){
        if(count($this->questions) < 1){
            return;
        }

        echo $this->getJsonLd($this->questions);
        echo $this->getFaqContent($this->questions);
    }

    private function getFaqContent($questions){
        $accordion_id = 'accordion_' . uniqid();

        $accordion_items = [];
        foreach($questions as $question){
            $accordion_items[] = new AccordionItem($accordion_id, $question['question'], $question['answer']);
        }

        $accordion = new Accordion($accordion_items, ['elevated'], $accordion_id);

        return BufferHelper::buffered(function() use ($accordion){
            (new Text("Häufig gestellte Fragen", "h2"))->display();
            $accordion->display();
        });
    }

    private function getJsonLd($questions){
        return BufferHelper::buffered(function() use ($questions){
            $questions_json_ld = [];
            foreach($questions as $question){
                $questions_json_ld[] = $this->getJsonLdQuestion($question);
            }

            ?>
            <script type=application/ld+json>{
                    "@context": "https://schema.org",
                    "@type": "FAQPage",
                    "mainEntity": [<?=implode(', ', $questions_json_ld)?>]
                } </script>
            <?php
        });
    }

    private function getJsonLdQuestion($question){
        return BufferHelper::buffered(function() use ($question){
            ?>
            {
                "@type": "Question",
                "name": "<?=$question['question']?>",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "<?=$question['answer']?>"
                }
            }
            <?php
        });
    }
}