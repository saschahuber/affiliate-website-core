<?php

namespace saschahuber\affiliatewebsitecore\component;

use ItemPreviewHandler;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductComparisonManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class PaginatedProductComparisonTable extends PaginatedItemTable
{

    public function __construct($items, $offset = 0, $items_per_page = 25, $page_number = 1, $id = null)
    {
        parent::__construct($items, $offset, $items_per_page, $page_number, $id);

        $this->product_comparison_manager = new ProductComparisonManager();
    }

    protected function getColumnLabels()
    {
        $column_labels = [
            'Thumbnail',
            'Titel',
            'Status',
            'Datum',
            'Index/Follow',
            'In Suche verstecken',
            'Gesponsert',
            'VG-Wort',
            'Meta',
            'Aktionen',
        ];
        return $column_labels;
    }

    protected function getRow($item)
    {
        global $CONFIG;

        $product_comparison_actions = [];

        if ($item->status == Manager::STATUS_PUBLISH) {
            $product_comparison_actions[] = new LinkButton($CONFIG->website_domain . $this->product_comparison_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true);
        }
        $product_comparison_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => ProductComparisonManager::TYPE_PRODUCT_COMPARISON, 'id' => $item->id]);
        $product_comparison_actions[] = new LinkButton("/dashboard/produkte/vergleichsseiten/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        $product_comparison_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(ProductComparisonManager::TYPE_PRODUCT_COMPARISON, $item->id), 'item_type' => ProductComparisonManager::TYPE_PRODUCT_COMPARISON, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

        $cells = [
            new Image($this->product_comparison_manager->getThumbnailSrc($item), 200),
            $item->title,
            new EditableSelect('product_comparison', 'status', $item->id, $item->status, Manager::getStatusTypes()),
            new EditableTextInput('product_comparison', 'product_comparison_date', $item->id, $item->product_comparison_date),
            BufferHelper::buffered(function () use ($item) {
                (new EditableToggle('product_comparison', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('product_comparison', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
            }),
            new EditableToggle('product_comparison', 'hide_in_search', $item->id, $item->hide_in_search, true, "Verstecken"),
            new EditableToggle('product_comparison', 'is_sponsored', $item->id, $item->is_sponsored, true, "Gesponsert"),
            BufferHelper::buffered(function() use ($item){
                if($item->vg_wort_pixel_id) {
                    echo '<i class="fas fa-check-circle" style="color: green; font-size: 24px;"></i>';
                }
                else {
                    echo '<i class="fas fa-times-circle" style="color: darkred; font-size: 24px;"></i>';
                }
            }),
            BufferHelper::buffered(function () use ($item) {
                (new Toggle('has_meta_title', "Titel", isset($item->meta_title) && strlen($item->meta_title), true, true))->display();
                (new BreakComponent())->display();
                (new Toggle('has_meta_description', "Description", isset($item->meta_description) && strlen($item->meta_description), true, true))->display();
            }),
            new FloatContainer($product_comparison_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination()
    {
        (new Pagination('/dashboard/produkte/vergleichsseiten/' . Pagination::PAGINATION_PLACEHOLDER . '?' . $_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}