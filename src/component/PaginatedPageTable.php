<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\ConfirmButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class PaginatedPageTable extends PaginatedItemTable {

    public function __construct($items, $offset=0, $items_per_page=25, $page_number=1, $id=null){
        parent::__construct($items, $offset, $items_per_page, $page_number, $id);

        $this->page_manager = new PageManager();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Titel',
            'Status',
            'Datum',
            'Index/Follow',
            'VG-Wort',
            'Aktionen',
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $page_actions = [];

        if($item->status == Manager::STATUS_PUBLISH) {
            $page_actions[] = new LinkButton($CONFIG->website_domain . $this->page_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true);
        }
        $page_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => PageManager::TYPE_PAGE, 'id' => $item->id]);
        $page_actions[] = new LinkButton("/dashboard/seiten/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        if($item->status == Manager::STATUS_DRAFT) {
            $confirm_button_id = uniqid();
            $javascript_code = BufferHelper::buffered(function() use ($item, $confirm_button_id){
                $parent_node_selector = "document.getElementById('component_{$confirm_button_id}').parentNode.parentNode.parentNode.parentNode.parentNode";

                ?>
                doApiCall('page/delete', {item_id: <?=$item->id?>}, function(responseCode, response){
                /*
                if (displayContainer) {
                displayContainer.removeNode();
                }
                */
                <?=$parent_node_selector?>.removeNode();
                });
                <?php
            });
            $page_actions[] = new ConfirmButton('<i class="fas fa-trash"></i> Löschen', "Möchtest du diese Seite unwiderruflich löschen?", $javascript_code, $confirm_button_id);
        }

        $page_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(PageManager::TYPE_PAGE, $item->id), 'item_type' => PageManager::TYPE_PAGE, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

        $cells = [
            $item->title,
            new EditableSelect('page', 'status', $item->id, $item->status, Manager::getStatusTypes()),
            new EditableDateTimePicker('page', 'page_date', $item->id, $item->page_date),
            BufferHelper::buffered(function() use ($item){
                (new EditableToggle('page', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('page', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
            }),
            BufferHelper::buffered(function() use ($item){
                if($item->vg_wort_pixel_id) {
                    echo '<i class="fas fa-check-circle" style="color: green; font-size: 24px;"></i>';
                }
                else {
                    echo '<i class="fas fa-times-circle" style="color: darkred; font-size: 24px;"></i>';
                }
            }),
            new FloatContainer($page_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/blog/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}