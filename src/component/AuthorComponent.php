<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\component\scheme\SchemeOrgAuthor;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AuthorComponent extends Component
{
    public function __construct($author, $id = null)
    {
        $this->author = $author;
        parent::__construct($id);
    }

    protected function build()
    {
        global $CONFIG;

        $author = $this->author;

        $image_url = (new ImageManager())->getAttachmentUrl($this->author->attachment_id);

        (new Card([
            new Text('Über den Autor', 'h4', ['author-headline']),
            new BreakComponent(),
            new Row([
                new Column(new Image($image_url), ['col-md-3', 'col-12', 'author-image']),
                new Column(BufferHelper::buffered(function () use ($author) {
                    ?>
                    <div><p><strong><?= $author->title ?></strong></p></div>
                    <div>
                        <p><?= $author->description ?></p>
                    </div>
                    <?php
                }), ['col-md-9', 'col-12', 'author-info'])
            ])
        ], ['author-box']))->display();

        $links = [];

        if (isset($author->links)) {
            $links = $author->links;
        }

        (new SchemeOrgAuthor($author->title, "{$CONFIG->website_domain}/autoren/{$author->permalink}",
            $CONFIG->website_domain . $image_url, $author->job_title, $author->organization, $links))->display();
    }
}