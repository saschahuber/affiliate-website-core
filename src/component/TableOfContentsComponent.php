<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\saastemplatecore\component\Component;

class TableOfContentsComponent extends Component
{
    private $items;

    public function __construct($items, $id = null)
    {
        parent::__construct($id);
        $this->setItems($items);
    }

    protected function build()
    {
        ?>
        <style>
            nav#table-of-contents a{
                text-decoration: none !important;
                color: #0099cc !important;
            }

            nav#table-of-contents a:hover{
                font-weight: bold;
            }

            #table-of-contents {
                padding-top: 15px;
                padding-bottom: 15px;
            }

            #table-of-contents ol {
                padding-left: 0;
            }

            #table-of-contents ol ol{
                padding-left: 15px;
                margin: 0;
            }

            #table-of-contents li {
                list-style-position: outside;
                list-style-type: none;
                margin-top: 1px;
                padding-bottom: 5px;
                font-size: 13pt;
            }


            @media (min-width: 1600px) {
                .toc-sticky {
                    position: sticky;
                    top: 10px;
                }
            }

            @media (max-width: 1000px){
                #table-of-contents ol.level-3,
                #table-of-contents ol.level-4,
                #table-of-contents ol.level-5 {
                    display: none;
                }
            }

            #table-of-contents ol ol {
                border-top: solid 1px #ddd;
                padding-top: 3px;
            }

            #table-of-contents ol li:not(:first-of-type) {
                border-top: solid 1px #ddd;
                padding-bottom: 3px;
            }


        </style>

        <div class="card toc-sticky" style="margin: auto; max-width: 600px; margin-bottom: 15px;">
            <nav id="table-of-contents">
                <p class="centered"><strong>Inhaltsverzeichnis</strong></p>
                <ol>
                    <?php
                    $level = null;
                    foreach($this->getItems() as $item){
                        if($level !== null) {
                            if ($item['level'] > $level) {
                                echo '<ol class="level-'.$level.'">';
                            } else if ($item['level'] < $level) {
                                echo "</ol>";
                            }
                        }

                        $level = $item['level'];

                        ?>
                        <li class="level-<?=$level?>"><a href="#<?=$item['id']?>"><?=$item['title']?></a></li>
                        <?php
                    }
                    ?>
                </ol>
            </nav>
        </div>
        <?php
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }
}