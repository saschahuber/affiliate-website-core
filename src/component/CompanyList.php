<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\component\item\CompanyListItem;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\BufferHelper;

class CompanyList extends HTMLElement {

    public function __construct($items, $id=null){
        parent::__construct($this->getListContent($items));
    }

    protected function getListContent($items){
        return BufferHelper::buffered(function () use ($items){
            $this->company_manager = new CompanyManager();
            ?>
            <div>
                <?php
                $item_list = [];
                foreach($items as $item) {
                    $item->permalink = $this->company_manager->generatePermalink($item);
                    $item_list[] = (new CompanyListItem($item))->getContent();
                }
                echo implode((new BreakComponent())->getContent(), $item_list);
                ?>
            </div>
            <?php
        });
    }
}