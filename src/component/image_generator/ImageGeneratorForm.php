<?php

namespace saschahuber\affiliatewebsitecore\component\image_generator;

use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\Form;
use saschahuber\saastemplatecore\component\form\input\ColorInput;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;

class ImageGeneratorForm {
    private $layers, $width, $height, $background_color, $img_preview_id, $url_input_name;

    public function __construct($layers, $width=null, $height=null, $background_color=null, $url_input_name=null){
        $this->layers = $layers;
        $this->img_preview_id = 'img-preview-'.uniqid();
        $this->width = $width;
        $this->height = $height;
        $this->background_color = $background_color;
        $this->url_input_name = $url_input_name;
    }

    public function allHidden(){
        return $this->width !== null && $this->height !== null && $this->background_color !== null;
    }

    public function displayForm(){
        $rows = [];

        $img_id = $this->getImgPreviewid();

        $rows[] = [
            new ElevatedCard([
                new Text('Vorschau', 'label'),
                new BreakComponent(),
                BufferHelper::buffered(function () use ($img_id){
                    ?>
                    <p style="text-align: center;"><img id="<?=$img_id?>" style="max-height: 500px;"></p>
                    <?php
                })
            ])
        ];

        $all_hidden = $this->allHidden();

        $rows[] = [
            BufferHelper::buffered(function() use ($all_hidden){
                ?>
                <div class="general-params" <?=($all_hidden?'style="display: none;"':'')?>>
                    <?php
                    (new ElevatedCard([
                        new Text('Daten des Bildes', 'label'),
                        new Row([
                            new Column(new NumberInput('width', 'Breite', null, $this->width??1920, 0), ['col-lg-4', 'col-12']),
                            new Column(new NumberInput('height', 'Höhe', null, $this->height??1080, 0), ['col-lg-4', 'col-12']),
                            new Column(new ColorInput('background_color', 'Hintergrundfarbe', $this->background_color??'#fff'), ['col-lg-4', 'col-12']),
                        ]),
                        new HiddenInput('save_path', null)
                    ]))->display();
                    ?>
                </div>
                <?php
            })
        ];

        foreach($this->getLayers() as $layer){
            $rows[] = $layer->getInputRow();
        }

        $rows[] = [
            new JsButton('Bild generieren', "generateImageForParams(this, '".$this->getImgPreviewid()."', ".($this->getUrlInputName()?"'".$this->getUrlInputName()."'":"null").")")
        ];

        (new Form($rows))->display();
    }

    /**
     * @return mixed
     */
    public function getLayers()
    {
        return $this->layers;
    }

    /**
     * @param mixed $layers
     */
    public function setLayers($layers): void
    {
        $this->layers = $layers;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width): void
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height): void
    {
        $this->height = $height;
    }

    /**
     * @return mixed|null
     */
    public function getBackgroundColor()
    {
        return $this->background_color;
    }

    /**
     * @param mixed|null $background_color
     */
    public function setBackgroundColor($background_color): void
    {
        $this->background_color = $background_color;
    }

    /**
     * @return mixed
     */
    public function getImgPreviewid()
    {
        return $this->img_preview_id;
    }

    /**
     * @param mixed $img_preview_id
     */
    public function setImgPreviewid($img_preview_id): void
    {
        $this->img_preview_id = $img_preview_id;
    }

    /**
     * @return mixed
     */
    public function getUrlInputName()
    {
        return $this->url_input_name;
    }

    /**
     * @param mixed $url_input_name
     */
    public function setUrlInputName($url_input_name): void
    {
        $this->url_input_name = $url_input_name;
    }
}