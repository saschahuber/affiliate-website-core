<?php

namespace saschahuber\affiliatewebsitecore\component\image_generator\layer;

use saschahuber\affiliatewebsitecore\component\image_generator\layer\ImageGeneratorLayer;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\form\input\ColorInput;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;

class TextImageLayer extends ImageGeneratorLayer{
    const FUNCTION = 'withAlignedText';

    private $text, $size, $max_width, $hexcode, $alignment, $margin;
    private $text_hidden, $size_hidden, $max_width_hidden, $hexcode_hidden, $alignment_hidden, $margin_hidden;

    public function __construct($text=null, $size=null, $max_width=null, $hexcode=null, $alignment=null, $margin=null,
                                $text_hidden=false, $size_hidden=false, $max_width_hidden=false,
                                $hexcode_hidden=false, $alignment_hidden=false, $margin_hidden=false){
        parent::__construct(self::FUNCTION);

        $this->text = $text;
        $this->size = $size;
        $this->max_width = $max_width;
        $this->hexcode = $hexcode;
        $this->alignment = $alignment;
        $this->margin = $margin;

        $this->text_hidden = $text_hidden;
        $this->size_hidden = $size_hidden;
        $this->max_width_hidden = $max_width_hidden;
        $this->hexcode_hidden = $hexcode_hidden;
        $this->alignment_hidden = $alignment_hidden;
        $this->margin_hidden = $margin_hidden;
    }

    public function allHidden()
    {
        return $this->text_hidden && $this->size_hidden && $this->max_width_hidden
            && $this->hexcode_hidden && $this->alignment_hidden && $this->margin_hidden;
    }

    public function getInputRowItems(){

        if($this->text_hidden){
            $text_input = new HiddenInput('text', $this->text);
        }
        else{
            $text_input = new TextInput('text', 'Text', null, $this->text);
        }

        if($this->size_hidden){
            $size_input = new HiddenInput('size', $this->size);
        }
        else{
            $size_input = new NumberInput('size', 'Schriftgröße', null, $this->size);
        }

        if($this->max_width_hidden){
            $max_width_input = new HiddenInput('max_width', $this->max_width);
        }
        else{
            $max_width_input = new NumberInput('max_width', 'Maximale Breite', null, $this->max_width);
        }

        if($this->hexcode_hidden){
            $hexcode_input = new HiddenInput('hexcode', $this->hexcode);
        }
        else{
            $hexcode_input = new ColorInput('hexcode', 'Farbe', $this->hexcode);
        }

        if($this->alignment_hidden){
            $alignment_input = new HiddenInput('alignment', $this->alignment);
        }
        else{
            $alignment_input = new Select('alignment', 'Orientierung', $this->getAlignmentOptions(), $this->alignment);
        }

        if($this->margin_hidden){
            $margin_input = new HiddenInput('margin', $this->margin);
        }
        else{
            $margin_input = new NumberInput('margin', 'Abstand', null, $this->margin);
        }

        $outputs = [];

        if(!$this->allHidden()){
            $outputs[] = new Text('Text', 'label');
        }

        $outputs[] = new Row([
            new Column($text_input, ['col-lg-4', 'col-12']),
            new Column($size_input, ['col-lg-4', 'col-12']),
            new Column($max_width_input, ['col-lg-4', 'col-12']),
            new Column($hexcode_input, ['col-lg-4', 'col-12']),
            new Column($alignment_input, ['col-lg-4', 'col-12']),
            new Column($margin_input, ['col-lg-4', 'col-12'])
        ]);

        return $outputs;
    }
}