<?php

namespace saschahuber\affiliatewebsitecore\component\image_generator\layer;

use saschahuber\affiliatewebsitecore\service\ImageGeneratorService;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\helper\BufferHelper;

abstract class ImageGeneratorLayer {
    protected $function;

    public function __construct($function){
        $this->function = $function;
    }

    public function allHidden(){
        return false;
    }

    public function getAlignmentOptions(){
        return [
                ImageGeneratorService::TopLeft => 'Oben Links',
                ImageGeneratorService::TopCenter => 'Oben Mitte',
                ImageGeneratorService::TopRight => 'Oben Rechts',
                ImageGeneratorService::CenterLeft => 'Mitte Links',
                ImageGeneratorService::Center => 'Mitte',
                ImageGeneratorService::CenterRight => 'Mitte Rechts',
                ImageGeneratorService::BottomLeft => 'Unten Links',
                ImageGeneratorService::BottomCenter => 'Unten Mitte',
                ImageGeneratorService::BottomRight => 'Unten Rechts'
        ];
    }

    public function getInputRow(){
        $items = $this->getInputRowItems();

        $function = $this->getFunction();

        return [BufferHelper::buffered(function() use ($items, $function){
            ?>
            <div class="image-generator-layer" data-layer="<?=$function?>" <?=($this->allHidden()?'style="display:none;"':'')?>>
                <?php (new ElevatedCard([$items]))->display(); ?>
            </div>
            <?php
        })];
    }

    public abstract function getInputRowItems();

    /**
     * @return mixed
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param mixed $function
     */
    public function setFunction($function): void
    {
        $this->function = $function;
    }
}