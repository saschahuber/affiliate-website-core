<?php

namespace saschahuber\affiliatewebsitecore\component\image_generator\layer;

use saschahuber\affiliatewebsitecore\component\form\input\AttachmentSelectForGenerator;
use saschahuber\affiliatewebsitecore\service\ImageGeneratorService;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\Row;

class AttachmentLayer extends ImageGeneratorLayer{
    const FUNCTION = 'withAttachmentImage';

    private $alignment, $vertical_offset, $horizontal_offset;
    private $alignment_hidden, $vertical_offset_hidden, $horizontal_offset_hidden;

    public function __construct($alignment=ImageGeneratorService::Center, $vertical_offset=0, $horizontal_offset=0,
                                $alignment_hidden=false, $vertical_offset_hidden=false, $horizontal_offset_hidden=false){
        parent::__construct(self::FUNCTION);
        $this->alignment = $alignment;
        $this->vertical_offset = $vertical_offset;
        $this->horizontal_offset = $horizontal_offset;

        $this->alignment_hidden = $alignment_hidden;
        $this->vertical_offset_hidden = $vertical_offset_hidden;
        $this->horizontal_offset_hidden = $horizontal_offset_hidden;
    }

    public function allHidden(){
        return $this->alignment_hidden && $this->vertical_offset_hidden && $this->horizontal_offset_hidden;
    }

    public function getInputRowItems(){
        if($this->alignment_hidden){
            $alignment_input = new HiddenInput('alignment', $this->alignment);
        }
        else{
            $alignment_input = new Select('alignment', 'Orientierung', $this->getAlignmentOptions(), $this->alignment);
        }

        if($this->vertical_offset_hidden){
            $vertical_offset_input = new HiddenInput('vertical_offset', $this->vertical_offset);
        }
        else{
            $vertical_offset_input = new NumberInput('vertical_offset', 'Vertikales Offset', null, $this->vertical_offset);
        }

        if($this->horizontal_offset_hidden){
            $horizontal_offset_input = new HiddenInput('horizontal_offset', $this->horizontal_offset);
        }
        else{
            $horizontal_offset_input = new NumberInput('horizontal_offset', 'Horizontales Offset', null, $this->horizontal_offset);
        }

        $outputs = [];

        $outputs[] = new Row([
            new Column(new AttachmentSelectForGenerator())
        ]);

        $outputs[] = new Row([
            new Column($alignment_input, ['col-lg-4', 'col-12']),
            new Column($vertical_offset_input, ['col-lg-4', 'col-12']),
            new Column($horizontal_offset_input, ['col-lg-4', 'col-12'])
        ]);

        return $outputs;
    }
}