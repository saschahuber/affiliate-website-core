<?php

namespace saschahuber\affiliatewebsitecore\component\image_generator\layer;

use saschahuber\affiliatewebsitecore\component\image_generator\layer\ImageGeneratorLayer;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;

class BackgroundImageLayer extends ImageGeneratorLayer{
    const FUNCTION = 'withBackgroundImage';

    private $img_src, $alignment, $vertical_offset, $horizontal_offset;
    private $img_src_hidden, $alignment_hidden, $vertical_offset_hidden, $horizontal_offset_hidden;

    public function __construct($img_src=null, $alignment=null, $vertical_offset=null, $horizontal_offset=null,
        $img_src_hidden=false, $alignment_hidden=false, $vertical_offset_hidden=false, $horizontal_offset_hidden=false){
        parent::__construct(self::FUNCTION);
        $this->img_src = $img_src;
        $this->alignment = $alignment;
        $this->vertical_offset = $vertical_offset;
        $this->horizontal_offset = $horizontal_offset;

        $this->img_src_hidden = $img_src_hidden;
        $this->alignment_hidden = $alignment_hidden;
        $this->vertical_offset_hidden = $vertical_offset_hidden;
        $this->horizontal_offset_hidden = $horizontal_offset_hidden;
    }

    public function allHidden(){
        return $this->img_src_hidden && $this->alignment_hidden && $this->vertical_offset_hidden && $this->horizontal_offset_hidden;
    }

    public function getInputRowItems(){
        if($this->img_src_hidden){
            $image_src_input = new HiddenInput('image_src', $this->img_src);
        }
        else{
            $image_src_input = new TextInput('image_src', 'URL des Bildes', null, $this->img_src, false, true);
        }

        if($this->alignment_hidden){
            $alignment_input = new HiddenInput('alignment', $this->alignment);
        }
        else{
            $alignment_input = new Select('alignment', 'Orientierung', $this->getAlignmentOptions(), $this->alignment);
        }

        if($this->vertical_offset_hidden){
            $vertical_offset_input = new HiddenInput('vertical_offset', $this->vertical_offset);
        }
        else{
            $vertical_offset_input = new NumberInput('vertical_offset', 'Vertikales Offset', null, $this->vertical_offset);
        }

        if($this->horizontal_offset_hidden){
            $horizontal_offset_input = new HiddenInput('horizontal_offset', $this->horizontal_offset);
        }
        else{
            $horizontal_offset_input = new NumberInput('horizontal_offset', 'Horizontales Offset', null, $this->horizontal_offset);
        }

        $outputs = [];

        if(!$this->allHidden()){
            $outputs[] = new Text('Hintergrund-URL', 'label');
        }

        $outputs[] = new Row([
            new Column($image_src_input, ['col-lg-3', 'col-12']),
            new Column($alignment_input, ['col-lg-3', 'col-12']),
            new Column($vertical_offset_input, ['col-lg-3', 'col-12']),
            new Column($horizontal_offset_input, ['col-lg-3', 'col-12'])
        ]);

        return $outputs;
    }
}