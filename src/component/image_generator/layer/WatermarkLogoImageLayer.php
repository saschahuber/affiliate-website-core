<?php

namespace saschahuber\affiliatewebsitecore\component\image_generator\layer;

use saschahuber\affiliatewebsitecore\component\image_generator\layer\ImageGeneratorLayer;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;

class WatermarkLogoImageLayer extends ImageGeneratorLayer{
    const FUNCTION = 'withWatermarkLogo';

    private $vertical_offset, $alignment, $size_multiplier;

    public function __construct($vertical_offset=null, $alignment=null, $size_multiplier=null){
        parent::__construct(self::FUNCTION);
        $this->setVerticalOffset($vertical_offset);
        $this->setAlignment($alignment);
        $this->setSizeMultiplier($size_multiplier);
    }

    #int $x, int $y, int $width, int $height, string $hexcode = "#000000", float $opacity = 1.0,
    #                  array $border_color = "#000000", float $border_opacity = 0.0, int $border_width = 2

    public function getInputRowItems(){

        if($this->vertical_offset){
            $vertical_offset_input = new HiddenInput('vertical_offset', $this->vertical_offset);
        }
        else{
            $vertical_offset_input = new NumberInput('vertical_offset', 'Vertikales Offset', null, $this->getVerticalOffset());
        }

        if($this->alignment){
            $alignment_input = new HiddenInput('alignment', $this->alignment);
        }
        else{
            $alignment_input = new Select('alignment', 'Position', $this->getAlignmentOptions(), $this->getAlignment());
        }

        if($this->size_multiplier){
            $size_multiplier_input = new HiddenInput('size_multiplier', $this->size_multiplier);
        }
        else{
            $size_multiplier_input = new TextInput('size_multiplier', 'Größen Multiplikator', null, $this->getSizeMultiplier());
        }

        return [
            new Text('Wasserzeichen', 'label'),
            new Row([
                new Column($vertical_offset_input, ['col-lg-4', 'col-12']),
                new Column($alignment_input, ['col-lg-4', 'col-12']),
                new Column($size_multiplier_input, ['col-lg-4', 'col-12']),
            ])
        ];
    }

    /**
     * @return mixed
     */
    public function getVerticalOffset()
    {
        return $this->vertical_offset;
    }

    /**
     * @param mixed $vertical_offset
     */
    public function setVerticalOffset($vertical_offset): void
    {
        $this->vertical_offset = $vertical_offset;
    }

    /**
     * @return mixed
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * @param mixed $alignment
     */
    public function setAlignment($alignment): void
    {
        $this->alignment = $alignment;
    }

    /**
     * @return mixed
     */
    public function getSizeMultiplier()
    {
        return $this->size_multiplier;
    }

    /**
     * @param mixed $size_multiplier
     */
    public function setSizeMultiplier($size_multiplier): void
    {
        $this->size_multiplier = $size_multiplier;
    }
}