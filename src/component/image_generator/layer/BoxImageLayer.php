<?php

namespace saschahuber\affiliatewebsitecore\component\image_generator\layer;

use saschahuber\affiliatewebsitecore\component\image_generator\layer\ImageGeneratorLayer;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;

class BoxImageLayer extends ImageGeneratorLayer{
    const FUNCTION = 'withBox';
    
    private $x_pos, $y_pos, $width, $height, $color, $opacity, $border_color, $border_opacity, $border_width;
    private $x_pos_hidden, $y_pos_hidden, $width_hidden, $height_hidden, $color_hidden,
        $opacity_hidden, $border_color_hidden, $border_opacity_hidden, $border_width_hidden;

    public function __construct($x_pos=null, $y_pos=null, $width=null, $height=null, $color=null, $opacity=null,
                                $border_color=null, $border_opacity=null, $border_width=null, $x_pos_hidden=false,
                                $y_pos_hidden=false, $width_hidden=false, $height_hidden=false, $color_hidden=false,
                                $opacity_hidden=false, $border_color_hidden=false, $border_opacity_hidden=false, $border_width_hidden=false){
        parent::__construct(self::FUNCTION);

        $this->x_pos = $x_pos;
        $this->y_pos = $y_pos;
        $this->width = $width;
        $this->height = $height;
        $this->color = $color;
        $this->opacity = $opacity;
        $this->border_color = $border_color;
        $this->border_opacity = $border_opacity;
        $this->border_width = $border_width;

        $this->x_pos_hidden = $x_pos_hidden;
        $this->y_pos_hidden = $y_pos_hidden;
        $this->width_hidden = $width_hidden;
        $this->height_hidden = $height_hidden;
        $this->color_hidden = $color_hidden;
        $this->opacity_hidden = $opacity_hidden;
        $this->border_color_hidden = $border_color_hidden;
        $this->border_opacity_hidden = $border_opacity_hidden;
        $this->border_width_hidden = $border_width_hidden;
    }

    public function allHidden()
    {
        return $this->x_pos_hidden && $this->y_pos_hidden && $this->width_hidden
            && $this->height_hidden && $this->color_hidden && $this->opacity_hidden
            && $this->border_color_hidden && $this->border_opacity_hidden && $this->border_width_hidden;
    }

    public function getInputRowItems(){

        if($this->x_pos_hidden){
            $x_pos_input = new HiddenInput('x_pos', $this->x_pos);
        }
        else{
            $x_pos_input = new NumberInput('x_pos', 'X-Position', null, $this->x_pos);
        }

        if($this->y_pos_hidden){
            $y_pos_input = new HiddenInput('y_pos', $this->y_pos);
        }
        else{
            $y_pos_input = new NumberInput('y_pos', 'Y-Position', null, $this->y_pos);
        }

        if($this->width_hidden){
            $widh_input = new HiddenInput('width', $this->width);
        }
        else{
            $widh_input = new NumberInput('width', 'Breite', null, $this->width);
        }

        if($this->height_hidden){
            $height_input = new HiddenInput('height', $this->height);
        }
        else{
            $height_input = new NumberInput('height', 'Höhe', null, $this->height);
        }

        if($this->color_hidden){
            $color_input = new HiddenInput('color', $this->color);
        }
        else{
            $color_input = new NumberInput('color', 'Farbe', null, $this->color);
        }

        if($this->opacity_hidden){
            $opacity_input = new HiddenInput('opacity', $this->opacity);
        }
        else{
            $opacity_input = new NumberInput('opacity', 'Deckkraft', null, $this->opacity);
        }

        if($this->border_color_hidden){
            $border_color_input = new HiddenInput('border_color', $this->border_color);
        }
        else{
            $border_color_input = new NumberInput('border_color', 'Ramenfarbe', null, $this->border_color);
        }

        if($this->border_opacity_hidden){
            $border_opacity_input = new HiddenInput('border_opacity', $this->border_opacity);
        }
        else{
            $border_opacity_input = new NumberInput('border_opacity', 'Rahmen-Deckkraft', null, $this->border_opacity);
        }

        if($this->border_width_hidden){
            $border_width_input = new HiddenInput('border_width', $this->border_width);
        }
        else{
            $border_width_input = new NumberInput('border_width', 'Rahmen-Dicke', null, $this->border_width);
        }

        $outputs = [];

        if(!$this->allHidden()){
            $outputs[] = new Text('Farbige Box', 'label');
        }

        $outputs[] = new Row([
            new Column($x_pos_input, ['col-lg-4', 'col-12']),
            new Column($y_pos_input, ['col-lg-4', 'col-12']),
            new Column($widh_input, ['col-lg-4', 'col-12']),
            new Column($height_input, ['col-lg-4', 'col-12']),
            new Column($color_input, ['col-lg-4', 'col-12']),
            new Column($opacity_input, ['col-lg-4', 'col-12']),
            new Column($border_color_input, ['col-lg-4', 'col-12']),
            new Column($border_opacity_input, ['col-lg-4', 'col-12']),
            new Column($border_width_input, ['col-lg-4', 'col-12']),
        ]);

        return $outputs;
    }
}