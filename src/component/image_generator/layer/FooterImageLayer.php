<?php

namespace saschahuber\affiliatewebsitecore\component\image_generator\layer;

use saschahuber\affiliatewebsitecore\component\image_generator\layer\ImageGeneratorLayer;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\form\input\ColorInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\Text;

class FooterImageLayer extends ImageGeneratorLayer{
    const FUNCTION = 'withFooterWatermark';

    public function __construct(){
        parent::__construct(self::FUNCTION);
    }

    #int $x, int $y, int $width, int $height, string $hexcode = "#000000", float $opacity = 1.0,
    #                  array $border_color = "#000000", float $border_opacity = 0.0, int $border_width = 2

    public function getInputRowItems(){
        return [
            new Text('Footer-Text', 'label'),
            new Row([
                new Column(new TextInput('watermark_text', 'Text', null, null), ['col-lg-4', 'col-12']),
                new Column(new ColorInput('color', 'Textfarbe', null, '#000000'), ['col-lg-4', 'col-12']),
                new Column(new NumberInput('textsize', 'Schriftgröße', null, 32), ['col-lg-4', 'col-12'])
            ])
        ];
    }
}