<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;

class ApplicationAnalyticsTable extends ItemTable {

    public function __construct($items, $id=null){
        parent::__construct($items, $id);
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Zeit',
            'Event-Typ',
            'URL',
            #'User-ID',
            'Identifier',
            'User-Agent',
            'Referrer',
            'UTM',
            'Daten'
        ];
        return $column_labels;
    }

    protected function getRow($item){
        $utm_data = [];
        if($item->utm_source && strlen($item->utm_source)){
            $utm_data[] = "Source: ".$item->utm_source;
        }
        if($item->utm_medium && strlen($item->utm_medium)){
            $utm_data[] = "Medium: ".$item->utm_medium;
        }
        if($item->utm_campaign && strlen($item->utm_campaign)){
            $utm_data[] = "Kampagne: ".$item->utm_campaign;
        }
        if($item->utm_content && strlen($item->utm_content)){
            $utm_data[] = "Content: ".$item->utm_content;
        }
        if($item->utm_term && strlen($item->utm_term)){
            $utm_data[] = "Term: ".$item->utm_term;
        }

        $identifier_buttons = [];
        if($item->session_id && strlen($item->session_id)){
            $identifier_buttons[] = new LinkButton('/dashboard/system/analytics/details?session_id='.$item->session_id, 'Session', null, false, true);
        }
        if($item->fingerprint && strlen($item->fingerprint)){
            $identifier_buttons[] = new LinkButton('/dashboard/system/analytics/details?fingerprint='.$item->fingerprint, 'Fingerprint', null, false, true);
        }
        if($item->user_ip && strlen($item->user_ip)){
            $identifier_buttons[] = new LinkButton('/dashboard/system/analytics/details?ip='.$item->user_ip, 'IP: '.$item->user_ip, null, false, true);
        }
        if($item->user_profile_fingerprint && strlen($item->user_profile_fingerprint)){
            $identifier_buttons[] = new LinkButton('/dashboard/user-profiles/'.$item->user_profile_fingerprint . '/details', 'Profil: '.$item->user_profile_fingerprint, null, false, true);
        }

        $analytics_data = [];
        $data = json_decode($item->event_data, true);
        switch ($item->event_type){
            case 'product_click':
                $product_id = ArrayHelper::getArrayValue($data, 'product_id');
                if($product_id) {
                    $product = ((AffiliateInterfacesHelper::getProductManager()))->getById($product_id);
                    $analytics_data[] = new LinkButton('/dashboard/produkte/bearbeiten/'.$product_id, $product->title, null, false, true);
                }
                break;
            case 'search':
                $keywords = ArrayHelper::getArrayValue($data, 'keywords');
                if($keywords) {
                    $analytics_data[] = "Keyword: $keywords";
                }
                break;
            case 'push_subscription':
                $push_endpoint = ArrayHelper::getArrayValue($data, 'keywords');
                $push_key_auth = ArrayHelper::getArrayValue($data, 'push_key_auth');
                $push_key_p256dh = ArrayHelper::getArrayValue($data, 'push_key_p256dh');
                //Eventuell Button integrieren, um Push-Subscription zu ermitteln
                break;
            default:
                if($data) {
                    $analytics_data[] = json_encode($data);
                }
                break;
        }


        $cells = [
            $item->log_time,
            $item->event_type,
            BufferHelper::buffered(function () use ($item){
                ?>
                <div style="max-width: 300px; margin: auto;">
                    <?=$item->url?>
                </div>
                <?php
            }),
            #($log_item->user_id&&strlen($log_item->user_id))?new LinkButton('/dashboard/system/analytics/details?user_id='.$log_item->user_id, $log_item->user_id, null, false, true):'',
            new FloatContainer($identifier_buttons),
            BufferHelper::buffered(function () use ($item){
                ?>
                <div style="max-width: 300px; margin: auto;">
                    <?php
                    (new LinkButton('/dashboard/system/analytics/details?user_agent='.$item->user_agent, $item->user_agent, null, false, true))->display();
                    ?>
                </div>
                <?php
            }),
            BufferHelper::buffered(function () use ($item){
                if(!($item->referrer && strlen($item->referrer))){
                    return;
                }

                ?>
                <div style="max-width: 300px; margin: auto;">
                    <?php
                    (new LinkButton('/dashboard/system/analytics/details?referrer='.$item->referrer, $item->referrer, null, false, true))->display();
                    ?>
                </div>
                <?php
            }),
            new ListContainer($utm_data),
            new ListContainer($analytics_data)
        ];
        return new TableRow($cells);
    }
}