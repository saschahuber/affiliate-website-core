<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;

class PaginatedLinkPageItemTable extends PaginatedItemTable {

    public function __construct($items, $offset=0, $items_per_page=25, $page_number=1, $id=null){
        parent::__construct($items, $offset, $items_per_page, $page_number, $id);

        $this->link_page_item_manager = new LinkPageItemManager();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Thumbnail',
            'Titel',
            'Status',
            'Datum',
            'Button',
            'Aktionen',
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $post_actions = [];
        $post_actions[] = new LinkButton("/dashboard/seiten/links/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        $post_actions[] = new DeleteButton('link_page_item', $item->id, 5);

        $image = null;
        if($item->attachment_id){
            $image = new Image($this->link_page_item_manager->getThumbnailSrc($item), 200);
        }
        else if($item->linked_item_type == ProductManager::TYPE_PRODUCT && $item->linked_item_id && $item->attachment_id === null){
            $product_manager = AffiliateInterfacesHelper::getProductManager();
            $product = $product_manager->getById($item->linked_item_id);

            $image = $product_manager->getProductImageTag($product);
        }
        else if($item->linked_item_type == PostManager::TYPE_POST && $item->linked_item_id && $item->attachment_id === null){
            $post_manager = new PostManager();
            $product = $post_manager->getById($item->linked_item_id);

            $image = new Image($post_manager->getThumbnailSrc($product));
        }

        $cells = [
            BufferHelper::buffered(function() use ($image){
                ?>
                <div style="max-width: 200px; text-align: center;">
                    <?php
                    if($image !== null) {
                        if($image instanceof Component) {
                            $image->display();
                        }
                        else{
                            echo $image;
                        }
                    }
                    ?>
                </div>
                <?php
            }),
            $item->title,
            new EditableSelect('link_page_item', 'status', $item->id, $item->status, Manager::getStatusTypes()),
            new EditableDateTimePicker('link_page_item', 'link_page_item_date', $item->id, $item->link_page_item_date),
            $this->link_page_item_manager->getLinkButton($item),
            new FloatContainer($post_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/seiten/links/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}