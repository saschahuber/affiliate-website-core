<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;

class AiProductDataLoaderButton extends JsButton
{
    private $product_id, $field_data, $model;

    public function __construct($product_id, $field_data, $model = ChatGenerationTemplate::MODEL_CHAT_GPT_16K, $id = null)
    {
        parent::__construct('<i class="fas fa-robot"></i> Produktdaten laden (' . $model . ')', null, $id);
        $this->setProductId($product_id);
        $this->setFieldData($field_data);
        $this->setModel($model);
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall()
    {
        return "loadProductDataWithAi(this, '{$this->getProductId()}', '" . $this->getModel() . "', '" . base64_encode(json_encode($this->getFieldData())) . "')";
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id): void
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getFieldData()
    {
        return $this->field_data;
    }

    /**
     * @param mixed $field_data
     */
    public function setFieldData($field_data): void
    {
        $this->field_data = $field_data;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model): void
    {
        $this->model = $model;
    }

    static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/AiProductDataLoaderButton.js';
    }
}