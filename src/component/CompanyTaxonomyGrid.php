<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\component\item\CompanyTaxonomyGridItem;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\BufferHelper;

class CompanyTaxonomyGrid extends HTMLElement {

    public function __construct($items, $id=null){
        parent::__construct($this->getGridContent($items));
    }

    protected function getGridContent($items){
        return BufferHelper::buffered(function () use ($items){
            $this->company_manager = new CompanyManager();
            ?>
            <div>
                <?php
                $item_list = [];
                foreach($items as $item) {
                    $item->permalink = $this->company_manager->getTaxonomyPermalink($item);
                    $item_list[] = (new CompanyTaxonomyGridItem($item))->getContent();
                }

                (new GridContainer($item_list, 4, GridContainer::SIZE_MD))->display();
                ?>
            </div>
            <?php
        });
    }
}