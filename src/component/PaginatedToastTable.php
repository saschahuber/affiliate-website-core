<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\service\ScheduledToastService;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\component\ToastComponent;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class PaginatedToastTable extends PaginatedItemTable {

    public function __construct($items, $offset=0, $items_per_page=25, $toast_number=1, $id=null){
        parent::__construct($items, $offset, $items_per_page, $toast_number, $id);

        $this->toast_service = new ScheduledToastService();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Titel',
            'Aktiv?',
            'Aktiv von',
            'Aktiv bis',
            'Vorschau',
            'Bedingungen',
            'Aktionen'
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $toast_actions = [];

        $toast_actions[] = new LinkButton("/dashboard/toasts/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        $toast_actions[] = new DeleteButton('toast', $item->id, 5);

        #$toast_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(PageManager::TYPE_PAGE, $item->id), 'item_type' => PageManager::TYPE_PAGE, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

        $cells = [
            $item->title,
            new EditableToggle('toast', 'is_active', $item->id, $item->is_active, true),
            DateHelper::displayDateTime($item->active_start),
            DateHelper::displayDateTime($item->active_end),
            BufferHelper::buffered(function() use ($item){
                ?>
                <div>
                    <div style="width: fit-content; margin: auto; margin-top: 10px; margin-bottom: 10px;">
                        <?php
                        (new ToastComponent($item->title, $item->content, $item->small_info))->display();
                        ?>
                    </div>
                </div>
                <?php
            }),
            BufferHelper::buffered(function() use ($item){
                ?>
                <div style="margin: auto; max-width: 400px;">
                    <ul>
                        <?php
                        foreach ($item->conditions as $condition) {
                            (new Text($condition->condition_type.': '.$condition->condition_data, 'li'))->display();
                        }
                        ?>
                    </ul>
                </div>
                <?php
            }),
            new FloatContainer($toast_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/toasts/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}