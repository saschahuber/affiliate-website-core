<?php

namespace saschahuber\affiliatewebsitecore\component\scaffolding;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\component\scaffolding\Main;
use saschahuber\saastemplatecore\helper\Helper;

class AffiliateMain extends Main {

    protected function build()
    {
        global $ROUTER, $CONFIG, $DB;

        ?>
        <main>
            <div class="toast-container full-width-container" id="toast-container"></div>

            <!-- Page main HTML -->
            <?php
            if (!empty($ROUTER->main_html)){
                if(!Helper::isAdminApp()) {
                    $ROUTER->main_html = ShortcodeHelper::doShortcode($ROUTER->main_html);
                }
                echo $ROUTER->main_html;
            }

            if($CONFIG->show_query_log || Helper::isAdminApp()) {
                $DB->displayShowQueryLogButton();
            }

            #if(!Helper::isAdminApp() && AuthHelper::isAdminPanelUser()){
            #    (new SEOAnalyzerButton())->display();
            #}

            if(Helper::isAdminApp() || AuthHelper::isAdminPanelUser()){
                ?>
                <script>
                    window.addEventListener('load', function (){
                        updateFabs();
                    });
                </script>
                <?php
            }
            ?>
        </main>
        <?php
    }
}