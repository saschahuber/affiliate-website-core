<?php

namespace saschahuber\affiliatewebsitecore\component\scaffolding;

use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\scaffolding\FooterWithLinkGroups;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\service\FooterLinkGroupService;

class AffiliateFooterWithLinkGroups extends FooterWithLinkGroups {
    protected function build()
    {
        global $ROUTER, $CONFIG;
        ?>
        <footer>
            <div class="footer-container">
                <?php

                $footer_link_group_service = new FooterLinkGroupService();

                $columns = [];
                foreach($footer_link_group_service->getGroups() as $group){
                    if(!$group->active){
                        continue;
                    }

                    $card_contents = [new Text($group->title, 'h3')];
                    foreach($group->items as $item){
                        if(!$item->active){
                            continue;
                        }
                        $card_contents[] = new HTMLElement('<p><a href="'.$item->url.'">'.$item->title.'</a></p>');
                    }

                    $columns[] = new Column($card_contents, ['col-md-'.($group->span?:'4'), 'col-12']);
                }

                (new Row([$columns], ['footer-content'], null))->display();


                ?>

                <br>

                <div style="max-width: 600px; font-size: 12px; text-align: center; margin: auto;">
                    Die Links, die mit einem * markiert sind, stellen Affiliate-Links dar. Wenn ein Kauf über einen dieser Links erfolgt, erhalten wir eine Provision. Für dich ändert das aber nichts am Preis.
                </div>

                <br>

                <small>
                    <span>
                        &copy; <?= date('Y') ?>
                        <strong><a href="/"><?=$CONFIG->app_name?></a></strong>
                        mit viel <i class="fas fa-heart" style="color: var(--primary_color);"></i> erstellt
                    </span>
                    <!-- Footer links -->
                    <span>
                        &nbsp; &nbsp;
                        | <a href="/legal/impressum" rel="nofollow">Impressum</a>
                        | <a href="/legal/datenschutz" rel="nofollow">Datenschutz</a>
                        | <a href="/werbe-hinweis" rel="nofollow">Werbe-Hinweis</a>
                    </span>
                </small>

                <?php if (isset($ROUTER->vg_wort_marke) && $ROUTER->vg_wort_marke !== null): ?>
                    <img src="https://vg08.met.vgwort.de/na/<?= $ROUTER->vg_wort_marke ?>" width="1" height="1" alt="">
                <?php endif; ?>
            </div>
        </footer>
        <?php
    }
}