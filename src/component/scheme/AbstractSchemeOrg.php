<?php

namespace saschahuber\affiliatewebsitecore\component\scheme;

use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\helper\BufferHelper;

abstract class AbstractSchemeOrg extends Component
{
    protected function build()
    {
        echo BufferHelper::buffered(function () {
            ?>
            <script type="application/ld+json"><?php
                echo json_encode($this->getSchemeContent())
                ?></script><?php
        });
    }

    abstract protected function getSchemeContent();
}
