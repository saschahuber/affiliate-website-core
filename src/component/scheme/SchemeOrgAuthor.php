<?php

namespace saschahuber\affiliatewebsitecore\component\scheme;

use saschahuber\affiliatewebsitecore\component\scheme\AbstractSchemeOrg;

class SchemeOrgAuthor extends AbstractSchemeOrg
{
    private $name, $url, $image, $job_title, $company_name, $links;

    /**
     * @param $name
     * @param $url
     * @param $image
     * @param $job_title
     * @param $company_name
     * @param $links
     */
    public function __construct($name, $url, $image, $job_title, $company_name, $links, $id = null)
    {
        parent::__construct($id);
        $this->name = $name;
        $this->url = $url;
        $this->image = $image;
        $this->job_title = $job_title;
        $this->company_name = $company_name;
        $this->links = $links;
    }


    protected function getSchemeContent()
    {
        $scheme_content = [
            "@context" => "https://schema.org",
            "@type" => "Person",
            "name" => $this->getName()
        ];

        if($this->getUrl()){
            $scheme_content['url'] = $this->getUrl();
        }

        if($this->getImage()){
            $scheme_content['image'] = $this->getImage();
        }

        if($this->getJobTitle()){
            $scheme_content['jobTitle'] = $this->getJobTitle();
        }

        if($this->getCompanyName()){
            $scheme_content['worksFor'] = [
                "@type" => "Organization",
                "name" => $this->getCompanyName()
            ];
        }

        if($this->getLinks()){
            $links = [];
            foreach($this->getLinks() as $link){
                $links[] = $link->url;
            }
            $scheme_content['sameAs'] = $links;
        }

        return $scheme_content;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getJobTitle()
    {
        return $this->job_title;
    }

    /**
     * @param mixed $job_title
     */
    public function setJobTitle($job_title): void
    {
        $this->job_title = $job_title;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * @param mixed $company_name
     */
    public function setCompanyName($company_name): void
    {
        $this->company_name = $company_name;
    }

    /**
     * @return mixed
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param mixed $links
     */
    public function setLinks($links): void
    {
        $this->links = $links;
    }
}