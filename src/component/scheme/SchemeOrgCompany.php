<?php

namespace saschahuber\affiliatewebsitecore\component\scheme;

class SchemeOrgCompany extends AbstractSchemeOrg
{
    private $name, $description, $phone_number, $url, $opening_hours, $latitude, $longitude;

    public function __construct($name, $description, $phone_number, $url, $opening_hours, $latitude, $longitude, $id = null)
    {
        $this->setName($name);
        $this->setDescription($description);
        $this->setPhoneNumber($phone_number);
        $this->setUrl($url);
        $this->setOpeningHours($opening_hours);
        $this->setLatitude($latitude);
        $this->setLongitude($longitude);
        parent::__construct($id);
    }

    protected function getSchemeContent()
    {
        $scheme_data = [
            "@context" => "https://schema.org",
            "@type" => "Store",
            "name" => $this->getName(),
            "description" => $this->getDescription(),
            "telephone" => $this->getPhoneNumber(),
            "url" => $this->getUrl(),
        ];

        if ($this->getOpeningHours()) {
            $scheme_data['openingHoursSpecification'] = [];

            foreach ($this->getOpeningHours() as $weekday => $opening_hour_item) {
                if ($opening_hour_item === null || $opening_hour_item['is_open'] === 0) {
                    continue;
                }

                $scheme_data['openingHoursSpecification'][] = [
                    "@type" => "OpeningHoursSpecification",
                    "dayOfWeek" => [ucfirst($weekday)],
                    "opens" => substr($opening_hour_item['open1'], 0, 5),
                    "closes" => substr($opening_hour_item['close2'] ?: $opening_hour_item['close1'], 0, 5)
                ];
            }

            if (count($scheme_data['openingHoursSpecification']) < 1) {
                unset($scheme_data['openingHoursSpecification']);
            }
        }

        if ($this->getLatitude() && $this->getLongitude()) {
            $scheme_data['geo'] = [
                '@type' => 'GeoCoordinates',
                'latitude' => $this->getLatitude(),
                'longitude' => $this->getLongitude()
            ];
        }

        return $scheme_data;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param mixed $phone_number
     */
    public function setPhoneNumber($phone_number): void
    {
        $this->phone_number = $phone_number;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getOpeningHours()
    {
        return $this->opening_hours;
    }

    /**
     * @param mixed $opening_hours
     */
    public function setOpeningHours($opening_hours): void
    {
        $this->opening_hours = $opening_hours;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): void
    {
        $this->longitude = $longitude;
    }
}