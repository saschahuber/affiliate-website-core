<?php

namespace saschahuber\affiliatewebsitecore\component\scheme;

class SchemeOrgProduct extends AbstractSchemeOrg
{
    private $author, $title, $description, $image, $price, $avg_rating, $rating_count;

    public function __construct($author, $title, $description, $image, $price, $avg_rating, $rating_count, $id = null)
    {
        $this->setAuthor($author);
        $this->setTitle($title);
        $this->setDescription($description);
        $this->setImage($image);
        $this->setPrice($price);
        $this->setAvgRating($avg_rating);
        $this->setRatingCount($rating_count);

        parent::__construct($id);
    }

    protected function getSchemeContent()
    {
        $scheme_data = [
            "@context" => "https://schema.org",
            "@type" => "Product",
            #"description" => $this->getDescription(),
            "name" => $this->getTitle(),
            "image" => $this->getImage(),
            "offers" => [
                "@type" => "Offer",
                "availability" => "https://schema.org/InStock",
                "price" => $this->getPrice()?round($this->getPrice(), 2):0,
                "priceCurrency" => "EUR",
            ],
            #"review" => [
            #    [
            #        "@type"=> "Review",
            #        "author"=> "Ellie",
            #        "datePublished"=> "2011-04-01",
            #        "reviewBody"=> "The lamp burned out and now I have to replace it.",
            #        "name"=> "Not a happy camper",
            #        "reviewRating"=> [
            #            "@type"=> "Rating",
            #            "bestRating"=> "5",
            #            "ratingValue"=> "1",
            #            "worstRating"=> "1"
            #        ]
            #    ]
            #]
        ];

        if ($this->getRatingCount()) {
            $scheme_data["aggregateRating"] = [
                "@type" => "AggregateRating",
                "bestRating" => "10",
                "ratingValue" => floatval($this->getAvgRating()),
                "ratingCount" => $this->getRatingCount()
            ];
        }

        if ($this->getAuthor() !== null) {
            $scheme_data["author"] = [
                "@type" => "Person",
                "name" => $this->getAuthor()->title
            ];
        }


        return $scheme_data;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getAvgRating()
    {
        return $this->avg_rating;
    }

    /**
     * @param mixed $avg_rating
     */
    public function setAvgRating($avg_rating): void
    {
        $this->avg_rating = $avg_rating;
    }

    /**
     * @return mixed
     */
    public function getRatingCount()
    {
        return $this->rating_count;
    }

    /**
     * @param mixed $rating_count
     */
    public function setRatingCount($rating_count): void
    {
        $this->rating_count = $rating_count;
    }
}