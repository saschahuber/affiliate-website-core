<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;

class SocialPostsWriterButton extends JsButton {
    public function __construct($container_id, $app, $hashtags=false, $id=null){
        parent::__construct('<i class="fas fa-robot"></i> KI', null, $id);
        $this->container_id = $container_id;
        $this->app = $app;
        $this->hashtags = $hashtags;
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall(){
        return "generateAiSocialPost(this, '{$this->app}', ".($this->hashtags?'true':'false').", '{$this->container_id}')";
    }

    static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/SocialPostsWriterButton.js';
    }
}