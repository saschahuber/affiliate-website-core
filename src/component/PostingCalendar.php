<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Calendar;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class PostingCalendar extends Calendar {
    private $posts;

    function __construct($posts, $month, $year, $id = null){
        parent::__construct($month, $year, $id);
        $this->setPosts($posts);
    }

    protected function getItemsForDay($day_of_month){
        return $this->getPostsForDay($day_of_month, $this->getMonth(), $this->getYear(), $this->getPosts());
    }

    function getPostsForDay($day, $month, $year, $posts){
        $default_time = '15:00';
        $date = $year.'-'.($month<10?'0'.$month:$month).'-'.($day<10?'0'.$day:$day);

        $cur_date = date('Y-m-d');

        $is_previous_date = false;
        $is_today = false;

        if($date == $cur_date){
            $is_today = true;
        }
        elseif ($date < $cur_date){
            $is_previous_date = true;
        }

        $posts_for_day = [];
        if(array_key_exists($day, $posts)){
            $posts_for_day = $posts[$day];
            ksort($posts_for_day);
        }

        $targets = [
            'facebook_page' => ['icon' => 'fab fa-facebook', 'name' => 'Seite'],
            'facebook_group' => ['icon' => 'fab fa-facebook', 'name' => 'Gruppe'],
            'instagram' => ['icon' => 'fab fa-instagram', 'name' => null],
            'twitter' => ['icon' => 'fab fa-twitter', 'name' => null],
            'pinterest' => ['icon' => 'fab fa-pinterest', 'name' => null],
            'reddit' => ['icon' => 'fab fa-reddit', 'name' => null],
            'bluesky' => ['icon' => null, 'name' => 'Bluesky'],
            'linkedin' => ['icon' => 'fab fa-linkedin', 'name' => 'LinkedIn']
        ];

        ob_start();
        ?>
        <div>
            <p class="day_num"><strong><?=$day?></strong></p>
            <?php if(count($posts_for_day) > 0): ?>
                <ul>
                    <?php
                    foreach($posts_for_day as $timestamp => $post_list){
                        foreach($post_list as $post) {
                            echo $this->buildCalendarItem($post);
                        }
                    }
                    ?>
                </ul>
            <?php endif; ?>
            <?php if(!$is_previous_date): ?>
                <p style="text-align: center;">
                    <?php
                    foreach($targets as $target_key => $target_data){
                        $url = "/dashboard/postings/bearbeiten?app=$target_key&publish_time={$date}T{$default_time}";
                        (new LinkButton($url, $target_data['name'], $target_data['icon'], false, true))->display();
                    }
                    ?>
                </p>
            <?php endif; ?>
        </div>
        <?php
        return ob_get_clean();
    }

    private function buildCalendarItem($item){
        switch($item->calendar_type){
            case "social_post":
                return $this->buildSocialCalendarItem($item);
            case "blog_post":
                return $this->buildBlogCalendarItem($item);
            case "news_post":
                return $this->buildNewsCalendarItem($item);
            case "product_post":
                return $this->buildProductCalendarItem($item);
            case "link_page":
                return $this->buildLinkPageItem($item);
            case "push_notification":
                return $this->buildPushNotificationItem($item);
        }
        return null;
    }

    private function buildSocialCalendarItem($post){
        return BufferHelper::buffered(function() use ($post){
            $post_content = str_replace('"', '\'', $post->post_data);
            $post_content = str_replace('\/', '/', $post_content);
            ?>
            <li data-post_data="<?=$post_content?>">
                <?php

                $url = "/dashboard/postings/bearbeiten/{$post->id}";

                $date = DateHelper::displayDateTime($post->posting_time);

                $button_content = "[$post->target] {$post->title} ($date)";

                (new LinkButton($url, $button_content, 'fas fa-edit', false, true))->display();

                ?>
            </li>
            <?php
        });
    }

    private function buildLinkPageItem($post){
        return BufferHelper::buffered(function() use ($post){
            $post_content = str_replace('"', '\'', $post->post_data);
            $post_content = str_replace('\/', '/', $post_content);
            ?>
            <li data-post_data="<?=$post_content?>">
                <?php

                $url = "/dashboard/seiten/links/bearbeiten/{$post->id}";

                $date = DateHelper::displayDateTime($post->link_page_item_date);

                $button_content = "{$post->title} ($date)";

                (new LinkButton($url, $button_content, 'fas fa-link', false, true))->display();

                ?>
            </li>
            <?php
        });
    }

    private function buildPushNotificationItem($post){
        return BufferHelper::buffered(function() use ($post){
            $post_content = str_replace('"', '\'', $post->post_data);
            $post_content = str_replace('\/', '/', $post_content);
            ?>
            <li data-post_data="<?=$post_content?>">
                <?php

                $url = "/dashboard/push-notifications/bearbeiten/{$post->id}";

                $date = DateHelper::displayDateTime($post->scheduled_time);

                $button_content = "{$post->title} ($date)";

                (new LinkButton($url, $button_content, 'fas fa-bell', false, true))->display();

                ?>
            </li>
            <?php
        });
    }

    private function buildBlogCalendarItem($post){
        return BufferHelper::buffered(function() use ($post){
            global $CONFIG;
            $formatted_post_time = DateHelper::format($post->post_date, 'H:i') . ' Uhr: '
            ?>
            <li>
                <?php
                if($post->status === Manager::STATUS_PUBLISH){
                    (new LinkButton($CONFIG->website_domain . $post->full_permalink, $formatted_post_time . $post->title, 'fas fa-pen', false, true, ['full-width']))->display();
                }
                else{
                    (new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> <i class="fas fa-pen"></i> Vorschau' . $formatted_post_time . $post->title, 'ItemPreviewHandler', ['type' => PostManager::TYPE_POST, 'id' => $post->id]))->display();
                }
                ?>
            </li>
            <?php
        });
    }

    private function buildNewsCalendarItem($post){
        return BufferHelper::buffered(function() use ($post){
            global $CONFIG;
            $formatted_post_time = DateHelper::format($post->news_date, 'H:i') . ' Uhr: '
            ?>
            <li>
                <?php
                if($post->status === Manager::STATUS_PUBLISH){
                    (new LinkButton($CONFIG->website_domain . $post->full_permalink, $formatted_post_time . $post->title, 'fas fa-newspaper', false, true, ['full-width']))->display();
                }
                else{
                    (new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> <i class="fas fa-newspaper"></i> ' . $formatted_post_time . $post->title, 'ItemPreviewHandler', ['type' => NewsManager::TYPE_NEWS, 'id' => $post->id]))->display();
                }
                ?>
            </li>
            <?php
        });
    }

    private function buildProductCalendarItem($post){
        return BufferHelper::buffered(function() use ($post){
            global $CONFIG;
            $formatted_post_time = DateHelper::format($post->product_date, 'H:i') . ' Uhr: '
            ?>
            <li>
                <?php
                if($post->status === Manager::STATUS_PUBLISH){
                    (new LinkButton($CONFIG->website_domain . $post->full_permalink, $formatted_post_time . $post->title, 'fas fa-shopping-cart', false, true, ['full-width']))->display();
                }
                else{
                    (new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> <i class="fas fa-shopping-cart"></i> ' . $formatted_post_time . $post->title, 'ItemPreviewHandler', ['type' => ProductManager::TYPE_PRODUCT, 'id' => $post->id]))->display();
                }
                ?>
            </li>
            <?php
        });
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }
}