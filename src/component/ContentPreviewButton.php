<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;

class ContentPreviewButton extends JsButton
{
    private $item_url, $data_config;

    public function __construct($item_url, $data_config, $id = null)
    {
        parent::__construct('<i class="fas fa-eye"></i> Vorschau', null, $id);
        $this->item_url = $item_url;
        $this->data_config = $data_config;
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall(){
        return "prepareContentPreview('{$this->item_url}', '".base64_encode(json_encode($this->data_config))."')";
    }

    static function getJsFiles(){
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/ContentPreviewButton.js';
    }
}