<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;

class EditAdRulesComponent extends Component{
    public function __construct($ad, $id=null){
        parent::__construct($id);
        $this->ad = $ad;
    }

    protected function build(){
        $post_manager = new PostManager();
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $news_manager = new NewsManager();

        $taxonomy_mappings = [
            PostManager::TYPE_POST => [],
            ProductManager::TYPE_PRODUCT=> [],
            NewsManager::TYPE_NEWS => [],
        ];

        foreach($post_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[PostManager::TYPE_POST][$taxonomy->id] = $taxonomy;
        }

        foreach($product_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[ProductManager::TYPE_PRODUCT][$taxonomy->id] = $taxonomy;
        }

        foreach($news_manager->getTaxonomies() as $taxonomy){
            $taxonomy_mappings[NewsManager::TYPE_NEWS][$taxonomy->id] = $taxonomy;
        }

        $column_labels = [
            'Taxonomie-Typ',
            'Taxonomie',
            'Positiv-Keywords',
            'Negativ-Keywords',
            'Aktionen'
        ];

        $rows = [];

        foreach($this->ad->mappings as $mapping){
            $taxonomy_type = null;
            $taxonomy_title = null;
            $selectable_taxonomies = ['NULL' => 'Alle Taxonomien'];

            if(isset($taxonomy_mappings[$mapping->taxonomy_type])) {
                $taxonomy_type = $mapping->taxonomy_type;
            }

            if(isset($taxonomy_mappings[$mapping->taxonomy_type][$mapping->taxonomy_id])){
                $taxonomy = $taxonomy_mappings[$mapping->taxonomy_type][$mapping->taxonomy_id];
                $taxonomy_title = $taxonomy->title;
            }

            if(isset($taxonomy_mappings[$mapping->taxonomy_type])){
                foreach($taxonomy_mappings[$mapping->taxonomy_type] as $taxonomy){
                    $selectable_taxonomies["{$taxonomy->id}"] = $taxonomy->title;
                }
            }

            $cells = [
                new EditableSelect('ad__taxonomy_mapping', 'taxonomy_type', $mapping->id, $taxonomy_type?:" - ", [PostManager::TYPE_POST => 'Beitrag', ProductManager::TYPE_PRODUCT => 'Produkt', NewsManager::TYPE_NEWS => 'News-Artikel']),
                new EditableSelect('ad__taxonomy_mapping', 'taxonomy_id', $mapping->id, $taxonomy_title?:"Alle Taxonomien", $selectable_taxonomies),
                new EditableTextInput('ad__taxonomy_mapping', 'positive_keywords', $mapping->id, $mapping->positive_keywords),
                new EditableTextInput('ad__taxonomy_mapping', 'negative_keywords', $mapping->id, $mapping->negative_keywords),
                new DeleteButton('ad__taxonomy_mapping', $mapping->id, 2)
            ];
            $rows[] = new TableRow($cells);
        }

        $table = new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']);

        $input_config = [
            'taxonomy_type' => ['type' => [PostManager::TYPE_POST => 'Beitrag', ProductManager::TYPE_PRODUCT => 'Produkt', NewsManager::TYPE_NEWS => 'News-Artikel'], 'label' => 'In diesen Beitragstypen anzeigen'],
            'positive_keywords' => ['type' => 'text', 'label' => 'Bei diesen Keywords anzeigen (Immer zeigen, wenn leer)'],
            'negative_keywords' => ['type' => 'text', 'label' => 'Bei diesen Keywords NICHT anzeigen'],
            'ad_id' => ['type' => 'hidden', 'value' => $this->ad->id]
        ];
        $insert_entry_button = new InsertEntryButton('ad__taxonomy_mapping', $input_config, "Neue Regel für Anzeige");

        (new ElevatedCard([$table, new BreakComponent(), $insert_entry_button]))->display();
    }
}