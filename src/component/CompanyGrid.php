<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\component\item\CompanyGridItem;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\BufferHelper;

class CompanyGrid extends HTMLElement {

    private $columns;

    public function __construct($items, $columns=3, $id=null){
        $this->columns = $columns;
        parent::__construct($this->getGridContent($items));
    }

    protected function getGridContent($items){
        return BufferHelper::buffered(function () use ($items){
            $this->company_manager = new CompanyManager();
            ?>
            <div>
                <?php
                $item_list = [];
                foreach($items as $item) {
                    $item->permalink = $this->company_manager->generatePermalink($item);
                    $item_list[] = (new CompanyGridItem($item))->getContent();
                }

                (new GridContainer($item_list, $this->columns, GridContainer::SIZE_MD))->display();
                ?>
            </div>
            <?php
        });
    }
}