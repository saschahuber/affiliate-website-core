<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\GlobalPopup;
use saschahuber\saastemplatecore\component\JsButton;

class SponsoredBadge extends Component
{
    private $button_active;

    public function __construct($button_active = true, $id = null)
    {
        parent::__construct($id);
        $this->setButtonActive($button_active);
    }

    protected function build()
    {
        $onclick = null;

        if ($this->getButtonActive()) {
            $onclick = "displaySponsoredPostInfoPopup('" . GlobalPopup::GLOBAL_POPUP_ID . "')";
        }

        (new JsButton('<i class="fas fa-info-circle" aria-hidden="true"></i>&nbsp;Werbung',
            $onclick, ['sponsored-badge']))->display();
    }

    public static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/SponsoredBadge.js';
    }

    public static function getCssFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/css/SponsoredBadge.css';
    }

    /**
     * @return mixed
     */
    public function getButtonActive()
    {
        return $this->button_active;
    }

    /**
     * @param mixed $button_active
     */
    public function setButtonActive($button_active): void
    {
        $this->button_active = $button_active;
    }
}