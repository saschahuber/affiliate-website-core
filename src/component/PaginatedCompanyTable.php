<?php

namespace saschahuber\affiliatewebsitecore\component;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\service\CrmService;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\ConfirmButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

#[AllowDynamicProperties]
class PaginatedCompanyTable extends PaginatedItemTable {

    public function __construct($items, $offset=0, $items_per_page=25, $page_number=1, $total_item_count=null, $id=null){
        parent::__construct($items, $offset, $items_per_page, $page_number, $total_item_count, $id);

        $this->company_manager = new CompanyManager();
        $this->crm_service = new CrmService();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Thumbnail',
            'Titel',
            'Status',
            'Hervorgehoben',
            'Datum',
            'Paket',
            'Kategorien',
            'Index/Follow',
            'VG-Wort',
            'In Suche verstecken',
            'Aktionen',
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $company_actions = [];

        $company_category_buttons = [];
        foreach($item->taxonomies as $taxonomy){
            $button = new LinkButton("/dashboard/dienstleister/kategorien/bearbeiten/" . $taxonomy->id, $taxonomy->title, 'fas fa-pen', false, false);
            $company_category_buttons[] = $button;
        }

        if($item->status == Manager::STATUS_PUBLISH) {
            $company_actions[] = new LinkButton($CONFIG->website_domain . $this->company_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true);
        }
        $company_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => CompanyManager::TYPE_COMPANY, 'id' => $item->id]);
        $company_actions[] = new LinkButton("/dashboard/dienstleister/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        if($item->status == Manager::STATUS_DRAFT) {
            $confirm_button_id = uniqid();
            $javascript_code = BufferHelper::buffered(function() use ($item, $confirm_button_id){
                $parent_node_selector = "document.getElementById('component_{$confirm_button_id}').parentNode.parentNode.parentNode.parentNode.parentNode";

                ?>
                doApiCall('company/delete', {item_id: <?=$item->id?>}, function(responseCode, response){
                    /*
                    if (displayContainer) {
                        displayContainer.removeNode();
                    }
                    */
                    <?=$parent_node_selector?>.removeNode();
                });
                <?php
            });
            $company_actions[] = new ConfirmButton('<i class="fas fa-trash"></i> Löschen', "Möchtest du diesen Dienstleister unwiderruflich löschen?", $javascript_code, $confirm_button_id);
        }

        $company_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(CompanyManager::TYPE_COMPANY, $item->id), 'item_type' => CompanyManager::TYPE_COMPANY, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

        $company_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Interaktionen', 'CompanyActionLogLastIntervalAsyncHandler', ['height' => 350, 'company_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

        $crm_contact = $this->crm_service->getContactByLinkedElement(CompanyManager::TYPE_COMPANY, $item->id);
        if($crm_contact){
            $company_actions[] = new LinkButton("/dashboard/crm/contacts/edit/" . $crm_contact->id, 'CRM', 'fas fa-building', false, false);
        }
        else{
            $company_actions[] = new LinkButton("/dashboard/crm/contacts/edit?title={$item->title}&website={$item->website}&email={$item->email}&linked_element_type=" . CompanyManager::TYPE_COMPANY . "&linked_element_id=".intval($item->id), 'CRM', 'fas fa-plus-circle', false, true);
        }

        $cells = [
            new Image($this->company_manager->getThumbnailSrc($item), 200),
            $item->title,
            new EditableSelect('company', 'status', $item->id, $item->status, Manager::getStatusTypes()),
            new EditableToggle('company', 'is_featured', $item->id, $item->is_featured, true),
            new EditableDateTimePicker('company', 'company_date', $item->id, $item->company_date, true),
            new EditableSelect('company', 'package', $item->id, $item->package, CompanyManager::getPackages()),
            new FloatContainer($company_category_buttons),
            BufferHelper::buffered(function() use ($item){
                (new EditableToggle('company', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('company', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
            }),
            BufferHelper::buffered(function() use ($item){
                if($item->vg_wort_pixel_id) {
                    echo '<i class="fas fa-check-circle" style="color: green; font-size: 24px;"></i>';
                }
                else {
                    echo '<i class="fas fa-times-circle" style="color: darkred; font-size: 24px;"></i>';
                }
            }),
            new EditableToggle('company', 'hide_in_search', $item->id, $item->hide_in_search, true, "Verstecken"),
            new FloatContainer($company_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/dienstleister/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}