<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;

class PaginatedAuthorTable extends PaginatedItemTable {

    public function __construct($items, $offset=0, $items_per_page=25, $page_number=1, $id=null){
        parent::__construct($items, $offset, $items_per_page, $page_number, $id);
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Bild',
            'Title',
            'Aktionen'
        ];
        return $column_labels;
    }

    protected function getRow($item){
        $image_manager = new ImageManager();

        $post_actions = [];

        $post_actions[] = new LinkButton("/dashboard/system/autoren/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        $cells = [
            new Image($image_manager->getAttachmentUrl($item->attachment_id), 150, ['author-image']),
            $item->title,
            BufferHelper::buffered(function () use ($item){
                ?>
                <p style="max-width: 600px; text-align: center;">
                    <?=$item->description?>
                </p>
                <?php
            }),
            new FloatContainer($post_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/system/autoren/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}