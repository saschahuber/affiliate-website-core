<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;

class CompanyDescriptionWriterButton extends JsButton {
    public function __construct($company_name_input_id, $address_input_id, $mail_input_id, $phone_input_id, $website_input_id, $categories, $company_id, $container_id, $model=ChatGenerationTemplate::MODEL_CHAT_GPT, $tokens=8192, $id=null){
        parent::__construct('<i class="fas fa-robot"></i> Unternehmensbeschreibung mit KI schreiben ('.$model.')', null, $id);
        $this->company_name_input_id = $company_name_input_id;
        $this->address_input_id = $address_input_id;
        $this->mail_input_id = $mail_input_id;
        $this->phone_input_id = $phone_input_id;
        $this->website_input_id = $website_input_id;
        $this->categories = $categories;
        $this->company_id = $company_id;
        $this->container_id = $container_id;
        $this->model = $model;
        $this->tokens = $tokens;
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall(){
        return "generateAiCompanyDescription(this, '{$this->company_name_input_id}', '{$this->address_input_id}', 
        '{$this->mail_input_id}', '{$this->phone_input_id}', '{$this->website_input_id}', '{$this->categories}', 
        '{$this->company_id}', '{$this->container_id}', '{$this->model}', {$this->tokens});";
    }

    static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/CompanyDescriptionWriterButton.js';
    }
}