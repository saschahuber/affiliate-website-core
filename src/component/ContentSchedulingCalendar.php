<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Calendar;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class ContentSchedulingCalendar extends Calendar {
    private $posts;

    function __construct($posts, $month, $year, $id = null){
        parent::__construct($month, $year, $id);
        $this->setPosts($posts);
    }

    protected function getItemsForDay($day_of_month){
        return $this->getPostsForDay($day_of_month, $this->getMonth(), $this->getYear(), $this->getPosts());
    }

    function getPostsForDay($day, $month, $year, $posts){
        $date = $year.'-'.($month<10?'0'.$month:$month).'-'.($day<10?'0'.$day:$day);

        $cur_date = date('Y-m-d');

        $is_previous_date = false;
        $is_today = false;

        if($date == $cur_date){
            $is_today = true;
        }
        elseif ($date < $cur_date){
            $is_previous_date = true;
        }

        $posts_for_day = [];
        foreach($posts as $post){
            if($post->scheduled_day === $day){
                $posts_for_day[] = $post;
            }
        }

        ob_start();
        ?>
        <div>
            <p class="day_num <?=($is_today?'is_today':'')?>"><strong><?=$day?></strong></p>
            <div
            <?php if(!$is_previous_date): ?>
                class="drop-zone item_list" data-year="<?=$year?>" data-month="<?=$month?>" data-day="<?=$day?>" ondrop="drop(this, event)" ondragover="allowDrop(this, event)" ondragenter="onDragEnter(this, event)" ondragleave="onDragLeave(this, event)"
            <?php else: ?>
                class="dead-drop-zone"
            <?php endif; ?>
            >
                <?php if(count($posts_for_day) > 0): ?>
                    <?php
                    foreach($posts_for_day as $post) {
                        echo self::buildCalendarItem($post);
                    }
                    ?>
                <?php endif; ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    public static function buildCalendarItem($post){
        return BufferHelper::buffered(function() use ($post){
            ?>
            <div id="<?=uniqid()?>" draggable="true" ondragstart="drag(this, event)" data-item-id="<?=$post->content_schedule_item_id?>">
                <?php (new GlobalAsyncComponentButton($post->title, 'AddEditContentScheduleItemHandler', ['content_schedule_item_id' => $post->content_schedule_item_id]))->display(); ?>
            </div>
            <?php
        });
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }
}