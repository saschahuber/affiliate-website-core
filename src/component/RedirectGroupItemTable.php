<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;

class RedirectGroupItemTable extends Component
{
    private $item, $items;

    public function __construct($item, $items, $id = null)
    {
        parent::__construct($id);
        $this->setItem($item);
        $this->setItems($items);
    }

    protected function build()
    {
        $redirect_group = $this->getItem();

        $column_labels = ['Titel', 'Ziel-URL', 'Aktiv?', 'Aufrufe', 'Aktionen'];

        $rows = [];
        foreach ($this->getItems() as $item) {
            $cells = [
                $item->title,
                $item->target_url,
                new EditableToggle('redirect__group_item', 'is_active', $item->id, $item->is_active, true),
                $item->hits,
                new FloatContainer([
                    new LinkButton("/dashboard/system/random-redirects/ziel-bearbeiten/{$redirect_group->id}/{$item->id}", 'Item bearbeiten', 'fas fa-edit', false, true)
                ])
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedCard([
            new Text('Redirect-Gruppen-Ziele', 'label'),
            new BreakComponent(),
            new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']),
            new BreakComponent(),
            new FloatContainer([
                new LinkButton("/dashboard/system/random-redirects/ziel-bearbeiten/{$redirect_group->id}", "Ziel hinzufügen", 'fas fa-circle-plus', false, true, ['centered'])
            ])
        ]))->display();
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $item
     */
    public function setItems($items): void
    {
        $this->items = $items;
    }
}