<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;

class ToolbarButton extends GlobalAsyncComponentButton
{
    public function __construct($classes = [], $custom_text = "Tools")
    {
        parent::__construct('<i class="fas fa-toolbox"></i> ' . $custom_text, "ToolbarAsyncHandler", [], null, $classes);
    }
}