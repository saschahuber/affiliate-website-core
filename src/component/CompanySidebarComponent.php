<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\helper\BufferHelper;

#[AllowDynamicProperties]
class CompanySidebarComponent extends Component{
    private $company_manager, $company;

    public function __construct($company_manager, $company, $id=null){
        $this->setCompanyManager($company_manager);
        $this->setCompany($company);
        parent::__construct($id);
    }

    protected function build(){
        $company_manager = $this->getCompanyManager();
        $company = $this->getCompany();

        ?>
        <h3>Mehr Informationen</h3>

        <?php
        if($company->phone_number || $company->email || $company->website || $company->address){
            $contact_info = BufferHelper::buffered(function() use($company){
                ?>
                <h5>Kontaktinfos</h5>
                <?php if($company->phone_number): ?>
                    <p><span><i class="fas fa-phone"></i> <?=CompanyManager::getClickableAndTrackablePhoneNumber($company)?></span></p>
                <?php endif; ?>
                <?php if($company->email): ?>
                    <p><span><i class="fas fa-envelope"></i> <?=CompanyManager::getClickableAndTrackableEmail($company)?></span></p>
                <?php endif; ?>
                <?php if($company->website): ?>
                    <p><span><i class="fas fa-globe"></i> <?=CompanyManager::getClickableAndTrackableWebsite($company)?></span></p>
                <?php endif; ?>
                <?php if($company->address): ?>
                    <p><span><i class="fas fa-map-marker"></i> <?=$company->address?></span></p>
                <?php endif; ?>
                <?php
            });

            echo $contact_info;
        }
        ?>

        <?php
        if($company->show_opening_hours){
            $opening_hours_content = BufferHelper::buffered(function() use ($company_manager, $company){
                ?>
                <h5>Öffnungszeiten</h5>
                <ul>
                    <?php foreach($company_manager->getFormattedOpeningHours($company) as $day_opening_hours): ?>
                        <li><?=$day_opening_hours['weekday']?>: <?=$day_opening_hours['text']?></li>
                    <?php endforeach; ?>
                </ul>
                <?php
            });

            echo $opening_hours_content;
        }
        ?>

        <?php
        if(count($this->company->taxonomies)){
            $taxonomy_content = BufferHelper::buffered(function() use ($company_manager, $company){
                ?>
                <h5>Kategorien</h5>
                <p><span><i class="far fa-folder"></i> <?=$company_manager->getTaxonomyLinks($company)?></span></p>
                <?php
            });

            echo $taxonomy_content;
        }
    }

    /**
     * @return mixed
     */
    public function getCompanyManager()
    {
        return $this->company_manager;
    }

    /**
     * @param mixed $company_manager
     */
    public function setCompanyManager($company_manager): void
    {
        $this->company_manager = $company_manager;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
    }


}