<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\service\ScheduledToastService;
use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\component\ToastComponent;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DateHelper;

class PaginatedUserTable extends PaginatedItemTable {

    public function __construct($items, $offset=0, $items_per_page=25, $toast_number=1, $id=null){
        parent::__construct($items, $offset, $items_per_page, $toast_number, $id);

        $this->toast_service = new ScheduledToastService();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'E-Mail',
            'Name',
            'Vorname',
            'Nachname',
            'Registriert',
            'Zuletzt gesehen',
            'Bestätigt?',
            'E-Mail valide?',
            'AGB akzeptiert?',
            'Rollen',
            'Aktionen'
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $toast_actions = [];

        $toast_actions[] = new LinkButton("/dashboard/system/users/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        #$toast_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(PageManager::TYPE_PAGE, $item->id), 'item_type' => PageManager::TYPE_PAGE, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

        $cells = [
            $item->email,
            $item->name,
            $item->first_name,
            $item->last_name,
            DateHelper::displayDateTime($item->registration_date),
            DateHelper::displayDateTime($item->date_last_seen),
            new Toggle('confirmed', null, $item->is_confirmed, true, true),
            new EditableToggle('user', 'email_valid', $item->user_id, $item->email_valid, true),
            new Toggle('agb_accepted', null, $item->agb_accepted, true, true),
            json_encode($item->roles),
            new FloatContainer($toast_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/system/users/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}