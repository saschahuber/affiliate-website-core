<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class PaginatedBlogPostTable extends PaginatedItemTable {

    public function __construct($items, $offset=0, $items_per_page=25, $page_number=1, $total_item_count=null,  $id=null){
        parent::__construct($items, $offset, $items_per_page, $page_number, $total_item_count, $id);

        $this->post_manager = new PostManager();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Thumbnail',
            'Titel',
            'Status',
            'Datum',
            'Kategorien',
            'Index/Follow',
            'In Suche verstecken',
            'In Newsletter verstecken',
            'Gesponsert',
            'VG-Wort',
            'Meta',
            'Aktionen',
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $post_actions = [];

        $post_category_buttons = [];
        foreach($item->taxonomies as $taxonomy){
            $button = new LinkButton("/dashboard/blog/kategorien/bearbeiten/" . $taxonomy->id, $taxonomy->title, 'fas fa-pen', false, false);
            $post_category_buttons[] = $button;
        }

        if($item->status == Manager::STATUS_PUBLISH) {
            $post_actions[] = new LinkButton($CONFIG->website_domain . $this->post_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true);
        }
        $post_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => PostManager::TYPE_POST, 'id' => $item->id]);
        $post_actions[] = new LinkButton("/dashboard/blog/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        $post_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(PostManager::TYPE_POST, $item->id), 'item_type' => PostManager::TYPE_POST, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

        $cells = [
            new Image($this->post_manager->getThumbnailSrc($item), 200),
            $item->title,
            new EditableSelect('post', 'status', $item->id, $item->status, Manager::getStatusTypes()),
            new EditableDateTimePicker('post', 'post_date', $item->id, $item->post_date),
            new FloatContainer($post_category_buttons),
            BufferHelper::buffered(function() use ($item){
                (new EditableToggle('post', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('post', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
            }),
            new EditableToggle('post', 'hide_in_search', $item->id, $item->hide_in_search, true, "Verstecken"),
            new EditableToggle('post', 'hide_in_newsletter', $item->id, $item->hide_in_newsletter, true, "Verstecken"),
            new EditableToggle('post', 'is_sponsored', $item->id, $item->is_sponsored, true, "Gesponsert"),
            BufferHelper::buffered(function() use ($item){
                if($item->vg_wort_pixel_id) {
                    echo '<i class="fas fa-check-circle" style="color: green; font-size: 24px;"></i>';
                }
                else {
                    echo '<i class="fas fa-times-circle" style="color: darkred; font-size: 24px;"></i>';
                }
            }),
            BufferHelper::buffered(function() use ($item){
                (new Toggle('has_meta_title', "Titel", isset($item->meta_title) && strlen($item->meta_title), true, true))->display();
                (new BreakComponent())->display();
                (new Toggle('has_meta_description', "Description", isset($item->meta_description) && strlen($item->meta_description), true, true))->display();
            }),
            new FloatContainer($post_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/blog/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}