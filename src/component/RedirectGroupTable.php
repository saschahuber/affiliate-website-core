<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class RedirectGroupTable extends ItemTable
{

    public function __construct($items, $id = null)
    {
        parent::__construct($items, $id);
    }

    protected function getColumnLabels()
    {
        $column_labels = [
            'Titel',
            'Alias',
            'Aktiv',
            'Aufrufe',
            'Aktionen'
        ];
        return $column_labels;
    }

    protected function getRow($item)
    {
        global $CONFIG;

        $post_actions = [];

        $post_actions[] = new LinkButton("/dashboard/system/random-redirects/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        $cells = [
            $item->title,
            $item->alias,
            new EditableToggle('redirect__group', 'is_active', $item->id, $item->is_active, true),
            $item->hits,
            new FloatContainer($post_actions)
        ];
        return new TableRow($cells);
    }
}