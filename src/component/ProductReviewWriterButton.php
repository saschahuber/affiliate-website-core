<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;

class ProductReviewWriterButton extends JsButton
{
    public function __construct($product_id, $title_input_id, $content_container_id, $pro_contra_container_id, $id = null)
    {
        parent::__construct('<i class="fas fa-robot"></i> Testbericht mit KI schreiben', null, $id);
        $this->product_id = $product_id;
        $this->title_input_id = $title_input_id;
        $this->content_container_id = $content_container_id;
        $this->pro_contra_container_id = $pro_contra_container_id;
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall()
    {
        return "generateAiProductReview(this, {$this->product_id}, '{$this->title_input_id}', '{$this->content_container_id}', '{$this->pro_contra_container_id}')";
    }

    static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/ProductReviewWriterButton.js';
    }
}