<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class PaginatedRedirectTable extends PaginatedItemTable {

    public function __construct($items, $offset=0, $items_per_page=25, $page_number=1, $id=null){
        parent::__construct($items, $offset, $items_per_page, $page_number, $id);
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Von-URL',
            'Ziel-URL',
            'Aktiv',
            'Aktionen'
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $post_actions = [];

        $post_actions[] = new LinkButton("/dashboard/system/redirects/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);

        $cells = [
            new LinkButton($CONFIG->website_domain . $item->from_url, $item->from_url),
            new LinkButton($CONFIG->website_domain . $item->to_url, $item->to_url),
            new EditableToggle('redirect', 'is_active', $item->id, $item->is_active, true),
            new FloatContainer($post_actions)
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/system/redirects/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}