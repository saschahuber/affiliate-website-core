<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;

class BlogWriterButton extends JsButton
{
    public function __construct($title_input_id, $container_id, $id = null)
    {
        parent::__construct('<i class="fas fa-robot"></i> Artikel mit KI schreiben', null, $id);
        $this->title_input_id = $title_input_id;
        $this->container_id = $container_id;
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall()
    {
        return "generateAiBlogPost(this, '{$this->title_input_id}', '{$this->container_id}')";
    }

    static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/BlogWriterButton.js';
    }
}