<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class NewsImporterPreviewTable extends ItemTable {

    public function __construct($items, $id=null){
        parent::__construct($items, $id);

        $this->news_manager = new NewsManager();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Thumbnail',
            'Titel',
            'Aktionen',
        ];
        return $column_labels;
    }

    protected function getRow($item){
        global $CONFIG;

        $post_actions = [];


        if(isset($item->imported_url)) {
            $post_actions[] = new LinkButton($CONFIG->website_domain . $item->imported_url, 'Originalen Artikel Ansehen', 'fas fa-eye', false, true);
        }

        if($item->imported_id) {
            $post_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Importierten Artikel Ansehen', 'ItemPreviewHandler', ['type' => NewsManager::TYPE_NEWS, 'id' => $item->imported_id]);
            $post_actions[] = new LinkButton("/dashboard/news/bearbeiten/" . $item->imported_id, 'Bearbeiten', 'fas fa-pen', false, false);
            $image = new Image($this->news_manager->getThumbnailSrc($item), 200);
        }
        else{
            $post_actions[] = new LinkButton("/dashboard/news/bearbeiten/" . $item->imported_id, 'Importieren', 'fas fa-pen', false, false);
            $post_actions[] = new LinkButton($item->post_url, 'Ansehen', 'fas fa-eye', false, true);
            $image = null;
        }

        $cells = [
            $image,
            $item->title,
            new FloatContainer($post_actions)
        ];
        return new TableRow($cells);
    }
}