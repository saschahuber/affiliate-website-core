<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AuthorLinkTable extends Component
{
    private $author;

    public function __construct($author, $id = null)
    {
        parent::__construct($id);
        $this->setAuthor($author);
    }

    protected function build()
    {
        $author = $this->getAuthor();

        $column_labels = [
            'Label',
            'Url',
            'Aktionen'
        ];

        $rows = [];
        foreach ($author->links as $link) {
            $cells = [
                new EditableTextInput('author__link', 'label', $link->id, $link->label),
                new EditableTextInput('author__link', 'url', $link->id, $link->url),
                new DeleteButton("author__link", $link->id, 2)
            ];
            $rows[] = new TableRow($cells);
        }

        $input_config = [
            'author_id' => ['type' => 'hidden', 'value' => $author->id],
            'label' => ['type' => 'text', 'label' => 'Label'],
            'url' => ['type' => 'text', 'label' => 'Link-URL']
        ];
        $add_link_button = new InsertEntryButton('author__link', $input_config, 'Neuer Link');

        $table = new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']);

        (new Card([
            $add_link_button, $table
        ]))->display();
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }
}