<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\FormGroup;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\BufferHelper;

class IndexFollowSettingsContainer extends Component{
    private $item;

    public function __construct($item, $id=null){
        $this->setItem($item);
        parent::__construct($id);
    }

    protected function build(){
        $item = $this->getItem();

        echo BufferHelper::buffered(function () use ($item){
            ?>
            <div>
                <label>Suchmaschinen-Indexierung</label>

                <br>

                <?php

                (new Toggle("doindex", "Index/Noindex", isset($item->doindex)?$item->doindex:false, false, true))->display();

                (new Toggle("dofollow", "Dofollow/Nofollow", isset($item->dofollow)?$item->dofollow:false, false, true))->display();

                ?>
            </div>
            <?php
        });
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }


}