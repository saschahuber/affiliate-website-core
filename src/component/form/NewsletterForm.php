<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\affiliatewebsitecore\service\NewsletterService;
use saschahuber\saastemplatecore\component\Component;

class NewsletterForm extends Component{
    private $hide_topic_selection, $style, $analytics_suffix;

    const STYLE_LIGHT = "light";
    const STYLE_DARK = "dark";

    public function __construct($hide_topic_selection=false, $style=self::STYLE_DARK, $analytics_suffix = null, $id = null){
        parent::__construct($id);
        $this->setHideTopicSelection($hide_topic_selection);
        $this->setStyle($style);
        $this->setAnalyticsSuffix($analytics_suffix);
    }

    protected function build()
    {
        ?>
            <script>
                function subscribeToNewsletter_<?=$this->getId()?>(id, event){

                    let newsletterFormContainer = document.getElementById(id);

                    let name = null;
                    let nameInput = newsletterFormContainer.querySelector('input[name="name"]');
                    if(nameInput) {
                        name = nameInput.value;
                    }

                    let email = newsletterFormContainer.querySelector('input[name="email"]').value;

                    let acceptedTerms = newsletterFormContainer.querySelector('input[name="terms"]').checked;

                    let allTopics = newsletterFormContainer.querySelector('input[name="all_topics"]');

                    let allTopicsChecked = allTopics && allTopics.value === "1";

                    let topicList = JSON.parse('<?=json_encode(array_keys(NewsletterService::ALL_NEWSLETTER_TOPICS))?>');

                    let selectedTopics = [];

                    topicList.forEach((topic) => {
                        let topicInput = newsletterFormContainer.querySelector('input[name="'+topic+'"]');

                        if(topicInput && topicInput.checked){
                            selectedTopics.push(topic);
                        }
                    });

                    if(!email || !acceptedTerms){
                        return false;
                    }

                    event.preventDefault();

                    let params = {
                        email: email,
                        topics: selectedTopics,
                        all_topics: allTopicsChecked
                    }

                    if(name){
                        params.name = name;
                    }

                    let ignoreBouncerLink = document.getElementById('ignore-bouncer');
                    if(ignoreBouncerLink){
                        ignoreBouncerLink.style.display = 'none';
                    }


                    newsletterFormContainer.innerHTML = '<br><p class="centered"><i class="fas fa-spinner fa-spin"></i> Anmeldung läuft...</p>';

                    doPublicApiCall('newsletter/subscribe', params, function (response) {
                        let responseData = JSON.parse(response);

                        if(responseData.error) {
                            newsletterFormContainer.innerHTML = '<br><p class="centered"><i class="fas fa-times-circle"></i> Es gab einen Fehler bei der Anmeldung zum Newsletter. Bitte versuche es später erneut.</p>';
                        }
                        else {
                            newsletterFormContainer.innerHTML = '<br><p class="centered"><i class="fas fa-check-circle"></i> Du hast dich erfolgreich zum Newsletter angemeldet. Bitte klicke auf den Bestätigungslink in der E-Mail, die dir gesendet wurde.</p>';

                            //gaEvent('Newsletter-Anmeldung');
                            saveAnalyticsEvent('newsletter_subscription<?=($this->getAnalyticsSuffix()?'_'.$this->getAnalyticsSuffix():'')?>', params);
                        }
                    });


                    return false;
                }
            </script>

        <div id="<?=$this->getId()?>" class="newsletter-form">
            <div>
                <div style="text-align: center;">
                    <form onsubmit="subscribeToNewsletter_<?=$this->getId()?>('<?=$this->getId()?>', event);">
                        <input name="action" type="hidden" value="register">
                        <div>
                            <?php if($this->getHideTopicSelection()=== false): ?>
                                <p>Zu welchen Themen möchtest du Updates erhalten?</p>
                                <p>
                                    <?php foreach(NewsletterService::ALL_NEWSLETTER_TOPICS as $topic => $title): ?>
                                        <label class="topic-selection">
                                            <input style="vertical-align: middle;" name="<?=$topic?>" type="checkbox" checked value="1">&nbsp;<?=$title?>
                                        </label>
                                    <?php endforeach; ?>
                                </p>
                            <?php else: ?>
                                <input name="all_topics" type="hidden" value="1">
                            <?php endif; ?>
                            <div class="row newsletter-form-inputs">
                                <!--
                                <div class="col-md-3">
                                    <input id="newsletter-name-input"
                                           type="text" name="name" placeholder="Dein Name (Optional)">
                                </div>
                                -->
                                <div class="col-md-8 newsletter-mail-input-container">
                                    <input id="newsletter-mail-input"
                                           type="email" name="email" required="" placeholder="Deine E-Mail Adresse">
                                </div>
                                <div class="col-md-4 newsletter-mail-button-container">
                                    <input class="btn btn-primary" style="width: 100%; height: 52px; border: none;"
                                           type="submit" onclick="saveAnalyticsEvent('newsletter_subscription', {email: document.getElementById('newsletter-email-input').value})" value="Jetzt anmelden">
                                </div>
                            </div>
                            <br>
                            <p><label class="<?=$this->getStyle()?>">
                                    <input style="vertical-align: middle;" name="terms" type="checkbox" value="1"
                                           required=""> Ich bin mit der Speicherung meiner E-Mail-Adresse zum Versand des
                                    Newsletters und der Verarbeitung meiner Daten - unter anderem zur Erfolgsmessung durch
                                    Klickraten - einverstanden. Weitere Informationen
                                    in der <a href="/legal/datenschutz/" target="_blank">Datenschutzerklärung</a>.
                                </label>
                            </p>
                        </div>
                        <label style="display: none !important;">Wenn du ein Mensch bist, lasse das Feld leer: <input
                                    type="text" name="parity" value="" tabindex="-1" autocomplete="off">
                        </label>
                        <div></div>
                    </form>
                </div>
            </div>
        </div>
        <?php
    }

    /**
     * @return mixed
     */
    public function getHideTopicSelection()
    {
        return $this->hide_topic_selection;
    }

    /**
     * @param mixed $hide_topic_selection
     */
    public function setHideTopicSelection($hide_topic_selection): void
    {
        $this->hide_topic_selection = $hide_topic_selection;
    }

    /**
     * @return mixed
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @param mixed $style
     */
    public function setStyle($style): void
    {
        $this->style = $style;
    }

    /**
     * @return mixed
     */
    public function getAnalyticsSuffix()
    {
        return $this->analytics_suffix;
    }

    /**
     * @param mixed $analytics_suffix
     */
    public function setAnalyticsSuffix($analytics_suffix): void
    {
        $this->analytics_suffix = $analytics_suffix;
    }
}