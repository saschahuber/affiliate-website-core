<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;

class CompanyOpeningHoursEditor extends Component{
    private $item;

    public function __construct($item, $id=null){
        $this->setItem($item);
        parent::__construct($id);
    }

    protected function build(){
        $item = $this->getItem();

        $company_manager = new CompanyManager();

        $opening_hours = $company_manager->getOpeningHours($item);

        $card_content = BufferHelper::buffered(function () use ($item, $opening_hours){
            ?>
            <style>
                .opening_hours{
                    overflow-x: scroll;
                }

                .opening_hours table,
                .opening_hours table input{
                    width: 100%;
                }
            </style>

            <div class="opening_hours">
                <strong>Öffnungszeiten</strong>

                <?php
                (new Toggle('show_opening_hours', 'Öffnungszeiten anzeigen', isset($item->show_opening_hours)?$item->show_opening_hours:false, false, true))->display();
                ?>

                <table>
                    <tr>
                        <th>Wochentag</th>
                        <th>Geöffnet?</th>
                        <th>Öffnung 1</th>
                        <th>Schließung 1</th>
                        <th>Öffnung 2</th>
                        <th>Schließung 2</th>
                    </tr>
                    <?php

                    $day_items = [
                        'monday' => ['title' => 'Montag'],
                        'tuesday' => ['title' => 'Dienstag'],
                        'wednesday' => ['title' => 'Mittwoch'],
                        'thursday' => ['title' => 'Donnerstag'],
                        'friday' => ['title' => 'Freitag'],
                        'saturday' => ['title' => 'Samstag'],
                        'sunday' => ['title' => 'Sonntag']
                    ];

                    foreach($day_items as $weekday => $day_item):
                        $day_opening_hours = $opening_hours[$weekday];
                        $open1 = ArrayHelper::getArrayValue($day_opening_hours, 'open1', '');
                        $close1 = ArrayHelper::getArrayValue($day_opening_hours, 'close1', '');
                        $open2 = ArrayHelper::getArrayValue($day_opening_hours, 'open2', '');
                        $close2 = ArrayHelper::getArrayValue($day_opening_hours, 'close2', '');
                        $is_open = ArrayHelper::getArrayValue($day_opening_hours, 'is_open', false);

                        ?>
                        <tr>
                            <td><?=$day_item['title']?></td>
                            <td>
                                <?php
                                (new Toggle($weekday.'_is_open', null, $is_open, false, true))->display();
                                ?>
                            </td>
                            <td>
                                <input type="time" name="<?=$weekday?>_open1" value="<?=$open1?>">
                            </td>
                            <td>
                                <input type="time" name="<?=$weekday?>_close1" value="<?=$close1?>">
                            </td>
                            <td>
                                <input type="time" name="<?=$weekday?>_open2" value="<?=$open2?>">
                            </td>
                            <td>
                                <input type="time" name="<?=$weekday?>_close2" value="<?=$close2?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <?php
        });

        (new Card($card_content))->display();
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }


}