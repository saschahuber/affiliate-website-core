<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableNumberInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;

class ProductLinkTable extends Component{
    private $item, $product_links, $allow_editing;

    public function __construct($item, $product_links, $allow_editing=true, $id = null){
        parent::__construct($id);
        $this->setItem($item);
        $this->setProductLinks($product_links);
        $this->setAllowEditing($allow_editing);
    }

    protected function build(){
        $product = $this->getItem();

        $column_labels = ['Reihenfolge', 'Shop', 'Feed', 'Link', 'Button-Text', 'Preis', 'Reduzierter Preis', 'Letztes Update', 'Verfügbar', 'Aktiv', 'Aktionen'];

        $rows = [];
        foreach ($this->getProductLinks() as $product_link){
            if($product_link->button_text) {
                $button_text = $product_link->button_text;
            }
            else if($product_link->product_shop_title) {
                $button_text = "Zu {$product_link->product_shop_title}";
            }
            else{
                $button_text = "Produkt ansehen";
            }

            $actions = [];

            if($this->getAllowEditing()){
                $actions[] = new LinkButton("/dashboard/produkte/link-bearbeiten/{$product->id}/{$product_link->id}", 'Link bearbeiten', 'fas fa-edit', false, true);
            }

            if(!$product_link->product__shop_id && !$product_link->product__feed_id){
                $actions[] = new DeleteButton('product__link', $product_link->id, 5);
            }

            $cells = [
                new EditableNumberInput('product__link', 'sort_order', $product_link->id, $product_link->sort_order?:0),
                $product_link->product_shop_title,
                $product_link->product_feed_title,
                new LinkButton($product_link->url, $button_text, false, false, true),
                $product_link->button_text,
                $product_link->price,
                $product_link->reduced_price,
                $product_link->last_update,
                new Toggle('is_available', null, $product_link->is_available, true, true),
                new EditableToggle('product__link', 'is_active', $product_link->id, $product_link->is_active, true, null),
                new FloatContainer($actions)
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedCard([
            new Text('Produkt-Links', 'label'),
            new BreakComponent(),
            new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']),
            new BreakComponent(),
            new FloatContainer([
                $this->getAllowEditing()?new LinkButton("/dashboard/produkte/link-bearbeiten/{$product->id}", "Link hinzufügen", 'fas fa-circle-plus', false, true, ['centered']):null
            ])
        ]))->display();
    }

    /**
     * @return mixed
     */
    public function getAllowEditing()
    {
        return $this->allow_editing;
    }

    /**
     * @param mixed $allow_editing
     */
    public function setAllowEditing($allow_editing): void
    {
        $this->allow_editing = $allow_editing;
    }



    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getProductLinks()
    {
        return $this->product_links;
    }

    /**
     * @param mixed $product_links
     */
    public function setProductLinks($product_links): void
    {
        $this->product_links = $product_links;
    }
}