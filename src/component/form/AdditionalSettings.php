<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\affiliatewebsitecore\component\form\input\AttachedFaqSelect;
use saschahuber\affiliatewebsitecore\component\form\input\AttachmentSelector;
use saschahuber\affiliatewebsitecore\component\form\input\DynamicImageSelector;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AdditionalSettings extends Component{
    private $item;

    public function __construct($item, $id=null){
        $this->setItem($item);
        parent::__construct($id);
    }

    protected function build(){
        $item = $this->getItem();

        echo BufferHelper::buffered(function() use ($item){
            ?>
            <div class="settings">
                <strong>Weitere Einstellungen</strong>

                <?php

                (new MetaOgBoxes($item))->display();

                (new DynamicImageSelector(isset($item->dynamic_image_type)?$item->dynamic_image_type:null, isset($item->dynamic_image_id)?$item->dynamic_image_id:null, 'dynamic_image_type', 'dynamic_image_id', 'Dynamisches Bild'))->display();

                (new Row([
                    new Column([
                        new AttachmentSelector($item?$item->attachment_id:null)
                    ], ["col-12", "col-md-6"]),
                    new Column([
                        new Card([
                            new AttachedFaqSelect($item?$item->attached_faq_id:null),
                            new BreakComponent(),
                            new IndexFollowSettingsContainer($item)
                        ])
                    ], ["col-12", "col-md-6"])
                ]))->display();
                ?>
            </div>
            <?php
        });
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }


}