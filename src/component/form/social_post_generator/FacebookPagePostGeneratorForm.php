<?php

namespace saschahuber\affiliatewebsitecore\component\form\social_post_generator;

use saschahuber\affiliatewebsitecore\component\form\social_post_generator\AbstractPostGeneratorForm;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;

class FacebookPagePostGeneratorForm extends AbstractPostGeneratorForm
{
    public function __construct($post = null, $id = null)
    {
        parent::__construct($post, SocialPostingFeedItemManager::TARGET_FACEBOOK_PAGE, $id);
        $this->setPost($post);
    }

    protected function buildFormInputs($post, $target)
    {
        return BufferHelper::buffered(function () use ($post, $target) {
            $description_input = new TextArea('description', 'Inhalt', null, isset($post->description) ? $post->description : null, 200, false, true);
            $description_input->addEventListener('onchange', "updateSocialPostPreview('description', this.value)");
            $description_input->addEventListener('onkeyup', "updateSocialPostPreview('description', this.value)");
            $description_input->display();

            $link_input = new TextInput('link', 'Link', null, isset($post->link) ? $post->link : null);
            $link_input->addEventListener('onchange', "updateSocialPostPreview('link', this.value)");
            $link_input->addEventListener('onkeyup', "updateSocialPostPreview('link', this.value)");
            $link_input->display();
        });
    }
}