<?php

namespace saschahuber\affiliatewebsitecore\component\form\social_post_generator;

use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\DateTimePicker;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\SimpleFabSaveForm;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\component\social_post_preview\FacebookPostPreview;
use saschahuber\saastemplatecore\component\social_post_preview\SocialPostPreview;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;
use saschahuber\saastemplatecore\ResourceProvider;

abstract class AbstractPostGeneratorForm extends Component
{
    private $post, $target;

    public function __construct($post = null, $target = null, $id = null)
    {
        parent::__construct($id);
        $this->setPost($post);
        $this->setTarget($target);
    }

    protected function build()
    {
        $container_id = $this->getId();

        $post = $this->getPost();
        $target = $this->getTarget();

        $form_column_content = $this->buildFormInputs($post, $target);

        $social_post_preview_component = $this->getSocialPostPreviewComponent($post, $target);

        $preview_column_content = BufferHelper::buffered(function () use ($container_id, $social_post_preview_component, $post, $target) {
            ?>
            <h3>Vorschau</h3>

            <p>Plattform: <?=$target?></p>

            <br>

            <?php

            $social_post_preview_component->display();

            ?>

            <script>
                setSocialPostPreviewContainerId('<?=$social_post_preview_component->getId()?>');

                let linkInput = document.querySelector('#<?=$container_id?> input[name="link"]');
                if(linkInput && linkInput.value) {
                    updateSocialPostPreview('link', linkInput.value);
                }
            </script>

            <br>
            <br>
            <?php

            $type_options = array_merge([null => "-- Nichts verknüpft --"], getContentTypes());

            $linked_element_type = isset($post->linked_element_type) ? $post->linked_element_type : RequestHelper::reqstr('linked_element_type');
            $linked_element_id = isset($post->linked_element_id) ? $post->linked_element_id : RequestHelper::reqstr('linked_element_id');

            $publish_time = isset($post->posting_time) ? $post->posting_time : RequestHelper::reqstr('publish_time');

            (new Select('linked_element_type', 'Typ des verknüpften Elements', $type_options, $linked_element_type, false))->display();

            (new NumberInput('linked_element_id', 'ID des verknüpften Elements', null, $linked_element_id, 0, null))->display();

            (new DateTimePicker('posting_time', 'Posting-Zeit', null, $publish_time, false, true))->display();

            #(new SubmitButton('Post speichern'))->display();
        });

        $columns = [
            new Column($form_column_content, ['col-md-6', 'col-12']),
            new Column(new Card($preview_column_content), ['col-md-6', 'col-12']),
        ];

        $input_rows = [
            [new HiddenInput("target", $target)],
            [new HiddenInput("linked_element_type", $post->linked_element_type)],
            [new HiddenInput("linked_element_id", $post->linked_element_id)],
            [new HiddenInput("posting_id", isset($post->id) ? $post->id : null)],
            [new Card(new Row([$columns]))]
        ];

        ?>
        <div id="<?=$container_id?>">
            <?php (new SimpleFabSaveForm($input_rows, 'social_posting/save'))->display(); ?>
        </div>
        <?php
    }

    public static function getSocialPostPreviewComponent($post, $target)
    {
        switch ($target) {
            case SocialPostingFeedItemManager::TARGET_FACEBOOK_PAGE:
            case SocialPostingFeedItemManager::TARGET_FACEBOOK_GROUP:
                return new FacebookPostPreview($post);
            case SocialPostingFeedItemManager::TARGET_INSTAGRAM:
            case SocialPostingFeedItemManager::TARGET_TWITTER:
            case SocialPostingFeedItemManager::TARGET_REDDIT:
            case SocialPostingFeedItemManager::TARGET_BLUESKY:
            case SocialPostingFeedItemManager::TARGET_PINTEREST:
                return new SocialPostPreview($post);
        }
        return new SocialPostPreview($post);
    }

    public static function getJsFiles()
    {
        return ResourceProvider::RESOURCE_PATH . '/component/js/AbstractPostGeneratorForm.js';
    }

    protected abstract function buildFormInputs($post, $target);

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post): void
    {
        $this->post = $post;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param mixed $target
     */
    public function setTarget($target): void
    {
        $this->target = $target;
    }
}