<?php

namespace saschahuber\affiliatewebsitecore\component\form\social_post_generator;

use saschahuber\affiliatewebsitecore\component\form\social_post_generator\AbstractPostGeneratorForm;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;

class RedditPostGeneratorForm extends AbstractPostGeneratorForm
{
    public function __construct($post = null, $id = null)
    {
        parent::__construct($post, SocialPostingFeedItemManager::TARGET_REDDIT, $id);
        $this->setPost($post);
    }

    protected function buildFormInputs($post, $target)
    {
        return BufferHelper::buffered(function () use ($post, $target) {
            $title_input = new TextInput('title', 'Titel', null, isset($post->title) ? $post->title : null, false, true);
            $title_input->addEventListener('onchange', "updateSocialPostPreview('title', this.value)");
            $title_input->addEventListener('onkeyup', "updateSocialPostPreview('title', this.value)");
            $title_input->display();

            $link_input = new TextInput('link', 'Link', null, isset($post->link) ? $post->link : null, false, true);
            $link_input->addEventListener('onchange', "updateSocialPostPreview('link', this.value)");
            $link_input->addEventListener('onkeyup', "updateSocialPostPreview('link', this.value)");
            $link_input->display();
        });
    }
}