<?php

namespace saschahuber\affiliatewebsitecore\component\form\social_post_generator;

use saschahuber\affiliatewebsitecore\component\form\social_post_generator\AbstractPostGeneratorForm;
use saschahuber\affiliatewebsitecore\component\form\SocialImageSettings;
use saschahuber\affiliatewebsitecore\service\SocialImageGeneratorService;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;

class LinkedInPostGeneratorForm extends AbstractPostGeneratorForm
{
    public function __construct($post = null, $id = null)
    {
        parent::__construct($post, SocialPostingFeedItemManager::TARGET_LINKEDIN, $id);
        $this->setPost($post);
    }

    protected function buildFormInputs($post, $target)
    {
        return BufferHelper::buffered(function () use ($post, $target) {
            $link_input = new TextInput('title', 'Titel', null, isset($post->title) ? $post->title : null, false, true);
            $link_input->addEventListener('onchange', "updateSocialPostPreview('title', this.value)");
            $link_input->addEventListener('onkeyup', "updateSocialPostPreview('title', this.value)");
            $link_input->display();

            $description_input = new TextArea('description', 'Inhalt', null, isset($post->description) ? $post->description : null, 200, false, true);
            $description_input->addEventListener('onchange', "updateSocialPostPreview('description', this.value)");
            $description_input->addEventListener('onkeyup', "updateSocialPostPreview('description', this.value)");
            $description_input->display();

            $link_input = new TextInput('link', 'Link', null, isset($post->link) ? $post->link : null, false, true);
            $link_input->addEventListener('onchange', "updateSocialPostPreview('link', this.value)");
            $link_input->addEventListener('onkeyup', "updateSocialPostPreview('link', this.value)");
            $link_input->display();

            $image_url_input = new TextInput('image_url', 'Bild-URL', null, isset($post->image_url) ? $post->image_url : null, false, true);
            $image_url_input->addEventListener('onchange', "updateSocialPostPreview('image_url', this.value)");
            $image_url_input->addEventListener('onkeyup', "updateSocialPostPreview('image_url', this.value)");
            $image_url_input->display();

            (new Text("Bild erstellen", 'h3'))->display();

            $social_image_generator_service = new SocialImageGeneratorService();
            $image_template_options = $social_image_generator_service->getImageTemplateOptions("pinterest");
            (new SocialImageSettings($image_template_options, null, $target))->display();
        });
    }
}