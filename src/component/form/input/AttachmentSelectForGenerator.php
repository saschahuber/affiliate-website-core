<?php

namespace saschahuber\affiliatewebsitecore\component\form\input;

use saschahuber\affiliatewebsitecore\service\AttachmentImageGeneratorService;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AttachmentSelectForGenerator extends Card{
    private $image_template;

    public function __construct($image_template=null){
        $this->setImageTemplate($image_template);
        parent::__construct($this->buildContent());
    }

    private function buildContent(){
        $image_template = $this->getImageTemplate();
        return BufferHelper::buffered(function() use ($image_template){
            global $CONFIG;
            
            $attachment_generator_service = new AttachmentImageGeneratorService();
            $image_options = $attachment_generator_service->getImageTemplateOptions();
            ?>
            <div>
                <p>
                    <label>Hintergrund</label>
                </p>
                <p style="text-align: center;">
                    <img style="max-height: 500px; max-width: 100%; object-fit: contain;"
                         class="attachment_preview" id="attachment_id_preview"
                         src="<?=$image_template?($CONFIG->website_domain . $image_options[$image_template]):''?>">
                </p>
            </div>
            <?php
            (new HiddenInput('image-template', $image_template))->display();

            (new GlobalAsyncComponentButton('Bild wählen', 'SelectGeneratorAttachmentHandler', ['image_template' => $image_template], ['centered']))->display();
        });
    }
    public function getImageTemplate()
    {
        return $this->image_template;
    }
    public function setImageTemplate($image_template)
    {
        $this->image_template = $image_template;
    }
}