<?php

namespace saschahuber\affiliatewebsitecore\component\form\input;

use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\saastemplatecore\component\form\input\Select;

class AttachedFaqSelect extends Select{
    public function __construct($selected_id = null){
        $this->faq_service = new FaqService();

        parent::__construct("attached_faq_id", "Verknüpftes FAQ", $this->buildOptions(), $selected_id, false, false, false, false, $this->buildOnChangeFunction());
    }

    private function buildOptions(){
        $options = [
            null => "--- Keins ---"
        ];
        foreach($this->faq_service->getAllFaqs() as $faq){
            $options["{$faq->id}"] = $faq->title;
        }
        return $options;
    }

    private function buildOnChangeFunction(){
        return null;
    }
}