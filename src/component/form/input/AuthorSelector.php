<?php

namespace saschahuber\affiliatewebsitecore\component\form\input;

use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\Select;

class AuthorSelector extends Component
{
    private $author_id;

    public function __construct($author_id = null, $id = null)
    {
        parent::__construct($id);
        $this->setAuthorId($author_id);
    }


    protected function build()
    {
        $author_options = [null => '-- Kein Autor gewählt --'];
        foreach ((new AuthorService())->getAll() as $author) {
            $author_options[$author->id] = $author->title;
        }
        (new Select('author_id', 'Autor', $author_options, $this->getAuthorId()))->display();
    }

    /**
     * @return mixed
     */
    public function getAuthorId()
    {
        return $this->author_id;
    }

    /**
     * @param mixed $author_id
     */
    public function setAuthorId($author_id): void
    {
        $this->author_id = $author_id;
    }
}