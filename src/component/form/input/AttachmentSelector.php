<?php

namespace saschahuber\affiliatewebsitecore\component\form\input;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\helper\BufferHelper;

class AttachmentSelector extends Card{
    private $attachment_id, $input_name, $input_title;

    public function __construct($dynamic_image_id = null, $input_name='attachment_id', $input_title='Verknüpftes Bild'){
        $this->setAttachmentId($dynamic_image_id);
        $this->setInputName($input_name);
        $this->setInputTitle($input_title);
        parent::__construct($this->buildContent());
    }

    private function buildContent(){
        $attachment_id = $this->getAttachmentId();
        $input_name = $this->getInputName();
        $input_title = $this->getInputTitle();
        return BufferHelper::buffered(function() use ($attachment_id, $input_name, $input_title){
            global $CONFIG;

            $image_manager = new ImageManager();

            $image = $image_manager->getAttachment($attachment_id);
            ?>
            <div>
                <p>
                    <label><?=$input_title?></label>
                </p>
                <p style="text-align: center;">
                    <img style="max-height: 500px; max-width: 100%; object-fit: contain;"
                         class="attachment_preview" id="attachment_id_preview_<?=$input_name?>"
                         src="<?=$image?($CONFIG->website_domain.'/data/media'.$image->file_path):''?>">
                </p>
            </div>
            <?php
            (new HiddenInput($input_name, $attachment_id))->display();

            (new GlobalAsyncComponentButton('Bild wählen', 'SelectAttachmentHandler',
                ['attachment_id' => $attachment_id, 'input_name' => $input_name], ['centered']))->display();
        });
    }

    /**
     * @return mixed
     */
    public function getAttachmentId()
    {
        return $this->attachment_id;
    }

    /**
     * @param mixed $attachment_id
     */
    public function setAttachmentId($attachment_id): void
    {
        $this->attachment_id = $attachment_id;
    }

    /**
     * @return mixed
     */
    public function getInputName()
    {
        return $this->input_name;
    }

    /**
     * @param mixed $input_name
     */
    public function setInputName($input_name): void
    {
        $this->input_name = $input_name;
    }

    /**
     * @return mixed
     */
    public function getInputTitle()
    {
        return $this->input_title;
    }

    /**
     * @param mixed $input_title
     */
    public function setInputTitle($input_title): void
    {
        $this->input_title = $input_title;
    }
}