<?php

namespace saschahuber\affiliatewebsitecore\component\form\input;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\helper\BufferHelper;

class DynamicImageSelector extends Card
{
    private $dynamic_image_type, $dynamic_image_id, $type_input_name, $id_input_name, $input_title;

    public function __construct($dynamic_image_type = null, $dynamic_image_id = null, $type_input_name = 'attachment_type', $id_input_name = 'attachment_id', $input_title = 'Verknüpftes Bild')
    {
        $this->setDynamicImageType($dynamic_image_type);
        $this->setDynamicImageId($dynamic_image_id);
        $this->setTypeInputName($type_input_name);
        $this->setIdInputName($id_input_name);
        $this->setInputTitle($input_title);
        parent::__construct($this->buildContent());
    }

    private function buildContent()
    {
        $attachment_type = $this->getDynamicImageType();
        $attachment_id = $this->getDynamicImageId();
        $type_input_name = $this->getTypeInputName();
        $id_input_name = $this->getIdInputName();
        $input_title = $this->getInputTitle();
        return BufferHelper::buffered(function () use ($attachment_type, $attachment_id, $type_input_name, $id_input_name, $input_title) {
            global $CONFIG;

            $image_manager = new ImageManager();
            $stock_image_manager = new StockImageManager();
            $ai_image_manager = new AiImageManager();

            $image = null;
            switch($attachment_type) {
                case "attachment":
                    $image = $image_manager->getAttachment($attachment_id);
                    break;
                case "stock_attachment":
                    $image = $stock_image_manager->getAttachment($attachment_id);
                    break;
                case "ai_attachment":
                    $image = $ai_image_manager->getAttachment($attachment_id);
                    break;
            }
            ?>
            <div>
                <p>
                    <label><?= $input_title ?></label>
                </p>
                <p style="text-align: center;">
                    <img style="max-height: 500px; max-width: 100%; object-fit: contain;"
                         class="attachment_preview" id="attachment_id_preview_<?= $id_input_name ?>"
                         src="<?= $image ? ($CONFIG->website_domain . '/data/media' . $image->file_path) : '' ?>">
                </p>
            </div>
            <?php
            (new HiddenInput($type_input_name, $attachment_type))->display();
            (new HiddenInput($id_input_name, $attachment_id))->display();

            (new GlobalAsyncComponentButton('Attachment wählen', 'SelectAttachmentHandler',
                ['attachment_id' => $attachment_id, 'input_name' => $id_input_name, 'type_input_name' => $type_input_name], ['centered']))->display();

            (new GlobalAsyncComponentButton('Stock-Attachment wählen', 'SelectStockAttachmentHandler',
                ['attachment_id' => $attachment_id, 'input_name' => $id_input_name, 'type_input_name' => $type_input_name], ['centered']))->display();

            (new GlobalAsyncComponentButton('AI-Attachment wählen', 'SelectAiAttachmentHandler',
                ['attachment_id' => $attachment_id, 'input_name' => $id_input_name, 'type_input_name' => $type_input_name], ['centered']))->display();
        });
    }

    /**
     * @return mixed
     */
    public function getDynamicImageType()
    {
        return $this->dynamic_image_type;
    }

    /**
     * @param mixed $dynamic_image_type
     */
    public function setDynamicImageType($dynamic_image_type): void
    {
        $this->dynamic_image_type = $dynamic_image_type;
    }

    /**
     * @return mixed
     */
    public function getDynamicImageId()
    {
        return $this->dynamic_image_id;
    }

    /**
     * @param mixed $dynamic_image_id
     */
    public function setDynamicImageId($dynamic_image_id): void
    {
        $this->dynamic_image_id = $dynamic_image_id;
    }

    /**
     * @return mixed
     */
    public function getTypeInputName()
    {
        return $this->type_input_name;
    }

    /**
     * @param mixed $type_input_name
     */
    public function setTypeInputName($type_input_name): void
    {
        $this->type_input_name = $type_input_name;
    }

    /**
     * @return mixed
     */
    public function getIdInputName()
    {
        return $this->id_input_name;
    }

    /**
     * @param mixed $id_input_name
     */
    public function setIdInputName($id_input_name): void
    {
        $this->id_input_name = $id_input_name;
    }

    /**
     * @return mixed
     */
    public function getInputTitle()
    {
        return $this->input_title;
    }

    /**
     * @param mixed $input_title
     */
    public function setInputTitle($input_title): void
    {
        $this->input_title = $input_title;
    }
}