<?php

namespace saschahuber\affiliatewebsitecore\component\form\input;

use saschahuber\affiliatewebsitecore\service\VgWortPixelService;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\Select;

class VgWortPixelSelector extends Component{
    private $vg_wort_pixel_id;

    public function __construct($vg_wort_pixel_id = null, $id = null){
        parent::__construct($id);
        $this->setVgWortPixelId($vg_wort_pixel_id);
    }

    public function getMappedPixels(){
        return (new VgWortPixelService())->getMappedPixels();
    }

    protected function build(){
        $vg_wort_pixel_options = [null => '-- Kein Pixel gewählt --'];
        foreach ($this->getMappedPixels() as $vg_wort_pixel) {

            $suffix = null;
            $content_element = getContentElementByTypeAndId($vg_wort_pixel->mapped_element_type, $vg_wort_pixel->mapped_element_id);
            if($content_element){
                $content_type = getContentTypes()[$vg_wort_pixel->mapped_element_type];
                $suffix = " [$content_type: {$content_element->title}]";
            }
            else if($vg_wort_pixel->relative_page_url){
                $suffix = " [URL: $vg_wort_pixel->relative_page_url]";
            }


            $option_name = $vg_wort_pixel->vg_wort_pixel . $suffix;

            $vg_wort_pixel_options[$vg_wort_pixel->vg_wort_pixel_id] = $option_name;
        }
        (new Select('vg_wort_pixel_id', 'VG-Wort-Pixel', $vg_wort_pixel_options, $this->getVgWortPixelId()))->display();
    }

    /**
     * @return mixed
     */
    public function getVgWortPixelId(){
        return $this->vg_wort_pixel_id;
    }

    /**
     * @param mixed $vg_wort_pixel_id
     */
    public function setVgWortPixelId($vg_wort_pixel_id): void{
        $this->vg_wort_pixel_id = $vg_wort_pixel_id;
    }
}