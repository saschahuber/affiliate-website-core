<?php

namespace saschahuber\affiliatewebsitecore\component\form\input;

use saschahuber\affiliatewebsitecore\manager\IconManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\helper\BufferHelper;

class IconSelector extends Card
{
    private $icon_id, $input_name, $input_title;

    public function __construct($dynamic_image_id = null, $input_name = 'icon_id', $input_title = 'Verknüpfter Icon')
    {
        $this->setIconId($dynamic_image_id);
        $this->setInputName($input_name);
        $this->setInputTitle($input_title);
        parent::__construct($this->buildContent());
    }

    private function buildContent()
    {
        $icon_id = $this->getIconId();
        $input_name = $this->getInputName();
        $input_title = $this->getInputTitle();
        return BufferHelper::buffered(function () use ($icon_id, $input_name, $input_title) {
            global $CONFIG;

            $icon_manager = new IconManager();

            $image = $icon_manager->getIcon($icon_id);
            ?>
            <div>
                <p>
                    <label><?= $input_title ?></label>
                </p>
                <p style="text-align: center;">
                    <img style="max-height: 500px; max-width: 100%; object-fit: contain; background-color: #eee;"
                         class="icon_preview" id="icon_id_preview_<?= $input_name ?>"
                         src="<?= $image ? ($CONFIG->website_domain . '/data/media' . $image->file_path) : '' ?>">
                </p>
            </div>
            <?php
            (new HiddenInput($input_name, $icon_id))->display();

            (new GlobalAsyncComponentButton('Icon wählen', 'SelectIconHandler',
                ['icon_id' => $icon_id, 'input_name' => $input_name], ['centered']))->display();
        });
    }

    /**
     * @return mixed
     */
    public function getIconId()
    {
        return $this->icon_id;
    }

    /**
     * @param mixed $icon_id
     */
    public function setIconId($icon_id): void
    {
        $this->icon_id = $icon_id;
    }

    /**
     * @return mixed
     */
    public function getInputName()
    {
        return $this->input_name;
    }

    /**
     * @param mixed $input_name
     */
    public function setInputName($input_name): void
    {
        $this->input_name = $input_name;
    }

    /**
     * @return mixed
     */
    public function getInputTitle()
    {
        return $this->input_title;
    }

    /**
     * @param mixed $input_title
     */
    public function setInputTitle($input_title): void
    {
        $this->input_title = $input_title;
    }
}