<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\async\AsyncComponent;
use saschahuber\saastemplatecore\component\Component;

class AsyncProductFilter extends Component{
    private $params;

    public function __construct($params=[], $id = null){
        parent::__construct($id);
        $this->setParams($params);
    }

    protected function build(){
        (new AsyncComponent('AsyncProductFilterHandler', $this->getParams()))->display();
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params): void
    {
        $this->params = $params;
    }
}