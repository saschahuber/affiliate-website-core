<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableNumberInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\async\inputs\InsertEntryButton;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;

class ProductReviewTable extends Component{
    private $product_id, $reviews;

    public function __construct($product_id, $reviews, $id = null){
        parent::__construct($id);
        $this->setProductId($product_id);
        $this->setReviews($reviews);
    }

    protected function build(){
        $item_id = $this->getProductId();

        $column_labels = [
            'Quelle',
            'Text',
            'Bewertung',
            'Aktionen',
        ];

        $rows = [];
        foreach ($this->getReviews() as $review){
            $cells = [
                new EditableTextInput('product__review', 'reviewer_name', $review->id, $review->reviewer_name),
                new EditableTextInput('product__review', 'review_summary', $review->id, $review->review_summary),
                new EditableNumberInput('product__review', 'review_value', $review->id, $review->review_value . ' / 10'),
                new DeleteButton("product__review", $review->id, 2)
            ];
            $rows[] = new TableRow($cells);
        }

        $input_config = [
            'product_id' => ['type' => 'hidden', 'value' => $item_id],
            'reviewer_name' => ['type' => 'text', 'label' => 'Quelle'],
            'review_summary' => ['type' => 'text', 'label' => 'Review-Text'],
            'review_value' => ['type' => 'number', 'label' => 'Score (0-10)', 'min' => 0, 'max' => 10, 'step' => 0.5],
        ];
        $add_review_button = new InsertEntryButton('product__review', $input_config, 'Neue Bewertung');

        $table = new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']);

        (new Card([
            $add_review_button, $table
        ]))->display();
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     */
    public function setProductId($product_id): void
    {
        $this->product_id = $product_id;
    }

    /**
     * @return mixed
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param mixed $reviews
     */
    public function setReviews($reviews): void
    {
        $this->reviews = $reviews;
    }
}