<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;

class ProductTestResult extends Component{
    private $item_id, $item;

    public function __construct($item_id, $item, $id = null){
        parent::__construct($id);
        $this->setItemId($item_id);
        $this->setItem($item);
    }

    protected function buildInnerContent(){
        $item = $this->getItem();

        return BufferHelper::buffered(function() use ($item){
            ?>
            <div class="pro-contra-container">
                <h3>Fazit</h3>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Positive Punkte (Titel)</label>
                            <input type="text" class="form-control" rows="10" name="product_pro_title"
                                   id="product_pro_title"
                                   value="<?=$item?ArrayHelper::getArrayValue($item->meta, "pro-titel", ""):''?>">
                        </div>
                        <div class="form-group">
                            <label>Positive Punkte</label>
                            <textarea class="form-control" rows="10" name="product_pro_list"
                                      id="product_pro_list"><?=$item?ArrayHelper::getArrayValue($item->meta, "pro-liste", ""):''?></textarea>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Negative Punkte (Titel)</label>
                            <input type="text" class="form-control" rows="10" name="product_contra_title"
                                   id="product_contra_title"
                                   value="<?=$item?ArrayHelper::getArrayValue($item->meta, "kontra-titel", ""):''?>">
                        </div>
                        <div class="form-group">
                            <label>Negative Punkte</label>
                            <textarea class="form-control" rows="10" name="product_contra_list"
                                      id="product_contra_list"><?=$item?ArrayHelper::getArrayValue($item->meta, "kontra-liste", ""):''?></textarea>
                        </div>
                    </div>
                </div>
                <br>
                <button class="btn btn-primary" onclick="updateProductReviewProContra(<?=$this->getItemId()?>)">Pro- & Kontra speichern</button>
            </div>
            <?php
        });
    }

    protected function build(){
        (new Card($this->buildInnerContent()))->display();
    }

    /**
     * @return mixed
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * @param mixed $item_id
     */
    public function setItemId($item_id): void
    {
        $this->item_id = $item_id;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }
}