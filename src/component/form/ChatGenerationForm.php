<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\async\AsyncComponent;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\Row;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;

class ChatGenerationForm extends Component
{
    private $system_prompt, $prompt, $model, $tokens;

    public function __construct($system_prompt='Du bist ein hilfreicher und kreativer Assistent.', $prompt=null,
                                $model=ChatGenerationTemplate::MODEL_CHAT_GPT, $tokens=3500, $id = null)
    {
        parent::__construct($id);
        $this->setSystemPrompt($system_prompt);
        $this->setPrompt($prompt);
        $this->setModel($model);
        $this->setTokens($tokens);
    }

    protected function build()
    {
        ?>
        <div id="<?=$this->getId()?>">
            <?php
            $seletable_models = [
                ChatGenerationTemplate::MODEL_CHAT_GPT =>  ChatGenerationTemplate::MODEL_CHAT_GPT,
                ChatGenerationTemplate::MODEL_CHAT_GPT_4 =>  ChatGenerationTemplate::MODEL_CHAT_GPT_4,
                ChatGenerationTemplate::MODEL_CHAT_GPT_4_TURBO =>  ChatGenerationTemplate::MODEL_CHAT_GPT_4_TURBO,
                ChatGenerationTemplate::MODEL_CHAT_GPT_4_TURBO_VISION =>  ChatGenerationTemplate::MODEL_CHAT_GPT_4_TURBO_VISION,
                ChatGenerationTemplate::MODEL_CHAT_GPT_4_O =>  ChatGenerationTemplate::MODEL_CHAT_GPT_4_O,
                ChatGenerationTemplate::MODEL_CHAT_GPT_4_O_MINI =>  ChatGenerationTemplate::MODEL_CHAT_GPT_4_O_MINI,
                ChatGenerationTemplate::MODEL_CHAT_GPT_O1_PREVIEW =>  ChatGenerationTemplate::MODEL_CHAT_GPT_O1_PREVIEW,
                ChatGenerationTemplate::MODEL_CHAT_GPT_O1_MINI =>  ChatGenerationTemplate::MODEL_CHAT_GPT_O1_MINI
            ];

            (new TextInput('system_prompt_input', 'System-Prompt', null, $this->system_prompt))->display();

            BreakComponent::break();

            (new TextArea('chat_generation_input', 'Prompt', null, $this->prompt))->display();

            BreakComponent::break();

            (new Row([
                new Column(new Select('model', 'Model', $seletable_models, $this->model), ['col-lg-6', 'col-12']),
                new Column(new NumberInput('tokens', 'Token', null, $this->tokens, 0, null, 1), ['col-lg-6', 'col-12'])
            ]))->display();

            BreakComponent::break();

            (new JsButton('<i class="fas fa-robot" aria-hidden="true"></i> Text generieren', "generateText(this, '".$this->getId()."')"))->display();
            ?>
            <br>

            <div class="response-container">

            </div>
        </div>
        <?php
    }

    /**
     * @return mixed
     */
    public function getSystemPrompt()
    {
        return $this->system_prompt;
    }

    /**
     * @param mixed $system_prompt
     */
    public function setSystemPrompt($system_prompt): void
    {
        $this->system_prompt = $system_prompt;
    }

    /**
     * @return mixed
     */
    public function getPrompt()
    {
        return $this->prompt;
    }

    /**
     * @param mixed $prompt
     */
    public function setPrompt($prompt): void
    {
        $this->prompt = $prompt;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model): void
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * @param mixed $tokens
     */
    public function setTokens($tokens): void
    {
        $this->tokens = $tokens;
    }


}