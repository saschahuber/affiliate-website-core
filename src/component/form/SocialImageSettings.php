<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\Slider;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class SocialImageSettings extends Component{
    private $template_options, $target, $post_item, $post_data;

    public function __construct($template_options, $attachment_item, $target, $id = null){
        parent::__construct($id);
        $this->setPostItem($attachment_item);
        $this->setTarget($target);
        if($attachment_item) {
            $this->setPostData(json_decode($attachment_item->post_data, true));
        }
        $this->setTemplateOptions($template_options);
    }

    protected function build(){
        $post_image_selector = null;
        $image_overlay_opacity = 50;
        $vertical_text_offset = 40;
        $image_background_url = null;
        $image_content = null;
        $template = null;
        if($this->getPostItem() && $this->getPostData()){
            $post_image_selector = $this->getPostData()['params']['post-image-selector'];
            $image_overlay_opacity = intval($this->getPostData()['params']['image-overlay-opacity']*100);
            $vertical_text_offset = $this->getPostData()['params']['vertical-text-offset'];
            $image_background_url = ArrayHelper::getArrayValue($this->getPostData()['params'], 'image-background-url');
            $image_content = $this->getPostData()['params']['image-content'];
            $template = ArrayHelper::getArrayValue($this->getPostData()['params'], 'template');
        }

        ?>
        <div id="img-settings-container">
            <p>
                <label>Beitragsbild:</label>
                <select id="post-image-selector" name="post-image-selector" onchange="changeImageSelection(this.value)">
                    <?= get_option_html([
                        'image-input-url' => 'URL',
                        'image-input-generated' => 'Bild generieren',
                        #'image-input-file' => 'Bild hochladen'
                    ], $post_image_selector);?>
                </select>
            </p>
            <div id="image-input-url" class="image-input-option">
                <p>Bild-URL: <input type="text" id="image-background-url"
                                    name="image-background-url"
                                    value="<?=$image_background_url?>"></p>
            </div>
            <div id="image-input-generated" class="image-input-option hidden">
                <h4>Bild generieren lassen</h4>
                <?php
                (new Select("image-template", "Hintergrund wählen", $this->template_options, $template))->display();
                ?>
            </div>
            <div id="image-input-file" class="image-input-option hidden">
                <p>Datei hochladen: <input type="file" accept="image/*" id="image-file"
                                           name="image-file" onchange="updatePostPreview()"></p>
            </div>

            <div id="additional-social-image-settings">
                <label>Text im Bild</label>
                <textarea name="image-content" rows="7"><?=$image_content?></textarea>

                <?php

                (new Text("Overlay-Opacity:", "span", ["centered"]))->display();

                (new Slider("image-overlay-opacity", 0, 100, $image_overlay_opacity))->display();

                (new NumberInput("vertical-text-offset", "Text-Offset", "", $vertical_text_offset, 0, 1080, 1))->display();

                (new HiddenInput("image-generated", null, 'image-generated'))->display();

                (new JsButton("Bild generieren", "generateTempImage(event, '".$this->getTarget()."')"))->display();
                ?>
            </div>
        </div>
        <?php
    }

    /**
     * @return mixed
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * @param mixed $item_id
     */
    public function setItemId($item_id): void
    {
        $this->item_id = $item_id;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getPostItem()
    {
        return $this->post_item;
    }

    /**
     * @param mixed $post_item
     */
    public function setPostItem($post_item): void
    {
        $this->post_item = $post_item;
    }

    /**
     * @return mixed
     */
    public function getPostData()
    {
        return $this->post_data;
    }

    /**
     * @param mixed $params
     */
    public function setPostData($post_data): void
    {
        $this->post_data = $post_data;
    }

    /**
     * @return mixed
     */
    public function getTemplateOptions()
    {
        return $this->template_options;
    }

    /**
     * @param mixed $template_options
     */
    public function setTemplateOptions($template_options): void
    {
        $this->template_options = $template_options;
    }

    /**
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param mixed $target
     */
    public function setTarget($target): void
    {
        $this->target = $target;
    }
}