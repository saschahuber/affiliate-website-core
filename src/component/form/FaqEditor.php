<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\FormGroup;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\Row;

class FaqEditor extends Component
{
    private $item_type, $item_id;

    public function __construct($item_type, $item_id, $id = null)
    {
        $this->item_type = $item_type;
        $this->item_id = $item_id;
        parent::__construct($id);
    }

    protected function build()
    {
        $item = $this->getItem();
        $meta_title = null;
        if (isset($item->meta_title)) {
            $meta_title = $item->meta_title;
        }
        $meta_description = null;
        if (isset($item->meta_description)) {
            $meta_description = $item->meta_description;
        }
        $og_title = null;
        if (isset($item->og_title)) {
            $og_title = $item->og_title;
        }
        $og_description = null;
        if (isset($item->og_description)) {
            $og_description = $item->og_description;
        }

        (new Row([
            new Column(
                new Card(
                    new FormGroup([
                        new TextArea("meta_title", 'Meta Titel', '(Lasse leer für Titel)', $meta_title),
                        new TextArea("meta_description", 'Meta Beschreibung', '(Lasse leer für Beschreibung)', $meta_description)
                    ], "Meta-Daten"
                    )
                ),
                ["col-12", "col-md-6"]
            ),
            new Column(
                new Card(
                    new FormGroup([
                        new TextArea("og_title", 'Open-Graph Titel', '(Lasse leer für Meta Titel)', $og_title),
                        new TextArea("og_description", 'Open-Graph Beschreibung', '(Lasse leer für Meta Beschreibung)', $og_description)
                    ], "OG-Daten"
                    )
                ),
                ["col-12", "col-md-6"]
            )
        ]))->display();
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }


}