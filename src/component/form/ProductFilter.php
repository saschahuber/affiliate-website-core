<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\service\ProductFilterService;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\RowContainer;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\RangeSlider;
use saschahuber\saastemplatecore\component\form\input\Select;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RowHelper;

class ProductFilter extends Component{
    protected $filter_params;

    public function __construct($filter_params=null, $id = null){
        parent::__construct($id);
        $this->setFilterParams($filter_params);
    }

    protected function build(){

        $filter_params = $this->getFilterParams();

        $content = BufferHelper::buffered(function () use ($filter_params){

            $product_manager = AffiliateInterfacesHelper::getProductManager();

            $id_prefix = 'product_filter';

            $pre_selected_categories = null;
            $pre_selected_hersteller = null;
            $pre_selected_price_min = 0;
            $pre_selected_price_max = 5000;

            $pre_selected_values = [];
            if(count($filter_params)){
                $pre_selected_values = (new ProductFilterService())->getPreFilledFilterValuesByParams($filter_params);

                $pre_selected_price_min = ArrayHelper::getArrayValue($pre_selected_values, 'price_min', $pre_selected_price_min);
                $pre_selected_price_max = ArrayHelper::getArrayValue($pre_selected_values, 'price_max', $pre_selected_price_max);
            }

            $contents = [];
            $hidden_contents = [];

            $this->addProductDataContents($id_prefix, $filter_params, $contents, $hidden_contents,
                $product_manager, $pre_selected_values, []);

            $this->addCategoryContents($id_prefix, $filter_params, $contents, $hidden_contents,
                $product_manager, $pre_selected_values, $pre_selected_categories);

            $this->addBrandContents($id_prefix, $filter_params, $contents, $hidden_contents,
                $pre_selected_values, $pre_selected_hersteller);

            $this->addPriceContents($id_prefix, $filter_params, $contents, $hidden_contents,
                $pre_selected_price_min, $pre_selected_price_max);

            //Angebote
            if($reduced = ArrayHelper::getArrayValue($filter_params, 'reduced', false)){
                $hidden_contents[] = new HiddenInput($id_prefix . '_reduced', $reduced);
            }
            else {
                $contents[] = new Card(new Toggle($id_prefix . '_reduced', 'Nur Angebote', false, false, true));
            }

            $submit_button = new JsButton('Filtern', 'runProductFilter(this)', ['btn-lg']);

            $column_count = min(4, count($contents));

            (new RowContainer(RowHelper::groupItemsInRows($contents, $column_count, 'lg')))->display();

            #(new GridContainer($contents, min(4, count($contents))))->display();
            foreach($hidden_contents as $hidden_content){
                $hidden_content->display();
            }
            BreakComponent::break();
            (new FloatContainer([$submit_button]))->display();
        });

        (new Card($content, ['product-filter']))->display();
    }

    protected function addProductDataContents($id_prefix, $filter_params, &$contents, &$hidden_contents, $product_manager,
                                            $pre_selected_values, $pre_selected_productdata){
        $allowed_categories = array_filter(explode(',', ArrayHelper::getArrayValue($filter_params, 'kategorie', '')));

        $produktdaten = ArrayHelper::getArrayValue($filter_params, 'produktdaten', false);

        $product_data_field_group_manager = new ProductDataFieldGroupManager();
        $product_data_field_manager = new ProductDataFieldManager();

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $extended_category_ids = [];
        foreach($allowed_categories as $category_id){
            $category = $product_manager->getTaxonomyById($category_id);

            if(!$category){
                continue;
            }

            $extended_category_ids[] = $category->id;

            if($category->is_virtual && $category->taxonomy_parent_id) {
                $parent_category = $product_manager->getTaxonomyById($category->taxonomy_parent_id);

                if(!$parent_category){
                    continue;
                }
                $extended_category_ids[] = $parent_category->id;
            }
        }

        $fields_in_filter = [];
        if(count($extended_category_ids)) {
            foreach ($product_data_field_group_manager->getDataFieldGroups($extended_category_ids) as $group) {
                foreach ($product_data_field_manager->getDataFields($group->id) as $field) {
                    if ($field->show_in_filter) {
                        $fields_in_filter[] = $field;
                    }
                }
            }
        }

        foreach($fields_in_filter as $field){
            $input = null;

            switch($field->field_type){
                case "enum":
                    $field_values = $product_data_field_manager->getDataFieldValueOptions($field->id);

                    $values = [];
                    foreach($field_values as $field_value){
                        $values[$field_value->value]  =$field_value->value;
                    }

                    $input = new Select($id_prefix . '_productdata-' . $field->id, $field->field_name, $values, null, true);
                    break;
                case "boolean":
                    $input = new Toggle($id_prefix . '_productdata-' . $field->id, $field->field_name, false, false, true);
                    break;
                case "float":
                    $min_max_data = $product_data_field_manager->getNumericDataFieldMinMax($field->id);
                    $value_min = floor($min_max_data->min);
                    if($value_min == 1){
                        $value_min = 0;
                    }

                    $value_max = ceil($min_max_data->max);

                    if($value_min != $value_max) {
                        $input = [
                            new Text($field->field_name, 'strong'),
                            new RangeSlider($id_prefix . '_productdata-' . $field->id, $value_min, $value_max,
                                $value_min, $value_max, $field->field_suffix)
                        ];
                    }
                    break;
                case "string":
                default:
                    $input = new TextInput($id_prefix . '_productdata-' . $field->id, $field->field_name, '');
                    break;
            }

            $contents[] = new Card($input);
        }
    }

    protected function addCategoryContents($id_prefix, $filter_params, &$contents, &$hidden_contents, $product_manager,
                                         $pre_selected_values, $pre_selected_categories){
        if(($allowed_categories = ArrayHelper::getArrayValue($pre_selected_values, 'kategorie', null)) === null){
            $allowed_categories = [];
        }

        //Produktkategorien
        if($kategorie = ArrayHelper::getArrayValue($filter_params, 'kategorie', false)){
            $hidden_contents[] = new HiddenInput($id_prefix . '_kategorie', $kategorie);
        }
        else {
            if($allowed_categories === null) {
                $allowed_categories = [];
            }

            $brand_id = ArrayHelper::getArrayValue($filter_params, 'hersteller', null);
            if($brand_id){
                foreach ((new BrandManager())->getTaxonomiesByBrand($brand_id) as $taxonomy) {
                    $allowed_categories[$taxonomy->id] = $taxonomy->title;
                }
            }

            #foreach ($product_manager->getTaxonomies('title', true, Manager::STATUS_PUBLISH) as $taxonomy) {
            #    $allowed_categories[$taxonomy->id] = $taxonomy->title;
            #}

            if(count($allowed_categories) > 1) {
                $contents[] = new Card(new Select($id_prefix . '_kategorie', 'Kategorie',
                    $allowed_categories, $pre_selected_categories, true));
            }
        }
    }

    protected function addBrandContents($id_prefix, $filter_params, &$contents, &$hidden_contents, $pre_selected_values, $pre_selected_hersteller){
        $brand_manager = new BrandManager();

        if(($allowed_brands = ArrayHelper::getArrayValue($pre_selected_values, 'hersteller', null)) === null){
            $allowed_brands = [];
        }

        //Hersteller
        if($hersteller = ArrayHelper::getArrayValue($filter_params, 'hersteller', false)){
            $hidden_contents[] = new HiddenInput($id_prefix . '_hersteller', $hersteller);
        }
        else {
            if($allowed_brands === null) {
                $allowed_brands = [];
                foreach ($brand_manager->getAll(Manager::STATUS_PUBLISH, false, 'title', false) as $brand) {
                    $allowed_brands[$brand->id] = $brand->title;
                }
            }
            if(count($allowed_brands) > 1) {
                $contents[] = new Card(new Select($id_prefix . '_hersteller', 'Hersteller', $allowed_brands, $pre_selected_hersteller, true));
            }
        }
    }

    protected function addPriceContents($id_prefix, $filter_params, &$contents, &$hidden_contents, $pre_selected_price_min, $pre_selected_price_max){
        if($price_min = ArrayHelper::getArrayValue($filter_params, 'price_min', false)){
            $hidden_contents[] = new HiddenInput($id_prefix . '_price_range_min', $price_min);
        }
        else if($price_max = ArrayHelper::getArrayValue($filter_params, 'price_max', false)){
            $hidden_contents[] = new HiddenInput($id_prefix . '_price_range_max', $price_max);
        }
        else {
            if($pre_selected_price_min != $pre_selected_price_max) {
                $contents[] = new Card([
                    new RangeSlider($id_prefix . '_price_range', $pre_selected_price_min, $pre_selected_price_max,
                        $pre_selected_price_min, $pre_selected_price_max, '€')
                ]);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getFilterParams()
    {
        return $this->filter_params;
    }

    /**
     * @param mixed $filter_params
     */
    public function setFilterParams($filter_params): void
    {
        $this->filter_params = $filter_params;
    }

    static function getCssFiles()
    {
        return COMMONS_BASE . '/resources/component/css/ProductFilter.css';
    }

    static function getJsFiles()
    {
        return COMMONS_BASE . '/resources/component/js/ProductFilter.js';
    }
}