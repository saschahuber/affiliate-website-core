<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\affiliatewebsitecore\component\form\input\AttachmentSelectForGenerator;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\form\input\HiddenInput;
use saschahuber\saastemplatecore\component\form\input\NumberInput;
use saschahuber\saastemplatecore\component\form\input\Slider;
use saschahuber\saastemplatecore\component\form\input\TextArea;
use saschahuber\saastemplatecore\component\form\input\TextInput;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class AttachmentImageSettings extends Component{
    private $template_options, $attachment_item, $generator_data;

    public function __construct($template_options, $attachment_item, $id = null){
        parent::__construct($id);
        $this->setAttachmentItem($attachment_item);
        if($attachment_item) {
            $this->setGeneratorData(json_decode($attachment_item->generator_data, true));
        }
        $this->setTemplateOptions($template_options);
    }

    protected function build(){
        $attachment = $this->getAttachmentItem();
        $generator_data = $this->getGeneratorData();
        $filename = null;
        $post_image_selector = null;
        $image_overlay_opacity = 50;
        $vertical_text_offset = -80;
        $image_background_url = null;
        $show_logo = true;
        $vertical_logo_offset = 40;
        $show_logo_as_watermark = false;
        $image_content = null;
        $template = null;
        if($attachment && $generator_data){
            $filename = ArrayHelper::getArrayValue($generator_data, 'filename', null);
            $post_image_selector = ArrayHelper::getArrayValue($generator_data, 'post-image-selector', null);
            $image_overlay_opacity = intval(ArrayHelper::getArrayValue($generator_data, 'image-overlay-opacity', 0)*100);
            $vertical_text_offset = ArrayHelper::getArrayValue($generator_data, 'vertical-text-offset', 0);
            $image_background_url = ArrayHelper::getArrayValue($generator_data, 'image-background-url');
            $image_content = ArrayHelper::getArrayValue($generator_data, 'image-content', null);
            $template = ArrayHelper::getArrayValue($generator_data, 'image-template');
        }

        /*
        Format
        Hintergrundbild (Medien, Stock)
        Overlay über Hintergrundbild
        Wasserzeichen (Ja, Nein, Position)
        Overlay über Wasserzeichen
        Text (Position, Größe, Font)
        Alt-Text des Bildes
        Titel des Bildes
        Dateiname des Bildes
        */

        ?>
        <div>
            <div id="img-settings-container">
                <?php

                (new AttachmentSelectForGenerator($template))->display();

                BreakComponent::break();

                (new HiddenInput('attachment_id', $attachment?$attachment->id:null))->display();
                TextInput::required('filename', "Name der Datei", null, $filename)->display();

                (new Text("Transparenz von Overlay über Hintergrund:", "span", ["centered"]))->display();
                (new Slider("image-overlay-opacity", 0, 100, $image_overlay_opacity))->display();

                BreakComponent::break();

                (new Toggle('show_logo', 'Logo anzeigen', $show_logo, false, true))->display();
                (new NumberInput("vertical-logo-offset", "Verikales Logo-Offset", "", $vertical_logo_offset, 0, 1080, 1))->display();

                BreakComponent::break();

                (new Toggle('show_logo_as_watermark', 'Logo als Wasserzeichen anzeigen (Zentriert)', $show_logo_as_watermark, false, true))->display();

                BreakComponent::break();

                (new TextArea('image-content', 'Text im Bild', null, $image_content))->display();

                BreakComponent::break();

                (new NumberInput("vertical-text-offset", "Text-Offset", "", $vertical_text_offset, -300, 1080, 1))->display();

                BreakComponent::break();

                (new HiddenInput("image-generated", null, 'image-generated'))->display();

                (new JsButton('<i class="fas fa-cog"></i> Bild generieren', "generateTempAttachmentImage(this, event)"))->display();
                ?>
            </div>
        </div>
        <?php
    }

    /**
     * @return mixed
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * @param mixed $item_id
     */
    public function setItemId($item_id): void
    {
        $this->item_id = $item_id;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getAttachmentItem()
    {
        return $this->attachment_item;
    }

    /**
     * @param mixed $attachment_item
     */
    public function setAttachmentItem($attachment_item): void
    {
        $this->attachment_item = $attachment_item;
    }

    /**
     * @return mixed
     */
    public function getGeneratorData()
    {
        return $this->generator_data;
    }

    /**
     * @param mixed $params
     */
    public function setGeneratorData($generator_data): void
    {
        $this->generator_data = $generator_data;
    }

    /**
     * @return mixed
     */
    public function getTemplateOptions()
    {
        return $this->template_options;
    }

    /**
     * @param mixed $template_options
     */
    public function setTemplateOptions($template_options): void
    {
        $this->template_options = $template_options;
    }
}