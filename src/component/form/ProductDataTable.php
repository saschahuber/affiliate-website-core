<?php

namespace saschahuber\affiliatewebsitecore\component\form;

use saschahuber\saastemplatecore\component\async\inputs\DeleteButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableNumberInput;
use saschahuber\saastemplatecore\component\async\inputs\EditableTextInput;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Card;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\container\ListContainer;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\BufferHelper;

class ProductDataTable extends Component{
    private $item, $group_data_mappings, $product_data_fields;

    public function __construct($item, $group_data_mappings, $product_data_fields, $id = null){
        parent::__construct($id);
        $this->setItem($item);
        $this->setGroupDataMappings($group_data_mappings);
        $this->setProductDataFields($product_data_fields);
    }

    protected function build(){
        $item = $this->getItem();
        $product_data_fields = $this->getProductDataFields();

        $column_labels = ['Gruppe', 'Felder'];

        $rows = [];
        foreach ($this->getGroupDataMappings() as $group_id => $group_data_row){
            $group = $group_data_row['group'];
            $data_fields = $group_data_row['data'];

            $value_items = [];
            foreach ($data_fields as $data_field){
                $field_value = "Kein Wert angegeben";
                if (isset($product_data_fields[$group_id])
                    && isset($product_data_fields[$group_id][$data_field->id])) {
                    switch($data_field->field_type){
                        case "boolean":
                            $field_value = $product_data_fields[$group_id][$data_field->id]->value;
                            $field_value = $field_value==="1"?'Ja':'Nein';
                            break;
                        case "float":
                        case "string":
                        case "enum":
                        default:
                            $field_value = $product_data_fields[$group_id][$data_field->id]->value;
                            break;
                    }
                }

                $value_items[] = "<strong>{$data_field->field_name}</strong>: {$data_field->field_prefix} $field_value {$data_field->field_suffix}";
            }

            $cells = [
                $group->group_name,
                new ListContainer($value_items)
            ];
            $rows[] = new TableRow($cells);
        }

        (new ElevatedCard([
            new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']),
            new BreakComponent(),
            new LinkButton("/dashboard/produkte/daten/produkt-bearbeiten/{$item->id}", 'Produktdaten bearbeiten', 'fas fa-edit', false, true)
        ]))->display();
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item): void
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getGroupDataMappings()
    {
        return $this->group_data_mappings;
    }

    /**
     * @param mixed $group_data_mappings
     */
    public function setGroupDataMappings($group_data_mappings): void
    {
        $this->group_data_mappings = $group_data_mappings;
    }

    /**
     * @return mixed
     */
    public function getProductDataFields()
    {
        return $this->product_data_fields;
    }

    /**
     * @param mixed $product_data_fields
     */
    public function setProductDataFields($product_data_fields): void
    {
        $this->product_data_fields = $product_data_fields;
    }
}