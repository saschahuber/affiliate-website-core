<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;

#[AllowDynamicProperties]
class SEOAnalyzerButton extends JsButton
{
    private $keyword;

    public function __construct($keyword=null, $id = null)
    {
        parent::__construct('<i class="fab fa-searchengine"></i>', null, ['fab', 'sticky'], $id);
        $this->setKeyword($keyword);
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall()
    {
        return "loadSEOReport(this, '{$this->getKeyword()}')";
    }

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword): void
    {
        $this->keyword = $keyword;
    }

    static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/SEOAnalyzerButton.js';
    }
}