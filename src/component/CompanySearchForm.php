<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\component\item\CompanyListItem;
use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;

class CompanySearchForm extends HTMLElement {

    private $taxonomy;

    public function __construct($taxonomy=null, $id=null){
        $this->taxonomy = $taxonomy;
        parent::__construct($this->getSearchFormContent());
    }

    protected function getSearchFormContent(){
        $taxonomy = $this->taxonomy;
        return BufferHelper::buffered(function () use ($taxonomy){
            ?>
            <form class="company-search-form-container" onsubmit="hideSearchResults(); search(this.value); return false;">
                <div class="company-search-form">
                    <div class="keyword-input">
                        <input id="keyword-input" type="text" placeholder="Suchbegriff..." name="s" value="<?=RequestHelper::reqstr('s')?>">
                    </div>
                    <div class="location-input">
                        <input id="location-input" type="text" placeholder="Ort / PLZ" name="location" value="<?=RequestHelper::reqstr('location')?>">
                    </div>
                    <div id="search-button">
                        <div>
                            <p><i class="fas fa-search" aria-hidden="true"></i></p>
                        </div>
                    </div>
                </div>
                <input id="taxonomy-input" type="hidden" name="taxonomy" value="<?=($taxonomy !== null ? $taxonomy : '')?>">

                <div id="search-results-container" class="hidden"></div>
            </form>
            <?php
        });
    }

    static function getCssFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/css/CompanySearchForm.css';
    }

    static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/CompanySearchForm.js';
    }
}