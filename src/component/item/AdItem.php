<?php

namespace saschahuber\affiliatewebsitecore\component\item;

use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\component\Row;

class AdItem extends HTMLElement
{
    public function __construct($ad, $id = null)
    {
        parent::__construct($ad->content, $id);
    }

    protected function build()
    {
        (new Row([new Column($this->getContent())]))->display();
    }
}