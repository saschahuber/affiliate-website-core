<?php

namespace saschahuber\affiliatewebsitecore\component\item;

use saschahuber\affiliatewebsitecore\component\container\AdContainer;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;

class AdContainerColumn extends Component
{
    private $position, $content_type, $content_id;

    public function __construct($position, $content_type, $content_id, $id = null)
    {
        parent::__construct($id);
        $this->position = $position;
        $this->content_type = $content_type;
        $this->content_id = $id;
    }

    protected function build()
    {
        (new Column(new AdContainer($this->position, $this->content_type, $this->content_id), ['col-12']))->display();
    }

    /**
     * @return mixed
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * @param mixed $ad_content
     */
    public function setAd($ad): void
    {
        $this->ad = $ad;
    }
}