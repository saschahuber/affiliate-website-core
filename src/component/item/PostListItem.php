<?php

namespace saschahuber\affiliatewebsitecore\component\item;

use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\ImgUtils;

class PostListItem extends HTMLElement
{
    public function __construct($post)
    {
        parent::__construct($this->getPostContent($post));
    }

    private function getPostContent($post)
    {
        return BufferHelper::buffered(function () use ($post) {
            ?>
            <div class="post-item text-center layout-list">
                <a href="<?= $post->permalink ?>">
                    <?php
                    #if($post->is_sponsored){
                    #    (new \SponsoredBadge(false))->display();
                    #}
                    ?>

                    <div class="row">
                        <div class="col-4">

                            <?=ImgUtils::getImageTag($post->thumbnail_src, true, $post->title, $post->title, 384, null, ['post-thumbnail', 'list'])?>
                        </div>
                        <div class="col-8">
                            <div class="post-data">
                                <span class="post-title"><?= $post->title ?></span>
                                <p><?= $post->excerpt ?></p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php
        });
    }
}