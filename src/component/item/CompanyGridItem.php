<?php

namespace saschahuber\affiliatewebsitecore\component\item;

use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\service\CompanyThumbnailGeneratorService;
use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\StaticHelper;

class CompanyGridItem extends HTMLElement {
    public function __construct($company){
        parent::__construct($this->getCompanyContent($company));
    }

    private function getCompanyContent($company){
        return BufferHelper::buffered(function() use ($company){
            ?>
            <div style="padding-top: 10px; padding-bottom: 10px;">
                <div class="company-item text-center">
                    <a href="<?=$company->permalink?>">
                        <?php
                        if($company->is_featured){
                            echo "<span class='featured-badge'><i class='fas fa-trophy'></i>&nbsp;Empfohlen</span>";
                        }

                        if(CompanyManager::hasThumbnail($company)){
                            $thumbnail = $company->thumbnail_src;
                        }
                        else {
                            $thumbnail = (new CompanyThumbnailGeneratorService())->getThumbnailSrc($company);
                        }

                        ?>

                        <div class="company-thumbnail-container">
                            <?php
                            if(isset($company->distance)){
                                echo "<span class='distance'>".round($company->distance, 1)." km</span>";
                            }
                            ?>

                            <img class="company-thumbnail" src="<?= StaticHelper::getResizedImgUrl($thumbnail, 600)?>">
                        </div>


                        <span class="company-title"><?=$company->title?></span>
                    </a>
                </div>
            </div>
            <?php
        });
    }
}