<?php

namespace saschahuber\affiliatewebsitecore\component\item;

use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\BufferHelper;

class CompanyListItem extends HTMLElement {
    public function __construct($company){
        parent::__construct($this->getPostContent($company));
    }

    private function getPostContent($company){
        return BufferHelper::buffered(function() use ($company){
            ?>
            <div class="company-item text-center">
                <a href="<?=$company->permalink?>">
                    <?php
                    if(isset($company->distance)){
                        echo "<span class='distance'>".round($company->distance, 1)." km</span>";
                    }
                    ?>
                    <div class="row">
                        <div class="col-4">
                            <img class="post-thumbnail list" src="<?=$company->thumbnail_src?>" title="Firma <?=$company->title?>" alt="Firma <?=$company->title?>">
                        </div>
                        <div class="col-8" style="display: table;">
                            <div class="post-data" style="display: table-cell; vertical-align: middle;">
                                <span class="post-title"><strong><?=$company->title?></strong></span>
                                <p><?=$company->excerpt?></p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php
        });
    }
}