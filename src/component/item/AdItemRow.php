<?php

namespace saschahuber\affiliatewebsitecore\component\item;

use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\Row;

class AdItemRow extends Component
{
    private $ad;

    public function __construct($ad, $id = null)
    {
        parent::__construct($id);
        $this->setAd($ad);
    }

    protected function build()
    {
        (new Row([new AdItemColumn($this->getAd())]))->display();
    }

    /**
     * @return mixed
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * @param mixed $ad_content
     */
    public function setAd($ad): void
    {
        $this->ad = $ad;
    }
}