<?php

namespace saschahuber\affiliatewebsitecore\component\item;

use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\BufferHelper;

class CompanyTaxonomyGridItem extends HTMLElement {
    public function __construct($company_taxonomy){
        parent::__construct($this->getPostContent($company_taxonomy));
    }

    private function getPostContent($company_taxonomy){
        return BufferHelper::buffered(function() use ($company_taxonomy){
            ?>
            <div class="company-taxonomy-item text-center">
                <div>
                    <a href="<?=$company_taxonomy->permalink?>">
                        <div class="company-taxonomy-data">
                            <span class="post-title"> <i class="<?=$company_taxonomy->icon?>"></i> <?=$company_taxonomy->title?></span>
                        </div>
                    </a>
                </div>
            </div>
            <?php
        });
    }
}