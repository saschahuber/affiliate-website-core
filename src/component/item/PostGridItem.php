<?php

namespace saschahuber\affiliatewebsitecore\component\item;

use saschahuber\saastemplatecore\component\HTMLElement;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;

class PostGridItem extends HTMLElement
{
    public function __construct($post)
    {
        parent::__construct($this->getPostContent($post));
    }

    private function getPostContent($post)
    {
        return BufferHelper::buffered(function () use ($post) {
            global $CONFIG;

            ?>
            <div class="post-item text-center layout-grid">
                <a href="<?= $post->full_permalink ?>">
                    <?php
                    #if($post->is_sponsored){
                    #    (new \SponsoredBadge(false))->display();
                    #}

                    #$relative_image_path = "/img/blog/thumbnail/{$post->id}/".UrlHelper::alias($post->title).".jpg";
                    #$save_path = APP_BASE . '/front' . $relative_image_path;
                    #FileHelper::createDirIfNotExists(dirname($save_path));
                    #$image_generator_service = new ThumbnailGeneratorService();
                    #$image_generator_service->generateThumbnail($CONFIG->website_domain . $post->thumbnail_src, $post->title, $save_path);
                    #echo ImgUtils::getImageTag($CONFIG->website_domain . $relative_image_path, true, $post->title, $post->title, 384, null, ['post-thumbnail']);

                    echo ImgUtils::getImageTag($post->thumbnail_src, true, $post->title, $post->title, 384, null, ['post-thumbnail']);

                    ?>

                    <div class="post-data">
                        <span class="post-title"><?= $post->title ?></span>
                    </div>
                </a>

                <i class="blog-post-button fas fa-chevron-right"></i>
            </div>
            <?php
        });
    }
}