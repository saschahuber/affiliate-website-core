<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\async\GlobalAsyncComponentButton;
use saschahuber\saastemplatecore\component\async\inputs\EditableDateTimePicker;
use saschahuber\saastemplatecore\component\async\inputs\EditableToggle;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\form\input\EditableSelect;
use saschahuber\saastemplatecore\component\form\input\Toggle;
use saschahuber\saastemplatecore\component\Image;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\Pagination;
use saschahuber\saastemplatecore\component\table\PaginatedItemTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\DatabaseTimeSeriesHelper;

class PaginatedProductTable extends PaginatedItemTable {

    protected $product_manager, $brand_manager;

    public function __construct($items, $offset=0, $items_per_page=25, $page_number=1, $total_item_count=null, $id=null){
        parent::__construct($items, $offset, $items_per_page, $page_number, $total_item_count, $id);

        $this->product_manager = AffiliateInterfacesHelper::getProductManager();
        $this->product_data_field_manager = new ProductDataFieldManager();
        $this->product_data_field_group_manager = new ProductDataFieldGroupManager();
        $this->brand_manager = new BrandManager();
    }

    protected function getColumnLabels(){
        $column_labels = [
            'Thumbnail',
            'Produkt',
            'Status',
            'Datum',
            'Kategorien',
            'Quick-Settings',
            'Info',
            'Datenpflege',
            'Aktionen',
        ];
        return $column_labels;
    }

    protected function getProductInfoRows($item){
        $item_brand_names = [];
        foreach($this->brand_manager->getBrandsByProductId($item->id, null) as $brand){
            $item_brand_names[] = $brand->title;
        }

        $product_info_rows = [new Text($item->title, 'p', ['product-title'])];

        if(count($item_brand_names)){
            $product_info_rows[] = new Text('Hersteller: ' . implode(',', $item_brand_names), 'strong', ['product-title']);
        }

        if(isset($item->ean) && $item->ean){
            $product_info_rows[] = new Text('EAN: ' . $item->ean, 'strong');
        }

        return $product_info_rows;
    }

    protected function getDataMaintenanceStatusField($item){
        $taxonomy_ids = [];
        foreach($item->taxonomies as $taxonomy){
            $taxonomy_ids[] = $taxonomy->id;
        }

        if(count($taxonomy_ids) < 1){
            return '';
        }

        $groups = [];
        $taxonomy_groups = [];
        foreach($this->product_data_field_group_manager->getDataFieldGroups($taxonomy_ids) as $group){
            $groups[$group->product__data_field_group_id] = $group;

            if(!array_key_exists($group->product__data_field_group_id, $taxonomy_groups)){
                $taxonomy_groups[$group->product__data_field_group_id] = [];
            }
            $fields = $this->product_data_field_manager->getDataFields($group->id);
            foreach($fields as $field) {
                $field->value = null;
                $taxonomy_groups[$group->id][$field->id] = $field;
            }
        }

        $data_fields = $this->product_data_field_manager->getProductDataFields($item);
        foreach($data_fields as $group_id => $fields) {
            if(!array_key_exists($group_id, $taxonomy_groups)){
                $taxonomy_groups[$group_id] = [];
            }
            foreach($fields as $field) {
                $taxonomy_groups[$group_id][$field->product__data_field_id] = $field;
            }
        }

        if(count($taxonomy_groups) < 1){
            return '';
        }

        $maintained_data_fields = 0;
        $total_data_fields = 0;

        foreach($taxonomy_groups as $group_id => $fields) {
            foreach ($fields as $product__data_field_id => $field) {
                $total_data_fields += 1;
                if($field->value !== null && $field->value !== ''){
                    $maintained_data_fields += 1;
                }
            }
        }

        return BufferHelper::buffered(function() use ($item, $maintained_data_fields, $total_data_fields){
            $style = '';

            if($maintained_data_fields >= $total_data_fields){
                $style = 'style="background-color: green; color: #fff;"';
            }
            else if($total_data_fields > 0 && $maintained_data_fields === 0){
                $style = 'style="background-color: red; color: #fff;"';
            }


            ?>
            <p <?=($style?$style:'')?>><?=$maintained_data_fields?>/<?=$total_data_fields?></p>
            <?php
        });
    }

    protected function getRow($item){
        global $CONFIG;

        $product_actions = [];

        $post_category_buttons = [];
        foreach($item->taxonomies as $taxonomy){
            $button = new LinkButton("/dashboard/produkte/kategorien/bearbeiten/" . $taxonomy->id, $taxonomy->title, 'fas fa-pen', false, false);
            $post_category_buttons[] = $button;
        }

        if($item->status == Manager::STATUS_PUBLISH) {
            $product_actions[] = new LinkButton($CONFIG->website_domain . $this->product_manager->generatePermalink($item), 'Ansehen', 'fas fa-eye', false, true);
        }
        $product_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-eye"></i> Vorschau', 'ItemPreviewHandler', ['type' => ProductManager::TYPE_PRODUCT, 'id' => $item->id]);
        $product_actions[] = new LinkButton("/dashboard/produkte/bearbeiten/" . $item->id, 'Bearbeiten', 'fas fa-pen', false, false);
        $product_actions[] = new LinkButton('/dashboard/produkte/link-clicks?product_id='.$item->id, 'Link-Klicks prüfen', 'fas fa-history', false, true);

        if(count($post_category_buttons)) {
            $product_actions[] = new LinkButton("/dashboard/produkte/daten/produkt-bearbeiten/" . $item->id, 'Daten Bearbeiten', 'fas fa-pen', false, true);
        }

        $product_actions[] = new GlobalAsyncComponentButton('<i class="fas fa-chart-line"></i> Klicks', 'ElementViewsLastIntervalAsyncHandler', ['height' => 350, 'url' => getContentPermalinkElementByTypeAndId(ProductManager::TYPE_PRODUCT, $item->id), 'item_type' => ProductManager::TYPE_PRODUCT, 'item_id' => $item->id, 'interval' => DatabaseTimeSeriesHelper::INTERVAL_DAY, 'number' => 90]);

        $product_info_rows = $this->getProductInfoRows($item);

        $cells = [
            new Image($this->product_manager->getProxiedProductImage($item)['url'], 200, ['product-image'], true),
            new GridContainer($product_info_rows, 1, null, null, ['product-title-container']),
            new EditableSelect('product', 'status', $item->id, $item->status, Manager::getStatusTypes()),
            new EditableDateTimePicker('product', 'product_date', $item->id, $item->product_date),
            new FloatContainer($post_category_buttons, ['product-categories']),
            BufferHelper::buffered(function() use ($item){
                (new EditableToggle('product', 'doindex', $item->id, $item->doindex, true, "Index"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('product', 'dofollow', $item->id, $item->dofollow, true, "Follow"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('product', 'hide_in_search', $item->id, $item->hide_in_search, true, "In Suche verstecken"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('product', 'hide_in_newsletter', $item->id, $item->hide_in_newsletter, true, "In Newsletter erstecken"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('product', 'is_sponsored', $item->id, $item->is_sponsored, true, "Gesponsert"))->display();
                (new BreakComponent())->display();
                (new EditableToggle('product', 'is_fake', $item->id, $item->is_fake, true, "Fake"))->display();
            }),
            BufferHelper::buffered(function() use ($item){
                (new Toggle('has_meta_title', "Titel", isset($item->meta_title) && strlen($item->meta_title), true, true))->display();
                (new BreakComponent())->display();
                (new Toggle('has_meta_description', "Description", isset($item->meta_description) && strlen($item->meta_description), true, true))->display();
                (new BreakComponent())->display();
                (new Toggle('has_content', "Inhalt", isset($item->content) && strlen($item->content), true, true))->display();
                (new BreakComponent())->display();
                (new Toggle('has_vg_wort_pixel', "VG-Wort", isset($item->vg_wort_pixel_id) && strlen($item->vg_wort_pixel_id), true, true))->display();
                (new BreakComponent())->display();
                (new Toggle('is_available', "Verfügbar", $item->available_link_count>0, true, true))->display();
            }),
            $this->getDataMaintenanceStatusField($item),
            new FloatContainer($product_actions, ['product-actions'])
        ];
        return new TableRow($cells);
    }

    protected function buildPagination(){
        (new Pagination('/dashboard/produkte/'.Pagination::PAGINATION_PLACEHOLDER.'?'.$_SERVER['QUERY_STRING'], $this->getPageCount(), $this->getPageNumber()))->display();
    }
}