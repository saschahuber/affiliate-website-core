<?php

namespace saschahuber\affiliatewebsitecore\component;

use saschahuber\affiliatewebsitecore\helper\AffiliateWebsiteEnvHelper;
use saschahuber\saastemplatecore\component\JsButton;

class EditorPreviewButton extends JsButton {
    public function __construct($type, $item_id, $title_input_id, $content_container_before_id, $separator_content=null, $content_container_after_id=null, $id=null){
        parent::__construct('<i class="fas fa-eye"></i> Vorschau', null, $id);
        $this->type = $type;
        $this->item_id = $item_id;
        $this->title_input_id = $title_input_id;
        $this->content_container_before_id = $content_container_before_id;
        $this->separator_content = $separator_content;
        $this->content_container_after_id = $content_container_after_id;
        $this->setFunctionCall($this->buildFunctionCall());
    }

    public function buildFunctionCall(){
        return "preparePreviewContent(this, '{$this->type}', ".($this->item_id?:"null").", '{$this->title_input_id}', '{$this->content_container_before_id}', '".base64_encode($this->separator_content)."', '{$this->content_container_after_id}')";
    }

    static function getJsFiles()
    {
        return AffiliateWebsiteEnvHelper::getLibPath() . '/resources/component/js/EditorPreviewButton.js';
    }
}