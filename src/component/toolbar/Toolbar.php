<?php

namespace saschahuber\affiliatewebsitecore\component\toolbar;

use saschahuber\affiliatewebsitecore\component\toolbar\ImageToolbar;
use saschahuber\affiliatewebsitecore\component\toolbar\ShortcodeToolbar;

class Toolbar{
    public function __construct(){}

    public function display(){
        ob_start();
        ?>
        <div style="display: inline-block;">
            <?=(new ImageToolbar())->display()?>
            <?=(new ShortcodeToolbar())->display()?>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        echo $content;
    }
}