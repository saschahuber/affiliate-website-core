<?php

namespace saschahuber\affiliatewebsitecore\component\toolbar;

use saschahuber\affiliatewebsitecore\manager\LayoutManager;
use saschahuber\affiliatewebsitecore\manager\UploadManager;

class UploadToolbar{
    public function __construct(){
        $this->upload_manager = new UploadManager();
        $this->layout_manager = new LayoutManager();
    }

    private function displayImageSelectionHtml(){
        global $CONFIG;

        $image_contents = array();
        foreach($this->upload_manager->getUploads() as $upload){
            ob_start();

            $upload_data = array(
                'id' => $upload->id,
                'src' => $CONFIG->website_domain . '/data/media' . $upload->file_path,
                'title' => $upload->title,
                'alttext' => $upload->alt_text,
            );

            ?>
            <div class="media-grid-item">
                <a onclick="selectImage('<?=base64_encode(json_encode($upload_data))?>')">
                    <img src="<?=$CONFIG->website_domain?>/<?=$upload->src?>">
                </a>
            </div>
            <?php
            $image_content = ob_get_contents();
            ob_end_clean();
            $image_contents[] = $image_content;
        }

        echo $this->layout_manager->grid($image_contents, 6, "sm");
    }

    private function getImageConfigurationHtml(){
        ob_start();
        ?>
            <button class="btn btn-md btn-primary" onclick="backToImageSelection()">Zurück zur Bilderauswahl</button>
            <div>
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            <label>Title-Attribut:</label>
                            <input name="image_title" type="text">
                        </p>
                        <p>
                            <label>Alt-Text:</label>
                            <textarea name="image_alt_text" cols="50"></textarea>
                        </p>
                        <p>
                            <label>Caption:</label>
                            <textarea name="image_caption" cols="50"></textarea>
                        </p>
                        <p>
                            <label>Breite:</label>
                            <input type="number" min="0" step="1" name="image_width">
                        </p>
                        <p>
                            <label>Höhe:</label>
                            <input type="number" min="0" step="1" name="image_height">
                        </p>
                        <p>
                            <label>Align (left, right, center):</label>
                            <input type="text" name="image_align">
                        </p>

                        <div class="shortcode_code row">
                            <div class="col-lg-3">
                                <button onclick="copyImageShortcodeToClipboard()"
                                        class="btn btn-secondary">Kopieren</button>
                            </div>
                            <div class="col-lg-9">
                                <div id="image_shortcode"></div>
                            </div>
                        </div>

                        <button class="btn btn-md btn-primary" onclick="showGeneratedImageShortcode()">Code generieren</button>
                    </div>
                    <div class="col-md-6">
                        <p>
                            <img id="configure-image-container-preview">
                        </p>
                    </div>
                </div>
            </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getHtml(){
        ob_start();
        ?>
        <div>
            <div id="select-image-container" class="selected">
                <?php $this->displayImageSelectionHtml(); ?>
            </div>
            <div id="configure-image-container">
                <?=$this->getImageConfigurationHtml()?>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function display(){
        $code = $this->getHtml()
            . $this->getStylesheet()
            . $this->getScript();

        ob_start();
        ?>
        <div id="image-toolbar-popup-container" class="hidden">
            <div id="image-toolbar-popup">
                <div class="close-button">
                    <i style="font-size: 38px;" class="fa fa-times-circle"
                       onclick="setImageToolbarPopupVisibility(false)"></i>
                </div>
                <h2>Image Manager</h2>
                <?=$code?>
            </div>
        </div>
        <button onclick="setImageToolbarPopupVisibility(true)" class="btn btn-primary">Bilder</button>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getScript(){
        ob_start();
        ?>
        <script>
            <?=file_get_contents(PUBLIC_DIR . '/js/image_toolbar.js')?>
            resetImageToolbar();
        </script>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function getStylesheet(){
        ob_start();
        ?>
        <style>
            <?=file_get_contents(PUBLIC_DIR . '/css/image_toolbar.css')?>
        </style>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}