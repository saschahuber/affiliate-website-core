<?php

namespace saschahuber\affiliatewebsitecore\component\toolbar;

class ShortcodeToolbar{
    public function __construct(){
        global $shortcode_docs;
        $this->docs = $shortcode_docs;
    }

    public function get_shortcode_manager_html(){
        ob_start();

        ?>
        <div class="row">
            <div class="col-lg-2">
                <?php foreach($this->docs as $shortcode => $shortcode_doc): ?>
                    <button class="btn btn-block btn-sm btn-primary shortcode-toolbar-nav-button" onclick="selectShortcode('<?=$shortcode?>')"><?=$shortcode?></button>
                <?php endforeach; ?>
            </div>
            <div class="col-lg-10">
                <?php foreach($this->docs as $shortcode => $shortcode_doc): ?>
                    <div class="shortcode-settings" id="shortcode_<?=$shortcode?>">
                        <h2><?=$shortcode_doc['description']?></h2>
                        <div>
                            <div>
                                <?php foreach($shortcode_doc['params'] as $param): ?>
                                    <div class="form-group">
                                        <label for="<?=$shortcode?>_<?=$param['param']?>"><?=$param['description']?>
                                            (Parameter: <?=$param['param']?>)</label>
                                        <input type="text" class="form-control" id="<?=$shortcode?>_<?=$param['param']?>"
                                               name="<?=$shortcode?>_<?=$param['param']?>"
                                               placeholder="<?=$param['default']?>"
                                               onchange="showGeneratedShortcode('<?=$shortcode?>')">
                                    </div>
                                <?php endforeach; ?>
                                <?php if(isset($shortcode_doc['has_content']) && $shortcode_doc['has_content'] == true): ?>
                                    <div class="form-group">
                                        <label for="content_<?=$shortcode?>">Inhalt des Shortcodes</label>
                                        <input type="text" class="form-control" id="content_<?=$shortcode?>"
                                               name="content_<?=$shortcode?>"
                                               onchange="showGeneratedShortcode('<?=$shortcode?>')">
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div class="shortcode_code row">
                                <div class="col-lg-3">
                                    <button onclick="copyShortcodeToClipboard('<?=$shortcode?>')"
                                            class="btn btn-secondary">Code kopieren</button>
                                </div>
                                <div class="col-lg-9">
                                    <div id="shortcode_code_<?=$shortcode?>"></div>
                                </div>
                            </div>
                            <div class="shortcode_preview">
                                <div id="shortcode_preview_<?=$shortcode?>"></div>
                            </div>

                            <div class="buttons-container">
                                <p>
                                    <button onclick="createShortcodePreview('<?=$shortcode?>')"
                                            class="btn btn-primary">Vorschau generieren</button>
                                    <button onclick="showGeneratedShortcode('<?=$shortcode?>')"
                                            class="btn btn-primary">Code generieren</button>
                                    <button onclick="insertShortcodeToEditor('<?=$shortcode?>')"
                                            class="btn btn-primary">Code in Editor einfügen</button>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function display(){
        $code = $this->get_shortcode_manager_html()
            . $this->get_shortcode_manager_stylesheet()
            . $this->get_shortcode_manager_script();

        ob_start();
        ?>
        <div id="shortcode-toolbar-popup-container" class="hidden">
            <div id="shortcode-toolbar-popup">
                <div class="close-button">
                    <i style="font-size: 38px;" class="fa fa-times-circle"
                       onclick="setShortcodeToolbarPopupVisibility(false)"></i>
                </div>
                <h2>Shortcode Manager</h2>
                <?=$code?>
            </div>
        </div>
        <button onclick="setShortcodeToolbarPopupVisibility(true)" class="btn btn-primary">Shortcodes</button>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function get_shortcode_manager_script(){
        ob_start();
        ?>
        <script>
            let firstShortcode = '<?=array_keys($this->docs)[0]?>';
            let shortcodeDocs = <?=json_encode($this->docs)?>;
            <?=file_get_contents(PUBLIC_DIR . '/js/shortcode_toolbar.js')?>
            resetShortcodeToolbar();
        </script>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    function get_shortcode_manager_stylesheet(){
        ob_start();
        ?>
        <style>
            <?=file_get_contents(PUBLIC_DIR . '/css/shortcode_toolbar.css')?>
        </style>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}