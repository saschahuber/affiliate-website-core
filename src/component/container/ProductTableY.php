<?php

namespace saschahuber\affiliatewebsitecore\component\container;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;

class ProductTableY extends Component
{
    private $product_manager, $products, $show_detail_button, $show_reduced_badge, $show_price;

    public function __construct($product_manager, $products, $show_detail_button, $show_reduced_badge, $show_price, $id = null)
    {
        $this->product_manager = $product_manager;
        $this->products = $products;
        $this->show_detail_button = $show_detail_button;
        $this->show_reduced_badge = $show_reduced_badge;
        $this->show_price = $show_price;

        parent::__construct($id);
    }


    protected function build()
    {
        list($product_data_map, $data_product_map, $product_data_id_count) = $this->product_manager->getProductDataMap($this->products);

        #die(json_encode($product_data_map));

        $data_fields = [];
        foreach ($data_product_map as $data_field_id => $product_data_fields) {
            foreach ($product_data_fields as $product_id => $product_data_field) {
                $data_fields[$data_field_id] = $product_data_field;
            }
        }

        ksort($data_fields);

        $column_labels = [null];
        foreach ($data_fields as $id => $data_field) {
            $column_labels[] = $data_field->field_name;
        }

        $rows = [];
        foreach ($this->products as $product) {
            //Todo Felder in Vergleich ausblenden, wenn zu wenige Produkte sie gefüllt haben
            #if($product_data_id_count[$id]){
            #
            #}

            $cells = [];

            ob_start();
            echo $this->product_manager->getProductImageTag($product);
            BreakComponent::break();
            ?>
            <div style="min-width: 200px; max-width: 300px; text-align: center; margin: auto;">
                <span class="product-title"><?= $product->title ?></span>
            </div>
            <?php
            #if($product->is_sponsored) {
            #    (new SponsoredBadge())->display();
            #}
            #BreakComponent::break();
            BreakComponent::break();
            ?>

            <div style="max-width: 300px; text-align: center; margin: auto;">
                <?= ShortcodeHelper::doShortcode($this->product_manager->getProductBuyButton($product)) ?>
                <br>
                <?= ShortcodeHelper::doShortcode($this->product_manager->getProductDetailButton($product)) ?>
            </div>
            <?php
            $cells[] = ob_get_clean();

            foreach ($data_fields as $id => $product_data_field) {
                $field_type = $product_data_field->field_type;

                switch ($field_type) {
                    case "boolean":
                        $field_value = $data_product_map[$id][$product->id]->value;
                        if ($field_value === "1") {
                            $field_value = '<i class="fas fa-check-circle" style="color: #7ab317;"></i>';
                        } else {
                            $field_value = '<i class="fas fa-times-circle" style="color: #c01313;"></i>';
                        }
                        break;
                    case "float":
                        $field_value = str_replace('.', ',', $data_product_map[$id][$product->id]->value);
                        break;
                    case "string":
                    case "enum":
                    default:
                        $field_value = $data_product_map[$id][$product->id]->value;
                        break;
                }

                $field_content = '-';

                if ($field_value) {
                    $prefix = $product_data_field->field_prefix;
                    $suffix = $product_data_field->field_suffix;

                    $field_content = ($prefix ?: '') . $field_value . ($suffix ?: '');
                }

                $cells[] = $field_content;
            }
            $rows[] = new TableRow($cells);
        }

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }

    /**
     * @return mixed
     */
    public function getProductManager()
    {
        return $this->product_manager;
    }

    /**
     * @param mixed $product_manager
     */
    public function setProductManager($product_manager): void
    {
        $this->product_manager = $product_manager;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getShowDetailButton()
    {
        return $this->show_detail_button;
    }

    /**
     * @param mixed $show_detail_button
     */
    public function setShowDetailButton($show_detail_button): void
    {
        $this->show_detail_button = $show_detail_button;
    }

    /**
     * @return mixed
     */
    public function getShowReducedBadge()
    {
        return $this->show_reduced_badge;
    }

    /**
     * @param mixed $show_reduced_badge
     */
    public function setShowReducedBadge($show_reduced_badge): void
    {
        $this->show_reduced_badge = $show_reduced_badge;
    }

    /**
     * @return mixed
     */
    public function getShowPrice()
    {
        return $this->show_price;
    }

    /**
     * @param mixed $show_price
     */
    public function setShowPrice($show_price): void
    {
        $this->show_price = $show_price;
    }
}