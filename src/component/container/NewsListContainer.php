<?php

namespace saschahuber\affiliatewebsitecore\component\container;

use saschahuber\affiliatewebsitecore\component\item\NewsListItem;
use saschahuber\saastemplatecore\component\container\GridContainer;

class NewsListContainer extends GridContainer
{
    public function __construct($posts, $column_count = 1, $size = GridContainer::SIZE_LG, $additional_classes = ['post-grid'], $row_classes = [], $id = null)
    {
        parent::__construct($this->getNewsListItems($posts), $column_count, $size, $additional_classes, $row_classes, $id);
    }

    private function getNewsListItems($posts)
    {
        $post_grid_items = [];

        foreach ($posts as $post) {
            $post_grid_items[] = new NewsListItem($post);
        }

        return $post_grid_items;
    }
}