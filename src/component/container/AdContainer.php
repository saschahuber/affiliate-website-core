<?php

namespace saschahuber\affiliatewebsitecore\component\container;

use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\saastemplatecore\component\container\Container;

class AdContainer extends Container {
    const AD_PRELOAD_SCROLL_DISTANCE = 250;

    private $position, $content_type, $content_id;

    /**
     * @param $position
     * @param $content_type
     * @param $content_id
     */
    public function __construct($position, $content_type, $content_id, $id = null)
    {
        $this->position = $position;
        $this->content_type = $content_type;
        $this->content_id = $content_id;
        parent::__construct($id);
    }

    protected function build()
    {
        $min_display_width = $this->getMinDisplayWidth();

        ?>
        <div class="position-<?=$this->getPosition()?>" id="<?=$this->getId()?>">
            <div class="ad-container">
                <script>
                    let loaded_<?=$this->getId()?> = false;

                    function load_<?=$this->getId()?>(force = false){
                        if(!loaded_<?=$this->getId()?> && (force || isElementInViewport(document.getElementById('<?=$this->getId()?>'), <?=self::AD_PRELOAD_SCROLL_DISTANCE?>))){
                            loadAds('<?=$this->getId()?>', '<?=$this->getPosition()?>', '<?=$this->getContentType()?>', '<?=$this->getContentId()?>', <?=$min_display_width?:'null'?>);
                            loaded_<?=$this->getId()?> = true;
                        }
                    }

                    window.addEventListener('load', function () {
                        load_<?=$this->getId()?>(true);

                        /*
                        window.addEventListener("scroll", function() {
                            load_<?=$this->getId()?>();
                        });
                        */
                    });
                </script>
            </div>
        </div>
        <?php
    }

    function getMinDisplayWidth(){
        switch($this->getPosition()){
            case AdManager::AD_TYPE_SIDEBAR_LEFT:
            case AdManager::AD_TYPE_SIDEBAR_RIGHT:
                return 1600;
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position): void
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->content_type;
    }

    /**
     * @param mixed $content_type
     */
    public function setContentType($content_type): void
    {
        $this->content_type = $content_type;
    }

    /**
     * @return mixed
     */
    public function getContentId()
    {
        return $this->content_id;
    }

    /**
     * @param mixed $content_id
     */
    public function setContentId($content_id): void
    {
        $this->content_id = $content_id;
    }
}