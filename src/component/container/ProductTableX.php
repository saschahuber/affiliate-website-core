<?php

namespace saschahuber\affiliatewebsitecore\component\container;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\table\ElevatedTable;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\NumberFormatHelper;

class ProductTableX extends Component
{
    const LABEL_COLUMN_WIDTH = 300;
    const PRODUCT_COLUMN_SIZE = 300;

    private $product_manager, $products, $show_detail_button, $show_reduced_badge, $show_price;

    public function __construct($product_manager, $products, $show_detail_button, $show_reduced_badge, $show_price, $id = null)
    {
        $this->product_manager = $product_manager;
        $this->products = $products;
        $this->show_detail_button = $show_detail_button;
        $this->show_reduced_badge = $show_reduced_badge;
        $this->show_price = $show_price;

        parent::__construct($id);
    }


    protected function build()
    {
        list($product_data_map, $data_product_map, $product_data_id_count) = $this->product_manager->getProductDataMap($this->products);

        #die(json_encode($product_data_map));

        $data_fields = [];
        foreach ($data_product_map as $data_field_id => $product_data_fields) {
            foreach ($product_data_fields as $product_id => $product_data_field) {
                $data_fields[$data_field_id] = $product_data_field;
            }
        }

        ksort($data_fields);

        $rows = [];

        $column_labels = [null];
        $buy_buttons = [null];
        foreach ($this->products as $product) {
            ob_start();
            echo $this->product_manager->getProductImageTag($product);
            BreakComponent::break();
            BreakComponent::break();
            ?>
            <div style="min-width: 200px; max-width: 300px; text-align: center; margin: auto;">
                <span class="product-title"><?= $product->title ?></span>
            </div>
            <?php

            #if($product->is_sponsored){
            #    (new SponsoredBadge())->display();
            #}

            $column_labels[] = ob_get_clean();

            ob_start();
            ?>
            <div style="max-width: 300px; text-align: center; margin: auto;">
                <?php if ($this->show_price && $product->product_links[0]->is_available): ?>
                    <div>
                        <?php if ($product->product_links[0]->reduced_price): ?>
                            <p>
                                <del><?= NumberFormatHelper::currency($product->product_links[0]->price) ?></del>&nbsp;
                                <strong style="color: red;"><?= NumberFormatHelper::currency($product->product_links[0]->reduced_price) ?></strong>
                            </p>
                        <?php else: ?>
                            <p><?= NumberFormatHelper::currency($product->product_links[0]->price) ?></p>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <?= ShortcodeHelper::doShortcode($this->product_manager->getProductBuyButtonsList($product)) ?>
                <br>
                <?= ShortcodeHelper::doShortcode($this->product_manager->getProductDetailButton($product)) ?>
            </div>
            <?php
            $buy_buttons[] = ob_get_clean();
        }

        foreach ($data_fields as $id => $product_data_field) {
            //Todo Felder in Vergleich ausblenden, wenn zu wenige Produkte sie gefüllt haben
            #if($product_data_id_count[$id]){
            #
            #}

            $cells = [$product_data_field->field_name];

            foreach ($this->products as $product) {
                $field_type = $product_data_field->field_type;

                $data_field = ArrayHelper::getArrayValue($data_product_map, $id, null);

                $product_field = ArrayHelper::getArrayValue($data_field, $product->id, null);

                $field_value = null;

                if ($product_field !== null && isset($product_field->value)) {
                    $field_value = $product_field->value;
                    switch ($field_type) {
                        case "boolean":
                            if ($field_value === "1") {
                                $field_value = '<i class="fas fa-check-circle" style="color: #7ab317;"></i>';
                            } else {
                                $field_value = '<i class="fas fa-times-circle" style="color: #c01313;"></i>';
                            }
                            break;
                        case "float":
                            $field_value = str_replace('.', ',', $field_value);
                            break;
                        case "string":
                        case "enum":
                        default:
                            //Just display $field_value as string
                            break;
                    }
                }

                $field_content = '-';

                if ($field_value) {
                    $prefix = $product_data_field->field_prefix;
                    $suffix = $product_data_field->field_suffix;

                    $field_content = ($prefix ?: '') . $field_value . ($suffix ?: '');
                }

                $cells[] = $field_content;
            }
            $rows[] = new TableRow($cells);
        }
        $rows[] = new TableRow($buy_buttons);

        $width = self::LABEL_COLUMN_WIDTH + count($this->products) * self::PRODUCT_COLUMN_SIZE;

        (new ElevatedTable($column_labels, $rows, ['table-bordered', 'table-hover', 'centered'], $width))->display();
    }

    /**
     * @return mixed
     */
    public function getProductManager()
    {
        return $this->product_manager;
    }

    /**
     * @param mixed $product_manager
     */
    public function setProductManager($product_manager): void
    {
        $this->product_manager = $product_manager;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getShowDetailButton()
    {
        return $this->show_detail_button;
    }

    /**
     * @param mixed $show_detail_button
     */
    public function setShowDetailButton($show_detail_button): void
    {
        $this->show_detail_button = $show_detail_button;
    }

    /**
     * @return mixed
     */
    public function getShowReducedBadge()
    {
        return $this->show_reduced_badge;
    }

    /**
     * @param mixed $show_reduced_badge
     */
    public function setShowReducedBadge($show_reduced_badge): void
    {
        $this->show_reduced_badge = $show_reduced_badge;
    }

    /**
     * @return mixed
     */
    public function getShowPrice()
    {
        return $this->show_price;
    }

    /**
     * @param mixed $show_price
     */
    public function setShowPrice($show_price): void
    {
        $this->show_price = $show_price;
    }
}