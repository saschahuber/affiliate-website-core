<?php

namespace saschahuber\affiliatewebsitecore\component\container;

use saschahuber\affiliatewebsitecore\component\form\ProductFilter;
use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\saastemplatecore\component\Column;
use saschahuber\saastemplatecore\component\Component;
use saschahuber\saastemplatecore\component\container\RowContainer;
use saschahuber\saastemplatecore\component\Row;

class ProductContainer extends Component
{
    protected $products, $layout, $show_review_score, $show_filter, $pre_filter_params, $column_count;

    public function __construct($products, $show_filter = false, $pre_filter_params = [], $layout = "grid", $show_review_score = false, $column_count = 4, $id = null)
    {
        $this->setProducts($products);
        $this->setLayout($layout);
        $this->setColumnCount($column_count);
        $this->setShowReviewScore($show_review_score);
        $this->setShowFilter($show_filter);
        $this->setPreFilterParams($pre_filter_params);
        parent::__construct($id);
    }

    protected function build()
    {
        $product_manager = AffiliateInterfacesHelper::getProductManager();

        $product_items = [];
        foreach ($this->getProducts() as $product) {
            $product_items[] = $product_manager->displayProduct($product, null, null, $this->getLayout(), true, $this->getShowReviewScore());
        }

        $ad_manager = new AdManager();

        $columns = [];
        foreach ($product_items as $product_item) {
            $columns[] = new Column($product_item, ['col-12', 'col-md-6', 'col-lg-3']);
        }

        $columns = $ad_manager->mixAdsInColumns($columns, AdManager::AD_TYPE_PRODUCT_GRID_WIDE, ProductManager::TYPE_PRODUCT_CATEGORY, [], null, 16, true);

        $this->rows_to_display = [new Row($columns)];

        #$rows = RowHelper::groupItemsInRows($product_items, $this->getColumnCount(), GridContainer::SIZE_LG, ['col-md-6']);
        #$this->rows_to_display = $ad_manager->mixAdsInContent($rows, AdManager::AD_TYPE_PRODUCT_GRID_WIDE, ProductManager::TABLE_NAME, [], null, 4, true);

        $items = [];

        if ($this->getShowFilter()) {
            $items[] = new ProductFilter($this->getPreFilterParams());
        }

        $items[] = new RowContainer($this->rows_to_display, ['product-grid']);

        (new RowContainer($items))->display();
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): void
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getColumnCount()
    {
        return $this->column_count;
    }

    /**
     * @param mixed $column_count
     */
    public function setColumnCount($column_count): void
    {
        $this->column_count = $column_count;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout): void
    {
        $this->layout = $layout;
    }

    /**
     * @return mixed
     */
    public function getShowReviewScore()
    {
        return $this->show_review_score;
    }

    /**
     * @param mixed $show_review_score
     */
    public function setShowReviewScore($show_review_score): void
    {
        $this->show_review_score = $show_review_score;
    }

    /**
     * @return mixed
     */
    public function getShowFilter()
    {
        return $this->show_filter;
    }

    /**
     * @param mixed $show_filter
     */
    public function setShowFilter($show_filter): void
    {
        $this->show_filter = $show_filter;
    }

    /**
     * @return mixed
     */
    public function getPreFilterParams()
    {
        return $this->pre_filter_params;
    }

    /**
     * @param mixed $pre_filter_params
     */
    public function setPreFilterParams($pre_filter_params): void
    {
        $this->pre_filter_params = $pre_filter_params;
    }
}