<?php

namespace saschahuber\affiliatewebsitecore\component\container;

use saschahuber\affiliatewebsitecore\component\item\PostGridItem;
use saschahuber\saastemplatecore\component\container\GridContainer;
use saschahuber\saastemplatecore\component\container\RowContainer;
use saschahuber\saastemplatecore\helper\RowHelper;

class PostGridContainer extends RowContainer
{
    const AD_EVERY_X_ROWS = 4;

    public function __construct($posts, $column_count = 3, $size = GridContainer::SIZE_LG, $additional_classes = ['post-grid'], $row_classes = [], $id = null)
    {
        $this->column_count = $column_count;
        $this->size = $size;
        parent::__construct($this->getPostGridItems($posts), $additional_classes, $row_classes, $id);
    }

    private function getPostGridItems($posts)
    {
        $post_items = [];
        foreach ($posts as $post) {
            $post_items[] = new PostGridItem($post);
        }

        $rows = RowHelper::groupItemsInRows($post_items, $this->column_count, $this->size);

        #$items_to_insert = [new Row([new Column("Mein Element")])];
        #$rows = ArrayHelper::insertItemsDistributed($rows, $items_to_insert, self::AD_EVERY_X_ROWS);

        return $rows;
    }
}