<?php

namespace saschahuber\affiliatewebsitecore\component\container;

use saschahuber\affiliatewebsitecore\component\item\PostListItem;
use saschahuber\saastemplatecore\component\container\GridContainer;

class PostListContainer extends GridContainer
{
    public function __construct($posts, $column_count = 1, $size = GridContainer::SIZE_LG, $additional_classes = ['post-grid'], $row_classes = [], $id = null)
    {
        parent::__construct($this->getPostListItems($posts), $column_count, $size, $additional_classes, $row_classes, $id);
    }

    private function getPostListItems($posts)
    {
        $post_grid_items = [];

        foreach ($posts as $post) {
            $post_grid_items[] = new PostListItem($post);
        }

        return $post_grid_items;
    }
}