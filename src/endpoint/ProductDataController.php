<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\AiProductDataLoaderService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class ProductDataController extends AdminRestController {

    /**
     * "/product_data/loadProductDataWithAi" Endpoint
     */
    public function loadProductDataWithAi($segments=null, $params=null){
        $product_id = ArrayHelper::getArrayValue($params, 'product_id');
        $model = ArrayHelper::getArrayValue($params, 'model');
        $field_data = json_decode(ArrayHelper::getArrayValue($params, 'field_data'), true);

        $ai_product_data_loader_service = new AiProductDataLoaderService();
        $ai_product_data_loader_service->displayAiLoadedProductDataSettings($product_id, $field_data, $model);
    }

    protected function getMethodPostMappings(){
        return [
            'loadProductDataWithAi' => 'loadProductDataWithAi'
        ];
    }
}