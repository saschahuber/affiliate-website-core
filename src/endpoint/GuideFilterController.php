<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use Exception;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\service\AdService;
use saschahuber\affiliatewebsitecore\service\AdTrackingService;
use saschahuber\affiliatewebsitecore\service\GuideFilterService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\RestController;
use stdClass;

class GuideFilterController extends AdminRestController
{
    /**
     * "/guide-filter/save" Endpoint
     */
    public function saveGuideFilter($segments = null, $params = null){
        $guide_filter_service = new GuideFilterService();

        $item_id = RequestHelper::reqint('guide_filter_id');

        $new_item = false;
        $guide_filter = $guide_filter_service->getGuideFilter($item_id);

        if($guide_filter === null){
            $new_item = true;
            $guide_filter = new stdClass();
        }

        $guide_filter->title = RequestHelper::reqstr('title');
        $guide_filter->result_type = RequestHelper::reqstr('result_type');
        $guide_filter->pre_filter_category_ids = RequestHelper::reqstr('pre_filter_category_ids');

        echo $guide_filter_service->saveGuideFilter($guide_filter);
    }

    /**
     * "/guide-filter/step/save" Endpoint
     */
    public function saveGuideFilterStep($segments = null, $params = null){
        $guide_filter_service = new GuideFilterService();

        $item_id = RequestHelper::reqint('guide_step_id');

        $new_item = false;
        $guide_filter_step = $guide_filter_service->getGuideFilterStep($item_id);

        if($guide_filter_step === null){
            $new_item = true;
            $guide_filter_step = new stdClass();
            $guide_filter_step->guide_filter_id = RequestHelper::reqint('guide_filter_id');
        }

        $guide_filter_step->title = RequestHelper::reqstr('title');
        $guide_filter_step->description = RequestHelper::reqstr('description');
        $guide_filter_step->filter_progress_text = RequestHelper::reqstr('filter_progress_text');
        $guide_filter_step->multiple_selections = RequestHelper::reqbool('multiple_selections');
        $guide_filter_step->extend_results = RequestHelper::reqbool('extend_results');
        $guide_filter_step->skippable = RequestHelper::reqbool('skippable');
        $guide_filter_step->step_order = RequestHelper::reqint('step_order');
        $guide_filter_step->active = RequestHelper::reqbool('active');

        echo $guide_filter_service->saveGuideFilterStep($guide_filter_step);
    }

    /**
     * "/guide-filter/step/option/save" Endpoint
     */
    public function saveGuideFilterOption($segments = null, $params = null){
        $guide_filter_service = new GuideFilterService();

        $item_id = RequestHelper::reqint('guide_step_option_id');

        $new_item = false;
        $guide_filter_step_option = $guide_filter_service->getGuideFilterOption($item_id);


        if($guide_filter_step_option === null){
            $new_item = true;
            $guide_filter_step_option = new stdClass();
            $guide_filter_step_option->guide_filter__step_id = RequestHelper::reqint('guide_step_id');
        }

        $guide_filter_step_option->img = RequestHelper::reqstr('img');
        $guide_filter_step_option->icon = RequestHelper::reqstr('icon');
        $guide_filter_step_option->label = RequestHelper::reqstr('label');
        $guide_filter_step_option->and_connections = RequestHelper::reqstr('and_connections');
        $guide_filter_step_option->option_order = RequestHelper::reqint('option_order');
        $guide_filter_step_option->active = RequestHelper::reqbool('active');

        echo $guide_filter_service->saveGuideFilterOption($guide_filter_step_option);
    }

    /**
     * "/guide-filter/step/option/filter/save" Endpoint
     */
    public function saveGuideFilterOptionFilter($segments = null, $params = null){
        $guide_filter_service = new GuideFilterService();

        $item_id = RequestHelper::reqint('guide_step_id');

        $new_item = false;
        $guide_filter_step_option_filter = $guide_filter_service->getGuideFilterOptionFilter($item_id);

        if($guide_filter_step_option_filter === null){
            $new_item = true;
            $guide_filter_step_option_filter = new stdClass();
            $guide_filter_step_option_filter->guide_filter__step_option_id = RequestHelper::reqint('guide_step_option_id');
        }

        $guide_filter_step_option_filter->filter_type = RequestHelper::reqstr('filter_type');
        $guide_filter_step_option_filter->filter_key = RequestHelper::reqstr('filter_key');
        $guide_filter_step_option_filter->filter_comparator = RequestHelper::reqstr('filter_comparator');
        $guide_filter_step_option_filter->filter_value = RequestHelper::reqstr('filter_value');

        echo $guide_filter_service->saveGuideFilterOptionFilter($guide_filter_step_option_filter);
    }

    protected function getMethodPostMappings()
    {
        return [
            'save' => 'saveGuideFilter',
            'step/save' => 'saveGuideFilterStep',
            'step/option/save' => 'saveGuideFilterOption',
            'step/option/filter/save' => 'saveGuideFilterOptionFilter',
        ];
    }
}