<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\saastemplatecore\helper\ApiAuthHelper;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\manager\PushNotificationManager;
use saschahuber\saastemplatecore\rest_controller\RestController;
use saschahuber\saastemplatecore\service\PushNotificationService;
use stdClass;

class PushNotificationController extends RestController
{
    /**
     * "/push_notification/save" Endpoint
     */
    public function savePushNotification($segments = null, $params = null){
        ApiAuthHelper::authenticate();
        if(!AuthHelper::isAdminPanelUser()){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_NOT_AUTHORIZED, 'Kein Admin-Benutzer');
        }

        $push_manager = new PushNotificationManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $push_manager->getById($item_id);

        if(!$item){
            $new_item = true;
            $item = new stdClass();
        }

        $item->subject = RequestHelper::reqstr('subject');
        $item->message = RequestHelper::reqstr('message');
        $item->image_url = RequestHelper::reqstr('image_url');
        $item->click_url = RequestHelper::reqstr('click_url');

        $item->linked_element_type = RequestHelper::reqstr('linked_element_type');
        $item->linked_element_id = RequestHelper::reqstr('linked_element_id');

        $item->scheduled_time = RequestHelper::reqstr('scheduled_time');

        if($new_item){
            $item->id = $push_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $push_manager->updateItem($item);
        }

        echo $item_id;
    }

    /**
     * "/push_notification/subscribe" Endpoint
     */
    public function subscribe($segments = null, $params = null)
    {
        $type = ArrayHelper::getArrayValue($params, 'push_type');
        $trigger = ArrayHelper::getArrayValue($params, 'push_trigger');
        $endpoint = ArrayHelper::getArrayValue($params, 'push_endpoint');
        $push_key_p256dh = ArrayHelper::getArrayValue($params, 'push_key_p256dh');
        $push_key_auth = ArrayHelper::getArrayValue($params, 'push_key_auth');

        $push_service = new PushNotificationService();
        $push_service->addSubscription($type, $endpoint, $trigger, $push_key_p256dh, $push_key_auth);
    }

    protected function getMethodPostMappings()
    {
        return [
            'save' => 'savePushNotification',
            'subscribe' => 'subscribe'
        ];
    }
}