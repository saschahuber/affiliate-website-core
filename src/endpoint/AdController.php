<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use Exception;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\service\AdService;
use saschahuber\affiliatewebsitecore\service\AdTrackingService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\RestController;
use stdClass;

#[AllowDynamicProperties]
class AdController extends RestController
{

    /**
     * "/ads/get_fitting" Endpoint
     */
    public function getFittingAds($segments = null, $params = null)
    {
        global $CONFIG;

        AuthHelper::getUserManager()->setup();

        $position = ArrayHelper::getArrayValue($params, 'position');
        $content_type = ArrayHelper::getArrayValue($params, 'content_type');
        $content_id = ArrayHelper::getArrayValue($params, 'content_id');
        $fingerprint = ArrayHelper::getArrayValue($params, 'fingerprint');
        $source = ArrayHelper::getArrayValue($params, 'source');
        $session_id = ArrayHelper::getArrayValue($params, 'session_id');

        $ad_tracking_service = new AdTrackingService();

        $success = true;
        $ad_items = [];
        try{
            $ad_service = new AdService();
            $ad_manager = new AdManager();
            $items = $ad_service->getFittingAds($position, $content_type, $content_id);

            foreach(array_filter($items) as $ad_item){
                $params = [
                    'fingerprint' => $fingerprint,
                    'source' => $source,
                    'session_id' => $session_id,
                    'user_ip' => RequestHelper::getClientIp()
                ];

                $ad_tracking_service->trackAdImpression($ad_item->id, $params);
                $ad_items[] = $ad_manager->getAdContent($ad_item);
            }
        }
        catch(Exception $e){
            $success = false;
        }

        if(($ad_items === null || count($ad_items) < 1) && !$CONFIG->is_prod){
            $ad_items = [
                BufferHelper::buffered(function() use ($position){
                    ?>
                    <div class="centered" style="background-color: #aaaaaa; border: 3px solid #000; padding: 100px 10px;">
                        <h3 style="color: #fff; margin-top: 0;">Werbung <?=$position?></h3>
                        <span style="color: #fff;">(Nur für Admins sichtbar)</span>
                    </div>
                    <?php
                })
            ];
        }

        $this->jsonResponse([
            'success' => $success,
            'items' => $ad_items
        ]);
    }

    /**
     * "/ads/save" Endpoint
     */
    public function saveAd($segments = null, $params = null){
        AdminRestController::authenticateOrFail();

        $new_item = false;

        $ad_manager = new AdManager();

        $item_id = RequestHelper::reqint('ad_id');

        $item = $ad_manager->getAdById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
        }

        $item->title = RequestHelper::reqstr('title');

        $item->content = RequestHelper::reqstr('content');

        $item->ads_txt = RequestHelper::reqstr('ads_txt');

        $item->type = RequestHelper::reqstr('type');

        $item->active = RequestHelper::reqbool('active');

        $item->start_date = RequestHelper::reqstr('start_date');

        $item->end_date = RequestHelper::reqstr('end_date');

        if($new_item){
            $item->id = $ad_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $ad_manager->updateItem($item);
        }

        echo $item_id;
    }

    protected function getMethodPostMappings()
    {
        return [
            'get_fitting' => 'getFittingAds',
            'save' => 'saveAd'
        ];
    }
}