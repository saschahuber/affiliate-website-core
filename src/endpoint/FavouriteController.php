<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\FavouriteService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;

#[AllowDynamicProperties]
class FavouriteController extends AuthenticatedRestController {

    /**
     * "/favourite/toggle" Endpoint
     */
    public function toggleFavourite($segments=null, $params=null){
        $fav_type = ArrayHelper::getArrayValue($params, 'favType');
        $fav_id = ArrayHelper::getArrayValue($params, 'favId');

        if(!AuthHelper::isLoggedIn()){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_NO_PERMISSION,
                'Du musst eingeloggt sein, um Favoriten speichern zu können.');
        }

        (new FavouriteService())->toggleFavourite(AuthHelper::getCurrentUserId(), $fav_type, $fav_id);
    }

    protected function getMethodPostMappings(){
        return [
            'toggle' => 'toggleFavourite'
        ];
    }
}