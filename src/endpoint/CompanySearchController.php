<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\CompanySearchService;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\rest_controller\RestController;

#[AllowDynamicProperties]
class CompanySearchController extends RestController {
    /**
     * "/company_search/do_search" Endpoint
     */
    public function searchCompanies($segments=null, $params=null){
        $company_search_service = new CompanySearchService();

        $keyword = ArrayHelper::getArrayValue($params, 'keyword', null);
        $location = ArrayHelper::getArrayValue($params, 'location', null);
        $radius = ArrayHelper::getArrayValue($params, 'radius', 25);
        $taxonomy = ArrayHelper::getArrayValue($params, 'taxonomy', null);

        $companies = $company_search_service->searchCompanies($keyword, $taxonomy, $location, $radius);

        if(count($companies) == 0){
            echo '
            <div class="no-search-results">
                <p><span><i class="fas fa-frown emoji"></i></span></p>
                <p><span>Für deine Suche wurden leider keine Suchergebnisse gefunden...</span></p>
                <p><span>Versuche es bitte noch einmal mit einem anderen Suchbegriff.</span></p>
            </div>
            ';
        }
        else {
            echo BufferHelper::buffered(function(){
                ?>
                <div style="padding: 5px;">
                    <?php
                    (new FloatContainer([new JsButton('Ergebnisse schließen', 'hideSearchResults()', ['centered'])]))->display();
                    ?>
                </div>
                <?php
            });

            $company_search_service->displayCompanies($companies);
        }
    }

    protected function getMethodPostMappings(){
        return [
            'do_search' => 'searchCompanies'
        ];
    }
}