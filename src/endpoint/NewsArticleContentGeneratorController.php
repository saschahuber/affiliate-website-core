<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\service\TextGenerationService;

class NewsArticleContentGeneratorController extends AdminRestController
{
    /**
     * "/news_article_generator/generate_title" Endpoint
     */
    public function generateTitle($segments = null, $params = null)
    {
        $current_title = base64_decode(ArrayHelper::getArrayValue($params, 'title'));

        $prompt = "Erstelle mir einen neuen Titel für meinen News-Artikel.\n"
            ."Der bisherige Titel lautet '$current_title'.\n"
            ."Gib mir bitte außer dem Titel nichts weiter zurück.\n"
            ."Der Titel sollte maximal 60 Zeichen lang sein.\n"
            ."Formuliere alles in der 'Du'-Form und versuche, dass der Titel nicht nach Clickbait aussieht.";

        $messages = [
            ['role' => 'system', 'content' => "Du bist ein SEO-Experte mit Fokus aus Content-Marketing. Deine Spezialität sind News-Artikel mit Titeln, die gerne geklickt werden."],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT_4, 2000);
        $result = (new TextGenerationService())->getNextChatMessage($template);

        echo trim(trim($result->getContent(), "\"'"));
    }

    /**
     * "/news_article_generator/generate_content" Endpoint
     */
    public function generateContent($segments = null, $params = null)
    {
        $content = base64_decode(ArrayHelper::getArrayValue($params, 'content'));
        $model = ArrayHelper::getArrayValue($params, 'model');
        $tokens = intval(ArrayHelper::getArrayValue($params, 'tokens', 4096));

        $prompt = "Bitte schreibe meinen News-Artikel um.\n"
            ."Formuliere alles in der 'Du'-Form.\n"
            ."Entferne DIVs, Bilder und alle Links aus dem HTML, sodass nur Text übrig bleibt.\n"
            ."Strukturiere den Text in HTML mit Überschriften und markiere wichtige Stellen fett.\n"
            ."Der Text sollte mindestens 750 Wörter haben.\n"
            ."Das ist der bisherige Inhalt:\n\n"
            ."$content";

        $messages = [
            ['role' => 'system', 'content' => "Du bist ein SEO-Experte mit Fokus aus Content-Marketing. Deine Spezialität ist die Erstellung von News-Artikeln."],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, $model, $tokens);
        $result = (new TextGenerationService())->getNextChatMessage($template);


        echo trim(trim($result->getContent(), "\"'"));
    }

    /**
     * "/news_article_generator/generate_meta_title" Endpoint
     */
    public function generateMetaTitle($segments = null, $params = null)
    {
        $current_title = base64_decode(ArrayHelper::getArrayValue($params, 'title'));

        $prompt = "Erstelle mit einen Meta-Titel (maximal 60 Zeichen) für meinen News-Artikel.\n"
            ."Der Titel des Artikels lautet '$current_title'. Gib mir bitte außer dem Titel nichts weiter zurück.\n"
            ."Der Titel sollte nicht nach Clickbait aussehen. Formuliere alles in der 'Du'-Form.";

        $messages = [
            ['role' => 'system', 'content' => "Du bist ein SEO-Experte mit Fokus aus Content-Marketing."],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT_4, 2000);
        $result = (new TextGenerationService())->getNextChatMessage($template);


        echo trim(trim($result->getContent(), "\"'"));
    }

    /**
     * "/news_article_generator/generate_meta_description" Endpoint
     */
    public function generateMetaDescription($segments = null, $params = null)
    {
        $current_title = base64_decode(ArrayHelper::getArrayValue($params, 'title'));

        $prompt = "Erstelle mit eine Meta-Beschreibung (maximal 160 Zeichen) für meinen News-Artikel.\n"
            ."Der Titel lautet '$current_title'. Gib mir bitte außer der Beschreibung nichts weiter zurück.\n"
            ."Formuliere alles in der 'Du'-Form. Die Beschreibung sollte seriös aber interessant wirken.";

        $messages = [
            ['role' => 'system', 'content' => "Du bist ein SEO-Experte mit Fokus aus Content-Marketing."],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, ChatGenerationTemplate::MODEL_CHAT_GPT_4, 2000);
        $result = (new TextGenerationService())->getNextChatMessage($template);

        echo trim(trim($result->getContent(), "\"'"));
    }

    protected function getMethodPostMappings()
    {
        return [
            'generate_title' => 'generateTitle',
            'generate_content' => 'generateContent',
            'generate_meta_title' => 'generateMetaTitle',
            'generate_meta_description' => 'generateMetaDescription'
        ];
    }
}