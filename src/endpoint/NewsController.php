<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\NewsManager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\service\ContentScheduleItemService;
use saschahuber\affiliatewebsitecore\service\ImprintCopyrightService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use stdClass;

class NewsController extends AdminRestController
{

    /**
     * "/news/post/save" Endpoint
     */
    public function saveNewsPost($segments = null, $params = null)
    {
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_NEWS_POSTS);

        $news_manager = new NewsManager();

        $new_item = false;

        $all_taxonomies = $news_manager->getTaxonomies('title', true, null);

        $item_id = RequestHelper::reqint('id');

        $item = $news_manager->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
            $item->status = Manager::STATUS_DRAFT;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->image_text = RequestHelper::reqstr('image_text');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }
        else{
            $item->permalink = UrlHelper::alias($item->permalink);
        }

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->author_id = RequestHelper::reqint('author_id');

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        if($new_item){
            $item->id = $news_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $news_manager->updateItem($item);
            ThumbnailHelper::deleteThumbnail(ThumbnailHelper::TYPE_NEWS, $item_id);
        }

        //Link Content schedule item
        $content_schedule_item_id = RequestHelper::reqint('linked_content_schedule_item_id');
        $content_schedule_item_service = new ContentScheduleItemService();
        if($content_schedule_item_id) {
            $content_schedule_item_service->linkToTypeAndItem($content_schedule_item_id, NewsManager::TYPE_NEWS, $item_id);
        }
        else {
            $content_schedule_item_service->unlinkElement(NewsManager::TYPE_NEWS, $item_id);
        }

        $selected_taxonomy_ids = RequestHelper::reqval('kategorien', []);

        $selected_taxonomies = array();
        foreach($all_taxonomies as $taxonomy){
            if(in_array($taxonomy->id, $selected_taxonomy_ids)){
                $selected_taxonomies[] = $taxonomy;
            }
        }
        $primary_taxonomy = RequestHelper::reqint('main_category');
        $news_manager->setTaxonomies($item, $selected_taxonomies, $primary_taxonomy);

        (new ImprintCopyrightService())->refreshElementCopyright(NewsManager::TYPE_NEWS, $item_id);

        echo $item_id;
    }

    /**
     * "/news/category/save" Endpoint
     */
    public function saveNewsCategory($segments = null, $params = null)
    {
        $news_manager = new NewsManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
        }
        else{
            $item = $news_manager->getTaxonomyById($item_id);
        }

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->taxonomy_parent_id = RequestHelper::reqint('parent_taxonomy', null);
        if($item->taxonomy_parent_id === 0){
            $item->taxonomy_parent_id = null;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->h1 = RequestHelper::reqstr('h1');
        $item->subtitle = RequestHelper::reqstr('subtitle');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }

        if($new_item){
            $item_id = $news_manager->createTaxonomy($item);
        }
        else {
            $news_manager->updateTaxonomy($item);
        }

        (new ImprintCopyrightService())->refreshElementCopyright(NewsManager::TYPE_NEWS_CATEGORY, $item_id);

        echo $item_id;
    }



    /**
     * "/news/post/delete" Endpoint
     */
    public function deleteNewsPost($segments = null, $params = null){
        $news_manager = new NewsManager();

        $news_manager->delete(ArrayHelper::getArrayValue($params, 'item_id'));
    }

    protected function getMethodPostMappings()
    {
        return [
            'post/save' => 'saveNewsPost',
            'post/delete' => 'deleteNewsPost',
            'category/save' => 'saveNewsCategory'
        ];
    }
}