<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class DbUtilityController extends AdminRestController
{

    /**
     * "/db/delete" Endpoint
     */
    public function deleteDbEntry($segments = null, $params = null)
    {
        global $DB;

        $table = ArrayHelper::getArrayValue($params, 'table');
        if(!(AuthHelper::getPermissionService())->hasTablePermission($table)){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_NO_PERMISSION, "User has no permission to delete from table '$table'");
        }

        $table = $DB->escape($table);
        $row_id = intval(ArrayHelper::getArrayValue($params, 'row'));

        if (!is_numeric($row_id)) {
            ErrorHelper::api(400, "Invalid row id");
        }

        $DB->query('DELETE FROM ' . $table . ' WHERE ' . $table . '_id = ' . $row_id);
    }

    /**
     * "/db/insert" Endpoint
     */
    public function insertDbEntry($segments = null, $params = null)
    {
        global $DB;

        $table = ArrayHelper::getArrayValue($params, 'table');
        if(!(AuthHelper::getPermissionService())->hasTablePermission($table)){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_NO_PERMISSION, "User has no permission to insert into table '$table'");
        }

        $table = $DB->escape($table);
        $item = ArrayHelper::getArrayValue($params, 'item');

        $keys = [];
        $vals = [];

        foreach ($item as $column => $value) {
            if (substr($column, 0, 5) == 'date_') {
                $value = strtotime($value);
            }

            array_push($keys, $DB->escape($column));
            array_push($vals, '"' . $DB->escape($value) . '"');
        }

        $DB->query('INSERT INTO ' . $table . '
                   (' . implode(', ', $keys) . ')
            VALUES (' . implode(', ', $vals) . ')');

        $this->requestOkay();
    }

    /**
     * "/db/insert_new" Endpoint
     */
    public function insertDbEntry_NEW($segments = null, $params = null)
    {
        global $DB;

        $data = $_REQUEST;

        $table = ArrayHelper::getArrayValue($data, 'table');
        unset($data['table']);

        if(!(AuthHelper::getPermissionService())->hasTablePermission($table)){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_NO_PERMISSION, "User has no permission to insert into table '$table'");
        }

        unset($data['authorization_token']);

        $object = new \stdClass();

        foreach ($data as $column => $value) {
            $object->{$column} = $value;
        }

        $item_id = $DB->insertFromObject($table, $object);

        $this->requestOkay();
    }

    /**
     * "/db/update" Endpoint
     */
    public function updateDbEntry($segments = null, $params = null)
    {
        global $DB;

        $table = ArrayHelper::getArrayValue($params, 'table');
        if(!(AuthHelper::getPermissionService())->hasTablePermission($table)){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_NO_PERMISSION, "User has no permission to insert update row in table '$table'");
        }

        $missing = array_diff_key(array_flip(['table', 'column', 'row', 'value']), $params);
        if (count($missing) > 0) {
            ErrorHelper::api(400, 'Error: Missing post data: ' . print_r($missing, true));
        }

        $table = $DB->escape($table);
        $column = $DB->escape(ArrayHelper::getArrayValue($params, 'column'));
        $row_id = intval(ArrayHelper::getArrayValue($params, 'row'));
        $value = $DB->escape(html_entity_decode(ArrayHelper::getArrayValue($params, 'value')));

        if (substr($column, 0, 5) == 'date_') {
            $value = strtotime($value);
        }

        if (!$row_id) {
            ErrorHelper::api(400, "Invalid row id");
        }

        $DB->query('UPDATE ' . $table . '
            SET ' . $column . ' = ' . ($value == 'NULL' ? 'NULL' : '"' . $value . '"') . '
            WHERE ' . $table . '_id = ' . $row_id);

        $this->requestOkay();
    }

    /**
     * "/db/toggle" Endpoint
     */
    public function toggleFlag($segments = null, $params = null)
    {
        global $DB;

        $table = ArrayHelper::getArrayValue($params, 'table');
        if(!(AuthHelper::getPermissionService())->hasTablePermission($table)){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_NO_PERMISSION, "User has no permission to toggle flag in table '$table'");
        }

        $table = $DB->escape($table);
        $column = $DB->escape(ArrayHelper::getArrayValue($params, 'column'));
        $row_id = intval(ArrayHelper::getArrayValue($params, 'row'));

        $DB->query('UPDATE ' . $table . '
            SET ' . $column . ' = !' . $column . '
            WHERE ' . $table . '_id = ' . $row_id);

        $this->requestOkay();
    }

    private function requestOkay()
    {
        $this->jsonResponse(['status' => 'okay']);
    }

    protected function getMethodPostMappings()
    {
        return [
            'delete' => 'deleteDbEntry',
            'insert' => 'insertDbEntry',
            'insert_new' => 'insertDbEntry_NEW',
            'update' => 'updateDbEntry',
            'toggle' => 'toggleFlag'
        ];
    }
}