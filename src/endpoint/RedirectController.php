<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\manager\ProductShopManager;
use saschahuber\affiliatewebsitecore\service\RedirectGroupItemService;
use saschahuber\affiliatewebsitecore\service\RedirectGroupService;
use saschahuber\affiliatewebsitecore\service\RedirectService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\RestController;
use stdClass;

#[AllowDynamicProperties]
class RedirectController extends RestController
{

    /**
     * "/redirect/product/{product_link_id}?source={source_url_in_base_64}" Endpoint
     */
    public function redirectToStore($segments = null, $params = null)
    {
        global $DB, $CONFIG;

        $product_link_id = $segments[2];

        $source_url = base64_decode(ArrayHelper::getArrayValue($params, 'source'));
        $session_id = session_id();

        $product_link_manager = new ProductLinkManager();
        $product_link = $product_link_manager->getById($product_link_id);

        $product_shop_manager = new ProductShopManager();
        $product_shop = $product_shop_manager->getById($product_link->product__shop_id);

        $redirect_url = $product_link->url;

        $product_link_click_log = new stdClass();
        $product_link_click_log->product_id = $product_link->product_id;
        $product_link_click_log->source_url = $source_url;
        $product_link_click_log->user_id = AuthHelper::getCurrentUserId();
        $product_link_click_log->user_ip = RequestHelper::getClientIp();
        $product_link_click_log->user_agent = ArrayHelper::getArrayValue($_SERVER, 'HTTP_USER_AGENT', null);
        $product_link_click_log->session_id = $session_id;
        $product_link_click_log->external_url = $redirect_url;
        $product_link_click_log->product__link_id = $product_link->id;
        $product_link_click_log->store_type = $product_shop ? $product_shop->title : 'Andere URL';
        $DB->insertFromObject('product__link_click', $product_link_click_log);

        UrlHelper::redirect($redirect_url, 307);
    }

    /**
     * "/redirect/save" Endpoint
     */
    public function saveRedirect($segments = null, $params = null){
        AdminRestController::authenticateOrFail();

        $redirect_service = new RedirectService();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $redirect_service->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
        }

        $item->from_url = trim(RequestHelper::reqstr('from_url'), '/');
        $item->to_url = RequestHelper::reqstr('to_url');
        $item->is_temporary = RequestHelper::reqflag('is_temporary');
        $item->is_active = RequestHelper::reqflag('is_active');

        echo $redirect_service->save($item);
    }

    /**
     * "/redirect/random/group/save" Endpoint
     */
    public function saveRandomRedirectGroup($segments = null, $params = null){
        AdminRestController::authenticateOrFail();

        $redirect_group_service = new RedirectGroupService();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $redirect_group_service->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
        }

        $item->title = trim(RequestHelper::reqstr('title'), '/');
        $item->alias = RequestHelper::reqstr('alias');
        $item->is_active = RequestHelper::reqflag('is_active');

        echo $redirect_group_service->save($item);
    }

    /**
     * "/redirect/random/save" Endpoint
     */
    public function saveRandomRedirect($segments = null, $params = null){
        AdminRestController::authenticateOrFail();

        $redirect_group_item_service = new RedirectGroupItemService();

        $item_id = RequestHelper::reqint('id');

        $item = $redirect_group_item_service->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
        }

        $item->redirect__group_id = RequestHelper::reqint('redirect_group_id');
        $item->title = trim(RequestHelper::reqstr('title'), '/');
        $item->target_url = RequestHelper::reqstr('target_url');
        $item->is_active = RequestHelper::reqflag('is_active');

        $item_id = $redirect_group_item_service->save($item);

        echo $item_id;
    }

    protected function getMethodGetMappings()
    {
        return [
            'product/*' => 'redirectToStore',
        ];
    }

    protected function getMethodPostMappings()
    {
        return [
            'save' => 'saveRedirect',
            'random/group/save' => 'saveRandomRedirectGroup',
            'random/save' => 'saveRandomRedirect'
        ];
    }
}