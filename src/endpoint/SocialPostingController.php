<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\manager\SocialPostingFeedItemManager;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use stdClass;

class SocialPostingController extends AdminRestController
{
    /**
     * "/social_post/save" Endpoint
     */
    public function savePost($segments = null, $params = null) {
        $target = RequestHelper::reqstr('target');
        $posting_id = RequestHelper::reqint('posting_id');

        $title = RequestHelper::reqstr('title');
        $description = RequestHelper::reqstr('description');
        $link = RequestHelper::reqstr('link');
        $image_url = RequestHelper::reqstr('image_url');

        $linked_element_type = RequestHelper::reqstr('linked_element_type');
        $linked_element_id = RequestHelper::reqint('linked_element_id');

        $posting_time = RequestHelper::reqstr('posting_time');

        $social_posting_feed_item_manager = new SocialPostingFeedItemManager();

        $posting_item = new stdClass();
        $posting_item->target = $target;

        $posting_item->title = $title;
        $posting_item->description = $description;
        $posting_item->link = $link;
        $posting_item->image_url = $image_url;

        $posting_item->linked_element_type = $linked_element_type;
        $posting_item->linked_element_id = $linked_element_id;

        $posting_item->posting_time = $posting_time;

        if($posting_id){
            $posting_item->id = $posting_id;
            $social_posting_feed_item_manager->updateItem($posting_item);
        }
        else {
            $posting_id = $social_posting_feed_item_manager->createItem($posting_item);
        }

        echo $posting_id;
    }

    protected function getMethodPostMappings()
    {
        return [
            'save' => 'savePost'
        ];
    }
}