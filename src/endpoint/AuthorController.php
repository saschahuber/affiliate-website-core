<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\AuthorService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use stdClass;

#[AllowDynamicProperties]
class AuthorController extends AdminRestController {

    /**
     * "/author/save" Endpoint
     */
    public function saveAuthor($segments=null, $params=null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_AUTHORS);

        $author_service = new AuthorService();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $author_service->getById($item_id);

        if ($item === null) {
            $new_item = true;
            $item = new stdClass();
        }

        $item->title = RequestHelper::reqstr('title');
        $item->permalink = UrlHelper::alias(trim(RequestHelper::reqstr('permalink')));
        $item->job_title = RequestHelper::reqstr('job_title');
        $item->organization = RequestHelper::reqstr('organization');
        $item->description = RequestHelper::reqstr('description');
        $item->attachment_id = RequestHelper::reqint('attachment_id');

        echo $author_service->save($item);
    }

    protected function getMethodPostMappings(){
        return [
            'save' => 'saveAuthor'
        ];
    }
}