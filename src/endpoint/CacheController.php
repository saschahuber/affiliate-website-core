<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use Exception;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\service\AdService;
use saschahuber\affiliatewebsitecore\service\AdTrackingService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\CacheHelper;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\RestController;
use stdClass;

class CacheController extends AdminRestController
{

    /**
     * "/cache/delete" Endpoint
     */
    public function deleteCache($segments = null, $params = null)
    {
        $cache_dir = ArrayHelper::getArrayValue($params, 'cache_dir', null);
        $cache_id = ArrayHelper::getArrayValue($params, 'cache_id', null);

        if ($cache_id) {
            CacheHelper::clearCache($cache_dir, $cache_id);
            echo "Cache geleert.";
        } else {
            $deleted_items = CacheHelper::clearCacheDir([$cache_dir]);
            echo "$deleted_items Cache-Einträge gelöscht.";
        }
    }

    /**
     * "/cache/static/delete" Endpoint
     */
    public function deleteStaticCache($segments = null, $params = null)
    {
        $cache_dir = ArrayHelper::getArrayValue($params, 'cache_dir', null);

        $allowed_dirs = ['data', 'img', 'produkt-img'];

        if (strstr($cache_dir, '..') || !in_array($cache_dir, $allowed_dirs)) {
            echo "Keine Einträge gelöscht. Ungültiger Pfad: '$cache_dir'";
            return;
        }

        $deleted_items = FileHelper::removeDirectory(APP_BASE . '/front/' . trim($cache_dir, '/'), ONE_MINUTE);
        echo "$deleted_items Cache-Einträge gelöscht.";
    }

    protected function getMethodPostMappings()
    {
        return [
            'delete' => 'deleteCache',
            'static/delete' => 'deleteStaticCache'
        ];
    }
}