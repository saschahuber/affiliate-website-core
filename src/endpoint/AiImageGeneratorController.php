<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\model\ImageGenerationTemplate;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\service\ImageGenerationService;

class AiImageGeneratorController extends AdminRestController
{
    /**
     * "/ai_image/generate" Endpoint
     */
    public function generateAiImage($segments = null, $params = null)
    {
        $prompt = base64_decode(ArrayHelper::getArrayValue($params, 'prompt'));
        $size = ArrayHelper::getArrayValue($params, 'size');

        $image_generation_template = new ImageGenerationTemplate($prompt, 1, $size);

        $image_generation_service = new ImageGenerationService();

        $ai_image_manager = new AiImageManager();

        $response = $image_generation_service->generateImageFromTemplate($image_generation_template);

        $json_result = json_decode($response);

        if($json_result && isset($json_result->data) && count($json_result->data) > 0){
            $data = $json_result->data[0];

            $revised_prompt = $data->revised_prompt;
            $image_url = $data->url;

            $attachment_id = $ai_image_manager->addAttachmentFromUrl($image_url, uniqid(), $prompt);

            $attachment = $ai_image_manager->getAttachment($attachment_id);
            $attachment->prompt = $prompt;
            $attachment->revised_prompt = $revised_prompt;
            $ai_image_manager->updateItem($attachment);

            $this->jsonResponse($data);
        }

        echo $response;
    }

    /**
     * "/ai_image/save_as_attachment" Endpoint
     */
    public function saveAsAttachment($segments = null, $params = null){
        global $CONFIG;

        $attachment_id = ArrayHelper::getArrayValue($params, 'attachment_id');

        $ai_image_manager = new AiImageManager();
        $image_manager = new ImageManager();

        $ai_attachment = $ai_image_manager->getAttachment($attachment_id);

        $ai_attachment_path = APP_BASE . '/..' . $ai_image_manager->getAttachmentUrl($attachment_id);
        $tmp_path = TMP_DIR . '/ai_images/' . $ai_attachment->file_name;
        $attachment_path = MEDIA_DIR . '/' . ImageManager::ATTACHMENT_DIR . '/' . $ai_attachment->file_name;

        FileHelper::createDirIfNotExists(TMP_DIR . '/ai_images');
        copy($ai_attachment_path, $attachment_path);

        $ai_attachment->attachment_id = $image_manager->addAttachment('/' . ImageManager::ATTACHMENT_DIR . '/' . $ai_attachment->file_name, $ai_attachment->prompt);

        $ai_image_manager->updateItem($ai_attachment);
    }

    /**
     * "/ai_image/delete" Endpoint
     */
    public function deleteAiImage($segments = null, $params = null){
        $attachment_id = ArrayHelper::getArrayValue($params, 'attachment_id');

        $ai_image_manager = new AiImageManager();
        $ai_image_manager->delete($attachment_id);
    }

    protected function getMethodPostMappings()
    {
        return [
            'generate' => 'generateAiImage',
            'save_as_attachment' => 'saveAsAttachment',
            'delete' => 'deleteAiImage'
        ];
    }
}