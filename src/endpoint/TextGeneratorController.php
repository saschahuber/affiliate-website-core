<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\model\ChatGenerationTemplate;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\service\TextGenerationService;
use service\text_generator\ProductContentGeneratorService;

#[AllowDynamicProperties]
class TextGeneratorController extends AdminRestController
{

    /**
     * "/text_generator/createContent" Endpoint
     */
    public function createContent($segments = null, $params = null)
    {
        $system_prompt = ArrayHelper::getArrayValue($params, 'system_prompt');
        $prompt = ArrayHelper::getArrayValue($params, 'prompt');
        $model = ArrayHelper::getArrayValue($params, 'model');
        $tokens = ArrayHelper::getArrayValue($params, 'tokens');

        $messages = [
            ['role' => 'system', 'content' => $system_prompt],
            ['role' => 'user', 'content' => $prompt]
        ];

        $template = new ChatGenerationTemplate($messages, $model, intval($tokens));
        $result = (new TextGenerationService())->getNextChatMessage($template);

        $data = [
            'content' => $result->getContent()
        ];

        $this->jsonResponse($data);
    }

    protected function getMethodPostMappings()
    {
        return [
            'createContent' => 'createContent'
        ];
    }
}