<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\service\ImprintCopyrightService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\JsButton;
use saschahuber\saastemplatecore\component\LinkButton;
use saschahuber\saastemplatecore\component\table\Table;
use saschahuber\saastemplatecore\component\table\TableRow;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use stdClass;

#[AllowDynamicProperties]
class BrandController extends AdminRestController
{

    /**
     * "/brand/save" Endpoint
     */
    public function saveBrand($segments = null, $params = null)
    {
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_BRANDS);

        $brand_manager = new BrandManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
        }
        else{
            $item = $brand_manager->getById($item_id);
        }

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->all_products_headline = RequestHelper::reqstr('all_products_headline');

        $item->title = RequestHelper::reqstr('title');

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->h1 = RequestHelper::reqstr('h1');
        $item->subtitle = RequestHelper::reqstr('subtitle');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }

        if($new_item){
            $item_id = $brand_manager->createItem($item);
        }
        else {
            $brand_manager->updateItem($item);
            ThumbnailHelper::deleteThumbnail(ThumbnailHelper::TYPE_BRAND, $item_id);
        }

        (new ImprintCopyrightService())->refreshElementCopyright(BrandManager::TYPE_BRAND, $item_id);

        echo $item_id;
    }

    /**
     * "/brand/add_to_product" Endpoint
     */
    public function addToProduct($segments = null, $params = null)
    {
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_BRANDS);

        global $DB;

        $brand_id = RequestHelper::reqint("brand_id");
        $product_id = RequestHelper::reqint("product_id");

        $DB->query("INSERT IGNORE INTO brand__product_mapping (brand_id, product_id) 
                VALUES (".intval($brand_id).", ".intval($product_id).")");

        echo "OK";
    }

    /**
     * "/brand/remove_from_product" Endpoint
     */
    public function removeFromProduct($segments = null, $params = null)
    {
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_BRANDS);

        global $DB;

        $brand_id = RequestHelper::reqint("brand_id");
        $product_id = RequestHelper::reqint("product_id");

        $DB->query("DELETE FROM brand__product_mapping  where brand_id = ".intval($brand_id)."
            and product_id = ".intval($product_id));

        echo "OK";
    }

    /**
     * "/brand/find_products" Endpoint
     */
    public function findProducts($segments = null, $params = null)
    {
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_BRANDS);

        global $DB;

        $brand_id = RequestHelper::reqint("brand_id");
        $keyword = RequestHelper::reqstr("keyword");

        if(strlen($keyword) < 3){
            return;
        }

        $dbquery = $DB->query("SELECT * FROM brand__product_mapping where brand_id = ".intval($brand_id));
        $brand_products = [];
        while($item = $dbquery->fetchObject()){
            $brand_products[] = $item->product_id;
        }

        $dbquery = $DB->query('SELECT * FROM product where title LIKE "'.$DB->escape('%'.$keyword.'%').'"');
        $products = [];
        while($item = $dbquery->fetchObject()){
            $item->already_added = array_search($item->id, $brand_products) !== false;
            $products[] = $item;
        }

        $column_labels = [
            'Titel',
            'Aktionen',
        ];

        $rows = [];
        foreach($products as $product){
            $actions = [
                new LinkButton("/dashboard/produkte/bearbeiten/".$product->id, 'Bearbeiten', 'fas fa-pen', false, true)
            ];

            if($product->already_added) $actions[] = new JsButton('Bereits hinzugefügt <i class="fas fa-check-circle"></i> (entfernen?)', "removeBrandFromProduct(this, $brand_id, {$product->id})");
            else {
                $actions[] = new JsButton('<i class="fas fa-plus-circle"></i> Marke hinzufügen</a>', "addBrandToProduct(this, $brand_id, {$product->id})");
            }

            $cells = [
                $product->title,
                new FloatContainer([$actions]),
            ];
            $rows[] = new TableRow($cells);
        }

        (new Table($column_labels, $rows, ['table-bordered', 'table-hover', 'centered']))->display();
    }

    protected function getMethodPostMappings()
    {
        return [
            'save' => 'saveBrand',
            'add_to_product' => 'addToProduct',
            'remove_from_product' => 'removeFromProduct',
            'find_products' => 'findProducts'
        ];
    }
}