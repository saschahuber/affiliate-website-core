<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\SocialImageGeneratorService;
use saschahuber\affiliatewebsitecore\service\text_generator\SocialPostCaptionGeneratorService;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class SocialPostsController extends AdminRestController
{
    function generateParams()
    {
        $params = array();
        $image_overlay_opacity = RequestHelper::reqdouble('image-overlay-opacity', null);
        if ($image_overlay_opacity !== null) {
            $params['image-overlay-opacity'] = floatval($image_overlay_opacity) / 100;
        }
        $vertical_text_offset = RequestHelper::reqdouble('vertical-text-offset', null);
        if ($vertical_text_offset !== null) {
            $params['vertical-text-offset'] = $vertical_text_offset;
        }
        $image_background_url = RequestHelper::reqstr('image-background-url', null);
        if ($image_background_url !== null) {
            $params['image-background-url'] = $image_background_url;
        }
        return $params;
    }

    /**
     * "/social_posts/generateSocialImage" Endpoint
     */
    public function generateSocialImage($segments = null, $params = null)
    {
        $app = RequestHelper::reqstr('app');
        $template = RequestHelper::reqstr('image-template');
        $text = RequestHelper::reqstr('image-text');
        $params = $this->generateParams();

        $social_image_generator_service = new SocialImageGeneratorService();
        echo $social_image_generator_service->generateSocialImage($app, $template, $text, 'tmp', $params);
    }

    /**
     * "/social_posts/getPostCaption" Endpoint
     */
    public function getPostCaption($segments = null, $params = null)
    {
        $app = RequestHelper::reqstr('app');
        $topic = RequestHelper::reqstr('topic');
        $hashtags = RequestHelper::reqbool('hashtags');

        $social_post_caption_generator_service = new SocialPostCaptionGeneratorService();
        $post_caption = $social_post_caption_generator_service->generateSocialPost($topic, $app, $hashtags);

        echo $post_caption;
    }

    protected function getMethodPostMappings()
    {
        return [
            'generateSocialImage' => 'generateSocialImage',
            'getPostCaption' => 'getPostCaption'
        ];
    }
}