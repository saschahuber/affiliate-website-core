<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class FileToolsController extends AdminRestController
{

    /**
     * "/file_tools/check_attachment_name" Endpoint
     */
    public function checkAttachmentName($segments = null, $params = null)
    {
        global $DB;

        $file_name = ArrayHelper::getArrayValue($params, 'file_name');

        $has_match = $DB->query("SELECT count(*) as anzahl from attachment where file_name = '".$DB->escape($file_name)."'")->fetchObject()->anzahl > 0;

        $this->jsonResponse(['has_match' => $has_match]);
    }

    /**
     * "/file_tools/check_stock_attachment_name" Endpoint
     */
    public function checkStockAttachmentName($segments = null, $params = null)
    {
        global $DB;

        $file_name = ArrayHelper::getArrayValue($params, 'file_name');

        $has_match = $DB->query("SELECT count(*) as anzahl from stock_attachment where file_name = '".$DB->escape($file_name)."'")->fetchObject()->anzahl > 0;

        $this->jsonResponse(['has_match' => $has_match]);
    }

    /**
     * "/file_tools/check_icon_name" Endpoint
     */
    public function checkIconName($segments = null, $params = null)
    {
        global $DB;

        $file_name = ArrayHelper::getArrayValue($params, 'file_name');

        $has_match = $DB->query("SELECT count(*) as anzahl from icon where file_name = '".$DB->escape($file_name)."'")->fetchObject()->anzahl > 0;

        $this->jsonResponse(['has_match' => $has_match]);
    }

    protected function getMethodPostMappings()
    {
        return [
            'check_attachment_name' => 'checkAttachmentName',
            'check_stock_attachment_name' => 'checkStockAttachmentName',
            'check_icon_name' => 'checkIconName'
        ];
    }
}