<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use DateTime;
use saschahuber\affiliatewebsitecore\service\ContentScheduleItemService;
use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use stdClass;

class ContentScheduleItemController extends AdminRestController
{

    /**
     * "/content_schedule/save" Endpoint
     */
    public function saveContentScheduleItem($segments = null, $params = null)
    {
        $new_item = false;

        $content_schedule_item_service = new ContentScheduleItemService();

        $content_schedule_item_id = RequestHelper::reqint('content_schedule_item_id');

        $content_schedule_item = $content_schedule_item_service->getById($content_schedule_item_id);

        if($content_schedule_item_id === null){
            $new_item = true;
            $content_schedule_item = new stdClass();
        }

        $content_schedule_item->title = RequestHelper::reqstr('title');

        $date = RequestHelper::reqstr('date');
        if($date) {
            $content_schedule_item->content_schedule_item_date = $date;
        }

        if($new_item){
            $content_schedule_item->id = $content_schedule_item_service->createContentScheduleItem($content_schedule_item);
            $content_schedule_item_id = $content_schedule_item->id;
        }
        else {
            $date = DateTime::createFromFormat('Y-m-d\TH:i:s', $content_schedule_item->content_schedule_item_date);
            if(!$date) {
                $date = DateTime::createFromFormat('Y-m-d\TH:i', $content_schedule_item->content_schedule_item_date);
            }
            $rescheduled_year = (int) $date->format('Y');
            $rescheduled_month = (int) $date->format('m');
            $rescheduled_day = (int) $date->format('d');

            $success = $content_schedule_item_service->rescheduleItem($content_schedule_item->id, $rescheduled_day, $rescheduled_month, $rescheduled_year);

            if($success){
                $content_schedule_item_service->updateContentScheduleItem($content_schedule_item);
            }
        }

        echo $content_schedule_item_id;
    }

    /**
     * "/content_schedule/reschedule" Endpoint
     */
    public function rescheduleContentScheduleItem($segments = null, $params = null){
        global $DB;

        $success = $DB->doTransaction(function() use($segments, $params){
            $content_schedule_item_service = new ContentScheduleItemService();

            $content_schedule_item_id = RequestHelper::reqint('element_id_to_reschedule');
            $rescheduled_day = RequestHelper::reqint('rescheduled_day');
            $rescheduled_month = RequestHelper::reqint('rescheduled_month');
            $rescheduled_year = RequestHelper::reqint('rescheduled_year');

            return $content_schedule_item_service->rescheduleItem($content_schedule_item_id, $rescheduled_day, $rescheduled_month, $rescheduled_year);
        });

        if($success === false){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_CONFLICT, 'Could not reschedule content item');
        }
    }

    /**
     * "/content_schedule/reschedule_without_date" Endpoint
     */
    public function rescheduleContentScheduleItemWithoutDate($segments = null, $params = null){
        global $DB;

        $success = $DB->doTransaction(function() use($segments, $params){
            $content_schedule_item_service = new ContentScheduleItemService();

            $content_schedule_item_id = RequestHelper::reqint('element_id_to_reschedule');

            return $content_schedule_item_service->rescheduleItemWithoutDate($content_schedule_item_id);
        });

        if($success === false){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_CONFLICT, 'Could not reschedule content item');
        }
    }

    protected function getMethodPostMappings()
    {
        return [
            'save' => 'saveContentScheduleItem',
            'reschedule' => 'rescheduleContentScheduleItem',
            'reschedule_without_date' => 'rescheduleContentScheduleItemWithoutDate',
        ];
    }
}