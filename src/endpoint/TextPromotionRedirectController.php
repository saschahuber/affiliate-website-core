<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\service\ContentScheduleItemService;
use saschahuber\affiliatewebsitecore\service\ImprintCopyrightService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\affiliatewebsitecore\service\TextPromotionService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use saschahuber\saastemplatecore\rest_controller\RestController;
use stdClass;

class TextPromotionRedirectController extends RestController
{

    /**
     * "/text_promotion/redirect" Endpoint
     */
    public function redirect($segments = null, $params = null)
    {
        (new TextPromotionService())->redirect();
    }

    protected function getMethodGetMappings()
    {
        return [
            'redirect' => 'redirect'
        ];
    }
}