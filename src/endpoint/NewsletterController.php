<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\NewsletterService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\affiliatewebsitecore\manager\NewsletterManager;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\RestController;
use stdClass;

class NewsletterController extends RestController
{

    /**
     * "/newsletter/subscribe/" Endpoint
     */
    public function subscribe($segments = null, $params = null)
    {
        $newsletter_manager = new NewsletterManager();

        $name = ArrayHelper::getArrayValue($params, 'name', null);
        $mail = ArrayHelper::getArrayValue($params, 'email', null);
        $topics = ArrayHelper::getArrayValue($params, 'topics', null);
        $all_topics_selected = ArrayHelper::getArrayValue($params, 'all_topics', null) === "true";

        $days_between_newsletter = intval(ArrayHelper::getArrayValue($_POST, 'days_between', DEFAULT_DAYS_BETWEEN_NEWSLETTER));

        $all_topics = array_keys(NewsletterService::ALL_NEWSLETTER_TOPICS);

        if($all_topics_selected){
            $subscriber_topics = $all_topics;
        }
        else{
            foreach($all_topics as $topic){
                if(in_array($topic, array_filter(explode(',', $topics)))){
                    $subscriber_topics[] = $topic;
                }
            }
        }

        //Wenn addSubscriber false zurückgibt => Fehler
        $success = $newsletter_manager->addSubscriber($mail, true, $name, $subscriber_topics, $days_between_newsletter);

        $this->jsonResponse(['error' => !$success]);
    }

    /**
     * "/newsletter/save/" Endpoint
     */
    public function saveNewsletter($segments = null, $params = null){
        AdminRestController::authenticateOrFail();

        $newsletter_manager = new NewsletterManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $newsletter_manager->getById($item_id);

        if(!$item){
            $new_item = true;
            $item = new stdClass();
        }

        $item->subject = RequestHelper::reqstr('subject');
        $item->message = RequestHelper::reqstr('message');

        $item->scheduled_time = RequestHelper::reqstr('scheduled_time');
        $item->receiver_groups = ArrayHelper::getArrayValue($params, 'receiver_groups')[0];
        $item->topics = ArrayHelper::getArrayValue($params, 'topics')[0];
        $item->template = RequestHelper::reqstr('template');

        if($new_item){
            $item->id = $newsletter_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $newsletter_manager->updateItem($item);
        }

        echo $item_id;
    }

    protected function getMethodPostMappings()
    {
        return [
            'subscribe' => 'subscribe',
            'save' => 'saveNewsletter'
        ];
    }
}