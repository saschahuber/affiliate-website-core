<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\affiliatewebsitecore\manager\CompanyManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\service\CompanyJsonImporterService;
use saschahuber\affiliatewebsitecore\service\CompanyService;
use saschahuber\affiliatewebsitecore\service\ImprintCopyrightService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use stdClass;

#[AllowDynamicProperties]
class CompanyController extends AdminRestController {


    /**
     * "/company/save" Endpoint
     */
    public function saveCompany($segments=null, $params=null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_COMPANIES);

        $company_manager = new CompanyManager();

        $new_item = false;

        $all_taxonomies = $company_manager->getTaxonomies('title', true, null);

        $item_id = RequestHelper::reqint('id');

        $item = $company_manager->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
            $item->status = Manager::STATUS_DRAFT;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }
        else{
            $item->permalink = UrlHelper::alias($item->permalink);
        }

        $item->website = RequestHelper::reqstr('website');
        $item->email = RequestHelper::reqstr('email');
        $item->phone_number = RequestHelper::reqstr('phone_number');
        $item->address = RequestHelper::reqstr('address');

        $item->latitude = RequestHelper::reqdouble('latitude');
        $item->longitude = RequestHelper::reqdouble('longitude');

        #TODO: Adresse in Koordinaten umwandeln

        $item->is_featured = RequestHelper::reqflag('is_featured');

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->show_opening_hours = RequestHelper::reqbool('show_opening_hours');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        if($new_item){
            $item->id = $company_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $company_manager->updateItem($item);
            ThumbnailHelper::deleteThumbnail(ThumbnailHelper::TYPE_COMPANY, $item_id);
        }

        $company_manager->updateOpeningHours($item_id, CompanyManager::getOpeningHoursFromRequest());

        $selected_taxonomy_ids = RequestHelper::reqval('kategorien', []);

        $selected_taxonomies = array();
        foreach($all_taxonomies as $taxonomy){
            if(in_array($taxonomy->id, $selected_taxonomy_ids)){
                $selected_taxonomies[] = $taxonomy;
            }
        }
        $primary_taxonomy = RequestHelper::reqint('main_category');
        $company_manager->setTaxonomies($item, $selected_taxonomies, $primary_taxonomy);
    }

    /**
     * "/company/category/save" Endpoint
     */
    public function saveCompanyCategory($segments=null, $params=null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_COMPANIES);

        $company_manager = new CompanyManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
        }
        else{
            $item = $company_manager->getTaxonomyById($item_id);
        }

        $item->doindex = RequestHelper::reqbool('doindex');

        $item->dofollow = RequestHelper::reqbool('dofollow');
        $item->icon = RequestHelper::reqstr('icon');

        $item->content = RequestHelper::reqstr('content');

        $item->taxonomy_parent_id = RequestHelper::reqint('parent_taxonomy', null);
        if($item->taxonomy_parent_id === 0){
            $item->taxonomy_parent_id = null;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        if($new_item){
            $item_id = $company_manager->createTaxonomy($item);
        }
        else {
            $company_manager->updateTaxonomy($item);
        }

        (new ImprintCopyrightService())->refreshElementCopyright(CompanyManager::TYPE_COMPANY_CATEGORY, $item_id);
    }

    /**
     * "/company/import" Endpoint
     */
    public function importCompany($segments=null, $params=null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_COMPANIES);

        $company_data = json_decode(base64_decode(ArrayHelper::getArrayValue($params, 'company_data')), true);

        $company_json_importer_service = new CompanyJsonImporterService();
        $status = $company_json_importer_service->importCompany($company_data);

        $this->jsonResponse(['status' => $status]);
    }

    /**
     * "/company/delete" Endpoint
     */
    public function deleteCompany($segments=null, $params=null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_COMPANIES);

        $company_id = ArrayHelper::getArrayValue($params, 'item_id');

        $company_service = new CompanyService();
        $successful = $company_service->deleteCompany($company_id);
    }

    protected function getMethodPostMappings(){
        return [
            'save' => 'saveCompany',
            'category/save' => 'saveCompanyCategory',
            'import' => 'importCompany',
            'delete' => 'deleteCompany'
        ];
    }
}