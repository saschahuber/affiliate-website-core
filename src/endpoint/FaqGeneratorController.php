<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\text_generator\FaqGeneratorService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class FaqGeneratorController extends AdminRestController
{

    /**
     * "/faq_generator/createFaqs" Endpoint
     */
    public function generateFaqs($segments = null, $params = null)
    {
        $faq_id = ArrayHelper::getArrayValue($params, 'faq_id');

        $faq_generator_service = new FaqGeneratorService();
        $faq_generator_service->createFaqs($faq_id);

        $this->jsonResponse(array('status' => 'ok'));
    }

    protected function getMethodPostMappings()
    {
        return [
            'generateFaqs' => 'generateFaqs'
        ];
    }
}