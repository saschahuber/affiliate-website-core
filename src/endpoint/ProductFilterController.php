<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\component\form\ProductFilter;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\component\BreakComponent;
use saschahuber\saastemplatecore\component\container\FloatContainer;
use saschahuber\saastemplatecore\component\Text;
use saschahuber\saastemplatecore\rest_controller\RestController;

class ProductFilterController extends RestController {

    /**
     * "/product_filter/getFilteredProducts/" Endpoint
     */
    public function getFilteredProducts($segments=null, $params=null){
        $param_items = [];

        $product_data_filter = [];

        foreach($params as $key => $value){
            $product_data_id = intval(str_replace('productdata-', '', $key));

            if(strpos($key, 'productdata-') === 0){
                if(strpos($key, '_min') !== false){
                    if(!array_key_exists($product_data_id, $product_data_filter)){
                        $product_data_filter[$product_data_id] = ['min' => null, 'max' => null];
                    }
                    $product_data_filter[$product_data_id]['min'] = $value;
                }
                elseif(strpos($key, '_max') !== false){
                    if(!array_key_exists($product_data_id, $product_data_filter)){
                        $product_data_filter[$product_data_id] = ['min' => null, 'max' => null];
                    }
                    $product_data_filter[$product_data_id]['max'] = $value;
                }
                else {
                    if($value === "true"){
                        $value = 1;
                    }
                    if($value === "false"){
                        continue;
                    }

                    $product_data_filter[$product_data_id] = explode(',', $value);
                }
            }
            else {
                $param_items[] = "$key=\"$value\"";
            }
        }

        $product_data_filter_param_items = [];
        foreach($product_data_filter as $filter_id => $value){
            if(is_array($value)){
                if(array_key_exists('min', $value) && array_key_exists('max', $value)){
                    $value = 'range__'.$value['min'].'__'.$value['max'];
                }
                else {
                    $value = implode(',', $value);
                }
            }

            $product_data_filter_param_items[] = "$filter_id=$value";
        }

        if(count($product_data_filter_param_items)){
            $param_items[] = 'data_fields="'.base64_encode(implode(';', $product_data_filter_param_items)) . '"';
        }

        $param_items[] = "show_detail_button=true";

        $shortcode = "[display_products ".implode(' ', $param_items)."]";

        $content = ShortcodeHelper::doShortcode($shortcode);

        if(strlen($content) < 10){
            BreakComponent::break();
            (new FloatContainer([new Text('Keine passenden Produkte gefunden', 'strong')]))->display();
            BreakComponent::break();
            return;
        }

        echo $content;
    }

    /**
     * "/product_filter/getFilter/" Endpoint
     */
    public function getFilter($segments=null, $params=null){
        (new ProductFilter($params))->display();
    }

    protected function getMethodPostMappings(){
        return [
            'getFilter' => 'getFilter',
            'getFilteredProducts' => 'getFilteredProducts'
        ];
    }
}