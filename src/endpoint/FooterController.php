<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\manager\FooterLinkGroupItemManager;
use saschahuber\saastemplatecore\manager\FooterLinkGroupManager;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use stdClass;

#[AllowDynamicProperties]
class FooterController extends AdminRestController {

    /**
     * "/footer/link/save" Endpoint
     */
    public function saveFooterLink($segments=null, $params=null){
        $footer_link_group_item_manager = new FooterLinkGroupItemManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');
        $group_id = RequestHelper::reqint('group_id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
            $item->footer_link_group_id = $group_id;
        }
        else{
            $item = $footer_link_group_item_manager->getById($item_id);
        }

        $item->title = RequestHelper::reqstr('title');
        $item->active = RequestHelper::reqbool('active');
        $item->sort_order = RequestHelper::reqint('sort_order');
        $item->url = RequestHelper::reqstr('url');

        if($new_item){
            $item_id = $footer_link_group_item_manager->createItem($item);
        }
        else {
            $footer_link_group_item_manager->updateItem($item);
        }

        echo $item_id;
    }

    /**
     * "/footer/linkgroup/save" Endpoint
     */
    public function saveFooterLinkGroup($segments=null, $params=null){
        $footer_link_group_manager = new FooterLinkGroupManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
        }
        else{
            $item = $footer_link_group_manager->getById($item_id);
        }

        $item->title = RequestHelper::reqstr('title');
        $item->active = RequestHelper::reqbool('active');
        $item->sort_order = RequestHelper::reqint('sort_order');
        $item->span = RequestHelper::reqint('span');

        if($new_item){
            $item_id = $footer_link_group_manager->createItem($item);
        }
        else {
            $footer_link_group_manager->updateItem($item);
        }

        echo $item_id;
    }

    protected function getMethodPostMappings(){
        return [
            'link/save' => 'saveFooterLink',
            'linkgroup/save' => 'saveFooterLinkGroup'
        ];
    }
}