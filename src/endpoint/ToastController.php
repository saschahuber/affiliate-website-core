<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\ScheduledToastService;
use saschahuber\affiliatewebsitecore\service\toast\ToastLoaderService;
use saschahuber\saastemplatecore\helper\ApiAuthHelper;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\RestController;
use stdClass;

#[AllowDynamicProperties]
class ToastController extends RestController
{

    /**
     * "/toast/save" Endpoint
     */
    public function saveToast($segments = null, $params = null){
        ApiAuthHelper::authenticate();
        AuthHelper::checkAdminPanelUser();

        $toast_service = new ScheduledToastService();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $toast_service->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
        }

        $item->title = RequestHelper::reqstr('title');
        $item->small_info = RequestHelper::reqstr('small_info');
        $item->content = RequestHelper::reqstr('content');

        $item->click_url = RequestHelper::reqstr('click_url');

        $item->linked_element_type = RequestHelper::reqstr('linked_element_type');
        $item->linked_element_id = RequestHelper::reqint('linked_element_id');

        $item->active_start = RequestHelper::reqstr('active_start');
        if($item->active_start === ""){
            $item->active_start = null;
        }

        $item->active_end = RequestHelper::reqstr('active_end');
        if($item->active_end === ""){
            $item->active_end = null;
        }

        if($new_item){
            $item->id = $toast_service->createItem($item);
            $item_id = $item->id;
        }
        else {
            $toast_service->updateItem($item);
        }

        echo $item_id;
    }

    /**
     * "/toast/user_toasts" Endpoint
     */
    public function getUserToasts($segments = null, $params = null)
    {
        $fingerprint = ArrayHelper::getArrayValue($params, 'fingerprint');
        $url = ArrayHelper::getArrayValue($params, 'url');
        $referrer = ArrayHelper::getArrayValue($params, 'referrer');
        $mobile = ArrayHelper::getArrayValue($params, 'mobile');
        $utm_source = ArrayHelper::getArrayValue($params, 'utm_source');
        $utm_medium = ArrayHelper::getArrayValue($params, 'utm_medium');
        $utm_campaign = ArrayHelper::getArrayValue($params, 'utm_campaign');
        $ugm_content = ArrayHelper::getArrayValue($params, 'ugm_content');
        $utm_term = ArrayHelper::getArrayValue($params, 'utm_term');

        $toast_loader_service = new ToastLoaderService();

        $toasts = $toast_loader_service->loadToastsForUser($fingerprint, $url, $referrer, $mobile, $utm_source, $utm_medium, $utm_campaign, $ugm_content, $utm_term);

        $this->jsonResponse($toasts);
    }

    protected function getMethodPostMappings()
    {
        return [
            'save' => 'saveToast',
            'user_toasts' => 'getUserToasts'
        ];
    }
}