<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\TempDataService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class TempDataController extends AdminRestController
{

    /**
     * "/temp/store" Endpoint
     */
    public function storeTempData($segments = null, $params = null){
        $encoded_data = ArrayHelper::getArrayValue($params, 'encoded_data');
        $json_data = base64_decode($encoded_data);

        $data = json_decode(utf8_encode($json_data));

        foreach ($data as $key => $value) {
            $data->{$key} = urldecode($value);
        }

        $temp_data_service = new TempDataService();
        echo $temp_data_service->addTempData($data);
    }

    protected function getMethodPostMappings()
    {
        return [
            'store' => 'storeTempData'
        ];
    }
}