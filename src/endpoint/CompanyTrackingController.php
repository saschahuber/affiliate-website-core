<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\CompanyTrackingService;
use saschahuber\saastemplatecore\rest_controller\RestController;

#[AllowDynamicProperties]
class CompanyTrackingController extends RestController {


    /**
     * "/company_tracking/{type}/{product_id}" Endpoint
     */
    public function trackCompanyAction($segments=null, $params=null){
        $type = $segments[1];
        $company_id = intval($segments[2]);

        $company_tracking_service = new CompanyTrackingService();
        $company_tracking_service->trackCompanyAction($company_id, $type);
    }

    protected function getMethodPostMappings(){
        return [
            '*/*' => 'trackCompanyAction'
        ];
    }
}