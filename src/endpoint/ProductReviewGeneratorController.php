<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\text_generator\ProductReviewGeneratorService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class ProductReviewGeneratorController extends AdminRestController {

    /**
     * "/product_review_generator/createIntroduction" Endpoint
     */
    public function createIntroduction($segments=null, $params=null){
        $title = ArrayHelper::getArrayValue($params, 'title');

        $product_review_generator_service = new ProductReviewGeneratorService();
        $result_list = $product_review_generator_service->createIntroduction($title);
        $this->jsonResponse($result_list);
    }

    /**
     * "/product_review_generator/createAttributeDescription" Endpoint
     */
    public function createAttributeDescription($segments=null, $params=null){
        $product_id = ArrayHelper::getArrayValue($params, 'productId');
        $title = ArrayHelper::getArrayValue($params, 'title');
        $proList = json_decode(ArrayHelper::getArrayValue($params, 'proList'));
        $contraList = json_decode(ArrayHelper::getArrayValue($params, 'contraList'));

        $product_review_generator_service = new ProductReviewGeneratorService();
        $result_list = $product_review_generator_service->createAttributeDescription($product_id, $title, $proList, $contraList);
        $this->jsonResponse($result_list);
    }

    /**
     * "/product_review_generator/createReview" Endpoint
     */
    public function createReview($segments=null, $params=null){
        $product_id = ArrayHelper::getArrayValue($params, 'productId');
        $title = ArrayHelper::getArrayValue($params, 'title');
        $proList = json_decode(ArrayHelper::getArrayValue($params, 'proList'));
        $contraList = json_decode(ArrayHelper::getArrayValue($params, 'contraList'));

        $product_review_generator_service = new ProductReviewGeneratorService();
        $result_list = $product_review_generator_service->createReview($product_id, $title, $proList, $contraList);
        $this->jsonResponse($result_list);
    }

    /**
     * "/product_review_generator/createSummary" Endpoint
     */
    public function createSummary($segments=null, $params=null){
        $title = ArrayHelper::getArrayValue($params, 'title');
        $review = ArrayHelper::getArrayValue($params, 'review');

        $product_review_generator_service = new ProductReviewGeneratorService();
        $result_list = $product_review_generator_service->createSummary($title, $review);
        $this->jsonResponse($result_list);
    }

    protected function getMethodPostMappings(){
        return [
            'createIntroduction' => 'createIntroduction',
            'createAttributeDescription' => 'createAttributeDescription',
            'createReview' => 'createReview',
            'createSummary' => 'createSummary'
        ];
    }
}