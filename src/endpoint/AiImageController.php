<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\manager\AiImageManager;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\model\ImageGenerationTemplate;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\service\ImageGenerationService;

class AiImageController extends AdminRestController
{
    /**
     * "/ai_image/rename" Endpoint
     */
    public function renameAiImage($segments = null, $params = null)
    {
        $ai_image_manager = new AiImageManager();

        $ai_image_manager->renameAttachment(ArrayHelper::getArrayValue($params, 'image_id'), ArrayHelper::getArrayValue($params, 'file_name'));

        $this->jsonResponse([$segments, $params]);
    }

    protected function getMethodPostMappings()
    {
        return [
            'rename' => 'renameAiImage'
        ];
    }
}