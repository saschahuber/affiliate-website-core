<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\misc\GuideFilter;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\RestController;

class KaufberaterController extends RestController
{

    /**
     * "kaufberater/search" Endpoint
     */
    public function doSearch($segments = null, $params = null)
    {
        #require_once(COMMONS_BASE . '/classes/GuideFilter.php');

        $guide_id = ArrayHelper::getArrayValue($params, 'guide_id');
        $filter_selections = ArrayHelper::getArrayValue($params, 'filter_selections');
        $filter_selections = json_decode($filter_selections, true);

        $guide_filter = new GuideFilter($guide_id);
        echo $guide_filter->getFilterResults($filter_selections);
    }

    /**
     * "kaufberater/result_count" Endpoint
     */
    public function getResultCount($segments = null, $params = null)
    {
        #require_once(COMMONS_BASE . '/classes/GuideFilter.php');

        $guide_id = ArrayHelper::getArrayValue($params, 'guide_id');
        $filter_selections = ArrayHelper::getArrayValue($params, 'filter_selections');
        $filter_selections = json_decode($filter_selections, true);

        $guide_filter = new GuideFilter($guide_id);
        echo $guide_filter->getResultCount($filter_selections);
    }

    protected function getMethodPostMappings()
    {
        return [
            'search' => 'doSearch',
            'result_count' => 'getResultCount'
        ];
    }
}