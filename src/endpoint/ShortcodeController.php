<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\CssHelper;
use saschahuber\saastemplatecore\helper\DependencyHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\JsHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class ShortcodeController extends AdminRestController
{

    /**
     * "/shortcode/preview" Endpoint
     */
    public function preview($segments = null, $params = null)
    {
        echo BufferHelper::buffered(function () {
            ?>
            <?= DependencyHelper::getAllCssAndJs(!Helper::isAdminApp()) ?>

            <link rel="preload" href="<?= CssHelper::getCssFileWithVersion() ?>" as="style">

            <link rel="preload" href="<?= JsHelper::getJsFileWithVersion() ?>" as="script">
            <?php
        });

        $code = $params['shortcode'];

        $code = str_replace(" ", "+", $code);

        $plaintext_code = base64_decode($code);

        echo ShortcodeHelper::doShortcode($plaintext_code);
    }

    protected function getMethodGetMappings()
    {
        return [
            'preview' => 'preview'
        ];
    }
}