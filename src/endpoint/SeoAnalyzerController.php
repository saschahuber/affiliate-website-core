<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\SeoReportService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class SeoAnalyzerController extends AdminRestController
{

    /**
     * "/seo/analyze" Endpoint
     */
    public function getSEOReport($segments = null, $params = null)
    {
        $url = ArrayHelper::getArrayValue($params, 'url', null);
        $keyword = ArrayHelper::getArrayValue($params, 'keyword', null);

        $seo_analyzer_service = new SeoReportService();

        $results = $seo_analyzer_service->getSeoReportForUrl($url, $keyword);

        $report_content = '';

        foreach($results as $result){
            $report_content .= "<h2 style='margin-bottom: 0;'>{$result['name']}</h2>";
            $report_content .= "<p style='margin-bottom: 0;'>{$result['description']}</p>";
            $report_content .= "<p style='margin-bottom: 0;'>{$result['analysis']}; <strong>Negativer Impact: {$result['negative_impact']}</strong></p>";
            $value = $result['value'];
            if(is_array($value)){
                $value = json_encode($value);
            }
            $report_content .= "<p style='margin-bottom: 0;'>Value: $value</p>";
        }

        echo $report_content;
    }

    protected function getMethodPostMappings()
    {
        return [
            'analyze' => 'getSEOReport'
        ];
    }
}