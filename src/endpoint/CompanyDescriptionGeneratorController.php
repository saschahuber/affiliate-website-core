<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\text_generator\CompanyDescriptionGeneratorService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

#[AllowDynamicProperties]
class CompanyDescriptionGeneratorController extends AdminRestController {
    /**
     * "/company_description_generator/generateAiCompanyDescription" Endpoint
     */
    public function generateAiCompanyDescription($segments=null, $params=null){
        $company_name = ArrayHelper::getArrayValue($params, 'company_name');
        $address = ArrayHelper::getArrayValue($params, 'address');
        $mail = ArrayHelper::getArrayValue($params, 'mail');
        $phone = ArrayHelper::getArrayValue($params, 'phone_number');
        $website = ArrayHelper::getArrayValue($params, 'website');
        $company_id = ArrayHelper::getArrayValue($params, 'company_id');
        $model = ArrayHelper::getArrayValue($params, 'model');
        $tokens = intval(ArrayHelper::getArrayValue($params, 'tokens'));

        $company_description_generator_service = new CompanyDescriptionGeneratorService();
        echo $company_description_generator_service->createCompanyDescription($company_name, $address, $mail, $phone, $website, $model, $tokens, $company_id);
    }

    protected function getMethodGetMappings(){
        return [
            'generateAiCompanyDescription' => 'generateAiCompanyDescription'
        ];
    }

    protected function getMethodPostMappings(){
        return [
            'generateAiCompanyDescription' => 'generateAiCompanyDescription'
        ];
    }
}