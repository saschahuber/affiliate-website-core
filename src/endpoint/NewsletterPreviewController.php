<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use ProductFilter;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\service\TemplateService;

#[AllowDynamicProperties]
class NewsletterPreviewController extends AdminRestController
{

    /**
     * "/newsletter_preview/getNewsletterPreview/" Endpoint
     */
    public function getNewsletterPreview($segments = null, $params = null)
    {
        global $CONFIG;

        $this->template_service = new TemplateService();

        $subject = ArrayHelper::getArrayValue($params, 'subject');
        $message = ArrayHelper::getArrayValue($params, 'message');
        $template = ArrayHelper::getArrayValue($params, 'template');

        if ($template) {
            $message = $this->template_service->generateMailSubTemplate($template, $params);
        }

        echo $this->template_service->generateMailContent($CONFIG->general_email_address, $subject, $message, false, false);
    }

    protected function getMethodGetMappings()
    {
        return [
            'getNewsletterPreview' => 'getNewsletterPreview'
        ];
    }
}