<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\ProductFeedUpdateService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class ProductFeedController extends AdminRestController {

    /**
     * "/product_feed/update_feed" Endpoint
     */
    public function updateProductFeed($segments=null, $params=null){
        $feed_id = ArrayHelper::getArrayValue($params, 'feed_id');

        $items = (new ProductFeedUpdateService())->updateProductFeedFile($feed_id);

        $this->jsonResponse($items);
    }

    protected function getMethodPostMappings(){
        return [
            'update_feed' => 'updateProductFeed'
        ];
    }
}