<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\service\ContentScheduleItemService;
use saschahuber\affiliatewebsitecore\service\ImprintCopyrightService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use stdClass;

class BlogController extends AdminRestController
{

    /**
     * "/blog/post/save" Endpoint
     */
    public function saveBlogPost($segments = null, $params = null)
    {
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_BLOG_POSTS);

        global $DB;

        $post_manager = new PostManager();

        $new_item = false;

        $all_taxonomies = $post_manager->getTaxonomies('title', true, null);

        $item_id = RequestHelper::reqint('id');

        $item = $post_manager->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
            $item->status = Manager::STATUS_DRAFT;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->image_text = RequestHelper::reqstr('image_text');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }
        else{
            $item->permalink = UrlHelper::alias($item->permalink);
        }

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->author_id = RequestHelper::reqint('author_id');

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        if($new_item){
            $item->id = $post_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $post_manager->updateItem($item);
            ThumbnailHelper::deleteThumbnail(ThumbnailHelper::TYPE_BLOG, $item_id);
        }

        //Link Content schedule item
        $content_schedule_item_id = RequestHelper::reqint('linked_content_schedule_item_id');
        $content_schedule_item_service = new ContentScheduleItemService();
        if($content_schedule_item_id) {
            $content_schedule_item_service->linkToTypeAndItem($content_schedule_item_id, PostManager::TYPE_POST, $item_id);
        }
        else {
            $content_schedule_item_service->unlinkElement(PostManager::TYPE_POST, $item_id);
        }

        (new ImprintCopyrightService())->refreshElementCopyright(PostManager::TYPE_POST, $item_id);

        $selected_taxonomy_ids = ArrayHelper::getArrayValue($params, 'kategorien');

        $selected_taxonomies = array();
        foreach($all_taxonomies as $taxonomy){
            if(in_array($taxonomy->id, $selected_taxonomy_ids)){
                $selected_taxonomies[] = $taxonomy;
            }
        }
        $primary_taxonomy = RequestHelper::reqint('main_category');
        $post_manager->setTaxonomies($item, $selected_taxonomies, $primary_taxonomy);

        $cache_cleared = false;#clear_page_cache($item_permalink);

        echo $item_id;
    }

    /**
     * "/blog/category/save" Endpoint
     */
    public function saveBlogCategory(){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_BLOG_POSTS);

        $post_manager = new PostManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
        }
        else{
            $item = $post_manager->getTaxonomyById($item_id);
        }

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->taxonomy_parent_id = RequestHelper::reqint('parent_taxonomy', null);
        if($item->taxonomy_parent_id === 0){
            $item->taxonomy_parent_id = null;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        $item->h1 = RequestHelper::reqstr('h1');
        $item->subtitle = RequestHelper::reqstr('subtitle');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }

        if($new_item){
            $item_id = $post_manager->createTaxonomy($item);
        }
        else {
            $post_manager->updateTaxonomy($item);
        }

        (new ImprintCopyrightService())->refreshElementCopyright(PostManager::TYPE_POST_CATEGORY, $item_id);

        echo $item_id;
    }

    protected function getMethodPostMappings()
    {
        return [
            'post/save' => 'saveBlogPost',
            'category/save' => 'saveBlogCategory'
        ];
    }
}