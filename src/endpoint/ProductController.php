<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\ProductComparisonManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldGroupManager;
use saschahuber\affiliatewebsitecore\manager\ProductDataFieldManager;
use saschahuber\affiliatewebsitecore\manager\ProductFeedManager;
use saschahuber\affiliatewebsitecore\manager\ProductLinkManager;
use saschahuber\affiliatewebsitecore\manager\ProductManager;
use saschahuber\affiliatewebsitecore\manager\ProductShopManager;
use saschahuber\affiliatewebsitecore\service\ContentScheduleItemService;
use saschahuber\affiliatewebsitecore\service\ImprintCopyrightService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use stdClass;

class ProductController extends AdminRestController {
    /**
     * "/product/save" Endpoint
     */
    public function saveProduct($segments=null, $params=null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_PRODUCTS);

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $brand_manager = new BrandManager();

        $new_item = false;

        $all_taxonomies = $product_manager->getTaxonomies('title', true, null);

        $item_id = RequestHelper::reqint('id');

        $item = $product_manager->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
            $item->status = Manager::STATUS_DRAFT;
            $item->is_prime = false;
        }


        $item->title = RequestHelper::reqstr('title');

        $item->image_text = RequestHelper::reqstr('image_text');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }

        $item->ean = RequestHelper::reqstr('ean');

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->author_id = RequestHelper::reqint('author_id');

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        if($new_item){
            $item->id = $product_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $product_manager->updateItem($item);
            #ThumbnailHelper::deleteThumbnail(ThumbnailHelper::TYPE_PRODUCT, $item_id);
        }

        $selected_taxonomy_ids = ArrayHelper::getArrayValue($params, 'kategorien');
        $selected_taxonomies = array();
        if($selected_taxonomy_ids) {
            foreach ($all_taxonomies as $taxonomy) {
                if (in_array($taxonomy->id, $selected_taxonomy_ids)) {
                    $selected_taxonomies[] = $taxonomy;
                }
            }
        }
        $primary_taxonomy = RequestHelper::reqint('main_category');
        $product_manager->setTaxonomies($item, $selected_taxonomies, $primary_taxonomy);

        $selected_brand_ids = ArrayHelper::getArrayValue($params, 'hersteller');
        $selected_brands = array();
        if($selected_brand_ids) {
            foreach ($brand_manager->getAll() as $brand) {
                if (in_array($brand->id, $selected_brand_ids)) {
                    $selected_brands[] = $brand;
                }
            }
        }
        $brand_manager->setProductBrands($item, $selected_brands);

        //Link Content schedule item
        $content_schedule_item_id = RequestHelper::reqint('linked_content_schedule_item_id');
        $content_schedule_item_service = new ContentScheduleItemService();
        if($content_schedule_item_id) {
            $content_schedule_item_service->linkToTypeAndItem($content_schedule_item_id, ProductManager::TYPE_PRODUCT, $item_id);
        }
        else {
            $content_schedule_item_service->unlinkElement(ProductManager::TYPE_PRODUCT, $item_id);
        }

        (new ImprintCopyrightService())->refreshElementCopyright(ProductManager::TYPE_PRODUCT, $item_id);

        echo $item_id;
    }

    /**
     * "/product/feed/save" Endpoint
     */
    public function saveProductFeed($segments=null, $params=null){
        $feed_manager = new ProductFeedManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
        }
        else{
            $item = $feed_manager->getById($item_id);
        }

        $item->title = RequestHelper::reqstr('title');
        $item->url = RequestHelper::reqstr('url');
        $item->import_cron_rule = RequestHelper::reqstr('import_cron_rule');
        $item->template = RequestHelper::reqstr('template');
        $item->product__shop_id = RequestHelper::reqint('shop_id');
        $item->is_active = RequestHelper::reqbool('is_active');

        if($new_item){
            $item_id = $feed_manager->createItem($item);
        }
        else {
            $feed_manager->updateItem($item);
        }

        echo $item_id;
    }

    /**
     * "/product/comparison/save" Endpoint
     */
    public function saveProductComparison($segments=null, $params=null){
        $product_comparison_manager = new ProductComparisonManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $product_comparison_manager->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
            $item->status = Manager::STATUS_DRAFT;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }
        else{
            $item->permalink = UrlHelper::alias($item->permalink);
        }

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->author_id = RequestHelper::reqint('author_id');

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        $product_count = 0;

        //Product #1
        $product_id_1 = RequestHelper::reqint('product_id_1');
        $item->product_id_1 = null;
        $item->product_title_1 = null;
        if($product_id_1) {
            $product_count++;
            $item->product_id_1 = $product_id_1;
            $item->product_title_1 = RequestHelper::reqstr('product_title_1');
        }

        //Product #2
        $product_id_2 = RequestHelper::reqint('product_id_2');
        $item->product_id_2 = null;
        $item->product_title_2 = null;
        if($product_id_2) {
            $product_count++;
            $item->product_id_2 = $product_id_2;
            $item->product_title_2 = RequestHelper::reqstr('product_title_2');
        }

        //Product #3
        $product_id_3 = RequestHelper::reqint('product_id_3');
        $item->product_id_3 = null;
        $item->product_title_3 = null;
        if($product_id_3) {
            $product_count++;
            $item->product_id_3 = $product_id_3;
            $item->product_title_3 = RequestHelper::reqstr('product_title_3');
        }

        if($product_count < 2){
            die("Bitte gib mindestens 2 Produkte für den Vergleich an.");
        }

        if($new_item){
            $item->id = $product_comparison_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $product_comparison_manager->updateItem($item);
            ThumbnailHelper::deleteThumbnail(ThumbnailHelper::TYPE_PRODUCT_COMPARISON, $item_id);
        }

        (new ImprintCopyrightService())->refreshElementCopyright(ProductComparisonManager::TYPE_PRODUCT_COMPARISON, $item_id);

        echo $item_id;
    }

    /**
     * "/product/shopü/save" Endpoint
     */
    public function saveProductShop($segments=null, $params=null){
        $shop_manager = new ProductShopManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
        }
        else{
            $item = $shop_manager->getById($item_id);
        }

        $item->title = RequestHelper::reqstr('title');
        $item->url_format = RequestHelper::reqstr('url_format');
        $item->button_text = RequestHelper::reqstr('button_text');
        $item->logo_src = RequestHelper::reqstr('logo_src');
        $item->is_hidden = RequestHelper::reqbool('is_hidden');
        $item->sort_order = RequestHelper::reqint('sort_order');

        if($new_item){
            $item_id = $shop_manager->createItem($item);
        }
        else {
            $shop_manager->updateItem($item);
        }
    }

    /**
     * "/product/link/save" Endpoint
     */
    public function saveProductLink($segments=null, $params=null){
        $link_manager = new ProductLinkManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');
        $product_id = RequestHelper::reqint('product_id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
            $item->product_id = $product_id;
        }
        else{
            $item = $link_manager->getById($item_id);
        }

        $item->sort_order = RequestHelper::reqint('sort_order');
        $item->price = RequestHelper::reqdouble('price');
        $item->reduced_price = RequestHelper::reqdouble('reduced_price');
        $item->external_id = RequestHelper::reqstr('external_id');
        $item->url = RequestHelper::reqstr('url');
        $item->button_text = RequestHelper::reqstr('button_text');
        $item->is_available = RequestHelper::reqbool('is_available');
        $item->is_active = RequestHelper::reqbool('is_active');
        $item->product__shop_id = RequestHelper::reqint('shop_id');
        $item->product__feed_id = RequestHelper::reqint('feed_id');

        if($new_item){
            $item_id = $link_manager->createItem($item);
        }
        else {
            $link_manager->updateItem($item);
        }

        echo $item_id;
    }

    /**
     * "/product/category/save" Endpoint
     */
    public function saveProductCategory($segments=null, $params=null){
        $product_manager = AffiliateInterfacesHelper::getProductManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        if($item_id === null){
            $new_item = true;
            $item = new stdClass();
        }
        else{
            $item = $product_manager->getTaxonomyById($item_id);
        }

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        $item->all_products_headline = RequestHelper::reqstr('all_products_headline');

        $item->taxonomy_parent_id = RequestHelper::reqint('parent_taxonomy', null);
        if($item->taxonomy_parent_id === 0){
            $item->taxonomy_parent_id = null;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->icon_id = RequestHelper::reqint('icon_id');

        $item->h1 = RequestHelper::reqstr('h1');
        $item->subtitle = RequestHelper::reqstr('subtitle');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::alias($item->title);
        }

        $item->is_virtual = RequestHelper::reqbool('is_virtual');

        $virtual_product_taxonomy_data_fields = [];
        foreach($_REQUEST as $key => $value){
            if(strpos($key, 'virtual_product_data_field__') === 0){
                $key_parts = explode('__', $key);

                $data_field_id = $key_parts[1];
                $field_name = $key_parts[2];

                if(!array_key_exists($data_field_id, $virtual_product_taxonomy_data_fields)){
                    $virtual_product_taxonomy_data_fields[$data_field_id] = [];
                }

                $virtual_product_taxonomy_data_fields[$data_field_id][$field_name] = $value;
            }
        }

        $product_manager->updateVirtualTaxonomyDataFieldMappings($item_id, $virtual_product_taxonomy_data_fields);

        if($new_item){
            $item_id = $product_manager->createTaxonomy($item);
        }
        else {
            $product_manager->updateTaxonomy($item);
            ThumbnailHelper::deleteThumbnail(ThumbnailHelper::TYPE_PRODUCT_CATEGORY, $item_id);
        }

        (new ImprintCopyrightService())->refreshElementCopyright(ProductManager::TYPE_PRODUCT_CATEGORY, $item_id);

        echo $item_id;
    }

    /**
     * "/product/datafield/save" Endpoint
     */
    public function saveProductDatafield($segments=null, $params=null){
        $product_data_field_manager = new ProductDataFieldManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $product_data_field_manager->getById($item_id);

        if(!$item){
            $new_item = true;
            $item = new stdClass();
        }

        $item->field_name = RequestHelper::reqstr('field_name');
        $item->field_key = UrlHelper::alias(RequestHelper::reqstr('field_name'));
        $item->field_description = RequestHelper::reqstr('field_description');
        $item->field_type = RequestHelper::reqstr('field_type');
        $item->field_values = RequestHelper::reqstr('field_values');
        $item->field_prefix = RequestHelper::reqstr('field_prefix');
        $item->field_suffix = RequestHelper::reqstr('field_suffix');
        $item->field_suffix = RequestHelper::reqstr('field_suffix');
        $item->group_id = RequestHelper::reqint('group_id');

        if($new_item){
            $item->id = $product_data_field_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $product_data_field_manager->updateItem($item);
        }

        echo $item_id;
    }

    /**
     * "/product/datagroup/save" Endpoint
     */
    public function saveProductDatagroup($segments=null, $params=null){
        $product_data_field_group_manager = new ProductDataFieldGroupManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $product_data_field_group_manager->getById($item_id);
        if(!$item){
            $new_item = true;
            $item = new stdClass();
        }

        $item->group_name = RequestHelper::reqstr('group_name');
        $item->product__taxonomy_id = RequestHelper::reqint('product__taxonomy_id');

        if($new_item){
            $item->id = $product_data_field_group_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $product_data_field_group_manager->updateItem($item);
        }

        echo $item_id;
    }

    /**
     * "/product/data/save" Endpoint
     */
    public function saveProductData($segments=null, $params=null){
        $product_manager = AffiliateInterfacesHelper::getProductManager();

        $product_id = RequestHelper::reqint('product_id');

        $items = [];
        foreach($_POST as $key => $value){
            if(strpos($key, 'data_field__') !== false){
                $key_parts = explode('__', $key);
                $items[] = [$key_parts[1], $key_parts[2], $value];
            }
        }

        foreach($items as $item){
            $group_id = $item[0];
            $data_field_id = $item[1];
            $value = $item[2];

            if($value === ""){
                $value = null;
            }

            $product_manager->updateProductField($product_id, $data_field_id, $value);
        }

        echo $product_id;
    }

    /**
     * "/product/update_pro_contra_list" Endpoint
     */
    public function updateProContraList($segments=null, $params=null){
        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $product_id = intval(ArrayHelper::getArrayValue($params, 'product_id'));
        $pro_title = ArrayHelper::getArrayValue($params, 'pro_title');
        $pro_list = ArrayHelper::getArrayValue($params, 'pro_list');
        $contra_title = ArrayHelper::getArrayValue($params, 'contra_title');
        $contra_list = ArrayHelper::getArrayValue($params, 'contra_list');

        #die(json_encode($_POST));

        #$product = $product_manager->getById($product_id);
        $product_manager->setMeta($product_id, 'pro-titel', $pro_title);
        $product_manager->setMeta($product_id, 'pro-liste', $pro_list);
        $product_manager->setMeta($product_id, 'kontra-titel', $contra_title);
        $product_manager->setMeta($product_id, 'kontra-liste', $contra_list);

        $this->sendOutput("Daten gespeichert");
    }

    protected function getMethodPostMappings(){
        return [
            'save' => 'saveProduct',
            'data/save' => 'saveProductData',
            'datafield/save' => 'saveProductDatafield',
            'datagroup/save' => 'saveProductDatagroup',
            'category/save' => 'saveProductCategory',
            'link/save' => 'saveProductLink',
            'shop/save' => 'saveProductShop',
            'feed/save' => 'saveProductFeed',
            'comparison/save' => 'saveProductComparison',
            'update_pro_contra_list' => 'updateProContraList'
        ];
    }
}