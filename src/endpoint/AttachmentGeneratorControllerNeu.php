<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\AttachmentImageGeneratorService;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class AttachmentGeneratorControllerNeu extends AdminRestController {
    function generateParams(){
        $params = array();
        $attachment_template = RequestHelper::reqdouble('attachment_template', null);
        if($attachment_template !== null){
            $params['attachment_template'] = $attachment_template;
        }
        $image_overlay_opacity = RequestHelper::reqdouble('image-overlay-opacity', null);
        if($image_overlay_opacity !== null){
            $params['image-overlay-opacity'] = floatval($image_overlay_opacity)/100;
        }
        $vertical_text_offset = RequestHelper::reqdouble('vertical-text-offset', null);
        if($vertical_text_offset !== null){
            $params['vertical-text-offset'] = $vertical_text_offset;
        }
        $image_background_url = RequestHelper::reqstr('image-background-url', null);
        if($image_background_url !== null){
            $params['image-background-url'] = $image_background_url;
        }

        $params['show_logo'] = RequestHelper::reqstr('show_logo') === "true";
        $params['show_logo_as_watermark'] = RequestHelper::reqstr('show_logo_as_watermark') === "true";
        $params['vertical-logo-offset'] = RequestHelper::reqint('vertical-logo-offset');

        return $params;
    }

    /**
     * "/attachment_generator/generateSocialImage" Endpoint
     */
    public function generateAttachment($segments=null, $params=null){
        $template = RequestHelper::reqstr('image-template');
        $text = RequestHelper::reqstr('image-content');
        $params = $this->generateParams();

        $attachment_generator_service = new AttachmentImageGeneratorService();
        echo $attachment_generator_service->generateAttachmentImage($template, $text, 'tmp', $params);
    }

    protected function getMethodPostMappings(){
        return [
            'generateAttachment' => 'generateAttachment'
        ];
    }
}