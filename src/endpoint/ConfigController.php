<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\ErrorHelper;
use saschahuber\saastemplatecore\persistence\ConfigRepository;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\service\ConfigService;

class ConfigController extends AdminRestController
{

    /**
     * "/config/update" Endpoint
     */
    public function updateConfig($segments = null, $params = null)
    {
        global $DB;

        if(!(AuthHelper::getPermissionService())->hasTablePermission(ConfigRepository::BASE_TABLE)){
            ErrorHelper::api(ErrorHelper::HTTP_ERROR_NO_PERMISSION, "User has no permission to change config");
        }

        $option_name = ArrayHelper::getArrayValue($params, 'name');
        $option_value = ArrayHelper::getArrayValue($params, 'value');

        $config_service = new ConfigService();

        return $config_service->updateConfigField($option_name, $option_value);
    }

    protected function getMethodPostMappings()
    {
        return [
            'update' => 'updateConfig'
        ];
    }
}