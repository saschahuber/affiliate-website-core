<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use ArticleGeneratorService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class BlogPostGeneratorController extends AdminRestController
{

    /**
     * "/blog_post_generator/createArticleContentList" Endpoint
     */
    public function createArticleContentList($segments = null, $params = null)
    {
        $title = ArrayHelper::getArrayValue($params, 'title');

        $blog_post_generator_service = new ArticleGeneratorService();
        $result_list = $blog_post_generator_service->createArticleContentList($title);

        $this->jsonResponse($result_list);
    }

    /**
     * "/blog_post_generator/createArticleParagraph" Endpoint
     */
    public function createArticleParagraph($segments = null, $params = null)
    {
        $post_title = ArrayHelper::getArrayValue($params, 'post_title');
        $headline = ArrayHelper::getArrayValue($params, 'headline');

        $blog_post_generator_service = new ArticleGeneratorService();
        echo $blog_post_generator_service->createArticleParagraph($post_title, $headline);
    }

    protected function getMethodPostMappings()
    {
        return [
            'generateAiCompanyDescription' => 'generateAiCompanyDescription'
        ];
    }
}