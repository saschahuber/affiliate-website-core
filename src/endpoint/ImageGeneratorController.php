<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use saschahuber\affiliatewebsitecore\service\ImageGeneratorServiceNeu;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;

class ImageGeneratorController extends AdminRestController {

    /**
     * "/image_generator/generateImage" Endpoint
     */
    public function generateImage($segments=null, $params=null){
        global $CONFIG;

        $width = RequestHelper::reqint('width');
        $height = RequestHelper::reqint('height');
        $background_color = RequestHelper::reqstr('background_color');
        $save_path = RequestHelper::reqstr('save_path', null);
        $layer_params = json_decode(RequestHelper::reqstr('layer_params_json'), true);

        $image_generator_service = new ImageGeneratorServiceNeu();
        $image_path = $image_generator_service->generateImageFromParams($width, $height, $background_color, $layer_params, $save_path);

        $image_src = str_replace(API_BASE . '/../app/..', $CONFIG->website_domain, $image_path);

        echo json_encode([
            'image_path' => $image_path,
            'image_src' => $image_src
        ]);
    }

    protected function getMethodPostMappings(){
        return [
            'generateImage' => 'generateImage'
        ];
    }
}