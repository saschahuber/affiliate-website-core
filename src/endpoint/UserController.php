<?php


namespace saschahuber\affiliatewebsitecore\endpoint;

use Exception;
use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\affiliatewebsitecore\manager\AdManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\service\AdService;
use saschahuber\affiliatewebsitecore\service\AdTrackingService;
use saschahuber\affiliatewebsitecore\service\ImprintCopyrightService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\AuthHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use saschahuber\saastemplatecore\rest_controller\RestController;
use saschahuber\saastemplatecore\service\UserService;
use stdClass;

class UserController extends AdminRestController
{
    /**
     * "/user/save" Endpoint
     */
    public function saveUser($segments = null, $params = null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_USERS);

        $user_service = new UserService();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $user_service->getByUserId($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
        }

        $roles = explode(',', ArrayHelper::getArrayValue($params, 'roles')[0]);

        $user_service->updateRoles($item_id, $roles);

        echo $item_id;
    }

    protected function getMethodPostMappings()
    {
        return [
            'save' => 'saveUser'
        ];
    }
}