<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\service\CrmService;
use saschahuber\affiliatewebsitecore\service\role_permission\AffiliateWebsitePermissionService;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use saschahuber\saastemplatecore\rest_controller\AuthenticatedRestController;
use stdClass;

#[AllowDynamicProperties]
class CrmController extends AdminRestController{

    /**
     * "/crm/contact/save" Endpoint
     */
    public function saveContact($segments = null, $params = null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_CRM);

        $crm_service = new CrmService();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $crm_service->getContact($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
        }

        $item->name = RequestHelper::reqstr('name');
        $item->website = RequestHelper::reqstr('website');
        $item->email = RequestHelper::reqstr('email');
        $item->status = RequestHelper::reqstr('status');

        $item->tags = RequestHelper::reqstr('tags');

        $item->linked_element_type = RequestHelper::reqstr('linked_element_type');
        $item->linked_element_id = RequestHelper::reqint('linked_element_id');

        if($new_item){
            $item->id = $crm_service->createContact($item);
            $item_id = $item->id;
        }
        else {
            $crm_service->updateContact($item);
        }

        echo $item_id;
    }

    /**
     * "/crm/person/save" Endpoint
     */
    public function savePerson($segments = null, $params = null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_CRM);

        $crm_service = new CrmService();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $crm_service->getContactPerson($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
        }

        $item->first_name = RequestHelper::reqstr('first_name');
        $item->last_name = RequestHelper::reqstr('last_name');
        $item->email = RequestHelper::reqstr('email');
        $item->position = RequestHelper::reqstr('position');

        $item->crm__contact_id = RequestHelper::reqint('crm__contact_id');

        if($new_item){
            $item->id = $crm_service->createContactPerson($item);
            $item_id = $item->id;
        }
        else {
            $crm_service->updateContactPerson($item);
        }

        echo $item_id;
    }

    /**
     * "/crm/note/save" Endpoint
     */
    public function saveNote($segments = null, $params = null){
        AuthenticatedRestController::needsPermission(AffiliateWebsitePermissionService::EDIT_CRM);

        $crm_service = new CrmService();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $crm_service->getNote($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();

            $contact_id = RequestHelper::reqint('contact_id');
            $person_id = RequestHelper::reqint('person_id');

            if($contact_id){
                $item->linked_element_type = CrmService::NOTE_TYPE_COMPANY;
                $item->linked_element_id = $contact_id;
            }
            else {
                $item->linked_element_type = CrmService::NOTE_TYPE_PERSON;
                $item->linked_element_id = $person_id;
            }
        }

        $item->title = RequestHelper::reqstr('title');
        $item->content = RequestHelper::reqstr('content');

        if($new_item){
            $item->id = $crm_service->createNote($item);
            $item_id = $item->id;
        }
        else {
            $crm_service->updateNote($item);
        }

        echo $item_id;
    }

    protected function getMethodPostMappings(){
        return [
            'contact/save' => 'saveContact',
            'person/save' => 'savePerson',
            'note/save' => 'saveNote'
        ];
    }
}