<?php

namespace saschahuber\affiliatewebsitecore\endpoint;

use AllowDynamicProperties;
use saschahuber\affiliatewebsitecore\helper\ThumbnailHelper;
use saschahuber\affiliatewebsitecore\manager\LinkPageItemManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\manager\PageManager;
use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\service\ContentScheduleItemService;
use saschahuber\affiliatewebsitecore\service\PageService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\RequestHelper;
use saschahuber\saastemplatecore\helper\UrlHelper;
use saschahuber\saastemplatecore\rest_controller\AdminRestController;
use stdClass;

#[AllowDynamicProperties]
class PageController extends AdminRestController {

    /**
     * "/page/save" Endpoint
     */
    public function savePage($segments=null, $params=null){

        $page_manager = new PageManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $page_manager->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
            $item->status = Manager::STATUS_DRAFT;
        }

        $item->title = RequestHelper::reqstr('title');

        $item->permalink = RequestHelper::reqstr('permalink');
        if($item->permalink === null || strlen($item->permalink) < 1) {
            $item->permalink = UrlHelper::cleanPermalink($item->title);
        }
        else{
            $item->permalink = UrlHelper::cleanPermalink($item->permalink);
        }

        $item->meta_title = RequestHelper::reqstr('meta_title');
        $item->meta_description = RequestHelper::reqstr('meta_description');
        $item->og_title = RequestHelper::reqstr('og_title');
        $item->og_description = RequestHelper::reqstr('og_description');

        $item->attached_faq_id = RequestHelper::reqint('attached_faq_id');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        $item->dynamic_image_type = RequestHelper::reqstr('dynamic_image_type');
        $item->dynamic_image_id = RequestHelper::reqint('dynamic_image_id');

        $item->h1 = RequestHelper::reqstr('h1');
        $item->subtitle = RequestHelper::reqstr('subtitle');

        $item->vg_wort_pixel_id = RequestHelper::reqint('vg_wort_pixel_id');

        $item->doindex = RequestHelper::reqbool('doindex');
        $item->dofollow = RequestHelper::reqbool('dofollow');

        $item->content = RequestHelper::reqstr('content');

        if($new_item){
            $item->id = $page_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $page_manager->updateItem($item);
            ThumbnailHelper::deleteThumbnail(ThumbnailHelper::TYPE_PAGE, $item_id);
        }

        //Link Content schedule item
        $content_schedule_item_id = RequestHelper::reqint('linked_content_schedule_item_id');
        $content_schedule_item_service = new ContentScheduleItemService();
        if($content_schedule_item_id) {
            $content_schedule_item_service->linkToTypeAndItem($content_schedule_item_id, PageManager::TYPE_PAGE, $item_id);
        }
        else {
            $content_schedule_item_service->unlinkElement(PageManager::TYPE_PAGE, $item_id);
        }

        echo $item_id;
    }

    /**
     * "/page/link/save" Endpoint
     */
    public function savePageLink($segments=null, $params=null){

        $link_page_item_manager = new LinkPageItemManager();

        $new_item = false;

        $item_id = RequestHelper::reqint('id');

        $item = $link_page_item_manager->getById($item_id);

        if($item === null){
            $new_item = true;
            $item = new stdClass();
            $item->status = Manager::STATUS_DRAFT;
        }

        $item->title = RequestHelper::reqstr('title');
        $item->content = RequestHelper::reqstr('content');

        $item->link_page_item_date = RequestHelper::reqstr('link_page_item_date');

        $item->linked_item_type = RequestHelper::reqstr('linked_item_type');
        $item->linked_item_id = RequestHelper::reqint('linked_item_id');

        $item->button_text = RequestHelper::reqstr('button_text');
        $item->button_url = RequestHelper::reqstr('button_url');

        $item->attachment_id = RequestHelper::reqint('attachment_id');

        if($new_item){
            $item->id = $link_page_item_manager->createItem($item);
            $item_id = $item->id;
        }
        else {
            $link_page_item_manager->updateItem($item);
        }

        echo $item_id;
    }

    /**
     * "/page/delete" Endpoint
     */
    public function deletePage($segments=null, $params=null){
        $page_id = ArrayHelper::getArrayValue($params, 'item_id');

        $page_service = new PageService();
        $page_service->deletePage($page_id);
    }

    protected function getMethodPostMappings(){
        return [
            'save' => 'savePage',
            'link/save' => 'savePageLink',
            'delete' => 'deletePage'
        ];
    }
}