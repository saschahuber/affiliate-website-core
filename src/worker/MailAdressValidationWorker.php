<?php

namespace saschahuber\affiliatewebsitecore\worker;

use saschahuber\saastemplatecore\helper\DateHelper;
use saschahuber\saastemplatecore\persistence\UserRepository;
use saschahuber\saastemplatecore\service\MailAdressValidatorService;
use saschahuber\saastemplatecore\worker\AbstractWorker;

class MailAdressValidationWorker extends AbstractWorker {
    function runTask(){
        global $CONFIG;

        $mail_adress_validator_service = new MailAdressValidatorService();
        
        $user_id = $this->task->data['user_id'];
        $mail_to_validate = $this->task->data['mail'];

        $mail_valid = $mail_adress_validator_service->isMailAdressValid($mail_to_validate, $CONFIG->general_email_address);

        $user_repository = new UserRepository();
        $user_repository->updateField('email_valid', $mail_valid, $user_id);
        $user_repository->updateField('email_valid_datetime', DateHelper::toDbDatetime(null), $user_id);
        $user_repository->updateField('email_validation_scheduled', false, $user_id);

        $this->updateState($this->task->data, 1, 1);

        return true;
    }
}