<?php

namespace saschahuber\affiliatewebsitecore\worker;

use saschahuber\affiliatewebsitecore\persistence\CrawlUrlAnalysisResultRepository;
use saschahuber\affiliatewebsitecore\persistence\CrawlUrlLinkRepository;
use saschahuber\affiliatewebsitecore\persistence\CrawlUrlRepository;
use saschahuber\affiliatewebsitecore\persistence\CrawlUrlResultRepository;
use saschahuber\affiliatewebsitecore\service\CrawlerService;
use saschahuber\affiliatewebsitecore\service\PagespeedService;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\worker\AbstractWorker;

class CrawlingWorker extends AbstractWorker {
    function runTask(){
        $crawl_url_repository = new CrawlUrlRepository();
        $crawl_url_result_repository = new CrawlUrlResultRepository();
        $crawl_url_link_repository = new CrawlUrlLinkRepository();
        $crawl_url_analysis_result_repository = new CrawlUrlAnalysisResultRepository();

        $crawler_service = new CrawlerService();

        $url_ids_to_crawl = $this->task->data;
        $max_numbers = count($url_ids_to_crawl);

        #print_r($this->task->data);

        $this->updateState($this->task->data, 0, $max_numbers);

        $crawl_url_cache = [];

        for($index = 0; $index < $max_numbers; $index++){
            $current_crawl_url_id = $url_ids_to_crawl[$index];
            $crawl_url = $crawl_url_repository->getById($current_crawl_url_id);

            $crawl_url_cache[$current_crawl_url_id] = $crawl_url->url;

            echo "Running iteration ".($index+1)."/$max_numbers => Crawling URL '{$crawl_url->url}'" . PHP_EOL;

            $result = $crawler_service->crawlUrl($crawl_url->url);

            #echo json_encode($result);

            #$content = $result['content'];
            $status = $result['status'];
            $redirect_url = $result['redirect_url'];
            $internal_links = $result['internal_links'];

            $redirect_url_id = null;
            //Add redirect_url to found links (if not yet existing) => crawl_url
            if($redirect_url){
                #echo "Redirect-URL: ".json_encode($redirect_url) . PHP_EOL;
                $redirected_crawl_url = $crawl_url_repository->getByUrl($redirect_url['url']);

                if(!$redirected_crawl_url){
                    $redirect_url_id = $crawl_url_repository->addNewUrl($redirect_url['url'], $redirect_url['type']);
                    $crawl_url_cache[$redirect_url_id] = $redirect_url['url'];
                }
                else {
                    $redirect_url_id = $redirected_crawl_url->id;
                    $crawl_url_cache[$redirect_url_id] = $redirected_crawl_url->url;
                }
            }

            $crawl_url_link_repository->deleteByStartUrlId($current_crawl_url_id);
            foreach($internal_links as $internal_link){
                #echo "Internal-Link: ".json_encode($internal_link) . PHP_EOL;
                // Add newly found links (if not yet existing) => crawl_url
                $new_crawl_url = $crawl_url_repository->getByUrl($internal_link['url']);

                if(!$new_crawl_url){
                    $new_crawl_url_id = $crawl_url_repository->addNewUrl($internal_link['url'], $internal_link['type']);
                    $crawl_url_cache[$new_crawl_url_id] = $internal_link['url'];
                }
                else {
                    $new_crawl_url_id = $new_crawl_url->id;
                    $crawl_url_cache[$new_crawl_url_id] = $new_crawl_url->id;
                }

                // Add newly found url-links (crawl_url__link)
                $crawl_url_link = new \stdClass();
                $crawl_url_link->start_url_id = $current_crawl_url_id;
                $crawl_url_link->end_url_id = $new_crawl_url_id;
                $crawl_url_link_repository->create($crawl_url_link, true);
            }

            // Add CrawlUrlResult
            $crawl_url_result = new \stdClass();
            $crawl_url_result->crawl_url_id = $current_crawl_url_id;
            $crawl_url_result->http_status = $status;
            $crawl_url_result->redirects_to_url_id = $redirect_url_id;
            $crawl_url_result_id = $crawl_url_result_repository->create($crawl_url_result);

            if($crawl_url->url_type === CrawlUrlRepository::URL_TYPE_WEBSITE) {
                // Do further analysis (pagespeed etc.) and add result to crawl_url__analysis_result
                $pagespeed_result = $this->doPagespeedScan($crawl_url->url);

                if($pagespeed_result) {
                    $crawl_url_analasys_result = new \stdClass();
                    $crawl_url_analasys_result->crawl_url__result_id = $crawl_url_result_id;
                    $crawl_url_analasys_result->analysis_type = 'pagespeed';
                    $crawl_url_analasys_result->analysis_data = json_encode($pagespeed_result);
                    $crawl_url_analysis_result_repository->create($crawl_url_analasys_result);
                }
            }

            #die(json_encode($pagespeed_result));

            if (($key = array_search($current_crawl_url_id, $this->task->data)) !== false) {
                unset($this->task->data[$key]);
            }

            $crawl_url_repository->setScheduled($crawl_url, false);

            $this->updateState($this->task->data, $max_numbers-count($this->task->data), $max_numbers);

            //TODO: Prüfen, ob Task mittlerweile gecancelt  oder pausiert wurde
        }

        return true;
    }

    function doPagespeedScan($page_url){
        $pagespeed_result = (new PagespeedService())->analyzePagespeed($page_url);

        $lighthouseResult = ArrayHelper::getArrayValue($pagespeed_result, 'lighthouseResult', null);

        if(!$lighthouseResult){
            return null;
        }

        #die(json_encode($lighthouseResult));

        $totalTime = $lighthouseResult['timing']['total'];

        $totalBlockingTime = $lighthouseResult['audits']['total-blocking-time']['numericValue'];
        $firstContentfulPaint = $lighthouseResult['audits']['first-contentful-paint']['numericValue'];
        $largestContentfulPaint = $lighthouseResult['audits']['largest-contentful-paint']['numericValue'];
        $cumulativeLayoutShift = $lighthouseResult['audits']['cumulative-layout-shift']['numericValue'];

        $score = $lighthouseResult['categories']['performance']['score'];

        return [
            'total_blocking_time' => $totalBlockingTime,
            'first_contentful_paint' => $firstContentfulPaint,
            'total_time' => $totalTime,
            'score' => $score
        ];
    }
}