<?php

namespace saschahuber\affiliatewebsitecore\builder;

use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\affiliatewebsitecore\service\ImageGeneratorService;
use saschahuber\saastemplatecore\helper\FileHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\ImgUtils;

class ImageBuilder
{
    private $image;
    private $width, $height;

    private $font = DM_SANS_FONT_DIR . '/DMSans-Bold.ttf';

    public function __construct($width, $height, $background_color = '#fff')
    {
        $this->width = $width;
        $this->height = $height;
        $this->image = $this->createBlankImage($width, $height, $background_color);
        return $this;
    }

    static function formatText($text, $max_chars_per_line = 20)
    {
        $formatted_text = "";
        $words = explode(' ', $text);

        $line = "";
        foreach ($words as $word) {
            $word_len = strlen($word);

            if (strlen($formatted_text) > 0 && strlen($line) > 0 && $word_len > $max_chars_per_line) {
                $formatted_text .= trim($line) . "\n";
                $line = "";
            }

            $line .= ' ' . $word;

            if (strlen($line) >= $max_chars_per_line) {
                $formatted_text .= trim($line) . "\n";
                $line = "";
            }
        }

        $formatted_text .= trim($line);

        return trim($formatted_text);
    }

    private function withFont($font_path)
    {
        $this->font = $font_path;
    }

    private function createBlankImage($width, $height, $color = "#FFFFFF")
    {
        $img = imagecreatetruecolor($width, $height);
        $rgb = self::convertHexToRgb($color);
        $bg = imagecolorallocate($img, $rgb[0], $rgb[1], $rgb[2]);
        imagefilledrectangle($img, 0, 0, $width, $height, $bg);
        return $img;
    }

    public function withBackgroundImage($image_src, $alignment = ImageGeneratorService::Center, $vertical_margin = 0, $horizontal_margin = 0)
    {
        $max_width = 0;
        $max_height = 0;

        if (file_exists($image_src)) {
            $img = (new ImgUtils())->loadImgFile($image_src);
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $image_src);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // good edit, thanks!
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); // also, this seems wise considering output is image.
            $data = curl_exec($ch);
            curl_close($ch);
            $img = imagecreatefromstring($data);
        }

        $img_width = imagesx($img);
        $img_height = imagesy($img);

        $background_ratio = ((float)$img_width) / ((float)$img_height);
        $img_ratio = ((float)$this->width) / ((float)$this->height);

        if ($background_ratio < $img_ratio) {
            $max_width = $this->width;
        } else {
            $max_height = $this->height;
        }

        return $this->withAlignedImage($image_src, $alignment, $vertical_margin, $horizontal_margin, $max_width, $max_height);
    }

    static function convertHexToRgb($hexcode)
    {
        list($r, $g, $b) = sscanf($hexcode, "#%02x%02x%02x");
        return array($r, $g, $b);
    }

    function withAttachmentImage($template, $alignment = ImageGeneratorService::Center, $vertical_offset = 0, $horizontal_offset = 0)
    {
        $parts = explode('__', $template);

        $background_image_path = null;

        if ($parts[0] === "stock") {
            $background_image_path = APP_BASE . '/..' . (new StockImageManager())->getAttachmentUrl($parts[1]);
        } else if ($parts[0] === "attachment") {
            $background_image_path = APP_BASE . '/..' . (new ImageManager())->getAttachmentUrl($parts[1]);
        }

        return $this->withBackgroundImage($background_image_path, $alignment, $vertical_offset, $horizontal_offset);
    }

    function withAlignedImage(string $image_src, int $alignment = ImageGeneratorService::Center, int $vertical_margin = 0,
                              int    $horizontal_margin = 0, int $max_dst_width = 0, int $max_dst_height = 0, array $background_color = null)
    {
        #die($image_src);

        $img_src = (new ImgUtils())->loadImgFile($image_src);

        /*
        if(file_exists($image_src)){
            $img_src = (new ImgUtils())->loadImgFile($image_src);
        }
        else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $image_src);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // good edit, thanks!
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1); // also, this seems wise considering output is image.
            $data = curl_exec($ch);
            curl_close($ch);
            $img_src = imagecreatefromstring($data);
        }
        */

        $dst_width = $img_width = imagesx($img_src);
        $dst_height = $img_height = imagesy($img_src);

        if ($max_dst_width) {
            $scale = (float)$max_dst_width / (float)$dst_width;
            $dst_width *= $scale;
            $dst_height *= $scale;
        }

        if ($max_dst_height) {
            $scale = (float)$max_dst_height / (float)$dst_height;
            $dst_width *= $scale;
            $dst_height *= $scale;
        }

        imageAlphaBlending($img_src, true);
        imageSaveAlpha($img_src, true);

        $x = $this->width;
        $y = $this->height;

        switch ($alignment) {
            default:
            case ImageGeneratorService::TopLeft:
                $x = $horizontal_margin;
                break;
            case ImageGeneratorService::TopCenter:
                $x = ($this->width * 0.5) - ($dst_width * 0.5);
                $y = $vertical_margin;
                break;
            case ImageGeneratorService::TopRight:
                $x = $this->width - $dst_width - $horizontal_margin;
                $y = $vertical_margin;
                break;
            case ImageGeneratorService::CenterLeft:
                $y = $this->height * 0.5 - $dst_height * 0.5 - $vertical_margin;
                break;
            case ImageGeneratorService::Center:
                $x = $this->width * 0.5 - $dst_width * 0.5 - $horizontal_margin;
                $y = $this->height * 0.5 - $dst_height * 0.5 - $vertical_margin;
                break;
            case ImageGeneratorService::CenterRight:
                $x = $this->width - $dst_width - $horizontal_margin;
                $y = $this->height * 0.5 - $dst_height * 0.5 - $vertical_margin;
                break;
            case ImageGeneratorService::BottomLeft:
                $y = $this->height - $dst_height - $horizontal_margin;
                break;
            case ImageGeneratorService::BottomCenter:
                $x = $this->width * 0.5 - $dst_width * 0.5 - $horizontal_margin;
                $y = $this->height - $dst_height - $vertical_margin;
                break;
            case ImageGeneratorService::BottomRight:
                $x = $this->width - $dst_width - $horizontal_margin;
                $y = $this->height - $dst_height - $vertical_margin;
                break;
        }

        #die(json_encode([$x, $y, $img_width, $img_height, $dst_width, $dst_height]));

        if ($background_color) {
            imageFilledRectangle($this->image,
                $x - 5, $y - 5,                               # point top left coordinates
                $x + $dst_width + 5, $y + $dst_height + 5,    # point bottom right coordinates
                imageColorAllocate($this->image, $background_color[0], $background_color[1], $background_color[2]));
        }

        if ($dst_width != $img_width || $dst_height != $img_height) {
            imageCopyResized($this->image, $img_src,
                $x, $y,                                 # destination coordinates
                0, 0,                                   # source coordinates
                $dst_width, $dst_height,                # destination size
                $img_width, $img_height);               # source size
        } else {
            imageCopy($this->image, $img_src,
                $x, $y,                                 # destination coordinates
                0, 0,                                   # source coordinates
                $img_width, $img_height);               # source size
        }

        imageDestroy($img_src);

        return $this;
    }

    function withWatermarkLogo($vertical_offset = false, $alignment = ImageGeneratorService::TopCenter, $size_multiplier = 0.4, $horizontal_margin = 0)
    {
        global $CONFIG;

        $watermark_logo = $CONFIG->watermark_logo;

        $size = $this->height * $size_multiplier;

        $vertical_margin = $size * 0.2;

        if ($vertical_offset) {
            $vertical_margin = $vertical_offset;
        }

        return $this->withAlignedImage($watermark_logo, $alignment, $vertical_margin, $horizontal_margin, $size, $size);
    }

    function withFooterWatermark($watermark_text, $color = null, $size = 36)
    {
        if ($color === null) {
            $color = "#666666";
        }
        $max_width = $this->width * 0.8;
        return $this->withAlignedText($watermark_text, $size, $max_width, $color, ImageGeneratorService::BottomCenter, 32);
    }

    function withCenteredText(string $text, int $initial_size = 32, int $max_chars = 80, int $max_lines = 6,
                              string $text_color = "#000000", $vertical_offset = 0, $line_height_multiplier = 1.7)
    {
        $lines = explode("\n", $text);

        $color = $this->convertHexToRgb($text_color);

        $size = $initial_size;

        for ($i = 0; $i < count($lines); $i++) {
            if (strlen($lines[$i]) > $max_chars) {
                $lines[$i] = substr($lines[$i], 0, $max_chars);
            }

            $text_box = imageTTFbBox($size, 0, $this->font, $lines[$i] . '...');
            #$line_height = abs($text_box[7]) + abs($text_box[1]);
            $line_length = abs($text_box[6]) + abs($text_box[4]);

            // Reduce font size until the line fits within 80% of the bg-image's width
            if ($line_length > $this->width * 0.90) {
                $size -= 1;
                $i--;
                continue;
            }
        }

        for ($i = 0; $i < count($lines); $i++) {
            $text_box = imageTTFbBox($size, 0, $this->font, $lines[$i]);
            #$line_height = abs($text_box[7]) + abs($text_box[1]);
            $line_height = $size * $line_height_multiplier;
            $line_length = abs($text_box[6]) + abs($text_box[4]);

            $offset = 0;#$line_height * (count($lines) - 1) * 0.5;

            imageTTFText($this->image, $size, 0,
                $this->width * 0.5 - ($text_box[4] * 0.5),                                     # x
                $this->height * 0.5 - ($text_box[7] * 0.5) - $offset + ($i * $line_height) + $vertical_offset,    # y
                imageColorAllocate($this->image, $color[0], $color[1], $color[2]),          # color
                $this->font, $lines[$i] . Helper::ifstr($i === $max_lines, '...'));

            if ($i === $max_lines - 1) {
                break;
            }
        }
        return $this;
    }

    function withAlignedText($text, $size = 32, $max_width = null, $hexcode = "#000000",
                             $alignment = ImageGeneratorService::TopLeft, $margin = 0)
    {
        $color = $this->convertHexToRgb($hexcode);

        $text_box = imageTTFbBox($size, 0, $this->font, $text);

        while ($size) {
            #$line_height = abs($text_box[7]) + abs($text_box[1]);
            $line_length = abs($text_box[6]) + abs($text_box[4]);

            // Reduce font size until the line fits within 80% of the bg-image's width
            if ($line_length > $this->width * 0.95) {
                $size -= 1;
                $text_box = imageTTFbBox($size, 0, $this->font, $text);
            } else {
                break;
            }
        }

        $line_height = abs($text_box[7]) + abs($text_box[1]);
        $line_length = abs($text_box[6]) + abs($text_box[4]);

        $x = $margin;
        $y = $line_height + $margin;

        switch ($alignment) {
            default:
            case ImageGeneratorService::TopLeft:
                break;
            case ImageGeneratorService::TopCenter:
                $x = $this->width * 0.5 - $line_length * 0.5;
                break;
            case ImageGeneratorService::TopRight:
                $x = $this->width - $line_length - $margin;
                break;
            case ImageGeneratorService::CenterLeft:
                $y = $this->height * 0.5 - $line_height * 0.5;
                break;
            case ImageGeneratorService::CenterRight:
                $x = $this->width - $line_length - $margin;
                $y = $this->height * 0.5 - $line_height * 0.5;
                break;
            case ImageGeneratorService::BottomLeft:
                $y = $this->height - $margin;
                break;
            case ImageGeneratorService::BottomCenter:
                $x = $this->width * 0.5 - $line_length * 0.5;
                $y = $this->height - $margin;
                break;
            case ImageGeneratorService::BottomRight:
                $x = $this->width - $line_length - $margin;
                $y = $this->height - $margin;
                break;
            case ImageGeneratorService::Center:
                $x = $this->width * 0.5 - $line_length * 0.5;
                $y = $this->height * 0.5 - $line_height * 0.5;
                break;
        }

        imageTTFText($this->image, $size, 0,
            $x, $y,
            imageColorAllocate($this->image, $color[0], $color[1], $color[2]),
            $this->font, $text);
        return $this;
    }

    function withTint(string $hexcode = "#000000", float $opacity = 0.5)
    {
        $color = $this->convertHexToRgb($hexcode);

        $overlay = imageCreate($this->width, $this->height);
        imageFill($overlay, 0, 0, imageColorAllocateAlpha($overlay, $color[0], $color[1], $color[2], 127 - intval($opacity * 127)));
        imageCopy($this->image, $overlay, 0, 0, 0, 0, $this->width, $this->height);
        return $this;
    }

    function withFrame(string $hexcode = "#000000", int $thickness = 1, int $margin = 0)
    {
        $color = $this->convertHexToRgb($hexcode);
        imageSetThickness($this->image, $thickness);
        imageRectangle($this->image,
            $margin, $margin,                                   # point top left coordinates
            $this->width - $margin, $this->height - $margin,    # point bottom right coordinates
            imageColorAllocate($this->image, $color[0], $color[1], $color[2]));
        return $this;
    }

    function withRibbon(string $hexcode = "#000000", float $opacity = 0.75, int $size = 128)
    {
        $color = $this->convertHexToRgb($hexcode);
        imageFilledRectangle($this->image,
            0, $this->height * 0.5 - $size * 0.5,               # point top left coordinates
            $this->width, $this->height * 0.5 + $size * 0.5,    # point bottom right coordinates
            imageColorAllocateAlpha($this->image, $color[0], $color[1], $color[2], 127 - $opacity * 127));
        return $this;
    }

    function withPolygon(string $hexcode = "#000000", $corner = ImageGeneratorService::TopLeft, float $scale_x = 1.0, float $scale_y = 1.0)
    {
        $color = $this->convertHexToRgb($hexcode);
        switch ($corner) {
            default:
            case ImageGeneratorService::TopLeft:
                $points = [
                    0, 0,
                    440 * $scale_x, 0,
                    360 * $scale_x, 140 * $scale_y,
                    0, 200 * $scale_y];
                break;
            case ImageGeneratorService::TopRight:
                $points = [
                    $this->width - 440 * $scale_x, 0,
                    $this->width, 0,
                    $this->width, 200 * $scale_y,
                    $this->width - 360 * $scale_x, 140 * $scale_y];
                break;
            case ImageGeneratorService::BottomLeft:
                $points = [
                    0, $this->height - 200 * $scale_y,
                    360 * $scale_x, $this->height - 140 * $scale_y,
                    440 * $scale_x, $this->height,
                    0, $this->height];
                break;
            case ImageGeneratorService::BottomRight:
                $points = [
                    $this->width - 360 * $scale_x, $this->height - 140 * $scale_y,
                    $this->width, $this->height - 200 * $scale_y,
                    $this->width, $this->height,
                    $this->width - 440 * $scale_x, $this->height];
                break;
        }

        imageSetThickness($this->image, 1);
        imageFilledPolygon($this->image,
            $points, count($points) * 0.5,
            imageColorAllocate($this->image, $color[0], $color[1], $color[2]));
        return $this;
    }

    function withBox($x, $y, $width, $height, $hexcode = "#000000", $opacity = 1.0,
                     $border_color = "#000000", $border_opacity = 0.0, $border_width = 2)
    {
        $color = self::convertHexToRgb($hexcode);
        $border_color = self::convertHexToRgb($hexcode);
        imageFilledRectangle($this->image,
            $x, $y,                         # point top left coordinates
            $x + $width, $y + $height,      # point bottom right coordinates
            imageColorAllocateAlpha($this->image, $color[0], $color[1], $color[2], 127 - $opacity * 127));

        if ($border_opacity > 0.0) {
            imageSetThickness($this->image, $border_width);
            imageRectangle($this->image,
                $x, $y,                         # point top left coordinates
                $x + $width, $y + $height,      # point bottom right coordinates
                imageColorAllocateAlpha($this->image,
                    $border_color[0], $border_color[1], $border_color[2], 127 - $border_opacity * 127));
        }
        return $this;
    }

    public function displayImage($png = false)
    {
        if ($png) {
            header('Content-Type: image/png');
            imagepng($this->image);
        } else {
            header('Content-Type: image/jpeg');
            imagejpeg($this->image);
        }
    }

    public function saveImage($path)
    {
        FileHelper::createDirIfNotExists(dirname($path));

        if (substr_compare($path, ".png", -strlen(".png")) === 0) {
            imagePNG($this->image, $path);
        } else {
            imagejpeg($this->image, $path);
        }


        imageDestroy($this->image);

        return $path;
    }
}