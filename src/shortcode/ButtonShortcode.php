<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\Helper;

class ButtonShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $left_icon = false;
        $right_icon = false;

        $url = ArrayHelper::getArrayValue($atts, 'href', '#');
        $color = ArrayHelper::getArrayValue($atts, 'color', 'btn-primary');
        $shape = 'btn-round';#ArrayHelper::getArrayValue($atts, 'shape', 'btn-rounded');
        $size = ArrayHelper::getArrayValue($atts, 'size', 'btn-md');
        $icon = ArrayHelper::getArrayValue($atts, 'icon', false);
        $icon_position = ArrayHelper::getArrayValue($atts, 'icon_position', 'left');
        $target = ArrayHelper::getArrayValue($atts, 'target', '');
        $rel = ArrayHelper::getArrayValue($atts, 'rel', 'follow');
        $block = ArrayHelper::getArrayValue($atts, 'block', false);
        $onclick = ArrayHelper::getArrayValue($atts, 'onclick', false);

        if($icon) {
            $icon = '<i class="'.(Helper::ifstr(count(explode(" ", $icon))<2, "fa ")).$icon.'"></i>';
            if ($icon_position == "left") {
                $left_icon = "$icon&nbsp;";
            }
            else{
                $right_icon = "&nbsp;$icon";
            }
        }

        ob_start();

        ?>
        <a href="<?=$url?>"
           class="btn <?=$color?> <?=$shape?> <?=$size?> <?=Helper::ifstr($block==="true", "w-100")?> active"
           role="button" rel="<?=$rel?>" target="<?=$target?>" <?=($onclick?'onclick="'.$onclick.'"':'')?>
           aria-pressed="true"><?=($left_icon?:"")?><?=$content?><?=($right_icon?:"")?></a>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($output);
    }

    public function getTag()
    {
        return "button";
    }
}