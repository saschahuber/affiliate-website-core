<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;

class NewsletterNewsShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        ob_start();

        ?>
        <div style="max-width: 800px; margin: auto;">
            <p style="text-align: center;"><strong>Du möchtest keine News mehr verpassen?</strong></p>
            <p style="text-align: center;">Dann abonniere doch unseren Newsletter!</p>

            [newsletter_form]
        </div>
        <?php

        $content = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($content);
    }

    public function getTag()
    {
        return "newsletter_news";
    }
}