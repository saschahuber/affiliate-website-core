<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;

class IconButtonInfoShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $icon = $atts['icon'];
        $title = $atts['title'];
        $text = $atts['text'];
        $btn_url = $atts['btn_url'];
        $btn_text = $atts['btn_text'];


        ob_start();

        ?>

        <div class="icon-button-info">
            <div class="card" style="border-radius: var(--card-custom-border-radius); padding: 20px; border: none; box-shadow: 0px 3px 20px #0000001A; margin: auto; max-width: 400px;">
                <?php
                if(strpos($icon, 'fa') === 0) {
                    echo '[fa_icon icon="'.$icon.'" icon_color="#ffffff" background_color="#0099cc"]';
                }
                else {
                    echo '<img class="icon-button-info-icon" src="'.$icon.'">';
                }
                ?>

                <p style="color: var(--primary_color); font-size: 24px; margin: 10px auto; text-align: center;"><strong><?=$title?></strong></p>

                <div class="content" style="display: table;">
                    <p style="text-align: center; margin: 0;"><?=$text?></p>
                </div>

                [button color="btn-primary" size="btn-xl" shape="btn-round" outline="false" block="true" icon="fa-chevron-right" icon_position="right" target="" rel="" href="<?=$btn_url?>"]<?=$btn_text?>[/button]
            </div>
        </div>

        <?php

        $content = ob_get_contents();
        ob_end_clean();

        return ShortcodeHelper::doShortcode($content);
    }

    public function getTag()
    {
        return "icon_button_info";
    }
}