<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\service\NewsletterService;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class SidebarNewsletterShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $topics = NewsletterService::ALL_NEWSLETTER_TOPICS;

        $hide_topic_selection = ArrayHelper::getArrayValue($atts, 'hide_topic_selection', 'true') === 'true';

        $hide_topic_selection = false;

        ob_start();
        ?>
        <div class="newsletter-form">
            <div>
                <div style="text-align: center;">
                    <form action="/newsletter/anmeldung" method="post">
                        <input name="action" type="hidden" value="register">
                        <div>
                            <?php if($hide_topic_selection === false): ?>
                                <p>Zu welchen Themen möchtest du Updates erhalten?</p>
                                <p>
                                    <?php foreach($topics as $topic => $title): ?>
                                        <label class="topic-selection">
                                            <input style="vertical-align: middle;" name="<?=$topic?>" type="checkbox" checked value="1">&nbsp;<?=$title?>
                                        </label>
                                    <?php endforeach; ?>
                                </p>
                            <?php else: ?>
                                <input name="all_topics" type="hidden" value="1">
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <input id="newsletter-name-input"
                                           type="text" name="name" placeholder="Dein Name (Optional)">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input id="newsletter-mail-input"
                                           type="email" name="email" required="" placeholder="Deine E-Mail Adresse">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input class="btn btn-primary" style="width: 100%; height: 52px;"
                                           type="submit" onclick="saveAnalyticsEvent('newsletter_subscription_sidebar', {email: document.getElementById('newsletter-mail-input').value, name: document.getElementById('newsletter-name-input').value})" value="Jetzt anmelden">
                                </div>
                            </div>
                            <br>
                            <p><label>
                                    <input style="vertical-align: middle;" name="terms" type="checkbox" value="1"
                                           required=""> Ich bin mit der Speicherung meiner E-Mail-Adresse zum Versand des
                                    Newsletters und der Verarbeitung meiner Daten - unter anderem zur Erfolgsmessung durch
                                    Klickraten - einverstanden. Weitere Informationen
                                    in der <a href="/legal/datenschutz/" target="_blank">Datenschutzerklärung</a>.
                                </label>
                            </p>
                        </div>
                        <label style="display: none !important;">Wenn du ein Mensch bist, lasse das Feld leer: <input
                                type="text" name="parity" value="" tabindex="-1" autocomplete="off">
                        </label>
                        <div></div>
                    </form>
                </div>
            </div>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getTag()
    {
        return "sidebar_newsletter_form";
    }
}