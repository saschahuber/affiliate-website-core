<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;

class FaIconShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $icon = $atts['icon'];
        $icon_color = $atts['icon_color'];
        $backgorund_color = $atts['background_color'];

        ob_start();
        ?>

        <div style="background-color: <?=$backgorund_color?>; border-radius: 50%; height: fit-content; width: fit-content; margin: auto; margin-top: 8px; margin-bottom: 8px">
            <i class="<?=$icon?>" style="color: <?=$icon_color?>; font-size: 30px; padding: 20px;"></i>
        </div>

        <?php

        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getTag()
    {
        return "fa_icon";
    }
}