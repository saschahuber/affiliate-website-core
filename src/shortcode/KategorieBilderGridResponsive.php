<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class KategorieBilderGridResponsive extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $cat_ids = explode(',', $atts['cat_ids']);
        $xl_columns = ArrayHelper::getArrayValue($atts, 'xl_columns', 5);
        $lg_columns = ArrayHelper::getArrayValue($atts, 'lg_columns', 4);
        $md_columns = ArrayHelper::getArrayValue($atts, 'md_columns', 3);
        $sm_columns = ArrayHelper::getArrayValue($atts, 'sm_columns', 2);
        $xs_columns = ArrayHelper::getArrayValue($atts, 'xs_columns', 1);
        $max_width = ArrayHelper::getArrayValue($atts, 'max_width', 500);

        $content = '<div class="d-none d-xl-block offset-1-7th">[kategorie_bilder_grid cat_ids="' . implode(',', $cat_ids) . '" columns="' . $xl_columns . '" max_width="' . $max_width . '"]</div>';
        $content .= '<div class="d-none d-lg-block d-xl-none">[kategorie_bilder_grid cat_ids="' . implode(',', $cat_ids) . '" columns="' . $lg_columns . '" max_width="' . $max_width . '"]</div>';
        $content .= '<div class="d-none d-md-block d-lg-none">[kategorie_bilder_grid cat_ids="' . implode(',', $cat_ids) . '" columns="' . $md_columns . '" max_width="' . $max_width . '"]</div>';
        $content .= '<div class="d-none d-sm-block d-md-none">[kategorie_bilder_grid cat_ids="' . implode(',', $cat_ids) . '" columns="' . $sm_columns . '" max_width="' . $max_width . '"]</div>';
        $content .= '<div class="d-block d-sm-none">[kategorie_bilder_grid cat_ids="' . implode(',', $cat_ids) . '" columns="' . $xs_columns . '" max_width="' . $max_width . '"]</div>';

        return ShortcodeHelper::doShortcode($content);
    }

    public function getTag()
    {
        return "kategorie_bilder_grid_responsive";
    }
}