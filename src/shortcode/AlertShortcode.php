<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class AlertShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $style = ArrayHelper::getArrayValue($atts, 'style', 'info');

        ob_start();
        ?>
        <div class="alert alert-<?=$style?>" role="alert">
            <?=$content?>
        </div>
        <?php
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    public function getTag()
    {
        return "alert";
    }
}