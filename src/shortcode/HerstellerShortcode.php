<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\manager\Manager;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;

class HerstellerShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $color = "btn-primary";
        if(is_array($atts) && array_key_exists('color', $atts)){
            $color = $atts['color'];
        }

        $shape = "btn-rounded";
        if(is_array($atts) && array_key_exists('shape', $atts)){
            $shape = $atts['shape'];
        }

        $anchor_link = false;
        if(is_array($atts) && array_key_exists('anchor_link', $atts)){
            $anchor_link = $atts['anchor_link'];
        }

        $brand_manager = new BrandManager();
        $brands = $brand_manager->getAll(Manager::STATUS_PUBLISH, true, "title", false, null, null, true);

        return $this->customDisplayBrandButtons($brands, $anchor_link, $color, $shape);
    }

    private function customDisplayBrandButtons($brands, $anchor_link = false, $color = "btn-primary", $shape = "btn-round"){
        ob_start();

        $brand_manager = new BrandManager();

        $brand_product_count = $brand_manager->getBrandProductCount();

        foreach ( $brands as $brand ) {
            $id = $brand->id;

            if(!isset($brand_product_count[$id])
                || $brand_product_count[$id]===null
                || $brand_product_count[$id] < 1){
                continue;
            }

            $name = $brand->title;
            $slug = $brand->permalink;

            if($anchor_link){
                ?>

                [button color="<?= $color; ?>" size="btn-lg" shape="<?= $shape; ?>"
                outline="false" icon="fa-chevron-right" icon_position="right" target="" rel=""
                href="#<?= $slug; ?>"]<?= $name; ?>[/button]

                <?php
            }
            else{
                ?>

                [button color="<?= $color; ?>" size="btn-lg" shape="<?= $shape; ?>" outline="false" block="false"
                target="" rel="" href="/hersteller/<?= $slug; ?>/"]<?= $name; ?>[/button]

                <?php
            }
        }

        $content = ob_get_contents();
        ob_end_clean();

        return ShortcodeHelper::doShortcode($content);
    }

    public function getTag()
    {
        return 'hersteller';
    }
}