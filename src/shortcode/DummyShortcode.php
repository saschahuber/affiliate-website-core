<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

class DummyShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        return json_encode([$atts, $content, $tag]);
    }

    public function getTag()
    {
        return "dummy";
    }
}