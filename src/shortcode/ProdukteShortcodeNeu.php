<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class ProdukteShortcodeNeu extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $ids = explode(',', ArrayHelper::getArrayValue($atts, 'include', ''));
        $limit = intval(ArrayHelper::getArrayValue($atts, 'limit', -1));
        $layout = ArrayHelper::getArrayValue($atts, 'layout', 'grid');
        $show_detail_button = ArrayHelper::getArrayValue($atts, 'show_detail_button', 'false') === 'true';
        $show_meta = ArrayHelper::getArrayValue($atts, 'show_meta', 'false') === 'true';
        $show_reduced_badge = ArrayHelper::getArrayValue($atts, 'show_reduced_badge', 'false') === 'true';
        $show_review_score = ArrayHelper::getArrayValue($atts, 'show_review_score', 'false') === 'true';

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $products = $product_manager->getByIds($ids);

        return $product_manager->displayProducts($products, $limit, $layout, $show_detail_button, $show_meta,
            $show_reduced_badge, $show_review_score);
    }

    public function getTag()
    {
        return "produkte_neu";
    }
}