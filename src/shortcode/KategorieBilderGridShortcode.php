<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\BufferHelper;

class KategorieBilderGridShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $cat_ids = explode(',', $atts['cat_ids']);
        $classes = $atts['classes'];
        $columns = intval($atts['columns']);
        $max_width = intval($atts['max_width']);

        $col_class = "";
        switch ($columns) {
            case 2:
                $col_class = 'col-sm-6';
                break;
            case 3:
                $col_class = 'col-sm-4';
                break;
            case 4:
                $col_class = 'col-sm-3';
                break;
            case 5:
                $col_class = 'col-sm-5th';
                break;
            case 6:
                $col_class = 'col-sm-2';
                break;
        }

        $content = "";

        $chunks = array_chunk($cat_ids, $columns);

        foreach ($chunks as $chunk) {
            $content .= '<div class="row row-padding">';
            foreach ($chunk as $cat_id) {
                $content .= '<div class="col-padding ' . $col_class . '">';
                $content .= '[kategorie_bild cat_id="' . $cat_id . '" classes="' . $classes . '" max_width="' . $max_width . '"]';
                $content .= '</div>';
            }
            $content .= '</div>';
        }


        return ShortcodeHelper::doShortcode($content);
    }

    public function getTag()
    {
        return "kategorie_bilder_grid";
    }
}