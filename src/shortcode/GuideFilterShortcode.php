<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\misc\GuideFilter;

class GuideFilterShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $guide_id = $atts['id'];
        $guide_filter = new GuideFilter($guide_id);
        return $guide_filter->getContent();
    }

    public function getTag()
    {
        return "guide_filter";
    }
}