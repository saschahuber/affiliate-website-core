<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class ColShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        ob_start();

        $span = ArrayHelper::getArrayValue($atts, 'span', null);
        $class = ArrayHelper::getArrayValue($atts, 'class', null);

        if($span){
            $col_class = "col-".$span;
        }
        else if($class){
            $col_class = $class;
        }
        else{
            $col_class = "col";
        }

        ?>
        <div class="col col-12 <?=$col_class?>">
            <div class="col-container">
                <?php
                echo $content;
                ?>
            </div>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($output);
    }

    public function getTag()
    {
        return "col";
    }
}