<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\BufferHelper;

class SearchFormShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        return BufferHelper::buffered(function (){
            ?>
            <style>
                #search-container {
                    position: relative;
                }

                #search-results-container ::-webkit-scrollbar {
                    width: 10px;
                }

                #search-results-container ::-webkit-scrollbar-track {
                    background: #f1f1f1;
                }

                /* Handle */
                #search-results-container ::-webkit-scrollbar-thumb {
                    background: #888;
                }

                /* Handle on hover */
                #search-results-container ::-webkit-scrollbar-thumb:hover {
                    background: #555;
                }

                .m-search #search-input{
                    width: calc(100% - 65px);
                    height: 60px;
                    padding: 25px;
                    border: none;
                    border-radius: 50px 0 0 50px;
                    font-size: 18px;
                    font-weight: bold;
                }

                .m-search #search-button {
                    background-color: var(--primary_color);
                    width: 65px;
                    display: table;
                    border-radius: 0 50px 50px 0;
                    cursor: pointer;
                }

                .m-search #search-button:hover {
                    background-color: var(--secondary_color);
                }

                .m-search #search-button > div {
                    display: table-cell;
                    vertical-align: middle;
                }

                .m-search #search-button > div p {
                    margin-top: 0;
                    margin-bottom: 0;
                    text-align: center;
                    color: #fff;
                    font-size: 30px;
                }



                .m-search #search-button > div p i{
                    margin-right: 5px;
                }

                .m-search #search-results-container{
                    width: 100%;
                    height: fit-content;
                    box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%),0 6px 8px 0 rgb(0 0 0 / 19%)!important;
                    transition: 0.3s;
                    background-color: #fff;
                    position: absolute;
                    max-width: 900px;
                    left: 0;
                    right: 0;
                    margin: auto;
                    margin-top: 25px;
                }

                #search-results-container > div{
                    overflow-y: scroll;
                    max-height: 500px;
                }

                .m-search #search-results-container.hidden{
                    height: 0;
                }

                .m-search #search-results-container .search-result-type-container {
                    margin: 5px 0;
                }

                .m-search #search-results-container .search-result-title {
                    font-weight: bold;
                }

                .m-search #search-results-container .search-result-type {
                    background-color: #09c;
                    color: #fff;
                    border-radius: 50px;
                    padding: 5px 15px;
                    margin: 5px 0;
                }

                .m-search #search-results-container a.result-link{
                    text-decoration: none;
                    color: inherit;
                }

                .m-search #search-results-container ul{
                    padding: 0;
                }

                .m-search #search-results-container ul{
                    list-style-type: none;
                }

                .m-search #search-results-container ul li{
                    transition: 0.3s;
                    padding: 16px;
                }

                .m-search #search-input-container{
                    max-width: 750px;
                    margin: auto;
                }

                .m-search #search-results-container ul li img{
                    max-height: 150px;
                    object-fit: contain;
                }

                .m-search #search-results-container ul li:hover{
                    background-color: #eeeeee;
                }

                .m-search #search-results-container .no-search-results .emoji{
                    font-size: 38px;
                }

                .m-search #search-results-container .no-search-results{
                    padding: 16px;
                }

                .m-search #search-results-container .no-search-results p{
                    padding: 5px;
                    margin-bottom: 0;
                    text-align: center;
                }

                .m-search #search-loading-animation .fa-2x {
                    width: fit-content;
                }

                .m-search #search-loading-animation {
                    display: none;
                    width: fit-content;
                    padding: 0;
                    position: absolute;
                    left: 0;
                    right: 0;
                    margin: auto;
                    margin-top: -48px;
                }
            </style>
            <script>
                var searchIsLoading = false;
                function search(keywords, searchInType){
                    if(searchIsLoading){
                        return;
                    }

                    searchIsLoading = true;
                    document.getElementById("search-loading-animation").style.display="block";

                    let params = {
                        keywords: keywords
                    }

                    doPublicApiCall('search/do_search', params, function (response) {
                        saveAnalyticsEvent('search', {'keywords': keywords});

                        searchIsLoading = false;
                        document.getElementById("search-loading-animation").style.display="none";
                        document.getElementById("search-results-container").classList.remove("hidden");
                        if (keywords.length===0) {
                            document.getElementById("search-results-container").innerHTML="";
                            document.getElementById("search-results-container").classList.add("hidden");
                            return;
                        }
                        document.getElementById("search-results-container").innerHTML=response;
                    });
                }

                function hideSearchResults(){
                    document.getElementById("search-loading-animation").style.display="none";
                    document.getElementById("search-results-container").innerHTML="";
                    document.getElementById("search-results-container").classList.add("hidden");
                }

                function hideSearchResultsDelayed(timeout){
                    setTimeout(function() { hideSearchResults(); }, timeout);
                }

                function delay(callback, ms) {
                    var timer = 0;
                    return function() {
                        var context = this, args = arguments;
                        clearTimeout(timer);
                        timer = setTimeout(function () {
                            callback.apply(context, args);
                        }, ms || 0);
                    };
                }

                $(document).ready(function(e) {
                    $('#search-input').keyup(delay(function (e) {
                        search(this.value);
                    }, 500));
                });
            </script>
            <div class="m-search">
                <div id="search-container">
                    <form onsubmit="hideSearchResults(); search(this.value); return false;">
                        <div>
                            <div id="search-input-container">
                                <div class="search-input-field-container">
                                    <div style="display: flex;">
                                        <input id="search-input" type="text" onfocusout="hideSearchResultsDelayed(500)" placeholder="<?=$this->getPlaceholder()?>" size="64">
                                        <div id="search-button">
                                            <div>
                                                <p><i class="fas fa-search"></i></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="search-loading-animation">
                                        <div class="fa-2x">
                                            <i class="fas fa-spinner fa-spin"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="search-results-container" class="hidden"></div>
                    </form>
                </div>
            </div>
            <?php
        });
    }

    public function getTag()
    {
        return "search_form";
    }

    public function getPlaceholder(){
        return "Suchbegriff eingeben...";
    }
}