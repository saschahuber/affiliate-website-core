<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ImgUtils;

class KategorieBildShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $cat_id = $atts['cat_id'];
        $classes = $atts['classes'];
        $max_width = $atts['max_width'];
        $category = get_term($cat_id);

        $img_id = wp_get_terms_meta($cat_id, 'bild-id', true);

        $img_data = wpGetAttachmentImageSrc($img_id, 'medium');

        ob_start();

        ?>
        <div class="kategorie-container <?= $classes ?>">
            <p style="text-align: center;">
                <a href="<?= get_term_link($category); ?>">
                    <?= ImgUtils::getImageTag($img_data[0], true, $category->title, $category->title, 350, null) ?>
                </a>
            </p>
        </div>
        <?php

        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function getTag()
    {
        return "kategorie_bild";
    }
}