<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\BrandManager;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class BrandOverviewShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        ob_start();

        $brand_manager = new BrandManager();
        $brands = $brand_manager->getAll('publish', true, 'title', false, null, null, true);

        $brand_product_count = $brand_manager->getBrandProductCount();

        $MAX_COLUMNS = 6; //Muss vielfaches von 3 oder 4 sein & Teiler von 12 => 1, 2, 3, 4, 6
        $counter = 0;

        if(ArrayHelper::getArrayValue($atts, 'hide_links', "false") !== "true"){
            foreach($brands as $brand){

                $id = $brand->id;

                if(!isset($brand_product_count[$id])
                    || $brand_product_count[$id]===null
                    || $brand_product_count[$id] < 1){
                    continue;
                }

                $name = $brand->title;
                $slug = $brand->permalink;

                ?>

                [button color="btn-primary" size="btn-lg" shape="btn-round" icon="fa-chevron-right" icon_position="right"
                href="#<?= $slug; ?>"]<?= $name; ?>[/button]

                <?php
            }
        }

        foreach ( $brands as $brand ) {
            $id = $brand->id;

            if(!isset($brand_product_count[$id])
                || $brand_product_count[$id]===null
                || $brand_product_count[$id] < 1){
                continue;
            }

            $name = $brand->title;
            $slug = $brand->permalink;

            ?>

            <br>
            <br>

            <h2 id="<?= $slug; ?>"><?= $name; ?></h2>

            <?= $brand->short_content; ?>

            <h3>Geräte von <?=$name?></h3>
            [row]
            [col class="col-lg-12"]
            [produkte limit="4" orderby="rand" order="desc" layout="grid" review="false" show_detail_button="true"
            buy_button="true" details_fields="true" details_tax="true" reduced="false" align="left"
            slider="false" hersteller="<?= $slug; ?>"]
            [/col]
            [/row]

            <br>

            <p style="text-align: center;">[button color="btn-primary" size="btn-lg" shape="btn-rounded"
                icon="fa-chevron-right" icon_position="right"
                href="/hersteller/<?= $slug; ?>/"]Alle Geräte von <?= $name; ?>[/button]</p>

            <br>
            <br>

            <?php
        }

        $content = ob_get_contents();
        ob_end_clean();

        return ShortcodeHelper::doShortcode($content);
    }

    public function getTag()
    {
        return "brand_overview";
    }
}