<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\StockImageManager;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class StockAttachmentShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $stock_image_manager = new StockImageManager();

        $attachment_id = ArrayHelper::getArrayValue($atts, 'attachment_id', null);
        $width = ArrayHelper::getArrayValue($atts, 'width', null);
        $height = ArrayHelper::getArrayValue($atts, 'height', null);
        $title = ArrayHelper::getArrayValue($atts, 'title', null);
        $alt_text = ArrayHelper::getArrayValue($atts, 'alt_text', null);
        $caption = ArrayHelper::getArrayValue($atts, 'caption', false);
        $align = ArrayHelper::getArrayValue($atts, 'align', false);

        ob_start();
        ?>
        <div class="attachment_img<?=($align?" ".$align:"")?>">
            <div class="attachment_container">
                <?=$stock_image_manager->getAttachmentImageTag($attachment_id, $alt_text, $title, $width, $height)?>
                <?php if($caption): ?>
                    <div class="attachment_img_caption">
                        <p><?=$caption?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($output);
    }

    public function getTag()
    {
        return "stock_attachment";
    }
}