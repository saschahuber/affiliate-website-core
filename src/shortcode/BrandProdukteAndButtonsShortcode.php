<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;

class BrandProdukteAndButtonsShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        ob_start();

        $slug = $atts['slug'];
        $name = $atts['name'];

        ?>
        [row]
        [col class="col-lg-12"]
        [produkte limit="4" orderby="rand" order="desc" layout="grid" review="false" detail_button="true"
        buy_button="true" details_fields="true" details_tax="true" reduced="false" align="left" slider="false" hersteller="<?= $slug; ?>"]
        [/col]
        [/row]

        <p style="text-align: center;">[button color="btn-primary" size="btn-lg" shape="btn-rounded"
            icon="fa-chevron-right" icon_position="right"
            href="/hersteller/<?= $slug; ?>/"]Alle <?= $name; ?> Geräte[/button]</p>

        <?php

        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    public function getTag()
    {
        return "brand_produkte_and_button";
    }
}