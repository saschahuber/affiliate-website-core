<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\manager\UploadManager;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class UploadShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        global $CONFIG;

        $upload_manager = new UploadManager();

        $upload_id = ArrayHelper::getArrayValue($atts, 'upload_id', null);
        $width = ArrayHelper::getArrayValue($atts, 'width', null);
        $height = ArrayHelper::getArrayValue($atts, 'height', null);
        $caption = ArrayHelper::getArrayValue($atts, 'caption', false);
        $align = ArrayHelper::getArrayValue($atts, 'align', false);

        $upload = $upload_manager->getUpload($upload_id);

        if(!$upload){
            return null;
        }

        $file_url = $CONFIG->website_domain . '/data/media' . $upload->file_path;

        switch($upload->file_extension){
            case "mp4":
                $caption_code = "";
                if($caption){
                    $caption_code = 'caption="'.$caption.'"';
                }

                $align_code = "";
                if($align){
                    $align_code = 'align="'.$align.'"';
                }
                return ShortcodeHelper::doShortcode('[video src="'.$file_url.'" '.$caption_code.' '.$align_code.' width="'.$width.'" height="'.$height.'"]');
            default:
                return json_encode($upload);
        }
    }

    public function getTag()
    {
        return "upload";
    }
}