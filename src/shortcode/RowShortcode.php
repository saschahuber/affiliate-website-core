<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\Helper;

class RowShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        ob_start();

        $class = ArrayHelper::getArrayValue($atts, 'class', null);

        ?>
        <div class="row <?=$class?>">
            <?php
            echo $content;
            ?>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($output);
    }

    public function getTag()
    {
        return "row";
    }
}