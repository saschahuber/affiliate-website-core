<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\service\AppStoreDataService;

class AppBoxShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        if(!isset($atts[0]) || !isset($atts[1])){
            return null;
        }

        $app_store_data_service = new AppStoreDataService();

        $app_store = $atts[0];
        $app_id = $atts[1];

        return ShortcodeHelper::doShortcode($app_store_data_service->getAppDownloadInfo($app_store, $app_id));
    }

    public function getTag()
    {
        return "appbox";
    }

    public function getDocs()
    {
        return array(
            'description' => 'Zeigt Download-Boxen aus den App-Stores an',
            'params' => array(
                array('param' => 0, 'description' => 'App Store (z.B. Google)',
                    'examples' => '"1,2,3,4"', 'default' => null),
                array('param' => 1, 'description' => 'ID der App aus dem App-Store', 'examples' => '4', 'default' => null)
            ));
    }
}