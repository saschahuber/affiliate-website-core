<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class DisplayProductsShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $keyword = ArrayHelper::getArrayValue($atts, 'keyword', null);
        $layout = ArrayHelper::getArrayValue($atts, 'layout', 'grid');
        #$columns = ArrayHelper::getArrayValue($atts, 'columns', (($layout==='grid'?4:1)));
        $show_detail_button = ArrayHelper::getArrayValue($atts, 'show_detail_button', false) === "true";
        $has_all_categories = ArrayHelper::getArrayValue($atts, 'has_all_categories', false) === "true";
        $show_meta = ArrayHelper::getArrayValue($atts, 'show_meta', false) === "true";
        $show_price = ArrayHelper::getArrayValue($atts, 'show_price', false) === "true";

        $limit = intval(ArrayHelper::getArrayValue($atts, 'limit', false));
        $produkt_ids = array_filter(explode(',', ArrayHelper::getArrayValue($atts, 'produkte', false)));
        $kategorie_ids = array_filter(explode(',', ArrayHelper::getArrayValue($atts, 'kategorie', false)));
        $hersteller_ids = array_filter(explode(',', ArrayHelper::getArrayValue($atts, 'hersteller', false)));
        $system_ids = array_filter(explode(',', ArrayHelper::getArrayValue($atts, 'system', false)));
        $raum_ids = array_filter(explode(',', ArrayHelper::getArrayValue($atts, 'raum', false)));
        $min_price = ArrayHelper::getArrayValue($atts, 'price_min', false);
        $max_price = ArrayHelper::getArrayValue($atts, 'price_max', false);
        $meta_true = ArrayHelper::getArrayValue($atts, 'meta_true', false);
        $data_field = ArrayHelper::getArrayValue($atts, 'data_field', false);
        $data_values = ArrayHelper::getArrayValue($atts, 'data_values', false);
        $data_values = explode(',', $data_values);
        $data_values = array_filter($data_values, function ($element) {
            return is_string($element) && '' !== trim($element);
        });
        $reduced = ArrayHelper::getArrayValue($atts, 'reduced', 'false') === "true";
        $data_fields_raw = ArrayHelper::getArrayValue($atts, 'data_fields', false);
        $orderby = ArrayHelper::getArrayValue($atts, 'order_by', 'rand');
        $order = ArrayHelper::getArrayValue($atts, 'order', 'desc');

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        $data_fields = [];
        if($data_fields_raw){
            $data_fields_raw = base64_decode($data_fields_raw);

            $raw_data_fields = explode(';', $data_fields_raw);
            foreach($raw_data_fields as $raw_data_field){
                $data_field_parts = explode('=', $raw_data_field);

                $data_field_id = $data_field_parts[0];

                $value = explode(',', $data_field_parts[1]);

                $data_fields[$data_field_id] = $value;
            }
        }

        $products = $product_manager->findProducts($keyword, $produkt_ids, $kategorie_ids, $hersteller_ids, $system_ids,
            $raum_ids, $min_price, $max_price, $reduced, $data_field, $data_values, $data_fields, $orderby, $order, $limit, $has_all_categories);

        if(count($products) < 1){
            return '';
        }

        return $product_manager->displayProducts($products, $limit, $layout, $show_detail_button, $show_meta,
            true, false, $show_price);
    }

    public function getTag()
    {
        return "display_products";
    }
}