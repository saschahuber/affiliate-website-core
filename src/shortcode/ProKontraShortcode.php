<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\component\ElevatedCard;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\BufferHelper;

class ProKontraShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $product_id = ArrayHelper::getArrayValue($atts, 'product_id', false);

        if(!$product_id) {
            $current_post = getTheItem();
        }
        else{
            $current_post = (AffiliateInterfacesHelper::getProductManager())->getById($product_id);
        }

        if(!$current_post){
            return null;
        }

        $pro_titel = ArrayHelper::getArrayValue($current_post->meta, "pro-titel", false);
        $kontra_titel = ArrayHelper::getArrayValue($current_post->meta, "kontra-titel", false);
        $pro_liste = ArrayHelper::getArrayValue($current_post->meta, "pro-liste", false);
        $kontra_liste = ArrayHelper::getArrayValue($current_post->meta, "kontra-liste", false);

        $pro_liste_items = explode("\n", trim($pro_liste));
        $pro_liste_code = "";
        foreach($pro_liste_items as $pro_liste_item){
            $pro_liste_code .= '<p><i class="fa fa-thumbs-up" style="color: #7ab317;"></i>&nbsp;'.$pro_liste_item.'</p>';
        }

        $kontra_liste_items = explode("\n", trim($kontra_liste));
        $kontra_liste_code = "";
        foreach($kontra_liste_items as $kontra_liste_item){
            $kontra_liste_code .= '<p><i class="fa fa-thumbs-down" style="color: #c01313;"></i>&nbsp;'.$kontra_liste_item.'</p>';
        }

        $content = '
	<br>
	[row]
		[col class="col-lg-6"]
			<h3 style="margin-top: 0;"><i class="fa fa-plus-circle" style="color: #7ab317;"></i>&nbsp;'.($pro_titel!='' ? $pro_titel : "Gut finden wir:").'</h3>
	        [spacer size="sm"]
			'.$pro_liste_code.'
		[/col]
		[col class="col-lg-6"]
			<h3 style="margin-top: 0;"><i class="fa fa-minus-circle" style="color: #c01313;"></i>&nbsp;'.($kontra_titel!='' ? $kontra_titel : "Nicht so gut finden wir:").'</h3>
			[spacer size="sm"]
			'.$kontra_liste_code.'
		[/col]
	[/row]
	<br>';

        $code = ShortcodeHelper::doShortcode($content);

        return BufferHelper::buffered(function() use ($code){
            ?>
            <div style="margin: auto; max-width: 1200px; /* text-align: center; */">
                <?=(new ElevatedCard($code))->getContent()?>
            </div>
            <?php
        });
    }

    public function getTag()
    {
        return "pro_kontra_liste";
    }
}