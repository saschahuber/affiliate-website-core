<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

abstract class AbstractShortcode {
    public abstract function handle($atts, $content, $tag);

    public abstract function getTag();

    public function getDocs(){
        return null;
    }
}