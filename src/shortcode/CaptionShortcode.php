<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use DOMDocument;
use DOMXPath;
use Exception;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\Helper;
use saschahuber\saastemplatecore\helper\ImgUtils;
use saschahuber\saastemplatecore\helper\LogHelper;

class CaptionShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        if(!$content){
            return $content;
        }

        if(!strstr($content, '<picture')){
            try {
                $dom = new DOMDocument();
                @$dom->loadHTML($content);
                $xpath = new DOMXPath($dom);
                $images = $xpath->query("//img");

                $attributes = [];
                foreach ($images as $img) {
                    foreach ($img->attributes as $attr) {
                        $name = $attr->nodeName;
                        $value = $attr->nodeValue;

                        $attributes[$name] = $value;
                    }
                }

                global $CONFIG;
                
                $src = ArrayHelper::getArrayValue($attributes, 'src', null);

                if($src) {
                    $src = str_replace([$CONFIG->website_domain, $CONFIG->host], '', $src);
                }

                $alt = ArrayHelper::getArrayValue($attributes, 'alt', null);
                $title = ArrayHelper::getArrayValue($attributes, 'title', $alt);
                $width = ArrayHelper::getArrayValue($attributes, 'width', null);
                $height = ArrayHelper::getArrayValue($attributes, 'height', null);
                $class = ArrayHelper::getArrayValue($attributes, 'class', null);

                $content = ImgUtils::getImageTag($src, true, $alt, $title, $width, $height, [$class]);
            }
            catch (Exception $e){
                LogHelper::logToMinuteLog('Shortcode', "Error parsing caption shortcode: " . htmlspecialchars($content) . ' => ' . $e->getMessage(), LogHelper::LOG_LEVEL_WARNING);
            }
        }


        ob_start();
        ?>
        <div class="attachment" <?=Helper::ifstr(isset($atts['width']), 'width="'.$atts['width'].'"')?>>
            <?php
            echo $content;
            ?>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($output);
    }

    public function getTag()
    {
        return "caption";
    }
}