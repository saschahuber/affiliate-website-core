<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\Helper;

class VideoShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $src = ArrayHelper::getArrayValue($atts, 'src', null);
        $width = ArrayHelper::getArrayValue($atts, 'width', null);
        $height = ArrayHelper::getArrayValue($atts, 'height', null);
        $caption = ArrayHelper::getArrayValue($atts, 'caption', false);

        $style = null;

        if($width){
            $style = "width: " . $width . "px";
        }

        ob_start();
        ?>
        <div class="video-container">
            <video width="<?=$width?>" height="<?=$height?>" <?=Helper::ifstr($style!==null, 'style="'.$style.'"')?> controls preload="metadata">
                <source src="<?=$src?>" type="video/mp4">
            </video>

            <?php if($caption && strlen($caption)): ?>
                <p class="centered"><?=$caption?></p>
            <?php endif; ?>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($output);
    }

    public function getTag()
    {
        return "video";
    }
}