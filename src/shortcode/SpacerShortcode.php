<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\Helper;

class SpacerShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $height = ArrayHelper::getArrayValue($atts, 'height', "64px");
        $size = ArrayHelper::getArrayValue($atts, 'size', null);

        $attributes = null;

        if($size !== null) {
            $attributes = 'class="spacer-size-' . $size . '"';
        }
        else{
            $attributes = 'style="height: '.$height.'"';
        }

        return '<div '.$attributes.'>
        <!--<span class="spacer_inner"></span>-->
    </div>';
    }

    public function getTag()
    {
        return "spacer";
    }
}