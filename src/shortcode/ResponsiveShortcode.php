<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class ResponsiveShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $screen = ArrayHelper::getArrayValue($atts, 'screen', 'lg');
        $display = ArrayHelper::getArrayValue($atts, 'display', $tag=='hidden'?'none':'block');

        $class = "d-".($display=='none'?'block':'none')." d-$screen-$display";

        ob_start();
        ?>
        <div class="<?=$class?>">
            <?=$content?>
        </div>
        <?php
        $output = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($output);
    }

    public function getTag()
    {
        return ["hidden", "visible"];
    }
}