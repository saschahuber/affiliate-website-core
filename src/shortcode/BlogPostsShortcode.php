<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\manager\PostManager;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class BlogPostsShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $ids = explode(',', ArrayHelper::getArrayValue($atts, 'include', ''));
        $limit = intval(ArrayHelper::getArrayValue($atts, 'limit', false));
        $layout = ArrayHelper::getArrayValue($atts, 'layout', 'grid');
        $kategorie = ArrayHelper::getArrayValue($atts, 'kategorie', false);
        $reduced = ArrayHelper::getArrayValue($atts, 'reduced', 'false') === "true";
        $orderby = ArrayHelper::getArrayValue($atts, 'orderby', 'rand');
        $order = ArrayHelper::getArrayValue($atts, 'order', 'desc');
        $field = ArrayHelper::getArrayValue($atts, 'field', '');

        $post_manager = new PostManager();
        $posts = [];

        if($kategorie){
            $posts = $post_manager->getByTaxonomyAliasMulti(explode(',', $kategorie), $reduced, $limit, $orderby, $order, $field);
        }
        else if(count($ids) > 0){
            $posts = $post_manager->getByOldIds($ids);
        }

        return $post_manager->displayPosts($posts, $limit, $layout);
    }

    public function getTag()
    {
        return "blogposts";
    }
}