<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;

class SocialLinkShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $title = $atts['title'];
        $img_id = $atts['img_id'];
        $img_text = $atts['img_text'];
        $btn_url = $atts['btn_url'];
        $btn_text = $atts['btn_text'];

        $content =
            ($title ? '<h2 style="text-align: center;">'.$title.'</h2>':'').'
		<p style="text-align: center;">'.wpGetAttachmentImage($img_id, 'medium').'</p>
		<p style="text-align: center;">'.$img_text.'</p>
		<p style="text-align: center;">[button color="btn-primary" size="btn-md" shape="btn-round"
			outline="false" block="false" icon="fa-chevron-right"
			icon_position="right" target="" rel="" href="'.$btn_url.'"]'.$btn_text.'[/button]</p>
	';

        return ShortcodeHelper::doShortcode($content);
    }

    public function getTag()
    {
        return "social_link_row";
    }
}