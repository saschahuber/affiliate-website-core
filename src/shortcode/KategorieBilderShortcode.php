<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\manager\ImageManager;
use saschahuber\saastemplatecore\helper\ArrayHelper;
use saschahuber\saastemplatecore\helper\ImgUtils;

class KategorieBilderShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        global $LAYOUT_MANAGER;

        $cat_ids = ArrayHelper::getArrayValue($atts, 'cat_ids', false);
        $new_cat_ids = ArrayHelper::getArrayValue($atts, 'new_cat_ids', false);
        $columns = ArrayHelper::getArrayValue($atts, 'columns', 4);
        $max_width = ArrayHelper::getArrayValue($atts, 'max_width', 500);

        $product_manager = AffiliateInterfacesHelper::getProductManager();

        if($cat_ids) {
            $categories = $product_manager->getTaxonomiesByOldIds(explode(',', $cat_ids));
        }
        else if($new_cat_ids) {
            $categories = $product_manager->getTaxonomiesByIds(explode(',', $new_cat_ids));
        }
        else{
            $categories = $product_manager->getTaxonomies();
        }

        $image_manager = new ImageManager();

        $items = [];
        foreach($categories as $cat){
            if($cat->attachment_id === null){
                continue;
            }

            ob_start();
            ?>
            <div class="kategorie-container">
                <p style="text-align: center;">
                    <a href="<?=$product_manager->getTaxonomyPermalink($cat)?>">
                        <?=ImgUtils::getImageTag($image_manager->getAttachmentUrl($cat->attachment_id), true, $cat->title, $cat->title, 350, null)?>
                    </a>
                </p>
            </div>
            <?php
            $content = ob_get_contents();
            ob_end_clean();
            $items[] = $content;
        }

        return $LAYOUT_MANAGER->grid($items, $columns, "md");
    }

    public function getTag()
    {
        return "kategorie_bilder_grid";
    }
}