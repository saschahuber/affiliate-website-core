<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\component\form\NewsletterForm;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class NewsletterShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $hide_topic_selection = ArrayHelper::getArrayValue($atts, 'hide_topic_selection', 'true') === 'true';
        $style = ArrayHelper::getArrayValue($atts, 'style', 'dark');
        $analytics_suffix = ArrayHelper::getArrayValue($atts, 'analytics', null);

        #$hide_topic_selection = false;

        return (new NewsletterForm($hide_topic_selection, $style, $analytics_suffix))->getContent();
    }

    public function getTag()
    {
        return "newsletter_form";
    }
}