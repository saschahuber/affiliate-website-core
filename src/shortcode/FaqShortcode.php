<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\component\FaqComponent;
use saschahuber\affiliatewebsitecore\service\FaqService;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class FaqShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $faq_id = ArrayHelper::getArrayValue($atts, 'id', null);
        return (new FaqComponent((new FaqService())->getQuestionsFromId($faq_id)))->getContent();
    }

    public function getTag()
    {
        return "faq";
    }
}