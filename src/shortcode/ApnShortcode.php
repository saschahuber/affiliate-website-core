<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\helper\AffiliateInterfacesHelper;
use saschahuber\affiliatewebsitecore\helper\ShortcodeHelper;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;
use saschahuber\saastemplatecore\helper\ArrayHelper;

class ApnShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        $asin = $atts['asin'];
        $title = ArrayHelper::getArrayValue($atts, 'tpl-pname', null);
        $button_text = "Auf Amazon ansehen";
        if(isset($atts['buttontext'])) {
            $button_text = $atts['buttontext'];
        }

        $product_manager = AffiliateInterfacesHelper::getProductManager();
        $product = $product_manager->getByAsin($asin, true, null);

        if($product === false){
            $product = $product_manager->downloadAmazonProductInfo($asin);
            if($product === null){
                return '<p>Produkt nicht verfügbar</p>';
            }
        }

        ob_start();
        ?>
        [row]
        [col span="lg-12"]
        <?=$product_manager->displayProduct($product, $title, $button_text)?>
        [/col]
        [/row]
        <?php
        $content = ob_get_contents();
        ob_end_clean();
        return ShortcodeHelper::doShortcode($content);
    }

    public function getTag()
    {
        return "apn";
    }
}