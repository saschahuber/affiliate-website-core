<?php

namespace saschahuber\affiliatewebsitecore\shortcode;

use saschahuber\affiliatewebsitecore\service\ImprintCopyrightService;
use saschahuber\affiliatewebsitecore\shortcode\AbstractShortcode;

class ImprintCopyrightShortcode extends AbstractShortcode {
    public function handle($atts, $content, $tag){
        return (new ImprintCopyrightService())->getCopyrightInfoHtml();
    }

    public function getTag()
    {
        return "imprint_copyright_info";
    }
}